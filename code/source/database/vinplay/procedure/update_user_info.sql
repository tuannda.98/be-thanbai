DROP PROCEDURE `update_user_info`;
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_user_info`(IN `p_user_id` INT(11), IN `p_new` VARCHAR(100), IN `p_type` INT(11)) NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER BEGIN
 IF p_type = 1 THEN -- avatar
  UPDATE users SET avatar = p_new WHERE id = p_user_id;
    ELSEIF p_type = 2 THEN -- password
  UPDATE users SET password = p_new WHERE id = p_user_id;
    ELSEIF p_type = 3 THEN -- identification
  UPDATE users SET identification = p_new WHERE id = p_user_id;
    ELSEIF p_type = 4 THEN -- mobile
  UPDATE users SET mobile = p_new WHERE id = p_user_id;
    ELSEIF p_type = 5 THEN -- email
  UPDATE users SET email = p_new WHERE id = p_user_id;
    ELSEIF p_type = 6 THEN -- nickname
  UPDATE users SET nick_name = p_new WHERE id = p_user_id;
 ELSEIF p_type = 7 THEN -- status
  UPDATE users SET status = p_new WHERE id = p_user_id;
 ELSEIF p_type = 8 THEN -- status, mobile
  UPDATE users SET status = SUBSTRING_INDEX(p_new, ',', -1), mobile = SUBSTRING_INDEX(p_new, ',', 1) WHERE id = p_user_id;
 ELSEIF p_type = 9 THEN -- insert user
  INSERT INTO users(user_name, password, vin, vin_total, xu, xu_total, avatar) VALUES(SUBSTRING_INDEX(p_new, ',', 1), SUBSTRING_INDEX(p_new, ',', -1), 0, 0, 500000, 500000, '0');
 ELSEIF p_type = 10 THEN -- insert user facebook
  INSERT INTO users(user_name, nick_name, facebook_id, vin, vin_total, xu, xu_total, avatar) VALUES(CONCAT('FB', UNIX_TIMESTAMP()), CONCAT('FB', UNIX_TIMESTAMP()), p_new, 0, 0, 500000, 500000, '0');        
 ELSEIF p_type = 11 THEN -- insert user google
  INSERT INTO users(user_name, nick_name, google_id, vin, vin_total, xu, xu_total, avatar) VALUES(CONCAT('GG', UNIX_TIMESTAMP()), CONCAT('GG', UNIX_TIMESTAMP()), p_new, 0, 0, 500000, 500000, '0');        
 ELSEIF p_type = 12 THEN -- money login otp
  UPDATE users SET login_otp = SUBSTRING_INDEX(p_new, ',', 1), status = SUBSTRING_INDEX(p_new, ',', -1) WHERE id = p_user_id;
 ELSEIF p_type = 13 THEN -- status when active mobile
  UPDATE users SET status = p_new, security_time = CURRENT_TIMESTAMP WHERE id = p_user_id;
 ELSEIF p_type = 14 THEN -- insert user apple
  INSERT INTO users(user_name, nick_name, identification, vin, vin_total, xu, xu_total, avatar) VALUES(CONCAT('AP', UNIX_TIMESTAMP()), CONCAT('AP', UNIX_TIMESTAMP()), p_new, 0, 0, 500000, 500000, '0');   
 ELSEIF p_type = 15 THEN -- insert user by device id
  INSERT INTO users(user_name, nick_name, identification, vin, vin_total, xu, xu_total, avatar) VALUES(CONCAT('DEV', UNIX_TIMESTAMP()), CONCAT('DEV', UNIX_TIMESTAMP()), p_new, 0, 0, 500000, 500000, '0');        
    END IF;
END