package casio.king365.util;

import com.google.common.base.Charsets;
import com.google.common.base.Throwables;
import com.google.common.io.CharStreams;
import com.vinplay.vbee.common.models.UserModel;
import okhttp3.*;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Map;

public class KingUtil {

    private static final Logger logger = Logger.getLogger("api");

    static RequestConfig config;
    static final int timeout = 90;
    private static HttpClient httpClient;
    public static final MediaType MEDIA_APPLICATIONJSON
            = MediaType.get("application/json; charset=utf-8");

    public static void main(String[] args) {
//        System.out.println("HEHE a Hai start Minigame nay. Khoi dong bitzero.server.Main....");
//        bitzero.server.Main.main(args);
        Date a = new Date();
        System.out.println("instant: " + convertToLocalDateTimeViaInstant(a));
        System.out.println("milisec: " + convertToLocalDateTimeViaMillisecond(a));
    }

    static HttpClient getHttpClient() {
        if (httpClient == null) {
            config = RequestConfig.custom().setConnectTimeout(timeout * 1000).setConnectionRequestTimeout(timeout * 1000)
                    .setSocketTimeout(timeout * 1000).build();
            httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
        }
        return httpClient;
    }

    public static String post(String url, String jsonData) {
        System.out.println("do send to url: " + url + "\n with data: " + jsonData);
        HttpResponse response = null;
        try {
            HttpPost httpost = new HttpPost(url);
            httpost.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0");
            httpost.addHeader("Content-Type", "application/json;charset=UTF-8");
            StringEntity entity = new StringEntity(jsonData, "UTF-8");
            httpost.setEntity(entity);
            response = getHttpClient().execute(httpost);
        } catch (Exception e) {
            e.printStackTrace();
        }

        BasicResponseHandler handler = new BasicResponseHandler();
        String body = "";
        try {
            body = handler.handleResponse(response);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("res content: " + body);
        return body;
    }

    public static String post(String url, String jsonData, String xVersion, String partnerId) {
        System.out.println("do send to url: " + url + "\n with data: " + jsonData);
        HttpResponse response = null;
        try {
            HttpPost httpost = new HttpPost(url);
            httpost.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0");
            httpost.addHeader("Content-Type", "application/json;charset=UTF-8");
            httpost.setHeader("x-version", xVersion);
            httpost.setHeader("x-partner", partnerId);
            StringEntity entity = new StringEntity(jsonData, "UTF-8");
            httpost.setEntity(entity);
            response = getHttpClient().execute(httpost);
        } catch (Exception e) {
            e.printStackTrace();
        }

        BasicResponseHandler handler = new BasicResponseHandler();
        String body = "";
        try {
            body = handler.handleResponse(response);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("res content: " + body);
        return body;
    }

    public static String post(String url, String jsonData, Map<String, String> headers) {
        System.out.println("KingUtil post url: " + url + "\n with data: " + jsonData);
        HttpResponse response = null;
        try {
            HttpPost httpost = new HttpPost(url);
            httpost.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0");
            httpost.addHeader("Content-Type", "application/json;charset=UTF-8");
            headers.forEach((k,v)->{
                System.out.println("headers: " + k +" - " + v);
                httpost.setHeader(k, v);
            });
            StringEntity entity = new StringEntity(jsonData, "UTF-8");
            httpost.setEntity(entity);
            response = getHttpClient().execute(httpost);
        } catch (Exception e) {
            e.printStackTrace();
        }

        BasicResponseHandler handler = new BasicResponseHandler();
        String body = "";
        try {
            body = handler.handleResponse(response);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("res content: " + body);
        return body;
    }

    public static String postOkHttp(String url, String jsonData, Map<String, String> headers) throws IOException {
        System.out.println("KingUtil post url: " + url + "\n with data: " + jsonData);

        headers.put("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0");
        headers.put("Content-Type", "application/json;charset=UTF-8");
        Headers okHeaders = Headers.of(headers);

        OkHttpClient client = new OkHttpClient();
        RequestBody body = RequestBody.create(jsonData, MEDIA_APPLICATIONJSON);
        Request request = new Request.Builder()
                .url(url)
                .headers(okHeaders)
                .post(body)
                .build();
        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }
    }

    public static String get(String urlStr, Map<String, String> headers)  throws IOException {
        System.out.println("KingUtil get url: : " + urlStr);
        URL url = new URL(urlStr);
        URLConnection conn = url.openConnection();
        conn.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0");
        conn.addRequestProperty("Content-Type", "application/json;charset=UTF-8");
        headers.forEach((k,v)->{
            System.out.println("headers: " + k +" - " + v);
            conn.addRequestProperty(k, v);
        });
        try (InputStream inputStream = conn.getInputStream();
             InputStreamReader isr = new InputStreamReader(inputStream, Charsets.UTF_8)) {
            // System.out.println("KingUtil - get(), getResultStr: " + getResultStr);
            return CharStreams.toString(isr);
        }
    }

    public static String getOkHttp(String urlStr, Map<String, String> headers)  throws IOException {
        System.out.println("KingUtil getOkHttp url: : " + urlStr);

        headers.put("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0");
        headers.put("Content-Type", "application/json;charset=UTF-8");

        OkHttpClient client = new OkHttpClient();
        Headers okHeaders = Headers.of(headers);
        Request request = new Request.Builder()
                .url(urlStr)
                .headers(okHeaders)
                .build();

        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }
    }

    public static String get(String urlString) throws IOException {
        // System.out.println("KingUtil - get(), urlString: " + urlString);
        URL url = new URL(urlString);
        URLConnection conn = url.openConnection();
        conn.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0");
        try (InputStream inputStream = conn.getInputStream();
             InputStreamReader isr = new InputStreamReader(inputStream, Charsets.UTF_8)) {
            // System.out.println("KingUtil - get(), getResultStr: " + getResultStr);
            return CharStreams.toString(isr);
        }
    }

    public static String MD5(String str) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] result = md.digest((str).getBytes(StandardCharsets.UTF_8));
            StringBuilder hexString = new StringBuilder();
            for (byte b : result) {
                String hex = Integer.toHexString(0xff & b);
                if (hex.length() == 1) {
                    hexString.append('0');
                }
                hexString.append(hex);
            }
            return hexString.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static void printLog(String log) {
        System.out.println(log);
        logger.debug(log);
    }

    public static void logUser(String prefix, UserModel userModel) {
        try {
            logger.debug(prefix + "Nickname " + userModel.getNickname() + " vinTotal " + userModel.getVinTotal());
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void printLogError(String log) {
        logger.debug(log);
    }

    public static String printException(Exception e) {
        return Throwables.getStackTraceAsString(e);
    }

    public static String printException(String serviceName, Exception e) {
        System.out.println(serviceName + " Exception: " + printException(e));
        logger.debug(serviceName + " Exception: " + printException(e));
        return "";
    }

    public static LocalDateTime convertToLocalDateTimeViaInstant(Date dateToConvert) {
        return dateToConvert.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }

    public static LocalDateTime convertToLocalDateTimeViaMillisecond(Date dateToConvert) {
        return Instant.ofEpochMilli(dateToConvert.getTime())
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }
}
