package casio.king365.mqtt;

import casio.king365.mqtt.listener.BaseConsumer;
import casio.king365.util.KingUtil;
import com.rabbitmq.client.Channel;
import com.vinplay.vbee.common.enums.RMQQueues;
import com.vinplay.vbee.common.rmq.RMQPool;
import org.apache.log4j.Logger;

import java.lang.reflect.Constructor;

public class MqttRegisterListener {
    private static final Logger logger = Logger.getLogger("api");

    public static void RegisterListener(){
        try{
            RegisterListener(RMQQueues.NOTIFY_UPDATE_USER_MONEY.getName(), "casio.king365.mqtt.listener.UpdateUserMoneyListener");
            RegisterListener(RMQQueues.NOTIFY_USER_MESSAGES.getName(), "casio.king365.mqtt.listener.SendMsgToUserListener");
            RegisterListener(RMQQueues.NOTIFY_KICK_USER.getName(), "casio.king365.mqtt.listener.KickUserListener");
        } catch (Exception e){
            logger.debug("REGISTER RABBITMQ LISTERNER Exception1 : "+KingUtil.printException(e));
            e.printStackTrace();
        }
    }

    public static void RegisterListener(String queue, String consumeClassName){
        try{
            RMQPool pool = RMQPool.getInstance();
            Channel channel = pool.getChannel(queue);
            Class<?> clazz = Class.forName(consumeClassName);
            Constructor<?> ctor = clazz.getConstructor(Channel.class);
            Object object = ctor.newInstance(channel);
            channel.basicConsume(queue, false, (BaseConsumer)object);
        } catch (Exception e){
            logger.debug("REGISTER RABBITMQ LISTERNER Exception3 : "+ KingUtil.printException(e));
            e.printStackTrace();
        }
    }
}
