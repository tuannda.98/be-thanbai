package casio.king365.mqtt;

import com.vinplay.vbee.common.messages.BaseMessage;

public class SimpleBaseMessage extends BaseMessage {
    private static final long serialVersionUID = 1L;
    public String jsonContent;

    public SimpleBaseMessage() {
    }
}
