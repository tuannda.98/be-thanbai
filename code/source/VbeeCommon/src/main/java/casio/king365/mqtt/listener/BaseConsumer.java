package casio.king365.mqtt.listener;

import casio.king365.util.KingUtil;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.vinplay.vbee.common.messages.BaseMessage;
import org.apache.log4j.Logger;

import java.io.IOException;

public abstract class BaseConsumer extends DefaultConsumer {
    private static final Logger logger = Logger.getLogger("api");

    public BaseConsumer(Channel channel) {
        super(channel);
    }

    @Override
    public void handleDelivery(String consumerTag,
                               Envelope envelope,
                               AMQP.BasicProperties properties,
                               byte[] body)
            throws IOException
    {
        String routingKey = envelope.getRoutingKey();
        String contentType = properties.getContentType();
        long deliveryTag = envelope.getDeliveryTag();
        // (process the message components here ...)
        // SimpleBaseMessage simpleBaseMessage = (SimpleBaseMessage) BaseMessage.fromBytes(body);
        logger.debug("BaseConsumer - Nhan dc msg, routingKey: "+routingKey+", contentType: "+contentType+", classname: "
                +this.getClass().getName()+", body length: "+body.length);
        BaseMessage bm = BaseMessage.fromBytes(body);
        // logger.debug("============\n RABBITMQ LISTERNER receive content: "+LogGameMessage.toJson());
        try {
            process(bm);
        } catch (Exception e) {
            logger.debug("BaseConsumer process exception: "+ KingUtil.printException(e));
            e.printStackTrace();
        }
        getChannel().basicAck(deliveryTag, false);
    }

    public abstract void process(BaseMessage msg);
}
