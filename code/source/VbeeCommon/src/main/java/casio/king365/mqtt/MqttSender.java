package casio.king365.mqtt;

import casio.king365.util.KingUtil;
import com.vinplay.vbee.common.messages.ObjToUserNickNameMessage;
import com.vinplay.vbee.common.messages.UserNicknameMessage;
import com.vinplay.vbee.common.rmq.RMQApi;
import org.json.JSONObject;

public class MqttSender {

    public static void Send(String queueName, JSONObject obj) {
        try {
            SimpleBaseMessage msg = new SimpleBaseMessage();
            msg.jsonContent = obj.toString();
            RMQApi.publishMessage(queueName, msg, 100000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void SendMsgToNickname(String userNickname, Object obj) {
        try {
            ObjToUserNickNameMessage msg = ObjToUserNickNameMessage.of(userNickname, obj);
            RMQApi.publishNotifyUserMessage(msg);
        } catch (Exception e) {
            KingUtil.printLog("SendMsgToNickname() Exception: " + KingUtil.printException(e));
            e.printStackTrace();
        }
    }

    public static void SendKickUser(String userNickname) {
        try {
            UserNicknameMessage msg = UserNicknameMessage.of(userNickname);
            RMQApi.publishNotifyKickUser(msg);
        } catch (Exception e) {
            KingUtil.printLog("SendMsgToNickname() Exception: " + KingUtil.printException(e));
            e.printStackTrace();
        }
    }

}
