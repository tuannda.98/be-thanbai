package casio.king365.util;

import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.regex.Pattern;

public class StringUtil {
    static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXY";
    static final SecureRandom rnd = new SecureRandom();
    static final DecimalFormat myFormatter = new DecimalFormat("###,###.###");

    public static void main(String[] args) {
        String ss = "VTT|10000|BVN1061429|BVNP-9999-1061429|00:00:00 17/04/2025";
        System.out.println(hasSpecialCharacter(ss));
		/*String ss="2YXX000059076|2Y00000059076|23:59:00 29/12/2016";
		String[] rr=ss.split("\\|");
		for (String string : rr) {
			System.out.println(string);
		}*/
        System.out.println(randomString(8));
    }

    public static String randomString(int len) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }

    public static boolean hasSpecialCharacter(String s) {
        if (s == null || s.trim().isEmpty())
            return true;
        return Pattern.compile("[^A-Za-z0-9]").matcher(s).find();
    }

    public static boolean hasCharCharacter(String s) {
        if (s == null || s.trim().isEmpty())
            return true;
        return Pattern.compile("[^0-9]").matcher(s).find();
    }

    public static String getNumberFormat(double value) {
        return myFormatter.format(value);
    }

    public static String getNumberFormat2(double value) {
        return myFormatter.format(value);
    }

    public static String getNumberFormat(int value) {
        return myFormatter.format(value);
    }

    public static String getNumberFormat(long value) {
        return myFormatter.format(value);
    }

    public static String formatMoney(long value) {
        return myFormatter.format(value);
    }

    public static String formatMoney(double value) {
        return myFormatter.format(value);
    }

    public static String randomID() {
        return String.valueOf(new Date().getTime());
    }

}
