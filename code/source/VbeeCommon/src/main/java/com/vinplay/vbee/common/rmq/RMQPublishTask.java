/*
 * Decompiled with CFR 0.150.
 *
 * Could not load the following classes:
 *  com.rabbitmq.client.AMQP$BasicProperties
 *  com.rabbitmq.client.Channel
 */
package com.vinplay.vbee.common.rmq;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.vinplay.vbee.common.enums.RMQQueues;
import com.vinplay.vbee.common.messages.BaseMessage;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class RMQPublishTask
        extends RMQTask {
    private final BaseMessage message;
    private final int command;

    public RMQPublishTask(BaseMessage message, String queueName, int command) {
        super(queueName);
        this.message = message;
        this.command = command;
    }

    public RMQPublishTask(BaseMessage message, RMQQueues queue) {
        super(queue.getName());
        this.message = message;
        this.command = queue.getCommand();
    }

    @Override
    public void run(Channel channel) throws IOException {
        AMQP.BasicProperties properties = new AMQP.BasicProperties.Builder()
                .contentType("text/plain")
                .contentEncoding(StandardCharsets.UTF_8.name())
                .expiration("600000") // 10 minutes
                .deliveryMode(2)
                .priority(0)
                .messageId(String.valueOf(this.command))
                .build();

        channel.basicPublish("", this.queueName, properties, this.message.toBytes());
    }
}

