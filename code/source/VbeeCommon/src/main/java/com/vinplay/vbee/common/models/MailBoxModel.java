package com.vinplay.vbee.common.models;

import com.vinplay.vbee.common.response.MailBoxResponse;
import com.vinplay.vbee.common.utils.VinPlayUtils;
import org.bson.Document;

import java.util.Arrays;
import java.util.Date;

public class MailBoxModel {
    public enum MailBoxType {
        NORMAL("Normal"),
        GIFT_CODE("GiftCode"),
        MOBILE_CARD("MobileCard"),
        POPUP("Popup");
        final String type;

        MailBoxType(String type) {
            this.type = type;
        }

        public static MailBoxType fromType(String type) {
            return Arrays.stream(values())
                    .filter(t -> t.type.equals(type))
                    .findFirst().orElse(NORMAL);
        }
    }

    public Document toDoc() {
        Document doc = new Document();
        doc.append("mail_id", mailId);
        doc.append("type", type.type);
        doc.append("author", author);
        doc.append("nick_name", nickName);
        if (type == MailBoxType.GIFT_CODE) {
            doc.append("mail_gift_code", mailGiftCode);
        } else if (type == MailBoxType.MOBILE_CARD) {
            doc.append("provider", provider);
            doc.append("serial", serial);
            doc.append("pin", pin);
        }
        doc.append("title", title);
        doc.append("content", content);
        doc.append("create_time", createdTime);
        doc.append("status", status);
        return doc;
    }

    public static MailBoxModel fromDoc(Document document) {
        MailBoxType type = MailBoxType.fromType(document.containsKey("type") ? document.getString("type") : "");
        Builder builder = builder().setMailId(document.getString("mail_id"))
                .setNickName(document.getString("nick_name"))
                .setTitle(document.getString("title"))
                .setContent(document.getString("content"))
                .setCreatedTime(document.getString("create_time"))
                .setAuthor(document.getString("author"))
                .setStatus(document.getInteger("status"))
                .setType(type);
        if (type == MailBoxType.GIFT_CODE) {
            builder.setMailGiftCode(document.getString("mail_gift_code"));
        } else if (type == MailBoxType.MOBILE_CARD) {
            builder.setProvider(document.getString("provider"))
                    .setSerial(document.getString("serial"))
                    .setPin(document.getString("pin"));
        }
        return builder.createMailBoxModel();
    }

    public MailBoxResponse toMailBoxResponse() {
        MailBoxResponse mail = new MailBoxResponse();
        mail.sysMail = nickName.equals("*") ? 1 : 0;
        mail.title = title;
        mail.createTime = createdTime;
        mail.author = author;
        mail.content = content;
        mail.status = status;
        mail.mail_id = mailId;
        mail.type = type.type;
        mail.giftCode = mailGiftCode;
        mail.provider = provider;
        mail.serial = serial;
        mail.pin = pin;
        return mail;
    }

    private final String mailId;
    private final MailBoxType type;
    private final String author;
    private final String nickName;
    private final String content;
    private final String title;
    private final String mailGiftCode;
    private final String provider;
    private final String serial;
    private final String pin;
    private final String createdTime;
    private final Date created;
    private final int status;
    private final String footer;
    private final int read;
    private final int receive;

    private MailBoxModel(
            String mailId, MailBoxType type, String author, String nickName, String content, String title,
            String mailGiftCode, String provider, String serial, String pin, String createdTime, int status, String footer, int read, int receive) {
        this.mailId = mailId;
        this.type = type;
        this.author = author;
        this.nickName = nickName;
        this.content = content;
        this.title = title;
        this.mailGiftCode = mailGiftCode;
        this.provider = provider;
        this.serial = serial;
        this.pin = pin;
        this.createdTime = createdTime;
        this.status = status;
        this.footer = footer;
        this.read = read;
        this.receive = receive;
        this.created = new Date();
    }

    public static class Builder {
        private String mailId;
        private MailBoxModel.MailBoxType type;
        private String author;
        private String nickName;
        private String content;
        private String title;
        private String provider;
        private String mailGiftCode;
        private String serial;
        private String pin;
        private String createdTime;
        private int status;
        private String footer;
        private int read;
        private int receive;

        public Builder setMailId(String mailId) {
            this.mailId = mailId;
            return this;
        }

        public Builder setType(MailBoxModel.MailBoxType type) {
            this.type = type;
            return this;
        }

        public Builder setAuthor(String author) {
            this.author = author;
            return this;
        }

        public Builder setNickName(String nickName) {
            this.nickName = nickName;
            return this;
        }

        public Builder setContent(String content) {
            this.content = content;
            return this;
        }

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setProvider(String provider) {
            this.provider = provider;
            return this;
        }

        public Builder setMailGiftCode(String mailGiftCode) {
            this.mailGiftCode = mailGiftCode;
            return this;
        }

        public Builder setSerial(String serial) {
            this.serial = serial;
            return this;
        }

        public Builder setPin(String pin) {
            this.pin = pin;
            return this;
        }

        public Builder setCreatedTime(String createdTime) {
            this.createdTime = createdTime;
            return this;
        }

        public Builder setStatus(int status) {
            this.status = status;
            return this;
        }

        public Builder setFooter(String footer) {
            this.footer = footer;
            return this;
        }

        public Builder setRead(int read) {
            this.read = read;
            return this;
        }

        public Builder setReceive(int receive) {
            this.receive = receive;
            return this;
        }

        public MailBoxModel createMailBoxModel() {
            return new MailBoxModel(
                    mailId, type, author, nickName, content, title, mailGiftCode, provider, serial, pin,
                    createdTime, status, footer, read, receive);
        }
    }

    public static Builder builder() {
        Builder builder = new Builder();
        builder.setMailId(String.valueOf(System.currentTimeMillis()))
                .setType(MailBoxType.NORMAL)
                .setCreatedTime(VinPlayUtils.getCurrentDateTime())
                .setAuthor("Hệ thống")
                .setStatus(0);
        return builder;
    }
}
