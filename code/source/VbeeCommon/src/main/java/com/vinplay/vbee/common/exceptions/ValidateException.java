package com.vinplay.vbee.common.exceptions;

public class ValidateException extends CommonException {
    public ValidateException(String msg) {
        super(msg);
    }
}
