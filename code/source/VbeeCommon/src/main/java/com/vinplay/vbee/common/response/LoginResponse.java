package com.vinplay.vbee.common.response;

public class LoginResponse
        extends BaseResponseModel {
    private String sessionKey;
    private String accessToken;
    private String username;
    private String avatar;

    public LoginResponse(boolean success, String errorCode) {
        super(success, errorCode);
    }

    public LoginResponse(boolean success, String errorCode, String sessionKey, String accessToken, String un, String avatar) {
        super(success, errorCode);
        this.sessionKey = sessionKey;
        this.accessToken = accessToken;
        this.username = un;
        this.avatar = avatar;
    }

    public static LoginResponse success(String sessionKey, String accessToken, String un, String avatar) {
        return new LoginResponse(true, "0", sessionKey, accessToken, un, avatar);
    }

    public static LoginResponse fail(String errorCode) {
        return new LoginResponse(false, errorCode, null, null, null, null);
    }

    public static LoginResponse fail() {
        return new LoginResponse(false, "1001", null, null, null, null);
    }

    public String getSessionKey() {
        return this.sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getAccessToken() {
        return this.accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUsername() { return username; }

    public void setUsername(String username) { this.username = username; }

    public String getAvatar() { return avatar; }

    public void setAvatar(String avatar) { this.avatar = avatar; }
}

