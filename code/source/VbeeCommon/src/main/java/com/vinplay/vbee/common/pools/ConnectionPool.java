package com.vinplay.vbee.common.pools;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

public class ConnectionPool {
    public static final String USER_POOL = "mysqlpoolname";
    public static final String MINIGAME_POOL = "mysqlpool_minigame";
    public static final String ADMIN_POOL = "mysqlpool_admin";
    public static final String GAMEBAI_POOL = "mysqlpool_gamebai";
    public static final String XC_GIFTCODE_POOL = "mysql_xc_giftcode";
    private static String CONFIG_FILE = "config/db_pool.properties";
    private static final int DEFAULT_TIMEOUT = 5000;
    //    private ConnectionPoolManager managerVinPlay = null;
    private static ConnectionPool _instance;

    public static ConnectionPool getInstance() {
        if (null == _instance) {
            _instance = new ConnectionPool();
        }
        return _instance;
    }

    public static void start(String configFile) {
        CONFIG_FILE = configFile;
    }

    private ConnectionPool() {
        try {
            this.init(CONFIG_FILE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void init(String configFile) throws IOException {
//        Properties prop = new Properties();
//        try (FileInputStream input = new FileInputStream(configFile)) {
//            prop.load(input);
//        }
//        ConnectionPoolManager.createInstance(prop);
//        this.managerVinPlay = ConnectionPoolManager.getInstance();
    }

    private static Map<String, DataSource> mDataSource = new ConcurrentHashMap<>();

    public Connection getConnection(String name) throws SQLException {
        if (mDataSource.containsKey(name))
            return mDataSource.get(name).getConnection();

        Properties prop = new Properties();
        try (FileInputStream input = new FileInputStream(CONFIG_FILE)) {
            prop.load(input);

            HikariConfig config = new HikariConfig();

            config.setDriverClassName(prop.getProperty("db." + name + ".driver_class"));
            config.setJdbcUrl(prop.getProperty("db." + name + ".url"));
            config.setUsername(prop.getProperty("db." + name + ".user"));
            config.setPassword(prop.getProperty("db." + name + ".password"));
            //The initial number of connections that are created when the pool is started.
            config.setMinimumIdle(Integer.parseInt(prop.getProperty("db." + name + ".inital_pool_size")));
            //The maximum number of active connections that can be allocated from this pool at the same time
            config.setMaximumPoolSize(Integer.parseInt(prop.getProperty("db." + name + ".max_pool_size")));

            mDataSource.put(name, new HikariDataSource(config));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return mDataSource.get(name).getConnection();
    }


//        Connection conn = null;
//        try {
//            conn = this.managerVinPlay.getConnection(poolName, 5000L);
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return conn;
//    }

    public static void releaseConnection(Connection conn) {
        try {
            if (conn != null && !conn.isClosed()) {
                conn.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

