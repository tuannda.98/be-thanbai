/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.vbee.common.messages.pay;

import com.vinplay.vbee.common.messages.BaseMessage;

public class ExchangeMessage
extends BaseMessage {
    private static final long serialVersionUID = 1L;
    public final String nickname;
    public final String merchantId;
    public final String merchantTransId;
    public final String transNo;
    public final long money;
    public final String moneyType;
    public final String type;
    public final long exchangeMoney;
    public final long fee;
    public final int code;
    public final String ip;

    public ExchangeMessage(String nickname, String merchantId, String merchantTransId, String transNo, long money, String moneyType, String type, long exchangeMoney, long fee, int code, String ip) {
        this.nickname = nickname;
        this.merchantId = merchantId;
        this.merchantTransId = merchantTransId;
        this.transNo = transNo;
        this.money = money;
        this.moneyType = moneyType;
        this.type = type;
        this.exchangeMoney = exchangeMoney;
        this.fee = fee;
        this.code = code;
        this.ip = ip;
    }
}

