package com.vinplay.vbee.common.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.pools.ConnectionPool;
import com.vinplay.vbee.common.statics.Consts;
import com.vinplay.vbee.common.statics.TimeBasedOneTimePasswordUtil;
import org.apache.log4j.Logger;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class VinPlayUtils {
    private static final Set<String> generatedCode = new HashSet<>();
    private static long oldTransId = 0L;
    private static final Logger logger = Logger.getLogger("api");
    public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat(Consts.DEFAULT_FORMAT_DATE);

    public static String getMD5Hash(String value) throws NoSuchAlgorithmException {
        StringBuilder sbMd5Hash = new StringBuilder();
        MessageDigest m = MessageDigest.getInstance("MD5");
        m.update(value.getBytes(StandardCharsets.UTF_8));
        byte[] digest = m.digest();
        for (byte element : digest) {
            sbMd5Hash.append(Character.forDigit(element >> 4 & 0xF, 16));
            sbMd5Hash.append(Character.forDigit(element & 0xF, 16));
        }
        return sbMd5Hash.toString();
    }

    public static String hmacDigest(String msg, String keyString, String algo) throws NoSuchAlgorithmException, InvalidKeyException {
        SecretKeySpec key = new SecretKeySpec(keyString.getBytes(StandardCharsets.UTF_8), algo);
        Mac mac = Mac.getInstance(algo);
        mac.init(key);
        byte[] bytes = mac.doFinal(msg.getBytes(StandardCharsets.US_ASCII));
        StringBuilder hash = new StringBuilder();
        for (byte aByte : bytes) {
            String hex = Integer.toHexString(0xFF & aByte);
            if (hex.length() == 1) {
                hash.append('0');
            }
            hash.append(hex);
        }
        return hash.toString();
    }

    public static Date subtractDay(Date date, int pre) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, -pre);
        return cal.getTime();
    }

    public static int compareDate(Date date1, Date date2) throws ParseException {
        long time2;
        long time1 = SIMPLE_DATE_FORMAT.parse(VinPlayUtils.parseDateToString(date1)).getTime();
        if (time1 == (time2 = SIMPLE_DATE_FORMAT.parse(VinPlayUtils.parseDateToString(date2)).getTime())) {
            return 0;
        }
        if (time1 > time2) {
            return 1;
        }
        return -1;
    }

    public static String parseDateToString(Date input) {
        return SIMPLE_DATE_FORMAT.format(input);
    }

    public static String parseDateTimeToString(Date input) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return df.format(input);
    }

    public static String getCurrentDate() {
        return SIMPLE_DATE_FORMAT.format(new Date());
    }

    public static String getYesterday() {
        Calendar aCalendar = Calendar.getInstance();
        aCalendar.add(Calendar.DATE, -1);
        return SIMPLE_DATE_FORMAT.format(aCalendar.getTime());
    }

    public static Date getTomorrow() {
        Calendar aCalendar = Calendar.getInstance();
        aCalendar.add(Calendar.DATE, 1);
        return aCalendar.getTime();
    }

    public static Date getCurrentDates() throws ParseException {
        return SIMPLE_DATE_FORMAT.parse(SIMPLE_DATE_FORMAT.format(new Date()));
    }

    public static String getCurrentDateMarketing() {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(new Date());
    }

    public static String getCurrentDateTime() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(new Date());
    }

    public static Date getDateTime(String datetime) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.parse(datetime);
    }

    public static Calendar getCalendar(String datetime) throws ParseException {
        if (datetime != null && !datetime.isEmpty()) {
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            calendar.setTime(format.parse(datetime));
            return calendar;
        }
        return null;
    }

    public static String getDateTimeStr(Date datetime) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(datetime);
    }

    public static Date getDateTimeFromDate(String date) throws ParseException {
        return SIMPLE_DATE_FORMAT.parse(date);
    }

    public static String getDateFromDateTime(String datetime) throws ParseException {
        return SIMPLE_DATE_FORMAT.format(VinPlayUtils.getDateTime(datetime));
    }

    public static String formatDateTime(String strDateTime) {
        return strDateTime.substring(8, 10) + "/" + strDateTime.substring(5, 7) + "/" + strDateTime.substring(0, 4) + strDateTime.substring(10);
    }

    public static String formatDate(String strDateTime) {
        return strDateTime.substring(0, 4) + "-" + strDateTime.substring(5, 7) + "-" + strDateTime.substring(8, 10) + " " + strDateTime.substring(11, 13) + ":" + strDateTime.substring(14, 16) + ":" + strDateTime.substring(17, 19);
    }

    public static String genSessionId(int userId, String gameName) {
        return userId + gameName + System.currentTimeMillis();
    }

    public static String genMessageId() {
        return String.valueOf(System.currentTimeMillis());
    }

    public static String genTransactionId(int userId) {
        return String.valueOf(userId) + System.currentTimeMillis();
    }

    public static synchronized long generateTransId() {
        long transId = System.currentTimeMillis();
        while (transId == oldTransId) {
            transId = System.currentTimeMillis();
        }
        oldTransId = transId;
        return oldTransId;
    }

    public static String genSessionKey(UserCacheModel userInfo) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return VinPlayUtils.encodeBase64(mapper.writeValueAsString(userInfo));
    }

    public static String genAccessToken(int userId) {
        try {
            return VinPlayUtils.getMD5Hash(userId + "@VinPlay#6102$817" + System.currentTimeMillis());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return UUID.randomUUID().toString();
    }

    public static boolean sessionTimeout(long recentTime) {
        return new Date().getTime() - recentTime > 10800000L;
    }

    public static boolean cashoutBlockTimeout(Date recentTime, int hoursBlock) {
        return new Date().getTime() - recentTime.getTime() > ((long) hoursBlock * 60 * 60 * 1000);
    }

    public static boolean socialTimeout(Date recentTime) {
        return new Date().getTime() - recentTime.getTime() > 86400000L;
    }

    public static boolean emailTimeout(Date emailTime) {
        return new Date().getTime() - emailTime.getTime() > 86400000L;
    }

    public static boolean updateMoneyTimeout(long activeTime, int timeOut) {
        return new Date().getTime() - activeTime > ((long) timeOut * 60 * 1000);
    }

    public static boolean isAlertTimeout(Date alertTime, int timeOut) {
        return alertTime == null || new Date().getTime() - alertTime.getTime() > ((long) timeOut * 60 * 1000);
    }

    public static String encodeBase64(String message) {
        return DatatypeConverter.printBase64Binary(message.getBytes(StandardCharsets.UTF_8));
    }

    public static String decodeBase64(String messageBase64) {
        return new String(DatatypeConverter.parseBase64Binary(messageBase64), StandardCharsets.UTF_8);
    }

    public static String genOtpApp(String nickname, String mobile) throws Exception {
        String secret = VinPlayUtils.getUserSecretKey(nickname);
        return TimeBasedOneTimePasswordUtil.generateCurrentNumberString(secret);
    }

    public static boolean setUserSecretKey(String nickname, String secret) throws Exception {
        String sql = "INSERT INTO vinplay.user_appotp (nick_name,secret) VALUES(?,?)";
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement("INSERT INTO vinplay.user_appotp (nick_name,secret) VALUES(?,?)")) {
            stm.setString(1, nickname);
            stm.setString(2, secret);
            int rs = stm.executeUpdate();
        }
        return true;
    }

    public static String getUserSecretKey(String nickname) {
        String secret = "";
        String sql = "SELECT * FROM user_appotp WHERE nick_name=?";
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement("SELECT * FROM user_appotp WHERE nick_name=?")) {
            stm.setString(1, nickname);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    secret = rs.getString("secret");
                }
            }
        } catch (SQLException sQLException) {
            // empty catch block
        }
        return secret;
    }

    public static String genOtpSMS(String mobile, String service) {
        return StringUtils.randomStringNumber(5);
    }

    public static boolean checkOtpTimeout(Date otpTime) {
        return new Date().getTime() - otpTime.getTime() > 300000L;
    }

    public static boolean checkSecurityTimeout(Date securityTime) {
        return new Date().getTime() - securityTime.getTime() > 86400000L;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static String genGiftCode(int leng) {
        Set<String> j;
        String strRandom = "1234567890QWERTYUIOPASDFGHJKLZXCVBNM";
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int j2 = 0; j2 < leng; ++j2) {
            sb.append("1234567890QWERTYUIOPASDFGHJKLZXCVBNM".charAt(random.nextInt("1234567890QWERTYUIOPASDFGHJKLZXCVBNM".length())));
        }
        synchronized (generatedCode) {
            if (generatedCode.contains(String.valueOf(sb))) {
                return VinPlayUtils.genGiftCode(leng);
            }
            generatedCode.add(String.valueOf(sb));
            return String.valueOf(sb);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void loadGiftcode(List<String> allGiftcode) {
        for (String gift : allGiftcode) {
            synchronized (generatedCode) {
                if (generatedCode.contains(gift)) {
                    continue;
                }
                generatedCode.add(gift);
            }
        }
    }

    public static String mapProviderByNumber(String number) {
        if (number == null) {
            return "null";
        }
        if (number.matches("^(096|097|098|0162|0163|0164|0165|0166|0167|0168|0169|086|082)[\\d]{7}$")) {
            return "vtt";
        }
        if (number.matches("^(091|094|0123|0124|0125|0127|0129|088)[\\d]{7}$")) {
            return "vnp";
        }
        if (number.matches("^(090|093|0120|0121|0122|0126|0128|089)[\\d]{7}$")) {
            return "vms";
        }
        if (number.matches("^(092|0188|0186)[\\d]{7}$")) {
            return "vnm";
        }
        if (number.matches("^(099|0199)[\\d]{7}$")) {
            return "gtel";
        }
        return "other";
    }

    public static String getServiceName(String actionName) {
        switch (actionName) {
            case "Sam": {
                return "Ch\u01a1i game S\u00e2m";
            }
            case "BaCay": {
                return "Ch\u01a1i game Ba C\u00e2y";
            }
            case "Binh": {
                return "Ch\u01a1i game M\u1eadu Binh";
            }
            case "Tlmn": {
                return "Ch\u01a1i game TLMN";
            }
            case "TaLa": {
                return "Ch\u01a1i game T\u00e1 L\u1ea3";
            }
            case "Lieng": {
                return "Ch\u01a1i game Li\u00eang";
            }
            case "XiTo": {
                return "Ch\u01a1i game X\u00ec T\u1ed1";
            }
            case "BaiCao": {
                return "Ch\u01a1i game B\u00e0i C\u00e0o";
            }
            case "Poker": {
                return "Ch\u01a1i game Poker";
            }
            case "XocDia": {
                return "Ch\u01a1i game X\u00f3c \u0110\u00eda";
            }
            case "XiDzach": {
                return "Ch\u01a1i game X\u00ec D\u00e1ch";
            }
            case "Caro": {
                return "Ch\u01a1i game C\u1edd Caro";
            }
            case "CoTuong": {
                return "Ch\u01a1i game C\u1edd T\u01b0\u1edbng";
            }
            case "CoVua": {
                return "Ch\u01a1i game C\u1edd Vua";
            }
            case "PokerTour": {
                return "Ch\u01a1i Poker Tour";
            }
            case "CoUp": {
                return "Ch\u01a1i game C\u1edd \u00dap";
            }
        }
        return "Ch\u01a1i game " + actionName;
    }

    public static String genMailId(int len) {
        String strRandom = String.valueOf(System.currentTimeMillis());
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < len; ++i) {
            sb.append(strRandom.charAt(random.nextInt(strRandom.length())));
        }
        return sb.toString();
    }

    public static boolean isMobile(String platform) {
        return platform != null && !platform.equals("web");
    }
}
