package com.vinplay.vbee.common.hazelcast;

import java.io.FileInputStream;
import java.util.Properties;

public class HazelcastLoader {
    public static void start() {
        String configFile = "config/hazelcast.properties";
        Properties prop = new Properties();
        try (FileInputStream in = new FileInputStream(configFile)) {
            prop.load(in);
            HazelcastClientFactory.ADDRESS = prop.getProperty("address");
            HazelcastClientFactory.GROUP_NAME = prop.getProperty("group_name");
            HazelcastClientFactory.GROUP_PASS = prop.getProperty("group_pass");
            HazelcastClientFactory.initDefault();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

