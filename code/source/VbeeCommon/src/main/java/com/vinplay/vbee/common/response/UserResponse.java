/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.vbee.common.response;

import com.vinplay.vbee.common.models.UserModel;

public class UserResponse
        extends BaseResponseModel {
    private UserModel user;

    private UserResponse(boolean success, String errorCode, UserModel user) {
        super(success, errorCode);
        this.user = user;
    }

    public static UserResponse success(UserModel user) {
        return new UserResponse(true, "0", user);
    }

    public static UserResponse failed() {
        return new UserResponse(false, "1001", null);
    }

    public static UserResponse failed(String errorCode) {
        return new UserResponse(false, errorCode, null);
    }

    public UserModel getUser() {
        return this.user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }
}

