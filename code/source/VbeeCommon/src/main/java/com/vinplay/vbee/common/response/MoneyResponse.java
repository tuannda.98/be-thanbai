/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.vbee.common.response;

import java.util.Arrays;

import static com.vinplay.vbee.common.response.MoneyResponse.MoneyResponseCode.fromErrorCode;

public class MoneyResponse
        extends BaseResponseModel {
    public MoneyResponseCode getResponseCode() {
        return responseCode;
    }

    public enum MoneyResponseCode {
        UNKNOWN("1001", "Unknown", false),
        SUCCESS("0", "Success", true),
        NOT_ENOUGH_MONEY("1002", "Not enough money", false),
        INFRASTRUCTURE_HAZELCAST_ERROR("1030", "INFRASTRUCTURE_HAZELCAST_ERROR", false),
        INFRASTRUCTURE_RMQ_ERROR("1031", "INFRASTRUCTURE_RMQ_ERROR", false),
        INFRASTRUCTURE_MYSQL_ERROR("1032", "INFRASTRUCTURE_MYSQL_ERROR", false),
        HAZELCAST_DO_NOT_CONTAINS_USER("1011", "HAZELCAST_DO_NOT_CONTAINS_USER", false),
        HAZELCAST_DO_NOT_CONTAINS_FREEZE_KEY("1003", "HAZELCAST_DO_NOT_CONTAINS_FREEZE_KEY", false),
        ;
        public final String errorCode;
        public final String description;
        private boolean success;

        MoneyResponseCode(String errorCode, String description, boolean success) {
            this.errorCode = errorCode;
            this.description = description;
            this.success = success;
        }

        public static MoneyResponseCode fromErrorCode(String errorCode) {
            return Arrays.stream(values()).filter(moneyResponseCode -> moneyResponseCode.errorCode.equalsIgnoreCase(errorCode)).findFirst().orElse(UNKNOWN);
        }
    }

    private MoneyResponseCode responseCode;
    private long currentMoney;
    private long currentMoneyXu;
    private long subtractMoney;
    private long safeMoney;
    private long freezeMoney;
    private long moneyUse;

    public MoneyResponse(boolean success, String errorCode) {
        super(success, errorCode);
        this.responseCode = fromErrorCode(errorCode);
    }

    @Override
    public void setErrorCode(String errorCode) {
        this.responseCode = fromErrorCode(errorCode);
        super.setErrorCode(errorCode);
    }

    public long getMoneyUse() {
        return this.moneyUse;
    }

    public void setMoneyUse(long moneyUse) {
        this.moneyUse = moneyUse;
    }

    public long getCurrentMoney() {
        return this.currentMoney;
    }

    public void setCurrentMoney(long currentMoney) {
        this.currentMoney = currentMoney;
    }

    public long getSubtractMoney() {
        return this.subtractMoney;
    }

    public void setSubtractMoney(long subtractMoney) {
        this.subtractMoney = subtractMoney;
    }

    public long getSafeMoney() {
        return this.safeMoney;
    }

    public void setSafeMoney(long safeMoney) {
        this.safeMoney = safeMoney;
    }

    public long getFreezeMoney() {
        return this.freezeMoney;
    }

    public void setFreezeMoney(long freezeMoney) {
        this.freezeMoney = freezeMoney;
    }

    public long getCurrentMoneyXu() {
        return this.currentMoneyXu;
    }

    public void setCurrentMoneyXu(long currentMoneyXu) {
        this.currentMoneyXu = currentMoneyXu;
    }

    public long getMoneyInGame() {
        return this.moneyUse + this.freezeMoney;
    }
}

