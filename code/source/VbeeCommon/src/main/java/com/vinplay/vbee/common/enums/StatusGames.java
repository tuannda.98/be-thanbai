/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.vbee.common.enums;

import java.util.Arrays;
import java.util.stream.Stream;

public enum StatusGames {
    RUN(0),
    SANDBOX(1),
    MAINTAIN(2),
    UNKNOWN(-1);

    private int id;

    StatusGames(final int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public static StatusGames getById(final int id) {
        return Arrays.stream(StatusGames.values()).filter(statusGames -> statusGames.id == id)
            .findFirst()
            .orElse(UNKNOWN);
    }

    public boolean isSandbox() {
        return this == SANDBOX;
    }

    public boolean isMaintain() {
        return this == MAINTAIN;
    }
}

