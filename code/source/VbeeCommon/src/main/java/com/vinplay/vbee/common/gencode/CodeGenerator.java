/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.vbee.common.gencode;

import java.util.HashSet;
import java.util.Set;

public class CodeGenerator {
    private static final Set<String> generatedCode = new HashSet<>();

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static Set<String> genCode(int quantity, int length, String prefix, String postfix) {
        synchronized (generatedCode) {
            HashSet<String> codes = new HashSet<>();
            CodeConfig config = CodeConfig.length(length).withPrefix(prefix).withPostfix(postfix);
            for (int i = 0; i < quantity; ++i) {
                String code;
                while ((code = VoucherCodes.generate(config)) == null || generatedCode.contains(code) || codes.contains(code)) {
                }
                codes.add(code);
            }
            generatedCode.addAll(codes);
            return codes;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void loadGeneratedcode(Set<String> allCode) {
        synchronized (generatedCode) {
            for (String code : allCode) {
                if (code == null || generatedCode.contains(code)) continue;
                generatedCode.add(code);
            }
        }
    }
}
