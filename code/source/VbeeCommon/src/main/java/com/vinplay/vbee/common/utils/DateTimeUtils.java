/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.vbee.common.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DateTimeUtils {
    public static final DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    public static final DateFormat df2 = new SimpleDateFormat("HH:mm:ss dd-MM-yyyy");

    public static Date getSundayEveryWeek() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.DAY_OF_WEEK, 1);
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
        c.set(Calendar.MILLISECOND, 999);
        c.add(Calendar.DATE, 7);
        return c.getTime();
    }

    public static Date getStartTimeThisMonth() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.DATE, 1);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTime();
    }

    public static Date getEndTimeEveryMonth() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.DATE, c.getActualMaximum(Calendar.DATE));
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
        c.set(Calendar.MILLISECOND, 999);
        return c.getTime();
    }

    public static String getStartTimeToDay() {
        String currentDate = df.format(new Date());
        return currentDate + " 00:00:00";
    }

    public static String getEndTimeToDay() {
        String currentDate = df.format(new Date());
        return currentDate + " 23:59:59";
    }

    public static long getStartTimeToDayAsLong() {
        return DateTimeUtils.getTimeToDayAsLong("00:00:00");
    }

    public static long getEndTimeToDayAsLong() {
        return DateTimeUtils.getTimeToDayAsLong("23:59:59");
    }

    public static long getTimeToDayAsLong(String t) {
        SimpleDateFormat df = new SimpleDateFormat(t + " dd-MM-yyyy");
        String currentDate = df.format(new Date());
        try {
            return df2.parse(currentDate).getTime();
        } catch (ParseException parseException) {
            return 0L;
        }
    }

    public static String getToDayAsDate() {
        return df.format(new Date());
    }

    public static String getCurrentTime() {
        return df2.format(new Date());
    }

    public static String getCurrentTime(String dateFormat) {
        SimpleDateFormat df = new SimpleDateFormat(dateFormat);
        return df.format(new Date());
    }

    public static String getFormatTime(String dateFormat, Date date) {
        SimpleDateFormat df = new SimpleDateFormat(dateFormat);
        return df.format(date);
    }

    public static long calculateTimeToEndDay() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTimeInMillis() - System.currentTimeMillis();
    }

    public static String getStartTimeThisWeek() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.DAY_OF_WEEK, 1);
        String currentDate = df.format(calendar.getTime());
        return currentDate + " 00:00:00";
    }

    public static String getEndTimeThisWeek() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.DAY_OF_WEEK, 7);
        String currentDate = df.format(calendar.getTime());
        return currentDate + " 23:59:59";
    }

    public static List<String> getLastTime(Calendar aCalendar) {
        ArrayList<String> res = new ArrayList<>();
        aCalendar.set(Calendar.DATE, 1);
        Date firstDateOfPreviousMonth = aCalendar.getTime();
        aCalendar.set(Calendar.DATE, aCalendar.getActualMaximum(Calendar.DATE));
        Date lastDateOfPreviousMonth = aCalendar.getTime();
        SimpleDateFormat startFormat = new SimpleDateFormat("yyyy-MM-dd 00;00:00");
        SimpleDateFormat endFormat = new SimpleDateFormat("yyyy-MM-dd 23;59:59");
        String startTime = startFormat.format(firstDateOfPreviousMonth);
        String endTime = endFormat.format(lastDateOfPreviousMonth);
        String month = (aCalendar.get(Calendar.MONTH) + 1) + "/" + aCalendar.get(Calendar.YEAR);
        res.add(startTime);
        res.add(endTime);
        res.add(month);
        return res;
    }
}

