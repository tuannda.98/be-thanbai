/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.vbee.common.enums;

public enum FreezeInGame {
    MORE(1),
    SET(2),
    ALL_MIN(3),
    ALL(4);

    private int id;

    FreezeInGame(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

