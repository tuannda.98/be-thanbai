/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.vbee.common.models;

public class CardTranferModel {
    public String nickname;
    public String serial;
    public String pin;
    public String provider;
    public long amount;
    public long addMoney;
    public String comment;
    public String time;
    public String timeRequest;
    public String transId;
    public int status;

    @Override
    public String toString() {
        return "CardTranferModel{" +
                "nickname='" + nickname + '\'' +
                ", serial='" + serial + '\'' +
                ", pin='" + pin + '\'' +
                ", provider='" + provider + '\'' +
                ", amount=" + amount +
                ", addMoney=" + addMoney +
                ", comment='" + comment + '\'' +
                ", time='" + time + '\'' +
                ", transId='" + transId + '\'' +
                ", status=" + status +
                '}';
    }
}

