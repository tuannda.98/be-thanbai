package com.vinplay.vbee.common.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;

@Getter
@RequiredArgsConstructor
public enum SocialEnum {
    FACEBOOK("fb"), TELEGRAM("tele"), UNKNOWN("");

    private final String type;

    public static SocialEnum parseByType(String type) {
        return Arrays.stream(SocialEnum.values()).filter(socialEnum -> socialEnum.getType().equalsIgnoreCase(type)).findFirst().orElse(UNKNOWN);
    }

    public boolean isUnknown() {
        return this == UNKNOWN;
    }

    public boolean isFacebook() {
        return this == FACEBOOK;
    }
}
