/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.vbee.common.messages.minigame;

import com.vinplay.vbee.common.messages.BaseMessage;

public class LogCaoThapWinMessage
extends BaseMessage {
    private static final long serialVersionUID = 1L;
    public long transId;
    public String nickname;
    public long betValue;
    public int result;
    public long prize;
    public String cards;
    public int moneyType;

    @Override
    public String toString() {
        return "LogCaoThapWinMessage{" +
                "transId=" + transId +
                ", nickname='" + nickname + '\'' +
                ", betValue=" + betValue +
                ", result=" + result +
                ", prize=" + prize +
                ", cards='" + cards + '\'' +
                ", moneyType=" + moneyType +
                '}';
    }
}

