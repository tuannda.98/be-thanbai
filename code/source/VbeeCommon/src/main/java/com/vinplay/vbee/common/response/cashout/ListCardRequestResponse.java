/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.vbee.common.response.cashout;

import com.vinplay.vbee.common.messages.dvt.CashoutByCardMessage;
import com.vinplay.vbee.common.response.BaseResponseModel;
import java.util.ArrayList;
import java.util.List;

public class ListCardRequestResponse
extends BaseResponseModel {
    private long totalPages;
    private List<CashoutByCardMessage> transactions = new ArrayList<>();

    public ListCardRequestResponse(boolean success, String errorCode) {
        super(success, errorCode);
    }

    public List<CashoutByCardMessage> getTransactions() {
        return this.transactions;
    }

    public void setTransactions(List<CashoutByCardMessage> transactions) {
        this.transactions = transactions;
    }

    public long getTotalPages() {
        return this.totalPages;
    }

    public void setTotalPages(long totalPages) {
        this.totalPages = totalPages;
    }
}

