/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.vbee.common.response.cashout;

public class MomoCashout {
    public String reference_id;
    public String nick_name;
    public String phone;
    public String name;
    public int amount;
    public int status;
    public String message;
    public int code;
    public String partner;
    public String description;
    public String time_log;
    public String update_time;
}

