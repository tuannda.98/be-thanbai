/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.vbee.common.exceptions;

public class KeyNotFoundException
extends Exception {
    private static final long serialVersionUID = 1L;

    public KeyNotFoundException() {
        super("Key not found");
    }
}

