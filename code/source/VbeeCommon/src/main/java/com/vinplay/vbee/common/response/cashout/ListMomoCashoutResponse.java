/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.vbee.common.response.cashout;

import com.vinplay.vbee.common.response.BaseResponseModel;
import com.vinplay.vbee.common.response.cashout.MomoCashout;
import java.util.ArrayList;
import java.util.List;

public class ListMomoCashoutResponse
extends BaseResponseModel {
    private long totalPages;
    private List<MomoCashout> transactions = new ArrayList<>();

    public ListMomoCashoutResponse(boolean success, String errorCode) {
        super(success, errorCode);
    }

    public List<MomoCashout> getTransactions() {
        return this.transactions;
    }

    public void setTransactions(List<MomoCashout> transactions) {
        this.transactions = transactions;
    }

    public long getTotalPages() {
        return this.totalPages;
    }

    public void setTotalPages(long totalPages) {
        this.totalPages = totalPages;
    }
}

