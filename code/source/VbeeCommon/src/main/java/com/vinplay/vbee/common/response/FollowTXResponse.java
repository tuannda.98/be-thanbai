/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.fasterxml.jackson.core.JsonProcessingException
 *  com.fasterxml.jackson.databind.ObjectMapper
 */
package com.vinplay.vbee.common.response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vinplay.vbee.common.response.BaseResponseModel;
import com.vinplay.vbee.common.response.TxData;

public class FollowTXResponse
extends BaseResponseModel {
    private TxData data;

    public TxData getData() {
        return this.data;
    }

    public void setData(TxData data) {
        this.data = data;
    }

    public FollowTXResponse(boolean success, String errorCode) {
        super(success, errorCode);
    }

    @Override
    public String toJson() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(this);
        }
        catch (JsonProcessingException mapper) {
            return "{\"success\":false,\"errorCode\":\"1001\"}";
        }
    }
}

