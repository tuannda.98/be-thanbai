/*
 * Decompiled with CFR 0.150.
 *
 * Could not load the following classes:
 *  com.rabbitmq.client.Channel
 */
package com.vinplay.vbee.common.rmq;

import com.rabbitmq.client.Channel;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public abstract class RMQTask extends Thread {
    protected final String queueName;

    public RMQTask(String queueName) {
        this.queueName = queueName;
    }

    public abstract void run(Channel var1) throws IOException;

    public void run() {
        try {
            RMQPool pool = RMQPool.getInstance();
            Channel channel = pool.getChannel(this.queueName);
            this.run(channel);
            if (channel != null) {
                pool.releaseChannel(channel);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }
}

