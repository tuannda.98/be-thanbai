/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.vbee.common.response;

public class LogUserMoneyResponse {
    public String userName;
    public String nickName;
    public String serviceName;
    public String description;
    public long currentMoney;
    public long moneyExchange;
    public String transactionTime;
    public String actionName;
    public long fee;
    public long createdTime;
    public String sender_nick_name;
    public String receiver_nick_name;
    public String action;

    @Override
    public String toString() {
        return "LogUserMoneyResponse{" +
                "userName='" + userName + '\'' +
                ", nickName='" + nickName + '\'' +
                ", serviceName='" + serviceName + '\'' +
                ", description='" + description + '\'' +
                ", currentMoney=" + currentMoney +
                ", moneyExchange=" + moneyExchange +
                ", transactionTime='" + transactionTime + '\'' +
                ", actionName='" + actionName + '\'' +
                ", fee=" + fee +
                ", createdTime=" + createdTime +
                ", sender_nick_name='" + sender_nick_name + '\'' +
                ", receiver_nick_name='" + receiver_nick_name + '\'' +
                ", action='" + action + '\'' +
                '}';
    }
}

