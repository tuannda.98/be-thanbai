/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.vbee.common.response;

public class SendMailResponse
        extends BaseResponseModel {
    private String nickName;

    private SendMailResponse(boolean success, String errorCode, String nickname) {
        super(success, errorCode);
        this.nickName = nickname;
    }

    public static SendMailResponse success() {
        return new SendMailResponse(true, "0", "");
    }

    public static SendMailResponse fail() {
        return new SendMailResponse(false, "1001", "");
    }

    public String getNickName() {
        return this.nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
}

