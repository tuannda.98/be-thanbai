/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.vbee.common.response;

import com.vinplay.vbee.common.models.MomoTranferModel;
import com.vinplay.vbee.common.response.BaseResponseModel;
import java.util.ArrayList;
import java.util.List;

public class ListMomoTransferResponse
extends BaseResponseModel {
    private long totalPages;
    private List<MomoTranferModel> transactions = new ArrayList<>();

    public ListMomoTransferResponse(boolean success, String errorCode) {
        super(success, errorCode);
    }

    public List<MomoTranferModel> getTransactions() {
        return this.transactions;
    }

    public void setTransactions(List<MomoTranferModel> transactions) {
        this.transactions = transactions;
    }

    public long getTotalPages() {
        return this.totalPages;
    }

    public void setTotalPages(long totalPages) {
        this.totalPages = totalPages;
    }
}

