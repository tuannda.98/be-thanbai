/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.vbee.common.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CommonUtils {
    public static final DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

    public static String convertTimestampToString(Date date) {
        return dateFormat.format(date);
    }

    public static String arrayLongToString(long[] arr) {
        StringBuilder builder = new StringBuilder();
        for (long l : arr) {
            builder.append(l);
            builder.append(",");
        }
        if (builder.length() > 0) {
            builder.deleteCharAt(builder.length() - 1);
        }
        return builder.toString();
    }

    public static String arrayByteToString(byte[] arr) {
        StringBuilder builder = new StringBuilder();
        for (byte b : arr) {
            builder.append(b);
            builder.append(",");
        }
        if (builder.length() > 0) {
            builder.deleteCharAt(builder.length() - 1);
        }
        return builder.toString();
    }

    public static long[] stringToLongArr(String input) {
        String[] arr = input.split(",");
        long[] result = new long[arr.length];
        for (int i = 0; i < arr.length; ++i) {
            result[i] = Long.parseLong(arr[i]);
        }
        return result;
    }

    public static String getRatioTime(long inputTime) {
        if (inputTime < 10L) {
            return "XX-FAST";
        }
        if (inputTime < 100L) {
            return "X-FAST";
        }
        if (inputTime < 300L) {
            return "FAST";
        }
        if (inputTime < 1000L) {
            return "SLOW";
        }
        if (inputTime < 2000L) {
            return "X-SLOW";
        }
        if (inputTime < 3000L) {
            return "XX-SLOW";
        }
        return "XXX-SLOW";
    }
}

