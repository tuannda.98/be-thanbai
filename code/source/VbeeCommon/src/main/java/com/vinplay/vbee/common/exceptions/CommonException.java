package com.vinplay.vbee.common.exceptions;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class CommonException extends Exception {

    private static final long serialVersionUID = -3698282368025675080L;

    protected int status = 400;

    protected String code;

    public CommonException(String message){
        super(message);
    }
}
