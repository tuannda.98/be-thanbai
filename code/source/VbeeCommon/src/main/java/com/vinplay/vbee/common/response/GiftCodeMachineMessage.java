/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.vbee.common.response;

public class GiftCodeMachineMessage {
    public final String GiftCode;
    public final String Price;
    public final int Quantity;
    public final String Source;
    public final int GiftCodeUse;
    public String CreateTime;

    public GiftCodeMachineMessage(String giftCode, String price, int quantity, String source, int giftCodeUse) {
        this.GiftCode = giftCode;
        this.Price = price;
        this.Quantity = quantity;
        this.Source = source;
        this.GiftCodeUse = giftCodeUse;
    }
}

