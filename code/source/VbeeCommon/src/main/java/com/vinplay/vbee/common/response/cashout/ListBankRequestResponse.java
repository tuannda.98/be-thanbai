/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.vbee.common.response.cashout;

import com.vinplay.vbee.common.response.BaseResponseModel;
import com.vinplay.vbee.common.response.cashout.BankRequest;
import java.util.ArrayList;
import java.util.List;

public class ListBankRequestResponse
extends BaseResponseModel {
    private long totalPages;
    private List<BankRequest> transactions = new ArrayList<>();

    public ListBankRequestResponse(boolean success, String errorCode) {
        super(success, errorCode);
    }

    public List<BankRequest> getTransactions() {
        return this.transactions;
    }

    public void setTransactions(List<BankRequest> transactions) {
        this.transactions = transactions;
    }

    public long getTotalPages() {
        return this.totalPages;
    }

    public void setTotalPages(long totalPages) {
        this.totalPages = totalPages;
    }
}

