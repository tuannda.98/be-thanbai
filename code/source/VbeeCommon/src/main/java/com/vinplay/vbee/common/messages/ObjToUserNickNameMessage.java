package com.vinplay.vbee.common.messages;


import static com.google.common.base.Preconditions.checkNotNull;

public class ObjToUserNickNameMessage extends BaseMessage {
    private final String userNickname;
    private final Object message;

    private ObjToUserNickNameMessage(String userNickname, Object baseMsg) {
        this.userNickname = userNickname;
        this.message = baseMsg;
    }

    public static ObjToUserNickNameMessage of(String userNickname, Object baseMsg) {
        checkNotNull(userNickname);
        checkNotNull(baseMsg);
        return new ObjToUserNickNameMessage(userNickname, baseMsg);
    }

    public String getUserNickname() {
        return userNickname;
    }

    public Object getMessage() {
        return message;
    }
}
