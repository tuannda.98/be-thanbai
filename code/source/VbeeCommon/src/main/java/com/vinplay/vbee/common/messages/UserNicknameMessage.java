package com.vinplay.vbee.common.messages;

import static com.google.common.base.Preconditions.checkNotNull;

public class UserNicknameMessage extends BaseMessage {
    private static final long serialVersionUID = -2083794324964258961L;
    private final String userNickname;

    private UserNicknameMessage(String userNickname) {
        this.userNickname = userNickname;
    }

    public static UserNicknameMessage of(String userNickname) {
        checkNotNull(userNickname);
        return new UserNicknameMessage(userNickname);
    }

    public String getUserNickname() {
        return userNickname;
    }
}
