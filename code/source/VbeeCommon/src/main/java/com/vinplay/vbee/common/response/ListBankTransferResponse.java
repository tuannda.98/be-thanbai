/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.vbee.common.response;

import com.vinplay.vbee.common.models.BankTranferModel;
import com.vinplay.vbee.common.response.BaseResponseModel;
import java.util.ArrayList;
import java.util.List;

public class ListBankTransferResponse
extends BaseResponseModel {
    private long totalPages;
    private List<BankTranferModel> transactions = new ArrayList<>();

    public ListBankTransferResponse(boolean success, String errorCode) {
        super(success, errorCode);
    }

    public List<BankTranferModel> getTransactions() {
        return this.transactions;
    }

    public void setTransactions(List<BankTranferModel> transactions) {
        this.transactions = transactions;
    }

    public long getTotalPages() {
        return this.totalPages;
    }

    public void setTotalPages(long totalPages) {
        this.totalPages = totalPages;
    }
}

