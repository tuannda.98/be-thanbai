/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.vbee.common.models;

public class BankTranferModel {
    public String bankNo;
    public String bankCode;
    public long amount;
    public long addMoney;
    public String comment;
    public String time;
    public String timeRequest;
    public String transId;
    public int status;
}

