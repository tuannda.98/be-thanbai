package com.vinplay.vbee.common.models;

import java.io.Serializable;

public class TopCaoThu implements Serializable {
    private String nickname;
    private long moneyWin;

    public TopCaoThu(String nickname, long moneyWin) {
        this.nickname = nickname;
        this.moneyWin = moneyWin;
    }

    public String getNickname() {
        return this.nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public long getMoneyWin() {
        return this.moneyWin;
    }

    public void setMoneyWin(long moneyWin) {
        this.moneyWin = moneyWin;
    }
}

