package com.vinplay.vbee.common.hazelcast;

import com.hazelcast.core.IMap;
import com.vinplay.vbee.common.models.cache.UserActiveModel;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.models.cache.UserMoneyModel;
import com.vinplay.vbee.common.models.cache.UserVippointEventModel;

import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class HazelcastUtils {

    public static IMap<String, UserCacheModel> getUsersMap() {
        return HazelcastClientFactory.getInstance().getMap("users");
    }

    public static void reloadUsersMap() {
        HazelcastClientFactory.getInstance().getMap("users").clear();
    }

    public static IMap<String, UserActiveModel> getActiveMap() {
        return HazelcastClientFactory.getInstance().getMap("cache_user_active");
    }

    public static IMap<String, UserMoneyModel> getMoneyMap() {
        return HazelcastClientFactory.getInstance().getMap("cache_user_money");
    }

    public static IMap<String, UserVippointEventModel> getVPEventMap() {
        return HazelcastClientFactory.getInstance().getMap("cache_user_vp_event");
    }

    public static <K, V> V autoExpireLoadingCache(K key, Function<K, V> cacheLoader, long second) {
        return autoExpireLoadingCache("auto_expire_loading_cache", key, cacheLoader, second);
    }

    public static <K, V> V autoExpireLoadingCache(String cacheName, K key, Function<K, V> cacheLoader, long second) {
        if (cacheName == null || cacheName.isEmpty()) {
            throw new IllegalArgumentException("cacheName can not be empty!");
        }
        IMap<K, V> cached = HazelcastClientFactory.getInstance().getMap(cacheName);

        Objects.requireNonNull(cacheLoader);
        V v, newValue;
        return ((v = cached.get(key)) == null &&
                (newValue = cacheLoader.apply(key)) != null &&
                (v = cached.put(key, newValue, second, TimeUnit.SECONDS)) == null) ? newValue : v;
    }
}
