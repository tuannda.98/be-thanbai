package com.vinplay.vbee.common.response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class BaseResponseModel {
    protected boolean success;
    protected String errorCode;

    protected BaseResponseModel(boolean success, String errorCode) {
        this.success = success;
        this.errorCode = errorCode;
    }

    public static BaseResponseModel success() {
        return new BaseResponseModel(true, "0");
    }

    public static BaseResponseModel success(String errorCode) {
        return new BaseResponseModel(true, errorCode);
    }

    public static BaseResponseModel failed(boolean failed, String errorCode) {
        return new BaseResponseModel(failed, errorCode);
    }

    public static BaseResponseModel failed(String errorCode) {
        return new BaseResponseModel(false, errorCode);
    }

    public static BaseResponseModel failed() {
        return new BaseResponseModel(false, "1001");
    }

    public String toJson() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException mapper) {
            return "{\"success\":false,\"errorCode\":\"1001\"}";
        }
    }

    public boolean isSuccess() {
        return this.success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}

