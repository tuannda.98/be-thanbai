package com.vinplay.vbee.common.enums;

public enum RMQQueues {
    NOTIFY_USER_MESSAGES("notify_user_messages", 1),
    NOTIFY_UPDATE_USER_MONEY("queue_update_user_money", 2),
    NOTIFY_KICK_USER("queue_kick_user", 3),
    NOTIFY_QUEUE_LOG_MONEY("queue_log_money", 601),
    NOTIFY_QUEUE_LOG_MONEY_EXTRA("queue_log_money_extra", 1001),
    NOTIFY_MONEY_IN_MINI_GAME("queue_payment_minigame", 30);

    RMQQueues(String name, int command) {
        this.name = name;
        this.command = command;
    }

    private final String name;
    private int command;

    public String getName() {
        return name;
    }

    public int getCommand() {
        return command;
    }
}
