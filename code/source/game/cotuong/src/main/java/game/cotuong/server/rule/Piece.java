/*
 * Decompiled with CFR 0.144.
 */
package game.cotuong.server.rule;

public class Piece
implements Cloneable {
    public final String key;
    public final char color;
    public final char character;
    public final char index;
    public int[] position = new int[2];

    public Piece(String name, int[] position) {
        this.key = name;
        this.color = name.charAt(0);
        this.character = name.charAt(1);
        this.index = name.charAt(2);
        this.position = position;
    }
}

