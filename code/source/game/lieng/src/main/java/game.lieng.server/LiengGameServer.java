package game.lieng.server;

import bitzero.server.core.BZEvent;
import bitzero.server.entities.User;
import bitzero.server.extensions.data.BaseMsg;
import bitzero.server.extensions.data.DataCmd;
import bitzero.util.ExtensionUtility;
import bitzero.util.common.business.CommonHandle;
import bitzero.util.common.business.Debug;
import game.entities.PlayerInfo;
import game.entities.UserScore;
import game.eventHandlers.GameEventParam;
import game.eventHandlers.GameEventType;
import game.lieng.server.cmd.SendStandUp;
import game.lieng.server.cmd.receive.RevBuyIn;
import game.lieng.server.cmd.receive.RevCheatCard;
import game.lieng.server.cmd.receive.RevShowCardSub;
import game.lieng.server.cmd.receive.RevTakeTurn;
import game.lieng.server.cmd.send.*;
import game.lieng.server.logic.*;
import game.modules.gameRoom.cmd.send.SendNoHu;
import game.modules.gameRoom.entities.*;
import game.utils.GameUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static game.modules.gameRoom.GameRoomModule.GAME_ROOM;
import static game.modules.gameRoom.entities.GameMoneyInfo.GAME_MONEY_INFO;

public class LiengGameServer
        extends GameServer {
    protected static final int gsNoPlay = 0;
    protected static final int gsPlay = 1;
    protected static final int gsResult = 2;
    protected static final int PHONG_CO_KHOA = 1;
    protected static final int PHONG_KHONG_CO_KHOA = 2;
    public final GameManager gameMgr = new GameManager(this);
    protected final List<GamePlayer> playerList = new ArrayList<>(9);
    protected int playingCount = 0;
    protected int winType;
    protected volatile int serverState = 0;
    protected volatile int groupIndex;
    protected volatile int playerCount;
    final StringBuilder gameLog = new StringBuilder();
    protected final Logger logger = LoggerFactory.getLogger("debug");
    protected final List<Turn> cheatTurns = new LinkedList<>();
    protected int turnIndex = 0;
    protected ThongTinThangLon thongTinNoHu = null;

    public LiengGameServer(GameRoom room) {
        super(room);
        int i = 0;
        while (i < 9) {
            GamePlayer gp = new GamePlayer();
            gp.gameServer = this;
            gp.spInfo.pokerInfo = new LiengPlayerInfo(gp);
            gp.chair = i++;
            this.playerList.add(gp);
        }
    }

    @Override
    public void onGameMessage(User user, DataCmd dataCmd) {
        this.logger.info("onGameMessage: ", dataCmd.getId(), user.getName());
        switch (dataCmd.getId()) {
            case 3111: {
                this.pOutRoom(user, dataCmd);
                break;
            }
            case 3102: {
                this.buyIn(user, dataCmd);
                break;
            }
            case 3101: {
                this.registerTakeTurn(user, dataCmd);
                break;
            }
            case 3115: {
                this.pCheatCards(user, dataCmd);
                break;
            }
            case 3116: {
                this.pDangKyChoiTiep(user, dataCmd);
                break;
            }
            case 3108: {
                this.pShowCard(user, dataCmd);
                break;
            }
            case 3112: {
                this.pShowCardSub(user, dataCmd);
                break;
            }
            case 3113: {
                this.standUp(user, dataCmd);
            }
        }
    }

    private void standUp(User user, DataCmd dataCmd) {
        GamePlayer gp = this.getPlayerByUser(user);
        if (gp != null) {
            gp.standUp = !gp.standUp;
            SendStandUp msg = new SendStandUp();
            msg.standUp = gp.standUp;
            this.send(msg, user);
            LiengPlayerInfo info = gp.spInfo.pokerInfo;
            if (!(!info.fold && gp.isPlaying() || this.gameMgr.isAutoStart)) {
                this.requestBuyIn(gp);
            }
        }
    }

    private void pShowCard(User user, DataCmd dataCmd) {
        SendShowCard msg = new SendShowCard();
        GamePlayer gp = this.getPlayerByUser(user);
        if (gp != null && gp.isPlaying() && this.gameMgr.isGameStateEnd() && !gp.spInfo.pokerInfo.fold) {
            msg.chair = (byte) gp.chair;
            this.send(msg);
        }
    }

    private void pShowCardSub(User user, DataCmd dataCmd) {
        RevShowCardSub cmd = new RevShowCardSub(dataCmd);
        SendShowCardSub msg = new SendShowCardSub();
        GamePlayer gp = this.getPlayerByUser(user);
        if (gp != null && gp.isPlaying() && !gp.spInfo.pokerInfo.fold) {
            msg.chair = (byte) gp.chair;
            this.checkCard(gp, cmd.cards);
            msg.cards = this.checkCard(gp, cmd.cards);
            if (msg.cards != null) {
                msg.cards = this.shuffleCard(msg.cards);
                this.send(msg);
            }
        }
    }

    private byte[] checkCard(GamePlayer gp, byte[] cards) {
        int i;
        byte[] handCard = gp.spInfo.handCards.toByteArray();
        if (cards.length >= handCard.length) {
            return null;
        }
        byte[] card = new byte[3];
        for (i = 0; i < card.length; ++i) {
            card[i] = -1;
        }
        block1:
        for (i = 0; i < cards.length; ++i) {
            byte b1 = cards[i];
            for (int j = 0; j < handCard.length; ++j) {
                byte b2 = handCard[j];
                if (b1 != b2) continue;
                card[j] = b1;
                continue block1;
            }
        }
        return card;
    }

    private void buyIn(User user, DataCmd data) {
        RevBuyIn cmd = new RevBuyIn(data);
        this.buyIn(user, cmd.moneyBuyIn, cmd.autoBuyIn);
    }

    private void buyIn(User user, long moneyBuyIn, boolean autoBuyIn) {
        GamePlayer gp = this.getPlayerByUser(user);
        this.buyIn(gp, moneyBuyIn, autoBuyIn);
    }

    private boolean checkBuyInMoney(long moneyBuyIn) {
        return moneyBuyIn >= 5L * this.getMoneyBet() && moneyBuyIn <= 200L * this.getMoneyBet();
    }

    private void buyIn(GamePlayer gp, long moneyBuyIn, boolean autoBuyIn) {
        if (!this.canBuyIn(gp)) {
            return;
        }

        if (!this.checkBuyInMoney(moneyBuyIn)) {
            GameRoomManager.instance().leaveRoom(gp.getUser(), this.gameRoom);
            this.notifyKickRoom(gp, (byte) 3);
            return;
        }

        ListGameMoneyInfo.instance().removeGameMoneyInfo(gp.gameMoneyInfo, this.gameRoom.getId());
        if (!gp.gameMoneyInfo.freezeMoneyBegining(moneyBuyIn)) {
            GameRoomManager.instance().leaveRoom(gp.getUser(), this.gameRoom);
            this.notifyKickRoom(gp, (byte) 3);
            return;
        }

        SendBuyIn msg = new SendBuyIn();
        msg.buyInMoney = gp.gameMoneyInfo.freezeMoney;
        LiengPlayerInfo pokerInfo = gp.spInfo.pokerInfo;
        pokerInfo.currentMoney = msg.buyInMoney;
        pokerInfo.lastBuyInMoney = msg.buyInMoney;
        Debug.trace(new Object[]{"buyIn: Freeze=;Current=;", gp.pInfo.nickName, gp.gameMoneyInfo.freezeMoney, pokerInfo.currentMoney});
        msg.chair = gp.chair;
        gp.autoBuyIn = autoBuyIn;
        this.send(msg);
        gp.standUp = false;
        gp.lastTimeBuyIn = System.currentTimeMillis();
    }

    private boolean canBuyIn(GamePlayer gp) {
        if (gp == null) {
            return false;
        }
        long delta = System.currentTimeMillis() - gp.lastTimeBuyIn;
        if (delta < 1000L) {
            return false;
        }
        LiengPlayerInfo info = gp.spInfo.pokerInfo;
        if (info == null) {
            return false;
        }
        boolean flag = gp.standUp && (!gp.isPlaying() || info.fold || this.gameMgr.isGameStateEnd());
        return info.currentMoney < 1L * this.getMoneyBet() || flag;
    }

    private void buyInAuTo(GamePlayer gp) {
        if (this.canBuyIn(gp)) {
            if (!this.checkBuyInMoney(gp.spInfo.pokerInfo.lastBuyInMoney) || !gp.autoBuyIn) {
                Debug.trace(new Object[]{"buyInAuTo failed", gp.gameMoneyInfo.freezeMoney, gp.autoBuyIn});
                return;
            }
            LiengPlayerInfo pokerInfo = gp.spInfo.pokerInfo;
            this.buyIn(gp, pokerInfo.lastBuyInMoney, true);
            return;
        }
        Debug.trace("buyInAuTo failed gameplayer null");
    }

    private GameManager getGameManager() {
        return this.gameMgr;
    }

    private int getServerState() {
        return this.serverState;
    }

    private GamePlayer getPlayerByChair(int i) {
        if (i >= 0 && i < 9) {
            return this.playerList.get(i);
        }
        Debug.trace("Get player null: " + i);
        return null;
    }

    private long getMoneyBet() {
        return this.gameRoom.setting.moneyBet;
    }

    private byte getPlayerCount() {
        return (byte) this.playerCount;
    }

    private boolean checkPlayerChair(int chair) {
        return chair >= 0 && chair < 9;
    }

    @Override
    public void onGameUserExit(User user) {
        Integer chair = user.getProperty(USER_CHAIR);
        if (chair == null) {
            Debug.trace(new Object[]{"onGameUserExit", "chair null", user.getName()});
            return;
        }
        synchronized (this) {
            GamePlayer gp = this.getPlayerByChair(chair);
            if (gp == null) {
                return;
            }
            if (gp.isPlaying()) {
                gp.reqQuitRoom = true;
                this.gameLog.append("DIS<").append(chair).append(">");
            } else {
                this.removePlayerAtChair(chair, !user.isConnected());
            }
            if (this.gameRoom.userManager.size() == 0) {
                this.resetPlayDisconnect();
                this.destroy();
            }
        }
    }

    private void resetPlayDisconnect() {
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (gp.pInfo == null) continue;
            gp.pInfo.setIsHold(false);
        }
    }

    @Override
    public void onGameUserDis(User user) {
        synchronized (this) {
            Integer chair = user.getProperty(USER_CHAIR);
            if (chair == null) {
                Debug.trace(new Object[]{"onGameUserExit", "chair null", user.getName()});
                return;
            }
            GamePlayer gp = this.getPlayerByChair(chair);
            if (gp == null) {
                return;
            }
            if (gp.isPlaying()) {
                gp.reqQuitRoom = true;
                this.gameLog.append("DIS<").append(chair).append(">");
            } else {
                GameRoomManager.instance().leaveRoom(user, this.gameRoom);
            }
        }
    }

    @Override
    public void onGameUserReturn(User user) {
        if (user == null) {
            return;
        }
        synchronized (this) {
            for (int i = 0; i < 9; ++i) {
                GamePlayer gp = this.playerList.get(i);
                if (gp.getPlayerStatus() == 0 || gp.pInfo == null || gp.pInfo.userId != user.getId()) continue;
                this.gameLog.append("RE<").append(i).append(">");
                GameMoneyInfo moneyInfo = user.getProperty(GAME_MONEY_INFO);
                if (moneyInfo != null && !gp.gameMoneyInfo.sessionId.equals(moneyInfo.sessionId)) {
                    ListGameMoneyInfo.instance().removeGameMoneyInfo(moneyInfo, -1);
                }
                user.setProperty(USER_CHAIR, gp.chair);
                gp.user = user;
                gp.reqQuitRoom = false;
                user.setProperty(GAME_MONEY_INFO, gp.gameMoneyInfo);
                gp.user.setProperty(USER_CHAIR, gp.chair);
                this.sendGameInfo(gp.chair);
                break;
            }
        }
    }

    @Override
    public void onGameUserEnter(User user) {
        if (user == null) {
            return;
        }
        PlayerInfo pInfo = PlayerInfo.getInfo(user);
        if (pInfo == null) {
            return;
        }
        synchronized (this) {
            GameMoneyInfo moneyInfo = user.getProperty(GAME_MONEY_INFO);
            if (moneyInfo == null) {
                return;
            }
            for (int i = 0; i < 9; ++i) {
                GamePlayer gp = this.playerList.get(i);
                if (gp.getPlayerStatus() == 0 || gp.pInfo == null || gp.pInfo.userId != user.getId()) continue;
                this.gameLog.append("RE<").append(i).append(">");
                if (moneyInfo != null && !gp.gameMoneyInfo.sessionId.equals(moneyInfo.sessionId)) {
                    ListGameMoneyInfo.instance().removeGameMoneyInfo(moneyInfo, -1);
                }
                user.setProperty(USER_CHAIR, gp.chair);
                gp.user = user;
                gp.reqQuitRoom = false;
                user.setProperty(GAME_MONEY_INFO, gp.gameMoneyInfo);
                gp.user.setProperty(USER_CHAIR, gp.chair);
                if (this.serverState == 1) {
                    this.sendGameInfo(gp.chair);
                } else {
                    this.notifyUserEnter(gp);
                }
                return;
            }
            if (this.gameRoom.setting.maxUserPerRoom == 2) {
                for (int i = 0; i < 9; ++i) {
                    GamePlayer gp = this.playerList.get(i);
                    if (i != 0 && i != 1 || gp.getPlayerStatus() != 0) continue;
                    if (this.serverState == 0) {
                        gp.setPlayerStatus(2);
                    } else {
                        gp.setPlayerStatus(1);
                    }
                    gp.takeChair(user, pInfo, moneyInfo);
                    ++this.playerCount;
                    if (this.playerCount == 1) {
                        this.gameMgr.roomCreatorUserId = user.getId();
                        this.gameMgr.roomOwnerChair = i;
                        this.init();
                    }
                    this.notifyUserEnter(gp);
                    this.kiemTraTuDongBatDau(2);
                    return;
                }
            }
            for (int i = 0; i < 9; ++i) {
                GamePlayer gp = this.playerList.get(i);
                if (gp.getPlayerStatus() != 0) continue;
                if (this.serverState == 0) {
                    gp.setPlayerStatus(2);
                } else {
                    gp.setPlayerStatus(1);
                }
                gp.takeChair(user, pInfo, moneyInfo);
                ++this.playerCount;
                if (this.playerCount == 1) {
                    this.gameMgr.roomCreatorUserId = user.getId();
                    this.gameMgr.roomOwnerChair = i;
                    this.init();
                }
                this.notifyUserEnter(gp);
                this.kiemTraTuDongBatDau(2);
                return;
            }
        }
    }

    private void sendMsgToPlayingUser(BaseMsg msg) {
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying()) continue;
            this.send(msg, gp.getUser());
        }
    }

    protected void send(BaseMsg msg) {
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (gp.getUser() == null) continue;
            ExtensionUtility.getExtension().send(msg, gp.getUser());
        }
    }

    protected void chiabai() {
        this.gameLog.append("CB<");
        SendDealPrivateCard msg = new SendDealPrivateCard();
        msg.gameId = this.gameMgr.gameTable.id;
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.playerList.get(i);
            if (!gp.isPlaying()) continue;
            User user = gp.getUser();
            msg.cards = gp.spInfo.handCards.toByteArray();
            msg.cards = this.shuffleCard(msg.cards);
            msg.card_name = gp.spInfo.handCards.tinhChoDiemBoChoClient();
            this.gameLog.append(gp.chair).append("/");
            this.gameLog.append(gp.spInfo.handCards.toString()).append("/");
            this.gameLog.append(gp.spInfo.handCards.kiemTraBo()).append(";");
            Debug.trace(new Object[]{"chiabai:", gp.pInfo.nickName, gp.spInfo.handCards});
            msg.countDown = this.gameMgr.countDown;
            this.send(msg, user);
        }
        this.gameLog.append(">");
        this.gameLog.append("PC<");
        LiengGameInfo gameInfo = this.gameMgr.gameTable.pokerGameInfo;
        this.gameLog.append(gameInfo.publicCard);
        this.gameLog.append(">");
    }

    private byte[] shuffleCard(byte[] cards) {
        int i;
        LinkedList<Byte> x = new LinkedList<>();
        for (i = 0; i < cards.length; ++i) {
            x.add(cards[i]);
        }
        Collections.shuffle(x);
        for (i = 0; i < cards.length; ++i) {
            cards[i] = x.get(i);
        }
        return cards;
    }

    protected void start() {
        Debug.trace("Start game");
        this.gameMgr.isAutoStart = false;
        this.gameLog.setLength(0);
        this.gameLog.append("BD<");
        this.playingCount = 0;
        this.serverState = 1;
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!this.coTheChoiTiep(gp)) continue;
            gp.setPlayerStatus(3);
            ++this.playingCount;
            gp.pInfo.setIsHold(true);
            PlayerInfo.setRoomId(gp.pInfo.nickName, this.gameRoom.getId());
            this.gameLog.append(gp.pInfo.nickName).append("/");
            this.gameLog.append(i).append(";");
            gp.choiTiepVanSau = false;
        }
        this.gameLog.append(this.gameRoom.setting.moneyType).append(";");
        this.gameLog.append(">");
        this.clearInfoNewGame();
        this.gameMgr.gameAction = 0;
        this.gameMgr.countDown = 0;
        this.logStartGame();
    }

    private void clearInfoNewGame() {
        LiengGameInfo gameInfo = this.gameMgr.gameTable.pokerGameInfo;
        gameInfo.clearNewGame();
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying()) continue;

            LiengPlayerInfo pokerInfo = gp.spInfo.pokerInfo;
            pokerInfo.clearNewGame();
        }
    }

    private void logStartGame() {
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying()) continue;
            Debug.trace(new Object[]{"logStartGame", gp.chair, gp.pInfo.nickName});
            GameUtils.logStartGame(this.gameMgr.gameTable.id, gp.pInfo.nickName, this.gameMgr.gameTable.logTime, this.gameRoom.setting.moneyType);
        }
    }

    private int demSoNguoiChoiTiep() {
        int count = 0;
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!this.coTheChoiTiep(gp)) continue;
            ++count;
        }
        return count;
    }

    private int demSoNguoiDangChoi() {
        int count = 0;
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            ++count;
        }
        return count;
    }

    protected void kiemTraTuDongBatDau(int after) {
        if (!this.gameMgr.isGameStateNoStart()) return;
        int count = this.demSoNguoiChoiTiep();
        if (count < 2) {
            this.gameMgr.cancelAutoStart();
            return;
        }

        this.gameMgr.makeAutoStart(after);
        if (count > 2) {
            // this.xuLiDanhCap(count);
        }
    }

    private void xuLiDanhCap(int count) {
        GamePlayer gp2;
        GamePlayer gp1;
        int j;
        int i;
        boolean checkIp = false;
        for (i = 0; i < 9; ++i) {
            gp1 = this.getPlayerByChair(i);
            if (gp1.getUser() == null) continue;
            for (j = i + 1; j < 9; ++j) {
                gp2 = this.getPlayerByChair(j);
                if (gp2.getUser() == null || gp1.getUser().getIpAddress().equalsIgnoreCase(gp2.getUser().getIpAddress()))
                    continue;
                checkIp = true;
            }
        }
        if (!checkIp) {
            return;
        }
        for (i = 0; i < 9; ++i) {
            gp1 = this.getPlayerByChair(i);
            if (gp1.getUser() == null) continue;
            for (j = i + 1; j < 9; ++j) {
                gp2 = this.getPlayerByChair(j);
                if (gp2.getUser() == null || this.kiemTraDuocDanhCungNhau(gp1, gp2, checkIp) || --count != 2) continue;
                return;
            }
        }
    }

    private boolean kiemTraDuocDanhCungNhau(GamePlayer gp1, GamePlayer gp2, boolean checkIp) {
        Debug.trace(new Object[]{"kiemTraDuocDanhCungNhau1", gp1.getUser().getName(), gp1.reqQuitRoom, gp1.timeJoinRoom, checkIp, gp1.getUser().getIpAddress()});
        Debug.trace(new Object[]{"kiemTraDuocDanhCungNhau2", gp2.getUser().getName(), gp2.reqQuitRoom, gp2.timeJoinRoom, checkIp, gp2.getUser().getIpAddress()});
        if (gp1.reqQuitRoom || gp2.reqQuitRoom) {
            return false;
        }
        if (checkIp && gp1.getUser().getIpAddress().equalsIgnoreCase(gp2.getUser().getIpAddress())) {
            if (gp1.timeJoinRoom > gp2.timeJoinRoom) {
                gp1.reqQuitRoom = true;
                GameRoomManager.instance().leaveRoom(gp1.getUser(), this.gameRoom);
            } else {
                gp2.reqQuitRoom = true;
                GameRoomManager.instance().leaveRoom(gp2.getUser(), this.gameRoom);
            }
            return false;
        }
        long delta = Math.abs(gp1.timeJoinRoom - gp2.timeJoinRoom);
        if ((double) delta < 1500.0) {
            if (gp1.timeJoinRoom > gp2.timeJoinRoom) {
                GameRoomManager.instance().leaveRoom(gp1.getUser(), this.gameRoom);
                gp1.reqQuitRoom = true;
            } else {
                GameRoomManager.instance().leaveRoom(gp2.getUser(), this.gameRoom);
                gp2.reqQuitRoom = true;
            }
            return false;
        }
        return true;
    }

    private boolean coTheChoiTiep(GamePlayer gp) {
        return gp.user != null && gp.user.isConnected() && gp.canPlayNextGame() && gp.spInfo.pokerInfo.currentMoney >= 1L * this.gameRoom.setting.moneyBet;
    }

    private void removePlayerAtChair(int chair, boolean disconnect) {
        if (!this.checkPlayerChair(chair)) {
            Debug.trace(new Object[]{"removePlayerAtChair error", chair});
            return;
        }
        GamePlayer gp = this.playerList.get(chair);
        gp.standUp = false;
        gp.choiTiepVanSau = true;
        gp.countToOutRoom = 0;
        this.notifyUserExit(gp, disconnect);
        if (gp.user != null) {
            gp.user.removeProperty(USER_CHAIR);
            gp.user.removeProperty(GAME_ROOM);
            gp.user.removeProperty(GAME_MONEY_INFO);
        }
        gp.spInfo.pokerInfo.clearOutGame();
        gp.user = null;
        gp.pInfo = null;
        if (gp.gameMoneyInfo != null) {
            ListGameMoneyInfo.instance().removeGameMoneyInfo(gp.gameMoneyInfo, this.gameRoom.getId());
        }
        gp.gameMoneyInfo = null;
        gp.setPlayerStatus(0);
        --this.playerCount;
        this.kiemTraTuDongBatDau(2);
    }

    private void notifyUserEnter(GamePlayer gamePlayer) {
        User user = gamePlayer.getUser();
        if (user == null) {
            return;
        }
        gamePlayer.timeJoinRoom = System.currentTimeMillis();
        SendNewUserJoin msg = new SendNewUserJoin();
        msg.money = gamePlayer.gameMoneyInfo.currentMoney;
        msg.uStatus = gamePlayer.getPlayerStatus();
        msg.setBaseInfo(gamePlayer.pInfo);
        msg.uChair = gamePlayer.chair;
        this.sendMsgExceptMe(msg, user);
        this.notifyJoinRoomSuccess(gamePlayer);
        if (GameUtils.isBot && user.isBot()) {
            this.buyIn(gamePlayer, 5L * this.getMoneyBet(), true);
        }
    }

    private void notifyJoinRoomSuccess(GamePlayer gamePlayer) {
        SendJoinRoomSuccess msg = new SendJoinRoomSuccess();
        msg.uChair = gamePlayer.chair;
        msg.roomId = this.gameRoom.getId();
        msg.comission = this.gameRoom.setting.commisionRate;
        msg.comissionJackpot = this.gameRoom.setting.rule;
        msg.moneyType = this.gameRoom.setting.moneyType;
        msg.rule = this.gameRoom.setting.rule;
        msg.gameId = this.gameMgr.gameTable.id;
        msg.moneyBet = this.gameRoom.setting.moneyBet;
        msg.roomOwner = (byte) this.gameMgr.roomOwnerChair;
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            msg.playerStatus[i] = (byte) gp.getPlayerStatus();
            msg.playerList[i] = gp.getPlayerInfo();
            msg.moneyInfoList[i] = gp.spInfo.pokerInfo;
            if (gp.getUser() == null || gp.spInfo.handCards == null) continue;
            msg.handCardSize[i] = 2;
        }
        msg.gameAction = (byte) this.gameMgr.gameAction;
        msg.curentChair = (byte) this.gameMgr.currentChair();
        msg.countDownTime = (byte) this.gameMgr.countDown;
        this.send(msg, gamePlayer.getUser());
    }

    private void notifyUserExit(GamePlayer gamePlayer, boolean disconnect) {
        if (gamePlayer.pInfo != null) {
            Debug.trace(new Object[]{gamePlayer.pInfo.nickName, gamePlayer.chair, "exit room ", disconnect});
            gamePlayer.pInfo.setIsHold(false);
            SendUserExitRoom msg = new SendUserExitRoom();
            msg.nChair = (byte) gamePlayer.chair;
            msg.nickName = gamePlayer.pInfo.nickName;
            this.send(msg);
        } else {
            Debug.trace(new Object[]{gamePlayer.chair, "exit room playerInfo null"});
        }
    }

    private GamePlayer getPlayerByUser(User user) {
        Integer chair = user.getProperty(USER_CHAIR);
        Debug.trace(new Object[]{"getPlayerByUser: ", user.getName(), chair});
        if (chair != null) {
            GamePlayer gp = this.getPlayerByChair(chair);
            if (gp != null && gp.pInfo != null && gp.pInfo.nickName.equalsIgnoreCase(user.getName())) {
                return gp;
            }
            return null;
        }
        return null;
    }

    private void sendGameInfo(int chair) {
        GamePlayer me = this.getPlayerByChair(chair);
        if (me != null) {
            SendGameInfo msg = new SendGameInfo();
            msg.pokerGameInfo = this.gameMgr.gameTable.pokerGameInfo;
            msg.currentChair = this.gameMgr.currentChair;
            msg.gameState = this.gameMgr.getGameStateId();
            msg.gameAction = this.gameMgr.gameAction;
            msg.countdownTime = this.gameMgr.countDown;
            msg.maxUserPerRoom = this.gameRoom.setting.maxUserPerRoom;
            msg.moneyType = this.gameRoom.setting.moneyType;
            msg.roomBet = this.gameRoom.setting.moneyBet;
            msg.gameId = this.gameMgr.gameTable.id;
            msg.roomId = this.gameRoom.getId();
            Round round = this.gameMgr.gameTable.getLastRound();
            if (round != null) {
                msg.roundId = round.roundId;
            }
            msg.initPrivateInfo(me);
            for (int i = 0; i < 9; ++i) {
                GamePlayer gp = this.getPlayerByChair(i);
                if (gp.isPlaying()) {
                    msg.hasInfoAtChair[i] = true;
                    msg.pInfos[i] = gp;
                    continue;
                }
                msg.hasInfoAtChair[i] = false;
            }
            this.send(msg, me.getUser());
        }
    }

    private void pOutRoom(User user, DataCmd dataCmd) {
        Debug.trace(new Object[]{"pOutRoom", user.getName()});
        GamePlayer gp = this.getPlayerByUser(user);
        if (gp != null) {
            if (gp.isPlaying()) {
                gp.reqQuitRoom = !gp.reqQuitRoom;
                this.notifyRegisterOutRoom(gp);
            } else {
                GameRoomManager.instance().leaveRoom(user, this.gameRoom);
            }
        }
    }

    private void notifyRegisterOutRoom(GamePlayer gp) {
        SendNotifyReqQuitRoom msg = new SendNotifyReqQuitRoom();
        msg.chair = (byte) gp.chair;
        msg.reqQuitRoom = gp.reqQuitRoom;
        this.send(msg);
    }

    private void registerTakeTurn(User user, DataCmd dataCmd) {
        RevTakeTurn cmd = new RevTakeTurn(dataCmd);
        this.registerTakeTurn(user, cmd);
    }

    private boolean registerTakeTurn(User user, RevTakeTurn cmd) {
        if (!this.gameMgr.isGameStatePlaying()) {
            return false;
        }
        Debug.trace("registerTakeTurn", user.getName());
        GamePlayer gp = this.getPlayerByUser(user);
        if (gp != null && this.gameMgr.gameAction == 4 && gp.chair == this.gameMgr.currentChair && this.gameMgr.countDown >= 1) {
            LiengPlayerInfo info = gp.spInfo.pokerInfo;
            LiengGameInfo gameInfo = this.gameMgr.gameTable.pokerGameInfo;
            boolean res = false;
            res = info.register(cmd, info, gameInfo);
            if (res) {
                this.takeTurn();
                return true;
            }
            Debug.trace("register take turn failed");
            return false;
        }
        Debug.trace("Take turn invalid order");
        return false;
    }

    private void endGame() {
        this.gameMgr.setGameStateEndGame();
        this.gameMgr.countDown = (int) (10.0 + Math.ceil(1.5 * (double) this.playingCount));
        this.gomTienVaoHu();
        this.sortUserRanking();
        this.tinhTraTien();
        this.traTien();
        this.notifyEndGame();
        this.kiemTraNoHuThangLon();
    }

    private void notifyEndGame() {
        LiengGameInfo gameInfo = this.gameMgr.gameTable.pokerGameInfo;
        LiengResult result = gameInfo.resultPoker;
        List<LiengRank> listRank = result.ranking;
        SendEndGame msg = new SendEndGame();
        msg.moneyPot = gameInfo.potMoney;
        for (LiengRank rank : listRank) {
            msg.ketQuaTinhTien[rank.chair] = rank.finalMoney;
            msg.winLost[rank.chair] = rank.win;
            msg.rank[rank.chair] = rank.fold ? 10L : (long) rank.rank;
        }
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (gp.isPlaying()) {
                LiengPlayerInfo pokerInfo = gp.spInfo.pokerInfo;
                msg.hasInfoAtChair[gp.chair] = pokerInfo.getStatus();
                msg.moneyArray[gp.chair] = pokerInfo.currentMoney;
                msg.balanceMoney[gp.chair] = gp.gameMoneyInfo.getCurrentMoneyFromCache();
                if (!pokerInfo.fold) {
                    msg.cards[gp.chair] = gp.spInfo.handCards.toByteArray();
                    msg.groupCardName[gp.chair] = (byte) gp.spInfo.handCards.tinhChoDiemBoChoClient();
                } else {
                    msg.cards[gp.chair] = new byte[0];
                }
                if (msg.ketQuaTinhTien[i] != 0L) continue;
            }
            msg.hasInfoAtChair[gp.chair] = 0;
        }
        msg.countdown = this.gameMgr.countDown;
        this.send(msg);
        this.logKetQua(msg);
    }

    private void logKetQua(SendEndGame msg) {
        this.gameLog.append("KT<");
        this.gameLog.append(0).append(";");
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying()) continue;
            this.gameLog.append(gp.chair).append("/").append(msg.ketQuaTinhTien[gp.chair]).append("/").append(gp.spInfo.maxCards).append(";");
        }
        this.gameLog.append(">");
        GameUtils.logEndGame(this.gameMgr.gameTable.id, this.gameLog.toString(), this.gameMgr.gameTable.logTime);
    }

    private void traTien() {
        LiengGameInfo gameInfo = this.gameMgr.gameTable.pokerGameInfo;
        LiengResult result = gameInfo.resultPoker;
        List<LiengRank> listRank = result.ranking;
        for (LiengRank rank : listRank) {
            GamePlayer gp = this.getPlayerByChair(rank.chair);
            LiengPlayerInfo info = gp.spInfo.pokerInfo;
            rank.finalMoney = rank.totalMoneyWin;
            boolean bl = rank.win = rank.finalMoney > info.totalBet;
            if (rank.finalMoney > 0L) {
                long moneyWin;
                UserScore score = new UserScore();
                score.money = rank.finalMoney;
                if (score.money > info.totalBet) {
                    moneyWin = score.money - info.totalBet;
                    score.wastedMoney = (long) ((double) (moneyWin * (long) this.gameRoom.setting.commisionRate) / 100.0);
                    score.money -= score.wastedMoney;
                    score.winCount = 1;
                    gameInfo.dealer = gp.chair;
                    if (this.gameRoom.setting.moneyType == 1) {
                        GameMoneyInfo.moneyService.addVippoint(gp.gameMoneyInfo.nickName, moneyWin, "vin");
                    }
                } else {
                    moneyWin = info.totalBet - score.money;
                    score.lostCount = 1;
                    if (this.gameRoom.setting.moneyType == 1) {
                        GameMoneyInfo.moneyService.addVippoint(gp.gameMoneyInfo.nickName, moneyWin, "vin");
                    }
                }
                try {
                    rank.finalMoney = gp.gameMoneyInfo.chargeMoneyInGame(score, this.gameRoom.getId(), this.gameMgr.gameTable.id);
                } catch (MoneyException e) {
                    if (!(e instanceof NotEnoughMoneyException)) {
                        CommonHandle.writeErrLog("ERROR WHEN CHARGE MONEY INGAME" + gp.gameMoneyInfo.toString(), e);
                    }
                    gp.reqQuitRoom = true;
                }
                score.money = rank.finalMoney;
                gp.spInfo.pokerInfo.currentMoney += rank.finalMoney;
                this.dispatchAddEventScore(gp.getUser(), score);
                continue;
            }
            if (this.gameRoom.setting.moneyType != 1) continue;
            GameMoneyInfo.moneyService.addVippoint(gp.gameMoneyInfo.nickName, info.totalBet, "vin");
        }
    }

    private void tinhTraTien() {
        int i;
        LiengRank rank;
        LiengGameInfo gameInfo = this.gameMgr.gameTable.pokerGameInfo;
        LiengResult result = gameInfo.resultPoker;
        List<LiengRank> listRank = result.ranking;
        int index = 0;
        int size = listRank.size();
        LinkedList<LiengRank> winList = new LinkedList<>();
        LinkedList<LiengRank> lostList = new LinkedList<>();
        Debug.trace("=================================================tinhTraTien1==================================================");
        for (i = 0; i < listRank.size(); ++i) {
            rank = listRank.get(i);
            Debug.trace(rank);
        }
        Debug.trace("=================================================tinhTraTien2==================================================");
        while (index < size) {
            try {
                winList.clear();
                lostList.clear();
                LiengRank myRank = listRank.get(index);
                for (int i2 = index; i2 < size; ++i2) {
                    LiengRank otherRank = listRank.get(i2);
                    if (myRank.rank < otherRank.rank || otherRank.fold) {
                        lostList.add(otherRank);
                        continue;
                    }
                    winList.add(otherRank);
                }
                long winUnit = myRank.totalBet;
                int winSize = winList.size();
                long totalLost = 0L;
                for (int i3 = index; i3 < listRank.size(); ++i3) {
                    LiengRank rank2 = listRank.get(i3);
                    long lostMoney = rank2.totalBet;
                    if (rank2.totalBet > winUnit) {
                        lostMoney = winUnit;
                    }
                    rank2.totalBet -= lostMoney;
                    totalLost += lostMoney;
                }
                long winShare = totalLost;
                if (winSize != 0) {
                    winShare = (long) Math.floor((double) totalLost * 1.0 / (double) winSize);
                    for (LiengRank liengRank : winList) {
                        LiengRank winRank = (LiengRank) liengRank;
                        winRank.totalMoneyWin += winShare;
                    }
                }
                ++index;
            } catch (Exception e) {
                CommonHandle.writeErrLog(this.gameLog.toString(), e);
                break;
            }
        }
        for (i = 0; i < listRank.size(); ++i) {
            rank = listRank.get(i);
            Debug.trace(rank);
        }
        Debug.trace("============================================tinhTraTien3=======================================================");
    }

    public void dispatchAddEventScore(User user, UserScore score) {
        if (user == null) {
            return;
        }
        Debug.trace(new Object[]{"Change money user:", user.getName(), GameUtils.toJsonString(score)});
        score.moneyType = this.gameRoom.setting.moneyType;
        UserScore newScore = score.clone();
        HashMap<GameEventParam, Object> evtParams = new HashMap<>();
        evtParams.put(GameEventParam.USER, user);
        evtParams.put(GameEventParam.USER_SCORE, newScore);
        ExtensionUtility.dispatchEvent(new BZEvent(GameEventType.EVENT_ADD_SCORE, evtParams));
    }

    private void sortUserRanking() {
        int i;
        ArrayList<LiengRank> listRank = new ArrayList<>();
        LiengGameInfo gameInfo = this.gameMgr.gameTable.pokerGameInfo;
        LiengResult result = gameInfo.resultPoker;
        for (int i2 = 0; i2 < 9; ++i2) {
            GamePlayer gp = this.getPlayerByChair(i2);
            if (!gp.isPlaying()) continue;
            LiengPlayerInfo playerInfo = gp.spInfo.pokerInfo;
            LiengRank rank = new LiengRank();
            rank.fold = playerInfo.fold;
            rank.chair = gp.chair;
            gp.spInfo.maxCards = rank.cards = gp.spInfo.handCards;
            rank.totalBet = playerInfo.totalBet;
            listRank.add(rank);
        }
        Collections.sort(listRank);
        int rank = 1;
        LiengRank prev = null;
        for (i = 0; i < listRank.size(); ++i) {
            LiengRank current = listRank.get(i);
            if (prev == null || LiengRule.soSanhBoBai(prev.cards, current.cards) != 0) {
                // empty if block
            }
            prev = current;
            prev.rank = ++rank;
        }
        result.ranking = listRank;
        for (i = 0; i < result.ranking.size(); ++i) {
            LiengRank r = result.ranking.get(i);
            Debug.trace(new Object[]{"sortUserRanking:", r});
        }
    }

    private boolean dispatchEventThangLon(GamePlayer gp, boolean isNoHu) {
        boolean result = GameUtils.dispatchEventThangLon(gp.getUser(), this.gameRoom, this.gameMgr.gameTable.id, gp.gameMoneyInfo, this.getMoneyBet(), isNoHu, gp.getHandCards());
        return result;
    }

    private void kiemTraNoHuThangLon() {
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying()) continue;
            if (gp.spInfo.kiemTraNoHu()) {
                if (!this.dispatchEventThangLon(gp, true)) continue;
                this.gameMgr.countDown += 5;
                continue;
            }
            this.dispatchEventThangLon(gp, false);
        }
    }

    private void notifyKickRoom(GamePlayer gp, byte reason) {
        SendKickRoom msg = new SendKickRoom();
        msg.reason = reason;
        this.send(msg, gp.getUser());
    }

    private boolean checkMoneyPlayer(GamePlayer gp) {
        return gp.spInfo.pokerInfo.currentMoney >= 1L * this.gameRoom.setting.moneyBet;
    }

    private boolean coTheOLaiBan(GamePlayer gp) {
        return gp.user != null && gp.user.isConnected() && !gp.reqQuitRoom;
    }

    protected void pPrepareNewGame() {
        this.gameMgr.setGameStateNoStart();
        this.tuDongBuyIn();
        SendUpdateMatch msg = new SendUpdateMatch();
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (gp.getPlayerStatus() != 0) {
                if (GameUtils.isMainTain || !this.coTheOLaiBan(gp) && gp.isPlaying()) {
                    if (GameUtils.isMainTain) {
                        this.notifyKickRoom(gp, (byte) 2);
                    }
                    if (gp.getUser() != null && this.gameRoom != null) {
                        GameRoom gameRoom = gp.getUser().getProperty(GAME_ROOM);
                        if (gameRoom == this.gameRoom) {
                            GameRoomManager.instance().leaveRoom(gp.getUser());
                        }
                    } else {
                        this.removePlayerAtChair(i, false);
                    }
                    msg.hasInfoAtChair[i] = false;
                } else {
                    if (!this.checkMoneyPlayer(gp)) {
                        this.requestBuyIn(gp);
                    }
                    msg.hasInfoAtChair[i] = true;
                    msg.pInfos[i] = gp;
                }
                gp.setPlayerStatus(2);
            } else {
                msg.hasInfoAtChair[i] = false;
            }
            gp.prepareNewGame();
        }
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!msg.hasInfoAtChair[i]) continue;
            msg.chair = (byte) i;
            this.send(msg, gp.getUser());
        }
        this.gameMgr.prepareNewGame();
        this.serverState = 0;
    }

    private void requestBuyIn(GamePlayer gp) {
        if (this.canBuyIn(gp)) {
            if (!gp.standUp) {
                ++gp.countToOutRoom;
            }
            if (this.gameMgr.isGameStateEnd()) {
                this.kiemTraTuDongBatDau(2);
            }
            gp.spInfo.pokerInfo.currentMoney = 0L;
            SendRequestBuyIn msg = new SendRequestBuyIn();
            this.send(msg, gp.getUser());
            Debug.trace(new Object[]{"Request buy in", gp.pInfo.nickName});
        }
    }

    private void tuDongBuyIn() {
        for (int i = 0; i < 9; ++i) {
            boolean flag2;
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.hasUser()) continue;
            if (gp.countToOutRoom >= 2) {
                gp.reqQuitRoom = true;
                continue;
            }
            LiengPlayerInfo pokerPlayerInfo = gp.spInfo.pokerInfo;
            if (gp.standUp) {
                pokerPlayerInfo.currentMoney = 0L;
                continue;
            }
            GameMoneyInfo moneyInfo = gp.gameMoneyInfo;
            boolean flag1 = moneyInfo.currentMoney >= 5L * this.gameRoom.setting.moneyBet;
            boolean bl = flag2 = pokerPlayerInfo.currentMoney < 1L * this.gameRoom.setting.moneyBet;
            if (!flag1 || !flag2) continue;
            this.buyInAuTo(gp);
        }
    }

    private void logEndGame() {
        GameUtils.logEndGame(this.gameMgr.gameTable.id, this.gameLog.toString(), this.gameMgr.gameTable.logTime);
    }

    private void tudongChoi1() {
        GamePlayer gp = this.getPlayerByChair(this.gameMgr.currentChair);
        if (gp != null && gp.getUser().isBot()) {
            Debug.trace(new Object[]{"TU DONG CHOI =========================", gp.pInfo.nickName, " BEGIN ====================="});
            LiengPlayerInfo pokerInfo = gp.spInfo.pokerInfo;
            Round round = this.gameMgr.gameTable.getCurrentRound();
            LiengGameInfo gameInfo = this.gameMgr.gameTable.pokerGameInfo;
            Random rd = new Random();
            boolean res = false;
            while (!res) {
                int action = Math.abs(rd.nextInt() % 5);
                RevTakeTurn cmd = new RevTakeTurn(new DataCmd(new byte[20]));
                if (action == 0) {
                    cmd.fold = true;
                }
                if (action == 1) {
                    cmd.check = true;
                }
                if (action == 2) {
                    cmd.callAll = true;
                }
                if (action == 4) {
                    cmd.allIn = true;
                }
                if (action == 3) {
                    cmd.raise = gameInfo.lastRaise + gameInfo.bigBlindMoney;
                }
                res = this.registerTakeTurn(gp.getUser(), cmd);
            }
            Debug.trace(new Object[]{"TU DONG CHOI **********************", gp.pInfo.nickName, " END **********************"});
        }
    }

    protected void tudongChoi() {
        GamePlayer gp = this.getPlayerByChair(this.gameMgr.currentChair);
        Debug.trace(new Object[]{"TU DONG CHOI =========================", gp.pInfo.nickName, " BEGIN ====================="});
        if (gp != null) {
            ++gp.countToOutRoom;
            LiengPlayerInfo pokerInfo = gp.spInfo.pokerInfo;
            pokerInfo.registerFold = true;
            this.changeTurn();
            Debug.trace(new Object[]{"TU DONG CHOI **********************", gp.pInfo.nickName, " END **********************"});
        }
    }

    protected void botAutoPlay() {
        if (!GameUtils.dev_mod) {
            return;
        }
        GamePlayer gp = this.getPlayerByChair(this.gameMgr.currentChair);
        Debug.trace(new Object[]{"TU DONG CHOI =========================", gp.pInfo.nickName, " BEGIN ====================="});
        if (gp != null && this.cheatTurns.size() == 0) {
            LiengPlayerInfo pokerInfo = gp.spInfo.pokerInfo;
            pokerInfo.registerFold = true;
            this.changeTurn();
            Debug.trace(new Object[]{"TU DONG CHOI **********************", gp.pInfo.nickName, " END **********************"});
        } else {
            Turn turn = this.cheatTurns.get(this.turnIndex % this.cheatTurns.size());
            ++this.turnIndex;
            LiengPlayerInfo pokerInfo = gp.spInfo.pokerInfo;
            pokerInfo.register(turn);
            this.changeTurn();
            Debug.trace(new Object[]{"TU DONG CHOI ********************************************", "**"});
        }
    }

    private void pCheatCards(User user, DataCmd dataCmd) {
        if (!GameUtils.isCheat) {
            return;
        }
        RevCheatCard cmd = new RevCheatCard(dataCmd);
        if (cmd.isCheat) {
            this.configGame(cmd.cards, cmd.moneyArray, cmd.chair);
        } else {
            this.gameMgr.gameTable.isCheat = false;
            this.gameMgr.gameTable.suit.initCard();
        }
    }

    private void configGame(byte[] cards, long[] moneyArray, int dealer) {
        this.gameMgr.gameTable.isCheat = true;
        this.gameMgr.gameTable.suit.setOrder(cards);
        this.gameMgr.gameTable.moneyArray = moneyArray;
        if (dealer >= 0 && dealer < 9) {
            this.gameMgr.gameTable.dealer = dealer;
        }
    }

    private void pDangKyChoiTiep(User user, DataCmd dataCmd) {
        GamePlayer gp = this.getPlayerByUser(user);
        if (gp != null) {
            gp.choiTiepVanSau = true;
        }
    }

    @Override
    public void onNoHu(ThongTinThangLon info) {
        this.thongTinNoHu = info;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected void notifyNoHu() {
        try {
            if (this.thongTinNoHu != null) {
                for (int i = 0; i < 9; ++i) {
                    GamePlayer gp = this.getPlayerByChair(i);
                    if (!gp.gameMoneyInfo.sessionId.equalsIgnoreCase(this.thongTinNoHu.moneySessionId) || !gp.gameMoneyInfo.nickName.equalsIgnoreCase(this.thongTinNoHu.nickName))
                        continue;
                    gp.gameMoneyInfo.currentMoney = this.thongTinNoHu.currentMoney;
                    break;
                }
                SendNoHu msg = new SendNoHu();
                msg.info = this.thongTinNoHu;
                for (Map.Entry entry : this.gameRoom.userManager.entrySet()) {
                    User u = (User) entry.getValue();
                    if (u == null) continue;
                    this.send(msg, u);
                }
            }
        } catch (Exception e) {
            CommonHandle.writeErrLog(e);
        } finally {
            this.thongTinNoHu = null;
        }
    }

    @Override
    public void choNoHu(String nickName) {
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (gp.getUser() == null || !gp.getUser().getName().equalsIgnoreCase(nickName)) continue;
            this.gameMgr.gameTable.suit.noHuAt(gp.chair);
        }
    }

    @Override
    protected Runnable getGameLoopTask() {
        return gameMgr::gameLoop;
    }

    protected void selectDealer() {
        LiengGameInfo pokerGameInfo = this.gameMgr.gameTable.pokerGameInfo;
        int from = pokerGameInfo.dealer % 9;
        if (this.gameMgr.gameTable.isCheat) {
            from = this.gameMgr.gameTable.dealer < 0 ? from : this.gameMgr.gameTable.dealer;
            for (int i = 0; i < 9; ++i) {
                GamePlayer gp = this.getPlayerByChair(i);
                if (!gp.isPlaying() || this.gameMgr.gameTable.moneyArray[i] == 0L) continue;
                gp.spInfo.pokerInfo.currentMoney = this.gameMgr.gameTable.moneyArray[i];
            }
        }
        int newDealer = -1;
        int newSmallBlind = -1;
        int newBigBlind = -1;
        int startChair = -1;
        int i = from;
        while (newDealer == -1) {
            int currentChair = i % 9;
            GamePlayer gp = this.getPlayerByChair(currentChair);
            if (gp.isPlaying() && newDealer == -1) {
                newDealer = currentChair;
                newSmallBlind = currentChair;
                newBigBlind = currentChair;
                startChair = currentChair;
                break;
            }
            ++i;
        }
        this.gameMgr.currentChair = newDealer;
        pokerGameInfo.dealer = newDealer;
        pokerGameInfo.smallBlind = newSmallBlind;
        pokerGameInfo.bigBlind = newBigBlind;
        pokerGameInfo.bigBlindMoney = 1L * this.gameRoom.setting.moneyBet;
        this.gameLog.append("SD<");
        this.gameLog.append("dl/").append(pokerGameInfo.dealer).append(";");
        this.gameLog.append("sb/").append(pokerGameInfo.smallBlind).append(";");
        this.gameLog.append("bb/").append(pokerGameInfo.bigBlind).append(">");
        this.initSmallAndBigBlind();
        this.notifyDealder();
    }

    private void initSmallAndBigBlind() {
        int from;
        Debug.trace(new Object[]{"============VAO CUOC DAU GAME:", "=================================="});
        LiengGameInfo pokerGameInfo = this.gameMgr.gameTable.pokerGameInfo;
        Round round = this.gameMgr.gameTable.getCurrentRound();
        for (int i = from = pokerGameInfo.dealer; i < 9 + from; ++i) {
            int chair = i % 9;
            GamePlayer gp = this.getPlayerByChair(chair);
            if (!gp.isPlaying()) continue;
            LiengPlayerInfo info = gp.spInfo.pokerInfo;
            info.registerMoney(1L * this.gameRoom.setting.moneyBet);
            Turn turn = info.take(pokerGameInfo, round, true);
            this.logTakeTurn(gp, turn);
        }
        Debug.trace(new Object[]{"===============================", "=================================="});
    }

    private void notifyDealder() {
        SendSelectDealer msg = new SendSelectDealer();
        LiengGameInfo pokerGameInfo = this.gameMgr.gameTable.pokerGameInfo;
        msg.smallBlind = pokerGameInfo.smallBlind;
        msg.bigBlind = pokerGameInfo.bigBlind;
        msg.dealer = pokerGameInfo.dealer;
        msg.gameId = this.gameMgr.gameTable.id;
        if (this.gameMgr.gameTable.isCheat) {
            msg.isCheat = true;
        }
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.hasUser()) continue;
            msg.hasInfoAtChair[i] = true;
            msg.gamePlayers[i] = gp;
            msg.moneyArray[i] = gp.spInfo.pokerInfo.currentMoney;
        }
        this.send(msg);
        this.gameMgr.gameAction = 1;
        this.gameMgr.countDown = 3;
    }

    protected void newRound() {
        Round round = this.gameMgr.gameTable.getCurrentRound();
        if (round.roundId == 0) {
            this.endGame();
        }
    }

    private int findFirstChairNewRound() {
        LiengGameInfo pokerGameInfo = this.gameMgr.gameTable.pokerGameInfo;
        int from = pokerGameInfo.smallBlind;
        int startChair = -1;
        int i = from;
        while (startChair == -1) {
            int currentChair = i % 9;
            GamePlayer gp = this.getPlayerByChair(currentChair);
            if (gp.isPlaying()) {
                LiengPlayerInfo info = gp.spInfo.pokerInfo;
                if (startChair == -1 && !info.fold && !info.allIn) {
                    startChair = currentChair;
                    break;
                }
            }
            ++i;
        }
        Debug.trace(new Object[]{"findFirstChairNewRound", startChair});
        return startChair;
    }

    private void gomTienVaoHu() {
        LiengGameInfo gameInfo = this.gameMgr.gameTable.pokerGameInfo;
        long totalBet = 0L;
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying()) continue;
            LiengPlayerInfo info = gp.spInfo.pokerInfo;
            totalBet += info.moneyBet;
            info.totalBet += info.moneyBet;
            info.clearNewRound();
        }
        gameInfo.clearNewRound();
    }

    private void notifyNewRound(Round round) {
        LiengGameInfo gameInfo = this.gameMgr.gameTable.pokerGameInfo;
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying()) continue;
            SendNewRound msg = new SendNewRound();
            msg.potMoney = gameInfo.potMoney;
            msg.roundId = round.roundId;
            msg.cards_name = (byte) gp.spInfo.handCards.kiemTraBo();
            this.send(msg, gp.getUser());
        }
        this.gameMgr.gameAction = 5;
        this.gameMgr.countDown = 1;
    }

    protected void changeTurn() {
        Turn turn = this.takeTurn();
        if (turn == null) {
            this.notifyChangeTurn();
        }
    }

    private void notifyChangeTurn() {
        this.gameMgr.countDown = 30;
        this.gameMgr.gameAction = 4;
        SendChangeTurn msg = new SendChangeTurn();
        Round round = this.gameMgr.gameTable.getCurrentRound();
        msg.roundId = round.roundId;
        msg.curentChair = this.gameMgr.currentChair;
        msg.countDownTime = this.gameMgr.countDown;
        Debug.trace(new Object[]{"notifyChangeTurn", GameUtils.toJsonString(msg)});
        this.send(msg);
    }

    private Turn takeTurn() {
        LiengGameInfo potInfo = this.gameMgr.gameTable.pokerGameInfo;
        Round round = this.gameMgr.gameTable.getCurrentRound();
        GamePlayer gp = this.getPlayerByChair(this.gameMgr.currentChair);
        LiengPlayerInfo pokerInfo = gp.spInfo.pokerInfo;
        Turn turn = pokerInfo.takeTurn(potInfo, round);
        if (turn != null) {
            pokerInfo.clearNewTurn();
            this.notifyTakeTurn(turn);
            if (this.checkEndGameByFoldAndAllIn()) {
                this.endGame();
            } else if (!this.checkNewRound()) {
                this.gameMgr.gameAction = 5;
                this.gameMgr.countDown = 1;
            }
        }
        return turn;
    }

    private void notifyTakeTurn(Turn turn) {
        GamePlayer gp = this.getPlayerByChair(this.gameMgr.currentChair);
        if (turn.action == 0) {
            gp.spInfo.pokerInfo.fold = true;
        }
        LiengGameInfo gameInfo = this.gameMgr.gameTable.pokerGameInfo;
        LiengPlayerInfo pokerInfo = gp.spInfo.pokerInfo;
        SendTakeTurn msg = new SendTakeTurn();
        msg.action = turn.action;
        msg.chair = this.gameMgr.currentChair;
        msg.currentBet = pokerInfo.moneyBet;
        msg.currentMoney = pokerInfo.currentMoney;
        msg.maxBet = gameInfo.maxBetMoney;
        msg.raiseAmount = turn.raiseAmount;
        msg.raiseStep = gameInfo.bigBlindMoney;
        msg.raiseBlock = gameInfo.raiseBlock = this.checkRaiseBlock();
        msg.potMoney = gameInfo.potMoney;
        Debug.trace(new Object[]{"notifyTakeTurn:", GameUtils.toJsonString(msg)});
        this.findNextChair();
        this.logTakeTurn(gp, turn);
        this.send(msg);
    }

    private boolean checkRaiseBlock() {
        int foldCount = 0;
        int allInCount = 0;
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying()) continue;
            if (gp.spInfo.pokerInfo.allIn) {
                ++allInCount;
                continue;
            }
            if (!gp.spInfo.pokerInfo.fold) continue;
            ++foldCount;
        }
        return allInCount + foldCount == this.playingCount - 1;
    }

    private void logTakeTurn(GamePlayer gp, Turn turn) {
        this.gameLog.append("TT<");
        this.gameLog.append(gp.chair).append(";");
        this.gameLog.append(turn.action).append(";-");
        this.gameLog.append(turn.raiseAmount);
        this.gameLog.append(">");
    }

    private boolean checkEndGameByFoldAndAllIn() {
        Debug.trace("checkEndGameByFoldAndAllIn");
        LiengGameInfo potInfo = this.gameMgr.gameTable.pokerGameInfo;
        int foldCount = 0;
        int allInCount = 0;
        int callCount = 0;
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying()) continue;
            if (gp.spInfo.pokerInfo.allIn) {
                ++allInCount;
                continue;
            }
            if (gp.spInfo.pokerInfo.fold) {
                ++foldCount;
                continue;
            }
            if (gp.spInfo.pokerInfo.moneyBet != potInfo.maxBetMoney) continue;
            ++callCount;
        }
        Round round = this.gameMgr.gameTable.getCurrentRound();
        if (allInCount == this.playingCount - foldCount || allInCount == this.playingCount - foldCount - 1 && callCount == 1) {
            Debug.trace(new Object[]{"Tat ca all in", "allInCount=", allInCount, "playingCount=", this.playingCount, "foldCount=", foldCount});
            return round.turns.size() >= 2 * this.playingCount - 1;
        }
        if (foldCount == this.playingCount - 1) {
            Debug.trace(new Object[]{"Tat ca up bai", "allInCount=", allInCount, "playingCount=", this.playingCount, "foldCount=", foldCount});
            return round.turns.size() >= 2 * this.playingCount - 1;
        }
        return false;
    }

    private boolean checkNewRound() {
        boolean flag;
        Round round = this.gameMgr.gameTable.getCurrentRound();
        LiengGameInfo potInfo = this.gameMgr.gameTable.pokerGameInfo;
        int allInCount = 0;
        int foldCount = 0;
        int callCount = 0;
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying()) continue;
            LiengPlayerInfo info = gp.spInfo.pokerInfo;
            if (info.fold) {
                ++foldCount;
                continue;
            }
            if (info.allIn) {
                ++allInCount;
                continue;
            }
            if (info.moneyBet != potInfo.maxBetMoney) continue;
            ++callCount;
        }
        int size = round.turns.size();
        boolean bl = flag = size >= 2 * this.playingCount;
        if (flag && allInCount + foldCount + callCount == this.playingCount) {
            this.gameMgr.gameAction = 3;
            this.gameMgr.countDown = 1;
            return true;
        }
        return false;
    }

    private void findNextChair() {
        int from;
        for (int i = from = this.gameMgr.currentChair + 1; i < 9 + from; ++i) {
            int currentChair = i % 9;
            GamePlayer gp = this.getPlayerByChair(currentChair);
            if (!gp.isPlaying() || gp.spInfo.pokerInfo.fold || gp.spInfo.pokerInfo.allIn) continue;
            this.gameMgr.currentChair = currentChair;
            Debug.trace(new Object[]{"Find next chair", this.gameMgr.currentChair});
            break;
        }
    }

    @Override
    public String toString() {
        try {
            JSONObject json = this.toJONObject();
            if (json != null) {
                return json.toString();
            }
            return "{}";
        } catch (Exception e) {
            return "{}";
        }
    }

    @Override
    public JSONObject toJONObject() {
        try {
            JSONObject json = new JSONObject();
            json.put("gameState", this.gameMgr.getGameStateId());
            json.put("gameAction", this.gameMgr.gameAction);
            JSONArray arr = new JSONArray();
            for (int i = 0; i < 9; ++i) {
                GamePlayer gp = this.getPlayerByChair(i);
                arr.put(gp.toJSONObject());
            }
            json.put("players", arr);
            return json;
        } catch (Exception e) {
            return null;
        }
    }
}
