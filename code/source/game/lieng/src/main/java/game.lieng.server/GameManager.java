package game.lieng.server;

import game.lieng.server.cmd.send.SendUpdateAutoStart;
import game.lieng.server.logic.GameTable;
import game.lieng.server.logic.GroupCard;

import java.util.List;

public class GameManager {

    public GameManager(LiengGameServer gameServer) {
        this.gameServer = gameServer;
    }

    private enum GameState {
        GS_NO_START(0), GS_GAME_PLAYING(1), GS_GAME_END(3);

        int id;

        GameState(int i) {
            id = i;
        }
    }

    protected static final int DEM_LA = 1;
    protected static final int NO_ACTION = -1;
    protected static final int SELECT_DEALER = 0;
    protected static final int DEAL_PRIVATE_CARD = 1;
    protected static final int DEAL_COMMUNITY_CARD = 2;
    protected static final int CHANGE_ROUND = 3;
    protected static final int IN_ROUND = 4;
    protected static final int CHANGE_TURN = 5;

    protected int roomOwnerChair = 9;
    protected int roomCreatorUserId;
    protected int rCuocLon = 1;
    private volatile GameState gameState = GameState.GS_NO_START;
    protected volatile int gameAction = -1;
    protected volatile int countDown = 0;
    protected volatile boolean isAutoStart = false;
    protected volatile int currentChair = -1;
    protected volatile int lastRaiseChair = -1;
    public GameTable gameTable = new GameTable();
    protected final LiengGameServer gameServer;
    protected GameLogic logic = new GameLogic();

    protected boolean isGameStateNoStart() {
        return this.gameState == GameState.GS_NO_START;
    }

    protected boolean isGameStatePlaying() {
        return this.gameState == GameState.GS_GAME_PLAYING;
    }

    protected boolean isGameStateEnd() {
        return this.gameState == GameState.GS_GAME_END;
    }

    protected int getGameStateId() {
        return this.gameState.id;
    }

    protected void setGameStateEndGame() {
        this.gameState = GameState.GS_GAME_END;
    }

    protected void setGameStateNoStart() {
        this.gameState = GameState.GS_NO_START;
    }

    protected void prepareNewGame() {
        this.gameTable.reset();
        this.isAutoStart = false;
        this.gameServer.kiemTraTuDongBatDau(2);
    }

    protected void gameLoop() {
        synchronized (gameServer) {
            --this.countDown;

            if (this.gameState == GameState.GS_NO_START) {
                if (this.isAutoStart) {
                    if (this.countDown <= 0) {
                        this.gameState = GameState.GS_GAME_PLAYING;
                        this.gameServer.start();
                    }
                    return;
                }

                if (!this.isAutoStart) {
                    if (this.gameServer.playerCount > 1) {
                        this.gameServer.kiemTraTuDongBatDau(2);
                    }
                }
                return;
            }


            if (this.gameState == GameState.GS_GAME_PLAYING) {
                if (this.countDown <= 0) {
                    if (this.gameAction == 0) {
                        this.gameServer.selectDealer();
                    } else if (this.gameAction == 1) {
                        this.chiaBai();
                    } else if (this.gameAction == 3) {
                        this.newRound();
                    } else if (this.gameAction == 5) {
                        this.changeTurn();
                    } else if (this.gameAction == 4) {
                        this.tudongChoi();
                    }
                }
                return;
            }

            if (this.gameState == GameState.GS_GAME_END) {
                if (this.countDown == 5) {
                    this.gameServer.notifyNoHu();
                }
                if (this.countDown <= 0) {
                    this.gameServer.pPrepareNewGame();
                }
            }
        }
    }

    protected void notifyAutoStartToUsers(int after) {
        SendUpdateAutoStart msg = new SendUpdateAutoStart();
        msg.isAutoStart = this.isAutoStart;
        msg.autoStartTime = (byte) after;
        this.gameServer.send(msg);
    }

    protected void newRound() {
        this.gameServer.newRound();
    }

    protected void cancelAutoStart() {
        this.isAutoStart = false;
        this.notifyAutoStartToUsers(0);
    }

    protected void makeAutoStart(int after) {
        if (this.gameState != GameState.GS_NO_START) {
            return;
        }

        if (!this.isAutoStart) {
            this.countDown = after;
        } else if (after < this.countDown) {
            this.countDown = after;
        } else {
            after = this.countDown;
        }

        this.isAutoStart = true;
        this.notifyAutoStartToUsers(after);
    }

    private void chiaBai() {
        List<GroupCard> cards = this.gameTable.suit.dealCards();
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.gameServer.playerList.get(i);
            gp.addCards(cards.get(i));
        }
        this.gameAction = 5;
        this.countDown = (int) Math.ceil(0.5 * (double) this.gameServer.playingCount) + 7;
        this.gameServer.chiabai();
    }

    private void tudongChoi() {
        this.gameServer.tudongChoi();
    }

    private void botPlay() {
        this.gameServer.botAutoPlay();
    }

    private void changeTurn() {
        this.gameServer.changeTurn();
    }

    protected int currentChair() {
        return this.currentChair;
    }

    protected boolean canOutRoom() {
        return this.isGameStateNoStart();
    }
}
