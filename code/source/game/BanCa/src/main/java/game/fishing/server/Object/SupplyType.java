package game.fishing.server.Object;

public enum SupplyType {
    EST_Gold(0),
    EST_Cold(1),
    EST_YuanBao(2),
    EST_Laser(3),
    EST_Speed(4),
    EST_Bignet(5),
    EST_Electric(6),
    EST_Bomb(7),
    EST_NULL(8);

    private int type;

    SupplyType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
