package casio.king365.mqtt.listener;

import casio.king365.service.MinigameUserService;
import casio.king365.util.KingUtil;
import com.rabbitmq.client.Channel;
import com.vinplay.vbee.common.messages.BaseMessage;
import com.vinplay.vbee.common.messages.UserNicknameMessage;
import org.apache.log4j.Logger;

public class KickUserListener extends BaseConsumer {

    private static final Logger logger = Logger.getLogger("api");

    MinigameUserService minigameUserService = new MinigameUserService();

    public KickUserListener(Channel channel) {
        super(channel);
    }

    @Override
    public void process(BaseMessage msg) {
        try {
            UserNicknameMessage userNicknameMessage = (UserNicknameMessage) msg;
            KingUtil.printLog("KickUserListener kickUserMessage.getUserNickname(): "+ userNicknameMessage.getUserNickname());
            minigameUserService.kickUser(userNicknameMessage.getUserNickname());
        } catch (Exception e) {
            logger.debug("KickUserListener exception: " + KingUtil.printException(e));
            e.printStackTrace();
        }
    }
}
