package casio.king365.mqtt.listener;

import casio.king365.mqtt.SimpleBaseMessage;
import casio.king365.service.MessageToUserService;
import com.rabbitmq.client.Channel;
import com.vinplay.vbee.common.messages.BaseMessage;
import casio.king365.dto.UpdateUserMoneyMsg;
import org.apache.log4j.Logger;
import org.json.JSONObject;

public class UpdateUserMoneyListener extends BaseConsumer {

    private static final Logger logger = Logger.getLogger("api");

    public UpdateUserMoneyListener(Channel channel) {
        super(channel);
    }

    @Override
    public void process(BaseMessage msg) {
        try {
            SimpleBaseMessage simpleBaseMessage = (SimpleBaseMessage) msg;
            JSONObject obj = new JSONObject(simpleBaseMessage.jsonContent);
            logger.debug("UpdateUserMoneyListener receive content: " + obj.toString());
            String userNickname = obj.getString("user_nickname");
            int totalMoney = obj.getInt("total_money");
            int changeMoney = obj.getInt("change_money");
            String desc = obj.getString("desc");
            UpdateUserMoneyMsg updateUserMoneyMsg = new UpdateUserMoneyMsg();
            updateUserMoneyMsg.userNickname = userNickname;
            updateUserMoneyMsg.totalMoney = totalMoney;
            updateUserMoneyMsg.changeMoney = changeMoney;
            updateUserMoneyMsg.desc = desc;
            (new MessageToUserService()).sendMsgToUserNickname(updateUserMoneyMsg, userNickname);
            logger.debug("UpdateUserMoneyListener after sendMsgToUserId");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
