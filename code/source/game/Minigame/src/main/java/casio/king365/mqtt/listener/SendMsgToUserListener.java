package casio.king365.mqtt.listener;

import casio.king365.service.MessageToUserService;
import casio.king365.util.KingUtil;
import com.rabbitmq.client.Channel;
import com.vinplay.vbee.common.messages.BaseMessage;
import com.vinplay.vbee.common.messages.ObjToUserNickNameMessage;
import game.BaseMsgEx;
import org.apache.log4j.Logger;

public class SendMsgToUserListener extends BaseConsumer {

    private static final Logger logger = Logger.getLogger("api");

    public SendMsgToUserListener(Channel channel) {
        super(channel);
    }

    @Override
    public void process(BaseMessage msg) {
        try {
            ObjToUserNickNameMessage baseMessageContainer = (ObjToUserNickNameMessage) msg;
            Object message = baseMessageContainer.getMessage();
            if (message == null
                    || !(message instanceof BaseMsgEx)) {
                logger.warn("SendMsgToNickname() ObjToUserNickNameMessage baseMessage.message is null or not BaseMsgEx: " + baseMessageContainer.getUserNickname()
                        + "data 2: " + baseMessageContainer.getMessage());
            }

            StringBuilder sb = new StringBuilder();
            for (byte b : ((BaseMsgEx) baseMessageContainer.getMessage()).createData()) {
                sb.append(" ");
                sb.append(b);
            }
            logger.info("SendMsgToNickname() ObjToUserNickNameMessage baseMessage.userNickname: " + baseMessageContainer.getUserNickname()
                    + "data 2: " + sb.toString());
            (new MessageToUserService()).sendMsgToUserNickname((BaseMsgEx) baseMessageContainer.getMessage(), baseMessageContainer.getUserNickname());

        } catch (Exception e) {
            logger.debug("SendMsgToUserListener exception: " + KingUtil.printException(e));
            e.printStackTrace();
        }
    }
}
