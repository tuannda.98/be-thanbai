package casio.king365;

import bitzero.server.BitZeroServer;
import casio.king365.mqtt.MqttRegisterListener;
import com.vinplay.vbee.common.utils.DateTimeUtils;
import game.web.filter.CorsFilter;
import org.apache.log4j.Logger;
import org.eclipse.jetty.annotations.AnnotationConfiguration;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.webapp.Configuration;
import org.eclipse.jetty.webapp.WebAppContext;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.hazelcast.HazelcastAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import javax.servlet.DispatcherType;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

// Main
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, HazelcastAutoConfiguration.class})
@ComponentScan({"game", "bitzero", "casio", "com.vinplay", "extension"})
public class MiniGameMain extends SpringBootServletInitializer {

    private static final Logger logger = Logger.getLogger("api");
    private static int API_PORT = 8081;
    private static ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(2);

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {

            System.out.println("Let's inspect the beans provided by Spring Boot:");

            String[] beanNames = ctx.getBeanDefinitionNames();
            Arrays.sort(beanNames);
            for (String beanName : beanNames) {
                System.out.println(beanName);
            }

        };
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(MiniGameMain.class);
    }

    public static void main(String[] args) {
        try {
            System.out.println("KING365 START BITZERO MAIN");
            logger.debug("KING365 START BITZERO MAIN");

            SpringApplication.run(MiniGameMain.class);

            bitzero.server.Main.init(args);
            System.out.println("KING365 FINISH START BITZERO MAIN");
            logger.debug("KING365 FINISH START BITZERO MAIN");

            // REGISTER RABBITMQ LISTERNER
            System.out.println("REGISTER RABBITMQ LISTERNER");
            logger.debug("REGISTER RABBITMQ LISTERNER");
            MqttRegisterListener.RegisterListener();
            System.out.println("FINISH REGISTER RABBITMQ LISTERNER");
            logger.debug("FINISH REGISTER RABBITMQ LISTERNER");
            scheduledThreadPoolExecutor.scheduleAtFixedRate(() -> {
                ScheduledThreadPoolExecutor poolExecutor = BitZeroServer.getInstance().getTaskScheduler().getScheduledThreadPoolExecutor();
                logger.info("Current bz server task scheduler core pool size: " + poolExecutor.getCorePoolSize()
                        + " \n\t maximum pool size " + poolExecutor.getMaximumPoolSize()
                        + " \n\t active count " + poolExecutor.getActiveCount());
                if (poolExecutor.getCorePoolSize() * 2 >= poolExecutor.getMaximumPoolSize()) {
                    logger.warn("There are much scheduler: " + poolExecutor.getQueue().stream().map(t -> t.toString()).collect(Collectors.joining("\n\t")));
                    GU.sendOperation("There are much scheduler: " + poolExecutor.getQueue().stream().map(t -> t.toString()).collect(Collectors.joining("\n\t")));
                }
            }, 31, 5 * 59, TimeUnit.SECONDS);

            Server server = new Server();
            ServerConnector connector = new ServerConnector(server);
            connector.setPort(API_PORT);
            connector.setIdleTimeout(30000L);

            WebAppContext webAppContext = new WebAppContext();
            webAppContext.setConfigurations(new Configuration[]{
                    new AnnotationConfiguration()});
            webAppContext.setParentLoaderPriority(true);
            webAppContext.addFilter(CorsFilter.class, "/*", EnumSet.of(DispatcherType.REQUEST));

            server.setHandler(webAppContext);
            server.setConnectors(new Connector[]{connector});
            server.start();
            logger.info((Object) "Minigame SERVER Started ...!!!");
//            startJavaMelodyEmbedded();
            server.join();
            GU.sendOperation("MiniGame: Started at " + DateTimeUtils.getCurrentTime("HH-mm-ss yyyy-MM-dd"));
        } catch (Exception var10) {
            logger.info("MINIGAME SERVER Start error: " + var10.getMessage());
            var10.printStackTrace();
        }
    }
}
