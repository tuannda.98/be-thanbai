package casio.king365.service;

import bitzero.server.BitZeroServer;
import bitzero.server.api.IBZApi;
import bitzero.server.api.response.ResponseApi;
import bitzero.server.entities.User;
import bitzero.server.entities.managers.IUserManager;
import bitzero.server.extensions.data.BaseMsg;
import org.apache.log4j.Logger;

import java.util.List;

public class MessageToUserService {

    private static final Logger logger = Logger.getLogger("api");
    protected final BitZeroServer bz = BitZeroServer.getInstance();
    protected final ResponseApi resApi;
    protected final IBZApi bzApi;
    protected final IUserManager globalUserManager;

    public MessageToUserService() {
        this.resApi = this.bz.getAPIManager().getResApi();
        this.bzApi = this.bz.getAPIManager().getBzApi();
        globalUserManager = bz.getUserManager();
    }

    public void sendMsgToUserNickname(BaseMsg msg, String nickname){
        List<User> listU = globalUserManager.getAllUsers();
        if(listU == null) {
            return;
        }
        User u = globalUserManager.getUserByName(nickname);
        if(u == null) {
            return;
        }
        this.resApi.sendExtResponse(msg, u);
    }
}
