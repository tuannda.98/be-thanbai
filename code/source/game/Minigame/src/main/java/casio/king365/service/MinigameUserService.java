package casio.king365.service;

import bitzero.server.BitZeroServer;
import bitzero.server.api.IBZApi;
import bitzero.server.api.response.ResponseApi;
import bitzero.server.entities.User;
import bitzero.server.entities.managers.IUserManager;
import bitzero.server.extensions.data.BaseMsg;
import bitzero.server.util.ClientDisconnectionReason;
import org.apache.log4j.Logger;

import java.util.List;

public class MinigameUserService {

    private static final Logger logger = Logger.getLogger("api");
    protected final BitZeroServer bz = BitZeroServer.getInstance();
    protected final ResponseApi resApi;
    protected final IBZApi bzApi;
    protected final IUserManager globalUserManager;

    public MinigameUserService() {
        this.resApi = this.bz.getAPIManager().getResApi();
        this.bzApi = this.bz.getAPIManager().getBzApi();
        globalUserManager = bz.getUserManager();
    }

    public void kickUser(String nickname){
        User u = globalUserManager.getUserByName(nickname);
        if(u == null) {
            return;
        }
        u.disconnect(ClientDisconnectionReason.KICK);
    }
}
