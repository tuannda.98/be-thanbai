// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.gameRoom.entities;

import bitzero.server.core.BZEvent;
import bitzero.server.entities.User;
import bitzero.util.ExtensionUtility;
import bitzero.util.common.business.Debug;
import game.eventHandlers.GameEventParam;
import game.eventHandlers.GameEventType;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingDeque;

import static game.modules.gameRoom.GameRoomModule.GAME_ROOM;

public class GameRoomGroup {
    public final GameRoomSetting setting;
    public final BlockingDeque<GameRoom> emptyRooms;
    public final BlockingDeque<GameRoom> freeRooms;
    public final BlockingDeque<GameRoom> busyRooms;
    public final Map<Integer, User> userManager;
    public int randomUserInitial;

    public GameRoomGroup(final GameRoomSetting setting) {
        this.emptyRooms = new LinkedBlockingDeque<>();
        this.freeRooms = new LinkedBlockingDeque<>();
        this.busyRooms = new LinkedBlockingDeque<>();
        this.userManager = new ConcurrentHashMap<>();
        this.randomUserInitial = 0;
        final Random rd = new Random();
        this.setting = setting;
        this.randomUserInitial = Math.abs(rd.nextInt() % 1000 + 100);
    }

    public int joinRoom(final User user) {
        GameRoom room = user.getProperty(GAME_ROOM);
        if (room != null) {
            return 1;
        }
        room = this.getCanJoinRoom();
        this.joinRoom(user, room, false);
        return 0;
    }

    public int leaveRoom(final User user) {
        final GameRoom room = user.getProperty(GAME_ROOM);
        if (room == null) {
            Debug.trace(user.getName(), "leave room null");
            return 1;
        }
        this.leaveRoom(user, room);
        return 0;
    }

    public void joinRoom(final User user, final GameRoom room, final boolean isReconnect) {
        this.userManager.put(user.getId(), user);
        user.setProperty(GAME_ROOM, room);
        this.recycleRoom(room);
        this.dispatchEventJoinRoom(user, room, isReconnect);
        Debug.trace(user.getName(), "join room", room.getId());
    }

    public void leaveRoom(final User user, final GameRoom room) {
        this.userManager.remove(user.getId());
        this.recycleRoom(room);
        this.dispatchEventLeaveRoom(user, room);
        Debug.trace(user.getName(), "leave room", room.getId());
    }

    public GameRoom getCanJoinRoom() {
        GameRoom room = this.freeRooms.poll();
        if (room == null) {
            room = this.emptyRooms.poll();
        }
        if (room == null) {
            room = GameRoomManager.instance().createNewGameRoom(this.setting);
            room.group = this;
        }
        return room;
    }

    private void recycleRoom(final GameRoom room) {
        if (room.getUserCount() == 0) {
            this.emptyRooms.add(room);
            this.freeRooms.remove(room);
            this.busyRooms.remove(room);
        } else if (room.getUserCount() == this.setting.maxUserPerRoom) {
            this.busyRooms.add(room);
            this.freeRooms.remove(room);
            this.emptyRooms.remove(room);
        } else {
            this.freeRooms.add(room);
            this.emptyRooms.remove(room);
            this.busyRooms.remove(room);
        }
    }

    private void dispatchEventJoinRoom(final User user, final GameRoom room, final boolean isReconnect) {
        final HashMap<GameEventParam, Object> evtParams = new HashMap<>();
        evtParams.put(GameEventParam.GAMEROOM, room);
        evtParams.put(GameEventParam.USER, user);
        evtParams.put(GameEventParam.IS_RECONNECT, isReconnect);
        ExtensionUtility.dispatchEvent(new BZEvent(GameEventType.GAME_ROOM_USER_JOIN, evtParams));
    }

    private void dispatchEventLeaveRoom(final User user, final GameRoom room) {
        final Map<GameEventParam, Object> evtParams = new HashMap<>();
        evtParams.put(GameEventParam.GAMEROOM, room);
        evtParams.put(GameEventParam.USER, user);
        ExtensionUtility.dispatchEvent(new BZEvent(GameEventType.GAME_ROOM_USER_LEAVE, evtParams));
    }

    int getUserCount() {
        return this.userManager.size() + this.randomUserInitial;
    }
}
