// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame;

public enum GroupItemType
{
    THREE_YELLOW, 
    THREE_VIOLET, 
    THREE_BLUE, 
    THREE_WHITE, 
    THREE_GREEN, 
    DOUBLE_KIND, 
    TWO_GREEN, 
    THREE_RED, 
    TWO_RED, 
    FAIL;
}
