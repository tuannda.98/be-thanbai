// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.rev.minipoker;

import java.nio.ByteBuffer;
import bitzero.server.extensions.data.DataCmd;
import bitzero.server.extensions.data.BaseCmd;

public class AutoPlayMiniPokerCmd extends BaseCmd
{
    public byte autoPlay;
    
    public AutoPlayMiniPokerCmd(final DataCmd dataCmd) {
        super(dataCmd);
        this.unpackData();
    }
    
    public void unpackData() {
        final ByteBuffer bf = this.makeBuffer();
        this.autoPlay = bf.get();
    }
}
