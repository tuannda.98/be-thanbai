// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.send.caothap;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class ResultCaoThapMsg extends BaseMsgEx
{
    public byte card;
    public long money1;
    public long money2;
    public long money3;
    
    public ResultCaoThapMsg() {
        super(6002);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        bf.put(this.card);
        bf.putLong(this.money1);
        bf.putLong(this.money2);
        bf.putLong(this.money3);
        return this.packBuffer(bf);
    }
}
