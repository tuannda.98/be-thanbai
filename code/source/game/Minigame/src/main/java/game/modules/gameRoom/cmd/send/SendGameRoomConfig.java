// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.gameRoom.cmd.send;

import java.util.List;
import java.nio.ByteBuffer;
import game.modules.gameRoom.entities.GameRoomSetting;
import game.modules.gameRoom.entities.GameRoomManager;
import game.BaseMsgEx;

public class SendGameRoomConfig extends BaseMsgEx
{
    public SendGameRoomConfig() {
        super(3003);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        final List<GameRoomSetting> gameRoomSettingList = GameRoomManager.instance().roomSettingList;
        final int size = gameRoomSettingList.size();
        bf.putShort((short)size);
        for (final GameRoomSetting setting : gameRoomSettingList) {
            bf.put((byte) setting.maxUserPerRoom);
            bf.put((byte) setting.moneyType);
            bf.putLong(setting.moneyBet);
            bf.putLong(setting.requiredMoney);
            bf.putInt(GameRoomManager.instance().getUserCount(setting));
        }
        return this.packBuffer(bf);
    }
}
