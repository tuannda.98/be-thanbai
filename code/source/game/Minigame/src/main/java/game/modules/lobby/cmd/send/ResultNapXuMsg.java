// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.lobby.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class ResultNapXuMsg extends BaseMsgEx
{
    public long currentMoneyVin;
    public long currentMoneyXu;
    
    public ResultNapXuMsg() {
        super(20031);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        bf.putLong(this.currentMoneyVin);
        bf.putLong(this.currentMoneyXu);
        return this.packBuffer(bf);
    }
}
