// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.gameRoom.entities;

import game.modules.gameRoom.cmd.rev.JoinGameRoomCmd;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;
import bitzero.util.common.business.CommonHandle;
import game.modules.gameRoom.config.GameRoomConfig;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;

public class GameRoomManager
{
    private static GameRoomManager ins;
    public List<GameRoomSetting> roomSettingList;
    public ConcurrentHashMap<String, GameRoomGroup> gameRoomGroups;
    public ConcurrentHashMap<Integer, GameRoom> allGameRooms;
    
    public static GameRoomManager instance() {
        if (GameRoomManager.ins == null) {
            GameRoomManager.ins = new GameRoomManager();
        }
        return GameRoomManager.ins;
    }
    
    private GameRoomManager() {
        this.roomSettingList = new ArrayList<>();
        this.gameRoomGroups = new ConcurrentHashMap<>();
        this.allGameRooms = new ConcurrentHashMap<>();
        this.init();
    }
    
    public void init() {
        final JSONObject config = GameRoomConfig.instance().config;
        try {
            final JSONArray roomConfigList = config.getJSONArray("roomList");
            for (int i = 0; i < roomConfigList.length(); ++i) {
                final JSONObject roomConfig = roomConfigList.getJSONObject(i);
                final GameRoomSetting setting = new GameRoomSetting(roomConfig);
                this.roomSettingList.add(setting);
                final GameRoomGroup group = new GameRoomGroup(setting);
                this.gameRoomGroups.put(setting.getSettingName(), group);
            }
        }
        catch (JSONException e) {
            CommonHandle.writeErrLog(e);
        }
    }
    
    public GameRoomGroup getGroupBySetting(final GameRoomSetting setting) {
        return this.gameRoomGroups.get(setting.getSettingName());
    }
    
    public GameRoom getGameRoomById(final int roomId) {
        return this.allGameRooms.get(roomId);
    }
    
    public GameRoom createNewGameRoom(final GameRoomSetting setting) {
        final GameRoom room = new GameRoom(setting);
        this.allGameRooms.put(room.getId(), room);
        return room;
    }
    
    public GameRoomGroup getGroup(final JoinGameRoomCmd cmd) {
        final GameRoomSetting setting = new GameRoomSetting(cmd);
        return this.getGroupBySetting(setting);
    }
    
    public int getUserCount(final GameRoomSetting setting) {
        return this.getGroupBySetting(setting).getUserCount();
    }
    
    static {
        GameRoomManager.ins = null;
    }
}
