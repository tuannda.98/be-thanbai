package game.modules.minigame;

import bitzero.server.BitZeroServer;
import bitzero.server.core.BZEventParam;
import bitzero.server.core.BZEventType;
import bitzero.server.core.IBZEvent;
import bitzero.server.entities.User;
import bitzero.server.exceptions.BZException;
import bitzero.server.extensions.BaseClientRequestHandler;
import bitzero.server.extensions.data.DataCmd;
import bitzero.util.common.business.Debug;
import com.vinplay.dal.service.CacheService;
import com.vinplay.dal.service.MiniGameService;
import com.vinplay.dal.service.PokeGoService;
import com.vinplay.dal.service.impl.CacheServiceImpl;
import com.vinplay.dal.service.impl.MiniGameServiceImpl;
import com.vinplay.dal.service.impl.PokeGoServiceImpl;
import com.vinplay.vbee.common.enums.Games;
import com.vinplay.vbee.common.exceptions.KeyNotFoundException;
import com.vinplay.vbee.common.utils.CommonUtils;
import game.modules.minigame.cmd.rev.pokego.*;
import game.modules.minigame.cmd.send.pokego.PokeGoX2Msg;
import game.modules.minigame.entities.BotMinigame;
import game.modules.minigame.room.MGRoom;
import game.modules.minigame.room.MGRoomPokeGo;
import game.modules.minigame.utils.MiniGameUtils;
import game.modules.minigame.utils.PokeGoUtils;
import game.utils.ConfigGame;
import game.utils.GameUtils;
import vn.yotel.yoker.util.Util;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class PokeGoModule extends BaseClientRequestHandler {
    private static Runnable pokeGoX2Task;
    private static Map<String, MGRoom> rooms;
    private final MiniGameService service;
    private final PokeGoService pgService;
    private static long referenceId;
    private static String ngayX2;
    private int countBot100;
    private int countBot1000;
    private int countBot10000;
    private final String fullLines;
    protected CacheService sv;

    public PokeGoModule() {
        this.service = new MiniGameServiceImpl();
        this.pgService = new PokeGoServiceImpl();
        this.countBot100 = 0;
        this.countBot1000 = 0;
        this.countBot10000 = 0;
        this.fullLines = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20";
        this.sv = new CacheServiceImpl();
    }

    public void init() {
        super.init();
        long[] pots = new long[6];
        long[] funds = new long[6];
        final int[] initPotValues = new int[6];
        try {
            final String initPotValuesStr = ConfigGame.getValueString("poke_go_init_pot_values");
            final String[] arr = initPotValuesStr.split(",");
            for (int i = 0; i < arr.length; ++i) {
                initPotValues[i] = Integer.parseInt(arr[i]);
            }
            pots = this.service.getPots(Games.POKE_GO.getName());
            Debug.trace("POKEGO POTS: " + CommonUtils.arrayLongToString(pots));
            funds = this.service.getFunds(Games.POKE_GO.getName());
            Debug.trace("POKEGO FUNDS: " + CommonUtils.arrayLongToString(funds));
        } catch (Exception e) {
            Debug.trace("Init POKE GO error ", e.getMessage());
        }
        PokeGoModule.rooms.put(Games.POKE_GO.getName() + "_vin_100", new MGRoomPokeGo(Games.POKE_GO.getName() + "_vin_100", (short) 1, pots[0], funds[0], 100, initPotValues[0]));
        PokeGoModule.rooms.put(Games.POKE_GO.getName() + "_vin_1000", new MGRoomPokeGo(Games.POKE_GO.getName() + "_vin_1000", (short) 1, pots[1], funds[1], 1000, initPotValues[1]));
        PokeGoModule.rooms.put(Games.POKE_GO.getName() + "_vin_10000", new MGRoomPokeGo(Games.POKE_GO.getName() + "_vin_10000", (short) 1, pots[2], funds[2], 10000, initPotValues[2]));
        PokeGoModule.rooms.put(Games.POKE_GO.getName() + "_xu_1000", new MGRoomPokeGo(Games.POKE_GO.getName() + "_xu_1000", (short) 0, pots[3], funds[3], 1000, initPotValues[3]));
        PokeGoModule.rooms.put(Games.POKE_GO.getName() + "_xu_10000", new MGRoomPokeGo(Games.POKE_GO.getName() + "_xu_10000", (short) 0, pots[4], funds[4], 10000, initPotValues[4]));
        PokeGoModule.rooms.put(Games.POKE_GO.getName() + "_xu_100000", new MGRoomPokeGo(Games.POKE_GO.getName() + "_xu_100000", (short) 0, pots[5], funds[5], 100000, initPotValues[5]));
        Debug.trace("INIT POKEGO DONE");
        this.getParentExtension().addEventListener(BZEventType.USER_DISCONNECT, this);
        PokeGoModule.referenceId = this.pgService.getLastReferenceId();
        Debug.trace("START POKE_GO REFERENCE ID= " + PokeGoModule.referenceId);
        final CacheServiceImpl sv = new CacheServiceImpl();
        try {
            sv.removeKey("poke_go_last_day_x2");
        } catch (KeyNotFoundException e2) {
            Debug.trace("KEY NOT FOUND");
        }
        PokeGoModule.ngayX2 = MiniGameUtils.calculateTimeX2AsString(PokeGoUtils.getX2Days(), PokeGoUtils.getLastDayX2(), "poke_go_time_x2");
        final int nextX2Time = MiniGameUtils.calculateTimeX2(PokeGoUtils.getX2Days(), PokeGoUtils.getLastDayX2(), "poke_go_time_x2");
        Debug.trace("POKEGO: Ngay X2: " + PokeGoModule.ngayX2 + ", remain time = " + nextX2Time);
        if (nextX2Time >= 0) {
            BitZeroServer.getInstance().getTaskScheduler().schedule(PokeGoModule.pokeGoX2Task, nextX2Time, TimeUnit.SECONDS);
        } else {
            startX2();
        }
        TimerTask gameLoopTask = new TimerTask() {
            @Override
            public void run() {
                gameLoop();
            }
        };
        new Timer().scheduleAtFixedRate(gameLoopTask, Util.randInt(5_000, 10_000), 1_000);
    }

    public static long getNewRefenceId() {
        return ++PokeGoModule.referenceId;
    }

    public static void startX2() {
        PokeGoModule.ngayX2 = "";
        final PokeGoX2Msg msg = new PokeGoX2Msg();
        msg.ngayX2 = PokeGoModule.ngayX2;
        for (final MGRoom room : PokeGoModule.rooms.values()) {
            room.sendMessageToRoom(msg);
        }
    }

    public static void stopX2() {
        PokeGoModule.ngayX2 = MiniGameUtils.calculateTimeX2AsString(PokeGoUtils.getX2Days(), PokeGoUtils.getLastDayX2(), "poke_go_time_x2");
        final PokeGoX2Msg msg = new PokeGoX2Msg();
        msg.ngayX2 = PokeGoModule.ngayX2;
        for (final MGRoom room : PokeGoModule.rooms.values()) {
            room.sendMessageToRoom(msg);
        }
        final Calendar cal = Calendar.getInstance();
        final int today = cal.get(Calendar.DAY_OF_WEEK);
        PokeGoUtils.saveLastDayX2(today);
        final int lastDayX2 = PokeGoUtils.getLastDayX2();
        final int nextX2Time = MiniGameUtils.calculateTimeX2(PokeGoUtils.getX2Days(), lastDayX2, "poke_go_time_x2");
        BitZeroServer.getInstance().getTaskScheduler().schedule(PokeGoModule.pokeGoX2Task, nextX2Time, TimeUnit.SECONDS);
    }

    public void handleServerEvent(final IBZEvent ibzevent) throws BZException {
        if (ibzevent.getType() == BZEventType.USER_DISCONNECT) {
            final User user = (User) ibzevent.getParameter(BZEventParam.USER);
            this.userDis(user);
        }
    }

    private void userDis(final User user) {
        final MGRoomPokeGo room = (MGRoomPokeGo) user.getProperty("MGROOM_POKEGO_INFO");
        if (room != null) {
            room.quitRoom(user);
            room.stopAutoPlay(user);
        }
    }

    public void handleClientRequest(final User user, final DataCmd dataCmd) {
        switch (dataCmd.getId()) {
            case 7003: {
                this.subScribePokeGo(user, dataCmd);
                break;
            }
            case 7004: {
                this.unSubScribePokeGo(user, dataCmd);
                break;
            }
            case 7005: {
                this.changeRoom(user, dataCmd);
                break;
            }
            case 7006: {
                if (GameUtils.disablePlayMiniGame(user)) {
                    return;
                }
                this.autoPlay(user, dataCmd);
                break;
            }
            case 7001: {
                if (GameUtils.disablePlayMiniGame(user)) {
                    return;
                }
                this.playPokeGo(user, dataCmd);
                break;
            }
        }
    }

    private void subScribePokeGo(final User user, final DataCmd dataCmd) {
        final SubscribePokeGoCmd cmd = new SubscribePokeGoCmd(dataCmd);
        final MGRoomPokeGo room = this.getRoom(cmd.roomId);
        if (room != null) {
            room.joinRoom(user);
            room.updatePotToUser(user);
            final PokeGoX2Msg msg = new PokeGoX2Msg();
            msg.ngayX2 = PokeGoModule.ngayX2;
            this.send(msg, user);
        } else {
            Debug.trace("POKEGO SUBSCRIBE: room " + cmd.roomId + " not found");
        }
    }

    private void unSubScribePokeGo(final User user, final DataCmd dataCmd) {
        final UnSubscribePokeGoCmd cmd = new UnSubscribePokeGoCmd(dataCmd);
        final MGRoomPokeGo room = this.getRoom(cmd.roomId);
        if (room != null) {
            room.stopAutoPlay(user);
            room.quitRoom(user);
        } else {
            Debug.trace("POKEGO UNSUBSCRIBE: room " + cmd.roomId + " not found");
        }
    }

    private void changeRoom(final User user, final DataCmd dataCmd) {
        final ChangeRoomPokeGoCmd cmd = new ChangeRoomPokeGoCmd(dataCmd);
        final MGRoomPokeGo roomLeaved = this.getRoom(cmd.roomLeavedId);
        final MGRoomPokeGo roomJoined = this.getRoom(cmd.roomJoinedId);
        if (roomLeaved != null && roomJoined != null) {
            roomLeaved.stopAutoPlay(user);
            roomLeaved.quitRoom(user);
            roomJoined.joinRoom(user);
            roomJoined.updatePotToUser(user);
        }
    }

    private void playPokeGo(final User user, final DataCmd dataCmd) {
        final PlayPokeGoCmd cmd = new PlayPokeGoCmd(dataCmd);
        final MGRoomPokeGo room = (MGRoomPokeGo) user.getProperty("MGROOM_POKEGO_INFO");
        if (room != null) {
            room.play(user, cmd.lines);
        }
    }

    private void autoPlay(final User user, final DataCmd dataCMD) {
        final AutoPlayPokeGoCmd cmd = new AutoPlayPokeGoCmd(dataCMD);
        final MGRoomPokeGo room = (MGRoomPokeGo) user.getProperty("MGROOM_POKEGO_INFO");
        if (room != null) {
            if (cmd.autoPlay == 1) {
                final short result = room.play(user, cmd.lines);
                if (result != 3 && result != 4 && result != 101 && result != 102 && result != 100) {
                    room.autoPlay(user, cmd.lines);
                } else {
                    room.forceStopAutoPlay(user);
                }
            } else {
                room.stopAutoPlay(user);
            }
        }
    }

    private String getRoomName(final short moneyType, final long baseBetting) {
        String moneyTypeStr = "xu";
        if (moneyType == 1) {
            moneyTypeStr = "vin";
        }
        return Games.POKE_GO.getName() + "_" + moneyTypeStr + "_" + baseBetting;
    }

    private MGRoomPokeGo getRoom(final byte roomId) {
        final short moneyType = this.getMoneyTypeFromRoomId(roomId);
        final long baseBetting = this.getBaseBetting(roomId);
        final String roomName = this.getRoomName(moneyType, baseBetting);
//        final MGRoomPokeGo room = PokeGoModule.rooms.get(roomName);
        return (MGRoomPokeGo) PokeGoModule.rooms.get(roomName);
    }

    private short getMoneyTypeFromRoomId(final byte roomId) {
        if (0 <= roomId && roomId < 3) {
            return 1;
        }
        return 0;
    }

    private long getBaseBetting(final byte roomId) {
        switch (roomId) {
            case 0: {
                return 100L;
            }
            case 1: {
                return 1000L;
            }
            case 2: {
                return 10000L;
            }
            case 3: {
                return 1000L;
            }
            case 4: {
                return 10000L;
            }
            case 5: {
                return 100000L;
            }
            default: {
                return 0L;
            }
        }
    }

    private int getCountTimeBot100() {
        int n = ConfigGame.getIntValue("poke_go_bot_100", 0);
        if (n == 0) {
            return 0;
        }
        if (BotMinigame.isNight()) {
            n *= 3;
        }
        return n;
    }

    private int getCountTimeBot1000() {
        int n = ConfigGame.getIntValue("poke_go_bot_1000", 0);
        if (n == 0) {
            return 0;
        }
        if (BotMinigame.isNight()) {
            n *= 5;
        }
        return n;
    }

    private int getCountTimeBot10000() {
        int n = ConfigGame.getIntValue("poke_go_bot_10000", 0);
        if (n == 0) {
            return 0;
        }
        if (BotMinigame.isNight()) {
            n *= 10;
        }
        return n;
    }

    private void gameLoop() {
        ++this.countBot100;
        if (this.countBot100 >= this.getCountTimeBot100()) {
            this.countBot100 = 0;
            final List<String> bots = BotMinigame.getBots(ConfigGame.getIntValue("poke_go_num_bot_100"), "vin");
            for (final String bot : bots) {
                if (bot == null) {
                    continue;
                }
                final MGRoomPokeGo room = (MGRoomPokeGo) PokeGoModule.rooms.get(Games.POKE_GO.getName() + "_vin_100");
                room.play(bot, this.fullLines);
            }
        }
        ++this.countBot1000;
        if (this.countBot1000 >= this.getCountTimeBot1000()) {
            this.countBot1000 = 0;
            final List<String> bots = BotMinigame.getBots(ConfigGame.getIntValue("poke_go_num_bot_1000"), "vin");
            for (final String bot : bots) {
                if (bot == null) {
                    continue;
                }
                final MGRoomPokeGo room = (MGRoomPokeGo) PokeGoModule.rooms.get(Games.POKE_GO.getName() + "_vin_1000");
                room.play(bot, this.fullLines);
            }
        }
        ++this.countBot10000;
        if (this.countBot10000 >= this.getCountTimeBot10000()) {
            this.countBot10000 = 0;
            final List<String> bots = BotMinigame.getBots(ConfigGame.getIntValue("poke_go_num_bot_10000"), "vin");
            for (final String bot : bots) {
                if (bot == null) {
                    continue;
                }
                final MGRoomPokeGo room = (MGRoomPokeGo) PokeGoModule.rooms.get(Games.POKE_GO.getName() + "_vin_10000");
                room.play(bot, this.fullLines);
            }
        }
    }

    static {
        PokeGoModule.pokeGoX2Task = new PokeGoX2Task();
        PokeGoModule.rooms = new HashMap<>();
        PokeGoModule.referenceId = 1L;
    }

    private static final class PokeGoX2Task implements Runnable {
        @Override
        public void run() {
            PokeGoModule.startX2();
            final MGRoomPokeGo room100 = (MGRoomPokeGo) PokeGoModule.rooms.get(Games.POKE_GO.getName() + "_vin_100");
            room100.startHuX2();
            final MGRoomPokeGo room101 = (MGRoomPokeGo) PokeGoModule.rooms.get(Games.POKE_GO.getName() + "_vin_1000");
            room101.startHuX2();
            Debug.trace("POKEGO START X2");
        }
    }
}
