// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.lobby.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class ResultKetSatMsg extends BaseMsgEx
{
    public long moneyUse;
    public long safe;
    public long currentMoney;
    
    public ResultKetSatMsg() {
        super(20029);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        bf.putLong(this.moneyUse);
        bf.putLong(this.safe);
        bf.putLong(this.currentMoney);
        return this.packBuffer(bf);
    }
}
