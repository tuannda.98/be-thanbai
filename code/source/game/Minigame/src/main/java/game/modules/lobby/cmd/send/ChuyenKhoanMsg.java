// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.lobby.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class ChuyenKhoanMsg extends BaseMsgEx
{
    public long moneyUse;
    
    public ChuyenKhoanMsg() {
        super(20014);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        bf.putLong(this.moneyUse);
        return this.packBuffer(bf);
    }
}
