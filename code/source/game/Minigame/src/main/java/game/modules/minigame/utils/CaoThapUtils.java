// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.utils;

import bitzero.server.entities.User;
import bitzero.util.ExtensionUtility;
import bitzero.util.common.business.Debug;
import com.vinplay.cardlib.models.Card;
import com.vinplay.cardlib.models.Deck;
import com.vinplay.cardlib.models.Rank;
import com.vinplay.dal.entities.caothap.TopCaoThap;
import com.vinplay.dal.service.impl.CaoThapServiceImpl;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.response.MoneyResponse;
import com.vinplay.vbee.common.statics.TransType;
import game.modules.minigame.cmd.send.UpdateUserInfoMsg;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.*;

public class CaoThapUtils {
    public static final byte UP = 1;
    public static final byte DOWN = 0;
    public static final double RATIO = 98.0;
    public static final double MIN_RATIO = 1.01;
    private static long[] prizes;
    private static Logger logger;
    private static final String FORMAT_SAN_BAI_DEP = "%s,\t%d,\t%s,\t%d,\t%s,\t%d,\t%d,\t%s";
    public static final double RATIO_NO_HU = 30.0;

    public static boolean isDoWithRatio(final double ratio) {
        final Random rd = new Random();
        final double i = rd.nextDouble() * 100.0;
        return i < ratio;
    }

    public static List<Double> getRatio(final Deck deck, final Card card) {
        double ratioUp = 0.0;
        double ratioDown = 0.0;
        if (deck.getSize() > 0) {
            int cdUp = 0;
            int cdDown = 0;
            int dkSize = 0;
            for (final Card cd : deck.getHand()) {
                if (cd.getRank().getRank() > card.getRank().getRank()) {
                    cdUp = (short) (cdUp + 1);
                } else {
                    if (cd.getRank().getRank() >= card.getRank().getRank()) {
                        continue;
                    }
                    cdDown = (short) (cdDown + 1);
                }
                dkSize = (short) (dkSize + 1);
            }
            if (cdUp == 0) {
                ratioUp = 0.0;
                ratioDown = 1.0;
            } else if (cdDown == 0) {
                ratioUp = 1.0;
                ratioDown = 0.0;
            } else {
                ratioUp = Math.round(98.0 * dkSize / cdUp) / 100.0;
                if (ratioUp <= 1.0) {
                    ratioUp = 1.01;
                }
                ratioDown = Math.round(98.0 * dkSize / cdDown) / 100.0;
                if (ratioDown <= 1.0) {
                    ratioDown = 1.01;
                }
            }
        }
        final ArrayList<Double> rs = new ArrayList<>();
        rs.add(ratioDown);
        rs.add(ratioUp);
        return rs;
    }

    public static Card randomThua(final Deck deck, final Card card, final byte choose) {
        Card cardThua = null;
        if (deck.getSize() > 0) {
            int numUp = 0;
            int numDown = 0;
            for (final Card cd : deck.getHand()) {
                if (cd.getRank().getRank() == card.getRank().getRank()) {
                    ++numUp;
                    ++numDown;
                } else if (cd.getRank().getRank() > card.getRank().getRank()) {
                    ++numUp;
                } else {
                    ++numDown;
                }
            }
            while (true) {
                final Deck dk;
                if ((cardThua = (dk = new Deck(deck.getDeck(), deck.getCount())).deal()).getRank().getRank() <= card.getRank().getRank() || choose != 1 || numDown <= 0) {
                    if (cardThua.getRank().getRank() < card.getRank().getRank() && choose == 0 && numUp > 0) {
                        continue;
                    }
                    break;
                }
            }
        }
        return cardThua;
    }

    public static Card randomNoA(final Deck deck) {
        Card cardThua = null;
        if (deck.getSize() > 0) {
            int numA = 0;
            for (final Card cd : deck.getHand()) {
                if (cd.getRank() != Rank.Ace) {
                    continue;
                }
                ++numA;
            }
            Deck dk;
            while ((cardThua = (dk = new Deck(deck.getDeck(), deck.getCount())).deal()).getRank() == Rank.Ace && numA != deck.getSize()) {
            }
        }
        return cardThua;
    }

    public static String getCardStr(final List<Card> cardList) {
        final StringBuilder cards = new StringBuilder();
        for (final Card cd : cardList) {
            cards.append(cd.getCode());
            cards.append(",");
        }
        return new String(cards);
    }

    public static void reward() {
        try {
            final SimpleDateFormat startTimeFormat = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
            final SimpleDateFormat endTimeFormat = new SimpleDateFormat("yyyy-MM-dd 23:59:59");
            final Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -1);
            final String startTime = startTimeFormat.format(cal.getTime());
            final String endTime = endTimeFormat.format(cal.getTime());
            Debug.trace("Tra thuong cao thap " + startTime + " - " + endTime);
            rewardCaoThap(startTime, endTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void rewardCaoThap(final String startTime, final String endTime) {
        final String actionName = "Cao Th\u00e1º¥p: Tr\u00e1º£ th\u00c6°\u00e1»\u0178ng s\u00e1»± ki\u00e1»\u2021n Th\u00c3¹ng ph\u00c3¡ s\u00e1º£nh";
        final CaoThapServiceImpl service = new CaoThapServiceImpl();
        final List<TopCaoThap> results = service.geTopCaoThap(startTime, endTime);
        Debug.trace("So user duoc Cao Thap tra thuong su kien thung pha sanh: " + results.size());
        int rank = 0;
        if (results.size() > 0) {
            final TopCaoThap entry = results.get(rank);
            Debug.trace("Cao thap tra thuong: " + entry.nickname + ", " + CaoThapUtils.prizes[rank]);
            final long moneyAfterUpdated = rewardCaoThapToUser(entry, CaoThapUtils.prizes[rank], actionName);
            log(entry.nickname, rank + 1, entry.hand, entry.money, entry.timestamp, CaoThapUtils.prizes[rank], moneyAfterUpdated);
            ++rank;
        }
        if (results.size() > 1) {
            final TopCaoThap entry = results.get(rank);
            Debug.trace("Cao thap tra thuong: " + entry.nickname + ", " + CaoThapUtils.prizes[rank]);
            final long moneyAfterUpdated = rewardCaoThapToUser(entry, CaoThapUtils.prizes[rank], actionName);
            log(entry.nickname, rank + 1, entry.hand, entry.money, entry.timestamp, CaoThapUtils.prizes[rank], moneyAfterUpdated);
            ++rank;
        }
        if (results.size() > 2) {
            final TopCaoThap entry = results.get(rank);
            Debug.trace("Cao thap tra thuong: " + entry.nickname + ", " + CaoThapUtils.prizes[rank]);
            final long moneyAfterUpdated = rewardCaoThapToUser(entry, CaoThapUtils.prizes[rank], actionName);
            log(entry.nickname, rank + 1, entry.hand, entry.money, entry.timestamp, CaoThapUtils.prizes[rank], moneyAfterUpdated);
            ++rank;
        }
    }

    private static long rewardCaoThapToUser(final TopCaoThap entry, final long prize, final String actionName) {
        final SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        final UserServiceImpl userService = new UserServiceImpl();
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        final MoneyResponse response = userService.updateMoney(entry.nickname, prize, "vin", "CaoThap", actionName, "Ng\u00c3 y " + format.format(cal.getTime()), 0L, null, TransType.NO_VIPPOINT);
        final User user = ExtensionUtility.getExtension().getApi().getUserByName(entry.nickname);
        if (response != null && response.isSuccess() && user != null) {
            final UpdateUserInfoMsg msg = new UpdateUserInfoMsg();
            msg.currentMoney = response.getCurrentMoney();
            msg.moneyType = 1;
            ExtensionUtility.getExtension().send(msg, user);
        }
        if (response != null) {
            return response.getCurrentMoney();
        }
        return -1L;
    }

    private static void log(final String nickname, final int rank, final String hand, final long moneyWin, final String playTime, final long prize, final long moneyAfterUpdated) {
        final SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
        final String str = String.format("%s,\t%d,\t%s,\t%d,\t%s,\t%d,\t%d,\t%s", nickname, rank, hand, moneyWin, playTime, prize, moneyAfterUpdated, df.format(new Date()));
        System.out.println(str);
        CaoThapUtils.logger.debug(str);
    }

    static {
        CaoThapUtils.prizes = new long[]{500000L, 200000L, 100000L};
        CaoThapUtils.logger = Logger.getLogger("csvCaoThapPrize");
    }
}
