// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.gameRoom.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class ReconnectGameRoomFailMsg extends BaseMsgEx
{
    public ReconnectGameRoomFailMsg() {
        super(3002);
    }
    
    public byte[] createData() {
        final ByteBuffer buffer = this.makeBuffer();
        return this.packBuffer(buffer);
    }
}
