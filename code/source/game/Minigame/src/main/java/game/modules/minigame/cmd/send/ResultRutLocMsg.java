// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class ResultRutLocMsg extends BaseMsgEx
{
    public int prize;
    public long currentMoney;
    
    public ResultRutLocMsg() {
        super(2119);
        this.prize = 0;
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        bf.putInt(this.prize);
        bf.putLong(this.currentMoney);
        return this.packBuffer(bf);
    }
}
