// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.entities.pokego;

public enum Award
{
    TRIPLE_POKER_BALL((byte)1, Item.POKER_BALL, (byte)3, -2.0f), 
    TRIPLE_PIKACHU((byte)2, Item.PIKACHU, (byte)3, 85.0f), 
    TRIPLE_BULBASAUR((byte)3, Item.BULBASAUR, (byte)3, 40.0f), 
    TRIPLE_CLEFABLE((byte)4, Item.CLEFABLE, (byte)3, 20.0f), 
    TRIPLE_MOUSE((byte)5, Item.MOUSE, (byte)3, 8.0f), 
    DOUBLE_MOUSE((byte)6, Item.MOUSE, (byte)2, 0.8f), 
    TRIPLE_TOGEPI((byte)7, Item.TOGEPI, (byte)3, 3.0f), 
    DOUBLE_TOGEPI((byte)8, Item.TOGEPI, (byte)2, 0.4f);
    
    private byte id;
    private Item item;
    private byte duplicate;
    private float ratio;
    
    Award(final byte id, final Item item, final byte duplicate, final float ratio) {
        this.id = id;
        this.item = item;
        this.duplicate = duplicate;
        this.ratio = ratio;
    }
    
    public void setId(final byte id) {
        this.id = id;
    }
    
    public byte getId() {
        return this.id;
    }
    
    public void setItem(final Item item) {
        this.item = item;
    }
    
    public Item getItem() {
        return this.item;
    }
    
    public void setDuplicate(final byte duplicate) {
        this.duplicate = duplicate;
    }
    
    public byte getDuplicate() {
        return this.duplicate;
    }
    
    public void setRatio(final float ratio) {
        this.ratio = ratio;
    }
    
    public float getRatio() {
        return this.ratio;
    }
}
