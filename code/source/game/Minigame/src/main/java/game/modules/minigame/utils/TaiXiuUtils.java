// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.utils;

import bitzero.server.entities.User;
import bitzero.util.ExtensionUtility;
import com.vinplay.dal.dao.impl.TaiXiuDAOImpl;
import com.vinplay.dal.entities.taixiu.ResultTaiXiu;
import com.vinplay.dal.entities.taixiu.TransactionTaiXiuDetail;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.models.cache.ThanhDuTXModel;
import com.vinplay.vbee.common.response.MoneyResponse;
import com.vinplay.vbee.common.statics.TransType;
import game.modules.minigame.cmd.send.UpdateUserInfoMsg;
import org.apache.log4j.Logger;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class TaiXiuUtils {
    private static Logger loggerTaiXiu;
    private static final String FORMAT_LOG_BET_TAI_XIU = "%d,\t%s,\t%d,\t%d,\t%d,\t%s,\t%s";
    private static Logger loggerThanhDu;
    private static long[] prizes;
    private static final String FORMAT_THANH_DU = "%s,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%s";

    public static String buildLichSuPhien(final List<ResultTaiXiu> input, final int number) {
        final int end = input.size();
        final int start = Math.max(end - number, 0);
        final StringBuilder builder = new StringBuilder();
        for (int i = start; i < end; ++i) {
            final ResultTaiXiu entry = input.get(i);
            builder.append(entry.dice1);
            builder.append(",");
            builder.append(entry.dice2);
            builder.append(",");
            builder.append(entry.dice3);
            builder.append(",");
        }
        if (builder.length() > 0) {
            builder.deleteCharAt(builder.length() - 1);
        }
        return builder.toString();
    }

    public static String logLichSuPhien(final List<ResultTaiXiu> input, final int number) {
        final int end = input.size();
        final int start = Math.max(end - number, 0);
        final StringBuilder builder = new StringBuilder();
        for (int i = start; i < end; ++i) {
            final ResultTaiXiu entry = input.get(i);
            builder.append(entry.result);
            builder.append(",");
        }
        if (builder.length() > 0) {
            builder.deleteCharAt(builder.length() - 1);
        }
        return builder.toString();
    }

    public static void rewardThanhDu() {
        try {
            final SimpleDateFormat startTimeFormat = new SimpleDateFormat("yyyy/MM/dd 00:00:00");
            final SimpleDateFormat endTimeFormat = new SimpleDateFormat("yyyy/MM/dd 23:59:59");
            final Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -1);
            final String startTime = startTimeFormat.format(cal.getTime());
            final String endTime = endTimeFormat.format(cal.getTime());
            rewardThanhDu(startTime, endTime, (short) 1);
            rewardThanhDu(startTime, endTime, (short) 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void rewardThanhDu(final String startTime, final String endTime, final short type) throws SQLException {
        final String actionName = "Tr\u00e1º£ th\u00c6°\u00e1»\u0178ng Th\u00c3¡nh D\u00e1»± top " + ((type == 1) ? "thua" : "th\u00e1º¯ng");
        final TaiXiuDAOImpl txDAO = new TaiXiuDAOImpl();
        final List<ThanhDuTXModel> listUser = txDAO.getTopThanhDuDaily(startTime, endTime, type);
        int rank = 0;
        if (listUser.size() > 0) {
            final ThanhDuTXModel entry = listUser.get(rank);
            final long moneyAfterUpdated = rewardThanhDuToUser(entry, TaiXiuUtils.prizes[rank], actionName);
            log(entry.username, rank + 1, type, entry.number, entry.totalValue, moneyAfterUpdated, TaiXiuUtils.prizes[rank]);
            ++rank;
        }
        if (listUser.size() > 1) {
            final ThanhDuTXModel entry = listUser.get(rank);
            final long moneyAfterUpdated = rewardThanhDuToUser(entry, TaiXiuUtils.prizes[rank], actionName);
            log(entry.username, rank + 1, type, entry.number, entry.totalValue, moneyAfterUpdated, TaiXiuUtils.prizes[rank]);
            ++rank;
        }
        if (listUser.size() > 2) {
            final ThanhDuTXModel entry = listUser.get(rank);
            final long moneyAfterUpdated = rewardThanhDuToUser(entry, TaiXiuUtils.prizes[rank], actionName);
            log(entry.username, rank + 1, type, entry.number, entry.totalValue, moneyAfterUpdated, TaiXiuUtils.prizes[rank]);
            ++rank;
        }
    }

    private static long rewardThanhDuToUser(final ThanhDuTXModel entry, final long prize, final String actionName) {
        final SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        final UserServiceImpl userService = new UserServiceImpl();
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        final MoneyResponse response = userService.updateMoney(entry.username, prize, "vin", "TaiXiu", "Th\u00c3¡nh D\u00e1»± - Tr\u00e1º£ th\u00c6°\u00e1»\u0178ng", "Ng\u00c3 y " + format.format(cal.getTime()), 0L, null, TransType.NO_VIPPOINT);
        final User u = ExtensionUtility.getExtension().getApi().getUserByName(entry.username);
        if (response != null && response.isSuccess() && u != null) {
            final UpdateUserInfoMsg msg = new UpdateUserInfoMsg();
            msg.currentMoney = response.getCurrentMoney();
            msg.moneyType = 1;
            ExtensionUtility.getExtension().send(msg, u);
        }
        if (response != null) {
            return response.getCurrentMoney();
        }
        return -1L;
    }

    private static void log(final String username, final int rank, final int type, final int number, final long totalValue, final long prize, final long moneyAfterUpdated) {
        final SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
        final String str = String.format("%s,\t%d,\t%d,\t%d,\t%d,\t%d,\t%d,\t%s", username, rank, type, number, totalValue, prize, moneyAfterUpdated, df.format(new Date()));
        TaiXiuUtils.loggerThanhDu.debug(str);
    }

    public static void logBetTaiXiu(final TransactionTaiXiuDetail tran) {
        final String moneyType = (tran.moneyType == 1) ? "vin" : "xu";
        logBetTaiXiu(tran.referenceId, tran.username, tran.betValue, tran.betSide, tran.inputTime, moneyType);
    }

    public static void logBetTaiXiu(final long referenceId, final String nickname, final long betValue, final int betSide, final int inputTime, final String moneyType) {
        final SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
        final String str = String.format("%d,\t%s,\t%d,\t%d,\t%d,\t%s,\t%s", referenceId, nickname, betValue, betSide, inputTime, moneyType, df.format(new Date()));
        TaiXiuUtils.loggerTaiXiu.debug(str);
    }

    static {
        TaiXiuUtils.loggerTaiXiu = Logger.getLogger("csvBetTaiXiu");
        TaiXiuUtils.loggerThanhDu = Logger.getLogger("csvThanhDuPrize");
        TaiXiuUtils.prizes = new long[]{500000L, 200000L, 100000L};
    }
}
