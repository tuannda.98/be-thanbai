// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class UpdateRutLocMsg extends BaseMsgEx
{
    public int soLuotRut;
    
    public UpdateRutLocMsg() {
        super(2122);
        this.soLuotRut = 0;
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        bf.putInt(this.soLuotRut);
        return this.packBuffer(bf);
    }
}
