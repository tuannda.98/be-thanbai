// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.entities.pokego;

public enum Item
{
    NONE("none", (byte)(-1)), 
    POKER_BALL("poker_ball", (byte)0), 
    PIKACHU("pikachu", (byte)1), 
    BULBASAUR("bulbasaur", (byte)2), 
    CLEFABLE("clefable", (byte)3), 
    MOUSE("mouse", (byte)4), 
    TOGEPI("togepi", (byte)5);
    
    private String name;
    private byte id;
    
    Item(final String name, final byte id) {
        this.name = name;
        this.id = id;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setId(final byte id) {
        this.id = id;
    }
    
    public byte getId() {
        return this.id;
    }
    
    public static Item findItem(final byte id) {
        for (final Item entry : values()) {
            if (entry.getId() == id) {
                return entry;
            }
        }
        return null;
    }
}
