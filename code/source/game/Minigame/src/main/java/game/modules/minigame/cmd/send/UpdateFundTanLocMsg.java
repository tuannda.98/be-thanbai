// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class UpdateFundTanLocMsg extends BaseMsgEx
{
    public long value;
    
    public UpdateFundTanLocMsg() {
        super(2120);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        bf.putLong(this.value);
        return this.packBuffer(bf);
    }
}
