// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.entities;

public class BotTaiXiu
{
    private String nickname;
    private short timeBetting;
    private long betValue;
    private short betSide;
    
    public BotTaiXiu(final String nickname, final short timeBetting, final long betValue, final short betSide) {
        this.nickname = nickname;
        this.timeBetting = timeBetting;
        this.betValue = betValue;
        this.betSide = betSide;
    }
    
    public void setNickname(final String nickname) {
        this.nickname = nickname;
    }
    
    public String getNickname() {
        return this.nickname;
    }
    
    public void setTimeBetting(final short timeBetting) {
        this.timeBetting = timeBetting;
    }
    
    public short getTimeBetting() {
        return this.timeBetting;
    }
    
    public void setBetValue(final long betValue) {
        this.betValue = betValue;
    }
    
    public long getBetValue() {
        return this.betValue;
    }
    
    public void setBetSide(final short betSide) {
        this.betSide = betSide;
    }
    
    public short getBetSide() {
        return this.betSide;
    }
}
