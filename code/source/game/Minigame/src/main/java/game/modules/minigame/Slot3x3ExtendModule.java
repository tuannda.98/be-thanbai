package game.modules.minigame;

import bitzero.server.core.BZEventParam;
import bitzero.server.core.BZEventType;
import bitzero.server.core.IBZEvent;
import bitzero.server.entities.User;
import bitzero.server.exceptions.BZException;
import bitzero.server.extensions.BaseClientRequestHandler;
import bitzero.server.extensions.data.DataCmd;
import bitzero.util.common.business.Debug;
import com.vinplay.dal.service.CacheService;
import com.vinplay.dal.service.MiniGameService;
import com.vinplay.dal.service.impl.CacheServiceImpl;
import com.vinplay.dal.service.impl.MiniGameServiceImpl;
import com.vinplay.vbee.common.utils.CommonUtils;
import game.SlotExtendService;
import game.SlotExtendServiceImplement;
import game.modules.minigame.cmd.rev.slot3x3.AutoPlaySlotExtendCmd;
import game.modules.minigame.cmd.rev.slot3x3.ChangeRoomSlotExtendCmd;
import game.modules.minigame.cmd.rev.slot3x3.SubscribeSlotExtendCmd;
import game.modules.minigame.cmd.rev.slot3x3.UnSubscribeSlotExtendCmd;
import game.modules.minigame.cmd.send.slot3x3.RequestSlotExtendSpin;
import game.modules.minigame.cmd.send.slot3x3.UpdatePotSlotExtend;
import game.modules.minigame.room.MGRoom;
import game.modules.minigame.room.MGRoomSlotExtend;
import game.utils.ConfigGame;
import game.utils.GameUtils;
import vn.yotel.yoker.util.Util;

import java.util.*;

public class Slot3x3ExtendModule extends BaseClientRequestHandler {
    private static Map<String, MGRoom> rooms;
    private final MiniGameService service;
    private final SlotExtendService slotExtendService;
    private static long referenceId;
    protected CacheService sv;
    private long countUpdateJackpot;
    private Set<User> usersSubJackpot;

    public Slot3x3ExtendModule() {
        this.service = new MiniGameServiceImpl();
        this.slotExtendService = new SlotExtendServiceImplement();
        this.sv = new CacheServiceImpl();
    }

    public void init() {
        super.init();
        long[] pots = new long[6];
        long[] funds = new long[6];
        final int[] initPotValues = new int[6];
        try {
            final String initPotValuesStr = ConfigGame.getValueString("slot_extend_init_pot_values");
            final String[] arr = initPotValuesStr.split(",");
            for (int i = 0; i < arr.length; ++i) {
                initPotValues[i] = Integer.parseInt(arr[i]);
            }
            pots = this.service.getPots("SlotExtend");
            Debug.trace("SLOT EXTEND POTS: " + CommonUtils.arrayLongToString(pots));
            funds = this.service.getFunds("SlotExtend");
            Debug.trace("SLOT EXTEND FUNDS: " + CommonUtils.arrayLongToString(funds));
        } catch (Exception e) {
            Debug.trace("Init SLOT EXTEND error ", e.getMessage());
        }
        Slot3x3ExtendModule.rooms.put("SlotExtend_vin_100", new MGRoomSlotExtend("SlotExtend_vin_100", (short) 1, pots[0], funds[0], 100, initPotValues[0]));
        Slot3x3ExtendModule.rooms.put("SlotExtend_vin_1000", new MGRoomSlotExtend("SlotExtend_vin_1000", (short) 1, pots[1], funds[1], 1000, initPotValues[1]));
        Slot3x3ExtendModule.rooms.put("SlotExtend_vin_10000", new MGRoomSlotExtend("SlotExtend_vin_10000", (short) 1, pots[2], funds[2], 10000, initPotValues[2]));
        Debug.trace("INIT SLOT_EXTEND DONE");
        this.getParentExtension().addEventListener(BZEventType.USER_DISCONNECT, this);
        Slot3x3ExtendModule.referenceId = this.slotExtendService.getLastReferenceId();
        Debug.trace("START SLOT_EXTEND REFERENCE ID= " + Slot3x3ExtendModule.referenceId);
        this.usersSubJackpot = new HashSet<>();
        TimerTask gameLoopTask = new TimerTask() {
            @Override
            public void run() {
                gameLoop();
            }
        };
        new Timer().scheduleAtFixedRate(gameLoopTask, Util.randInt(5_000, 10_000), 1_000);
    }

    public static long getNewRefenceId() {
        return ++Slot3x3ExtendModule.referenceId;
    }

    public void handleServerEvent(final IBZEvent ibzevent) throws BZException {
        if (ibzevent.getType() == BZEventType.USER_DISCONNECT) {
            final User user = (User) ibzevent.getParameter(BZEventParam.USER);
            this.userDis(user);
        }
    }

    private void userDis(final User user) {
        final MGRoomSlotExtend room = (MGRoomSlotExtend) user.getProperty("MGROOM_SLOT_EXTEND_INFO");
        if (room != null) {
            room.quitRoom(user);
            room.stopAutoPlay(user);
        }
    }

    public void handleClientRequest(final User user, final DataCmd dataCmd) {
        System.out.println("handleClientRequest " + dataCmd.getId());
        switch (dataCmd.getId()) {
            case 8001: {
                this.subScribeSlotExtend(user, dataCmd);
                break;
            }
            case 8002: {
                this.unSubScribePokeGo(user, dataCmd);
                break;
            }
            case 8004: {
                this.changeRoom(user, dataCmd);
                break;
            }
            case 8005: {
                if (GameUtils.disablePlayMiniGame(user)) {
                    return;
                }
                this.autoPlay(user, dataCmd);
                break;
            }
            case 8003: {
                if (GameUtils.disablePlayMiniGame(user)) {
                    return;
                }
                this.playSlotExtend(user, dataCmd);
                break;
            }
        }
    }

    private void subScribeSlotExtend(final User user, final DataCmd dataCmd) {
        final SubscribeSlotExtendCmd cmd = new SubscribeSlotExtendCmd(dataCmd);
        final MGRoomSlotExtend room = this.getRoom(cmd.roomId);
        if (room != null) {
            room.joinRoom(user);
            room.updatePotToUser(user);
        } else {
            Debug.trace("MSlotExtend SUBSCRIBE: room " + cmd.roomId + " not found");
        }
        if (user != null) {
            this.usersSubJackpot.add(user);
        }
    }

    private void unSubScribePokeGo(final User user, final DataCmd dataCmd) {
        final UnSubscribeSlotExtendCmd cmd = new UnSubscribeSlotExtendCmd(dataCmd);
        final MGRoomSlotExtend room = this.getRoom(cmd.roomId);
        if (room != null) {
            room.stopAutoPlay(user);
            room.quitRoom(user);
        } else {
            Debug.trace("MGRoomSlotExtend UNSUBSCRIBE: room " + cmd.roomId + " not found");
        }
        if (user != null) {
            this.usersSubJackpot.remove(user);
        }
    }

    private void changeRoom(final User user, final DataCmd dataCmd) {
        final ChangeRoomSlotExtendCmd cmd = new ChangeRoomSlotExtendCmd(dataCmd);
        final MGRoomSlotExtend roomLeaved = this.getRoom(cmd.roomLeavedId);
        final MGRoomSlotExtend roomJoined = this.getRoom(cmd.roomJoinedId);
        if (roomLeaved != null && roomJoined != null) {
            roomLeaved.stopAutoPlay(user);
            roomLeaved.quitRoom(user);
            roomJoined.joinRoom(user);
            roomJoined.updatePotToUser(user);
        }
    }

    private void playSlotExtend(final User user, final DataCmd dataCmd) {
        final RequestSlotExtendSpin cmd = new RequestSlotExtendSpin(dataCmd);
        final MGRoomSlotExtend room = (MGRoomSlotExtend) user.getProperty("MGROOM_SLOT_EXTEND_INFO");
        if (room != null) {
            room.play(user, cmd.gold);
        }
    }

    private void autoPlay(final User user, final DataCmd dataCMD) {
        final AutoPlaySlotExtendCmd cmd = new AutoPlaySlotExtendCmd(dataCMD);
        final MGRoomSlotExtend room = (MGRoomSlotExtend) user.getProperty("MGROOM_SLOT_EXTEND_INFO");
        if (room != null) {
            if (cmd.autoPlay == 1) {
                final short result = room.play(user, cmd.gold);
                if (result != 3 && result != 4 && result != 101 && result != 102 && result != 100) {
                    room.autoPlay(user, cmd.gold);
                } else {
                    room.forceStopAutoPlay(user);
                }
            } else {
                room.stopAutoPlay(user);
            }
        }
    }

    private String getRoomName(final short moneyType, final long baseBetting) {
        String moneyTypeStr = "xu";
        if (moneyType == 1) {
            moneyTypeStr = "vin";
        }
        return "SlotExtend_" + moneyTypeStr + "_" + baseBetting;
    }

    private MGRoomSlotExtend getRoom(final byte roomId) {
        final short moneyType = this.getMoneyTypeFromRoomId(roomId);
        final long baseBetting = this.getBaseBetting(roomId);
        final String roomName = this.getRoomName(moneyType, baseBetting);
        return (MGRoomSlotExtend) Slot3x3ExtendModule.rooms.get(roomName);
    }

    private short getMoneyTypeFromRoomId(final byte roomId) {
        if (0 <= roomId && roomId < 3) {
            return 1;
        }
        return 0;
    }

    private long getBaseBetting(final byte roomId) {
        switch (roomId) {
            case 0: {
                return 100L;
            }
            case 1:
            case 3: {
                return 1000L;
            }
            case 2:
            case 4: {
                return 10000L;
            }
            case 5: {
                return 100000L;
            }
            default: {
                return 0L;
            }
        }
    }

    public void updateJackpot() {
        try {
            final com.vinplay.usercore.service.impl.CacheServiceImpl cacheService = new com.vinplay.usercore.service.impl.CacheServiceImpl();
            final int miniPoker100 = cacheService.getValueInt("SlotExtend_vin_100");
            final int miniPoker101 = cacheService.getValueInt("SlotExtend_vin_1000");
            final int miniPoker102 = cacheService.getValueInt("SlotExtend_vin_10000");
            final UpdatePotSlotExtend msg = new UpdatePotSlotExtend();
            msg.value1 = miniPoker100;
            msg.value2 = miniPoker101;
            msg.value3 = miniPoker102;
            for (final User user : this.usersSubJackpot) {
                if (user != null) {
                    this.send(msg, user);
                }
            }
        } catch (Exception e) {
            Debug.trace("Update jackpot exception: " + e.getMessage());
        }
    }

    private void gameLoop() {
        ++this.countUpdateJackpot;
        if (this.countUpdateJackpot >= ConfigGame.getIntValue("update_jackpot_time")) {
            this.updateJackpot();
            this.countUpdateJackpot = 0L;
        }
    }

    static {
        Slot3x3ExtendModule.rooms = new HashMap<>();
        Slot3x3ExtendModule.referenceId = 1L;
    }
}
