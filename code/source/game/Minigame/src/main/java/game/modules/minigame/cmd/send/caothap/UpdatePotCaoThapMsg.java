// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.send.caothap;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class UpdatePotCaoThapMsg extends BaseMsgEx
{
    public long value;
    
    public UpdatePotCaoThapMsg() {
        super(6003);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        bf.putLong(this.value);
        return this.packBuffer(bf);
    }
}
