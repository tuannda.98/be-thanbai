// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.rev.caothap;

import java.nio.ByteBuffer;
import bitzero.server.extensions.data.DataCmd;
import bitzero.server.extensions.data.BaseCmd;

public class StartPlayCaoThapCmd extends BaseCmd
{
    public int betValue;
    public byte moneyType;
    
    public StartPlayCaoThapCmd(final DataCmd dataCmd) {
        super(dataCmd);
        this.unpackData();
    }
    
    public void unpackData() {
        final ByteBuffer bf = this.makeBuffer();
        this.betValue = bf.getInt();
        this.moneyType = this.readByte(bf);
    }
}
