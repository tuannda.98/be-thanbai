// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.utils;

import com.vinplay.cardlib.models.*;
import com.vinplay.cardlib.utils.CardLibUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class GenerationMiniPoker
{
    private final int[] tile;
    private final int[] prizes;
    
    public GenerationMiniPoker() {
        this.tile = new int[] { 523985, 783985, 933985, 970985, 989985, 994985, 998985, 999835, 999985, 999995, 1000000 };
        this.prizes = new int[] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
    }
    
    public List<Card> randomCards2() {
        final Random rd = new Random();
        final int n = rd.nextInt(1000000);
        int prize = 11;
        for (int i = 0; i < this.tile.length; ++i) {
            if (n < this.tile[i]) {
                prize = this.prizes[i];
                break;
            }
        }
        switch (prize) {
            case 1: {
                return randomThungPhaSanhJDenA();
            }
            case 2: {
                return randomThungPhaSanhNho();
            }
            case 3: {
                return randomTuQuy();
            }
            case 4: {
                return this.randomCuLu();
            }
            case 5: {
                return this.randomThung();
            }
            case 6: {
                return this.randomSanh();
            }
            case 7: {
                return this.randomSamCo();
            }
            case 8: {
                return this.randomHaiDoi();
            }
            case 9: {
                return this.randomMotDoiTo();
            }
            default: {
                return this.randomTruot();
            }
        }
    }
    
    public static List<Card> randomThungPhaSanhA() {
        final ArrayList<Card> cards = new ArrayList<>();
        final Random rd = new Random();
        final int idRank = 8;
        final int n = rd.nextInt(4);
        final Suit suit = Suit.getSuitFromId(n);
        for (int i = 0; i < 5; ++i) {
            cards.add(new Card(Rank.getRankFromId(idRank + i), suit));
        }
        Collections.shuffle(cards);
        return cards;
    }
    
    public static List<Card> randomThungPhaSanhJDenK() {
        final ArrayList<Card> cards = new ArrayList<>();
        final Random rd = new Random();
        int n = rd.nextInt(4);
        final int idRank = n + 4;
        n = rd.nextInt(4);
        final Suit suit = Suit.getSuitFromId(n);
        for (int i = 0; i < 5; ++i) {
            cards.add(new Card(Rank.getRankFromId(idRank + i), suit));
        }
        Collections.shuffle(cards);
        return cards;
    }
    
    public static List<Card> randomThungPhaSanhJDenA() {
        final ArrayList<Card> cards = new ArrayList<>();
        final Random rd = new Random();
        int n = rd.nextInt(4);
        final int idRank = n + 5;
        n = rd.nextInt(4);
        final Suit suit = Suit.getSuitFromId(n);
        for (int i = 0; i < 5; ++i) {
            cards.add(new Card(Rank.getRankFromId(idRank + i), suit));
        }
        Collections.shuffle(cards);
        return cards;
    }
    
    public static List<Card> randomThungPhaSanhNho() {
        final ArrayList<Card> cards = new ArrayList<>();
        final Random rd = new Random();
        final int idRank;
        int n = idRank = rd.nextInt(5);
        n = rd.nextInt(4);
        final Suit suit = Suit.getSuitFromId(n);
        for (int i = 0; i < 5; ++i) {
            cards.add(new Card(Rank.getRankFromId(idRank + i), suit));
        }
        Collections.shuffle(cards);
        return cards;
    }
    
    public static List<Card> randomTuQuy() {
        final ArrayList<Card> cards = new ArrayList<>();
        final Random rd = new Random();
        final int n = rd.nextInt(13);
        final Deck deck = new Deck();
        cards.add(deck.popCard(Rank.getRankFromId(n), Suit.Clubs));
        cards.add(deck.popCard(Rank.getRankFromId(n), Suit.Diamonds));
        cards.add(deck.popCard(Rank.getRankFromId(n), Suit.Hearts));
        cards.add(deck.popCard(Rank.getRankFromId(n), Suit.Spades));
        cards.add(deck.deal());
        Collections.shuffle(cards);
        return cards;
    }
    
    private List<Card> randomCuLu() {
        final ArrayList<Card> cards = new ArrayList<>();
        final Random rd = new Random();
        final int n = rd.nextInt(13);
        final Deck deck = new Deck();
        for (int i = 0; i < 3; ++i) {
            Card card;
            int s;
            for (card = null; card == null; card = deck.popCard(Rank.getRankFromId(n), Suit.getSuitFromId(s))) {
                s = rd.nextInt(4);
            }
            cards.add(card);
        }
        int n2;
        for (n2 = n; n2 == n; n2 = rd.nextInt(13)) {}
        for (int j = 0; j < 2; ++j) {
            Card card2;
            int s2;
            for (card2 = null; card2 == null; card2 = deck.popCard(Rank.getRankFromId(n2), Suit.getSuitFromId(s2))) {
                s2 = rd.nextInt(4);
            }
            cards.add(card2);
        }
        Collections.shuffle(cards);
        return cards;
    }
    
    private List<Card> randomThung() {
        final ArrayList<Card> cards = new ArrayList<>();
        final Random rd = new Random();
        final Deck deck = new Deck();
        final int s = rd.nextInt(4);
        for (int i = 0; i < 5; ++i) {
            Card card;
            int r;
            for (card = null; card == null; card = deck.popCard(Rank.getRankFromId(r), Suit.getSuitFromId(s))) {
                r = rd.nextInt(13);
            }
            cards.add(card);
        }
        Collections.shuffle(cards);
        return cards;
    }
    
    private List<Card> randomSanh() {
        final ArrayList<Card> cards = new ArrayList<>();
        final Random rd = new Random();
        final Deck deck = new Deck();
        final int r = rd.nextInt(9);
        for (int i = 0; i < 5; ++i) {
            Card card;
            int s;
            for (card = null; card == null; card = deck.popCard(Rank.getRankFromId(r + i), Suit.getSuitFromId(s))) {
                s = rd.nextInt(4);
            }
            cards.add(card);
        }
        Collections.shuffle(cards);
        return cards;
    }
    
    private List<Card> randomSamCo() {
        final ArrayList<Card> cards = new ArrayList<>();
        final Random rd = new Random();
        boolean loop = true;
        while (loop) {
            final Deck deck = new Deck();
            final int r = rd.nextInt(13);
            for (int i = 0; i < 3; ++i) {
                Card card;
                int s;
                for (card = null; card == null; card = deck.popCard(Rank.getRankFromId(r), Suit.getSuitFromId(s))) {
                    s = rd.nextInt(4);
                }
                cards.add(card);
            }
            cards.add(deck.deal());
            cards.add(deck.deal());
            if (CardLibUtils.calculateTypePoker(cards) == GroupType.ThreeOfKind) {
                loop = false;
            }
            else {
                cards.clear();
            }
        }
        Collections.shuffle(cards);
        return cards;
    }
    
    private List<Card> randomHaiDoi() {
        final ArrayList<Card> cards = new ArrayList<>();
        final Random rd = new Random();
        final Deck deck = new Deck();
        int r2;
        int r1;
        for (r1 = (r2 = rd.nextInt(13)); r2 == r1; r2 = rd.nextInt(13)) {}
        for (int i = 0; i < 2; ++i) {
            Card card;
            int s;
            for (card = null; card == null; card = deck.popCard(Rank.getRankFromId(r1), Suit.getSuitFromId(s))) {
                s = rd.nextInt(4);
            }
            cards.add(card);
        }
        for (int i = 0; i < 2; ++i) {
            Card card;
            int s;
            for (card = null; card == null; card = deck.popCard(Rank.getRankFromId(r2), Suit.getSuitFromId(s))) {
                s = rd.nextInt(4);
            }
            cards.add(card);
        }
        boolean loop = true;
        while (loop) {
            int r3;
            for (r3 = r1; r3 == r1 || r3 == r2; r3 = rd.nextInt(13)) {}
            final int s = rd.nextInt(4);
            final Card c5 = new Card(Rank.getRankFromId(r3), Suit.getSuitFromId(s));
            cards.add(c5);
            if (CardLibUtils.calculateTypePoker(cards) == GroupType.TwoPair) {
                loop = false;
            }
            else {
                cards.remove(c5);
            }
        }
        Collections.shuffle(cards);
        return cards;
    }
    
    private List<Card> randomMotDoiTo() {
        final ArrayList<Card> cards = new ArrayList<>();
        final Random rd = new Random();
        boolean loop = true;
        while (loop) {
            final Deck deck = new Deck();
            final int r1 = rd.nextInt(4) + 9;
            for (int i = 0; i < 2; ++i) {
                Card card;
                int s;
                for (card = null; card == null; card = deck.popCard(Rank.getRankFromId(r1), Suit.getSuitFromId(s))) {
                    s = rd.nextInt(4);
                }
                cards.add(card);
            }
            for (int i = 0; i < 3; ++i) {
                int r2;
                for (r2 = r1; r2 == r1; r2 = rd.nextInt(13)) {}
                Card card2;
                int s2;
                for (card2 = null; card2 == null; card2 = deck.popCard(Rank.getRankFromId(r2), Suit.getSuitFromId(s2))) {
                    s2 = rd.nextInt(4);
                }
                cards.add(card2);
            }
            if (CardLibUtils.calculateTypePoker(cards) == GroupType.OnePair && CardLibUtils.pairEqualOrGreatJack(cards)) {
                loop = false;
            }
            else {
                cards.clear();
            }
        }
        Collections.shuffle(cards);
        return cards;
    }
    
    private List<Card> randomTruot() {
        List<Card> cards = new ArrayList<>();
        boolean loop = true;
        while (loop) {
            cards = this.randomCards();
            if ((CardLibUtils.calculateTypePoker(cards) != GroupType.OnePair || CardLibUtils.pairEqualOrGreatJack(cards)) && CardLibUtils.calculateTypePoker(cards) != GroupType.HighCard) {
                continue;
            }
            loop = false;
        }
        return cards;
    }
    
    public List<Card> randomCards() {
        final ArrayList<Card> cards = new ArrayList<>();
        final Deck deck = new Deck();
        deck.shuffle();
        for (int i = 0; i < 5; ++i) {
            final Card card = deck.deal();
            cards.add(card);
        }
        return cards;
    }
    
    public List<Card> random() {
        final ArrayList<Card> cards = new ArrayList<>();
        cards.add(new Card(Rank.Nine, Suit.Diamonds));
        cards.add(new Card(Rank.Seven, Suit.Diamonds));
        cards.add(new Card(Rank.Six, Suit.Diamonds));
        cards.add(new Card(Rank.Five, Suit.Diamonds));
        cards.add(new Card(Rank.Eight, Suit.Diamonds));
        return cards;
    }
}
