// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class LichSuPhienMsg extends BaseMsgEx
{
    public String data;
    
    public LichSuPhienMsg() {
        super(2116);
    }
    
    public byte[] createData() {
        final ByteBuffer buffer = this.makeBuffer();
        this.putStr(buffer, this.data);
        return this.packBuffer(buffer);
    }
}
