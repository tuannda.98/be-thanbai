// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.send.minipoker;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class MiniPokerX2Msg extends BaseMsgEx
{
    public String ngayX2;
    
    public MiniPokerX2Msg() {
        super(4009);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        this.putStr(bf, this.ngayX2);
        return this.packBuffer(bf);
    }
}
