package game;

import bitzero.engine.sessions.ISession;
import bitzero.server.BitZeroServer;
import bitzero.server.core.BZEventType;
import bitzero.server.entities.User;
import bitzero.server.entities.data.ISFSObject;
import bitzero.server.exceptions.BZException;
import bitzero.server.extensions.BZExtension;
import bitzero.server.extensions.data.DataCmd;
import bitzero.util.ExtensionUtility;
import bitzero.util.common.business.Debug;
import bitzero.util.socialcontroller.bean.UserInfo;
import com.vinplay.usercore.utils.GameCommon;
import com.vinplay.vbee.common.hazelcast.HazelcastLoader;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.pools.ConnectionPool;
import com.vinplay.vbee.common.rmq.RMQApi;
import game.eventHandlers.LoginSuccessHandler;
import game.eventHandlers.UserDisconnectHandler;
import game.modules.player.cmd.rev.LoginCmd;
import game.utils.ConfigGame;
import game.utils.GameUtils;
import vn.yotel.yoker.util.Util;

import java.util.concurrent.TimeUnit;

public class SlotExtendExtension extends BZExtension {
    public void init() {
        try {
            RMQApi.start("config/rmq.properties");
            HazelcastLoader.start();
            MongoDBConnectionFactory.init();
            ConnectionPool.start("config/db_pool.properties");
            ConfigGame.reload();
            GameCommon.init();
        } catch (Exception e) {
            Debug.trace("INIT SLOT EXTEND ERROR" + e.getMessage());
        }
        this.addEventHandler(BZEventType.USER_LOGIN, LoginSuccessHandler.class);
        this.addEventHandler(BZEventType.USER_DISCONNECT, UserDisconnectHandler.class);
        BitZeroServer.getInstance().getTaskScheduler().scheduleAtFixedRate(
                () -> ConfigGame.reload(), Util.randInt(5, 20), 300, TimeUnit.SECONDS);
    }

    public void doLogin(final short s, final ISession iSession, final DataCmd dataCmd) throws BZException {
        if (s != 1) {
            return;
        }
        final LoginCmd cmd = new LoginCmd(dataCmd);
        final UserInfo info = GameUtils.getUserInfo(cmd.nickname, cmd.sessionKey);
        final User user = ExtensionUtility.instance().canLogin(info, "", iSession);
        if (info != null && user != null) {
            user.setProperty("dai_ly", info.getStatus());
        }
    }

    public void doLogin(final ISession iSession, final ISFSObject isfsObject) {
    }
}
