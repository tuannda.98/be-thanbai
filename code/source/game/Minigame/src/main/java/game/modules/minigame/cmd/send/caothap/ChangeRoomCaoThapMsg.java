// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.send.caothap;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class ChangeRoomCaoThapMsg extends BaseMsgEx
{
    public byte status;
    
    public ChangeRoomCaoThapMsg() {
        super(6006);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        bf.put(this.status);
        return this.packBuffer(bf);
    }
}
