package game.modules.minigame;

import bitzero.server.BitZeroServer;
import bitzero.server.core.BZEventParam;
import bitzero.server.core.BZEventType;
import bitzero.server.core.IBZEvent;
import bitzero.server.entities.User;
import bitzero.server.exceptions.BZException;
import bitzero.server.extensions.BaseClientRequestHandler;
import bitzero.server.extensions.data.DataCmd;
import bitzero.util.common.business.Debug;
import casio.king365.GU;
import casio.king365.util.KingUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vinplay.cardlib.models.Card;
import com.vinplay.dal.entities.config.SlotAutoConfig;
import com.vinplay.dal.service.BroadcastMessageService;
import com.vinplay.dal.service.CaoThapService;
import com.vinplay.dal.service.impl.BroadcastMessageServiceImpl;
import com.vinplay.dal.service.impl.CaoThapServiceImpl;
import com.vinplay.usercore.service.impl.GameConfigServiceImpl;
import com.vinplay.vbee.common.enums.Games;
import com.vinplay.vbee.common.response.ResultGameConfigResponse;
import com.vinplay.vbee.common.utils.DateTimeUtils;
import game.modules.minigame.cmd.rev.caothap.*;
import game.modules.minigame.cmd.send.caothap.ChangeRoomCaoThapMsg;
import game.modules.minigame.cmd.send.caothap.SubscribeCaoThapMsg;
import game.modules.minigame.cmd.send.caothap.UserInfoCaoThapMsg;
import game.modules.minigame.entities.BotMinigame;
import game.modules.minigame.entities.CaoThapInfo;
import game.modules.minigame.room.MGRoomCaoThap;
import game.modules.minigame.room.MGRoomMiniPoker;
import game.modules.minigame.utils.CaoThapUtils;
import game.modules.minigame.utils.GenerationMiniPoker;
import game.modules.minigame.utils.MiniGameUtils;
import game.utils.ConfigGame;
import game.utils.GameUtils;
import vn.yotel.yoker.util.Util;

import java.sql.SQLException;
import java.text.ParseException;
import java.time.LocalTime;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class CaoThapModule extends BaseClientRequestHandler {
    private static final Map<String, MGRoomCaoThap> rooms = new HashMap<>();
    private static String gameNameKey = "cao_thap";
    private final CaoThapService service;
    private final Runnable rewardDailyTask;
    private final Runnable botDailyTask;
    public long referenceId;

    private BroadcastMessageService broadcastMsgService;
    private static final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private static SlotAutoConfig slotAutoConfig;
    public static Calendar calNextTimeWinJackpot;

    public CaoThapModule() {
        this.service = new CaoThapServiceImpl();
        this.rewardDailyTask = new RewardDaily();
        this.botDailyTask = new BotDailyTask();
        this.referenceId = 0L;
    }

    public void init() {
        super.init();
        this.broadcastMsgService = new BroadcastMessageServiceImpl();
        long[] pots = new long[10];
        long[] funds = new long[10];
        try {
            pots = this.service.getPotCaoThap();
            Debug.trace("CAO THAP POTS: " + Arrays.toString(pots));
            funds = this.service.getFundCaoThap();
            Debug.trace("CAO THAP FUNDS: " + Arrays.toString(funds));
            this.referenceId = this.service.getLastReferenceId();
            Debug.trace("CAO THAP phien: " + this.referenceId);
        } catch (SQLException e) {
            Debug.trace("Get cao thap pot error ", e.getMessage());
        }
        this.rooms.put(gameNameKey + "_vin_1000", new MGRoomCaoThap(gameNameKey + "_vin_1000", (byte) 1, pots[0], funds[0], 1000));
        this.rooms.put(gameNameKey + "_vin_10000", new MGRoomCaoThap(gameNameKey + "_vin_10000", (byte) 1, pots[1], funds[1], 10000));
        this.rooms.put(gameNameKey + "_vin_50000", new MGRoomCaoThap(gameNameKey + "_vin_50000", (byte) 1, pots[2], funds[2], 50000));
        this.rooms.put(gameNameKey + "_vin_100000", new MGRoomCaoThap(gameNameKey + "_vin_100000", (byte) 1, pots[3], funds[3], 100000));
        this.rooms.put(gameNameKey + "_vin_500000", new MGRoomCaoThap(gameNameKey + "_vin_500000", (byte) 1, pots[4], funds[4], 500000));
        this.rooms.put(gameNameKey + "_xu_10000", new MGRoomCaoThap(gameNameKey + "_xu_10000", (byte) 0, pots[5], funds[5], 10000));
        this.rooms.put(gameNameKey + "_xu_100000", new MGRoomCaoThap(gameNameKey + "_xu_100000", (byte) 0, pots[6], funds[6], 100000));
        this.rooms.put(gameNameKey + "_xu_500000", new MGRoomCaoThap(gameNameKey + "_xu_500000", (byte) 0, pots[7], funds[7], 500000));
        this.rooms.put(gameNameKey + "_xu_1000000", new MGRoomCaoThap(gameNameKey + "_xu_1000000", (byte) 0, pots[8], funds[8], 1000000));
        this.rooms.put(gameNameKey + "_xu_5000000", new MGRoomCaoThap(gameNameKey + "_xu_5000000", (byte) 0, pots[9], funds[9], 5000000));
        this.rooms.forEach((key, value) -> value.savePot());
        try {
            final int remainTimeTraThuong = MiniGameUtils.calculateTimeRewardOnNextDay("");
            BitZeroServer.getInstance().getTaskScheduler().schedule(this.rewardDailyTask, remainTimeTraThuong, TimeUnit.SECONDS);
        } catch (ParseException e2) {
            Debug.trace("Calculate time reward Cao Thap error ", e2.getMessage());
        }
        Debug.trace("INIT CAO THAP DONE");
        this.getParentExtension().addEventListener(BZEventType.USER_DISCONNECT, this);
        this.scheduleBotCT();

        BitZeroServer.getInstance().getTaskScheduler().scheduleAtFixedRate(
                () -> this.gameLoop(), 10, 1, TimeUnit.SECONDS);
    }

    private void scheduleBotCT() {
        final long currentTime = System.currentTimeMillis() / 1000L;
        final long endToday = DateTimeUtils.getEndTimeToDayAsLong() / 1000L;
        final int n = (int) (endToday - currentTime);
        Debug.trace("current= " + currentTime);
        Debug.trace("end today= " + endToday);
        Debug.trace("n= " + n);
        BitZeroServer.getInstance().getTaskScheduler().schedule(this.botDailyTask, n + 100, TimeUnit.SECONDS);
        if (n >= 21600) {
            final int numBots = 5;
            final int[] times = new int[numBots];
            final Random rd = new Random();
            final int r = rd.nextInt(100);
            int indexTPSA = -1;
            if (r < 90) {
                indexTPSA = rd.nextInt(numBots);
            }
            for (int i = 0; i < numBots; ++i) {
                times[i] = rd.nextInt(n);
                boolean tpsA = false;
                if (i == indexTPSA) {
                    tpsA = true;
                }
                final BotTask botTask = new BotTask(tpsA);
                BitZeroServer.getInstance().getTaskScheduler().schedule(botTask, times[i], TimeUnit.SECONDS);
                Debug.trace("" + i + " = " + times[i]);
            }
        }
    }

    public void handleServerEvent(final IBZEvent ibzevent) throws BZException {
        if (ibzevent.getType() == BZEventType.USER_DISCONNECT) {
            final User user = (User) ibzevent.getParameter(BZEventParam.USER);
            this.userDis(user);
        }
    }

    private void userDis(final User user) {
        final MGRoomCaoThap room = (MGRoomCaoThap) user.getProperty("MGROOM_CAO_THAP_INFO");
        if (room != null) {
            if (room.getUsers().containsKey(user.getName())) {
                final CaoThapInfo info = room.getUsers().get(user.getName());
                info.setId(-1);
                room.getUsers().put(user.getName(), info);
            }
            room.quitRoom(user);
        }
    }

    public void handleClientRequest(final User user, final DataCmd dataCmd) {
        switch (dataCmd.getId()) {
            case 6004: {
                this.subScribeCaoThap(user, dataCmd);
                break;
            }
            case 6005: {
                this.unSubScribeCaoThap(user, dataCmd);
                break;
            }
            case 6006: {
                this.changeRoom(user, dataCmd);
                break;
            }
            case 6001: {
                if (GameUtils.disablePlayMiniGame(user)) {
                    return;
                }
                this.startPlayCaoThap(user, dataCmd);
                break;
            }
            case 6002: {
                if (GameUtils.disablePlayMiniGame(user)) {
                    return;
                }
                this.playCaoThap(user, dataCmd);
                break;
            }
            case 6007: {
                this.stopPlayCaoThap(user, dataCmd);
                break;
            }
        }
    }

    private void subScribeCaoThap(final User user, final DataCmd dataCmd) {
        final SubscribeCaoThapMsg msg = new SubscribeCaoThapMsg();
        byte roomId = -1;
        boolean play = false;
        int cntRoomPlaying = 0;
        for (byte i = 0; i < 10; ++i) {
            final MGRoomCaoThap room = this.getRoom(i);
            if (room.getUsers().containsKey(user.getName())) {
                final CaoThapInfo info = room.getUsers().get(user.getName());
                if (info.getId() == -1) {
                    info.setId(user.getId());
                    info.setUser(user);
                    room.getUsers().put(user.getName(), info);
                }
                if (info.getId() == user.getId()) {
                    roomId = i;
                    room.joinRoom(user);
                    room.updatePotToUser(user);
                    msg.status = 0;
                    msg.roomId = roomId;
                    this.send(msg, user);
                    final UserInfoCaoThapMsg msgInfo = new UserInfoCaoThapMsg();
                    msgInfo.numA = info.getNumA();
                    msgInfo.card = (byte) info.getCard().getCode();
                    msgInfo.money1 = info.getMoneyUp();
                    msgInfo.money2 = info.getMoney();
                    msgInfo.money3 = info.getMoneyDown();
                    msgInfo.time = info.getTime();
                    msgInfo.step = (byte) info.getStep();
                    msgInfo.referenceId = info.getReferenceId();
                    msgInfo.cards = CaoThapUtils.getCardStr(info.getCarryCards());
                    this.send(msgInfo, user);
                    return;
                }
                ++cntRoomPlaying;
            } else if (!play) {
                roomId = i;
                play = true;
            }
        }
        if (cntRoomPlaying >= 10) {
            msg.status = 1;
        } else {
            final MGRoomCaoThap room2 = this.getRoom(roomId);
            room2.joinRoom(user);
            room2.updatePotToUser(user);
            msg.status = 0;
        }
        msg.roomId = roomId;
        this.send(msg, user);
    }

    private void unSubScribeCaoThap(final User user, final DataCmd dataCmd) {
        final UnSubscribeCaoThapCmd cmd = new UnSubscribeCaoThapCmd(dataCmd);
        final MGRoomCaoThap room = this.getRoom(cmd.roomId);
        if (room != null) {
            room.quitRoom(user);
        } else {
            Debug.trace("CAO THAP UNSUBSCRIBE: room " + cmd.roomId + " not found");
        }
    }

    private void changeRoom(final User user, final DataCmd dataCmd) {
        final ChangeRoomCaoThapCmd cmd = new ChangeRoomCaoThapCmd(dataCmd);
        final MGRoomCaoThap roomLeaved = this.getRoom(cmd.roomLeavedId);
        final MGRoomCaoThap roomJoined = this.getRoom(cmd.roomJoinedId);
        final ChangeRoomCaoThapMsg msg = new ChangeRoomCaoThapMsg();
        if (roomLeaved != null && roomJoined != null) {
            if (roomLeaved.getUsers().containsKey(user.getName())) {
                final CaoThapInfo info = roomLeaved.getUsers().get(user.getName());
                if (info.getId() != -1 && info.getId() == user.getId()) {
                    msg.status = 2;
                    this.send(msg, user);
                } else {
                    roomLeaved.quitRoom(user);
                    roomJoined.joinRoom(user);
                    roomJoined.updatePotToUser(user);
                    msg.status = 0;
                    this.send(msg, user);
                }
            } else if (roomJoined.getUsers().containsKey(user.getName())) {
                final CaoThapInfo info = roomJoined.getUsers().get(user.getName());
                if (info.getId() == -1) {
                    info.setUser(user);
                    info.setId(user.getId());
                    roomJoined.getUsers().put(user.getName(), info);
                }
                if (info.getId() == user.getId()) {
                    roomLeaved.quitRoom(user);
                    roomJoined.joinRoom(user);
                    roomJoined.updatePotToUser(user);
                    msg.status = 1;
                    this.send(msg, user);
                    final UserInfoCaoThapMsg msg2 = new UserInfoCaoThapMsg();
                    msg2.numA = info.getNumA();
                    msg2.card = (byte) info.getCard().getCode();
                    msg2.money1 = info.getMoneyUp();
                    msg2.money2 = info.getMoney();
                    msg2.money3 = info.getMoneyDown();
                    msg2.time = info.getTime();
                    msg2.step = (byte) info.getStep();
                    msg2.referenceId = info.getReferenceId();
                    msg2.cards = CaoThapUtils.getCardStr(info.getCarryCards());
                    this.send(msg2, user);
                } else {
                    msg.status = 3;
                    this.send(msg, user);
                }
            } else {
                roomLeaved.quitRoom(user);
                roomJoined.joinRoom(user);
                roomJoined.updatePotToUser(user);
                msg.status = 0;
                this.send(msg, user);
            }
        } else {
            Debug.trace("CAO THAP: change room error, leaved= " + cmd.roomLeavedId + ", joined= " + cmd.roomJoinedId);
        }
    }

    private void startPlayCaoThap(final User user, final DataCmd dataCmd) {
        final StartPlayCaoThapCmd cmd = new StartPlayCaoThapCmd(dataCmd);
        final String roomName = this.getRoomName(cmd.moneyType, cmd.betValue);
        final MGRoomCaoThap room = this.rooms.get(roomName);
        if (room != null) {
            ++this.referenceId;
            room.startPlay(user, cmd.betValue, this.referenceId);
        } else {
            Debug.trace("CAO THAP: room " + roomName + " not found");
        }
    }

    private void playCaoThap(final User user, final DataCmd dataCmd) {
        final PlayCaoThapCmd cmd = new PlayCaoThapCmd(dataCmd);
        final String roomName = this.getRoomName(cmd.moneyType, cmd.betValue);
        final MGRoomCaoThap room = this.rooms.get(roomName);
        if (room != null) {
            room.play(user, cmd.choose);
        }
    }

    private void stopPlayCaoThap(final User user, final DataCmd dataCmd) {
        final StopPlayCaoThapCmd cmd = new StopPlayCaoThapCmd(dataCmd);
        final String roomName = this.getRoomName(cmd.moneyType, cmd.betValue);
        final MGRoomCaoThap room = this.rooms.get(roomName);
        if (room != null) {
            room.stopPlay(user);
        }
    }

    private String getRoomName(final short moneyType, final long baseBetting) {
        String moneyTypeStr = "xu";
        if (moneyType == 1) {
            moneyTypeStr = "vin";
        }
        return "cao_thap_" + moneyTypeStr + "_" + baseBetting;
    }

    private MGRoomCaoThap getRoom(final byte roomId) {
        final short moneyType = this.getMoneyTypeFromRoomId(roomId);
        final long baseBetting = this.getBaseBetting(roomId);
        final String roomName = this.getRoomName(moneyType, baseBetting);
        return this.rooms.get(roomName);
    }

    private short getMoneyTypeFromRoomId(final byte roomId) {
        if (0 <= roomId && roomId < 5) {
            return 1;
        }
        return 0;
    }

    private long getBaseBetting(final byte roomId) {
        switch (roomId) {
            case 0: {
                return 1000L;
            }
            case 1: {
                return 10000L;
            }
            case 2: {
                return 50000L;
            }
            case 3: {
                return 100000L;
            }
            case 4: {
                return 500000L;
            }
            case 5: {
                return 10000L;
            }
            case 6: {
                return 100000L;
            }
            case 7: {
                return 500000L;
            }
            case 8: {
                return 1000000L;
            }
            case 9: {
                return 5000000L;
            }
            default: {
                return 0L;
            }
        }
    }

    private int[] getTiLeTPS() {
        final String configTPS = ConfigGame.getValueString("cp_ti_le_tps");
        final String[] arr = configTPS.split(",");
        final int[] tiLe = new int[arr.length];
        for (int i = 0; i < arr.length; ++i) {
            tiLe[i] = Integer.parseInt(arr[i]);
        }
        return tiLe;
    }

    private void insertBotSuKien(final boolean thungPhaSanhA) {
        final String botName = BotMinigame.getRandomBot("vin");
        final int[] tiLe = this.getTiLeTPS();
        if (botName != null) {
            final Random rd = new Random();
            int n = rd.nextInt(5);
            long betValue = 1000L;
            long prize = 50000L;
            if (n == 0) {
                betValue = 10000L;
                n = rd.nextInt(950000);
                prize = n + 50000;
            } else {
                n = rd.nextInt(180000);
                prize = n + 20000;
            }
            int type = 0;
            if (thungPhaSanhA) {
                type = 3;
            } else {
                n = rd.nextInt(100);
                for (int i = 0; i < tiLe.length; ++i) {
                    if (n < tiLe[i]) {
                        type = i;
                        break;
                    }
                }
            }
            List<Card> cards = new ArrayList<>();
            switch (type) {
                case 0: {
                    cards = GenerationMiniPoker.randomTuQuy();
                    break;
                }
                case 1: {
                    cards = GenerationMiniPoker.randomThungPhaSanhNho();
                    break;
                }
                case 2: {
                    cards = GenerationMiniPoker.randomThungPhaSanhJDenK();
                    break;
                }
                default: {
                    cards = GenerationMiniPoker.randomThungPhaSanhA();
                    break;
                }
            }
            final StringBuilder cardsStr = new StringBuilder();
            for (final Card c : cards) {
                cardsStr.append(c.getCode()).append(",");
            }
            Debug.trace("CaoThap Su kien: Bot " + botName + " prize= " + prize + ", cards= " + cardsStr);
            this.service.insertBotEvent(botName, betValue, prize, cardsStr.toString());

            if (prize >= BroadcastMessageServiceImpl.MIN_MONEY) {
                this.broadcastMsgService.putMessage(Games.CAO_THAP.getId(), botName, prize);
            }
        }
    }

    private void loadSlotAutoConfig() throws Exception {
        GameConfigServiceImpl gameConfigServiceImpl = new GameConfigServiceImpl();
        List<ResultGameConfigResponse> list = gameConfigServiceImpl.getGameConfigAdmin("caothap_auto_config", "");
        if(list.size() == 0){
            KingUtil.printLog("CaoThapModule loadSlotAutoConfig() caothap_auto_config list size == 0");
            GU.sendOperation("CaoThapModule loadSlotAutoConfig() caothap_auto_config list size == 0");
        }
        ResultGameConfigResponse configObj = list.get(0);
        if (configObj == null)
            throw new Exception("slot_auto_config gameConfig not found");
        slotAutoConfig = gson.fromJson(configObj.value, SlotAutoConfig.class);
    }

    private SlotAutoConfig getSlotAutoConfig() throws Exception {
        if (slotAutoConfig == null)
            loadSlotAutoConfig();
        return slotAutoConfig;
    }

    /**
     * Kiểm tra xem thời điểm này có phải là thời điểm nổ jackpot ko?
     *
     * @return
     */
    private boolean isTimeToWinJackpot() throws Exception {
        Calendar calCurrentTime = Calendar.getInstance();
        return calCurrentTime.get(Calendar.HOUR_OF_DAY) == getNextTimeWinJackpot().get(Calendar.HOUR_OF_DAY)
                && calCurrentTime.get(Calendar.MINUTE) == getNextTimeWinJackpot().get(Calendar.MINUTE);
    }

    /**
     * Lấy thời gian trong tương lai mà jackpot sẽ nổ
     *
     * @return
     * @throws Exception
     */
    private Calendar getNextTimeWinJackpot() throws Exception {
        if (calNextTimeWinJackpot == null)
            genNextTimeWinJackpot();
        return calNextTimeWinJackpot;
    }

    /**
     * Tạo random 1 thời điểm trong tương lai sẽ nổ jackpot
     *
     * @throws Exception
     */
    private void genNextTimeWinJackpot() throws Exception {
        Calendar calCurrentTime = Calendar.getInstance();
        int minuteToNextWinJackpot = Util.getRandom(getSlotAutoConfig().getMinMinuteToNextBotWinJackpot(), getSlotAutoConfig().getMaxMinuteToNextBotWinJackpot());
        calCurrentTime.add(Calendar.MINUTE, minuteToNextWinJackpot);
        calCurrentTime.add(Calendar.SECOND, Util.getRandom(1, 59));
        calNextTimeWinJackpot = calCurrentTime;
        KingUtil.printLog("genNextTimeWinJackpot() calNextTimeWinJackpot: " + calNextTimeWinJackpot);
    }

    private void gameLoop() {
        try {
            // Load auto config mỗi tiếng 1 lần - vào thời điểm 0phút
            if (LocalTime.now().getMinute() == 0 && LocalTime.now().getSecond() == 0) {
                loadSlotAutoConfig();
            }

            // Tăng tiền hũ 100
            SlotAutoConfig config = getSlotAutoConfig();

            final MGRoomCaoThap room1k =  this.rooms.get(gameNameKey + "_vin_1000");
            room1k.increasePot(Util.getRandom(config.getSlot1000().getMinIncreasePerSecond(), config.getSlot1000().getMaxIncreasePerSecond()));
            room1k.savePot();
            final MGRoomCaoThap room10k =  this.rooms.get(gameNameKey + "_vin_10000");
            room10k.increasePot(Util.getRandom(config.getSlot10000().getMinIncreasePerSecond(), config.getSlot10000().getMaxIncreasePerSecond()));
            room10k.savePot();
            final MGRoomCaoThap room50k =  this.rooms.get(gameNameKey + "_vin_50000");
            room50k.increasePot(Util.getRandom(config.getSlot50000().getMinIncreasePerSecond(), config.getSlot50000().getMaxIncreasePerSecond()));
            room50k.savePot();
            final MGRoomCaoThap room100k =  this.rooms.get(gameNameKey + "_vin_100000");
            room100k.increasePot(Util.getRandom(config.getSlot100000().getMinIncreasePerSecond(), config.getSlot100000().getMaxIncreasePerSecond()));
            room100k.savePot();
            final MGRoomCaoThap room500k =  this.rooms.get(gameNameKey + "_vin_500000");
            room500k.increasePot(Util.getRandom(config.getSlot500000().getMinIncreasePerSecond(), config.getSlot500000().getMaxIncreasePerSecond()));
            room500k.savePot();

            // Xu li auto no hu
            if (isTimeToWinJackpot()) {
                // Xử lí nổ hũ
                ++referenceId;
                List<String> listBotName = BotMinigame.getBots(1, "vin");
                if(listBotName.size() == 0){
                    KingUtil.printLog("CaoThapModule gameLoop() listBotName == 0");
                    GU.sendOperation("CaoThapModule gameLoop() listBotName == 0");
                }
                String botName = listBotName.get(0);
                // Chọn room sẽ nổ hũ
                Random r = new Random();
                int randomVal = r.nextInt(100);
                if (randomVal < 2) {
                    // 1% Nổ hũ 500k
                    room500k.playWinJackpot(botName, 500000L, referenceId);
                } else if (randomVal < 15) {
                    room100k.playWinJackpot(botName, 100000L, referenceId);
                } else if (randomVal < 35) {
                    room50k.playWinJackpot(botName, 50000L, referenceId);
                } else if (randomVal < 65) {
                    room10k.playWinJackpot(botName, 10000L, referenceId);
                } else {
                    room1k.playWinJackpot(botName, 1000L, referenceId);
                }
                // Khởi tạo lịch nổ hũ lần tiếp theo
                genNextTimeWinJackpot();
            }
        } catch (Exception e) {
            KingUtil.printException("CaoThap Module Exception", e);
            GU.sendOperation("CaoThap Module Exception: " + KingUtil.printException(e));
        }
    }

    private final class RewardDaily implements Runnable {
        @Override
        public void run() {
            CaoThapUtils.reward();
            BitZeroServer.getInstance().getTaskScheduler().schedule(CaoThapModule.this.rewardDailyTask, 24, TimeUnit.HOURS);
            Debug.trace("Tra thuong Cao Thap");
        }
    }

    private final class BotDailyTask implements Runnable {
        @Override
        public void run() {
            CaoThapModule.this.scheduleBotCT();
        }
    }

    private final class BotTask implements Runnable {
        private boolean thungPhaSanhA;

        public BotTask(final boolean thungPhaSanhA) {
            this.thungPhaSanhA = false;
            this.thungPhaSanhA = thungPhaSanhA;
        }

        @Override
        public void run() {
            CaoThapModule.this.insertBotSuKien(this.thungPhaSanhA);
        }
    }
}
