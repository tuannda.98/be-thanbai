// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.utils;

import bitzero.server.entities.User;
import bitzero.server.extensions.data.BaseMsg;
import bitzero.util.ExtensionUtility;
import bitzero.util.common.business.Debug;
import com.vinplay.dal.service.impl.CacheServiceImpl;
import com.vinplay.vbee.common.exceptions.KeyNotFoundException;
import game.utils.ConfigGame;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MiniGameUtils {
    public static int calculateTimeRewardOnNextDay(String nextTimeRewarded) throws ParseException {
        if (nextTimeRewarded.isEmpty()) {
            nextTimeRewarded = "00:30:00";
        }
        final long startTime = System.currentTimeMillis();
        final SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd " + nextTimeRewarded);
        final SimpleDateFormat format2 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 1);
        final String rewardTimeStr = format.format(cal.getTime());
        final Date rewardTimeD = format2.parse(rewardTimeStr);
        final long rewardTime = rewardTimeD.getTime();
        final long remainTime = rewardTime - startTime;
        final long seconds = remainTime / 1000L;
        return (int) seconds;
    }

    public static Date getNextGioVang(final int[] input, final int lastDayFinishGioVang, final String configTimeX2) {
        final Calendar cal = Calendar.getInstance();
        final int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
        int nextDayGioVang = input[0];
        boolean nextWeek = true;
        for (int j : input) {
            if (dayOfWeek < j) {
                nextDayGioVang = j;
                nextWeek = false;
                break;
            }
            if (dayOfWeek == j && lastDayFinishGioVang != j) {
                nextDayGioVang = j;
                nextWeek = false;
                break;
            }
        }
        Debug.trace("Next day gio vang: " + nextDayGioVang + ", nextWeek= " + nextWeek);
        cal.set(Calendar.DAY_OF_WEEK, nextDayGioVang);
        if (nextWeek) {
            cal.add(Calendar.DATE, 7);
        }
        final SimpleDateFormat df1 = new SimpleDateFormat("dd/MM/yyyy");
        final String dayX2 = df1.format(cal.getTime());
        final String timeGioVang = ConfigGame.getValueString(configTimeX2);
        final String str = timeGioVang + " " + dayX2;
        final SimpleDateFormat df2 = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
        Date out = cal.getTime();
        try {
            out = df2.parse(str);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return out;
    }

    public static int calculateTimeX2(final int[] input, final int lastDayFinishGioVang, final String configTimeX2) {
        final Date nextDay = getNextGioVang(input, lastDayFinishGioVang, configTimeX2);
        return (int) (nextDay.getTime() - System.currentTimeMillis()) / 1000;
    }

    public static String calculateTimeX2AsString(final int[] input, final int lastDayFinishGioVang, final String configTimeX2) {
        final Date nextDay = getNextGioVang(input, lastDayFinishGioVang, configTimeX2);
        final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(nextDay);
    }

    public static int[] getX2Days(final String configName) {
        final String x2DaysStr = ConfigGame.getValueString(configName);
        final String[] arr = x2DaysStr.split(",");
        final int[] result = new int[arr.length];
        for (int i = 0; i < result.length; ++i) {
            result[i] = Integer.parseInt(arr[i]);
        }
        return result;
    }

    public static int getLastDayX2(final String configName) {
        final CacheServiceImpl cache = new CacheServiceImpl();
        try {
            return cache.getValueInt(configName);
        } catch (KeyNotFoundException e) {
            final int lastDay = ConfigGame.getIntValue(configName);
            saveLastDayX2(configName, lastDay);
            return lastDay;
        }
    }

    public static void saveLastDayX2(final String configName, final int lastDay) {
        final CacheServiceImpl cache = new CacheServiceImpl();
        cache.setValue(configName, lastDay);
    }

    public static void sendMessageToUser(final BaseMsg msg, final String username) {
        final User user = ExtensionUtility.getExtension().getApi().getUserByName(username);
        if (user != null) {
            ExtensionUtility.getExtension().send(msg, user);
        }
    }
}
