// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.mission.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class RewardMissionMsg extends BaseMsgEx
{
    public int prize;
    public long currentMoney;
    
    public RewardMissionMsg() {
        super(22001);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        bf.putInt(this.prize);
        bf.putLong(this.currentMoney);
        return this.packBuffer(bf);
    }
}
