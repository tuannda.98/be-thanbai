// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.rev;

import java.nio.ByteBuffer;
import bitzero.server.extensions.data.DataCmd;
import bitzero.server.extensions.data.BaseCmd;

public class TanLocCMD extends BaseCmd
{
    public long money;
    
    public TanLocCMD(final DataCmd dataCmd) {
        super(dataCmd);
        this.unpackData();
    }
    
    public void unpackData() {
        final ByteBuffer br = this.makeBuffer();
        this.money = this.readLong(br);
    }
}
