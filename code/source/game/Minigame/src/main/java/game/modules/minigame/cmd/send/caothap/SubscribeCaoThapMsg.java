// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.send.caothap;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class SubscribeCaoThapMsg extends BaseMsgEx
{
    public byte status;
    public byte roomId;
    
    public SubscribeCaoThapMsg() {
        super(6004);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        bf.put(this.status);
        bf.put(this.roomId);
        return this.packBuffer(bf);
    }
}
