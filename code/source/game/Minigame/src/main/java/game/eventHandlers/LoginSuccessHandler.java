// 
// Decompiled by Procyon v0.5.36
// 

package game.eventHandlers;

import bitzero.util.ExtensionUtility;
import bitzero.server.exceptions.BZException;
import bitzero.server.core.BZEventParam;
import bitzero.server.entities.User;
import bitzero.server.core.IBZEvent;
import bitzero.server.extensions.BaseServerEventHandler;
import casio.king365.core.HCMap;
import com.hazelcast.core.IMap;

public class LoginSuccessHandler extends BaseServerEventHandler
{
    public void handleServerEvent(final IBZEvent ibzevent) throws BZException {
        this.onLoginSuccess((User)ibzevent.getParameter(BZEventParam.USER));
    }
    
    private void onLoginSuccess(final User user) {
        ExtensionUtility.instance().sendLoginOK(user);

        // Add to user online map
        IMap<String, Boolean> connectedMap = HCMap.getConnectedMinigameMap();
        connectedMap.put(user.getName(), true);
    }
}
