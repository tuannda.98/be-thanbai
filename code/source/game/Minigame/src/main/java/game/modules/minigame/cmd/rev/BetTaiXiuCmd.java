// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.rev;

import java.nio.ByteBuffer;
import bitzero.server.extensions.data.DataCmd;
import bitzero.server.extensions.data.BaseCmd;

public class BetTaiXiuCmd extends BaseCmd
{
    public int userId;
    public long referenceId;
    public long betValue;
    public short moneyType;
    public short betSide;
    public short inputTime;
    
    public BetTaiXiuCmd(final DataCmd dataCmd) {
        super(dataCmd);
        this.unpackData();
    }
    
    public void unpackData() {
        final ByteBuffer buffer = this.makeBuffer();
        this.userId = this.readInt(buffer);
        this.referenceId = buffer.getLong();
        this.betValue = buffer.getLong();
        this.moneyType = this.readShort(buffer);
        this.betSide = this.readShort(buffer);
        this.inputTime = this.readShort(buffer);
    }
}
