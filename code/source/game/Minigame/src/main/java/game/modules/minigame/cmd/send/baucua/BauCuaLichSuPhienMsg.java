// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.send.baucua;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class BauCuaLichSuPhienMsg extends BaseMsgEx
{
    public String data;
    
    public BauCuaLichSuPhienMsg() {
        super(5010);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        this.putStr(bf, this.data);
        return this.packBuffer(bf);
    }
}
