// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class BetTaiXiuMsg extends BaseMsgEx
{
    public long currentMoney;
    
    public BetTaiXiuMsg() {
        super(2110);
    }
    
    public byte[] createData() {
        final ByteBuffer buffer = this.makeBuffer();
        buffer.putLong(this.currentMoney);
        return this.packBuffer(buffer);
    }
}
