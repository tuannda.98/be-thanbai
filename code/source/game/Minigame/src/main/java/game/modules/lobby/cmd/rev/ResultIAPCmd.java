// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.lobby.cmd.rev;

import java.nio.ByteBuffer;
import bitzero.server.extensions.data.DataCmd;
import bitzero.server.extensions.data.BaseCmd;

public class ResultIAPCmd extends BaseCmd
{
    public String signedData;
    public String signature;
    
    public ResultIAPCmd(final DataCmd dataCmd) {
        super(dataCmd);
        this.unpackData();
    }
    
    public void unpackData() {
        final ByteBuffer bf = this.makeBuffer();
        this.signedData = this.readString(bf);
        this.signature = this.readString(bf);
    }
}
