// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.lobby.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class GetEventVPInfoMsg extends BaseMsgEx
{
    public byte status;
    public long time;
    
    public GetEventVPInfoMsg() {
        super(20039);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        bf.put(this.status);
        bf.putLong(this.time);
        return this.packBuffer(bf);
    }
}
