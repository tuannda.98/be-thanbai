// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.lobby.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class BroadcastMessageMsg extends BaseMsgEx
{
    public String message;
    
    public BroadcastMessageMsg() {
        super(20100);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        this.putStr(bf, this.message);
        return this.packBuffer(bf);
    }
}
