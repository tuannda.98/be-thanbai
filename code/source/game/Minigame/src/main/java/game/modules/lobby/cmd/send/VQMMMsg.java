// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.lobby.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class VQMMMsg extends BaseMsgEx
{
    public String prizeVin;
    public String prizeXu;
    public String prizeSlot;
    public short remainCount;
    public long currentMoneyVin;
    public long currentMoneyXu;
    
    public VQMMMsg() {
        super(20042);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        this.putStr(bf, this.prizeVin);
        this.putStr(bf, this.prizeXu);
        this.putStr(bf, this.prizeSlot);
        bf.putShort(this.remainCount);
        bf.putLong(this.currentMoneyVin);
        bf.putLong(this.currentMoneyXu);
        return this.packBuffer(bf);
    }
}
