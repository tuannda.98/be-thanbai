package game.modules.minigame;

import bitzero.server.BitZeroServer;
import bitzero.server.core.BZEventParam;
import bitzero.server.core.BZEventType;
import bitzero.server.core.IBZEvent;
import bitzero.server.entities.User;
import bitzero.server.exceptions.BZException;
import bitzero.server.extensions.BaseClientRequestHandler;
import bitzero.server.extensions.data.BaseMsg;
import bitzero.server.extensions.data.DataCmd;
import bitzero.util.ExtensionUtility;
import bitzero.util.common.business.Debug;
import casio.king365.GU;
import casio.king365.core.HCMap;
import casio.king365.util.KingUtil;
import com.hazelcast.core.IMap;
import com.vinplay.dal.service.BauCuaService;
import com.vinplay.dal.service.BroadcastMessageService;
import com.vinplay.dal.service.CacheService;
import com.vinplay.dal.service.MiniGameService;
import com.vinplay.dal.service.impl.BauCuaServiceImpl;
import com.vinplay.dal.service.impl.BroadcastMessageServiceImpl;
import com.vinplay.dal.service.impl.CacheServiceImpl;
import com.vinplay.dal.service.impl.MiniGameServiceImpl;
import com.vinplay.usercore.service.UserService;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.response.MoneyResponse;
import com.vinplay.vbee.common.statics.TransType;
import com.vinplay.vbee.common.utils.CommonUtils;
import com.vinplay.vbee.common.utils.DateTimeUtils;
import game.modules.lobby.cmd.send.BroadcastMessageMsg;
import game.modules.minigame.cmd.rev.baucua.BetBauCuaCmd;
import game.modules.minigame.cmd.rev.baucua.ChangeRoomBauCuaCmd;
import game.modules.minigame.cmd.rev.baucua.SubscribeBauCuaCmd;
import game.modules.minigame.cmd.rev.baucua.UnsubscribeBauCuaCmd;
import game.modules.minigame.cmd.send.baucua.StartNewGameBauCuaMsg;
import game.modules.minigame.room.MGRoom;
import game.modules.minigame.room.MGRoomBauCua;
import game.modules.minigame.utils.BauCuaUtils;
import game.modules.minigame.utils.MiniGameUtils;
import game.utils.GameUtils;

import java.sql.SQLException;
import java.text.ParseException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class BauCuaModule extends BaseClientRequestHandler {
    private final Map<String, MGRoom> rooms;
    private long referenceId;
    private boolean isBettingRound;
    private byte count;
    private long[] funds;
    private boolean serverReady;
    private final BauCuaService bcService;
    private final MiniGameService mgService;
    CacheService cacheService = new CacheServiceImpl();
    private final Runnable serverReadyTask;
    private final Runnable rewardToiChonCaTask;
    private final BroadcastMessageService broadcastMsg;
    private static final int END_BET_TIME = 30;
    private static final int END_MATCH_TIME = 45;
    private UserService userService;

    public BauCuaModule() {
        this.rooms = new HashMap<>();
        this.count = 0;
        this.funds = new long[6];
        this.serverReady = false;
        this.bcService = new BauCuaServiceImpl();
        this.mgService = new MiniGameServiceImpl();
        this.serverReadyTask = new ServerReadyTask();
        this.rewardToiChonCaTask = new RewardToiChonCaTask();
        this.broadcastMsg = new BroadcastMessageServiceImpl();
        this.userService = new UserServiceImpl();
    }

    public void init() {
        super.init();
        this.loadData();
        this.rooms.put("BauCua_vin_1000", new MGRoomBauCua("BauCua_vin_1000", 1000, (byte) 1, (byte) 0, this.funds[0]));
        this.rooms.put("BauCua_vin_10000", new MGRoomBauCua("BauCua_vin_10000", 10000, (byte) 1, (byte) 1, this.funds[1]));
        this.rooms.put("BauCua_vin_100000", new MGRoomBauCua("BauCua_vin_100000", 100000, (byte) 1, (byte) 2, this.funds[2]));
        this.rooms.put("BauCua_xu_10000", new MGRoomBauCua("BauCua_xu_10000", 10000, (byte) 0, (byte) 3, this.funds[3]));
        this.rooms.put("BauCua_xu_100000", new MGRoomBauCua("BauCua_xu_100000", 100000, (byte) 0, (byte) 4, this.funds[4]));
        this.rooms.put("BauCua_xu_1000000", new MGRoomBauCua("BauCua_xu_1000000", 1000000, (byte) 0, (byte) 5, this.funds[5]));

        BitZeroServer.getInstance().getTaskScheduler().scheduleAtFixedRate(() -> {
            LocalDateTime start = LocalDateTime.now();
            gameLoop();
            Duration duration = Duration.between(start, LocalDateTime.now());
            long durationInMs = duration.toMillis();
            if (durationInMs > 900) {
                GU.sendOperation("[CẢNH BÁO BẦU CUA]Xử lý game loop chậm - count: " + this.count + " - ref:" + this.referenceId
                        + " - duration: " + durationInMs + " ms.");
            }
        }, 5, 1, TimeUnit.SECONDS);
        BitZeroServer.getInstance().getTaskScheduler().schedule(this.serverReadyTask, 10, TimeUnit.SECONDS);
        this.getParentExtension().addEventListener(BZEventType.USER_DISCONNECT, this);
        try {
            final int remainTimeRewardToiChonCa = MiniGameUtils.calculateTimeRewardOnNextDay("");
            BitZeroServer.getInstance().getTaskScheduler().schedule(this.rewardToiChonCaTask, remainTimeRewardToiChonCa, TimeUnit.SECONDS);
        } catch (ParseException e) {
            Debug.trace("Calculate time reward Toi chon ca error ", e.getMessage());
        }

        // Hoàn tiền cho các tài khoản đặt cược ván tài xỉu bị reset đột ngột
        IMap<String, Long> usersBetBauCua = HCMap.getUsersBetBauCua();
        KingUtil.printLog("hoan tien bau cua voi user bi loi phien, usersBetBauCua size: "+usersBetBauCua.size());
        for (Map.Entry<String, Long> entry : usersBetBauCua.entrySet()) {
            String nickname = entry.getKey();
            Long betvalue = entry.getValue();
            KingUtil.printLog("BauCuaModule restore money user: "+nickname+", number: "+betvalue);
            MoneyResponse res = userService.updateMoney(nickname, betvalue, "vin", "BauCua", "Bầu Cua: Hoàn tiền phiên lỗi", "Hoàn tiền phiên lỗi", 0L, 0L, TransType.NO_VIPPOINT);
        }
        usersBetBauCua.clear();
    }

    private void loadData() {
        try {
            this.referenceId = this.mgService.getReferenceId(3);
            this.funds = this.mgService.getFunds("BauCua");
        } catch (SQLException e) {
            Debug.trace("LOAD DATA BAU CUA ERROR: " + e.getMessage());
        }
        Debug.trace("BAU CUA referenceId: " + this.referenceId);
        Debug.trace("BAU CUA FUND: " + CommonUtils.arrayLongToString(this.funds));
    }

    public void handleServerEvent(final IBZEvent ibzevent) throws BZException {
        if (ibzevent.getType() == BZEventType.USER_DISCONNECT) {
            final User user = (User) ibzevent.getParameter(BZEventParam.USER);
            this.userDis(user);
        }
    }

    private void userDis(final User user) {
        final MGRoomBauCua room = (MGRoomBauCua) user.getProperty("MGROOM_BAU_CUA_INFO");
        if (room != null) {
            room.quitRoom(user);
        }
    }

    public void handleClientRequest(final User user, final DataCmd dataCmd) {
        if (!this.serverReady) {
            Debug.trace("Server bau cua not ready, try again!");
            return;
        }
        switch (dataCmd.getId()) {
            case 5001: {
                this.subscribeBauCua(user, dataCmd);
                break;
            }
            case 5002: {
                this.unsubscribeBauCua(user, dataCmd);
                break;
            }
            case 5003: {
                this.changeRoomBauCua(user, dataCmd);
                break;
            }
            case 5004: {
                if (GameUtils.disablePlayMiniGame(user)) {
                    return;
                }
                this.betBauCua(user, dataCmd);
                break;
            }
        }
    }

    private void gameLoop() {
        ++this.count;
        if (this.count <= END_BET_TIME) {
            for (final MGRoom entry : this.rooms.values()) {
                final MGRoomBauCua room = (MGRoomBauCua) entry;
                room.updateBauCuaPerSecond(this.getRemainTime(), this.isBettingRound);
                room.botBet(END_BET_TIME - this.count, this.isBettingRound);
            }
        }
        switch (this.count) {
            case END_BET_TIME: {
                this.isBettingRound = false;
                break;
            }
            case (END_BET_TIME + 1): {
                KingUtil.printLog("BC gameLoop begin 61s");
                this.generateResult();
                KingUtil.printLog("BC gameLoop end 61s");
                break;
            }
            case (END_BET_TIME + 5): {
                KingUtil.printLog("BC gameLoop begin 65s");
                final CalculatePrizeTask task = new CalculatePrizeTask();
                BitZeroServer.getInstance().getTaskScheduler().schedule(task, 10, TimeUnit.MILLISECONDS);
                KingUtil.printLog("BC gameLoop end 65s");
                break;
            }
            case (END_BET_TIME + 10): {
                KingUtil.printLog("BC gameLoop begin 90s");
                this.startNewRound();
                KingUtil.printLog("BC gameLoop end 90s");
                break;
            }
            default:
                break;
        }
        if (count > END_MATCH_TIME) {
            KingUtil.printLog("BC gameLoop begin > 90s");
            this.startNewRound();
            GU.sendOperation(this.getClass().getName() + " game loop count > 90");
            KingUtil.printLog("BC gameLoop end > 90s");
            KingUtil.printLog("BC gameLoop end > 90s");
        }

        if (count % 5 == 0)
            this.broadcastMessage();
    }

    private void startNewRound() {
        Debug.trace("START NEW ROUND BAU CUA");
        ++this.referenceId;
        final StartNewGameBauCuaMsg msg = new StartNewGameBauCuaMsg();
        msg.referenceId = this.referenceId;
        this.sendMessageBauCuaNewThread(msg);
        for (final MGRoom entry : this.rooms.values()) {
            final MGRoomBauCua room = (MGRoomBauCua) entry;
            room.startNewGame(this.referenceId);
        }
        this.count = 0;
        this.isBettingRound = true;
        this.saveReferences();
    }

    private void subscribeBauCua(final User user, final DataCmd dataCmd) {
        final SubscribeBauCuaCmd cmd = new SubscribeBauCuaCmd(dataCmd);
        final MGRoomBauCua room = this.getRoom(cmd.roomId);
        if (room != null) {
            room.joinRoom(user);
            room.updateBauCuaInfoToUser(user, this.getRemainTime(), this.isBettingRound);
        }
    }

    private void unsubscribeBauCua(final User user, final DataCmd dataCmd) {
        final UnsubscribeBauCuaCmd cmd = new UnsubscribeBauCuaCmd(dataCmd);
        final MGRoomBauCua room = this.getRoom(cmd.roomId);
        if (room != null) {
            room.quitRoom(user);
        }
    }

    private void changeRoomBauCua(final User user, final DataCmd dataCmd) {
        final ChangeRoomBauCuaCmd cmd = new ChangeRoomBauCuaCmd(dataCmd);
        final MGRoomBauCua roomLeaved = this.getRoom(cmd.roomLeavedId);
        final MGRoomBauCua roomJoined = this.getRoom(cmd.roomJoinedId);
        if (roomLeaved != null && roomJoined != null) {
            roomLeaved.quitRoom(user);
            roomJoined.joinRoom(user);
            roomJoined.updateBauCuaInfoToUser(user, this.getRemainTime(), this.isBettingRound);
        }
    }

    private void betBauCua(final User user, final DataCmd dataCmd) {
        // Kiểm tra xem có đang cho mở bet Tài Xỉu ko?
        try {
            if(cacheService.checkKeyExist("allow_bet_baucua")){
                String allowBetTaixiu = cacheService.getValueStr("allow_bet_baucua");
                if(allowBetTaixiu.equals("false"))
                    return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        final BetBauCuaCmd cmd = new BetBauCuaCmd(dataCmd);
        final MGRoomBauCua room = (MGRoomBauCua) user.getProperty("MGROOM_BAU_CUA_INFO");
        room.bet(user, cmd.betValue, this.isBettingRound);
    }

    private void generateResult() {
        for (final MGRoom entry : this.rooms.values()) {
            final MGRoomBauCua room = (MGRoomBauCua) entry;
            room.generateResult();
        }
    }

    private void calculateResult() {
        try {
            KingUtil.printLog("calculateResult() 1");
            for (final MGRoom entry : this.rooms.values()) {
                final MGRoomBauCua room = (MGRoomBauCua) entry;
                room.calculatePrizes();
            }
            KingUtil.printLog("calculateResult() 2");
            if (this.referenceId % 50L == 0L) {
                this.bcService.updateAllTop();
            }
            KingUtil.printLog("calculateResult() 3");
            // Xóa danh sách đặt cược TX trong cache
            IMap<String, Long> usersBetBauCua = HCMap.getUsersBetBauCua();
            usersBetBauCua.clear();
        } catch (Exception e) {
            KingUtil.printException("BauCuaModule calculateResult() Exception", e);
        }
    }

    private byte getRemainTime() {
        if (this.isBettingRound) {
            return (byte) (END_BET_TIME - this.count);
        }
        return (byte) (END_MATCH_TIME - this.count);
    }

    private String getRoomName(final short moneyType, final long baseBetting) {
        String moneyTypeStr = "xu";
        if (moneyType == 1) {
            moneyTypeStr = "vin";
        }
        return "BauCua_" + moneyTypeStr + "_" + baseBetting;
    }

    private MGRoomBauCua getRoom(final byte roomId) {
        final short moneyType = this.getMoneyTypeFromRoomId(roomId);
        final long baseBetting = this.getBaseBetting(roomId);
        final String roomName = this.getRoomName(moneyType, baseBetting);
        return (MGRoomBauCua) this.rooms.get(roomName);
    }

    private short getMoneyTypeFromRoomId(final byte roomId) {
        if (0 <= roomId && roomId < 3) {
            return 1;
        }
        return 0;
    }

    private long getBaseBetting(final byte roomId) {
        switch (roomId) {
            case 0: {
                return 1000L;
            }
            case 1:
            case 3: {
                return 10000L;
            }
            case 2:
            case 4: {
                return 100000L;
            }
            case 5: {
                return 1000000L;
            }
            default: {
                return 0L;
            }
        }
    }

    private void saveReferences() {
        try {
            this.mgService.saveReferenceId(this.referenceId, 3);
        } catch (SQLException e) {
            Debug.trace("Save reference error " + e.getMessage());
        }
    }

    private void sendMessageToBauCua(final BaseMsg msg) {
        for (final MGRoom room : this.rooms.values()) {
            room.sendMessageToRoom(msg);
        }
    }

    private void broadcastMessage() {
        try {
            final String message = this.broadcastMsg.toJson();
            final BroadcastMessageMsg msg = new BroadcastMessageMsg();
            msg.message = message;
            final List<User> users = ExtensionUtility.globalUserManager.getAllUsers();
            if (users != null && !users.isEmpty()) {
                this.send(msg, users.stream().map(User::getSession).collect(Collectors.toList()));
            }
            this.broadcastMsg.clearMessage();
        } catch (Exception e) {
            KingUtil.printException("LobbyModule broadcastMessage()", e);
        }
    }

    private void sendMessageBauCuaNewThread(final BaseMsg msg) {
        final SendMessageToTXThread t = new SendMessageToTXThread(msg);
        t.start();
    }

    private final class SendMessageToTXThread extends Thread {
        private final BaseMsg msg;

        private SendMessageToTXThread(final BaseMsg msg) {
            this.msg = msg;
        }

        @Override
        public void run() {
            BauCuaModule.this.sendMessageToBauCua(this.msg);
        }
    }

    private final class ServerReadyTask implements Runnable {
        @Override
        public void run() {
            if (!BauCuaModule.this.serverReady) {
                Debug.trace("START BAU CUA");
                BauCuaModule.this.serverReady = true;
                BauCuaModule.this.startNewRound();
            }
        }
    }

    private final class RewardToiChonCaTask implements Runnable {
        @Override
        public void run() {
            BauCuaUtils.rewardToiChonCa();
            BitZeroServer.getInstance().getTaskScheduler().schedule(BauCuaModule.this.rewardToiChonCaTask, 24, TimeUnit.HOURS);
            Debug.trace("Tra thuong Toi chon ca");
        }
    }

    private final class CalculatePrizeTask implements Runnable {
        @Override
        public void run() {
            BauCuaModule.this.calculateResult();
        }
    }
}
