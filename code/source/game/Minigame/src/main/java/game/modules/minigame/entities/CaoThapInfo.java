// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.entities;

import java.util.List;
import com.vinplay.cardlib.models.Card;
import com.vinplay.cardlib.models.Deck;
import bitzero.server.entities.User;

public class CaoThapInfo
{
    private int id;
    private User user;
    private long referenceId;
    private Deck deck;
    private Card card;
    private short step;
    private short time;
    private long moneyUp;
    private long money;
    private long moneyDown;
    private byte numA;
    private List<Card> carryCards;
    
    public CaoThapInfo() {
    }
    
    public CaoThapInfo(final User user, final long referenceId, final Deck deck, final Card card, final short step, final short time, final long money, final byte numA, final List<Card> carryCards, final long moneyUp, final long moneyDown, final int id) {
        this.user = user;
        this.referenceId = referenceId;
        this.money = money;
        this.deck = deck;
        this.step = step;
        this.time = time;
        this.numA = numA;
        this.card = card;
        this.carryCards = carryCards;
        this.moneyUp = moneyUp;
        this.moneyDown = moneyDown;
        this.id = id;
    }
    
    public int getId() {
        return this.id;
    }
    
    public void setId(final int id) {
        this.id = id;
    }
    
    public List<Card> getCarryCards() {
        return this.carryCards;
    }
    
    public void setCarryCards(final List<Card> carryCards) {
        this.carryCards = carryCards;
    }
    
    public User getUser() {
        return this.user;
    }
    
    public void setUser(final User user) {
        this.user = user;
    }
    
    public short getStep() {
        return this.step;
    }
    
    public void setStep(final short step) {
        this.step = step;
    }
    
    public short getTime() {
        return this.time;
    }
    
    public void setTime(final short time) {
        this.time = time;
    }
    
    public Deck getDeck() {
        return this.deck;
    }
    
    public void setDeck(final Deck deck) {
        this.deck = deck;
    }
    
    public long getMoney() {
        return this.money;
    }
    
    public void setMoney(final long money) {
        this.money = money;
    }
    
    public long getMoneyUp() {
        return this.moneyUp;
    }
    
    public void setMoneyUp(final long moneyUp) {
        this.moneyUp = moneyUp;
    }
    
    public long getMoneyDown() {
        return this.moneyDown;
    }
    
    public void setMoneyDown(final long moneyDown) {
        this.moneyDown = moneyDown;
    }
    
    public byte getNumA() {
        return this.numA;
    }
    
    public void setNumA(final byte numA) {
        this.numA = numA;
    }
    
    public long getReferenceId() {
        return this.referenceId;
    }
    
    public void setReferenceId(final long referenceId) {
        this.referenceId = referenceId;
    }
    
    public Card getCard() {
        return this.card;
    }
    
    public void setCard(final Card card) {
        this.card = card;
    }
}
