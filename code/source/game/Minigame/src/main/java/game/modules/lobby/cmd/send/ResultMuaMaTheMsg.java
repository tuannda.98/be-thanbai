// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.lobby.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class ResultMuaMaTheMsg extends BaseMsgEx
{
    public long currentMoney;
    public String softpin;
    
    public ResultMuaMaTheMsg() {
        super(20035);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        bf.putLong(this.currentMoney);
        this.putStr(bf, this.softpin);
        return this.packBuffer(bf);
    }
}
