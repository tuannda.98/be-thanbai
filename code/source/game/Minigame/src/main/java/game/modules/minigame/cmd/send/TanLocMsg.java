// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class TanLocMsg extends BaseMsgEx
{
    public short result;
    public long currentMoney;
    
    public TanLocMsg() {
        super(2118);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        bf.putShort(this.result);
        bf.putLong(this.currentMoney);
        return this.packBuffer(bf);
    }
}
