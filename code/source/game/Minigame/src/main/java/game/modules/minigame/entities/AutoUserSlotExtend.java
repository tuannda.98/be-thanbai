// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.entities;

import bitzero.server.entities.User;

public class AutoUserSlotExtend
{
    private User user;
    private int count;
    private long bet;
    private int maxCount;
    
    public AutoUserSlotExtend(final User user, final long bet) {
        this.maxCount = 6;
        this.user = user;
        this.count = 0;
        this.bet = bet;
    }
    
    public void setUser(final User user) {
        this.user = user;
    }
    
    public User getUser() {
        return this.user;
    }
    
    public void setCount(final int count) {
        this.count = count;
    }
    
    public int getCount() {
        return this.count;
    }
    
    public long getBet() {
        return this.bet;
    }
    
    public void setBet(final long bet) {
        this.bet = bet;
    }
    
    public boolean incCount() {
        ++this.count;
        if (this.count == this.maxCount) {
            this.count = 0;
            return true;
        }
        return false;
    }
    
    public void setMaxCount(final int maxCount) {
        this.maxCount = maxCount;
    }
}
