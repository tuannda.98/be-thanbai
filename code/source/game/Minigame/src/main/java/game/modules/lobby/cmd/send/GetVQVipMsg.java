// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.lobby.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class GetVQVipMsg extends BaseMsgEx
{
    public short remainCount;
    
    public GetVQVipMsg() {
        super(20043);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        bf.putShort(this.remainCount);
        return this.packBuffer(bf);
    }
}
