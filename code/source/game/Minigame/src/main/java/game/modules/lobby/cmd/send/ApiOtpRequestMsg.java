// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.lobby.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class ApiOtpRequestMsg extends BaseMsgEx
{
    public String requestId;
    public String url;
    public long time;
    public int numFail;
    
    public ApiOtpRequestMsg() {
        super(20040);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        this.putStr(bf, this.requestId);
        this.putStr(bf, this.url);
        bf.putLong(this.time);
        bf.putInt(this.numFail);
        return this.packBuffer(bf);
    }
}
