// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class TaiXiuInfoMsg extends BaseMsgEx
{
    public short gameId;
    public short moneyType;
    public long referenceId;
    public short remainTime;
    public boolean bettingState;
    public long potTai;
    public long potXiu;
    public long myBetTai;
    public long myBetXiu;
    public short dice1;
    public short dice2;
    public short dice3;
    public short remainTimeRutLoc;
    
    public TaiXiuInfoMsg() {
        super(2111);
        this.dice1 = 0;
        this.dice2 = 0;
        this.dice3 = 0;
        this.remainTimeRutLoc = 0;
    }
    
    public byte[] createData() {
        final ByteBuffer buffer = this.makeBuffer();
        buffer.putShort(this.gameId);
        buffer.putShort(this.moneyType);
        buffer.putLong(this.referenceId);
        buffer.putShort(this.remainTime);
        this.putBoolean(buffer, this.bettingState);
        buffer.putLong(this.potTai);
        buffer.putLong(this.potXiu);
        buffer.putLong(this.myBetTai);
        buffer.putLong(this.myBetXiu);
        buffer.putShort(this.dice1);
        buffer.putShort(this.dice2);
        buffer.putShort(this.dice3);
        buffer.putShort(this.remainTimeRutLoc);
        return this.packBuffer(buffer);
    }
}
