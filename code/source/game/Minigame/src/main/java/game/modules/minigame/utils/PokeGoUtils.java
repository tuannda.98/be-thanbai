// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.utils;

import com.vinplay.dal.service.impl.CacheServiceImpl;
import com.vinplay.vbee.common.exceptions.KeyNotFoundException;
import game.modules.minigame.entities.pokego.*;
import game.utils.ConfigGame;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Random;

public class PokeGoUtils
{
    private static Logger logger;
    private static String FORMAT_PLAY_POKE_GO;
    
    public static void log(final long referenceId, final String username, final int betValue, final String matrix, final short result, final short moneyType, final long handleTime, final String ratioTime, final String timeLog) {
        final String matrixStr = matrix.replaceAll(",", " ");
        PokeGoUtils.logger.debug(String.format(PokeGoUtils.FORMAT_PLAY_POKE_GO, referenceId, username, betValue, matrixStr, result, moneyType, handleTime, ratioTime, timeLog));
    }
    
    public static Item[][] generateMatrix() {
        final Items items = new Items();
        final Item[][] matrix = new Item[3][3];
        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 3; ++j) {
                matrix[i][j] = items.random();
            }
        }
        return matrix;
    }
    
    public static Item[][] generateMatrixNoHu(final String[] lineArr) {
        final Item[][] matrix = new Item[3][3];
        final Random rd = new Random();
        final int n = rd.nextInt(lineArr.length);
        final int indexLineNoHu = Integer.parseInt(lineArr[n]) - 1;
        final Lines lines = new Lines();
        final Items items = new Items();
        final Line lineNoHu = lines.get(indexLineNoHu);
        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 3; ++j) {
                boolean genRandom = true;
                for (int k = 0; k < lineNoHu.getCells().size(); ++k) {
                    if (i == lineNoHu.getCell(k).getRow()) {
                        if (j == lineNoHu.getCell(k).getCol()) {
                            genRandom = false;
                            matrix[i][j] = Item.POKER_BALL;
                        }
                    }
                }
                if (genRandom) {
                    matrix[i][j] = items.random();
                }
            }
        }
        return matrix;
    }
    
    public static String matrixToString(final Item[][] matrix) {
        final StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 3; ++j) {
                builder.append(",");
                builder.append(matrix[i][j].getId());
            }
        }
        if (builder.length() > 0) {
            builder.deleteCharAt(0);
        }
        return builder.toString();
    }
    
    public static Line getLine(final Lines lines, final Item[][] matrix, final int lineIndex) {
        final Line line = lines.get(lineIndex - 1);
        for (final Cell cell : line.getCells()) {
            final Item itemInMatrix = matrix[cell.getRow()][cell.getCol()];
            cell.setItem(itemInMatrix);
        }
        return line;
    }
    
    public static void calculateLine(final Line line, final List<Award> awardList) {
        for (int i = 0; i < line.getCells().size(); ++i) {
            int countNumItems = 0;
            final Item itemSample = line.getItem(i);
            for (int j = 0; j < line.getCells().size(); ++j) {
                if (line.getItem(j) == itemSample || line.getItem(j) == Item.POKER_BALL) {
                    ++countNumItems;
                }
            }
            final Award award;
            if (countNumItems >= 2 && (award = Awards.getAward(itemSample, countNumItems)) != null) {
                if (!checkAwardExist(awardList, award)) {
                    awardList.add(award);
                }
            }
        }
    }
    
    private static boolean checkAwardExist(final List<Award> awardList, final Award awardLine) {
        for (final Award award : awardList) {
            if (award.getId() != awardLine.getId()) {
                continue;
            }
            return true;
        }
        return false;
    }
    
    public static int[] getX2Days() {
        final String x2DaysStr = ConfigGame.getValueString("poke_go_days_x2");
        final String[] arr = x2DaysStr.split(",");
        final int[] result = new int[arr.length];
        for (int i = 0; i < result.length; ++i) {
            result[i] = Integer.parseInt(arr[i]);
        }
        return result;
    }
    
    public static int getLastDayX2() {
        final CacheServiceImpl cache = new CacheServiceImpl();
        try {
            return cache.getValueInt("poke_go_last_day_x2");
        }
        catch (KeyNotFoundException e) {
            final int lastDay = ConfigGame.getIntValue("poke_last_day_gio_vang");
            saveLastDayX2(lastDay);
            return lastDay;
        }
    }
    
    public static void saveLastDayX2(final int lastDay) {
        final CacheServiceImpl cache = new CacheServiceImpl();
        cache.setValue("poke_go_last_day_x2", lastDay);
    }
    
    public static void main(final String[] args) {
        final String lines = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20";
        final Item[][] matrix = generateMatrixNoHu(lines.split(","));
        for (int i = 0; i < 3; ++i) {
            final StringBuilder b = new StringBuilder();
            for (int j = 0; j < 3; ++j) {
                b.append(" ").append(matrix[i][j].getId());
            }
            System.out.println(b.toString());
        }
    }
    
    static {
        PokeGoUtils.logger = Logger.getLogger("csvPokeGo");
        PokeGoUtils.FORMAT_PLAY_POKE_GO = ", %10d, %15s, %8d, %20s, %5d, %5d, %10d, %15s, %20s";
    }
}
