// 
// Decompiled by Procyon v0.5.36
// 

package game;

import com.hazelcast.core.IMap;
import com.hazelcast.core.HazelcastInstance;
import com.vinplay.vbee.common.models.minigame.pokego.TopPokeGo;
import com.vinplay.vbee.common.models.cache.TopPokeGoModel;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.models.minigame.pokego.LSGDPokeGo;
import java.util.List;
import java.util.concurrent.TimeoutException;
import java.io.IOException;

import com.vinplay.vbee.common.rmq.RMQApi;
import com.vinplay.vbee.common.messages.minigame.pokego.LogPokeGoMessage;
import com.vinplay.dal.dao.PokeGoDAO;

public class SlotExtendServiceImplement implements SlotExtendService
{
    private final PokeGoDAO dao;
    
    public SlotExtendServiceImplement() {
        this.dao = new SlotExtendDaoImpl();
    }
    
    @Override
    public void logSlotExtend(final long referenceId, final String username, final long betValue, final String linesBetting, final String linesWin, final String prizesOnLine, final short result, final long totalPrizes, final short moneyType, final String time) throws IOException, TimeoutException, InterruptedException {
        final LogPokeGoMessage message = new LogPokeGoMessage();
        message.referenceId = referenceId;
        message.username = username;
        message.betValue = betValue;
        message.linesBetting = linesBetting;
        message.linesWin = linesWin;
        message.prizesOnLine = prizesOnLine;
        message.result = result;
        message.totalPrizes = totalPrizes;
        message.moneyType = moneyType;
        message.time = time;
        RMQApi.publishMessage("queue_slotExtend", message, 135);
    }
    
    @Override
    public int countLSDG(final String username, final int moneyType) {
        return this.dao.countLSGD(username, moneyType);
    }
    
    @Override
    public List<LSGDPokeGo> getLSGD(final String username, final int pageNumber, final int moneyType) {
        return this.dao.getLSGD(username, moneyType, pageNumber);
    }
    
    @Override
    public void addTop(final String username, final int betValue, final long totalPrizes, final int moneyType, final String time, final int result) {
        final HazelcastInstance client = HazelcastClientFactory.getInstance();
        final IMap<String, Object> topMap = client.getMap("cacheTop");
        TopPokeGoModel topPokeGo = (TopPokeGoModel)topMap.get("SlotExtend_" + moneyType);
        if (topPokeGo == null) {
            topPokeGo = new TopPokeGoModel();
        }
        final TopPokeGo entry = new TopPokeGo();
        entry.un = username;
        entry.bv = betValue;
        entry.pz = totalPrizes;
        entry.ts = time;
        entry.rs = result;
        topPokeGo.put(entry);
        topMap.put("SlotExtend_" + moneyType, topPokeGo);
    }
    
    @Override
    public List<TopPokeGo> getTopSlotExtend(final int moneyType, final int page) {
        if (page <= 10) {
            final HazelcastInstance client = HazelcastClientFactory.getInstance();
            final IMap<String, Object> topMap = client.getMap("cacheTop");
            TopPokeGoModel topPokeGo = (TopPokeGoModel)topMap.get("SlotExtend_" + moneyType);
            if (topPokeGo == null) {
                topPokeGo = new TopPokeGoModel();
            }
            if (topPokeGo.getResults().size() == 0) {
                final List<TopPokeGo> results = this.dao.getTop(moneyType, 100);
                topPokeGo.setResults(results);
                topMap.put(("SlotExtend_" + moneyType), topPokeGo);
            }
            return topPokeGo.getResults(page, 10);
        }
        return this.dao.getTopPokeGo(moneyType, page);
    }
    
    @Override
    public long getLastReferenceId() {
        return this.dao.getLastRefenceId();
    }
}
