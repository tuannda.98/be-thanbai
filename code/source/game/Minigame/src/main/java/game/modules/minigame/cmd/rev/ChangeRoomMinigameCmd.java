// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.rev;

import java.nio.ByteBuffer;
import bitzero.server.extensions.data.DataCmd;
import bitzero.server.extensions.data.BaseCmd;

public class ChangeRoomMinigameCmd extends BaseCmd
{
    public short gameId;
    public short lastRoomId;
    public short newRoomId;
    
    public ChangeRoomMinigameCmd(final DataCmd dataCmd) {
        super(dataCmd);
        this.unpackData();
    }
    
    public void unpackData() {
        final ByteBuffer buffer = this.makeBuffer();
        this.gameId = buffer.getShort();
        this.lastRoomId = buffer.getShort();
        this.newRoomId = buffer.getShort();
    }
}
