// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.lobby.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class ResultChuyenKhoanMsg extends BaseMsgEx
{
    public long moneyUse;
    public long currentMoney;
    
    public ResultChuyenKhoanMsg() {
        super(20034);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        bf.putLong(this.moneyUse);
        bf.putLong(this.currentMoney);
        return this.packBuffer(bf);
    }
}
