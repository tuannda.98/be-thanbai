// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.lobby.cmd.rev;

import java.nio.ByteBuffer;
import bitzero.server.extensions.data.DataCmd;
import bitzero.server.extensions.data.BaseCmd;

public class NapQuaNganHangCmd extends BaseCmd
{
    public byte bank;
    public long money;
    
    public NapQuaNganHangCmd(final DataCmd dataCmd) {
        super(dataCmd);
        this.unpackData();
    }
    
    public void unpackData() {
        final ByteBuffer bf = this.makeBuffer();
        this.bank = this.readByte(bf);
        this.money = bf.getLong();
    }
}
