// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.send.slot3x3;

import java.nio.ByteBuffer;
import game.modules.minigame.LineWin;
import java.util.List;
import bitzero.server.extensions.data.BaseMsg;

public class ResultSlotExtendMsg extends BaseMsg
{
    public long prize;
    public int winType;
    public int result;
    public int mutil;
    public int mutil1;
    public int mutil2;
    public int spinId;
    public int[] showItem;
    public List<LineWin> listLineWin;
    public long currMoney;
    
    public ResultSlotExtendMsg() {
        super((short)8003);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        bf.putInt(this.result);
        bf.putLong(this.prize);
        bf.putInt(this.winType);
        bf.putInt(this.mutil);
        bf.putInt(this.mutil1);
        bf.putInt(this.mutil2);
        bf.putInt(this.spinId);
        final int size1 = this.showItem.length;
        bf.putInt(size1);
        for (int j : this.showItem) {
            bf.putInt(j);
        }
        final int size2 = this.listLineWin.size();
        bf.putInt(size2);
        for (LineWin lineWin : this.listLineWin) {
            bf.putInt(lineWin.getLine());
            bf.putDouble(lineWin.getPrizeAmount());
            if (lineWin.isJackpot()) {
                bf.putInt(1);
            } else {
                bf.putInt(0);
            }
        }
        this.putStr(bf, "");
        this.putStr(bf, "");
        this.putStr(bf, "");
        bf.putLong(this.currMoney);
        return this.packBuffer(bf);
    }
}
