// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.rev.baucua;

import java.nio.ByteBuffer;
import bitzero.server.extensions.data.DataCmd;
import bitzero.server.extensions.data.BaseCmd;

public class ChangeRoomBauCuaCmd extends BaseCmd
{
    public byte roomLeavedId;
    public byte roomJoinedId;
    
    public ChangeRoomBauCuaCmd(final DataCmd dataCmd) {
        super(dataCmd);
        this.unpackData();
    }
    
    public void unpackData() {
        final ByteBuffer bf = this.makeBuffer();
        this.roomLeavedId = bf.get();
        this.roomJoinedId = bf.get();
    }
}
