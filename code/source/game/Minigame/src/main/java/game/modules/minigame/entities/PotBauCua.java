package game.modules.minigame.entities;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class PotBauCua extends Pot {
    public final PotType type;
    public final ConcurrentMap<String, Long> contributors;

    public PotBauCua(final int id) {
        this.contributors = new ConcurrentHashMap<>();
        this.type = PotType.findPotType((byte) id);
    }

    public void bet(final String username, final long betValue) {
        if (this.contributors.containsKey(username)) {
            final long currentBetValue = this.contributors.get(username);
            this.contributors.put(username, currentBetValue + betValue);
        } else {
            this.contributors.put(username, betValue);
        }
        this.totalValue += betValue;
    }

    @Override
    public void renew() {
        super.renew();
        this.contributors.clear();
    }

    public long getTotalBetByUsername(final String username) {
        long totalValue = 0L;
        if (this.contributors.containsKey(username)) {
            totalValue = this.contributors.get(username);
        }
        return totalValue;
    }

    public enum PotType {
        BAU(0),
        CUA(1),
        TOM(2),
        CA(3),
        GA(4),
        HUOU(5);

        public final byte id;

        PotType(final int id) {
            this.id = (byte) id;
        }

        public static PotType findPotType(final byte id) {
            for (final PotType entry : values()) {
                if (entry.id == id) {
                    return entry;
                }
            }
            return null;
        }
    }
}
