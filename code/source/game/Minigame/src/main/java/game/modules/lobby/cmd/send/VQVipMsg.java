// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.lobby.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class VQVipMsg extends BaseMsgEx
{
    public int prizeVin;
    public short prizeMulti;
    public short remainCount;
    public long currentMoneyVin;
    
    public VQVipMsg() {
        super(20044);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        bf.putInt(this.prizeVin);
        bf.putShort(this.prizeMulti);
        bf.putShort(this.remainCount);
        bf.putLong(this.currentMoneyVin);
        return this.packBuffer(bf);
    }
}
