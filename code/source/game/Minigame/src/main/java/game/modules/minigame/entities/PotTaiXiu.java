package game.modules.minigame.entities;

import com.vinplay.dal.entities.taixiu.TransactionTaiXiuDetail;

import java.util.ArrayList;
import java.util.List;

public class PotTaiXiu extends Pot {
    private long totalBotBet;
    private int numBot;
    private final List<TransactionTaiXiuDetail> contributors;
    public final List<String> users;

    public PotTaiXiu() {
        this.totalBotBet = 0L;
        this.numBot = 0;
        this.contributors = new ArrayList<>();
        this.users = new ArrayList<>();
    }

    public void bet(final TransactionTaiXiuDetail trans, final boolean isBot) {
        synchronized (this.contributors) {
            this.contributors.add(trans);
        }
        synchronized (this.users) {
            if (!this.users.contains(trans.username)) {
                this.users.add(trans.username);
            }
        }
        this.totalValue += trans.betValue;
        if (isBot) {
            this.totalBotBet += trans.betValue;
            ++this.numBot;
        }
    }

    @Override
    public void renew() {
        super.renew();
        this.contributors.clear();
        this.users.clear();
        this.totalBotBet = 0L;
        this.numBot = 0;
    }

    public long getTotalBetByUsername(final String username) {
        long totalValue = 0L;
        for (final TransactionTaiXiuDetail tran : this.contributors) {
            if (!tran.username.equals(username)) {
                continue;
            }
            totalValue += tran.betValue;
        }
        return totalValue;
    }

    public short getNumBet() {
        return (short) this.users.size();
    }

    public long getTotalBotBet() {
        return this.totalBotBet;
    }

    public int getNumBotBet() {
        return this.numBot;
    }

    public List<TransactionTaiXiuDetail> getContributors() {
        return new ArrayList<>(contributors);
    }
}
