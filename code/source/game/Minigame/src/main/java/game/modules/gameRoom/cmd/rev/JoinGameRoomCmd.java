// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.gameRoom.cmd.rev;

import java.nio.ByteBuffer;
import bitzero.server.extensions.data.DataCmd;
import bitzero.server.extensions.data.BaseCmd;

public class JoinGameRoomCmd extends BaseCmd
{
    public int moneyType;
    public int maxUserPerRoom;
    public long moneyBet;
    public int rule;
    
    public JoinGameRoomCmd(final DataCmd dataCmd) {
        super(dataCmd);
        this.rule = 0;
        this.unpackData();
    }
    
    public void unpackData() {
        final ByteBuffer bf = this.makeBuffer();
        this.moneyType = this.readInt(bf);
        this.maxUserPerRoom = this.readInt(bf);
        this.moneyBet = bf.getLong();
        this.rule = bf.getInt();
    }
}
