// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class BroadcastTXTimeMsg extends BaseMsgEx
{
    public byte remainTime;
    public boolean betting;
    
    public BroadcastTXTimeMsg() {
        super(2124);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        bf.put(this.remainTime);
        this.putBoolean(bf, this.betting);
        return this.packBuffer(bf);
    }
}
