// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.send.pokego;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class UpdatePotPokeGoMsg extends BaseMsgEx
{
    public long value;
    public byte x2;
    
    public UpdatePotPokeGoMsg() {
        super(7002);
        this.x2 = 0;
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        bf.putLong(this.value);
        bf.put(this.x2);
        return this.packBuffer(bf);
    }
}
