// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.rev.minipoker;

import java.nio.ByteBuffer;
import bitzero.server.extensions.data.DataCmd;
import bitzero.server.extensions.data.BaseCmd;

public class SubscribeMiniPokerCmd extends BaseCmd
{
    public byte roomId;
    
    public SubscribeMiniPokerCmd(final DataCmd dataCmd) {
        super(dataCmd);
        this.unpackData();
    }
    
    public void unpackData() {
        final ByteBuffer bf = this.makeBuffer();
        this.roomId = this.readByte(bf);
    }
}
