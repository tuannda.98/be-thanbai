// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class UnsubscribeMinigameMsg extends BaseMsgEx
{
    public short gameId;
    
    public UnsubscribeMinigameMsg() {
        super(2001);
    }
    
    public byte[] createData() {
        final ByteBuffer buffer = this.makeBuffer();
        buffer.putShort(this.gameId);
        return this.packBuffer(buffer);
    }
}
