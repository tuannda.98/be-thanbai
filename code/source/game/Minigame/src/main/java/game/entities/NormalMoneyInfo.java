// 
// Decompiled by Procyon v0.5.36
// 

package game.entities;

import bitzero.server.entities.User;
import game.utils.DataUtils;
import game.utils.GameUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

public class NormalMoneyInfo {
    private static final User.PropertyKey<NormalMoneyInfo> NORMAL_DATA_INFO = new User.PropertyKey<>("NORMAL_DATA_INFO");
    public String nickName;
    public int winCount;
    public int lostCount;
    public long moneyWin;
    public long moneyWinToday;
    public long moneyWinThisWeek;
    public long moneyWinThisMonth;
    public long moneyWinThisYear;
    public int lastDay;
    public int lastWeek;
    public int lastMonth;
    public int lastYear;
    public long moneyLost;
    public long moneyLostToday;
    public long moneyLostThisWeek;
    public long moneyLostThisMonth;
    public long moneyLostThisYear;
    public int exp;
    private static final boolean dependOnGame = true;

    public static NormalMoneyInfo copyFromDB(final String userId) {
        String key = NormalMoneyInfo.class.getName() + userId +
                GameUtils.gameName;
        return (NormalMoneyInfo) DataUtils.copyDataFromDB(key, NormalMoneyInfo.class);
    }

    public void save() {
        String key = this.getClass().getName() + this.nickName +
                GameUtils.gameName;
        DataUtils.saveToDB(key, this, this.getClass());
    }

    public static NormalMoneyInfo getInfo(final User user) {
        NormalMoneyInfo info = user.getProperty(NORMAL_DATA_INFO);
        if (info == null) {
            info = copyFromDB(user.getName());
            user.setProperty(NORMAL_DATA_INFO, info);
        }
        return info;
    }

    public JSONObject toJSONObject() throws JSONException {
        final JSONObject json = new JSONObject();
        json.put("u", this.nickName);
        json.put("m", this.moneyWinToday);
        return json;
    }

    private void updateValueByTime() {
        final int today = Calendar.getInstance().get(Calendar.DAY_OF_YEAR);
        final int thisWeek = Calendar.getInstance().get(Calendar.WEEK_OF_YEAR);
        final int thisMonth = Calendar.getInstance().get(Calendar.MONTH);
        final int thisYear = Calendar.getInstance().get(Calendar.YEAR);
        if (today != this.lastDay) {
            this.lastDay = today;
            this.moneyWinToday = 0L;
            this.moneyLostToday = 0L;
        }
        if (thisWeek != this.lastWeek) {
            this.lastWeek = thisWeek;
            this.moneyWinThisWeek = 0L;
            this.moneyLostThisWeek = 0L;
        }
        if (thisMonth != this.lastMonth) {
            this.lastMonth = thisMonth;
            this.moneyWinThisMonth = 0L;
            this.moneyLostThisMonth = 0L;
        }
        if (thisYear != this.lastYear) {
            this.lastYear = thisYear;
            this.moneyWinThisYear = 0L;
            this.moneyLostThisYear = 0L;
        }
    }

    public void addScore(final UserScore score) {
        this.updateValueByTime();
        if (score.money >= 0L) {
            this.moneyWinToday += score.money;
            this.moneyWinThisWeek += score.money;
            this.moneyWinThisMonth += score.money;
            this.moneyWinThisYear += score.money;
        } else {
            this.moneyLostToday -= score.money;
            this.moneyLostThisWeek -= score.money;
            this.moneyLostThisMonth -= score.money;
            this.moneyLostThisYear -= score.money;
        }
        this.save();
    }
}
