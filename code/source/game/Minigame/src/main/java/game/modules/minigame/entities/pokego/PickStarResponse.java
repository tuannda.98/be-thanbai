// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.entities.pokego;

public class PickStarResponse
{
    private long totalValue;
    private String prizes;
    
    public void setTotalValue(final long totalValue) {
        this.totalValue = totalValue;
    }
    
    public long getTotalValue() {
        return this.totalValue;
    }
    
    public void setPrizes(final String prizes) {
        this.prizes = prizes;
    }
    
    public String getPrizes() {
        return this.prizes;
    }
}
