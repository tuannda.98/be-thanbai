// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.utils;

import bitzero.server.entities.User;
import bitzero.util.ExtensionUtility;
import bitzero.util.common.business.Debug;
import com.vinplay.dal.service.impl.BauCuaServiceImpl;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.models.minigame.baucua.ToiChonCa;
import com.vinplay.vbee.common.response.MoneyResponse;
import com.vinplay.vbee.common.statics.TransType;
import game.modules.minigame.cmd.send.UpdateUserInfoMsg;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class BauCuaUtils {
    private static Logger logger;
    private static long[] prizes;
    private static final String FORMAT_TOI_CHON_CA = "%s,\t%d,\t%d,\t%d,\t%d,\t%d,\t%s";

    public static void rewardToiChonCa() {
        final SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        final SimpleDateFormat startTimeFormat = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        final SimpleDateFormat endTimeFormat = new SimpleDateFormat("yyyy-MM-dd 23:59:59");
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        final String yesterday = df.format(cal.getTime());
        final String startTime = startTimeFormat.format(cal.getTime());
        final String endTime = endTimeFormat.format(cal.getTime());
        final BauCuaServiceImpl bcService = new BauCuaServiceImpl();
        final UserServiceImpl userService = new UserServiceImpl();
        final List<ToiChonCa> list = bcService.getTopToiChonCa(startTime, endTime);
        int rank = 1;
        Debug.trace("List toi chon ca: " + list.size());
        for (int i = 0; i < list.size() && i < BauCuaUtils.prizes.length; ++i) {
            final ToiChonCa player = list.get(i);
            Debug.trace("Tra thuong toi chon ca: " + player.username);
            final MoneyResponse response = userService.updateMoney(player.username, BauCuaUtils.prizes[i], "vin", "BauCua", "T\u00c3´i ch\u00e1»\ufffdn c\u00c3¡ - Tr\u00e1º£ th\u00c6°\u00e1»\u0178ng", "Ng\u00c3 y " + yesterday, 0L, null, TransType.NO_VIPPOINT);
            if (response != null && response.isSuccess()) {
                User user = ExtensionUtility.getExtension().getApi().getUserByName(player.username);
                if (user != null) {
                    final UpdateUserInfoMsg msg = new UpdateUserInfoMsg();
                    msg.currentMoney = response.getCurrentMoney();
                    msg.moneyType = 1;
                    ExtensionUtility.getExtension().send(msg, user);
                }
                log(player.username, rank, player.soCa, player.soVan, BauCuaUtils.prizes[i], response.getCurrentMoney());
            }
            ++rank;
        }
    }

    private static void log(final String username, final int rank, final int soCa, final int soVan, final long prize, final long moneyAfterUpdated) {
        final SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
        final String str = String.format("%s,\t%d,\t%d,\t%d,\t%d,\t%d,\t%s", username, rank, soCa, soVan, prize, moneyAfterUpdated, df.format(new Date()));
        System.out.println(str);
        BauCuaUtils.logger.debug(str);
    }

    static {
        BauCuaUtils.logger = Logger.getLogger("csvToiChonCa");
        BauCuaUtils.prizes = new long[]{100000L, 50000L, 20000L, 10000L, 10000L};
    }
}
