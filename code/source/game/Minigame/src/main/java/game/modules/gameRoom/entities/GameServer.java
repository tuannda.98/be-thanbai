/*
 * Decompiled with CFR 0.150.
 *
 * Could not load the following classes:
 *  bitzero.server.entities.User
 *  bitzero.server.extensions.data.BaseMsg
 *  bitzero.server.extensions.data.DataCmd
 *  bitzero.util.ExtensionUtility
 *  bitzero.util.common.business.CommonHandle
 *  org.json.JSONObject
 */
package game.modules.gameRoom.entities;

import bitzero.server.entities.User;
import bitzero.server.extensions.data.BaseMsg;
import bitzero.server.extensions.data.DataCmd;
import bitzero.util.ExtensionUtility;
import bitzero.util.common.business.CommonHandle;
import game.utils.GameUtils;
import org.json.JSONObject;

public abstract class GameServer {
    public static final User.PropertyKey<Integer> USER_CHAIR = new User.PropertyKey<>("user_chair");
    public GameRoom room;

    public static GameServer createNewGameServer(GameRoom room) {
        String gameServerClassPath = GameUtils.gameServerClassPath;
        try {
            GameServer mainServer = (GameServer) Class.forName(gameServerClassPath).newInstance();
            mainServer.init(room);
            return mainServer;
        } catch (Exception e) {
            CommonHandle.writeErrLog(e);
            return null;
        }
    }

    public abstract void init(GameRoom var1);

    public abstract void init();

    public abstract void destroy();

    public abstract void onGameUserReturn(User var1);

    public abstract void onGameUserEnter(User var1);

    public abstract void onGameUserExit(User var1);

    public abstract void onGameUserDis(User var1);

    public abstract void onGameMessage(User var1, DataCmd var2);

    public abstract String toString();

    public abstract JSONObject toJONObject();

    public abstract void choNoHu(String var1);

    public void send(BaseMsg cmd, User user) {
        if (user != null) {
            ExtensionUtility.getExtension().send(cmd, user);
        }
    }

    public int countPlayers() {
        return 0;
    }

    public boolean isPlaying() {
        return false;
    }
}

