// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.lobby.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class UpdateJackpotMsg extends BaseMsgEx
{
    public long potMiniPoker100;
    public long potMiniPoker1000;
    public long potMiniPoker10000;
    public long potPokeGo100;
    public long potPokeGo1000;
    public long potPokeGo10000;
    public long potKhoBau100;
    public long potKhoBau1000;
    public long potKhoBau10000;
    public long potNDV100;
    public long potNDV1000;
    public long potNDV10000;
    public long potAvengers100;
    public long potAvengers1000;
    public long potAvengers10000;
    public long vqv100;
    public long vqv1000;
    public long vqv10000;
    public long fish100;
    public long fish1000;
    
    public UpdateJackpotMsg() {
        super(20101);
        this.potMiniPoker100 = 0L;
        this.potMiniPoker1000 = 0L;
        this.potMiniPoker10000 = 0L;
        this.potPokeGo100 = 0L;
        this.potPokeGo1000 = 0L;
        this.potPokeGo10000 = 0L;
        this.potKhoBau100 = 0L;
        this.potKhoBau1000 = 0L;
        this.potKhoBau10000 = 0L;
        this.potNDV100 = 0L;
        this.potNDV1000 = 0L;
        this.potNDV10000 = 0L;
        this.potAvengers100 = 0L;
        this.potAvengers1000 = 0L;
        this.potAvengers10000 = 0L;
        this.vqv100 = 0L;
        this.vqv1000 = 0L;
        this.vqv10000 = 0L;
        this.fish100 = 0L;
        this.fish1000 = 0L;
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        this.putLong(bf, this.potMiniPoker100);
        this.putLong(bf, this.potMiniPoker1000);
        this.putLong(bf, this.potMiniPoker10000);
        this.putLong(bf, this.potPokeGo100);
        this.putLong(bf, this.potPokeGo1000);
        this.putLong(bf, this.potPokeGo10000);
        this.putLong(bf, this.potKhoBau100);
        this.putLong(bf, this.potKhoBau1000);
        this.putLong(bf, this.potKhoBau10000);
        this.putLong(bf, this.potNDV100);
        this.putLong(bf, this.potNDV1000);
        this.putLong(bf, this.potNDV10000);
        this.putLong(bf, this.potAvengers100);
        this.putLong(bf, this.potAvengers1000);
        this.putLong(bf, this.potAvengers10000);
        this.putLong(bf, this.vqv100);
        this.putLong(bf, this.vqv1000);
        this.putLong(bf, this.vqv10000);
        this.putLong(bf, this.fish100);
        this.putLong(bf, this.fish1000);
        return this.packBuffer(bf);
    }
}
