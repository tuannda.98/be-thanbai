// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class StartNewGameTaiXiuMsg extends BaseMsgEx
{
    public long referenceId;
    public short remainTimeRutLoc;
    
    public StartNewGameTaiXiuMsg() {
        super(2115);
    }
    
    public byte[] createData() {
        final ByteBuffer buffer = this.makeBuffer();
        buffer.putLong(this.referenceId);
        buffer.putShort(this.remainTimeRutLoc);
        return this.packBuffer(buffer);
    }
}
