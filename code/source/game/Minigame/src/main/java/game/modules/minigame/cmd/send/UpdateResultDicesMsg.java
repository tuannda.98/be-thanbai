// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class UpdateResultDicesMsg extends BaseMsgEx
{
    public short result;
    public short dice1;
    public short dice2;
    public short dice3;
    
    public UpdateResultDicesMsg() {
        super(2113);
    }
    
    public byte[] createData() {
        final ByteBuffer buffer = this.makeBuffer();
        buffer.putShort(this.result);
        buffer.putShort(this.dice1);
        buffer.putShort(this.dice2);
        buffer.putShort(this.dice3);
        return this.packBuffer(buffer);
    }
}
