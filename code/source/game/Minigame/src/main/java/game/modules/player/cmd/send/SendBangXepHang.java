// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.player.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class SendBangXepHang extends BaseMsgEx
{
    public byte type;
    public String top;
    
    public SendBangXepHang() {
        super(1001);
        this.top = "[]";
    }
    
    public byte[] createData() {
        final ByteBuffer buffer = this.makeBuffer();
        buffer.put(this.type);
        this.putStr(buffer, this.top);
        return this.packBuffer(buffer);
    }
}
