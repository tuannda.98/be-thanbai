// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.lobby.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class NapTheDienThoaiMsg extends BaseMsgEx
{
    public long currentMoney;
    public long timeFail;
    public int numFail;
    
    public NapTheDienThoaiMsg() {
        super(20012);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        bf.putLong(this.currentMoney);
        bf.putLong(this.timeFail);
        bf.putInt(this.numFail);
        return this.packBuffer(bf);
    }
}
