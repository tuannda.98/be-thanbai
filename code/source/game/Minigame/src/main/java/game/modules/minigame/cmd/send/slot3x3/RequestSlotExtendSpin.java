// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.send.slot3x3;

import java.nio.ByteBuffer;
import bitzero.server.extensions.data.DataCmd;
import bitzero.server.extensions.data.BaseCmd;

public class RequestSlotExtendSpin extends BaseCmd
{
    public long gold;
    
    public RequestSlotExtendSpin(final DataCmd data) {
        super(data);
        this.unpackData();
    }
    
    public void unpackData() {
        final ByteBuffer bf = this.makeBuffer();
        this.gold = this.readLong(bf);
    }
}
