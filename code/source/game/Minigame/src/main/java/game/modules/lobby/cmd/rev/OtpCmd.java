// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.lobby.cmd.rev;

import java.nio.ByteBuffer;
import bitzero.server.extensions.data.DataCmd;
import bitzero.server.extensions.data.BaseCmd;

public class OtpCmd extends BaseCmd
{
    public String otp;
    public byte type;
    
    public OtpCmd(final DataCmd dataCmd) {
        super(dataCmd);
        this.unpackData();
    }
    
    public void unpackData() {
        final ByteBuffer bf = this.makeBuffer();
        this.otp = this.readString(bf);
        this.type = this.readByte(bf);
    }
}
