// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.entities;

public class BotBauCua
{
    private String nickname;
    private short timeBetting;
    private String betStr;
    
    public BotBauCua(final String nickname, final short timeBetting, final String betStr) {
        this.nickname = nickname;
        this.timeBetting = timeBetting;
        this.betStr = betStr;
    }
    
    public void setNickname(final String nickname) {
        this.nickname = nickname;
    }
    
    public String getNickname() {
        return this.nickname;
    }
    
    public void setTimeBetting(final short timeBetting) {
        this.timeBetting = timeBetting;
    }
    
    public short getTimeBetting() {
        return this.timeBetting;
    }
    
    public void setBetStr(final String betStr) {
        this.betStr = betStr;
    }
    
    public String getBetStr() {
        return this.betStr;
    }
}
