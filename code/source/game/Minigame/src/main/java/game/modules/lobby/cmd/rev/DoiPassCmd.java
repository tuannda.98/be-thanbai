// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.lobby.cmd.rev;

import java.nio.ByteBuffer;
import bitzero.server.extensions.data.DataCmd;
import bitzero.server.extensions.data.BaseCmd;

public class DoiPassCmd extends BaseCmd
{
    public String oldPass;
    public String newPass;
    
    public DoiPassCmd(final DataCmd dataCmd) {
        super(dataCmd);
        this.unpackData();
    }
    
    public void unpackData() {
        final ByteBuffer bf = this.makeBuffer();
        this.oldPass = this.readString(bf);
        this.newPass = this.readString(bf);
    }
}
