// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.test;

import bitzero.server.entities.User;
import com.vinplay.cardlib.models.Card;
import com.vinplay.cardlib.models.GroupType;
import com.vinplay.cardlib.utils.CardLibUtils;
import game.modules.minigame.utils.GenerationMiniPoker;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class TestMiniPoker
{
    private int soLanNoHu;
    private int soLanNoHuKhongDuTraThuong;
    private int soLanKhongDuTraThuong;
    private int thungPhaSanhNho;
    private int tuQuy;
    private int cuLu;
    private int thung;
    private int sanh;
    private int samCo;
    private int haiDoi;
    private int motDoiTo;
    private int motDoiNho;
    private int mauThau;
    private long tongTraThuong;
    private long tongTienAn;
    private final GenerationMiniPoker gen;
    private int randomLoi;
    private long pot;
    private static final int initPotValue = 500000;
    private long fund;
    private final short moneyType;
    private long currentMoney;
    private final long betValue;
    private int khongDuTien;
    
    public TestMiniPoker() {
        this.soLanNoHu = 0;
        this.soLanNoHuKhongDuTraThuong = 0;
        this.soLanKhongDuTraThuong = 0;
        this.thungPhaSanhNho = 0;
        this.tuQuy = 0;
        this.cuLu = 0;
        this.thung = 0;
        this.sanh = 0;
        this.samCo = 0;
        this.haiDoi = 0;
        this.motDoiTo = 0;
        this.motDoiNho = 0;
        this.mauThau = 0;
        this.tongTraThuong = 0L;
        this.tongTienAn = 0L;
        this.gen = new GenerationMiniPoker();
        this.randomLoi = 0;
        this.pot = 500000L;
        this.fund = 3000000L;
        this.moneyType = 1;
        this.currentMoney = 1000000000L;
        this.betValue = 100L;
        this.khongDuTien = 0;
    }
    
    private void play(final String username) {
        final StringBuilder builder = new StringBuilder();
        int result = 0;
        long prize = 0L;
        if (this.betValue > 0L) {
            if (this.currentMoney >= this.betValue) {
                boolean enoughToPair = false;
                final long moneyToPot = this.betValue / 100L;
                final long moneyToSystem = this.betValue * 2L / 100L;
                final long moneyToFund = this.betValue - moneyToPot - moneyToSystem;
                this.pot += moneyToPot;
                this.fund += moneyToFund;
                while (!enoughToPair) {
                    prize = 0L;
                    long moneyExchange = 0L;
                    final List<Card> cards = this.gen.randomCards2();
                    if (cards.size() != 5) {
                        System.out.println("Random loi");
                        ++this.randomLoi;
                    }
                    else {
                        final GroupType groupType = CardLibUtils.calculateTypePoker(cards);
                        if (groupType == null) {
                            continue;
                        }
                        switch (groupType) {
                            case HighCard: {
                                result = 11;
                                ++this.mauThau;
                                break;
                            }
                            case OnePair: {
                                if (CardLibUtils.pairEqualOrGreatJack(cards)) {
                                    result = 10;
                                    prize = (int)(this.betValue * 2.5f);
                                    ++this.motDoiTo;
                                    break;
                                }
                                result = 9;
                                prize = 0L;
                                ++this.motDoiNho;
                                break;
                            }
                            case TwoPair: {
                                result = 8;
                                prize = this.betValue * 5L;
                                ++this.haiDoi;
                                break;
                            }
                            case ThreeOfKind: {
                                result = 7;
                                prize = this.betValue * 8L;
                                ++this.samCo;
                                break;
                            }
                            case Straight: {
                                result = 6;
                                prize = ((this.moneyType == 1) ? (this.betValue * 13L) : (this.betValue * 12L));
                                ++this.sanh;
                                break;
                            }
                            case Flush: {
                                result = 5;
                                prize = ((this.moneyType == 1) ? (this.betValue * 20L) : (this.betValue * 18L));
                                ++this.thung;
                                break;
                            }
                            case FullHouse: {
                                result = 4;
                                prize = ((this.moneyType == 1) ? (this.betValue * 50L) : (this.betValue * 40L));
                                ++this.cuLu;
                                break;
                            }
                            case FourOfKind: {
                                result = 3;
                                prize = ((this.moneyType == 1) ? (this.betValue * 150L) : (this.betValue * 120L));
                                ++this.tuQuy;
                                break;
                            }
                            case StraightFlush: {
                                if (!CardLibUtils.isStraightFlushJack(cards)) {
                                    result = 2;
                                    prize = this.betValue * 1000L;
                                    ++this.thungPhaSanhNho;
                                    break;
                                }
                                if (!this.checkDienKienNoHu(username)) {
                                    continue;
                                }
                                result = 1;
                                prize = this.pot;
                                break;
                            }
                        }
                        moneyExchange = prize - this.betValue;
                        final long l;
                        final long fundExchange = l = (Math.max(prize, 0L));
                        if (result == 1) {
                            if (this.fund - 500000L < 0L) {
                                ++this.soLanNoHuKhongDuTraThuong;
                                ++this.soLanKhongDuTraThuong;
                                continue;
                            }
                            ++this.soLanNoHu;
                        }
                        else if (this.fund - fundExchange < 0L) {
                            ++this.soLanKhongDuTraThuong;
                            continue;
                        }
                        enoughToPair = true;
                        if (prize > 0L) {
                            if (result == 1) {
                                this.pot = 500000L;
                                this.fund -= 500000L;
                            }
                            else {
                                this.fund -= fundExchange;
                            }
                        }
                        this.tongTraThuong += prize;
                        if (moneyExchange != 0L) {
                            this.currentMoney += moneyExchange;
                        }
                        if (moneyExchange > 0L) {
                            this.tongTienAn += moneyExchange;
                        }
                        builder.append(cards.get(0).toString());
                        for (int i = 1; i < cards.size(); ++i) {
                            builder.append(",");
                            builder.append(cards.get(i).toString());
                        }
                    }
                }
                this.saveFund();
                this.savePot();
            }
            else {
                result = 102;
                ++this.khongDuTien;
            }
        }
        else {
            result = 101;
        }
    }
    
    private boolean checkDienKienNoHu(final String username) {
        return true;
    }
    
    private void saveFund() {
    }
    
    private void savePot() {
    }
    
    public void updatePotToUser(final User user) {
    }
    
    public static void main(final String[] args) throws IOException {
        final FileWriter fw = new FileWriter("D:/fund.log");
        final TestMiniPoker test = new TestMiniPoker();
        final long soLanChay = 10000000L;
        long maxTime = 0L;
        for (int i = 0; i < soLanChay; ++i) {
            if (i % 100 == 0) {
                fw.write("" + test.fund + "\n");
                fw.flush();
            }
            final long startTime = System.currentTimeMillis();
            test.play("banhcuon");
            final long endTime = System.currentTimeMillis();
            if (maxTime < endTime - startTime) {
                maxTime = endTime - startTime;
            }
        }
        fw.close();
        System.out.println("Random loi: " + test.randomLoi);
        System.out.println("MAX TIME: " + maxTime + " (ms)");
        System.out.println("So lan khong du tien: " + test.khongDuTien);
        System.out.println("Hu con lai: " + test.pot);
        System.out.println("Quy con lai: " + test.fund);
        System.out.println("So lan no hu: " + test.soLanNoHu);
        System.out.println("So lan no hu khong du tra thuong: " + test.soLanNoHuKhongDuTraThuong);
        System.out.println("So lan khong du tra thuong: " + test.soLanKhongDuTraThuong);
        System.out.println("Thung pha sanh nho: " + test.thungPhaSanhNho);
        System.out.println("Tu quy: " + test.tuQuy);
        System.out.println("Cu lu: " + test.cuLu);
        System.out.println("Thung: " + test.thung);
        System.out.println("Sanh: " + test.sanh);
        System.out.println("Sam co: " + test.samCo);
        System.out.println("Hai doi: " + test.haiDoi);
        System.out.println("Mot doi to: " + test.motDoiTo);
        System.out.println("Mot doi nho: " + test.motDoiNho);
        System.out.println("Mau thau: " + test.mauThau);
        System.out.println("Tong tien an duoc (tru di tien dat): " + test.tongTienAn);
        System.out.println("Tong tra thuong: " + test.tongTraThuong);
        System.out.println("So tien con lai nguoi choi: " + test.currentMoney);
    }
    
    private static class ResultPoker
    {
        private static final short DAT_CUOC_KHONG_HOP_LE = 101;
        private static final short KHONG_DU_TIEN = 102;
        private static final short TRUOT = 0;
        private static final short NO_HU = 1;
        private static final short THUNG_PHA_SANH_NHO = 2;
        private static final short TU_QUY = 3;
        private static final short CU_LU = 4;
        private static final short THUNG = 5;
        private static final short SANH = 6;
        private static final short SAM_CO = 7;
        private static final short HAI_DOI = 8;
        private static final short MOT_DOI_NHO = 9;
        private static final short MOT_DOI_TO = 10;
        private static final short BAI_CAO = 11;
    }
}
