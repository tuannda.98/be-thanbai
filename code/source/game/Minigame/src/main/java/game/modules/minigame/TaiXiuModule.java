// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame;

import bitzero.server.BitZeroServer;
import bitzero.server.core.BZEventParam;
import bitzero.server.core.BZEventType;
import bitzero.server.core.IBZEvent;
import bitzero.server.entities.User;
import bitzero.server.exceptions.BZException;
import bitzero.server.extensions.BaseClientRequestHandler;
import bitzero.server.extensions.data.BaseMsg;
import bitzero.server.extensions.data.DataCmd;
import bitzero.util.ExtensionUtility;
import bitzero.util.common.business.CommonHandle;
import bitzero.util.common.business.Debug;
import casio.king365.GU;
import casio.king365.core.HCMap;
import casio.king365.util.KingUtil;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.hazelcast.core.IMap;
import com.vinplay.dal.entities.taixiu.ResultTaiXiu;
import com.vinplay.dal.service.CacheService;
import com.vinplay.dal.service.MiniGameService;
import com.vinplay.dal.service.TaiXiuService;
import com.vinplay.dal.service.impl.CacheServiceImpl;
import com.vinplay.dal.service.impl.MiniGameServiceImpl;
import com.vinplay.dal.service.impl.TaiXiuServiceImpl;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.response.MoneyResponse;
import com.vinplay.vbee.common.statics.TransType;
import com.vinplay.vbee.common.utils.DateTimeUtils;
import game.modules.chat.cmd.send.ChatMsg;
import game.modules.minigame.cmd.rev.*;
import game.modules.minigame.cmd.send.*;
import game.modules.minigame.entities.BotMinigame;
import game.modules.minigame.entities.BotTaiXiu;
import game.modules.minigame.entities.Vi;
import game.modules.minigame.room.MGRoom;
import game.modules.minigame.room.MGRoomTaiXiu;
import game.modules.minigame.utils.GenerationTaiXiu;
import game.modules.minigame.utils.MiniGameUtils;
import game.modules.minigame.utils.RutLocUtils;
import game.modules.minigame.utils.TaiXiuUtils;
import game.utils.GameUtils;
import vn.yotel.yoker.util.Util;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.text.ParseException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class TaiXiuModule extends BaseClientRequestHandler {
    private final Map<String, MGRoom> rooms;
    private final Runnable gameLoopTask;
    private final Runnable serverReadyTask;
    private final Runnable calculatingTXVinTask;
    private final Runnable calculatingTXXuTask;
    private final Runnable rewardThanhDuDailyTask;
    private int count;
    private boolean serverReady;
    private final ThreadPoolExecutor executor;
    private long referenceTaiXiuId;
    private final TaiXiuService txService;
    private final MiniGameService mgService;
    private List<ResultTaiXiu> lichSuPhienTX;
    private final GenerationTaiXiu generationTX;
    private short result;
    private long fundRutLoc;
    private int countRutLoc;
    private int countReqRutLoc;
    private int[] rutLocPrizes;
    private int[] phanBoGiaiThuong;
    private boolean enableRutLoc;
    private int tongSoNguoiRutLocLanTruoc;
    private final List<BotTaiXiu> botsVin;
    private final List<BotTaiXiu> botsXu;
    private short forceBetSide;
    private long MinCtrl;
    private final long MaxCtrl;
    private final List<String> listChat;
    private final List<String> listChatUsers;
    private final Timer chatTimer = new Timer("TX-Chat");
    CacheService cacheService = new CacheServiceImpl();
    private ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(
            50, new ThreadFactoryBuilder().setNameFormat("TaiXiu-executorService-%d").build());
    UserServiceImpl userService;

    public TaiXiuModule() {
        this.rooms = new HashMap<>();
        this.gameLoopTask = new GameLoopTask();
        this.serverReadyTask = new ServerReadyTask();
        this.calculatingTXVinTask = new CalculatingTaiXiuPrize((byte) 1);
        this.calculatingTXXuTask = new CalculatingTaiXiuPrize((byte) 0);
        this.rewardThanhDuDailyTask = new RewardThanhDuDaily();
        this.count = 0;
        this.serverReady = false;
        this.executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(
                200, new ThreadFactoryBuilder().setNameFormat("TaiXiu-executor-%d").build());
        this.txService = new TaiXiuServiceImpl();
        this.mgService = new MiniGameServiceImpl();
        this.lichSuPhienTX = new ArrayList<>();
        this.generationTX = new GenerationTaiXiu();
        this.result = -1;
        this.fundRutLoc = 0L;
        this.countRutLoc = 0;
        this.countReqRutLoc = 0;
        this.enableRutLoc = false;
        this.tongSoNguoiRutLocLanTruoc = 30;
        this.botsVin = new ArrayList<>();
        this.botsXu = new ArrayList<>();
        this.forceBetSide = -1;
        this.MinCtrl = 500000L;
        this.MaxCtrl = 1000000L;
        this.listChat = new ArrayList<>();
        this.listChatUsers = new ArrayList<>();
        userService = new UserServiceImpl();
    }

    public void init() {
        this.rooms.put(MGRoomTaiXiu.getKeyRoom((short) 1), new MGRoomTaiXiu("TaiXiu_1", this.referenceTaiXiuId, (short) 1));
        this.rooms.put(MGRoomTaiXiu.getKeyRoom((short) 0), new MGRoomTaiXiu("TaiXiu_0", this.referenceTaiXiuId, (short) 0));
        this.loadData();
        this.loadChatData();
        this.loadChatUsers();
        BitZeroServer.getInstance().getTaskScheduler().scheduleAtFixedRate(this.gameLoopTask, 10, 1, TimeUnit.SECONDS);
        BitZeroServer.getInstance().getTaskScheduler().scheduleAtFixedRate(() -> {
            chatTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    final MGRoomTaiXiu roomTXVin = TaiXiuModule.this.getRoomTX((short) 1);
                    final ChatMsg msg = new ChatMsg();
                    msg.nickname = TaiXiuModule.this.listChatUsers.get(Util.generateRandom(TaiXiuModule.this.listChatUsers.size()));
                    msg.mesasge = TaiXiuModule.this.listChat.get(Util.generateRandom(TaiXiuModule.this.listChat.size()));
                    roomTXVin.sendMessageToRoom(msg);
                }
            }, Util.randInt(5_000, 30_000));
        }, Util.randInt(5, 10), 30, TimeUnit.SECONDS);
        BitZeroServer.getInstance().getTaskScheduler().schedule(this.serverReadyTask, 10, TimeUnit.SECONDS);
        Debug.trace("SERVER READY TASK RUNNING...");
        this.getParentExtension().addEventListener(BZEventType.USER_DISCONNECT, this);
        try {
            final int remainTimeTraThuongThanhDu = MiniGameUtils.calculateTimeRewardOnNextDay("");
            BitZeroServer.getInstance().getTaskScheduler().schedule(this.rewardThanhDuDailyTask, remainTimeTraThuongThanhDu, TimeUnit.SECONDS);
        } catch (ParseException e) {
            Debug.trace("Calculate time reward Thanh du error ", e.getMessage());
            GU.sendOperation("Có lỗi TaiXiuModule, Exception: " + KingUtil.printException(e));
        }

        KingUtil.printLog("hoan tien tai xiu voi user bi loi phien");
        // Hoàn tiền cho các tài khoản đặt cược ván tài xỉu bị reset đột ngột
        IMap<String, Long> usersBetTaiXiu = HCMap.getUsersBetTaiXiu();
        for (Map.Entry<String, Long> entry : usersBetTaiXiu.entrySet()) {
            String nickname = entry.getKey();
            Long betvalue = entry.getValue();
            KingUtil.printLog("MGRoomTaiXiu restore money user: "+nickname+", number: "+betvalue);
            MoneyResponse res = userService.updateMoney(nickname, betvalue, "vin", "TaiXiu", "Tài Xỉu Đu Dây: Hoàn tiền phiên lỗi", "Hoàn tiền phiên lỗi", 0L, 0L, TransType.NO_VIPPOINT);
        }
        usersBetTaiXiu.clear();
    }

    public void loadChatData() {
        try (BufferedReader br2 = new BufferedReader(new InputStreamReader(new FileInputStream("config/list_chat.txt"), StandardCharsets.UTF_8))) {
            String entry;
            while ((entry = br2.readLine()) != null) {
                this.listChat.add(entry);
            }
            Debug.trace("BOT CHAT :" + this.listChat.size());
        } catch (IOException ex) {
            GU.sendOperation("Có lỗi TaiXiuModule, Exception: " + KingUtil.printException(ex));
        }
    }

    public void loadChatUsers() {
        try (BufferedReader br2 = new BufferedReader(new InputStreamReader(new FileInputStream("config/listchat.txt"), StandardCharsets.UTF_8))) {
            String entry;
            while ((entry = br2.readLine()) != null) {
                this.listChatUsers.add(entry);
            }
            Debug.trace("BOT CHAT USERS :" + this.listChatUsers.size());
        } catch (IOException ex) {
            GU.sendOperation("Có lỗi TaiXiuModule, Exception: " + KingUtil.printException(ex));
        }
    }

    public void handleServerEvent(final IBZEvent ibzevent) throws BZException {
        if (ibzevent.getType() == BZEventType.USER_DISCONNECT) {
            final User user = (User) ibzevent.getParameter(BZEventParam.USER);
            this.userDis(user);
        }
    }

    private void userDis(final User user) {
        final MGRoom room = (MGRoom) user.getProperty("MGROOM_TAI_XIU_INFO");
        if (room != null) {
            room.quitRoom(user);
        }
    }

    private void loadData() {
        this.referenceTaiXiuId = 1L;
        try {
            this.referenceTaiXiuId = this.mgService.getReferenceId(2);
            this.lichSuPhienTX = this.txService.getListLichSuPhien(120, 1);
        } catch (SQLException e) {
            Debug.trace("Load reference error ", e.getMessage());
            GU.sendOperation("Có lỗi TaiXiuModule, Exception: " + KingUtil.printException(e));
        }
        try {
            this.generationTX.readConfig();
        } catch (IOException e2) {
            Debug.trace("Load cau tai xiu error ", e2.getMessage());
            GU.sendOperation("Có lỗi TaiXiuModule, Exception: " + KingUtil.printException(e2));
        }
        try {
            this.fundRutLoc = this.txService.getPotTanLoc();
            if (this.fundRutLoc < 100000L) {
                this.countRutLoc = -1;
            }
        } catch (SQLException e) {
            Debug.trace("Load fund tan loc error ", e.getMessage());
            GU.sendOperation("Có lỗi TaiXiuModule, Exception: " + KingUtil.printException(e));
        }
        Debug.trace("Phien TX: " + this.referenceTaiXiuId);
        Debug.trace("SIZE LSDG: " + this.lichSuPhienTX.size());
        Debug.trace("LSDG: " + TaiXiuUtils.logLichSuPhien(this.lichSuPhienTX, 120));
        Debug.trace("Fund tan loc: " + this.fundRutLoc);
    }

    private void saveReferences() {
        try {
            this.mgService.saveReferenceId(this.referenceTaiXiuId, 2);
        } catch (SQLException e) {
            Debug.trace("Save reference error " + e.getMessage());
            GU.sendOperation("Có lỗi TaiXiuModule, Exception: " + KingUtil.printException(e));
        }
    }

    public void handleClientRequest(final User user, final DataCmd dataCmd) {
        switch (dataCmd.getId()) {
            case 2000: {
                this.subscribeMiniGame(user, dataCmd);
                break;
            }
            case 2001: {
                this.unsubscribeMiniGame(user, dataCmd);
                break;
            }
            case 2002: {
                this.changeRoom(user, dataCmd);
                break;
            }
            case 2110: {
                if (GameUtils.disablePlayMiniGame(user)) {
                    return;
                }
                this.betTaiXiu(user, dataCmd);
                break;
            }
            case 2116: {
                this.getLichSuPhienTX(user);
                break;
            }
            case 2118: {
                if (GameUtils.disablePlayMiniGame(user)) {
                    return;
                }
                this.tanLoc(user, dataCmd);
                break;
            }
            case 2119: {
                if (GameUtils.disablePlayMiniGame(user)) {
                    return;
                }
                this.rutLoc(user, dataCmd);
                break;
            }
        }
    }

    private void subscribeMiniGame(final User user, final DataCmd dataCmd) {
        final SubcribeMinigameCmd cmd = new SubcribeMinigameCmd(dataCmd);
        this.doSubcribeMiniGame(user, cmd.gameId, cmd.roomId);
        final LichSuPhienMsg msgLSGD = new LichSuPhienMsg();
        msgLSGD.data = TaiXiuUtils.buildLichSuPhien(this.lichSuPhienTX, 22);
        this.send(msgLSGD, user);
        final UpdateRutLocMsg rutLocMsg = new UpdateRutLocMsg();
        try {
            rutLocMsg.soLuotRut = this.txService.getLuotRutLoc(user.getName());
        } catch (Exception e) {
            Debug.trace("Get so luot rut loc " + user.getName() + " error ", e.getMessage());
            GU.sendOperation("Có lỗi TaiXiuModule, Exception: " + KingUtil.printException(e));
        }
        this.send(rutLocMsg, user);
        final UpdateFundTanLocMsg fundRLMsg = new UpdateFundTanLocMsg();
        fundRLMsg.value = this.fundRutLoc;
        this.send(fundRLMsg, user);
    }

    private void doSubcribeMiniGame(final User user, final short gameId, final short roomId) {
        if (gameId == 2) {
            final short moneyType = MGRoomTaiXiu.getMoneyType(roomId);
            final String keyRoom = MGRoomTaiXiu.getKeyRoom(moneyType);
            final MGRoomTaiXiu roomTX = (MGRoomTaiXiu) this.getGame(keyRoom);
            if (roomTX != null) {
                roomTX.joinRoom(user);
                roomTX.updateTaiXiuInfo(user, this.getRemainTimeRutLoc());
                return;
            }
            CommonHandle.writeErrLog("Game TAI XIU not found");
        } else {
            Debug.trace("Game id not found");
        }
    }

    private void unsubscribeMiniGame(final User user, final DataCmd dataCmd) {
        final UnsubscribeMiniGameCmd cmd = new UnsubscribeMiniGameCmd(dataCmd);
        this.doUnsubscribeMiniGame(user, cmd.gameId, cmd.roomId);
    }

    private void doUnsubscribeMiniGame(final User user, final short gameId, final short roomId) {
        if (gameId == 2) {
            final short moneyType = MGRoomTaiXiu.getMoneyType(roomId);
            final String keyRoom = MGRoomTaiXiu.getKeyRoom(moneyType);
            final MGRoom room = this.getGame(keyRoom);
            if (room == null) {
                return;
            }
            room.quitRoom(user);
        }
    }

    private void changeRoom(final User user, final DataCmd dataCmd) {
        final ChangeRoomMinigameCmd cmd = new ChangeRoomMinigameCmd(dataCmd);
        this.doUnsubscribeMiniGame(user, cmd.gameId, cmd.lastRoomId);
        this.doSubcribeMiniGame(user, cmd.gameId, cmd.newRoomId);
    }

    private void startNewRoundTX() {
        final MGRoomTaiXiu roomTXVin = this.getRoomTX((short) 1);
        final MGRoomTaiXiu roomTXXu = this.getRoomTX((short) 0);
        roomTXVin.startNewGame(++this.referenceTaiXiuId);
        roomTXXu.startNewGame(this.referenceTaiXiuId);
        final StartNewGameTaiXiuMsg msg = new StartNewGameTaiXiuMsg();
        msg.referenceId = this.referenceTaiXiuId;
        msg.remainTimeRutLoc = this.getRemainTimeRutLoc();
        this.sendMessageToTaiXiuNewThread(msg);
        this.saveReferences();
    }

    private void scheduleBot() {
        try {
            this.botsXu.clear();
            synchronized (this.botsVin) {
                this.botsVin.clear();
                this.botsVin.addAll(BotMinigame.getBotTaiXiu("vin"));
                Debug.trace("BOTS VIN: " + this.botsVin.size());
                final List<BotTaiXiu> botsVip = BotMinigame.getVipBotTaiXiu();
                this.botsVin.addAll(botsVip);
                Debug.trace("TX BOTS VIP: " + botsVip.size());
            }
            synchronized (this.botsXu) {
                this.botsXu.addAll(BotMinigame.getBotTaiXiu("xu"));
                Debug.trace("BOTS XU: " + this.botsXu.size());
            }
        } catch (Exception e) {
            GameUtils.sendAlert("Bot tai xiu start error: " + e.getMessage() + ", time= " + DateTimeUtils.getCurrentTime());
            GU.sendOperation("Có lỗi TaiXiuModule, Exception: " + KingUtil.printException(e));
        }
    }

    private void botBet(final int count) {
        final MGRoomTaiXiu roomVin = this.getRoomTX((short) 1);
        synchronized (this.botsVin) {
            this.botsVin.forEach(b -> {
                if (b.getTimeBetting() != 60 - count) {
                    return;
                }
                try {
                    roomVin.betTaiXiu(b.getNickname(), 0, b.getBetValue(), b.getTimeBetting(), (short) 1, b.getBetSide(), true);
                } catch (Exception ex) {
                    Debug.warn("Bot bet failed, room Vin");
                }
            });
        }

        final MGRoomTaiXiu roomXu = this.getRoomTX((short) 0);
        synchronized (this.botsXu) {
            this.botsXu.forEach(b -> {
                if (b.getTimeBetting() != 60 - count) {
                    return;
                }
                try {
                    roomXu.betTaiXiu(b.getNickname(), 0, b.getBetValue(), b.getTimeBetting(), (short) 0, b.getBetSide(), true);
                } catch (Exception ex) {
                    Debug.warn("Bot bet failed, room Xu");
                }
            });
        }
    }

    public void betTaiXiu(final User user, final DataCmd dataCmd) {
        // Kiểm tra xem có đang cho mở bet Tài Xỉu ko?
        try {
            if (cacheService.checkKeyExist("allow_bet_taixiu")) {
                String allowBetTaixiu = cacheService.getValueStr("allow_bet_taixiu");
                if (allowBetTaixiu.equals("false"))
                    return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        final BetTaiXiuCmd cmd = new BetTaiXiuCmd(dataCmd);
        final MGRoomTaiXiu roomTX = this.getRoomTX(cmd.moneyType);
        if (roomTX != null) {
            roomTX.betTaiXiu(user, cmd);
        }
    }

    private void gameLoop() {
        try {
            this.botBet(++this.count);
            if (this.countRutLoc > -1) {
                ++this.countRutLoc;
            }
            final MGRoomTaiXiu roomTXVin = this.getRoomTX((short) 1);
            boolean isBalancePot = false;
            if(this.count >= 58)
                isBalancePot = true;
            roomTXVin.updateTaiXiuPerSecond(isBalancePot);
            final MGRoomTaiXiu roomTXXu = this.getRoomTX((short) 0);
            roomTXXu.updateTaiXiuPerSecond(isBalancePot);
            this.sendTXTime(roomTXVin.getRemainTime(), roomTXVin.isBetting());
            switch (this.count) {
                case 55: {
                    roomTXVin.disableBetting();
                    roomTXXu.disableBetting();
                    break;
                }
                case 58: {
                    this.forceBalanceLateGame(roomTXVin);
                    break;
                }
                case 60: {
                    roomTXVin.finish();
                    roomTXXu.finish();
                    break;
                }
                case 61: {
                    this.generateTaiXiuDices(roomTXVin, roomTXXu);
                    break;
                }
                case 65: {
                    BitZeroServer.getInstance().getTaskScheduler().schedule(this.calculatingTXVinTask, 1, TimeUnit.SECONDS);
                    BitZeroServer.getInstance().getTaskScheduler().schedule(this.calculatingTXXuTask, 1, TimeUnit.SECONDS);
                    break;
                }
                case 85: {
                    final ScheduleBotTask t = new ScheduleBotTask();
                    this.executor.execute(t);
                    break;
                }
                case 90: {
                    roomTXVin.getBalanceTX().startNewRound();
                    this.startNewRoundTX();
                    this.count = 0;
                    break;
                }
            }
            if (this.count > 90) {
                roomTXVin.getBalanceTX().startNewRound();
                this.startNewRoundTX();
                this.count = 0;
                GU.sendOperation("TaiXiu: count > 90");
            }
            switch (this.countRutLoc) {
                case 869: {
                    Debug.trace("RUT LOC");
                    this.rutLocPrizes = RutLocUtils.getPrizes(this.fundRutLoc, 10);
                    this.phanBoGiaiThuong = RutLocUtils.phanBoGiaiThuong(this.tongSoNguoiRutLocLanTruoc, 10);
                    final StringBuilder builder = new StringBuilder();
                    for (int j : this.phanBoGiaiThuong) {
                        builder.append(j);
                        builder.append(",");
                    }
                    Debug.trace("PHAN BO GIAI THUONG: " + builder.toString());
                    this.enableRutLoc = true;
                    this.sendMessageToTaiXiuNewThread(new EnableRutLocMsg());
                    break;
                }
                case 900: {
                    Debug.trace("PHIEN RUT LOC MOI");
                    this.countRutLoc = 0;
                    if (this.fundRutLoc < 100000L) {
                        this.countRutLoc = -1;
                    }
                    this.tongSoNguoiRutLocLanTruoc = (Math.max(this.countReqRutLoc, 30));
                    Debug.trace("Tong so nguoi rut loc: " + this.tongSoNguoiRutLocLanTruoc);
                    this.countReqRutLoc = 0;
                    this.enableRutLoc = false;
                    final StartNewRoundRutLocMsg newRoundMsg = new StartNewRoundRutLocMsg();
                    newRoundMsg.remainTime = this.getRemainTimeRutLoc();
                    this.sendMessageToTaiXiuNewThread(newRoundMsg);
                    break;
                }
            }
        } catch (Exception e) {
            Debug.trace("Exception: " + e.getMessage(), e);
            GU.sendOperation("Có lỗi TaiXiuModule, Exception: " + KingUtil.printException(e));
        }
    }

    private void forceBalanceLateGame(final MGRoomTaiXiu roomTXVin) {
        final int randomMin = ThreadLocalRandom.current().nextInt(0, (int) this.MaxCtrl);
        this.MinCtrl = randomMin;
        Vi vi = dealVi(roomTXVin.getUserBetTai(), roomTXVin.getUserBetXiu());
        this.forceBetSide = (short) (vi.isBelow() ? 0 : 1);
        new Thread(() -> {
            try (final PrintStream out = new PrintStream(new FileOutputStream("logTaiXiu.txt", true))) {
                out.println(roomTXVin.referenceId + "> Tai:" + roomTXVin.nguoichoidatTai + "(" + roomTXVin.getUserBetTai() + ") Xiu:" + roomTXVin.nguoichoidatXiu + "(" + roomTXVin.getUserBetXiu() + ") >" + this.forceBetSide);
            } catch (Exception ex) {
                GU.sendOperation("Có lỗi TaiXiuModule, Exception: " + KingUtil.printException(ex));
            }
        }).start();
    }

    private Vi dealVi(long totalAbove, long totalBelow) {
        long distance = Math.abs(totalAbove - totalBelow);
        if (distance < 10_000) return new Vi();

        int rate = Util.getRandom(1, 100);
        if (((distance < 50_000 && rate >= 93) || (distance >= 50_000 && distance < 200_000 && rate >= 91) ||
                (distance >= 200_000 && distance < 500_000 && rate >= 88) ||
                (distance >= 500_000 && distance < 1_000_000 && rate >= 86) ||
                (distance >= 1_000_000 && distance < 2_000_000 && rate >= 83) || (distance >= 2_000_000 && rate >= 78)))
            return new Vi(totalBelow < totalAbove);

        return new Vi();
    }

    private void resetForceBalance() {
        this.forceBetSide = -1;
    }

    private void generateTaiXiuDices(final MGRoomTaiXiu roomTXVin, final MGRoomTaiXiu roomTXXu) {
        Debug.trace("FORCE==============" + this.forceBetSide);
        final short[] dices;
        boolean genRandomTxResult = false;
        try {
            if (cacheService.checkKeyExist("gen_random_tx_result")) {
                String genRandomTxResultStr = cacheService.getValueStr("gen_random_tx_result");
                if (genRandomTxResultStr.equals("true"))
                    genRandomTxResult = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        KingUtil.printLog("generateTaiXiuDices() genRandomTxResult: "+genRandomTxResult);
        if(genRandomTxResult)
            dices = this.generationTX.generateRandomResult();
        else
            dices = this.generationTX.generateResult(this.forceBetSide);

        this.resetForceBalance();
        final short total = (short) (dices[0] + dices[1] + dices[2]);
        roomTXVin.updateResultDices(dices, this.result = (short) ((total > 10) ? 1 : 0));
        roomTXXu.updateResultDices(dices, this.result);
        final ResultTaiXiu resultTX = new ResultTaiXiu();
        resultTX.referenceId = this.referenceTaiXiuId;
        resultTX.result = this.result;
        resultTX.dice1 = dices[0];
        resultTX.dice2 = dices[1];
        resultTX.dice3 = dices[2];
        Debug.trace("GENERATE RESULT DICES: " + dices[0] + " - " + dices[1] + " - " + dices[2] + "   " + this.result);
        this.lichSuPhienTX.add(resultTX);
        if (this.lichSuPhienTX.size() > 120) {
            this.lichSuPhienTX.remove(0);
        }
    }

    private void getLichSuPhienTX(final User user) {
        final LichSuPhienMsg msg = new LichSuPhienMsg();
        msg.data = TaiXiuUtils.buildLichSuPhien(this.lichSuPhienTX, 22);
        Debug.trace("LSDG: " + TaiXiuUtils.logLichSuPhien(this.lichSuPhienTX, 120));
        this.send(msg, user);
    }

    private void tanLoc(final User user, final DataCmd dataCmd) {
        final TanLocCMD cmd = new TanLocCMD(dataCmd);
        final TanLocMsg msg = new TanLocMsg();
        msg.result = 1;
        long curretnMoney = userService.getMoneyUserCache(user.getName(), "vin");
        if (cmd.money <= curretnMoney) {
            if (cmd.money >= 1000L) {
                final MoneyResponse response = userService.updateMoney(user.getName(), -cmd.money, "vin", "TaiXiu", "T\u00c3 i X\u00e1»\u2030u Đu Dây - T\u00c3¡n l\u00e1»\u2122c", "T\u00c3¡n l\u00e1»\u2122c t\u00c3 i x\u00e1»\u2030u", 0L, null, TransType.NO_VIPPOINT);
                final boolean success;
                if (response != null && response.isSuccess() && (success = this.updateFundRutLoc(cmd.money))) {
                    curretnMoney = response.getCurrentMoney();
                    msg.result = 0;
                    try {
                        this.txService.logTanLoc(user.getName(), cmd.money);
                    } catch (IOException | TimeoutException | InterruptedException ex) {
                        GU.sendOperation("Có lỗi TaiXiuModule, Exception: " + KingUtil.printException(ex));
                    }
                    if (this.countRutLoc == -1 && this.fundRutLoc >= 100000L) {
                        this.countRutLoc = 0;
                        final StartNewRoundRutLocMsg newRoundMsg = new StartNewRoundRutLocMsg();
                        newRoundMsg.remainTime = this.getRemainTimeRutLoc();
                        this.sendMessageToTaiXiu(newRoundMsg);
                    }
                }
            } else {
                msg.result = 3;
            }
        } else {
            msg.result = 2;
        }
        msg.currentMoney = curretnMoney;
        this.send(msg, user);
    }

    private void rutLoc(final User user, final DataCmd dataCmd) {
        final ResultRutLocMsg msg = new ResultRutLocMsg();
        final UserServiceImpl userService = new UserServiceImpl();
        long currentMoney = userService.getMoneyUserCache(user.getName(), "vin");
        if (this.countRutLoc > -1) {
            if (this.enableRutLoc) {
                int soLuotRut = 0;
                try {
                    soLuotRut = this.txService.getLuotRutLoc(user.getName());
                } catch (SQLException e) {
                    Debug.trace("Get so luot rut loc error ", e.getMessage());
                    GU.sendOperation("Có lỗi TaiXiuModule, Exception: " + KingUtil.printException(e));
                }
                if (soLuotRut > 0) {
                    int indexPirze = -1;
                    int prize = 0;
                    for (int i = 0; i < this.phanBoGiaiThuong.length; ++i) {
                        if (this.countReqRutLoc == this.phanBoGiaiThuong[i]) {
                            indexPirze = i;
                            break;
                        }
                    }
                    if (0 <= indexPirze && indexPirze < this.rutLocPrizes.length) {
                        prize = this.rutLocPrizes[indexPirze];
                    }
                    if (prize > this.fundRutLoc) {
                        prize = 0;
                    }
                    final MoneyResponse response;
                    if (prize > 0 && (response = userService.updateMoney(user.getName(), prize, "vin", "TaiXiu", "T\u00c3 i x\u00e1»\u2030u Đu Dây - R\u00c3ºt l\u00e1»\u2122c", "C\u00e1»\u2122ng ti\u00e1»\ufffdn r\u00c3ºt l\u00e1»\u2122c", 0L, null, TransType.NO_VIPPOINT)) != null && response.isSuccess()) {
                        currentMoney = response.getCurrentMoney();
                    }
                    ++this.countReqRutLoc;
                    --soLuotRut;
                    msg.prize = prize;
                    try {
                        this.txService.updateLuotRutLoc(user.getName(), -1);
                        final UpdateRutLocMsg soLuotRutMsg = new UpdateRutLocMsg();
                        soLuotRutMsg.soLuotRut = soLuotRut;
                        this.send(soLuotRutMsg, user);
                    }
//                    catch (IOException | InterruptedException | TimeoutException ex3) {
//                        final Exception ex;
//                        final Exception e2 = ex;
//                        Debug.trace((Object[])new Object[] { "Update luot rut loc error ", e2.getMessage() });
//                    }
                    catch (Exception ex3) {
                        Debug.trace("Update luot rut loc error ", ex3.getMessage());
                        GU.sendOperation("Có lỗi TaiXiuModule, Exception: " + KingUtil.printException(ex3));
                    }

                } else {
                    msg.prize = -2;
                }
            } else {
                msg.prize = -1;
            }
        } else {
            msg.prize = -3;
        }
        msg.currentMoney = currentMoney;
        this.send(msg, user);
        if (msg.prize > 0) {
            this.updateFundRutLoc(-msg.prize);
            try {
                this.txService.logRutLoc(user.getName(), msg.prize, this.countReqRutLoc, this.fundRutLoc);
            }
//            catch (IOException | InterruptedException | TimeoutException ex4) {
//                final Exception ex2;
//                final Exception e3 = ex2;
//                Debug.trace((Object[])new Object[] { "Log rut loc error ", e3.getMessage() });
//            }
            catch (Exception ex4) {
                Debug.trace("Log rut loc error ", ex4.getMessage());
                GU.sendOperation("Có lỗi TaiXiuModule, Exception: " + KingUtil.printException(ex4));
            }
        }
    }

    private boolean updateFundRutLoc(final long moneyExchagne) {
        boolean success = false;
        this.fundRutLoc += moneyExchagne;
        if (this.fundRutLoc < 0L) {
            Debug.trace("Quy rut loc " + this.fundRutLoc + " < 0");
        }
        try {
            this.txService.updatePotTanLoc(this.fundRutLoc);
            final UpdateFundTanLocMsg msg = new UpdateFundTanLocMsg();
            msg.value = this.fundRutLoc;
            this.sendMessageToTaiXiu(msg);
            success = true;
        } catch (Exception e) {
            Debug.trace("Update fund tan loc error ", e.getMessage());
            GU.sendOperation("Có lỗi TaiXiuModule, Exception: " + KingUtil.printException(e));
        }
        return success;
    }

    private short getRemainTimeRutLoc() {
        if (this.countRutLoc == -1) {
            return 0;
        }
        int remainTime = 900 - this.countRutLoc;
        if (remainTime < 0) {
            remainTime = 0;
        }
        return (short) remainTime;
    }

    private void sendMessageToTaiXiuNewThread(final BaseMsg msg) {
        final SendMessageToTXThread t = new SendMessageToTXThread(false, msg);
        this.executor.execute(t);
    }

    private void sendTXTime(final short remainTime, final boolean betting) {
        final BroadcastTXTimeMsg msg = new BroadcastTXTimeMsg();
        msg.remainTime = (byte) remainTime;
        msg.betting = betting;
        final SendMessageToTXThread t = new SendMessageToTXThread(true, msg);
        this.executor.execute(t);
    }

    private void sendMessageToAllUsers(final BaseMsg msg) {
        final List<User> users = ExtensionUtility.globalUserManager.getAllUsers();
        if (users != null && !users.isEmpty()) {
            this.send(msg, users.stream().map(User::getSession).collect(Collectors.toList()));
        }
    }

    private void sendMessageToTaiXiu(final BaseMsg msg) {
        final MGRoomTaiXiu roomTXVin = this.getRoomTX((short) 1);
        roomTXVin.sendMessageToRoom(msg);
        final MGRoomTaiXiu roomTXXu = this.getRoomTX((short) 0);
        roomTXXu.sendMessageToRoom(msg);
    }

    public MGRoom getGame(final String key) {
        return this.rooms.get(key);
    }

    private MGRoomTaiXiu getRoomTX(final short moneyType) {
        final String keyRoom = MGRoomTaiXiu.getKeyRoom(moneyType);
        return (MGRoomTaiXiu) this.getGame(keyRoom);
    }

    private final class GameLoopTask implements Runnable {
        @Override
        public void run() {
            try {
                TaiXiuModule.this.gameLoop();
            } catch (Exception e) {
                e.printStackTrace();
                GU.sendOperation("Có lỗi TaiXiuModule, Exception: " + KingUtil.printException(e));
            }
        }
    }

    private final class ServerReadyTask implements Runnable {
        @Override
        public void run() {
            if (!TaiXiuModule.this.serverReady) {
                Debug.trace("START MINI GAME");
                TaiXiuModule.this.serverReady = true;
                final ScheduleBotTask t = new ScheduleBotTask();
                TaiXiuModule.this.executor.execute(t);
                TaiXiuModule.this.startNewRoundTX();
            }
        }
    }

    private final class CalculatingTaiXiuPrize implements Runnable {
        private final byte moneyType;
        private final UpdatingAllTopTask runUpdatingAllTopTask;

        public CalculatingTaiXiuPrize(final byte moneyType) {
            this.moneyType = moneyType;
            this.runUpdatingAllTopTask = new UpdatingAllTopTask(moneyType);
        }

        @Override
        public void run() {
            ScheduledFuture future = scheduledExecutorService.schedule(() -> {
                GU.sendOperation("[CẢNH BÁO - KHẢ NĂNG LỖI]Xử lý TaiXiu-CalculatingTaiXiuPrize chậm hơn 3s: moneyType " + moneyType);
            }, 3, TimeUnit.SECONDS);
            LocalDateTime start = LocalDateTime.now();

            try {
                final MGRoomTaiXiu room = TaiXiuModule.this.getRoomTX(this.moneyType);
                room.calculatePrize(TaiXiuModule.this.referenceTaiXiuId);
            } catch (Exception e) {
                Debug.trace("Calculate TX " + this.moneyType + ", phien= " + TaiXiuModule.this.referenceTaiXiuId + " error: " + e.getMessage());
                GU.sendOperation("Có lỗi TaiXiuModule, Exception: " + KingUtil.printException(e));
            }
            BitZeroServer.getInstance().getTaskScheduler().schedule(this.runUpdatingAllTopTask, 1, TimeUnit.MILLISECONDS);

            Duration duration = Duration.between(start, LocalDateTime.now());
            long durationInMs = duration.toMillis();
            if (durationInMs > 2000) {
                GU.sendOperation("[CẢNH BÁO]Xử lý TaiXiu-CalculatingTaiXiuPrize chậm: moneyType " + moneyType + " - " + durationInMs + " ms.");
            }

            if (!future.isDone()) {
                future.cancel(true);
            }
        }
    }

    private final class UpdatingAllTopTask implements Runnable {
        private final byte moneyType;

        public UpdatingAllTopTask(final byte moneyType) {
            this.moneyType = moneyType;
        }

        @Override
        public void run() {
            ScheduledFuture future = scheduledExecutorService.schedule(() -> {
                GU.sendOperation("[CẢNH BÁO - KHẢ NĂNG LỖI]Xử lý TaiXiu-UpdatingAllTopTask chậm hơn 3s: moneyType " + moneyType);
            }, 3, TimeUnit.SECONDS);
            LocalDateTime start = LocalDateTime.now();

            TaiXiuModule.this.txService.updateAllTop(moneyType);

            Duration duration = Duration.between(start, LocalDateTime.now());
            long durationInMs = duration.toMillis();
            if (durationInMs > 5000) {
                GU.sendOperation("[CẢNH BÁO]Xử lý TaiXiu-UpdatingAllTopTask chậm: moneyType " + moneyType + " - " + durationInMs + " ms.");
            }

            if (!future.isDone()) {
                future.cancel(true);
            }
        }
    }

    private final class RewardThanhDuDaily implements Runnable {
        @Override
        public void run() {
            TaiXiuUtils.rewardThanhDu();
            BitZeroServer.getInstance().getTaskScheduler().schedule(TaiXiuModule.this.rewardThanhDuDailyTask, 24, TimeUnit.HOURS);
            Debug.trace("Tra thuong Thanh Du");
        }
    }

    private final class SendMessageToTXThread extends Thread {
        private final BaseMsg msg;
        private final boolean all;

        private SendMessageToTXThread(final boolean all, final BaseMsg msg) {
            this.msg = msg;
            this.all = all;
        }

        @Override
        public void run() {
            if (this.all) {
                TaiXiuModule.this.sendMessageToAllUsers(this.msg);
            } else {
                TaiXiuModule.this.sendMessageToTaiXiu(this.msg);
            }
        }
    }

    private final class ScheduleBotTask extends Thread {
        @Override
        public void run() {
            try {
                Debug.trace("Schedule bot running ...");
                TaiXiuModule.this.scheduleBot();
                Debug.trace("Schedule bot finished ...");
            } catch (Exception ex) {
                Debug.trace(ex.getMessage());
                GU.sendOperation("Có lỗi TaiXiuModule, Exception: " + KingUtil.printException(ex));
            }
        }
    }
}
