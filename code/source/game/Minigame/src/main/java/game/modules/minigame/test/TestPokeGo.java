// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.test;

import game.modules.minigame.entities.pokego.*;
import game.modules.minigame.utils.PokeGoUtils;

import java.util.ArrayList;
import java.util.Random;

public class TestPokeGo
{
    public long pot;
    public long fund;
    public final long initPotValue;
    public final long hanMuc;
    private final long betValue;
    private short moneyType;
    private final Lines lines;
    public int numNoHu;
    public int numNoHuForce;
    public int numThangLon;
    public int numTruot;
    public long numKhongDuTraThuong;
    public long maxHu;
    public long minHu;
    public int triplePokeBall;
    public int triplePikaChu;
    public int tripleBubasaur;
    public int tripleClefable;
    public int tripleMouse;
    public int doubleMouse;
    public int tripleToGePi;
    public int doubleToGePi;
    public long tongTraThuong;
    
    public TestPokeGo() {
        this.pot = 500000L;
        this.fund = 3000000L;
        this.initPotValue = 500000L;
        this.hanMuc = -3000000L;
        this.betValue = 100L;
        this.lines = new Lines();
        this.numNoHu = 0;
        this.numNoHuForce = 0;
        this.numThangLon = 0;
        this.numTruot = 0;
        this.numKhongDuTraThuong = 0L;
        this.maxHu = 0L;
        this.minHu = 1000000L;
        this.triplePokeBall = 0;
        this.triplePikaChu = 0;
        this.tripleBubasaur = 0;
        this.tripleClefable = 0;
        this.tripleMouse = 0;
        this.doubleMouse = 0;
        this.tripleToGePi = 0;
        this.doubleToGePi = 0;
        this.tongTraThuong = 0L;
    }
    
    private boolean validateRecharge(final String username) {
        return true;
    }
    
    public void play(final String username, final String linesStr) {
        int result = 0;
        final String[] lineArr = linesStr.split(",");
        final long currentMoney = 1000000L;
        final int soLanNoHu = 20000;
        final long totalBetValue = lineArr.length * this.betValue;
        if (lineArr.length > 0 && !linesStr.isEmpty()) {
            if (totalBetValue > 0L) {
                if (totalBetValue <= 1000000L) {
                    final long fee = totalBetValue * 2L / 100L;
                    final long moneyToPot = totalBetValue / 100L;
                    final long moneyToFund = totalBetValue - fee - moneyToPot;
                    this.fund += moneyToFund;
                    this.pot += moneyToPot;
                    boolean enoughPair = false;
                    final ArrayList<AwardsOnLine> awardsOnLines = new ArrayList<>();
                    long totalPrizes = 0L;
                Label_0129:
                    while (!enoughPair) {
                        awardsOnLines.clear();
                        totalPrizes = 0L;
                        result = 0;
                        boolean forceNoHu = false;
                        if (lineArr.length >= 5 && soLanNoHu > 0 && this.fund > this.initPotValue * 2L) {
                            final Random rd = new Random();
                            if (rd.nextInt(soLanNoHu) == 0) {
                                forceNoHu = true;
                            }
                        }
                        final Item[][] matrix = forceNoHu ? PokeGoUtils.generateMatrixNoHu(lineArr) : PokeGoUtils.generateMatrix();
                        for (final Object entry : lineArr) {
                            final ArrayList<Award> awardList = new ArrayList<>();
                            final Line line = PokeGoUtils.getLine(this.lines, matrix, Integer.parseInt((String) entry));
                            PokeGoUtils.calculateLine(line, awardList);
                            for (final Award award : awardList) {
                                final int money = (int) (award.getRatio() * this.betValue);
                                final AwardsOnLine aol = new AwardsOnLine(award, money, line.getName());
                                awardsOnLines.add(aol);
                            }
                        }
                        final StringBuilder builderLinesWin = new StringBuilder();
                        final StringBuilder builderPrizesOnLine = new StringBuilder();
                        for (final AwardsOnLine entry2 : awardsOnLines) {
                            if (entry2.getAward() == Award.TRIPLE_POKER_BALL) {
                                if (this.moneyType == 1 && this.betValue == 10000L && !this.validateRecharge(username)) {
                                    continue Label_0129;
                                }
                                result = 3;
                                totalPrizes += this.pot;
                            }
                            else {
                                totalPrizes += entry2.getMoney();
                            }
                            builderLinesWin.append(",");
                            builderLinesWin.append(entry2.getLineId());
                            builderPrizesOnLine.append(",");
                            builderPrizesOnLine.append(entry2.getMoney());
                        }
                        if (builderLinesWin.length() > 0) {
                            builderLinesWin.deleteCharAt(0);
                        }
                        if (builderPrizesOnLine.length() > 0) {
                            builderPrizesOnLine.deleteCharAt(0);
                        }
                        if (result == 3) {
                            if (this.fund - this.initPotValue < 0L) {
                                ++this.numKhongDuTraThuong;
                                continue;
                            }
                        }
                        else if (this.fund - totalPrizes < 0L) {
                            ++this.numKhongDuTraThuong;
                            continue;
                        }
                        enoughPair = true;
                        for (final AwardsOnLine entry2 : awardsOnLines) {
                            switch (entry2.getAward()) {
                                case TRIPLE_POKER_BALL: {
                                    ++this.triplePokeBall;
                                    continue;
                                }
                                case TRIPLE_PIKACHU: {
                                    ++this.triplePikaChu;
                                    continue;
                                }
                                case TRIPLE_BULBASAUR: {
                                    ++this.tripleBubasaur;
                                    continue;
                                }
                                case TRIPLE_CLEFABLE: {
                                    ++this.tripleClefable;
                                    continue;
                                }
                                case TRIPLE_MOUSE: {
                                    ++this.tripleMouse;
                                    continue;
                                }
                                case DOUBLE_MOUSE: {
                                    ++this.doubleMouse;
                                    continue;
                                }
                                case TRIPLE_TOGEPI: {
                                    ++this.tripleToGePi;
                                    continue;
                                }
                                case DOUBLE_TOGEPI: {
                                    ++this.doubleToGePi;
                                    continue;
                                }
                                default: {
                                }
                            }
                        }
                        if (totalPrizes > 0L) {
                            if (result == 3) {
                                System.out.println("NO HU= " + this.pot + ", FUND= " + this.fund + ", FORCE= " + forceNoHu);
                                if (this.maxHu < this.pot) {
                                    this.maxHu = this.pot;
                                }
                                if (this.minHu > this.pot) {
                                    this.minHu = this.pot;
                                }
                                if (forceNoHu) {
                                    ++this.numNoHuForce;
                                }
                                ++this.numNoHu;
                                this.pot = this.initPotValue;
                                this.fund -= this.initPotValue;
                            }
                            else {
                                this.fund -= totalPrizes;
                            }
                            if (totalPrizes >= this.betValue * 1000L) {
                                result = 2;
                                ++this.numThangLon;
                            }
                            else {
                                result = 1;
                            }
                        }
                        this.tongTraThuong += totalPrizes;
                    }
                }
                else {
                    result = 102;
                }
            }
            else {
                result = 101;
            }
        }
        else {
            result = 101;
        }
        if (result == 0) {
            ++this.numTruot;
        }
    }
    
    public static void main(final String[] args) {
        final String username = "tuyennd";
        final String lines = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20";
        final long soLan = 1000000L;
        final TestPokeGo test = new TestPokeGo();
        for (int i = 0; i < soLan; ++i) {
            test.play(username, lines);
        }
        final long tongCuoc = test.betValue * 20L * soLan;
        System.out.println("TONG CUOC= " + tongCuoc);
        System.out.println("TONG TRA THUONG= " + test.tongTraThuong);
        System.out.println("% TRA THUONG= " + test.tongTraThuong / (float)tongCuoc * 100.0f);
        System.out.println("FUND= " + test.fund);
        System.out.println("POT= " + test.pot);
        System.out.println("======================================");
        System.out.println("SO LAN NO HU= " + test.numNoHu);
        System.out.println("SO LAN FORCE NO HU= " + test.numNoHuForce);
        System.out.println("MIN HU= " + test.minHu + ", MAX HU= " + test.maxHu);
        System.out.println("SO LAN THANG LON= " + test.numThangLon);
        System.out.println("SO LAN TRUOT= " + test.numTruot);
        System.out.println("% QUAY TRUOT= " + test.numTruot / (float)soLan * 100.0f);
        System.out.println("SO LAN KHONG DU TRA THUONG= " + test.numKhongDuTraThuong);
        System.out.println("% KHONG DU TRA THUONG= " + test.numKhongDuTraThuong / (float)soLan * 100.0f);
        System.out.println("======================================");
        System.out.println("TRIPLE POKEBALl= " + test.triplePokeBall);
        System.out.println("TRIPLE PIKACHU= " + test.triplePikaChu);
        System.out.println("TRIPLE BUBASAUR= " + test.tripleBubasaur);
        System.out.println("TRIPLE CLEFABLE= " + test.tripleClefable);
        System.out.println("TRIPLE MOUSE= " + test.tripleMouse);
        System.out.println("DOUBLE MOUSE= " + test.doubleMouse);
        System.out.println("TRIPLE TOGEPI= " + test.tripleToGePi);
        System.out.println("DOUBLE TOGEPI= " + test.doubleToGePi);
    }
}
