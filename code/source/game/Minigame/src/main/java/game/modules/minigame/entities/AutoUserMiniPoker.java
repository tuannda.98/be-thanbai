// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.entities;

import bitzero.server.entities.User;

public class AutoUserMiniPoker
{
    private User user;
    private int count;
    
    public AutoUserMiniPoker(final User user, final int count) {
        this.user = user;
        this.count = count;
    }
    
    public void setUser(final User user) {
        this.user = user;
    }
    
    public User getUser() {
        return this.user;
    }
    
    public void setCount(final int count) {
        this.count = count;
    }
    
    public int getCount() {
        return this.count;
    }
    
    public boolean incCount() {
        ++this.count;
        if (this.count == 6) {
            this.count = 0;
            return true;
        }
        return false;
    }
}
