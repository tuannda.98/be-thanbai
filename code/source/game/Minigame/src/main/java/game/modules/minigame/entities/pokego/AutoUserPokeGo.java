// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.entities.pokego;

import bitzero.server.entities.User;

public class AutoUserPokeGo
{
    private User user;
    private int count;
    private String lines;
    private int maxCount;
    
    public AutoUserPokeGo(final User user, final String lines) {
        this.maxCount = 6;
        this.user = user;
        this.count = 0;
        this.lines = lines;
    }
    
    public void setUser(final User user) {
        this.user = user;
    }
    
    public User getUser() {
        return this.user;
    }
    
    public void setCount(final int count) {
        this.count = count;
    }
    
    public int getCount() {
        return this.count;
    }
    
    public void setLines(final String lines) {
        this.lines = lines;
    }
    
    public String getLines() {
        return this.lines;
    }
    
    public boolean incCount() {
        ++this.count;
        if (this.count == this.maxCount) {
            this.count = 0;
            return true;
        }
        return false;
    }
    
    public void setMaxCount(final int maxCount) {
        this.maxCount = maxCount;
    }
}
