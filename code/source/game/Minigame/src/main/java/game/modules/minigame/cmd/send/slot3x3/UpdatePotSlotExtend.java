// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.send.slot3x3;

import java.nio.ByteBuffer;
import bitzero.server.extensions.data.BaseMsg;

public class UpdatePotSlotExtend extends BaseMsg
{
    public long value1;
    public long value2;
    public long value3;
    
    public UpdatePotSlotExtend() {
        super((short)8006);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        bf.putLong(this.value1);
        bf.putLong(this.value2);
        bf.putLong(this.value3);
        return this.packBuffer(bf);
    }
}
