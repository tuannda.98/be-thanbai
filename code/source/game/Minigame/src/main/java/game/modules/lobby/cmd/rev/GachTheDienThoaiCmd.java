// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.lobby.cmd.rev;

import java.nio.ByteBuffer;
import bitzero.server.extensions.data.DataCmd;
import bitzero.server.extensions.data.BaseCmd;

public class GachTheDienThoaiCmd extends BaseCmd
{
    public byte provider;
    public String serial;
    public String pin;
    public String menhgia;
    
    public GachTheDienThoaiCmd(final DataCmd dataCmd) {
        super(dataCmd);
        this.unpackData();
    }
    
    public void unpackData() {
        final ByteBuffer bf = this.makeBuffer();
        this.provider = bf.get();
        this.serial = this.readString(bf);
        this.pin = this.readString(bf);
        this.menhgia = this.readString(bf);
    }
}
