// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class UpdateUserInfoMsg extends BaseMsgEx
{
    public long currentMoney;
    public short moneyType;
    
    public UpdateUserInfoMsg() {
        super(2003);
    }
    
    public byte[] createData() {
        final ByteBuffer buffer = this.makeBuffer();
        buffer.putLong(this.currentMoney);
        buffer.putShort(this.moneyType);
        return this.packBuffer(buffer);
    }
}
