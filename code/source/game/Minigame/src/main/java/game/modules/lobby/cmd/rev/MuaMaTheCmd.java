// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.lobby.cmd.rev;

import java.nio.ByteBuffer;
import bitzero.server.extensions.data.DataCmd;
import bitzero.server.extensions.data.BaseCmd;

public class MuaMaTheCmd extends BaseCmd
{
    public byte provider;
    public byte amount;
    public byte quantity;
    
    public MuaMaTheCmd(final DataCmd dataCmd) {
        super(dataCmd);
        this.unpackData();
    }
    
    public void unpackData() {
        final ByteBuffer bf = this.makeBuffer();
        this.provider = bf.get();
        this.amount = bf.get();
        this.quantity = bf.get();
    }
}
