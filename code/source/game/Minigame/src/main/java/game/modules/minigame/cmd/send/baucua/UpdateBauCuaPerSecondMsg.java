// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.send.baucua;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class UpdateBauCuaPerSecondMsg extends BaseMsgEx
{
    public String potData;
    public byte remainTime;
    public boolean bettingState;
    
    public UpdateBauCuaPerSecondMsg() {
        super(5006);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        this.putStr(bf, this.potData);
        bf.put(this.remainTime);
        this.putBoolean(bf, this.bettingState);
        return this.packBuffer(bf);
    }
}
