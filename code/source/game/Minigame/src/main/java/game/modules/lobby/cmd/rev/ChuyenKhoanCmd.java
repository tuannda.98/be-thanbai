// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.lobby.cmd.rev;

import java.nio.ByteBuffer;
import bitzero.server.extensions.data.DataCmd;
import bitzero.server.extensions.data.BaseCmd;

public class ChuyenKhoanCmd extends BaseCmd
{
    public String receiver;
    public long moneyExchange;
    public String description;
    public String otp;
    
    public ChuyenKhoanCmd(final DataCmd dataCmd) {
        super(dataCmd);
        this.unpackData();
    }
    
    public void unpackData() {
        final ByteBuffer bf = this.makeBuffer();
        this.receiver = this.readString(bf);
        this.moneyExchange = bf.getLong();
        this.description = this.readString(bf);
        this.otp = this.readString(bf);
    }

    @Override
    public String toString() {
        return "ChuyenKhoanCmd{" +
                "receiver='" + receiver + '\'' +
                ", moneyExchange=" + moneyExchange +
                ", description='" + description + '\'' +
                ", otp='" + otp + '\'' +
                '}';
    }
}
