// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.player.cmd.rev;

import java.nio.ByteBuffer;
import bitzero.server.extensions.data.DataCmd;
import bitzero.server.extensions.data.BaseCmd;

public class LoginCmd extends BaseCmd
{
    public String nickname;
    public String sessionKey;
    
    public LoginCmd(final DataCmd dataCmd) {
        super(dataCmd);
        this.unpackData();
    }
    
    public void unpackData() {
        final ByteBuffer bf = this.makeBuffer();
        this.nickname = this.readString(bf);
        this.sessionKey = this.readString(bf);
    }
}
