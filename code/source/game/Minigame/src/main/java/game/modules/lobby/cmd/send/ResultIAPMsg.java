// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.lobby.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class ResultIAPMsg extends BaseMsgEx
{
    public byte productId;
    public long currentMoney;
    
    public ResultIAPMsg() {
        super(20038);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        bf.put(this.productId);
        bf.putLong(this.currentMoney);
        return this.packBuffer(bf);
    }
}
