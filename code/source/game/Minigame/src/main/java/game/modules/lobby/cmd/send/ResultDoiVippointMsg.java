// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.lobby.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class ResultDoiVippointMsg extends BaseMsgEx
{
    public long currentMoney;
    public long moneyAdd;
    
    public ResultDoiVippointMsg() {
        super(20021);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        bf.putLong(this.currentMoney);
        bf.putLong(this.moneyAdd);
        return this.packBuffer(bf);
    }
}
