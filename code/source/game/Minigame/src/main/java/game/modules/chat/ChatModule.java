package game.modules.chat;

import bitzero.server.core.BZEventParam;
import bitzero.server.core.BZEventType;
import bitzero.server.core.IBZEvent;
import bitzero.server.entities.User;
import bitzero.server.exceptions.BZException;
import bitzero.server.extensions.BaseClientRequestHandler;
import bitzero.server.extensions.data.BaseMsg;
import bitzero.server.extensions.data.DataCmd;
import casio.king365.dto.UpdateUserMoneyMsg;
import casio.king365.service.MongoDbService;
import casio.king365.service.impl.MongoDbServiceImpl;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.vinplay.dal.service.CacheService;
import com.vinplay.dal.service.ChatLobbyService;
import com.vinplay.dal.service.impl.CacheServiceImpl;
import com.vinplay.dal.service.impl.ChatLobbyServiceImpl;
import com.vinplay.usercore.dao.impl.UserDaoImpl;
import com.vinplay.usercore.service.UserService;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.exceptions.KeyNotFoundException;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.utils.DateTimeUtils;
import game.modules.chat.cmd.rev.ChatCmd;
import game.modules.chat.cmd.send.ChatInfoMsg;
import game.modules.chat.cmd.send.ChatMsg;
import game.modules.chat.entities.ChatEntry;
import game.utils.ConfigGame;
import game.utils.ServerUtil;
import org.apache.log4j.Logger;
import org.bson.Document;
import org.json.simple.JSONArray;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class ChatModule extends BaseClientRequestHandler {
    private static final int MAX_LOG_CHAT = 10;
    private ChatLobbyService chatService;
    private final List<ChatEntry> entries;
    private final Map<String, User> userByNicknames;
    private final UserService userService;
    private final List<String> listChat;
    private final Logger logger;
    CacheService cacheService = new CacheServiceImpl();
    MongoDbService mongoDbService = new MongoDbServiceImpl();

    public ChatModule() {
        this.chatService = new ChatLobbyServiceImpl();
        this.entries = new ArrayList<>();
        this.userByNicknames = new ConcurrentHashMap<>();
        this.userService = new UserServiceImpl();
        this.listChat = new ArrayList<>();
        this.logger = Logger.getLogger("BlockChat");
    }

    public void init() {
        super.init();
        this.loadData();
        this.getParentExtension().addEventListener(BZEventType.USER_DISCONNECT, this);
    }

    public void setChatService(ChatLobbyService chatService) {
        this.chatService = chatService;
    }

    public void loadData() {
        try (BufferedReader br2 = new BufferedReader(new InputStreamReader(new FileInputStream("config/list_chat.txt"), StandardCharsets.UTF_8))) {
            String entry;
            while ((entry = br2.readLine()) != null) {
                this.listChat.add(entry);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void handleServerEvent(final IBZEvent ibzevent) throws BZException {
        if (ibzevent.getType() == BZEventType.USER_DISCONNECT) {
            final User user = (User) ibzevent.getParameter(BZEventParam.USER);
            this.userDis(user);
        }
    }

    private void userDis(final User user) {
        this.unsubscribe(user);
    }

    public void handleClientRequest(final User user, final DataCmd dataCmd) {
        switch (dataCmd.getId()) {
            case 18001: {
                this.subscribe(user);
                break;
            }
            case 18002: {
                this.unsubscribe(user);
                break;
            }
            case 18000: {
                this.chat(user, dataCmd);
                break;
            }
        }
    }

    private void subscribe(final User user) {
        userByNicknames.putIfAbsent(user.getName(), user);
        this.sendChatLobbyInfo(user);
    }

    private void unsubscribe(final User user) {
        userByNicknames.remove(user.getName());
    }

    private void chat(final User user, final DataCmd dataCmd) {
        final int daiLy = this.getStatusDaiLy(user);
        final ChatCmd cmd = new ChatCmd(dataCmd);
        String username = user.getName();
        // Kiểm tra xem có cho phép tài khoản ko nạp tiền có dc chat hay ko
        try {
            if (cacheService.checkKeyExist("allow_chat_without_verify_telegram")) {
                String allowChatWithoutVerifyTele = cacheService.getValueStr("allow_chat_without_verify_telegram");
                if (allowChatWithoutVerifyTele.equals("false")) {
                    Map<String, Object> findMap = new HashMap<>();
                    findMap.put("nickName", username);
                    List<Document> result = mongoDbService.find("AccountTeleLink", findMap);
                    if (result.size() == 0) {
                        UserCacheModel u = userService.getCacheUserIgnoreCase(username);
                        UpdateUserMoneyMsg msgg = new UpdateUserMoneyMsg();
                        msgg.userNickname = u.getNickname();
                        msgg.totalMoney = u.getVinTotal();
                        msgg.changeMoney = 0;
                        msgg.desc = "Vui lòng Kích Hoạt Bảo Mật OTP Telegram để sử dụng tính năng Chat";
                        msgg.showPopup = true;
                        send(msgg, user);
                        return;
                    }
                }
            }
        } catch (KeyNotFoundException e) {
            e.printStackTrace();
        }

        if (this.allowUserChat(user.getName(), daiLy) && !this.containBadword(username, cmd.message)) {
            if (daiLy == 100) {
                username = "Admin";
            }
            this.chat(username, cmd.message);
        } else {
            final ChatMsg msg = new ChatMsg();
            msg.Error = 2;
            this.send(msg, user);
        }
    }

    private void chat(final String username, final String content) {
        ChatEntry newEntry = new ChatEntry(username, content);
        final ChatMsg msg = new ChatMsg();
        final HazelcastInstance client = HazelcastClientFactory.getInstance();
        final IMap<String, UserModel> userMap = client.getMap("users");
        UserModel model = null;
        String displayName = username;
        if (userMap.containsKey(username)) {
            model = userMap.get(displayName);
            /*if (model.getClient() != null && !Objects.equals(model.getClient(), "")) {
                displayName = "[" + model.getClient() + "] " + username;
            } else {
                displayName = "[X] " + username;
            }*/
        } else {
            final UserDaoImpl dao = new UserDaoImpl();
            try {
                model = dao.getUserByNickName(username);
                /*if (model.getClient() != null && !Objects.equals(model.getClient(), "")) {
                    displayName = "[" + model.getClient() + "] " + username;
                } else {
                    displayName = "[X] " + username;
                }*/
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        msg.nickname = displayName;
        msg.mesasge = content;
        this.userByNicknames.forEach((s, user) -> {
            ServerUtil.sendMsgToUser(msg, user);
        });
        newEntry = new ChatEntry(displayName, content);
        this.logChat(newEntry);
    }

    private void logChat(final ChatEntry newEntry) {
        synchronized (this.entries) {
            this.entries.add(newEntry);
            while (this.entries.size() > 10) {
                this.entries.remove(0);
            }
        }
    }

    private boolean containBadword(final String username, final String content) {
        if (ConfigGame.checkBadword(content)) {
            this.logger.debug(DateTimeUtils.getCurrentTime() + "\t" + username + " \t " + content);
            this.chatService.banChatUser(username, ConfigGame.TIME_BLOCK_CHAT * 60 * 1000);
            return true;
        }
        return false;
    }

    private void sendChatLobbyInfo(final User user) {
        final JSONArray arr = this.entries.stream().map(ChatEntry::toJson).collect(Collectors.toCollection(JSONArray::new));
        final String str = arr.toString();
        final ChatInfoMsg chatInfoMsg = new ChatInfoMsg();
        chatInfoMsg.msg = str;
        chatInfoMsg.minVipPointRequire = (byte) ConfigGame.getIntValue("chat_min_vp_require", 20);
        chatInfoMsg.timeUnBan = this.chatService.getBanTime(user.getName());
        chatInfoMsg.userType = (byte) this.getStatusDaiLy(user);
        this.sendMessageToUser(chatInfoMsg, user);
    }

    public void sendMessageToUser(BaseMsg msg, User recipient) {
        this.send(msg, recipient);
    }

    private boolean allowUserChat(final String username, final int daiLy) {
        if (daiLy == 100) {
            return true;
        }
        boolean allow = true;
        final int minVPRequire = ConfigGame.getIntValue("chat_min_vp_require", 20);
        final int vipPointSave = this.userService.getVipPointSave(username);
        if (vipPointSave < minVPRequire) {
            allow = false;
        }
        if (0 < daiLy && daiLy < 100) {
            allow = false;
        }
        final long timeUnBan;
        if ((timeUnBan = this.chatService.getBanTime(username)) != 0L) {
            allow = false;
        }
        return allow;
    }

    private int getStatusDaiLy(final User user) {
        final String status = (String) user.getProperty("dai_ly");
        int daiLy = 0;
        if (status != null && !status.isEmpty()) {
            daiLy = Integer.parseInt(status);
        }
        return daiLy;
    }
}
