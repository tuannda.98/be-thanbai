// 
// Decompiled by Procyon v0.5.36
// 

package game.eventHandlers;

import bitzero.server.core.IBZEventType;

public enum GameEventType implements IBZEventType
{
    GAME_ROOM_USER_JOIN, 
    GAME_ROOM_USER_LEAVE, 
    EVENT_ADD_SCORE;
}
