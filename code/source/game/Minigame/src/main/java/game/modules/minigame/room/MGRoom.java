package game.modules.minigame.room;

import bitzero.server.entities.User;
import bitzero.server.extensions.data.BaseMsg;
import bitzero.util.ExtensionUtility;
import com.vinplay.vbee.common.enums.Games;

import java.util.ArrayList;
import java.util.List;

public abstract class MGRoom {
    public static final String MGROOM_TAI_XIU_INFO = "MGROOM_TAI_XIU_INFO";
    public static final String MGROOM_MINI_POKER_INFO = "MGROOM_MINI_POKER_INFO";
    public static final String MGROOM_BAU_CUA_INFO = "MGROOM_BAU_CUA_INFO";
    public static final String MGROOM_CAO_THAP_INFO = "MGROOM_CAO_THAP_INFO";
    public static final String MGROOM_POKEGO_INFO = "MGROOM_POKEGO_INFO";
    protected final String name;
    protected Games gameName;
    protected final List<User> users;

    public MGRoom(final String name, Games gameName) {
        this.users = new ArrayList<>();
        this.name = name;
        this.gameName = gameName;
    }

    public boolean joinRoom(final User user) {
        synchronized (this.users) {
            if (!this.users.contains(user)) {
                this.users.add(user);
                return true;
            }
        }
        return false;
    }

    public boolean quitRoom(final User user) {
        synchronized (this.users) {
            if (this.users.contains(user)) {
                this.users.remove(user);
                return true;
            }
        }
        return false;
    }

    public void sendMessageToRoom(final BaseMsg msg) {
        final ArrayList<User> usersCopy = new ArrayList<>(this.users);
        for (final User user : usersCopy) {
            if (user == null) {
                continue;
            }
            ExtensionUtility.getExtension().send(msg, user);
        }
    }

    public void sendMessageToUser(final BaseMsg msg, final String username) {
        final User user = ExtensionUtility.getExtension().getApi().getUserByName(username);
        if (user != null) {
            ExtensionUtility.getExtension().send(msg, user);
        }
    }

    public void sendMessageToUser(final BaseMsg msg, final User user) {
        if (user != null) {
            ExtensionUtility.getExtension().send(msg, user);
        }
    }
}
