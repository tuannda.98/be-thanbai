// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.chat.entities;

import org.json.simple.JSONObject;

public class ChatEntry
{
    private String nickname;
    private String message;
    
    public ChatEntry() {
    }
    
    public ChatEntry(final String nickname, final String message) {
        this.nickname = nickname;
        this.message = message;
    }
    
    public String getNickname() {
        return this.nickname;
    }
    
    public String getMessage() {
        return this.message;
    }
    
    public JSONObject toJson() {
        final JSONObject result = new JSONObject();
        result.put("u", this.nickname);
        result.put("m", this.message);
        return result;
    }
}
