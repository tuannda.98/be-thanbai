// 
// Decompiled by Procyon v0.5.36
// 

package game.utils;

import bitzero.util.common.business.Debug;
import com.vinplay.usercore.utils.GameCommon;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Calendar;

public class HuVangConfig {
    private JSONObject config;
    private static HuVangConfig huVangConfig;

    private HuVangConfig() {
        this.config = null;
        this.initconfig();
    }

    public static HuVangConfig instance() {
        if (HuVangConfig.huVangConfig == null) {
            HuVangConfig.huVangConfig = new HuVangConfig();
        }
        return HuVangConfig.huVangConfig;
    }

    public void initconfig() {
        try {
            final String configHu = GameCommon.getHuVangGameBai();
            Debug.trace(configHu);
            this.config = new JSONObject(configHu);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initconfigFromFile() {
        final String path = System.getProperty("user.dir");
        final File file = new File(path + "/conf/huvang.json");
        final StringBuilder contents = new StringBuilder();
        try {
            try (InputStreamReader r = new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8)) {
                BufferedReader reader = new BufferedReader(r);
                String text = null;
                while ((text = reader.readLine()) != null) {
                    contents.append(text).append(System.getProperty("line.separator"));
                }
            }
            this.config = new JSONObject(contents.toString());
        } catch (Exception e) {
            this.initconfig();
            e.printStackTrace();
        }
    }

    public JSONObject timHuVangDangChay() {
        try {
            final JSONArray array = this.config.getJSONArray("huvang");
            JSONObject game1 = null;
            JSONObject game2 = null;
            for (int i = 0; i < array.length(); ++i) {
                final JSONObject game3 = array.getJSONObject(i);
                final int remain = this.kiemTraHuVangTheoThoiGian(game3);
                if (remain < 0) {
                    game1 = game3;
                    break;
                }
                if (remain != 0) {
                    game2 = game3;
                }
            }
            if (game1 != null) {
                return game1;
            }
            return game2;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public int getThoiGianHuVang(final String gameName) {
        final JSONObject gameConfig = this.getGameConfig(gameName);
        if (gameConfig != null) {
            return this.kiemTraHuVangTheoThoiGian(gameConfig);
        }
        return 0;
    }

    public int kiemTraHuVangTheoThoiGian(final JSONObject gameConfig) {
        try {
            if (gameConfig == null) {
                return 0;
            }
            final Calendar today = Calendar.getInstance();
            final int dayInWeek = today.get(Calendar.DAY_OF_WEEK);
            final int hour = today.get(Calendar.HOUR_OF_DAY);
            final int minute = today.get(Calendar.MINUTE);
            final int second = today.get(Calendar.SECOND);
            int flag = -1;
            final JSONArray arr = gameConfig.getJSONArray("dayInWeek");
            for (int i = 0; i < arr.length(); ++i) {
                final int day = arr.getInt(i);
                if (day == dayInWeek) {
                    flag = 0;
                    break;
                }
            }
            if (flag == -1) {
                return 0;
            }
            final int startHour = gameConfig.getJSONObject("startTime").getInt("h");
            final int startMinute;
            final int remain1 = this.subTimeGetMinute(hour, minute, startHour, startMinute = gameConfig.getJSONObject("startTime").getInt("m"));
            if (remain1 > 0) {
                return remain1 * 60 - second;
            }
            final int endHour = gameConfig.getJSONObject("endTime").getInt("h");
            final int endMinute;
            final int remain2 = this.subTimeGetMinute(hour, minute, endHour, endMinute = gameConfig.getJSONObject("endTime").getInt("m"));
            if (remain2 > 0) {
                return -remain2 * 60 - second;
            }
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int subTimeGetMinute(final int h1, final int m1, final int h2, final int m2) {
        final int minute1 = h1 * 60 + m1;
        final int minute2 = h2 * 60 + m2;
        return minute2 - minute1;
    }

    public JSONObject getGameConfig(final String gameName) {
        try {
            final JSONArray array = this.config.getJSONArray("huvang");
            for (int i = 0; i < array.length(); ++i) {
                final JSONObject game = array.getJSONObject(i);
                final String name = game.getString("gameName");
                if (gameName.equalsIgnoreCase(name)) {
                    return game;
                }
            }
            return null;
        } catch (JSONException e) {
            return null;
        }
    }

    public double getRate(final String game, final long moneyBet) {
        try {
            final JSONObject rate = this.getGameConfig(game).getJSONObject("rate");
            return rate.getInt(String.valueOf(moneyBet)) / 1000.0;
        } catch (Exception e) {
            e.printStackTrace();
            return 0.0;
        }
    }

    static {
        HuVangConfig.huVangConfig = null;
    }
}
