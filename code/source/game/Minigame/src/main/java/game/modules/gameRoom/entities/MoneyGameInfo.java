// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.gameRoom.entities;

import bitzero.util.common.business.Debug;
import bitzero.server.entities.User;

public class MoneyGameInfo
{
    public final int userId;
    public final int moneyType;
    public long currentMoney;
    public long winMoney;
    public final long requireMoney;
    public long preChargedMoney;
    
    public MoneyGameInfo(final User user, final GameRoomSetting setting) {
        this.userId = user.getId();
        this.moneyType = setting.moneyType;
        this.requireMoney = setting.requiredMoney;
        this.startGameUpdateMoney();
    }
    
    public boolean startGameUpdateMoney() {
        final boolean result = this.freezeMoney();
        if (result) {
            this.winMoney = 0L;
            return true;
        }
        return false;
    }
    
    public boolean moneyCheck() {
        this.preChargedMoney += this.winMoney;
        this.winMoney = 0L;
        if (this.preChargedMoney >= this.requireMoney) {
            return true;
        }
        final boolean result = this.freezeMoney();
        if (result) {
            return true;
        }
        this.restoreMoney();
        return false;
    }
    
    public boolean freezeMoney() {
        Debug.trace("Dong bang tai khoan khach hang");
        this.preChargedMoney = this.requireMoney;
        return true;
    }
    
    public void restoreMoney() {
        Debug.trace("Hoan tien cho tai khoan khach hang");
        this.preChargedMoney = 0L;
    }
    
    public boolean outGameUpdate() {
        this.restoreMoney();
        return true;
    }
}
