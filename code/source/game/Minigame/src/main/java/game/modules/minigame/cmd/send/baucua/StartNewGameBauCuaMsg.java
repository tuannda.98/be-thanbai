// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.send.baucua;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class StartNewGameBauCuaMsg extends BaseMsgEx
{
    public long referenceId;
    
    public StartNewGameBauCuaMsg() {
        super(5007);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        this.putLong(bf, this.referenceId);
        return this.packBuffer(bf);
    }
}
