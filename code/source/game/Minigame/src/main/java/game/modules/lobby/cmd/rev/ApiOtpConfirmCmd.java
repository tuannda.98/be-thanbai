// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.lobby.cmd.rev;

import java.nio.ByteBuffer;
import bitzero.server.extensions.data.DataCmd;
import bitzero.server.extensions.data.BaseCmd;

public class ApiOtpConfirmCmd extends BaseCmd
{
    public String requestId;
    public String otp;
    
    public ApiOtpConfirmCmd(final DataCmd dataCmd) {
        super(dataCmd);
        this.unpackData();
    }
    
    public void unpackData() {
        final ByteBuffer bf = this.makeBuffer();
        this.requestId = this.readString(bf);
        this.otp = this.readString(bf);
    }
}
