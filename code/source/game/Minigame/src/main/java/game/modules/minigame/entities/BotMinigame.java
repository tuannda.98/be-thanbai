// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.entities;

import bitzero.util.common.business.Debug;
import com.vinplay.dal.service.BotService;
import com.vinplay.dal.service.impl.BotServiceImpl;
import com.vinplay.usercore.service.UserService;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.utils.DateTimeUtils;
import game.utils.ConfigGame;

import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.*;

public class BotMinigame
{
    private static List<String> bots;
    private static List<String> botsVipDaily;
    private static List<String> botsVip;
    private static UserService userService;
    private static BotService botService;
    private static List<Integer> betValueDefault;
    private static long[] soVinBan;
    private static long updateTime;
    
    public static void loadData() {
        try {
            final BufferedReader br22 = new BufferedReader(new FileReader("config/bots.txt"));
            final BotServiceImpl service = new BotServiceImpl();
            String botName;
            while ((botName = br22.readLine()) != null) {
                try {
                    service.login(botName);
                    BotMinigame.bots.add(botName);
                }
//                catch (NoSuchAlgorithmException | SQLException ex3) {
//                    final Exception ex;
//                    final Exception e = ex;
//                    Debug.trace((Object[])new Object[] { "Load bot " + botName + " error: ", e });
//                }
                catch (Exception ex3) {
                    Debug.trace("Load bot " + botName + " error: ", ex3);
                }

            }
            br22.close();
        } catch (IOException ex5) {
            ex5.printStackTrace();
        }
        try {
            final BufferedReader br22 = new BufferedReader(new FileReader("config/bots_vip.txt"));
            final BotServiceImpl service = new BotServiceImpl();
            String botName;
            while ((botName = br22.readLine()) != null) {
                try {
                    service.login(botName);
                    BotMinigame.botsVip.add(botName);
                }
                catch (NoSuchAlgorithmException | SQLException ex6) {
                    Debug.trace("Load vip bot " + botName + " error: ", ex6);
                }
            }
            br22.close();
        } catch (IOException ex8) {
            ex8.printStackTrace();
        }
        loadBotsVip();
        Debug.trace("TOTAL BOTS: " + BotMinigame.bots.size());
    }
    
    public static void loadBotsVip() {
        BotMinigame.botsVipDaily.clear();
        final int maxBotVip = ConfigGame.getIntValue("tx_vip_max_vin");
        final int numBotVip = maxBotVip + 10;
        final Random rd = new Random();
        if (numBotVip >= BotMinigame.botsVip.size()) {
            Debug.trace("Khong the tao bot vip hang ngay");
            return;
        }
        int i = 0;
        while (i < numBotVip) {
            final int n = rd.nextInt(BotMinigame.botsVip.size());
            final String bot = BotMinigame.botsVip.get(n);
            if (bot == null) {
                continue;
            }
            boolean exist = false;
            for (final String str : BotMinigame.botsVipDaily) {
                if (!str.equals(bot)) {
                    continue;
                }
                exist = true;
                break;
            }
            if (exist) {
                continue;
            }
            BotMinigame.botsVipDaily.add(bot);
            ++i;
        }
    }
    
    public static String getRandomBot(final String moneyType) {
        final Random rd = new Random();
        final int index = rd.nextInt(BotMinigame.bots.size());
        final String nickname = BotMinigame.bots.get(index);
        pushMoneyToBot(nickname, moneyType);
        return nickname;
    }
    
    private static void pushMoneyToBot(final String nickname, final String moneyType) {
        final long currentMoney = BotMinigame.userService.getCurrentMoneyUserCache(nickname, moneyType);
        if (currentMoney < 1000000L) {
            BotMinigame.botService.addMoney(nickname, 10000000L, moneyType, "Chuyen tien cho bot minigame");
        }
        else {
            banVin(nickname, moneyType, currentMoney);
        }
    }
    
    private static void banVin(final String nickname, final String moneyType, final long currentMoney) {
        if (currentMoney >= 60000000L) {
            final Random rd = new Random();
            final int index = rd.nextInt(BotMinigame.soVinBan.length);
            final long tienBan = BotMinigame.soVinBan[index];
            BotMinigame.botService.addMoney(nickname, -tienBan, moneyType, "Chuyen tien");
        }
    }
    
    private static void pushMoneyToBotVip(final String nickname, final String moneyType, final long moneyPushed) {
        final long currentMoney = BotMinigame.userService.getCurrentMoneyUserCache(nickname, moneyType);
        if (currentMoney < moneyPushed) {
            BotMinigame.botService.addMoney(nickname, moneyPushed, moneyType, "Cong tien cho bot minigame");
        }
        else {
            banVin(nickname, moneyType, currentMoney);
        }
    }
    
    public static List<String> getBots(final int amount, final String moneyType) {
        final ArrayList<String> results = new ArrayList<>();
        final ArrayList<String> copyBots = new ArrayList<>(BotMinigame.bots);
        for (int i = 0; i < amount; ++i) {
            final Random rd = new Random();
            final int index = rd.nextInt(copyBots.size());
            final String nickname = copyBots.get(index);
            pushMoneyToBot(nickname, moneyType);
            results.add(copyBots.remove(index));
        }
        return results;
    }
    
    public static List<String> getBotsVip(final int amount, final String moneyType) {
        final ArrayList<String> results = new ArrayList<>();
        final ArrayList<String> copyBots = new ArrayList<>(BotMinigame.botsVipDaily);
        for (int i = 0; i < amount; ++i) {
            final Random rd = new Random();
            final int index = rd.nextInt(copyBots.size());
            final String nickname = copyBots.get(index);
            pushMoneyToBotVip(nickname, moneyType, 10000000L);
            results.add(copyBots.remove(index));
        }
        return results;
    }
    
    public static List<String> getBotsSuperVip(final int amount, final String moneyType, final long moneyPushed) {
        final ArrayList<String> results = new ArrayList<>();
        final ArrayList<String> copyBots = new ArrayList<>(BotMinigame.botsVipDaily);
        for (int i = 0; i < amount; ++i) {
            final Random rd = new Random();
            final int index = rd.nextInt(copyBots.size());
            final String nickname = copyBots.get(index);
            pushMoneyToBotVip(nickname, moneyType, moneyPushed);
            results.add(copyBots.remove(index));
        }
        return results;
    }
    
    public static List<BotTaiXiu> getVipBotTaiXiu() {
        if (BotMinigame.updateTime < DateTimeUtils.getStartTimeToDayAsLong()) {
            loadBotsVip();
        }
        BotMinigame.updateTime = System.currentTimeMillis();
        final ArrayList<BotTaiXiu> results = new ArrayList<>();
        final ArrayList<Integer> betValues = new ArrayList<>();
        final Random rd = new Random();
        int numBetTai = 0;
        int minBetValue = 0;
        int maxBetValue = 0;
        int minBettingTime = 0;
        int maxBettingTime = 0;
        final int minBotsVin = ConfigGame.getIntValue("tx_vip_min_vin");
        final int maxBotsVin = ConfigGame.getIntValue("tx_vip_max_vin");
        if (maxBotsVin == 0) {
            return new ArrayList<>();
        }
        int totalBot = rd.nextInt(maxBotsVin - minBotsVin) + minBotsVin;
        if (isNight()) {
            final int n = rd.nextInt(2) + 3;
            totalBot /= n;
        }
        final int minBot = totalBot * 4 / 10;
        final int maxBot = totalBot - minBot;
        numBetTai = rd.nextInt(maxBot - minBot) + minBot;
        minBetValue = ConfigGame.getIntValue("tx_vip_min_value_vin");
        maxBetValue = ConfigGame.getIntValue("tx_vip_max_value_vin");
        for (int stepBetting = ConfigGame.getIntValue("tx_vip_step_betting_vin"), betValue = minBetValue; betValue < maxBetValue; betValue += stepBetting) {
            betValues.add(betValue);
        }
        try {
            minBettingTime = ConfigGame.getIntValue("tx_vip_min_betting_time");
            maxBettingTime = ConfigGame.getIntValue("tx_vip_max_betting_time");
            final List<String> botsName = getBotsVip(totalBot, "vin");
            for (int i = 0; i < totalBot && i < botsName.size(); ++i) {
                final String nickname = botsName.get(i);
                final int n2 = rd.nextInt(betValues.size());
                final int betValue = betValues.get(n2);
                final short bettingTime = (short)randomBettingTime(minBettingTime, maxBettingTime, 80);
                short betSide = 0;
                if (i < numBetTai) {
                    betSide = 1;
                }
                final BotTaiXiu bot = new BotTaiXiu(nickname, bettingTime, betValue, betSide);
                results.add(bot);
            }
        }
        catch (Exception ex) {
            Debug.trace("Exception:" + ex.getMessage());
            final StringWriter sw = new StringWriter();
            final PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw);
            final String sStackTrace = sw.toString();
            Debug.trace(sStackTrace);
        }
        return results;
    }
    
    public static List<BotTaiXiu> getBotTaiXiu(final String moneyType) {
        final Random rd = new Random();
        int phanTramVaoSom = 80;
        final int[] arr = { 60, 70, 80, 85, 90 };
        final int index = rd.nextInt(arr.length);
        phanTramVaoSom = arr[index];
        final ArrayList<BotTaiXiu> results = new ArrayList<>();
        final ArrayList<Integer> betValues = new ArrayList<>(BotMinigame.betValueDefault);
        int numBetTai = 0;
        int numBetXiu = 0;
        int minBetValue = 0;
        int maxBetValue = 0;
        int minBettingTime = 0;
        int maxBettingTime = 0;
        if (moneyType.equalsIgnoreCase("vin")) {
            final int minBotsVin = ConfigGame.getIntValue("tx_min_bot_betting_vin");
            final int maxBotsVin = ConfigGame.getIntValue("tx_max_bot_betting_vin");
            if (maxBotsVin == 0 || minBotsVin == 0) {
                return new ArrayList<>();
            }
            int totalBot = rd.nextInt(maxBotsVin - minBotsVin) + minBotsVin;
            final int n = ratioTXInNight();
            Debug.trace("Bot n = " + n);
            totalBot = totalBot * n / 100;
            final int minBot = totalBot * 4 / 10;
            final int maxBot = totalBot - minBot;
            numBetTai = rd.nextInt(maxBot - minBot) + minBot;
            numBetXiu = totalBot - numBetTai;
            Debug.trace("NUM BET TAI= " + numBetTai + ", NUM BET XIU= " + numBetXiu);
            minBetValue = ConfigGame.getIntValue("tx_min_bet_value_vin");
            maxBetValue = ConfigGame.getIntValue("tx_max_bet_value_vin");
            for (int stepBetting = ConfigGame.getIntValue("tx_step_betting_vin"), betValue = minBetValue; betValue < maxBetValue; betValue += stepBetting) {
                betValues.add(betValue);
            }
        }
        else {
            final int minBotsXu = ConfigGame.getIntValue("tx_min_bot_betting_xu");
            final int maxBotsXu = ConfigGame.getIntValue("tx_max_bot_betting_xu");
            if (maxBotsXu == 0) {
                return new ArrayList<>();
            }
            final int totalBots = rd.nextInt(maxBotsXu - minBotsXu) + minBotsXu;
            numBetTai = rd.nextInt(totalBots);
            numBetXiu = totalBots - numBetTai;
            minBetValue = ConfigGame.getIntValue("tx_min_bet_value_xu");
            maxBetValue = ConfigGame.getIntValue("tx_max_bet_value_xu");
            for (int stepBetting2 = ConfigGame.getIntValue("tx_step_betting_xu"), betValue2 = minBetValue; betValue2 < maxBetValue; betValue2 += stepBetting2) {
                betValues.add(betValue2);
            }
        }
        try {
            minBettingTime = ConfigGame.getIntValue("tx_min_betting_time");
            maxBettingTime = ConfigGame.getIntValue("tx_max_betting_time");
            final int totalBot2 = numBetTai + numBetXiu;
            final List<String> botsName = getBots(totalBot2, moneyType);
            for (int i = 0; i < totalBot2 && i < botsName.size(); ++i) {
                final String nickname = botsName.get(i);
                final int n2 = rd.nextInt(betValues.size());
                final long betValue3 = betValues.get(n2);
                final short bettingTime = (short)randomBettingTime(minBettingTime, maxBettingTime, phanTramVaoSom);
                short betSide = 0;
                if (i < numBetTai) {
                    betSide = 1;
                }
                final BotTaiXiu bot = new BotTaiXiu(nickname, bettingTime, betValue3, betSide);
                results.add(bot);
            }
            Debug.trace("NUMBER BOTS:" + results.size());
        }
        catch (Exception ex) {
            Debug.trace("Exception:" + ex.getMessage());
            final StringWriter sw = new StringWriter();
            final PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw);
            final String sStackTrace = sw.toString();
            Debug.trace(sStackTrace);
        }
        return results;
    }
    
    private static int randomBettingTime(final int minTime, final int maxTime, final int phanTramVaoSom) {
        final Random rd = new Random();
        final int n = rd.nextInt(100);
        if (n > phanTramVaoSom) {
            final int minTime5s = maxTime - 5;
            return rd.nextInt(maxTime - minTime5s) + minTime5s;
        }
        return rd.nextInt(maxTime - minTime) + minTime;
    }
    
    public static List<BotBauCua> getBotBauCua(final int roomId) {
        String moneyType = "xu";
        if (roomId < 3) {
            moneyType = "vin";
        }
        final long baseBetValue = getBaseBettingBC(roomId);
        final ArrayList<BotBauCua> results = new ArrayList<>();
        final Random rd = new Random();
        final int minBot = ConfigGame.getIntValue("bc_min_bot_" + roomId);
        final int maxBot = ConfigGame.getIntValue("bc_max_bot_" + roomId);
        if (maxBot <= minBot || maxBot == 0) {
            return new ArrayList<>();
        }
        final int minRatio = ConfigGame.getIntValue("bc_min_ratio_" + roomId);
        final int maxRatio = ConfigGame.getIntValue("bc_max_ratio_" + roomId);
        final int minBettingTime = ConfigGame.getIntValue("bc_min_betting_time");
        final int maxBettingTime = ConfigGame.getIntValue("bc_max_betting_time");
        final int numBots = rd.nextInt(maxBot - minBot) + minBot;
        final int maxBetSide = ConfigGame.getIntValue("bc_max_bet_side");
        final List<String> botsName = getBots(numBots, moneyType);
        for (int i = 0; i < numBots && i < botsName.size(); ++i) {
            final String nickname = botsName.get(i);
            final short bettingTime = (short)randomBettingTime(minBettingTime, maxBettingTime, 70);
            final long[] betArr = new long[6];
            int j = maxBetSide;
            while (j > 0) {
                final short betSide = (short)rd.nextInt(6);
                if (betArr[betSide] != 0L) {
                    continue;
                }
                final long betValue = betArr[betSide] = baseBetValue * (rd.nextInt(maxRatio - minRatio) + minRatio);
                --j;
            }
            final StringBuilder builder = new StringBuilder();
            for (j = 0; j < 6; ++j) {
                builder.append(",");
                builder.append(betArr[j]);
            }
            if (builder.length() > 0) {
                builder.deleteCharAt(0);
            }
            final BotBauCua bot = new BotBauCua(nickname, bettingTime, builder.toString());
            results.add(bot);
        }
        return results;
    }
    
    private static long getBaseBettingBC(final int roomId) {
        switch (roomId) {
            case 0: {
                return 1000L;
            }
            case 1: {
                return 10000L;
            }
            case 2: {
                return 100000L;
            }
            case 3: {
                return 10000L;
            }
            case 4: {
                return 100000L;
            }
            case 5: {
                return 1000000L;
            }
            default: {
                return 1000L;
            }
        }
    }
    
    public static List<String> getBotChat() {
        int number = 0;
        final Random rd = new Random();
        if (isNight()) {
            final int n = rd.nextInt(5);
            if (n == 0) {
                number = 1;
            }
        }
        else {
            number = rd.nextInt(3);
        }
        final ArrayList<String> results = new ArrayList<>();
        if (number > 0) {
            for (int i = 0; i < number; ++i) {
                int n2 = rd.nextInt(10);
                if (n2 == 0) {
                    n2 = rd.nextInt(BotMinigame.botsVip.size());
                    results.add(BotMinigame.botsVip.get(n2));
                }
                else {
                    n2 = rd.nextInt(BotMinigame.bots.size());
                    results.add(BotMinigame.bots.get(n2));
                }
            }
        }
        return results;
    }
    
    public static boolean isNight() {
        final Calendar cal = Calendar.getInstance();
        final int hourOfDay = cal.get(Calendar.HOUR_OF_DAY);
        return 2 <= hourOfDay && hourOfDay <= 8;
    }
    
    public static int ratioTXInNight() {
        final Calendar cal = Calendar.getInstance();
        final int hourOfDay = cal.get(Calendar.HOUR_OF_DAY);
        final Random rd = new Random();
        if (2 <= hourOfDay && hourOfDay <= 8) {
            switch (hourOfDay) {
                case 2:
                case 8: {
                    return rd.nextInt(20) + 80;
                }
                case 3:
                case 7: {
                    return rd.nextInt(30) + 50;
                }
                case 4:
                case 5:
                case 6: {
                    return rd.nextInt(20) + 30;
                }
            }
        }
        return 100;
    }
    
    public static void main(final String[] args) {
        isNight();
    }
    
    static {
        BotMinigame.bots = new ArrayList<>();
        BotMinigame.botsVipDaily = new ArrayList<>();
        BotMinigame.botsVip = new ArrayList<>();
        BotMinigame.userService = new UserServiceImpl();
        BotMinigame.botService = new BotServiceImpl();
        BotMinigame.betValueDefault = Arrays.asList(5555, 6666, 7777, 8888, 9999, 6789, 11111, 22222, 33333, 44444, 55555);
        BotMinigame.soVinBan = new long[] { 3000000L, 35000000L, 4000000L, 50000000L };
        BotMinigame.updateTime = System.currentTimeMillis();
    }
}
