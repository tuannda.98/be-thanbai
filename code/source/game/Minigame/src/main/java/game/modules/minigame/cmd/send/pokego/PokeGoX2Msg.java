// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.send.pokego;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class PokeGoX2Msg extends BaseMsgEx
{
    public String ngayX2;
    
    public PokeGoX2Msg() {
        super(7009);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        this.putStr(bf, this.ngayX2);
        return this.packBuffer(bf);
    }
}
