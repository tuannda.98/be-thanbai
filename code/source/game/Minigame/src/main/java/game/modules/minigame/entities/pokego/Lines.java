// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.entities.pokego;

import java.util.ArrayList;
import java.util.List;

public class Lines
{
    private final List<Line> lines;
    
    public Lines() {
        this.lines = new ArrayList<>();
        this.initLines();
    }
    
    private void initLines() {
        this.lines.clear();
        final Line line1 = new Line("line1", 0, 0, 0, 1, 0, 2);
        this.lines.add(line1);
        final Line line2 = new Line("line2", 1, 0, 1, 1, 1, 2);
        this.lines.add(line2);
        final Line line3 = new Line("line3", 2, 0, 2, 1, 2, 2);
        this.lines.add(line3);
        final Line line4 = new Line("line4", 0, 0, 2, 1, 0, 2);
        this.lines.add(line4);
        final Line line5 = new Line("line5", 2, 0, 0, 1, 2, 2);
        this.lines.add(line5);
        final Line line6 = new Line("line6", 0, 0, 1, 1, 0, 2);
        this.lines.add(line6);
        final Line line7 = new Line("line7", 0, 0, 1, 1, 2, 2);
        this.lines.add(line7);
        final Line line8 = new Line("line8", 2, 0, 1, 1, 0, 2);
        this.lines.add(line8);
        final Line line9 = new Line("line9", 1, 0, 2, 1, 1, 2);
        this.lines.add(line9);
        final Line line10 = new Line("line10", 1, 0, 0, 1, 1, 2);
        this.lines.add(line10);
        final Line line11 = new Line("line11", 2, 0, 1, 1, 2, 2);
        this.lines.add(line11);
        final Line line12 = new Line("line12", 0, 0, 0, 1, 1, 2);
        this.lines.add(line12);
        final Line line13 = new Line("line13", 1, 0, 1, 1, 2, 2);
        this.lines.add(line13);
        final Line line14 = new Line("line14", 1, 0, 1, 1, 0, 2);
        this.lines.add(line14);
        final Line line15 = new Line("line15", 2, 0, 2, 1, 1, 2);
        this.lines.add(line15);
        final Line line16 = new Line("line16", 1, 0, 0, 1, 0, 2);
        this.lines.add(line16);
        final Line line17 = new Line("line17", 2, 0, 1, 1, 1, 2);
        this.lines.add(line17);
        final Line line18 = new Line("line18", 0, 0, 1, 1, 1, 2);
        this.lines.add(line18);
        final Line line19 = new Line("line19", 1, 0, 2, 1, 2, 2);
        this.lines.add(line19);
        final Line line20 = new Line("line20", 0, 0, 2, 1, 1, 2);
        this.lines.add(line20);
    }
    
    public List<Line> list() {
        return this.lines;
    }
    
    public Line get(final int index) {
        return this.lines.get(index);
    }
    
    public void renew() {
    }
}
