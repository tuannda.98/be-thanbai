// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.chat.cmd.rev;

import java.nio.ByteBuffer;
import bitzero.server.extensions.data.DataCmd;
import bitzero.server.extensions.data.BaseCmd;

public class ChatCmd extends BaseCmd
{
    public String message;
    
    public ChatCmd(final DataCmd dataCmd) {
        super(dataCmd);
        this.unpackData();
    }
    
    public void unpackData() {
        final ByteBuffer bf = this.makeBuffer();
        this.message = this.readString(bf);
    }
}
