// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.utils;

import java.util.Random;

public class RandomUtil
{
    public static int randInt(final int min, int max) {
        ++max;
        final Random rand = new Random();
        return rand.nextInt(max - min) + min;
    }
    
    public static int randInt(final int max) {
        final Random random = new Random();
        return random.nextInt(max);
    }
    
    public static double randDouble(final double min, final double max) {
        final Random r = new Random();
        return min + (max - min) * r.nextDouble();
    }
}
