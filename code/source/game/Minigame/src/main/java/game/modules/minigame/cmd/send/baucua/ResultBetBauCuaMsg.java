// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.send.baucua;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class ResultBetBauCuaMsg extends BaseMsgEx
{
    public byte result;
    public long currentMoney;
    
    public ResultBetBauCuaMsg() {
        super(5004);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        bf.put(this.result);
        bf.putLong(this.currentMoney);
        return this.packBuffer(bf);
    }
}
