// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.lobby.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class ResultNapTienDienThoaiMsg extends BaseMsgEx
{
    public long currentMoney;
    
    public ResultNapTienDienThoaiMsg() {
        super(20036);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        bf.putLong(this.currentMoney);
        return this.packBuffer(bf);
    }
}
