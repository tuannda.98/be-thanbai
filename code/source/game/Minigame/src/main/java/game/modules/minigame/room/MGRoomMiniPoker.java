package game.modules.minigame.room;

import bitzero.server.entities.User;
import bitzero.util.common.business.Debug;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.vinplay.cardlib.models.Card;
import com.vinplay.cardlib.models.GroupType;
import com.vinplay.cardlib.utils.CardLibUtils;
import com.vinplay.dal.service.BroadcastMessageService;
import com.vinplay.dal.service.CacheService;
import com.vinplay.dal.service.MiniGameService;
import com.vinplay.dal.service.MiniPokerService;
import com.vinplay.dal.service.impl.BroadcastMessageServiceImpl;
import com.vinplay.dal.service.impl.CacheServiceImpl;
import com.vinplay.dal.service.impl.MiniGameServiceImpl;
import com.vinplay.dal.service.impl.MiniPokerServiceImpl;
import com.vinplay.usercore.service.UserService;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.enums.Games;
import com.vinplay.vbee.common.exceptions.KeyNotFoundException;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.response.MoneyResponse;
import com.vinplay.vbee.common.statics.TransType;
import game.modules.minigame.MiniPokerModule;
import game.modules.minigame.cmd.send.minipoker.ForceStopAuatoPlayMiniPokerMsg;
import game.modules.minigame.cmd.send.minipoker.ResultMiniPokerMsg;
import game.modules.minigame.cmd.send.minipoker.UpdatePotMiniPokerMsg;
import game.modules.minigame.entities.AutoUserMiniPoker;
import game.modules.minigame.entities.MinigameConstant;
import game.modules.minigame.utils.GenerationMiniPoker;
import game.utils.ConfigGame;
import game.utils.GameUtils;
import vn.yotel.yoker.util.Util;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeoutException;

public class MGRoomMiniPoker extends MGRoom {
    private float tax;
    private long pot;
    private long fund;
    private final short moneyType;
    private String moneyTypeStr;
    private long betValue;
    private long initPotValue;
    private final UserService userService;
    private final MiniPokerService mpService;
    private final MiniGameService mgService;
    private final BroadcastMessageService broadcastMsgService;
    private final GenerationMiniPoker gen;
    private final Map<String, AutoUserMiniPoker> usersAuto;
    private long lastTimeUpdatePotToRoom;
    private long lastTimeUpdateFundToRoom;
    private final ThreadPoolExecutor executor;
    private int countHu;
    private int countNoHuX2;
    private boolean huX2;
    protected final CacheService sv;

    public MGRoomMiniPoker(final String roomName, final short moneyType, final long pot, final long fund, final long baseBetValue, final long initPotValue) {
        super(roomName, Games.MINI_POKER);
        this.tax = MinigameConstant.MINIGAME_TAX_VIN;
        this.betValue = 0L;
        this.initPotValue = 500000L;
        this.userService = new UserServiceImpl();
        this.mpService = new MiniPokerServiceImpl();
        this.mgService = new MiniGameServiceImpl();
        this.broadcastMsgService = new BroadcastMessageServiceImpl();
        this.gen = new GenerationMiniPoker();
        this.usersAuto = new HashMap<>();
        this.lastTimeUpdatePotToRoom = 0L;
        this.lastTimeUpdateFundToRoom = 0L;
        this.countHu = -1;
        this.countNoHuX2 = 0;
        this.huX2 = false;
        this.sv = new CacheServiceImpl();
        this.moneyType = moneyType;
        if (moneyType == 1) {
            this.moneyTypeStr = "vin";
            this.tax = MinigameConstant.MINIGAME_TAX_VIN;
        } else if (moneyType == 0) {
            this.moneyTypeStr = "xu";
            this.tax = MinigameConstant.MINIGAME_TAX_XU;
        }
        this.executor = (ThreadPoolExecutor) ((moneyType == 1) ? Executors.newFixedThreadPool(
                ConfigGame.getIntValue("mini_poker_thread_pool_per_room_vin"), new ThreadFactoryBuilder().setNameFormat("MiniPoker-thread-%d").build())
                : ((ThreadPoolExecutor) Executors.newFixedThreadPool(
                ConfigGame.getIntValue("mini_poker_thread_pool_per_room_xu"), new ThreadFactoryBuilder().setNameFormat("MiniPoker-thread-%d").build())));
        this.pot = pot;
        final CacheServiceImpl cacheService = new CacheServiceImpl();
        cacheService.setValue(this.name, (int) pot);
        this.fund = fund;
        this.betValue = baseBetValue;
        this.initPotValue = initPotValue;
        TimerTask gameLoopTask = new TimerTask() {
            @Override
            public void run() {
                gameLoop();
            }
        };
        new Timer().scheduleAtFixedRate(gameLoopTask, Util.randInt(5_000, 10_000), 1_000);
        try {
            this.countHu = this.sv.getValueInt(this.name + "_count_hu");
            this.countNoHuX2 = this.sv.getValueInt(this.name + "_count_no_hu_x2");
            this.calculatHuX2();
        } catch (KeyNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            this.mgService.savePot(this.name, pot, this.huX2);
        } catch (IOException | TimeoutException | InterruptedException ex2) {
            ex2.printStackTrace();
        }
    }

    public short play(final User user) {
        return this.play(user, this.betValue);
    }

    public short play(final User user, final long betValue) {
        final ResultMiniPokerMsg msg = this.play(user.getName(), betValue);
        this.sendMessageToUser(msg, user);
        return msg.result;
    }

    public ResultMiniPokerMsg play(final String username, final long betValue) {
        final long lastPot = this.pot;
        final long lastFund = this.fund;
        final ResultMiniPokerMsg resultMiniPokerMsg = new ResultMiniPokerMsg();
        long currentMoney = this.userService.getMoneyUserCache(username, this.moneyTypeStr);
        if (this.fund < 0L) {
            resultMiniPokerMsg.result = 100;
            resultMiniPokerMsg.prize = 0;
            resultMiniPokerMsg.currentMoney = currentMoney;
            return resultMiniPokerMsg;
        }

        final UserCacheModel u = this.userService.forceGetCachedUser(username);
        final long referenceId = System.currentTimeMillis();
        final StringBuilder builder = new StringBuilder();
        if (betValue <= 0L) {
            resultMiniPokerMsg.result = 101;
            resultMiniPokerMsg.prize = 0;
            resultMiniPokerMsg.currentMoney = currentMoney;
            return resultMiniPokerMsg;
        }
        long minVin = this.userService.getAvailableVinMoney(username);
        if (minVin < betValue) {
            resultMiniPokerMsg.result = 102;
            resultMiniPokerMsg.prize = 0;
            resultMiniPokerMsg.currentMoney = currentMoney;
            return resultMiniPokerMsg;
        }

        MoneyResponse moneyRes = this.userService.updateMoney(username, -betValue, this.moneyTypeStr, Games.MINI_POKER.getName(), "Quay MiniPoker", "\u0110\u1eb7t c\u01b0\u1ee3c MiniPoker", 0L, referenceId, TransType.START_TRANS);
        if (moneyRes == null || !moneyRes.isSuccess()) {
            resultMiniPokerMsg.result = 0;
            resultMiniPokerMsg.prize = 0;
            resultMiniPokerMsg.currentMoney = currentMoney;
            return resultMiniPokerMsg;
        }

        short result = 0;
        long prize = 0L;
        boolean enoughToPair = false;
        final long moneyToPot = betValue / 100L;
        final long fee = (long) (betValue * this.tax / 100.0f);
        final long moneyToFund = betValue - moneyToPot - fee;
        long tienThuongX2 = 0L;
        increasePot(moneyToPot);
        this.fund += moneyToFund;
        while (!enoughToPair) {
            prize = 0L;
            tienThuongX2 = 0L;
            long moneyExchange = 0L;
            List<Card> cards = this.gen.randomCards2();
            if (cards.size() != 5) {
                cards = this.gen.randomCards();
            }
            final GroupType groupType;
            if ((groupType = CardLibUtils.calculateTypePoker(cards)) == null) {
                continue;
            }
            switch (groupType) {
                case HighCard: {
                    result = 11;
                    break;
                }
                case OnePair: {
                    if (CardLibUtils.pairEqualOrGreatJack(cards)) {
                        result = 9;
                        prize = (int) (betValue * 2.5f);
                        break;
                    }
                    result = 10;
                    prize = 0L;
                    break;
                }
                case TwoPair: {
                    result = 8;
                    prize = betValue * 5L;
                    break;
                }
                case ThreeOfKind: {
                    result = 7;
                    prize = betValue * 8L;
                    break;
                }
                case Straight: {
                    result = 6;
                    if (this.moneyType == 1) {
                        prize = betValue * 13L;
                        break;
                    }
                    prize = betValue * 12L;
                    break;
                }
                case Flush: {
                    result = 5;
                    if (this.moneyType == 1) {
                        prize = betValue * 20L;
                        break;
                    }
                    prize = betValue * 18L;
                    break;
                }
                case FullHouse: {
                    result = 4;
                    if (this.moneyType == 1) {
                        prize = betValue * 50L;
                        break;
                    }
                    prize = betValue * 40L;
                    break;
                }
                case FourOfKind: {
                    result = 3;
                    if (this.moneyType == 1) {
                        prize = betValue * 150L;
                        break;
                    }
                    prize = betValue * 120L;
                    break;
                }
                case StraightFlush: {
                    if (!u.isBot()) {
                        continue;
                    }
                    if (!CardLibUtils.isStraightFlushJack(cards)) {
                        result = 2;
                        prize = betValue * 1000L;
                        break;
                    }
                    if (!u.isBot()) {
                        continue;
                    }
                    result = 1;
                    if (this.huX2) {
                        tienThuongX2 = this.pot;
                        prize = this.pot * 2L;
                        break;
                    }
                    prize = this.pot;
                    break;
                }
            }
            final long l;
            final long fundExchange = l = (Math.max(prize, 0L));
            if (result == 1) {
                if (this.fund - this.initPotValue < 0L) {
                    continue;
                }
            } else if (this.fund - fundExchange < 0L) {
                continue;
            }
            enoughToPair = true;
            if (cards.size() == 5) {
                resultMiniPokerMsg.card1 = (byte) cards.get(0).getCode();
                resultMiniPokerMsg.card2 = (byte) cards.get(1).getCode();
                resultMiniPokerMsg.card3 = (byte) cards.get(2).getCode();
                resultMiniPokerMsg.card4 = (byte) cards.get(3).getCode();
                resultMiniPokerMsg.card5 = (byte) cards.get(4).getCode();
            }
            if (prize > 0L) {
                if (result == 1) {
                    if (this.huX2) {
                        result = 12;
                    }
                    this.noHuX2();
                    if (this.moneyType == 1) {
                        GameUtils.sendSMSToUser(username, "Chuc mung " + username + " da no hu game MiniPoker phong " + betValue + ". So tien no hu: " + this.pot + " Vin");
                    }
                    this.pot = this.initPotValue;
                    this.fund -= this.initPotValue;
                } else {
                    this.fund -= fundExchange;
                }
            }
            long moneyAdded = prize;
            final String des = "Quay MiniPoker";
            if (result == 12) {
                moneyAdded -= tienThuongX2;
                this.userService.updateMoney(username, tienThuongX2, this.moneyTypeStr, this.gameName.getName(), des, "Th\u1eafng X2", 0L, null, TransType.NO_VIPPOINT);
            }
            moneyRes = this.userService.updateMoney(username, moneyAdded, this.moneyTypeStr, Games.MINI_POKER.getName(), des, this.buildDescription(betValue, moneyAdded, result), fee, referenceId, TransType.END_TRANS);
            moneyExchange = prize - betValue;
            if (moneyRes != null && moneyRes.isSuccess()) {
                currentMoney = moneyRes.getCurrentMoney();
                if (this.moneyType == 1 && moneyExchange >= BroadcastMessageServiceImpl.MIN_MONEY) {
                    this.broadcastMsgService.putMessage(Games.MINI_POKER.getId(), username, moneyExchange);
                }
            }
            builder.append(cards.get(0).toString());
            for (int i = 1; i < cards.size(); ++i) {
                builder.append(",");
                builder.append(cards.get(i).toString());
            }
            try {
                this.mpService.logMiniPoker(username, betValue, result, prize, builder.toString(), lastPot, lastFund, this.moneyType);
            } catch (Exception ex2) {
                Debug.trace("Log mini poker error ", ex2.getMessage());
            }
        }
        this.saveFund();
        this.savePot();

        resultMiniPokerMsg.result = result;
        resultMiniPokerMsg.prize = prize;
        resultMiniPokerMsg.currentMoney = currentMoney;
        return resultMiniPokerMsg;
    }

    public ResultMiniPokerMsg playWinJackpot(final String username, final long betValue) {
        final long lastPot = this.pot;
        final long lastFund = this.fund;
        final ResultMiniPokerMsg resultMiniPokerMsg = new ResultMiniPokerMsg();
        final StringBuilder builder = new StringBuilder();
        short result = 0;
        long prize = 0L;
        long currentMoney = this.userService.getMoneyUserCache(username, this.moneyTypeStr);
        final UserCacheModel u = this.userService.forceGetCachedUser(username);
        final long referenceId = System.currentTimeMillis();
        if (betValue > 0L) {
            if (currentMoney >= betValue) {
                MoneyResponse moneyRes = this.userService.updateMoney(username, -betValue, this.moneyTypeStr, Games.MINI_POKER.getName(), "Quay MiniPoker", "\u0110\u1eb7t c\u01b0\u1ee3c MiniPoker", 0L, referenceId, TransType.START_TRANS);
                if (moneyRes != null && moneyRes.isSuccess()) {
                    boolean enoughToPair = false;
                    final long moneyToPot = betValue / 100L;
                    final long fee = (long) (betValue * this.tax / 100.0f);
                    long tienThuongX2 = 0L;
                    increasePot(moneyToPot);
                    while (!enoughToPair) {
                        prize = 0L;
                        tienThuongX2 = 0L;
                        long moneyExchange = 0L;
                        List<Card> cards = this.gen.randomCards2();
                        if (cards.size() != 5) {
                            cards = this.gen.randomCards();
                        }
                        final GroupType groupType = GroupType.StraightFlush;
                            /*if ((groupType = CardLibUtils.calculateTypePoker((List)cards)) == null) {
                                continue;
                            }
                             */
                        switch (groupType) {
                            case HighCard: {
                                result = 11;
                                break;
                            }
                            case OnePair: {
                                if (CardLibUtils.pairEqualOrGreatJack(cards)) {
                                    result = 9;
                                    prize = (int) (betValue * 2.5f);
                                    break;
                                }
                                result = 10;
                                prize = 0L;
                                break;
                            }
                            case TwoPair: {
                                result = 8;
                                prize = betValue * 5L;
                                break;
                            }
                            case ThreeOfKind: {
                                result = 7;
                                prize = betValue * 8L;
                                break;
                            }
                            case Straight: {
                                result = 6;
                                if (this.moneyType == 1) {
                                    prize = betValue * 13L;
                                    break;
                                }
                                prize = betValue * 12L;
                                break;
                            }
                            case Flush: {
                                result = 5;
                                if (this.moneyType == 1) {
                                    prize = betValue * 20L;
                                    break;
                                }
                                prize = betValue * 18L;
                                break;
                            }
                            case FullHouse: {
                                result = 4;
                                if (this.moneyType == 1) {
                                    prize = betValue * 50L;
                                    break;
                                }
                                prize = betValue * 40L;
                                break;
                            }
                            case FourOfKind: {
                                result = 3;
                                if (this.moneyType == 1) {
                                    prize = betValue * 150L;
                                    break;
                                }
                                prize = betValue * 120L;
                                break;
                            }
                            case StraightFlush: {
                                Random r = new Random();
                                int randomVal = r.nextInt(100);
                                // 50% cơ hội trúng thắng lớn x 1000
                                if (randomVal < 50) {
                                    result = 2;
                                    prize = betValue * 1000L;
                                    break;
                                }
                                // hoặc 50% cơ hội ăn nổ hũ
                                result = 1;
                                if (this.huX2) {
                                    tienThuongX2 = this.pot;
                                    prize = this.pot * 2L;
                                    break;
                                }
                                prize = this.pot;
                                break;
                            }
                        }
                        final long l;
                        final long fundExchange = l = (Math.max(prize, 0L));
                        // Giới hạn nổ hũ
                            /*if (result == 1) {
                                if (this.fund - this.initPotValue < 0L) {
                                    continue;
                                }
                            } else if (this.fund - fundExchange < 0L) {
                                continue;
                            }*/
                        enoughToPair = true;
                        if (cards.size() == 5) {
                            resultMiniPokerMsg.card1 = (byte) cards.get(0).getCode();
                            resultMiniPokerMsg.card2 = (byte) cards.get(1).getCode();
                            resultMiniPokerMsg.card3 = (byte) cards.get(2).getCode();
                            resultMiniPokerMsg.card4 = (byte) cards.get(3).getCode();
                            resultMiniPokerMsg.card5 = (byte) cards.get(4).getCode();
                        }
                        if (prize > 0L) {
                            if (result == 1) {
                                if (this.huX2) {
                                    result = 12;
                                }
                                this.noHuX2();
                                if (this.moneyType == 1) {
                                    GameUtils.sendSMSToUser(username, "Chuc mung " + username + " da no hu game MiniPoker phong " + betValue + ". So tien no hu: " + this.pot + " Vin");
                                }
                                this.pot = this.initPotValue;
                            }
                        }
                        long moneyAdded = prize;
                        final String des = "Quay MiniPoker";
                        if (result == 12) {
                            moneyAdded -= tienThuongX2;
                            this.userService.updateMoney(username, tienThuongX2, this.moneyTypeStr, this.gameName.getName(), des, "Th\u1eafng X2", 0L, null, TransType.NO_VIPPOINT);
                        }
                        moneyRes = this.userService.updateMoney(username, moneyAdded, this.moneyTypeStr, Games.MINI_POKER.getName(), des, this.buildDescription(betValue, moneyAdded, result), fee, referenceId, TransType.END_TRANS);
                        moneyExchange = prize - betValue;
                        if (moneyRes != null && moneyRes.isSuccess()) {
                            currentMoney = moneyRes.getCurrentMoney();
                            if (this.moneyType == 1 && moneyExchange >= BroadcastMessageServiceImpl.MIN_MONEY) {
                                this.broadcastMsgService.putMessage(Games.MINI_POKER.getId(), username, moneyExchange);
                            }
                        }
                        builder.append(cards.get(0).toString());
                        for (int i = 1; i < cards.size(); ++i) {
                            builder.append(",");
                            builder.append(cards.get(i).toString());
                        }
                        try {
                            this.mpService.logMiniPoker(username, betValue, result, prize, builder.toString(), lastPot, lastFund, this.moneyType);
                        }
//                            catch (IOException | InterruptedException | TimeoutException ex2) {
//                                final Exception ex;
//                                final Exception e = ex;
//                                Debug.trace((Object[])new Object[] { "Log mini poker error ", e.getMessage() });
//                            }
                        catch (Exception ex2) {
                            Debug.trace("Log mini poker error ", ex2.getMessage());
                        }
                    }
                    this.saveFund();
                    this.savePot();
                }
            } else {
                result = 102;
            }
        } else {
            result = 101;
        }
        resultMiniPokerMsg.result = result;
        resultMiniPokerMsg.prize = prize;
        resultMiniPokerMsg.currentMoney = currentMoney;
        return resultMiniPokerMsg;
    }

    public void autoPlay(final User user) {
        synchronized (this.usersAuto) {
            if (this.usersAuto.containsKey(user.getName())) {
                final AutoUserMiniPoker entry = this.usersAuto.get(user.getName());
                this.forceStopAutoPlay(entry.getUser());
            }
            this.usersAuto.put(user.getName(), new AutoUserMiniPoker(user, 0));
        }
    }

    public void stopAutoPlay(final User user) {
        synchronized (this.usersAuto) {
            final AutoUserMiniPoker entry;
            if (this.usersAuto.containsKey(user.getName()) && (entry = this.usersAuto.get(user.getName())).getUser().getId() == user.getId()) {
                this.usersAuto.remove(user.getName());
            }
        }
    }

    public void forceStopAutoPlay(final User user) {
        synchronized (this.usersAuto) {
            this.usersAuto.remove(user.getName());
            final ForceStopAuatoPlayMiniPokerMsg msg = new ForceStopAuatoPlayMiniPokerMsg();
            this.sendMessageToUser(msg, user);
        }
    }

    private boolean checkDienKienNoHu(final String username) {
        try {
            final UserModel u = this.userService.getUserByUserName(username);
            return u.isBot();
        } catch (Exception u2) {
            return false;
        }
    }

    private void saveFund() {
        final long currentTime = System.currentTimeMillis();
        if (currentTime - this.lastTimeUpdateFundToRoom >= 60000L) {
            try {
                this.mgService.saveFund(this.name, this.fund);
            }
//            catch (IOException | InterruptedException | TimeoutException ex2) {
//                final Exception ex;
//                final Exception e = ex;
//                Debug.trace((Object[])new Object[] { "MINI POKER: update fund poker error ", e.getMessage() });
//            }
            catch (Exception ex2) {
                Debug.trace("MINI POKER: update fund poker error ", ex2.getMessage());
            }

            this.lastTimeUpdateFundToRoom = currentTime;
        }
    }

    public synchronized void increasePot(long moneyToPot) {
        this.pot += moneyToPot;
    }

    public void savePot() {
        final long currentTime = System.currentTimeMillis();
        if (currentTime - this.lastTimeUpdatePotToRoom >= 3000L) {
            final UpdatePotMiniPokerMsg msg = new UpdatePotMiniPokerMsg();
            msg.value = this.pot;
            msg.x2 = (byte) (this.huX2 ? 1 : 0);
            this.sendMessageToRoom(msg);
            this.lastTimeUpdatePotToRoom = currentTime;
            try {
                this.mgService.savePot(this.name, this.pot, false);
            } catch (Exception ex2) {
                Debug.trace("MINI POKER: update pot poker error ", ex2.getMessage());
            }
        }
    }

    public void updatePotToUser(final User user) {
        final UpdatePotMiniPokerMsg msg = new UpdatePotMiniPokerMsg();
        msg.value = this.pot;
        msg.x2 = (byte) (this.huX2 ? 1 : 0);
        this.sendMessageToUser(msg, user);
    }

    public void gameLoop() {
        final ArrayList<User> usersPlay = new ArrayList<>();
        synchronized (this.usersAuto) {
            for (final AutoUserMiniPoker user : this.usersAuto.values()) {
                final boolean play = user.incCount();
                if (!play) {
                    continue;
                }
                usersPlay.add(user.getUser());
            }
        }
        for (int numThreads = usersPlay.size() / 100 + 1, i = 1; i <= numThreads; ++i) {
            final int fromIndex = (i - 1) * 100;
            int toIndex = i * 100;
            if (toIndex > usersPlay.size()) {
                toIndex = usersPlay.size();
            }
            final ArrayList<User> tmp = new ArrayList(usersPlay.subList(fromIndex, toIndex));
            final PlayListMiniPokerTask task = new PlayListMiniPokerTask(tmp);
            this.executor.execute(task);
        }
        usersPlay.clear();
    }

    public void playListMiniPoker(final List<User> users) {
        for (final User user : users) {
            final short result = this.play(user, this.betValue);
            if (result != 1 && result != 2 && result != 12 && result != 102 && result != 100) {
                continue;
            }
            this.forceStopAutoPlay(user);
        }
        users.clear();
    }

    private String buildDescription(final long totalBet, final long totalPrizes, final short result) {
        if (totalBet == 0L) {
            return this.resultToString(result) + ": " + totalPrizes;
        }
        return "Quay: " + totalBet + ", " + this.resultToString(result) + ": " + totalPrizes;
    }

    private String resultToString(final short result) {
        switch (result) {
            case 1: {
                return "N\u1ed5 h\u0169";
            }
            case 12: {
                return "N\u1ed5 h\u0169 X2";
            }
            case 2: {
                return "Th\u00f9ng ph\u00e1 s\u1ea3nh";
            }
            case 3: {
                return "T\u1ee9 qu\u00fd";
            }
            case 4: {
                return "C\u00f9 l\u0169";
            }
            case 5: {
                return "Th\u1eafng";
            }
            case 6: {
                return "S\u1ea3nh";
            }
            case 7: {
                return "S\u1ea3nh ch\u00faa";
            }
            case 8: {
                return "Hai \u0111\u00f4i";
            }
            case 9: {
                return "L\u00e1 b\u00e0i cao";
            }
            default: {
                return "Tr\u01b0\u1ee3t";
            }
        }
    }

    @Override
    public boolean joinRoom(final User user) {
        final boolean result = super.joinRoom(user);
        if (result) {
            user.setProperty("MGROOM_MINI_POKER_INFO", this);
        }
        return result;
    }

    @Override
    public boolean quitRoom(final User user) {
        return super.quitRoom(user);
    }

    public void startHuX2() {
        Debug.trace(this.gameName + " start hu X2");
        this.countHu = 1;
        this.sv.setValue(this.name + "_count_hu", this.countHu);
        if (this.moneyType == 1 && this.betValue == 100L) {
            this.huX2 = true;
        }
    }

    public void stopHuX2() {
        Debug.trace(this.gameName + " stop hu x2");
        this.countHu = -1;
        this.countNoHuX2 = 0;
        this.huX2 = false;
        this.sv.setValue(this.name + "_count_hu", this.countHu);
        this.sv.setValue(this.name + "_count_no_hu_x2", this.countNoHuX2);
    }

    public void noHuX2() {
        if (this.countHu > -1) {
            ++this.countHu;
            this.sv.setValue(this.name + "_count_hu", this.countHu);
            if (this.huX2) {
                ++this.countNoHuX2;
                this.sv.setValue(this.name + "_count_no_hu_x2", this.countNoHuX2);
                Debug.trace(this.gameName + " No hu X2: " + this.countHu + " , huX2= " + this.countNoHuX2);
                if (this.betValue == 100L && this.countNoHuX2 >= 10) {
                    MiniPokerModule.stopX2();
                    this.stopHuX2();
                }
                if (this.betValue == 1000L && this.countNoHuX2 >= 1) {
                    MiniPokerModule.stopX2();
                    this.stopHuX2();
                }
            }
            this.calculatHuX2();
        }
    }

    private void calculatHuX2() {
        if (this.countHu > -1 && this.moneyType == 1) {
            if (this.betValue == 100L) {
                this.huX2 = (this.countHu % 4 == 1 && this.countNoHuX2 < 10);
            } else if (this.betValue == 1000L) {
                this.huX2 = (this.countHu == 3 && this.countNoHuX2 < 1);
            }
            Debug.trace("Count hu X2 " + this.name + ": " + this.countNoHuX2);
        }
        Debug.trace("Count hu " + this.name + ": " + this.countHu + ", x2= " + this.huX2);
    }

    public static class ResultPoker {
        public static final short LOI_HE_THONG = 100;
        public static final short DAT_CUOC_KHONG_HOP_LE = 101;
        public static final short KHONG_DU_TIEN = 102;
        public static final short TRUOT = 0;
        public static final short NO_HU = 1;
        public static final short THUNG_PHA_SANH_NHO = 2;
        public static final short TU_QUY = 3;
        public static final short CU_LU = 4;
        public static final short THUNG = 5;
        public static final short SANH = 6;
        public static final short SAM_CO = 7;
        public static final short HAI_DOI = 8;
        public static final short MOT_DOI_TO = 9;
        public static final short MOT_DOI_NHO = 10;
        public static final short BAI_CAO = 11;
        public static final short NO_HU_X2 = 12;
    }

    private final class PlayListMiniPokerTask extends Thread {
        private final List<User> users;

        private PlayListMiniPokerTask(final List<User> users) {
            this.users = users;
            this.setName("AutoPlayMiniPoker");
        }

        @Override
        public void run() {
            MGRoomMiniPoker.this.playListMiniPoker(this.users);
        }
    }
}
