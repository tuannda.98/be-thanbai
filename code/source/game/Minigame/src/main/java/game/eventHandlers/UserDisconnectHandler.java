package game.eventHandlers;

import bitzero.server.exceptions.BZException;
import casio.king365.core.HCMap;
import com.hazelcast.core.IMap;
import game.BaseGameExtension;
import com.vinplay.vbee.common.enums.Platform;
import bitzero.server.core.BZEventParam;
import bitzero.server.entities.User;
import bitzero.server.core.IBZEvent;
import bitzero.server.extensions.BaseServerEventHandler;

public class UserDisconnectHandler extends BaseServerEventHandler {
    public void handleServerEvent(final IBZEvent ibzevent) throws BZException {
        final User user = (User) ibzevent.getParameter(BZEventParam.USER);
        final String pf = (String) user.getProperty("pf");
        final Platform platform = Platform.find(pf);
        BaseGameExtension.decreaseCCU(platform);

        // Remove to user online map
        IMap<String, Boolean> connectedMap = HCMap.getConnectedMinigameMap();
        connectedMap.remove(user.getName());
    }
}
