// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.send.caothap;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class UpdateTimeCaoThapMsg extends BaseMsgEx
{
    public short time;
    
    public UpdateTimeCaoThapMsg() {
        super(6008);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        bf.putShort(this.time);
        return this.packBuffer(bf);
    }
}
