package game.modules.minigame.room;

import bitzero.server.entities.User;
import bitzero.util.common.business.Debug;
import com.vinplay.dal.service.BroadcastMessageService;
import com.vinplay.dal.service.CacheService;
import com.vinplay.dal.service.MiniGameService;
import com.vinplay.dal.service.PokeGoService;
import com.vinplay.dal.service.impl.BroadcastMessageServiceImpl;
import com.vinplay.dal.service.impl.CacheServiceImpl;
import com.vinplay.dal.service.impl.MiniGameServiceImpl;
import com.vinplay.dal.service.impl.PokeGoServiceImpl;
import com.vinplay.usercore.service.UserService;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.enums.Games;
import com.vinplay.vbee.common.exceptions.KeyNotFoundException;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.response.MoneyResponse;
import com.vinplay.vbee.common.statics.TransType;
import com.vinplay.vbee.common.utils.CommonUtils;
import com.vinplay.vbee.common.utils.DateTimeUtils;
import game.modules.minigame.PokeGoModule;
import game.modules.minigame.cmd.send.pokego.ForceStopAutoPlayPokeGoMsg;
import game.modules.minigame.cmd.send.pokego.ResultPokeGoMsg;
import game.modules.minigame.cmd.send.pokego.UpdatePotPokeGoMsg;
import game.modules.minigame.entities.pokego.*;
import game.modules.minigame.utils.PokeGoUtils;
import game.utils.ConfigGame;
import game.utils.GameUtils;
import vn.yotel.yoker.util.Util;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeoutException;

public class MGRoomPokeGo extends MGRoom {
    private long pot;
    private long fund;
    private final long initPotValue;
    private final int betValue;
    private final short moneyType;
    private final String moneyTypeStr;
    private final UserService userService;
    private final MiniGameService mgService;
    private final PokeGoService pgService;
    private final BroadcastMessageService broadcastMsgService;
    private final Map<String, AutoUserPokeGo> usersAuto;
    private final Lines lines;
    private long lastTimeUpdatePotToRoom;
    private long lastTimeUpdateFundToRoom;
    private final ThreadPoolExecutor executor;
    private int countHu;
    private int countNoHuX2;
    private boolean huX2;
    protected final CacheService sv;

    public MGRoomPokeGo(final String name, final short moneyType, final long pot, final long fund, final int betValue, final long initPotValue) {
        super(name, Games.POKE_GO);
        this.userService = new UserServiceImpl();
        this.mgService = new MiniGameServiceImpl();
        this.pgService = new PokeGoServiceImpl();
        this.broadcastMsgService = new BroadcastMessageServiceImpl();
        this.usersAuto = new HashMap<>();
        this.lines = new Lines();
        this.lastTimeUpdatePotToRoom = 0L;
        this.lastTimeUpdateFundToRoom = 0L;
        this.countHu = -1;
        this.countNoHuX2 = 0;
        this.huX2 = false;
        this.sv = new CacheServiceImpl();
        this.moneyType = moneyType;
        this.moneyTypeStr = ((this.moneyType == 1) ? "vin" : "xu");
        this.executor = (ThreadPoolExecutor) ((moneyType == 1) ? Executors.newFixedThreadPool(ConfigGame.getIntValue("poke_go_thread_pool_per_room_vin")) : ((ThreadPoolExecutor) Executors.newFixedThreadPool(ConfigGame.getIntValue("poke_go_thread_pool_per_room_xu"))));
        this.pot = pot;
        final CacheServiceImpl cacheService = new CacheServiceImpl();
        cacheService.setValue(name, (int) pot);
        this.fund = fund;
        this.betValue = betValue;
        this.initPotValue = initPotValue;
        TimerTask gameLoopTask = new TimerTask() {
            @Override
            public void run() {
                gameLoop();
            }
        };
        new Timer().scheduleAtFixedRate(gameLoopTask, Util.randInt(5_000, 10_000), 1_000);
        try {
            this.countHu = this.sv.getValueInt(name + "_count_hu");
            this.countNoHuX2 = this.sv.getValueInt(name + "_count_no_hu_x2");
            this.calculatHuX2();
        } catch (KeyNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            this.mgService.savePot(name, pot, this.huX2);
        } catch (IOException | TimeoutException | InterruptedException ex2) {
            ex2.printStackTrace();
        }
    }

    public void autoPlay(final User user, final String lines) {
        synchronized (this.usersAuto) {
            if (this.usersAuto.containsKey(user.getName())) {
                final AutoUserPokeGo entry = this.usersAuto.get(user.getName());
                this.forceStopAutoPlay(entry.getUser());
            }
            this.usersAuto.put(user.getName(), new AutoUserPokeGo(user, lines));
        }
    }

    public void stopAutoPlay(final User user) {
        synchronized (this.usersAuto) {
            final AutoUserPokeGo entry;
            if (this.usersAuto.containsKey(user.getName()) && (entry = this.usersAuto.get(user.getName())).getUser().getId() == user.getId()) {
                this.usersAuto.remove(user.getName());
            }
        }
    }

    public void forceStopAutoPlay(final User user) {
        synchronized (this.usersAuto) {
            this.usersAuto.remove(user.getName());
            final ForceStopAutoPlayPokeGoMsg msg = new ForceStopAutoPlayPokeGoMsg();
            this.sendMessageToUser(msg, user);
        }
    }

    public ResultPokeGoMsg play(final String username, final String linesStr) {
        final long startTime = System.currentTimeMillis();
        final String currentTimeStr = DateTimeUtils.getCurrentTime();
        final long refernceId = PokeGoModule.getNewRefenceId();
        short result = 0;
        final int soLanNoHu = ConfigGame.getIntValue("poke_go_so_lan_no_hu");
        final String[] lineArr = linesStr.split(",");
        long currentMoney = this.userService.getMoneyUserCache(username, this.moneyTypeStr);
        final UserCacheModel u = this.userService.forceGetCachedUser(username);
        final long totalBetValue = lineArr.length * this.betValue;
        final ResultPokeGoMsg msg = new ResultPokeGoMsg();
        if (lineArr.length > 0 && !linesStr.isEmpty()) {
            if (totalBetValue > 0L) {
                long minVin = this.userService.getAvailableVinMoney(username);
                if (totalBetValue <= minVin) {
                    MoneyResponse moneyRes = this.userService.updateMoney(username, -totalBetValue, this.moneyTypeStr, Games.POKE_GO.getName(), "Quay Kim Cuong", "\u0110\u1eb7t c\u01b0\u1ee3c Quay Kim Cuong", 0L, refernceId, TransType.START_TRANS);
                    if (moneyRes != null && moneyRes.isSuccess()) {
                        final long fee = totalBetValue * 2L / 100L;
                        final long moneyToPot = totalBetValue / 100L;
                        final long moneyToFund = totalBetValue - fee - moneyToPot;
                        this.fund += moneyToFund;
                        this.pot += moneyToPot;
                        boolean enoughPair = false;
                        final ArrayList<AwardsOnLine> awardsOnLines = new ArrayList<>();
                        long totalPrizes = 0L;
                        Label_0228:
                        while (!enoughPair) {
                            result = 0;
                            awardsOnLines.clear();
                            totalPrizes = 0L;
                            String linesWin = "";
                            String prizesOnLine = "";
                            boolean forceNoHu = false;
                            if (lineArr.length >= 5 && soLanNoHu > 0 && this.fund > this.initPotValue * 2L) {
                                final Random rd = new Random();
                                if (rd.nextInt(soLanNoHu) == 0) {
                                    forceNoHu = true;
                                }
                            }
                            final Item[][] matrix = forceNoHu ? PokeGoUtils.generateMatrixNoHu(lineArr) : PokeGoUtils.generateMatrix();
                            for (final String entry : lineArr) {
                                final ArrayList<Award> awardList = new ArrayList<>();
                                final Line line = PokeGoUtils.getLine(this.lines, matrix, Integer.parseInt(entry));
                                PokeGoUtils.calculateLine(line, awardList);
                                for (final Award award : awardList) {
                                    long money = 0L;
                                    if (award != Award.TRIPLE_POKER_BALL) {
                                        money = (long) (award.getRatio() * this.betValue);
                                    } else {
                                        for (final AwardsOnLine e : awardsOnLines) {
                                            if (e.getAward() != Award.TRIPLE_POKER_BALL) {
                                                continue;
                                            }
                                            continue Label_0228;
                                        }
                                        result = 3;
                                        money = (this.huX2 ? (this.pot * 2L) : this.pot);
                                    }
                                    final AwardsOnLine aol = new AwardsOnLine(award, money, line.getName());
                                    awardsOnLines.add(aol);
                                }
                            }
                            final StringBuilder builderLinesWin = new StringBuilder();
                            final StringBuilder builderPrizesOnLine = new StringBuilder();
                            for (final AwardsOnLine entry2 : awardsOnLines) {
                                if (entry2.getAward() == Award.TRIPLE_POKER_BALL) {
                                    if (!u.isBot()) {
                                        continue Label_0228;
                                    }
                                    totalPrizes += this.pot;
                                } else {
                                    totalPrizes += entry2.getMoney();
                                }
                                builderLinesWin.append(",");
                                builderLinesWin.append(entry2.getLineId());
                                builderPrizesOnLine.append(",");
                                builderPrizesOnLine.append(entry2.getMoney());
                            }
                            if (builderLinesWin.length() > 0) {
                                builderLinesWin.deleteCharAt(0);
                            }
                            if (builderPrizesOnLine.length() > 0) {
                                builderPrizesOnLine.deleteCharAt(0);
                            }
                            if (result == 3) {
                                if (this.fund - this.initPotValue < 0L) {
                                    continue;
                                }
                            } else if (this.fund - totalPrizes < 0L) {
                                continue;
                            }
                            enoughPair = true;
                            if (totalPrizes > 0L) {
                                if (result == 3) {
                                    if (this.huX2) {
                                        totalPrizes += this.pot;
                                        result = 4;
                                    }
                                    this.noHuX2();
                                    if (this.moneyType == 1) {
                                        GameUtils.sendSMSToUser(username, "Chuc mung " + username + " da no hu game Kim Cuong phong " + this.betValue + ". So tien no hu: " + totalPrizes + " Vin");
                                    }
                                    this.pot = this.initPotValue;
                                    this.fund -= this.initPotValue;
                                } else {
                                    this.fund -= totalPrizes;
                                    result = (short) ((totalPrizes >= this.betValue * 100) ? 2 : 1);
                                }
                            }
                            moneyRes = this.userService.updateMoney(username, totalPrizes, this.moneyTypeStr, Games.POKE_GO.getName(), "Quay Kim Cuong", this.buildDescription(totalBetValue, totalPrizes, result), fee, refernceId, TransType.END_TRANS);
                            final long moneyExchange = totalPrizes - this.betValue;
                            if (moneyRes != null && moneyRes.isSuccess()) {
                                currentMoney = moneyRes.getCurrentMoney();
                                if (this.moneyType == 1 && moneyExchange >= BroadcastMessageServiceImpl.MIN_MONEY) {
                                    this.broadcastMsgService.putMessage(Games.POKE_GO.getId(), username, moneyExchange);
                                }
                            }
                            linesWin = builderLinesWin.toString();
                            prizesOnLine = builderPrizesOnLine.toString();
                            msg.matrix = PokeGoUtils.matrixToString(matrix);
                            msg.linesWin = linesWin;
                            msg.prize = totalPrizes;
                            try {
                                this.pgService.logPokeGo(refernceId, username, this.betValue, linesStr, linesWin, prizesOnLine, result, totalPrizes, this.moneyType, currentTimeStr);
                                if (result == 3 || result == 4) {
                                    this.pgService.addTop(username, this.betValue, totalPrizes, this.moneyType, currentTimeStr, result);
                                }
                            } catch (InterruptedException | IOException | TimeoutException ex) {
                                ex.printStackTrace();
                            }
                            this.saveFund();
                            this.savePot();
                        }
                    }
                } else {
                    result = 102;
                }
            } else {
                result = 101;
            }
        } else {
            result = 101;
        }
        msg.result = (byte) result;
        msg.currentMoney = currentMoney;
        final long endTime = System.currentTimeMillis();
        final long handleTime = endTime - startTime;
        final String ratioTime = CommonUtils.getRatioTime(handleTime);
        PokeGoUtils.log(refernceId, username, this.betValue, msg.matrix, result, this.moneyType, handleTime, ratioTime, currentTimeStr);
        return msg;
    }

    public short play(final User user, final String linesStr) {
        final String username = user.getName();
        final ResultPokeGoMsg msg = this.play(username, linesStr);
        this.sendMessageToUser(msg, user);
        return msg.result;
    }

    private void saveFund() {
        final long currentTime = System.currentTimeMillis();
        if (currentTime - this.lastTimeUpdateFundToRoom >= 60000L) {
            try {
                this.mgService.saveFund(this.name, this.fund);
            }
//            catch (IOException | InterruptedException | TimeoutException ex2) {
//                final Exception ex;
//                final Exception e = ex;
//                Debug.trace((Object[])new Object[] { "MINI POKER: update fund poker error ", e.getMessage() });
//            }
            catch (Exception ex2) {
                Debug.trace("MINI POKER: update fund poker error ", ex2.getMessage());
            }
            this.lastTimeUpdateFundToRoom = currentTime;
        }
    }

    private void savePot() {
        final long currentTime = System.currentTimeMillis();
        if (currentTime - this.lastTimeUpdatePotToRoom >= 3000L) {
            final UpdatePotPokeGoMsg msg = new UpdatePotPokeGoMsg();
            msg.value = this.pot;
            msg.x2 = (byte) (this.huX2 ? 1 : 0);
            this.sendMessageToRoom(msg);
            this.lastTimeUpdatePotToRoom = currentTime;
            try {
                this.mgService.savePot(this.name, this.pot, this.huX2);
            }
//            catch (IOException | InterruptedException | TimeoutException ex2) {
//                final Exception ex;
//                final Exception e = ex;
//                Debug.trace((Object[])new Object[] { "MINI POKER: update pot poker error ", e.getMessage() });
//            }
            catch (Exception ex2) {
                Debug.trace("MINI POKER: update pot poker error ", ex2.getMessage());
            }
        }
    }

    public void updatePotToUser(final User user) {
        final UpdatePotPokeGoMsg msg = new UpdatePotPokeGoMsg();
        msg.value = this.pot;
        this.sendMessageToUser(msg, user);
    }

    private boolean checkDieuKienNoHu(final String username) {
        try {
            final UserModel u = this.userService.getUserByUserName(username);
            return u.isBot();
        } catch (Exception u2) {
            return false;
        }
    }

    private void gameLoop() {
        final ArrayList<AutoUserPokeGo> usersPlay = new ArrayList<>();
        synchronized (this.usersAuto) {
            for (final AutoUserPokeGo user : this.usersAuto.values()) {
                final boolean play = user.incCount();
                if (!play) {
                    continue;
                }
                usersPlay.add(user);
            }
        }
        for (int numThreads = usersPlay.size() / 100 + 1, i = 1; i <= numThreads; ++i) {
            final int fromIndex = (i - 1) * 100;
            int toIndex = i * 100;
            if (toIndex > usersPlay.size()) {
                toIndex = usersPlay.size();
            }
            final ArrayList<AutoUserPokeGo> tmp = new ArrayList(usersPlay.subList(fromIndex, toIndex));
            final PlayListPokeGoTask task = new PlayListPokeGoTask(tmp);
            this.executor.execute(task);
        }
        usersPlay.clear();
    }

    public void playListPokeGo(final List<AutoUserPokeGo> users) {
        for (final AutoUserPokeGo user : users) {
            final short result = this.play(user.getUser(), user.getLines());
            if (result == 3 || result == 4 || result == 101 || result == 102 || result == 100) {
                this.forceStopAutoPlay(user.getUser());
            } else if (result == 0) {
                user.setMaxCount(4);
            } else {
                user.setMaxCount(8);
            }
        }
        users.clear();
    }

    @Override
    public boolean joinRoom(final User user) {
        final boolean result = super.joinRoom(user);
        if (result) {
            user.setProperty("MGROOM_POKEGO_INFO", this);
        }
        return result;
    }

    public void startHuX2() {
        Debug.trace(this.gameName + " start hu X2");
        this.countHu = 1;
        this.sv.setValue(this.name + "_count_hu", this.countHu);
        if (this.moneyType == 1 && this.betValue == 100) {
            this.huX2 = true;
        }
    }

    public void stopHuX2() {
        Debug.trace(this.gameName + " stop hu x2");
        this.countHu = -1;
        this.countNoHuX2 = 0;
        this.huX2 = false;
        this.sv.setValue(this.name + "_count_hu", this.countHu);
        this.sv.setValue(this.name + "_count_no_hu_x2", this.countNoHuX2);
    }

    public void noHuX2() {
        if (this.countHu > -1) {
            ++this.countHu;
            this.sv.setValue(this.name + "_count_hu", this.countHu);
            if (this.huX2) {
                ++this.countNoHuX2;
                this.sv.setValue(this.name + "_count_no_hu_x2", this.countNoHuX2);
                Debug.trace(this.gameName + " No hu X2: " + this.countHu + " , huX2= " + this.countNoHuX2);
                if (this.betValue == 100 && this.countNoHuX2 >= 10) {
                    PokeGoModule.stopX2();
                    this.stopHuX2();
                }
                if (this.betValue == 1000 && this.countNoHuX2 >= 1) {
                    PokeGoModule.stopX2();
                    this.stopHuX2();
                }
            }
            this.calculatHuX2();
        }
    }

    private void calculatHuX2() {
        if (this.countHu > -1 && this.moneyType == 1) {
            if (this.betValue == 100) {
                this.huX2 = (this.countHu % 4 == 1 && this.countNoHuX2 < 10);
            } else if (this.betValue == 1000) {
                this.huX2 = (this.countHu == 3 && this.countNoHuX2 < 1);
            }
        }
    }

    private String buildDescription(final long totalBet, final long totalPrizes, final short result) {
        if (totalBet == 0L) {
            return this.resultToString(result) + ": " + totalPrizes;
        }
        return "Quay: " + totalBet + ", " + this.resultToString(result) + ": " + totalPrizes;
    }

    private String resultToString(final short result) {
        switch (result) {
            case 3: {
                return "N\u1ed5 h\u0169";
            }
            case 4: {
                return "N\u1ed5 h\u0169 X2";
            }
            case 1: {
                return "Th\u1eafng";
            }
            case 2: {
                return "Th\u1eafng l\u1edbn";
            }
            default: {
                return "Tr\u01b0\u1ee3t";
            }
        }
    }

    public static class ResultPokeGo {
        public static final short LOI_HE_THONG = 100;
        public static final short DAT_CUOC_KHONG_HOP_LE = 101;
        public static final short KHONG_DU_TIEN = 102;
        public static final short TRUOT = 0;
        public static final short THANG = 1;
        public static final short THANG_LON = 2;
        public static final short NO_HU = 3;
        public static final short NO_HU_X2 = 4;
    }

    private final class PlayListPokeGoTask extends Thread {
        private final List<AutoUserPokeGo> users;

        private PlayListPokeGoTask(final List<AutoUserPokeGo> users) {
            this.users = users;
            this.setName("AutoPlayPokeGo");
        }

        @Override
        public void run() {
            MGRoomPokeGo.this.playListPokeGo(this.users);
        }
    }
}
