// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.entities;

import bitzero.util.common.business.Debug;
import com.vinplay.vbee.common.utils.DateTimeUtils;
import game.utils.ConfigGame;
import game.utils.GameUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class BalanceMoneyTX
{
    private long moneyWin;
    private long preMoneyBet;
    private long moneyBet;
    private long fee;
    private long updateTime;
    private boolean alerted;
    private String dateReset;
    
    public BalanceMoneyTX() {
        this.moneyWin = 0L;
        this.preMoneyBet = 0L;
        this.moneyBet = 0L;
        this.fee = 0L;
        this.alerted = true;
        this.updateTime = System.currentTimeMillis();
    }
    
    public BalanceMoneyTX(final long moneyWin, final long moneyLoss, final long fee, final String dateReset) {
        this.moneyWin = 0L;
        this.preMoneyBet = 0L;
        this.moneyBet = 0L;
        this.fee = 0L;
        this.alerted = true;
        this.moneyWin = moneyWin;
        this.moneyBet = moneyLoss;
        this.preMoneyBet = moneyLoss;
        this.fee = fee;
        this.updateTime = System.currentTimeMillis();
        this.dateReset = dateReset;
    }
    
    public void addBet(final long moneyBet) {
        this.moneyBet -= moneyBet;
    }
    
    public void addWin(final long moneyWin) {
        this.moneyWin += moneyWin;
    }
    
    public void addFee(final long fee) {
        this.fee += fee;
    }
    
    public int isForceBalance(final boolean hasBlackList, final boolean hasWhiteList) {
        Debug.trace("Money win=" + this.moneyWin);
        Debug.trace("Pre Money bet=" + this.preMoneyBet);
        Debug.trace("Money bet=" + this.moneyBet);
        Debug.trace("Money fee=" + this.fee);
        final long revenueUser = this.preMoneyBet + this.moneyWin;
        Debug.trace("revenueUser=" + revenueUser);
        if (revenueUser >= 50000000L) {
            if (!this.alerted) {
                this.alerted = true;
                GameUtils.sendAlertAndCall("Loi nhuan user vuot qua 10 trieu (" + DateTimeUtils.getCurrentTime() + "), value= " + revenueUser);
            }
        }
        else {
            this.alerted = false;
        }
        if (revenueUser > -this.fee * ConfigGame.getFloatValue("tx_min_fee", 1.0f)) {
            return 1;
        }
        if (hasBlackList && revenueUser > -this.fee * ConfigGame.getFloatValue("tx_max_black_list_lost", 10.0f)) {
            return -2;
        }
        if (hasWhiteList) {
            return -3;
        }
        if (revenueUser < -this.fee * ConfigGame.getFloatValue("tx_max_fee", 4.0f) && Math.abs(revenueUser) - this.fee >= ConfigGame.getIntValue("tx_min_money_force_user_win", 30000000)) {
            return -1;
        }
        return 0;
    }
    
    public void startNewRound() {
        this.preMoneyBet = this.moneyBet;
        this.updateTime();
    }
    
    public void updateTime() {
        if (this.updateTime < DateTimeUtils.getStartTimeToDayAsLong()) {
            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
            final Calendar cal;
            final String toDay;
            if ((toDay = sdf.format((cal = Calendar.getInstance()).getTime())).equalsIgnoreCase(this.dateReset)) {
                this.moneyBet = 0L;
                this.moneyWin = 0L;
                this.preMoneyBet = 0L;
                this.fee = 0L;
                final int range = ConfigGame.getIntValue("interval_reset_balance", 10);
                final int currentMonth = cal.get(Calendar.MONTH);
                cal.add(Calendar.DATE, range);
                this.dateReset = sdf.format(cal.getTime());
                final int tmpMonth = cal.get(Calendar.MONTH);
                if (currentMonth != tmpMonth) {
                    final SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/01");
                    this.dateReset = sdf2.format(cal.getTime());
                }
                Debug.trace("Reset balance next date reset= " + this.dateReset + ", range= " + range);
            }
        }
        this.updateTime = System.currentTimeMillis();
    }
    
    public long getTienHeThongDangThua() {
        final long revenueUser = this.preMoneyBet + this.moneyWin;
        return revenueUser + this.fee;
    }
    
    public long getMaxWinUser() {
        final long revenueUser = this.preMoneyBet + this.moneyWin;
        return (long)(Math.abs(revenueUser) - this.fee * ConfigGame.getFloatValue("tx_max_fee", 4.0f) / 2.0f);
    }
    
    public long getFee() {
        return this.fee;
    }
    
    public long getRevenueUser() {
        return this.preMoneyBet + this.moneyWin;
    }
    
    public String getDateReset() {
        return this.dateReset;
    }
    
    public void setDateReset(final String dateReset) {
        this.dateReset = dateReset;
    }
    
    public static class BalanceType
    {
        public static final int RANDOM = 0;
        public static final int HE_THONG_AM = 1;
        public static final int NGUOI_CHOI_AM = -1;
        public static final int BLACK_LIST_THUA = -2;
        public static final int WHITE_LIST_THANG = -3;
    }
}
