package game.modules.minigame.room;

import bitzero.server.BitZeroServer;
import bitzero.server.entities.User;
import bitzero.util.common.business.Debug;
import casio.king365.util.KingUtil;
import com.vinplay.cardlib.models.Card;
import com.vinplay.cardlib.models.Deck;
import com.vinplay.cardlib.models.Rank;
import com.vinplay.dal.service.BroadcastMessageService;
import com.vinplay.dal.service.CaoThapService;
import com.vinplay.dal.service.impl.BroadcastMessageServiceImpl;
import com.vinplay.dal.service.impl.CacheServiceImpl;
import com.vinplay.dal.service.impl.CaoThapServiceImpl;
import com.vinplay.usercore.service.UserService;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.enums.Games;
import com.vinplay.vbee.common.response.MoneyResponse;
import com.vinplay.vbee.common.statics.TransType;
import game.modules.minigame.cmd.send.caothap.*;
import game.modules.minigame.entities.CaoThapInfo;
import game.modules.minigame.entities.MinigameConstant;
import game.modules.minigame.utils.CaoThapUtils;
import game.utils.GameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vn.yotel.yoker.util.Util;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

public class MGRoomCaoThap extends MGRoom {
    private final Logger log = LoggerFactory.getLogger(BitZeroServer.class);
    private long pot;
    private Long fund;
    private float tax;
    private final byte moneyType;
    private String moneyTypeStr;
    private int baseBetValue;
    private final UserService userService;
    private final Map<String, CaoThapInfo> usersCaoThap;
    private final CaoThapService ctService;
    private final BroadcastMessageService broadcastMsgService;

    public MGRoomCaoThap(final String roomName, final byte moneyType, final long pot, final long fund, final int baseBetValue) {
        super(roomName, Games.CAO_THAP);
        this.tax = MinigameConstant.MINIGAME_TAX_VIN;
        this.baseBetValue = 0;
        this.userService = new UserServiceImpl();
        this.usersCaoThap = new ConcurrentHashMap<>();
        this.ctService = new CaoThapServiceImpl();
        this.broadcastMsgService = new BroadcastMessageServiceImpl();
        this.moneyType = moneyType;
        if (moneyType == 1) {
            this.moneyTypeStr = "vin";
            this.tax = MinigameConstant.MINIGAME_TAX_VIN;
        } else if (moneyType == 0) {
            this.moneyTypeStr = "xu";
            this.tax = MinigameConstant.MINIGAME_TAX_XU;
        }
        this.pot = pot;
        this.fund = fund;
        this.baseBetValue = baseBetValue;
        BitZeroServer.getInstance().getTaskScheduler().scheduleAtFixedRate(
                this::gameLoop, Util.randInt(5, 10), 1, TimeUnit.SECONDS);
        BitZeroServer.getInstance().getTaskScheduler().scheduleAtFixedRate(
                this::timeLoop, Util.randInt(5, 10), 5, TimeUnit.SECONDS);
    }

    public void startPlay(final User user, final int betValue, final long referenceId) {
        final StartPlayCaoThapMsg msg = new StartPlayCaoThapMsg();
        if (this.usersCaoThap.containsKey(user.getName())) {
            msg.Error = 1;
            this.sendMessageToUser(msg, user);
            return;
        }
        long minVin = this.userService.getAvailableVinMoney(user.getName());
        if (minVin < betValue) {
            msg.Error = 3;
            this.sendMessageToUser(msg, user);
            return;
        }
        final long moneyToSystem = Math.round(betValue * this.tax / 100.0f);
        final MoneyResponse mnres = this.userService.updateMoney(user.getName(), -betValue, this.moneyTypeStr,
                "CaoThap", "Cao th\u1ea5p: \u0110\u1eb7t c\u01b0\u1ee3c", "Phi\u00ean: " + referenceId + ", B\u01b0\u1edbc: 1",
                moneyToSystem, referenceId, TransType.START_TRANS);
        if (mnres == null || !mnres.isSuccess()) {
            msg.Error = 100;
            this.sendMessageToUser(msg, user);
            return;
        }

        final Deck deck = new Deck();
        deck.shuffle();
        final Card card = deck.deal();
        byte numA = 0;
        if (card.getRank() == Rank.Ace) {
            numA = 1;
        }
        final long moneyToFund = betValue - moneyToSystem;
        this.addFundAndSave(moneyToFund);
        final List<Double> ratioLst = CaoThapUtils.getRatio(deck, card);
        msg.money1 = Math.round(betValue * ratioLst.get(1));
        msg.money2 = betValue;
        msg.money3 = Math.round(betValue * ratioLst.get(0));
        msg.card = (byte) card.getCode();
        msg.currentMoney = mnres.getCurrentMoney();
        msg.referenceId = referenceId;
        final ArrayList<Card> carryCards = new ArrayList<>();
        carryCards.add(card);
        final CaoThapInfo info = new CaoThapInfo(
                user, referenceId, deck, card, (short) 1, (short) 120, betValue, numA, carryCards,
                msg.money1, msg.money3, user.getId());
        this.usersCaoThap.put(user.getName(), info);
        try {
            this.ctService.logCaoThap(
                    referenceId, user.getName(), betValue, (short) 0, -betValue, card.toString(),
                    this.pot, this.fund, this.moneyType, (short) 0, 1);
        } catch (Exception e) {
            Debug.trace("CAO THAP: log cao thap error ", e.getMessage());
        }
        this.sendMessageToUser(msg, user);
    }

    public void play(final User user, final byte choose) {
        final ResultCaoThapMsg msg = new ResultCaoThapMsg();
        if (!this.usersCaoThap.containsKey(user.getName())) {
            return;
        }
        long currentMoney = this.userService.getCurrentMoneyUserCache(user.getName(), this.moneyTypeStr);
        final CaoThapInfo info = this.usersCaoThap.get(user.getName());
        info.setTime((short) 120);
        info.setStep((short) (info.getStep() + 1));
        if ((info.getMoneyUp() == 0 && choose == 1)
                || (info.getMoneyDown() == 0 && choose == 0)) {
            return;
        }

        boolean noHu = false;
        if (this.moneyType == 1 && info.getNumA() == 2) {
            noHu = ((this.baseBetValue < 100000 || this.userService.getTotalRechargeMoney(user.getName()) >= Math.round(this.pot * 0.1))
                    && CaoThapUtils.isDoWithRatio(30.0));
        }

        long fundStep;
        byte numA;
        Deck deck;
        Card card;
        long moneyWin;
        int i = 0;
        short result = 0;
        do {
            fundStep = this.fund;
            numA = info.getNumA();
            if (info.getMoneyUp() == 0L || info.getMoneyDown() == 0L) {
                if (noHu) {
                    deck = info.getDeck();
                    card = deck.deal();
                } else {
                    card = CaoThapUtils.randomNoA(info.getDeck());
                    deck = info.getDeck();
                    deck.popCard(card);
                }
                ++i;
            } else if (i == 0) {
                if (noHu) {
                    deck = info.getDeck();
                    card = deck.deal();
                } else {
                    card = CaoThapUtils.randomNoA(info.getDeck());
                    deck = info.getDeck();
                    deck.popCard(card);
                }
            } else {
                card = CaoThapUtils.randomThua(info.getDeck(), info.getCard(), choose);
                deck = info.getDeck();
                deck.popCard(card);
            }
            moneyWin = 0L;
            if (card.getRank().getRank() == info.getCard().getRank().getRank()) {
                result = 6;
                moneyWin = Math.round((float) (info.getMoney() * 9L / 10L));
            } else if (card.getRank().getRank() > info.getCard().getRank().getRank()) {
                if (choose == 1) {
                    result = 4;
                    moneyWin = info.getMoneyUp();
                } else {
                    result = 5;
                }
            } else if (card.getRank().getRank() < info.getCard().getRank().getRank()) {
                if (choose == 1) {
                    result = 5;
                } else {
                    result = 4;
                    moneyWin = info.getMoneyDown();
                }
            }
            if (result == 4) {
                fundStep = ((info.getStep() > 2) ? (fundStep -= moneyWin - info.getMoney()) : (fundStep -= moneyWin));
            } else if (result == 6) {
                if (info.getStep() > 2) {
                    ++i;
                } else {
                    fundStep -= info.getMoney();
                }
            } else if (result == 5) {
                ++i;
            }
            if (result != 5 && card.getRank() == Rank.Ace) {
                if (++numA != 3) {
                    continue;
                }
                result = 7;
            }
        } while (fundStep <= 0L && ++i <= 1);
        boolean next = false;
        long moneyToUser = 0L;
        if (result == 4) {
            moneyToUser = moneyWin;
            next = true;
            if (info.getStep() > 2) {
                this.addFundAndSave(info.getMoney() - moneyWin);
            } else {
                this.addFundAndSave(-moneyWin);
            }
        } else if (result == 6) {
            moneyToUser = moneyWin;
            this.pot += info.getMoney() - moneyWin;
            this.savePot();
            next = true;
            if (info.getStep() <= 2) {
                this.addFundAndSave(-info.getMoney());
            }
        } else if (result == 5) {
            moneyToUser = 0L;
            if (info.getStep() > 2) {
                this.addFundAndSave(info.getMoney());
            }
            currentMoney = this.userService.getCurrentMoneyUserCache(user.getName(), this.moneyTypeStr);
            this.userService.updateMoney(user.getName(), moneyToUser, this.moneyTypeStr, "CaoThap", "", "", 0L, info.getReferenceId(), TransType.END_TRANS);
        } else if (result == 7) {
            if (info.getMoney() > moneyWin) {
                this.pot += info.getMoney() - moneyWin;
            } else {
                this.addFundAndSave(info.getMoney() - moneyWin);
            }
            moneyToUser = Math.round((float) (this.pot / 2L));
            this.pot -= moneyToUser;
            this.savePot();
            final MoneyResponse mnres = this.userService.updateMoney(user.getName(), moneyToUser += moneyWin, this.moneyTypeStr, "CaoThap", "Cao th\u1ea5p: N\u1ed5 h\u0169", "Phi\u00ean: " + info.getReferenceId() + ", B\u01b0\u1edbc: " + info.getStep(), 0L, info.getReferenceId(), TransType.END_TRANS);
            if (mnres != null && mnres.isSuccess()) {
                if (this.moneyType == 1) {
                    GameUtils.sendSMSToUser(user.getName(), "Chuc mung " + user.getName() + " da no hu game Cao Thap phong " + this.baseBetValue + ". So tien no hu: " + moneyToUser + " Vin");
                    if (moneyToUser >= BroadcastMessageServiceImpl.MIN_MONEY) {
                        this.broadcastMsgService.putMessage(Games.CAO_THAP.getId(), user.getName(), moneyToUser);
                    }
                }
                currentMoney = mnres.getCurrentMoney();
                try {
                    final List<Card> carryCardsNoHu = info.getCarryCards();
                    carryCardsNoHu.add(card);
                    this.ctService.logCaoThapWin(info.getReferenceId(), user.getName(), this.baseBetValue, (short) 7, moneyToUser, CaoThapUtils.getCardStr(carryCardsNoHu), this.moneyType);
                } catch (Exception e) {
                    Debug.trace("CAO THAP: log cao thap error ", e.getMessage());
                }
            }
        }
        try {
            this.ctService.logCaoThap(info.getReferenceId(), user.getName(), info.getMoney(), result, moneyToUser, card.toString(), this.pot, this.fund, this.moneyType, choose, info.getStep());
        } catch (Exception e2) {
            Debug.trace("CAO THAP: log cao thap error ", e2.getMessage());
        }
        final List<Double> ratioLst = CaoThapUtils.getRatio(deck, card);
        msg.money1 = Math.round(moneyToUser * ratioLst.get(1));
        msg.money2 = moneyToUser;
        msg.money3 = Math.round(moneyToUser * ratioLst.get(0));
        msg.card = (byte) card.getCode();
        this.sendMessageToUser(msg, user);
        if (next) {
            info.setDeck(deck);
            info.setCard(card);
            info.setNumA(numA);
            info.setMoney(moneyToUser);
            final List<Card> carryCards = info.getCarryCards();
            carryCards.add(card);
            info.setCarryCards(carryCards);
            info.setMoneyUp(msg.money1);
            info.setMoneyDown(msg.money3);
            this.usersCaoThap.put(user.getName(), info);
        } else {
            final StopPlayCaoThapMsg msgStop = new StopPlayCaoThapMsg();
            msgStop.result = (byte) result;
            msgStop.currentMoney = currentMoney;
            msgStop.moneyExchange = moneyToUser;
            this.usersCaoThap.remove(user.getName());
            this.sendMessageToUser(msgStop, user);
        }
    }

    public void playWinJackpot(final String nickName, final long betValue, final long referenceId){
        KingUtil.printLog("playWinJackpot() nickName: "+nickName+", betValue: "+betValue+", referenceId: "+referenceId);
        long moneyToUser = Math.round((float) (this.pot / 2L));
        this.pot -= moneyToUser;
        this.savePot();
        KingUtil.printLog("playWinJackpot() pot:: "+this.pot+", moneyToUser: "+moneyToUser);
        if (this.moneyType == 1) {
            if (moneyToUser >= BroadcastMessageServiceImpl.MIN_MONEY) {
                KingUtil.printLog("playWinJackpot() send broadCastMsg");
                this.broadcastMsgService.putMessage(Games.CAO_THAP.getId(), nickName, moneyToUser);
            }
        }
        try {
            KingUtil.printLog("playWinJackpot() log");
            this.ctService.logCaoThapWin(referenceId, nickName, this.baseBetValue, (short) 7, moneyToUser, "1,", this.moneyType);
        } catch (Exception e) {
            KingUtil.printException("CAO THAP: log cao thap error ", e);
        }
    }

    public void stopPlay(final User user) {
        final StopPlayCaoThapMsg msg = new StopPlayCaoThapMsg();
        if (!this.usersCaoThap.containsKey(user.getName())) {
            return;
        }
        final CaoThapInfo info = this.usersCaoThap.get(user.getName());
        if (info.getStep() == 1) {
            msg.Error = 2;
            this.sendMessageToUser(msg, user);
            return;
        }
        final MoneyResponse mnres = this.userService.updateMoney(
                user.getName(), info.getMoney(), this.moneyTypeStr, "CaoThap",
                "Cao th\u1ea5p: Tr\u1eadn th\u1eafng", "Phi\u00ean: " + info.getReferenceId() + ", B\u01b0\u1edbc: " + info.getStep(),
                0L, info.getReferenceId(), TransType.END_TRANS);
        if (mnres == null || !mnres.isSuccess()) {
            msg.Error = 100;
            this.sendMessageToUser(msg, user);
            return;
        }
        try {
            this.ctService.logCaoThapWin(info.getReferenceId(), user.getName(), this.baseBetValue, (short) 4, info.getMoney(), CaoThapUtils.getCardStr(info.getCarryCards()), this.moneyType);
        } catch (Exception e) {
            e.printStackTrace();
        }
        msg.result = 4;
        msg.currentMoney = mnres.getCurrentMoney();
        msg.moneyExchange = info.getMoney();
        this.usersCaoThap.remove(user.getName());
        if (this.moneyType == 1 && info.getMoney() >= BroadcastMessageServiceImpl.MIN_MONEY) {
            this.broadcastMsgService.putMessage(Games.CAO_THAP.getId(), user.getName(), info.getMoney());
        }
        this.sendMessageToUser(msg, user);
    }

    public void updatePotToUser(final User user) {
        final UpdatePotCaoThapMsg msg = new UpdatePotCaoThapMsg();
        msg.value = this.pot;
        this.sendMessageToUser(msg, user);
    }

    private synchronized void addFundAndSave(long addValue) {
        this.fund += addValue;
        try {
            this.ctService.updateFundCaoThap(this.name, this.fund);
        } catch (Exception ex2) {
            Debug.trace("CAO THAP: update fund cao thap error ", ex2.getMessage());
        }
    }

    public synchronized void increasePot(long moneyToPot) {
        this.pot += moneyToPot;
    }

    public void savePot() {
        final UpdatePotCaoThapMsg msg = new UpdatePotCaoThapMsg();
        msg.value = this.pot;
        this.sendMessageToRoom(msg);
        try {
            this.ctService.updatePotCaoThap(this.name, this.pot);
            CacheServiceImpl cacheService = new CacheServiceImpl();
            cacheService.setValue(this.name, (int) this.pot);
        } catch (Exception ex2) {
            Debug.trace("CAO THAP: update pot cao thap error ", ex2.getMessage());
        }
    }

    public Map<String, CaoThapInfo> getUsers() {
        return this.usersCaoThap;
    }

    public void gameLoop() {
        this.usersCaoThap.forEach((key, info) -> {
            final short i = Short.parseShort(String.valueOf(info.getTime() - 1));
            info.setTime(i);
            if (i == 0) {
                if (info.getStep() == 1) {
                    byte choose = -1;
                    if (info.getMoneyUp() == 0L) {
                        choose = 0;
                    } else if (info.getMoneyDown() == 0L) {
                        choose = 1;
                    } else {
                        final Random rd = new Random();
                        choose = (byte) rd.nextInt(2);
                    }
                    this.play(info.getUser(), choose);
                } else {
                    this.stopPlay(info.getUser());
                }
            } else {
                this.usersCaoThap.put(key, info);
            }
        });
    }

    public void timeLoop() {
        try {
            this.usersCaoThap.forEach((key, value) -> {
                final UpdateTimeCaoThapMsg msg = new UpdateTimeCaoThapMsg();
                msg.time = value.getTime();
                this.sendMessageToUser(msg, value.getUser());
            });
        } catch (Exception e) {
            Debug.trace("Exception: " + e.getMessage(), e);
        }
    }

    @Override
    public boolean joinRoom(final User user) {
        final boolean result = super.joinRoom(user);
        if (result) {
            user.setProperty("MGROOM_CAO_THAP_INFO", this);
        }
        return result;
    }

    @Override
    public boolean quitRoom(final User user) {
        return super.quitRoom(user);
    }

    public static class ResultCaoThap {
        public static final short LOI_HE_THONG = 100;
        public static final short USER_PLAYING = 1;
        public static final short STEP_ONE = 2;
        public static final short NOT_ENOUGH_MONEY = 3;
        public static final short THANG = 4;
        public static final short THUA = 5;
        public static final short HOA = 6;
        public static final short NO_HU = 7;
    }
}
