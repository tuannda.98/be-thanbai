// 
// Decompiled by Procyon v0.5.36
// 

package game.utils;

import bitzero.server.entities.User;
import bitzero.server.extensions.data.BaseMsg;
import bitzero.util.ExtensionUtility;

import java.util.List;

public class ServerUtil {
    public static void sendMsgToUser(final BaseMsg msg, final String username) {
        final User user = ExtensionUtility.getExtension().getApi().getUserByName(username);
        if (user != null) {
            ExtensionUtility.getExtension().send(msg, user);
        }
    }

    public static void sendMsgToUser(final BaseMsg msg, final User user) {
        if (user != null) {
            ExtensionUtility.getExtension().send(msg, user);
        }
    }

    public static void sendMsgToAllUsers(final BaseMsg msg) {
        final List<User> users = ExtensionUtility.globalUserManager.getAllUsers();
        for (final User user : users) {
            sendMsgToUser(msg, user);
        }
    }
}
