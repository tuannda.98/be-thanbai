// 
// Decompiled by Procyon v0.5.36
// 

package game.utils;

import bitzero.util.common.business.CommonHandle;
import bitzero.util.datacontroller.business.DataController;
import com.google.gson.Gson;

public class DataUtils
{
    public static final Gson gson;
    
    public static Object copyDataFromDB(final String key, final Class theClass) {
        try {
            final String data = (String)DataController.getController().get(key);
            if (data != null) {
                return DataUtils.gson.fromJson(data, theClass);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }
    
    public static void saveToDB(final String key, final Object object, final Class theClass) {
        String data = "";
        try {
            data = DataUtils.gson.toJson(object, theClass);
            DataController.getController().set(key, data);
        }
        catch (Exception e) {
            CommonHandle.writeErrLog(data, e);
        }
    }
    
    static {
        gson = new Gson();
    }
}
