// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.player;

import bitzero.server.core.IBZEvent;
import bitzero.server.entities.User;
import bitzero.server.exceptions.BZException;
import bitzero.server.extensions.BaseClientRequestHandler;
import bitzero.server.extensions.data.DataCmd;
import game.entities.NormalMoneyInfo;
import game.entities.UserScore;
import game.entities.VipMoneyInfo;
import game.eventHandlers.GameEventParam;
import game.eventHandlers.GameEventType;
import game.modules.player.cmd.rev.RevBangXepHang;
import game.modules.player.cmd.send.SendBangXepHang;
import game.modules.player.entities.RankingTableNormal;
import game.modules.player.entities.RankingTableVip;

public class PlayerModule extends BaseClientRequestHandler
{
    public void handleServerEvent(final IBZEvent ibzevent) throws BZException {
        if (ibzevent.getType() == GameEventType.EVENT_ADD_SCORE) {
            final User user = (User)ibzevent.getParameter(GameEventParam.USER);
            final UserScore score = (UserScore)ibzevent.getParameter(GameEventParam.USER_SCORE);
            this.userAddEventScore(user, score);
        }
    }
    
    public void init() {
    }
    
    public void handleClientRequest(final User user, final DataCmd dataCmd) {
        switch (dataCmd.getId()) {
            case 1001: {
                this.getRankingTable(user, dataCmd);
                break;
            }
        }
    }
    
    private void getRankingTable(final User user, final DataCmd dataCmd) {
        final RevBangXepHang cmd = new RevBangXepHang(dataCmd);
        final SendBangXepHang msg = new SendBangXepHang();
        if (cmd.type == 1) {
            msg.type = 1;
            msg.top = RankingTableVip.getIntansce().getTopInfoWinToday();
            this.send(msg, user);
        }
        if (cmd.type == 0) {
            msg.type = 0;
            msg.top = RankingTableNormal.getIntansce().getTopInfoWinToday();
            this.send(msg, user);
        }
    }
    
    private void userAddEventScore(final User user, final UserScore score) {
        final VipMoneyInfo info;
        if (score.moneyType == 1 && (info = VipMoneyInfo.getInfo(user)) != null) {
            info.addScore(score);
            info.save();
        }
        final NormalMoneyInfo info2;
        if (score.moneyType == 0 && (info2 = NormalMoneyInfo.getInfo(user)) != null) {
            info2.addScore(score);
            info2.save();
        }
    }
}
