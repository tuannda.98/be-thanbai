package game;

import bitzero.engine.sessions.ISession;
import bitzero.server.BitZeroServer;
import bitzero.server.core.BZEventType;
import bitzero.server.core.IBZEvent;
import bitzero.server.entities.User;
import bitzero.server.entities.data.ISFSObject;
import bitzero.server.exceptions.BZException;
import bitzero.server.extensions.BZExtension;
import bitzero.server.extensions.data.DataCmd;
import bitzero.util.ExtensionUtility;
import bitzero.util.common.business.Debug;
import bitzero.util.socialcontroller.bean.UserInfo;
import casio.king365.GU;
import casio.king365.core.HCMap;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.hazelcast.core.IMap;
import com.vinplay.dal.service.ServerInfoService;
import com.vinplay.dal.service.impl.ServerInfoServiceImpl;
import com.vinplay.usercore.utils.GameCommon;
import com.vinplay.vbee.common.enums.Platform;
import com.vinplay.vbee.common.hazelcast.HazelcastLoader;
import com.vinplay.vbee.common.models.cache.UserExtraInfoModel;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.pools.ConnectionPool;
import com.vinplay.vbee.common.rmq.RMQApi;
import com.vinplay.vbee.common.utils.DateTimeUtils;
import game.eventHandlers.LoginSuccessHandler;
import game.eventHandlers.UserDisconnectHandler;
import game.modules.admin.AdminModule;
import game.modules.chat.ChatModule;
import game.modules.gameRoom.GameRoomModule;
import game.modules.lobby.LobbyModule;
import game.modules.minigame.*;
import game.modules.minigame.entities.BotMinigame;
import game.modules.mission.MissionModule;
import game.modules.player.PlayerModule;
import game.modules.player.cmd.rev.LoginCmd;
import game.utils.ConfigGame;
import game.utils.GameUtils;
import vn.yotel.yoker.util.Util;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.concurrent.*;

public class BaseGameExtension extends BZExtension {
    private final Runnable reloadConfigTask;
    private final Runnable logCCUTask;
    private int logCCUPeriodInSecond;
    private final ServerInfoService serverInfoSrv;
    private int lastCCU;
    private static int ccuWeb;
    private static int ccuAD;
    private static int ccuIOS;
    private static int ccuWP;
    private static int ccuFB;
    private static int ccuDT;
    private ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(
            50, new ThreadFactoryBuilder().setNameFormat("BaseGame-thread-%d").build());

    public BaseGameExtension() {
        this.serverInfoSrv = new ServerInfoServiceImpl();
        this.logCCUTask = () -> {
            final int ccu = ExtensionUtility.globalUserManager.getUserCount();
            final long ccuGiam = this.lastCCU - ccu;
            if (ccuGiam >= ConfigGame.getIntValue("min_so_ccu_giam", 50)) {
                GameUtils.sendAlertAndCall(
                        "CCU giam " + ccuGiam + " trong " +
                                ConfigGame.getIntValue("update_log_ccu") +
                                " s, time= " + DateTimeUtils.getCurrentTime());
            }
            this.lastCCU = ccu;
            final int ccuOT = ccu - (BaseGameExtension.ccuWeb + BaseGameExtension.ccuAD + BaseGameExtension.ccuIOS + BaseGameExtension.ccuWP + BaseGameExtension.ccuFB + BaseGameExtension.ccuDT);
            this.serverInfoSrv.logCCU(ccu, BaseGameExtension.ccuWeb, BaseGameExtension.ccuAD, BaseGameExtension.ccuIOS, BaseGameExtension.ccuWP, BaseGameExtension.ccuFB, BaseGameExtension.ccuDT, ccuOT);
        };
        this.reloadConfigTask = () -> {
            ConfigGame.reload();
            if (logCCUPeriodInSecond != ConfigGame.getIntValue("update_log_ccu")) {
                BitZeroServer.getInstance().getTaskScheduler().remove(logCCUTask);
            }
        };
        this.lastCCU = 0;
    }


    public void handleClientRequest(short requestId, User sender, DataCmd dataCmd) {
        ScheduledFuture future = scheduledExecutorService.schedule(() -> {
            GU.sendOperation("[CẢNH BÁO - KHẢ NĂNG LỖI]Xử lý command chậm hơn 1s: " + dataCmd.getId() + " - " + sender.getName());
        }, 1, TimeUnit.SECONDS);
        LocalDateTime start = LocalDateTime.now();
        super.handleClientRequest(requestId, sender, dataCmd);
        Duration duration = Duration.between(start, LocalDateTime.now());
        long durationInMs = duration.toMillis();
        if (durationInMs > 200) {
            GU.sendOperation("[CẢNH BÁO]Xử lý command chậm: " + dataCmd.getId() + " - " + sender.getName()
                    + " - " + durationInMs + " ms.");
        }
        if (!future.isDone()) {
            future.cancel(true);
        }
    }

    public void handleServerEvent(IBZEvent event) throws Exception {
        LocalDateTime start = LocalDateTime.now();
        super.handleServerEvent(event);
        Duration duration = Duration.between(start, LocalDateTime.now());
        if (duration.getNano() > 100_000_000) {
            GU.sendOperation("[CẢNH BÁO]Xử lý event chậm: " + event.getType() + " - " + duration.getNano() / 1000 + " ms.");
        }
    }

    public void init() {
        try {
            RMQApi.start("config/rmq.properties");
            HazelcastLoader.start();
            MongoDBConnectionFactory.init();
            ConnectionPool.start("config/db_pool.properties");
            ConfigGame.reload();
            BotMinigame.loadData();
            GameCommon.init();
        } catch (Exception e) {
            Debug.trace("INIT MINIGAME ERROR " + e.getMessage());
        }
        this.addRequestHandler((short) 1000, PlayerModule.class);
        if (GameUtils.gameName.equalsIgnoreCase("Minigame")) {
            this.addRequestHandler((short) 2000, TaiXiuModule.class);
            this.addRequestHandler((short) 4000, MiniPokerModule.class);
            this.addRequestHandler((short) 5000, BauCuaModule.class);
            this.addRequestHandler((short) 6000, CaoThapModule.class);
            this.addRequestHandler((short) 7000, PokeGoModule.class);
            this.addRequestHandler((short) 18000, ChatModule.class);
            this.addRequestHandler((short) 19000, AdminModule.class);
            this.addRequestHandler((short) 20000, LobbyModule.class);
            this.addRequestHandler((short) 22000, MissionModule.class);
        } else {
            this.addRequestHandler((short) 3000, GameRoomModule.class);
        }
        this.addEventHandler(BZEventType.USER_LOGIN, LoginSuccessHandler.class);
        this.addEventHandler(BZEventType.USER_DISCONNECT, UserDisconnectHandler.class);
        this.logCCUPeriodInSecond = ConfigGame.getIntValue("update_log_ccu");
        BitZeroServer.getInstance().getTaskScheduler().scheduleAtFixedRate(
                this.reloadConfigTask, Util.randInt(5, 20), 300, TimeUnit.SECONDS);
        BitZeroServer.getInstance().getTaskScheduler().scheduleAtFixedRate(
                this.logCCUTask, Util.randInt(5, 20), logCCUPeriodInSecond, TimeUnit.SECONDS);
    }

    public void doLogin(final short s, final ISession iSession, final DataCmd dataCmd) throws BZException {
        if (s != 1) {
            return;
        }
        final LoginCmd cmd = new LoginCmd(dataCmd);
        final UserInfo info = GameUtils.getUserInfo(cmd.nickname, cmd.sessionKey);
        final User user;
        if (info != null && (user = ExtensionUtility.instance().canLogin(info, "", iSession)) != null) {
            user.setProperty("dai_ly", info.getStatus());
            this.saveCCUPlatform(user);
        }
    }

    public void doLogin(final ISession iSession, final ISFSObject iSFSObject) {
    }

    public void saveCCUPlatform(final User user) {
        final IMap<String, UserExtraInfoModel> userExtraModel = HCMap.getUserExtraInfoMap();
        if (userExtraModel.containsKey(user.getName())) {
            final UserExtraInfoModel model = userExtraModel.get(user.getName());
            if (model != null && model.getPlatfrom() != null) {
                user.setProperty("pf", model.getPlatfrom());
                final Platform platform = Platform.find(model.getPlatfrom());
                switch (platform) {
                    case WEB: {
                        ++BaseGameExtension.ccuWeb;
                        break;
                    }
                    case ANDROID: {
                        ++BaseGameExtension.ccuAD;
                        break;
                    }
                    case IOS: {
                        ++BaseGameExtension.ccuIOS;
                        break;
                    }
                    case WINPHONE: {
                        ++BaseGameExtension.ccuWP;
                        break;
                    }
                    case FACEBOOK_APP: {
                        ++BaseGameExtension.ccuFB;
                        break;
                    }
                    case DESKTOP: {
                        ++BaseGameExtension.ccuDT;
                        break;
                    }
                }
            }
        } else {
            Debug.trace("Cannot find user's extra info " + user.getName());
        }
    }

    public synchronized static void decreaseCCU(Platform platform) {
        switch (platform) {
            case WEB: {
                --BaseGameExtension.ccuWeb;
                break;
            }
            case ANDROID: {
                --BaseGameExtension.ccuAD;
                break;
            }
            case IOS: {
                --BaseGameExtension.ccuIOS;
                break;
            }
            case WINPHONE: {
                --BaseGameExtension.ccuWP;
                break;
            }
            case FACEBOOK_APP: {
                --BaseGameExtension.ccuFB;
                break;
            }
            case DESKTOP: {
                --BaseGameExtension.ccuDT;
                break;
            }
        }
    }

    static {
        BaseGameExtension.ccuWeb = 0;
        BaseGameExtension.ccuAD = 0;
        BaseGameExtension.ccuIOS = 0;
        BaseGameExtension.ccuWP = 0;
        BaseGameExtension.ccuFB = 0;
        BaseGameExtension.ccuDT = 0;
    }
}
