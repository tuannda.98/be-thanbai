// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.entities.pokego;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Awards
{
    private static List<Award> awards;
    
    public Awards() {
        Awards.awards.addAll(Arrays.asList(Award.values()));
    }
    
    public static List<Award> list() {
        return Awards.awards;
    }
    
    public static Award getAward(final Item item, final int numItems) {
        for (final Award entry : Award.values()) {
            if (entry.getItem() == item && entry.getDuplicate() == numItems) {
                return entry;
            }
        }
        return null;
    }
    
    static {
        Awards.awards = new ArrayList<>();
    }
}
