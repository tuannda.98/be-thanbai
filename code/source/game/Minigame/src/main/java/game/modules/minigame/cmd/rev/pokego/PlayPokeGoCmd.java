// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.rev.pokego;

import java.nio.ByteBuffer;
import bitzero.server.extensions.data.DataCmd;
import bitzero.server.extensions.data.BaseCmd;

public class PlayPokeGoCmd extends BaseCmd
{
    public int betValue;
    public String lines;
    
    public PlayPokeGoCmd(final DataCmd dataCmd) {
        super(dataCmd);
        this.unpackData();
    }
    
    public void unpackData() {
        final ByteBuffer bf = this.makeBuffer();
        this.betValue = bf.getInt();
        this.lines = this.readString(bf);
    }
}
