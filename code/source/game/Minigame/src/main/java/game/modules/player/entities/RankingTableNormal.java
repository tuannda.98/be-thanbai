// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.player.entities;

import bitzero.util.common.business.CommonHandle;
import bitzero.util.datacontroller.business.DataController;
import game.entities.NormalMoneyInfo;
import game.utils.GameUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

public class RankingTableNormal {
    public static final int RANK_USER_COUNT = 10;
    public static final int SORTED_USER_COUNT = 100;
    public final static long UPDATE_INTERVAL = 120000L;
    private static RankingTableNormal ins;
    private String dbKey;
    public Set<NormalMoneyInfo> topInfoList;
    private String topInfoWinTodayString;
    public volatile long lastimeUpdate;
    public volatile boolean isUpdate;

    public static RankingTableNormal getIntansce() {
        if (RankingTableNormal.ins == null) {
            RankingTableNormal.ins = new RankingTableNormal();
        }
        return RankingTableNormal.ins;
    }

    public RankingTableNormal() {
        Comparator<NormalMoneyInfo> moneyWinTodaySortting = (o1, o2) -> Long.compare(o2.moneyWinToday, o1.moneyWinToday);
        this.dbKey = null;
        this.topInfoList = new TreeSet<>(moneyWinTodaySortting);
        this.topInfoWinTodayString = "[{\"m\":996,\"n\":\"vp_5\"}, \n{\"m\":902,\"n\":\"vp_3\"}, \n{\"m\":857,\"n\":\"vp_7\"}, \n{\"m\":838,\"n\":\"vp_8\"}, \n{\"m\":622,\"n\":\"vp_6\"}, \n{\"m\":589,\"n\":\"vp_1\"}, \n{\"m\":318,\"n\":\"vp_2\"}, \n{\"m\":302,\"n\":\"vp_9\"}, \n{\"m\":264,\"n\":\"vp_4\"}, \n{\"m\":100,\"n\":\"vp_0\"}]";
        this.lastimeUpdate = 0L;
        this.isUpdate = false;
        this.saveDB();
        this.loadFromDB();
    }

    public void addTopInfo(final NormalMoneyInfo info) {
        this.topInfoList.add(info);
        this.updateInfo();
    }

    private void updateInfo() {
        if (this.isUpdate) {
            return;
        }
        this.isUpdate = true;
        try {
            final long now = System.currentTimeMillis();
            final long interval = now - this.lastimeUpdate;
            if (interval > 120000L) {
                this.lastimeUpdate = now;
                this.updateValue();
            }
        } catch (Exception e) {
            CommonHandle.writeErrLog(e);
        } finally {
            this.isUpdate = false;
        }
    }

    private void updateValue() {
        final JSONArray arr = new JSONArray();
        int count = 0;
        for (final NormalMoneyInfo info2 : this.topInfoList) {
            if (++count <= 10) {
                try {
                    arr.put(info2.toJSONObject());
                    continue;
                } catch (JSONException e) {
                    CommonHandle.writeErrLog(e);
                    return;
                }
            }
            if (count < 100) {
                continue;
            }
            this.topInfoList.remove(info2);
        }
        this.topInfoWinTodayString = arr.toString();
    }

    public String getTopInfoWinToday() {
        return this.topInfoWinTodayString;
    }

    public void loadFromDB() {
        try {
            this.topInfoWinTodayString = (String) DataController.getController().get(this.getDBKey());
            if (this.topInfoWinTodayString == null) {
                final JSONArray arr = new JSONArray(this.topInfoWinTodayString);
                for (int i = 0; i < arr.length(); ++i) {
                    final JSONObject json = arr.getJSONObject(i);
                    final NormalMoneyInfo info = NormalMoneyInfo.copyFromDB(json.getString("n"));
                    this.topInfoList.add(info);
                }
            }
        } catch (Exception e) {
            CommonHandle.writeErrLog(e);
            this.topInfoWinTodayString = "[]";
            this.topInfoList.clear();
        }
    }

    public void saveDB() {
        try {
            DataController.getController().set(this.getDBKey(), this.topInfoWinTodayString);
        } catch (Exception e) {
            CommonHandle.writeErrLog(e);
        }
    }

    public String getDBKey() {
        if (this.dbKey == null) {
            this.dbKey = GameUtils.gameName + "RankingTable";
        }
        return this.dbKey;
    }

    static {
        RankingTableNormal.ins = null;
    }
}
