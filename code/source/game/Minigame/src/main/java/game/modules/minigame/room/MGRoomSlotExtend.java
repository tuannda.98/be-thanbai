package game.modules.minigame.room;

import bitzero.server.entities.User;
import bitzero.util.common.business.Debug;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.vinplay.dal.service.BroadcastMessageService;
import com.vinplay.dal.service.CacheService;
import com.vinplay.dal.service.MiniGameService;
import com.vinplay.dal.service.impl.BroadcastMessageServiceImpl;
import com.vinplay.dal.service.impl.CacheServiceImpl;
import com.vinplay.dal.service.impl.MiniGameServiceImpl;
import com.vinplay.usercore.dao.impl.UserDaoImpl;
import com.vinplay.usercore.service.UserService;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.enums.Games;
import com.vinplay.vbee.common.exceptions.KeyNotFoundException;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.response.MoneyResponse;
import com.vinplay.vbee.common.statics.TransType;
import com.vinplay.vbee.common.utils.CommonUtils;
import com.vinplay.vbee.common.utils.DateTimeUtils;
import game.SlotExtendService;
import game.SlotExtendServiceImplement;
import game.modules.minigame.GroupItem;
import game.modules.minigame.LineWin;
import game.modules.minigame.Slot3x3ExtendModule;
import game.modules.minigame.cmd.send.slot3x3.ForceStopAutoPlaySlotExtendMsg;
import game.modules.minigame.cmd.send.slot3x3.ResultSlotExtendMsg;
import game.modules.minigame.cmd.send.slot3x3.UpdatePotSlotExtend;
import game.modules.minigame.entities.AutoUserSlotExtend;
import game.modules.minigame.utils.RandomUtil;
import game.modules.minigame.utils.SlotExtendUtils;
import game.utils.ConfigGame;
import vn.yotel.yoker.util.Util;

import java.io.IOException;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeoutException;

public class MGRoomSlotExtend extends MGRoom {
    private long pot;
    private long fund;
    private final long initPotValue;
    private final int betValue;
    private final short moneyType;
    private final String moneyTypeStr;
    private final UserService userService;
    private final MiniGameService mgService;
    private final SlotExtendService slexService;
    private final BroadcastMessageService broadcastMsgService;
    private final Map<String, AutoUserSlotExtend> usersAuto;
    private long lastTimeUpdatePotToRoom;
    private long lastTimeUpdateFundToRoom;
    private final ThreadPoolExecutor executor;
    private final boolean huX2;
    protected final CacheService sv;
    public static final int MINI_GAME_WIN_TYPE_FAIL = 0;
    public static final int MINI_GAME_WIN_TYPE_NORMAL = 1;
    public static final int MINI_GAME_WIN_TYPE_BIG_WIN = 2;
    public static final int MINI_GAME_WIN_TYPE_JACKPOT_BROKEN = 3;
    public static final int MAX_NUMBER_GET_FAIL = 15;
    public static final int DIAMOND_MAX_NUMBER_RANDOM = 5;
    private final int[] arrItemValue;
    private final int[] arrItemValue1;
    private final int[] arrItemValue2;
    private final int[] arrItemValue3;
    private final int[][] retVal;
    final int[][] arrLines;
    final int[] arrMutil;

    public MGRoomSlotExtend(final String name, final short moneyType, final long pot, final long fund, final int betValue, final long initPotValue) {
        // TODO: CHECK GAME AGAIN
        super(name, Games.NOOOOOOOOOOOOOOOOOOOOO_SLOT_EXTEND);
        this.userService = new UserServiceImpl();
        this.mgService = new MiniGameServiceImpl();
        this.slexService = new SlotExtendServiceImplement();
        this.broadcastMsgService = new BroadcastMessageServiceImpl();
        this.arrItemValue = new int[]{1, 2, 3, 4, 5, 6};
        this.arrItemValue1 = new int[]{1, 6, 5, 3, 4, 5, 2, 5, 3, 4, 3, 5, 2, 5, 4, 6, 4, 5, 6, 2};
        this.arrItemValue2 = new int[]{6, 5, 2, 5, 3, 6, 5, 5, 3, 4, 3, 2, 6, 6, 6, 1, 3, 5, 2, 4};
        this.arrItemValue3 = new int[]{3, 5, 2, 5, 3, 6, 5, 2, 6, 4, 4, 5, 5, 6, 6, 3, 6, 2, 1, 5};
        this.retVal = new int[][]{{3, 1, 5, 3, 5, 3, 4, 5, 2}, {3, 1, 5, 3, 5, 5, 4, 5, 2}, {3, 1, 5, 4, 5, 6, 4, 5, 5}, {3, 1, 5, 3, 5, 1, 4, 5, 5}, {3, 1, 5, 3, 6, 5, 4, 5, 5}, {3, 4, 5, 3, 5, 3, 4, 4, 2}, {3, 4, 5, 3, 2, 3, 4, 4, 2}};
        this.arrLines = new int[][]{{0, 1, 2}, {3, 4, 5}, {6, 7, 8}, {0, 7, 2}, {6, 1, 8}, {0, 4, 2}, {0, 4, 8}, {6, 4, 2}, {3, 7, 5}, {3, 1, 5}, {6, 4, 8}, {0, 1, 5}, {3, 4, 8}, {3, 4, 2}, {6, 7, 5}, {3, 1, 2}, {6, 4, 5}, {0, 4, 5}, {3, 7, 8}, {0, 7, 5}, {0, 1, 8}, {0, 7, 8}, {6, 1, 2}, {3, 1, 8}, {3, 7, 2}, {6, 1, 5}, {6, 7, 2}};
        this.arrMutil = new int[]{1, 3, 5, 10};
        this.usersAuto = new HashMap<>();
        this.lastTimeUpdatePotToRoom = 0L;
        this.lastTimeUpdateFundToRoom = 0L;
        int countHu = -1;
        int countNoHuX2 = 0;
        this.huX2 = false;
        this.sv = new CacheServiceImpl();
        this.moneyType = moneyType;
        this.moneyTypeStr = ((this.moneyType == 1) ? "vin" : "xu");
        this.executor = (ThreadPoolExecutor) ((moneyType == 1) ? Executors.newFixedThreadPool(ConfigGame.getIntValue("slot_extend_thread_pool_per_room_vin")) : ((ThreadPoolExecutor) Executors.newFixedThreadPool(ConfigGame.getIntValue("slot_extend_thread_pool_per_room_xu"))));
        this.pot = pot;
        final CacheServiceImpl cacheService = new CacheServiceImpl();
        cacheService.setValue(name, (int) pot);
        this.fund = fund;
        this.betValue = betValue;
        this.initPotValue = initPotValue;
        TimerTask gameLoopTask = new TimerTask() {
            @Override
            public void run() {
                gameLoop();
            }
        };
        new Timer().scheduleAtFixedRate(gameLoopTask, Util.randInt(5_000, 10_000), 1_000);
        try {
            countHu = this.sv.getValueInt(name + "_count_hu");
            countNoHuX2 = this.sv.getValueInt(name + "_count_no_hu_x2");
        } catch (KeyNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            this.mgService.savePot(name, pot, this.huX2);
        } catch (IOException | TimeoutException | InterruptedException ex2) {
            ex2.printStackTrace();
        }
    }

    public void autoPlay(final User user, final long gold) {
        synchronized (this.usersAuto) {
            if (this.usersAuto.containsKey(user.getName())) {
                final AutoUserSlotExtend entry = this.usersAuto.get(user.getName());
                this.forceStopAutoPlay(entry.getUser());
            }
            this.usersAuto.put(user.getName(), new AutoUserSlotExtend(user, gold));
        }
    }

    public void stopAutoPlay(final User user) {
        synchronized (this.usersAuto) {
            if (this.usersAuto.containsKey(user.getName()) && this.usersAuto.get(user.getName()).getUser().getId() == user.getId()) {
                this.usersAuto.remove(user.getName());
            }
        }
    }

    public void forceStopAutoPlay(final User user) {
        synchronized (this.usersAuto) {
            this.usersAuto.remove(user.getName());
            final ForceStopAutoPlaySlotExtendMsg msg = new ForceStopAutoPlaySlotExtendMsg();
            this.sendMessageToUser(msg, user);
        }
    }

    public ResultSlotExtendMsg play(final String username, final long gold) {
        final long startTime = System.currentTimeMillis();
        long refernceId = Slot3x3ExtendModule.getNewRefenceId();
        final String currentTimeStr = DateTimeUtils.getCurrentTime();
        short result = 0;
        long currentMoney = this.userService.getMoneyUserCache(username, this.moneyTypeStr);
        boolean isValid = false;
        long prizeAmount = 0L;
        int i = 0;
        int[] validShowItem = new int[9];
        boolean isJackpotBroken = false;
        List<LineWin> listLineWin = new ArrayList<>();
        final StringBuilder strLineWin = new StringBuilder();
        int rd1 = 1;
        int rd2 = 1;
        int mutil = 1;
        int winType = 0;
        if (gold == this.betValue) {
            if (gold > 0L) {
                long minVin = this.userService.getAvailableVinMoney(username);
                if (gold <= minVin) {
                    MoneyResponse moneyRes = this.userService.updateMoney(username, -gold, this.moneyTypeStr, "SlotExtend", "Quay SlotExtend", "\u00c4\ufffd\u00e1º·t c\u00c6°\u00e1»£c SlotExtend", 0L, refernceId, TransType.START_TRANS);
                    if (moneyRes != null && moneyRes.isSuccess()) {
                        final long fee = gold * 2L / 100L;
                        final long moneyToPot = gold / 100L;
                        final long moneyToFund = gold - fee - moneyToPot;
                        this.fund += moneyToFund;
                        this.pot += moneyToPot;
                        Label_0612:
                        while (!isValid && i < 5) {
                            final int[] showItem = this.initShuffleCollectItem();
                            long checkPrizeAmount = 0L;
                            final List<LineWin> checkLineWin = new ArrayList<>();
                            boolean isJackpot = false;
                            int j = 0;
                            while (j < this.arrLines.length) {
                                rd1 = this.arrMutil[RandomUtil.randInt(4)];
                                rd2 = this.arrMutil[RandomUtil.randInt(4)];
                                mutil = 1;
                                if (rd1 != 1) {
                                    mutil = rd1 * rd2;
                                }
                                final int line = j;
                                final GroupItem groupItem = new GroupItem(this.getItemValueByLineIndex(showItem, line), mutil);
                                if (groupItem.isJackpot()) {
                                    isJackpot = true;
                                    if (this.isJackpotBroken()) {
                                        isJackpotBroken = true;
                                        prizeAmount = this.initPotValue;
                                        final LineWin lineWin = new LineWin();
                                        lineWin.setLine(line);
                                        lineWin.setJackpot(true);
                                        lineWin.setPrizeAmount((double) prizeAmount);
                                        listLineWin.add(lineWin);
                                        strLineWin.append(line).append(",");
                                        validShowItem = showItem;
                                        break Label_0612;
                                    }
                                    break;
                                } else {
                                    if (groupItem.getPrizeAmount() > 0L) {
                                        final long itemPrizeAmount = groupItem.getPrizeAmount();
                                        final long prizeAmount2 = groupItem.getPrizeAmount() * this.betValue;
                                        final LineWin lineWin2 = new LineWin();
                                        lineWin2.setLine(line);
                                        checkPrizeAmount += itemPrizeAmount;
                                        lineWin2.setPrizeAmount((double) prizeAmount2);
                                        checkLineWin.add(lineWin2);
                                    }
                                    ++j;
                                }
                            }
                            if (!isJackpot) {
                                prizeAmount = checkPrizeAmount * this.betValue * mutil;
                                if (prizeAmount >= this.betValue * 100) {
                                    if (prizeAmount <= this.fund && prizeAmount <= this.fund * 50L / 100L) {
                                        validShowItem = showItem;
                                        isValid = true;
                                        listLineWin = checkLineWin;
                                    } else {
                                        prizeAmount = 0L;
                                    }
                                } else if (prizeAmount > 0L && prizeAmount <= this.fund * 80L / 100L) {
                                    validShowItem = showItem;
                                    isValid = true;
                                    listLineWin = checkLineWin;
                                } else {
                                    prizeAmount = 0L;
                                }
                            }
                            ++i;
                        }
                        boolean bigWin = false;
                        if (prizeAmount >= this.betValue * 100L) {
                            bigWin = true;
                        }
                        refernceId = Slot3x3ExtendModule.getNewRefenceId();
                        if (isJackpotBroken) {
                            winType = 3;
                        } else if (isValid && bigWin) {
                            winType = 2;
                        } else if (isValid) {
                            winType = 1;
                        } else {
                            prizeAmount = 0L;
                            winType = 0;
                            validShowItem = this.getCollectItemFail();
                        }
                        moneyRes = this.userService.updateMoney(username, prizeAmount, this.moneyTypeStr, "SlotExtend", "Quay SlotExtend", this.buildDescription(this.betValue, prizeAmount, result), fee, refernceId, TransType.END_TRANS);
                        final double moneyExchange = (double) (prizeAmount - this.betValue);
                        if (moneyRes != null && moneyRes.isSuccess()) {
                            currentMoney = moneyRes.getCurrentMoney();
                            if (this.moneyType == 1 && moneyExchange >= BroadcastMessageServiceImpl.MIN_MONEY) {
                                this.broadcastMsgService.putMessage(31, username, (long) moneyExchange);
                            }
                        }
                        try {
                            this.slexService.logSlotExtend(refernceId, username, this.betValue, "20", Arrays.toString(validShowItem), "prizeAmount", result, prizeAmount, this.moneyType, currentTimeStr);
                            if (result == 3 || result == 4) {
                                final HazelcastInstance client = HazelcastClientFactory.getInstance();
                                final IMap<String, UserModel> userMap = client.getMap("users");
                                UserModel model = null;
                                String displayName = username;
                                if (userMap.containsKey(username)) {
                                    model = userMap.get(displayName);
                                    if (model.getClient() != null && !Objects.equals(model.getClient(), "")) {
                                        displayName = "[" + model.getClient() + "] " + username;
                                    } else {
                                        displayName = "[X] " + username;
                                    }
                                } else {
                                    final UserDaoImpl dao = new UserDaoImpl();
                                    try {
                                        model = dao.getUserByNickName(username);
                                        if (model.getClient() != null && !Objects.equals(model.getClient(), "")) {
                                            displayName = "[" + model.getClient() + "] " + username;
                                        } else {
                                            displayName = "[X] " + username;
                                        }
                                    } catch (SQLException ex) {
                                        ex.printStackTrace();
                                    }
                                }
                                this.slexService.addTop(displayName, this.betValue, prizeAmount, this.moneyType, currentTimeStr, result);
                            }
                        } catch (InterruptedException | IOException | TimeoutException ex2) {
                            ex2.printStackTrace();
                        }
                        this.saveFund();
                        this.savePot();
                    }
                } else {
                    result = 102;
                }
            } else {
                result = 101;
            }
        } else {
            result = 101;
        }
        final long endTime = System.currentTimeMillis();
        final long handleTime = endTime - startTime;
        final String ratioTime = CommonUtils.getRatioTime(handleTime);
        SlotExtendUtils.log(refernceId, username, this.betValue, Arrays.toString(validShowItem), result, this.moneyType, handleTime, ratioTime, currentTimeStr);
        if (isJackpotBroken) {
            this.pot = this.initPotValue;
            this.fund -= this.initPotValue;
        } else {
            this.fund -= prizeAmount;
        }
        return this.sendDiamondNewSpinSuccess(result, (int) refernceId, mutil, rd1, rd2, listLineWin, validShowItem, prizeAmount, winType, currentMoney);
    }

    private ResultSlotExtendMsg sendDiamondNewSpinSuccess(final int result, final int spinId, final int mutil, final int mutil1, final int mutil2, final List<LineWin> listLineWin, final int[] showItem, final long moneyPrize, final int winType, final long currMoney) {
        final ResultSlotExtendMsg res = new ResultSlotExtendMsg();
        res.result = result;
        res.prize = moneyPrize;
        res.mutil = mutil;
        res.mutil1 = mutil1;
        res.mutil2 = mutil2;
        res.winType = winType;
        res.spinId = spinId;
        res.listLineWin = listLineWin;
        res.showItem = showItem;
        res.currMoney = currMoney;
        return res;
    }

    public short play(final User user, final long betvalue) {
        final String username = user.getName();
        final ResultSlotExtendMsg msg = this.play(username, betvalue);
        this.sendMessageToUser(msg, user);
        return (short) msg.result;
    }

    private void saveFund() {
        final long currentTime = System.currentTimeMillis();
        if (currentTime - this.lastTimeUpdateFundToRoom >= 60000L) {
            try {
                this.mgService.saveFund(this.name, this.fund);
            }
//            catch (IOException | InterruptedException | TimeoutException ex2) {
//                final Exception ex;
//                final Exception e = ex;
//                Debug.trace((Object[])new Object[] { "Slot extend: update fund Slot extend error ", e.getMessage() });
//            }
            catch (Exception ex2) {
                Debug.trace("Slot extend: update fund Slot extend error ", ex2.getMessage());
            }
            this.lastTimeUpdateFundToRoom = currentTime;
        }
    }

    private void savePot() {
        final long currentTime = System.currentTimeMillis();
        if (currentTime - this.lastTimeUpdatePotToRoom >= 3000L) {
            final UpdatePotSlotExtend msg = new UpdatePotSlotExtend();
            msg.value1 = this.pot;
            msg.value2 = this.pot;
            msg.value3 = this.pot;
            this.sendMessageToRoom(msg);
            this.lastTimeUpdatePotToRoom = currentTime;
            try {
                this.mgService.savePot(this.name, this.pot, this.huX2);
            }
//            catch (IOException | InterruptedException | TimeoutException ex2) {
//                final Exception ex;
//                final Exception e = ex;
//                Debug.trace((Object[])new Object[] { "Slot extend: update pot Slot extend error ", ex.getMessage() });
//            }
            catch (Exception ex2) {
                Debug.trace("Slot extend: update pot Slot extend error ", ex2.getMessage());
            }
        }
    }

    public void updatePotToUser(final User user) {
        final UpdatePotSlotExtend msg = new UpdatePotSlotExtend();
        msg.value1 = this.pot;
        msg.value2 = this.pot;
        msg.value3 = this.pot;
        this.sendMessageToUser(msg, user);
    }

    private void gameLoop() {
        final ArrayList<AutoUserSlotExtend> usersPlay = new ArrayList<>();
        synchronized (this.usersAuto) {
            for (final AutoUserSlotExtend user : this.usersAuto.values()) {
                final boolean play = user.incCount();
                if (play) {
                    usersPlay.add(user);
                }
            }
        }
        for (int numThreads = usersPlay.size() / 100 + 1, i = 1; i <= numThreads; ++i) {
            final int fromIndex = (i - 1) * 100;
            int toIndex = i * 100;
            if (toIndex > usersPlay.size()) {
                toIndex = usersPlay.size();
            }
            final ArrayList<AutoUserSlotExtend> tmp = new ArrayList(usersPlay.subList(fromIndex, toIndex));
            final PlayListSlotExtendTask task = new PlayListSlotExtendTask(tmp);
            this.executor.execute(task);
        }
        usersPlay.clear();
    }

    public void playListSlotExtend(final List<AutoUserSlotExtend> users) {
        for (final AutoUserSlotExtend user : users) {
            final short result = this.play(user.getUser(), user.getBet());
            if (result == 3 || result == 4 || result == 101 || result == 102 || result == 100) {
                this.forceStopAutoPlay(user.getUser());
            } else if (result == 0) {
                user.setMaxCount(4);
            } else {
                user.setMaxCount(8);
            }
        }
        users.clear();
    }

    @Override
    public boolean joinRoom(final User user) {
        final boolean result = super.joinRoom(user);
        if (result) {
            user.setProperty("MGROOM_SLOT_EXTEND_INFO", this);
        }
        return result;
    }

    private String buildDescription(final long totalBet, final long totalPrizes, final short result) {
        if (totalBet == 0L) {
            return this.resultToString(result) + ": " + totalPrizes;
        }
        return "Quay: " + totalBet + ", " + this.resultToString(result) + ": " + totalPrizes;
    }

    private String resultToString(final short result) {
        switch (result) {
            case 3: {
                return "N\u00e1»\u2022 h\u00c5©";
            }
            case 4: {
                return "N\u00e1»\u2022 h\u00c5© X2";
            }
            case 1: {
                return "Th\u00e1º¯ng";
            }
            case 2: {
                return "Th\u00e1º¯ng l\u00e1»\u203an";
            }
            default: {
                return "Tr\u00c6°\u00e1»£t";
            }
        }
    }

    private int[] getCollectItemJackpot() {
        final int[] collection = new int[9];
        for (int j = 0; j < 15; ++j) {
            for (int i = 0; i < 9; ++i) {
                collection[i] = this.arrItemValue[RandomUtil.randInt(0, this.arrItemValue.length - 1)];
            }
            for (final int[] line : this.arrLines) {
                final int[] arrItem = {collection[line[0]], collection[line[1]], collection[line[2]]};
                final GroupItem groupItem = new GroupItem(arrItem, 100);
                if (groupItem.isJackpot()) {
                    return collection;
                }
            }
        }
        return new int[]{1, 1, 1, 1, 5, 3, 4, 5, 4};
    }

    private boolean isJackpotBroken() {
        return this.fund > this.initPotValue * 2L;
    }

    private int[] initShuffleCollectItem() {
        final int[] collection = new int[9];
        try {
            final int id1 = RandomUtil.randInt(0, this.arrItemValue1.length);
            final int id2 = RandomUtil.randInt(0, this.arrItemValue2.length);
            final int id3 = RandomUtil.randInt(0, this.arrItemValue3.length);
            collection[0] = this.arrItemValue1[id1];
            collection[1] = this.arrItemValue1[(id1 + 1) % this.arrItemValue1.length];
            collection[2] = this.arrItemValue1[(id1 + 2) % this.arrItemValue1.length];
            collection[3] = this.arrItemValue2[id2];
            collection[4] = this.arrItemValue2[(id2 + 1) % this.arrItemValue2.length];
            collection[5] = this.arrItemValue2[(id2 + 2) % this.arrItemValue2.length];
            collection[6] = this.arrItemValue3[id3];
            collection[7] = this.arrItemValue3[(id3 + 1) % this.arrItemValue3.length];
            collection[8] = this.arrItemValue3[(id3 + 2) % this.arrItemValue3.length];
        } catch (Exception e) {
            return new int[]{3, 4, 5, 3, 2, 3, 4, 4, 2};
        }
        return collection;
    }

    private int[] getCollectItemFail() {
        final int[] collection = new int[9];
        for (int j = 0; j < 15; ++j) {
            for (int i = 0; i < 9; ++i) {
                collection[i] = this.arrItemValue[RandomUtil.randInt(0, this.arrItemValue.length - 1)];
            }
            double prizeAmount = 0.0;
            for (final int[] line : this.arrLines) {
                final int[] arrItem = {collection[line[0]], collection[line[1]], collection[line[2]]};
                final GroupItem groupItem = new GroupItem(arrItem);
                if (groupItem.isJackpot()) {
                    ++prizeAmount;
                    break;
                }
                prizeAmount += groupItem.getPrizeAmount();
            }
            if (prizeAmount <= 0.0) {
                return collection;
            }
        }
        final int rd = RandomUtil.randInt(0, 6);
        return this.retVal[rd];
    }

    private int[] getItemValueByLineIndex(final int[] showItem, final int index) {
        final int[] line = this.arrLines[index];
        return new int[]{showItem[line[0]], showItem[line[1]], showItem[line[2]]};
    }

    private final class PlayListSlotExtendTask extends Thread {
        private final List<AutoUserSlotExtend> users;

        private PlayListSlotExtendTask(final List<AutoUserSlotExtend> users) {
            this.users = users;
            this.setName("AutoPlaySlotExtend");
        }

        @Override
        public void run() {
            MGRoomSlotExtend.this.playListSlotExtend(this.users);
        }
    }
}
