package game.modules.minigame.entities;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Vi implements Serializable, Cloneable {

    private static final long serialVersionUID = 2228240771605027317L;
    private final int[] arrValue = new int[]{1, 2, 3, 4, 5, 6};
    //value 1 to 6
    @Expose
    private int[] faces;
    @Expose
    private int point;
    @Expose
    private int matchId;
    @Expose
    private int displayId;

    public Vi() {
        super();
        this.faces = new int[3];
        this.point = 0;
        this.generate();
    }

    public Vi(boolean below) {
        super();
        this.faces = new int[3];
        this.point = 0;
        if (below) this.generateBelow();
        else this.generateAbove();
    }

    public static void main(String[] args) {
        Vi vi = new Vi();
        for (int i = 0; i < 10; i++) {
            vi = new Vi(true);
            System.out.println("vi " + vi.toString());
        }
    }

    public int[] getValues() {
        return faces;
    }

    public void setValues(int[] values) {
        this.faces = values;
    }

    public List<Integer> getSlotBet() {
        List<Integer> pot = new ArrayList<>();
        for (int i = 0; i < faces.length; i++)
            if (!pot.contains(faces[i])) pot.add(faces[i]);
        return pot;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public int[] getFaces() {
        return faces;
    }

    public void setFaces(int[] faces) {
        this.faces = faces;
    }

    public int[] getArrValue() {
        return arrValue;
    }

    public byte randomPot() {
        Random random = new Random();
        int index = random.nextInt(arrValue.length);
        return (byte) arrValue[index];
    }

    public boolean isBelow() {
        if (this.point <= 10) return true;
        return false;
    }

    public boolean isMax() {
        if (this.point > 10) return true;
        return false;
    }

    public int getSameFace() {
        if (this.faces[0] == this.faces[1] && this.faces[1] == this.faces[2]) return 4;
        else if (this.faces[0] == this.faces[1] || this.faces[1] == this.faces[2] || this.faces[0] == this.faces[2])
            return 3;
        return 2;
    }

    private void generate() {
        Random random = new Random();
        int p = 0;
        for (int i = 0; i < 3; i++) {
            int index = random.nextInt(arrValue.length);
            this.faces[i] = arrValue[index];
            p += arrValue[index];
        }
        this.point = p;
    }

    private void generateBelow() {
        Random random = new Random();
        do {
            int p = 0;
            for (int i = 0; i < 3; i++) {
                int index = random.nextInt(arrValue.length);
                this.faces[i] = arrValue[index];
                p += arrValue[index];
            }
            this.point = p;
        } while (this.point > 10);
    }

    private void generateAbove() {
        Random random = new Random();
        do {
            int p = 0;
            for (int i = 0; i < 3; i++) {
                int index = random.nextInt(arrValue.length);
                this.faces[i] = arrValue[index];
                p += arrValue[index];
            }
            this.point = p;
        } while (this.point <= 10);
    }

    public int getMatchId() {
        return matchId;
    }

    public void setMatchId(int matchId) {
        this.matchId = matchId;
    }

    public int getDisplayId() {
        return displayId;
    }

    public void setDisplayId(int displayId) {
        this.displayId = displayId;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.faces[0]);
        builder.append("-");
        builder.append(this.faces[1]);
        builder.append("-");
        builder.append(this.faces[2]);
        builder.append(":");
        builder.append(point);
        return builder.toString();
    }
}