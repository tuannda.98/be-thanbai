// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame;

import bitzero.server.BitZeroServer;
import bitzero.server.core.*;
import bitzero.server.entities.User;
import bitzero.server.exceptions.BZException;
import bitzero.server.extensions.BaseClientRequestHandler;
import bitzero.server.extensions.data.DataCmd;
import bitzero.util.common.business.Debug;
import casio.king365.GU;
import casio.king365.util.KingUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vinplay.dal.entities.config.SlotAutoConfig;
import com.vinplay.dal.service.MiniGameService;
import com.vinplay.dal.service.impl.CacheServiceImpl;
import com.vinplay.dal.service.impl.MiniGameServiceImpl;
import com.vinplay.usercore.service.impl.GameConfigServiceImpl;
import com.vinplay.vbee.common.enums.Games;
import com.vinplay.vbee.common.exceptions.KeyNotFoundException;
import com.vinplay.vbee.common.response.ResultGameConfigResponse;
import com.vinplay.vbee.common.utils.CommonUtils;
import game.modules.minigame.cmd.rev.minipoker.*;
import game.modules.minigame.cmd.send.minipoker.MiniPokerX2Msg;
import game.modules.minigame.entities.BotMinigame;
import game.modules.minigame.room.MGRoom;
import game.modules.minigame.room.MGRoomMiniPoker;
import game.modules.minigame.utils.MiniGameUtils;
import game.utils.ConfigGame;
import game.utils.GameUtils;
import vn.yotel.yoker.util.Util;

import java.sql.SQLException;
import java.time.LocalTime;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class MiniPokerModule extends BaseClientRequestHandler {
    private static Map<String, MGRoom> rooms;
    private final MiniGameService mgService;
    private int countBot100;
    private int countBot1000;
    private int countBot10000;
    private final TimerTask gameLoopTask;
    private static String ngayX2;
    private static Runnable miniPokerX2Task;
    private static String gameName;

    private static final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private static SlotAutoConfig slotAutoConfig;
    public static Calendar calNextTimeWinJackpot;

    public MiniPokerModule() {
        this.mgService = new MiniGameServiceImpl();
        this.countBot100 = 0;
        this.countBot1000 = 0;
        this.countBot10000 = 0;
        this.gameLoopTask = new TimerTask() {
            @Override
            public void run() {
                gameLoop();
            }
        };
    }

    public void init() {
        super.init();
        long[] pots = new long[6];
        long[] funds = new long[6];
        try {
            pots = this.mgService.getPots(Games.MINI_POKER.getName());
            Debug.trace("MINI POKER POTS: " + CommonUtils.arrayLongToString(pots));
            funds = this.mgService.getFunds(Games.MINI_POKER.getName());
            Debug.trace("MINI POKER FUNDS: " + CommonUtils.arrayLongToString(funds));
        } catch (SQLException e) {
            Debug.trace("Get mini poker pot error ", e.getMessage());
        }
        MiniPokerModule.rooms.put(Games.MINI_POKER.getName() + "_vin_100", new MGRoomMiniPoker(MiniPokerModule.gameName + "_vin_100", (short) 1, pots[0], funds[0], 100L, 500000L));
        MiniPokerModule.rooms.put(Games.MINI_POKER.getName() + "_vin_1000", new MGRoomMiniPoker(MiniPokerModule.gameName + "_vin_1000", (short) 1, pots[1], funds[1], 1000L, 5000000L));
        MiniPokerModule.rooms.put(Games.MINI_POKER.getName() + "_vin_10000", new MGRoomMiniPoker(MiniPokerModule.gameName + "_vin_10000", (short) 1, pots[2], funds[2], 10000L, 50000000L));
        MiniPokerModule.rooms.put(Games.MINI_POKER.getName() + "_xu_1000", new MGRoomMiniPoker(MiniPokerModule.gameName + "_xu_1000", (short) 0, pots[3], funds[3], 1000L, 5000000L));
        MiniPokerModule.rooms.put(Games.MINI_POKER.getName() + "_xu_10000", new MGRoomMiniPoker(MiniPokerModule.gameName + "_xu_10000", (short) 0, pots[4], funds[4], 10000L, 50000000L));
        MiniPokerModule.rooms.put(Games.MINI_POKER.getName() + "_xu_100000", new MGRoomMiniPoker(MiniPokerModule.gameName + "_xu_100000", (short) 0, pots[5], funds[5], 100000L, 500000000L));
        Debug.trace("INIT MINI POKER DONE");
        final CacheServiceImpl sv = new CacheServiceImpl();
        try {
            sv.removeKey("mini_poker_last_day_x2");
        } catch (KeyNotFoundException ex) {
            ex.printStackTrace();
        }
        final int lastDayFinish = MiniGameUtils.getLastDayX2("mini_poker_last_day_x2");
        final int[] daysX2 = MiniGameUtils.getX2Days("mini_poker_days_x2");
        MiniPokerModule.ngayX2 = MiniGameUtils.calculateTimeX2AsString(daysX2, lastDayFinish, "mini_poker_time_x2");
        final int nextX2Time = MiniGameUtils.calculateTimeX2(daysX2, lastDayFinish, "mini_poker_time_x2");
        Debug.trace(MiniPokerModule.gameName + " Ngay X2: " + MiniPokerModule.ngayX2 + " remain time= " + nextX2Time);
        if (nextX2Time > 0) {
            BitZeroServer.getInstance().getTaskScheduler().schedule(MiniPokerModule.miniPokerX2Task, nextX2Time, TimeUnit.SECONDS);
        } else {
            startX2();
        }
        this.getParentExtension().addEventListener(BZEventType.USER_DISCONNECT, this);
        BitZeroServer.getInstance().getTaskScheduler().scheduleAtFixedRate(
                () -> this.gameLoop(), Util.randInt(5, 10), 5, TimeUnit.SECONDS);
    }

    public void handleServerEvent(final IBZEvent ibzevent) throws BZException {
        if (ibzevent.getType() == BZEventType.USER_DISCONNECT) {
            final User user = (User) ibzevent.getParameter(BZEventParam.USER);
            this.userDis(user);
        }
    }

    private void userDis(final User user) {
        final MGRoomMiniPoker room = (MGRoomMiniPoker) user.getProperty("MGROOM_MINI_POKER_INFO");
        if (room != null) {
            room.quitRoom(user);
            room.stopAutoPlay(user);
        }
    }

    public void handleClientRequest(final User user, final DataCmd dataCmd) {
        switch (dataCmd.getId()) {
            case 4001: {
                if (GameUtils.disablePlayMiniGame(user)) {
                    return;
                }
                this.playMiniPoker(user, dataCmd);
                break;
            }
            case 4003: {
                this.subScribeMiniPoker(user, dataCmd);
                break;
            }
            case 4004: {
                this.unSubScribeMiniPoker(user, dataCmd);
                break;
            }
            case 4005: {
                this.changeRoom(user, dataCmd);
                break;
            }
            case 4006: {
                if (GameUtils.disablePlayMiniGame(user)) {
                    return;
                }
                this.autoPlay(user, dataCmd);
                break;
            }
        }
    }

    private void subScribeMiniPoker(final User user, final DataCmd dataCmd) {
        final SubscribeMiniPokerCmd cmd = new SubscribeMiniPokerCmd(dataCmd);
        final MGRoomMiniPoker room = this.getRoom(cmd.roomId);
        if (room != null) {
            room.joinRoom(user);
            room.updatePotToUser(user);
            final MiniPokerX2Msg msg = new MiniPokerX2Msg();
            msg.ngayX2 = MiniPokerModule.ngayX2;
            this.send(msg, user);
        } else {
            Debug.trace("MINI POKER SUBSCRIBE: room " + cmd.roomId + " not found");
        }
    }

    private void unSubScribeMiniPoker(final User user, final DataCmd dataCmd) {
        final UnSubscribeMiniPokerCmd cmd = new UnSubscribeMiniPokerCmd(dataCmd);
        final MGRoomMiniPoker room = this.getRoom(cmd.roomId);
        if (room != null) {
            room.stopAutoPlay(user);
            room.quitRoom(user);
        } else {
            Debug.trace("MINI POKER UNSUBSCRIBE: room " + cmd.roomId + " not found");
        }
    }

    private void changeRoom(final User user, final DataCmd dataCmd) {
        final ChangeRoomMiniPokerCmd cmd = new ChangeRoomMiniPokerCmd(dataCmd);
        final MGRoomMiniPoker roomLeaved = this.getRoom(cmd.roomLeavedId);
        final MGRoomMiniPoker roomJoined = this.getRoom(cmd.roomJoinedId);
        if (roomLeaved != null && roomJoined != null) {
            roomLeaved.stopAutoPlay(user);
            roomLeaved.quitRoom(user);
            roomJoined.joinRoom(user);
            roomJoined.updatePotToUser(user);
        } else {
            Debug.trace("MINI POKER: change room error, leaved= " + cmd.roomLeavedId + ", joined= " + cmd.roomJoinedId);
        }
    }

    private void playMiniPoker(final User user, final DataCmd dataCmd) {
        final PlayMiniPokerCmd cmd = new PlayMiniPokerCmd(dataCmd);
        final String roomName = this.getRoomName(cmd.moneyType, cmd.betValue);
        final MGRoomMiniPoker room = (MGRoomMiniPoker) MiniPokerModule.rooms.get(roomName);
        if (room != null) {
            room.play(user, cmd.betValue);
        }
    }

    private void autoPlay(final User user, final DataCmd dataCMD) {
        final AutoPlayMiniPokerCmd cmd = new AutoPlayMiniPokerCmd(dataCMD);
        final MGRoomMiniPoker room = (MGRoomMiniPoker) user.getProperty("MGROOM_MINI_POKER_INFO");
        if (room != null) {
            if (cmd.autoPlay == 1) {
                final short result = room.play(user);
                if (result != 1 && result != 2 && result != 12 && result != 102 && result != 100) {
                    room.autoPlay(user);
                }
            } else {
                room.stopAutoPlay(user);
            }
        }
    }

    private String getRoomName(final short moneyType, final long baseBetting) {
        String moneyTypeStr = "xu";
        if (moneyType == 1) {
            moneyTypeStr = "vin";
        }
        return Games.MINI_POKER.getName() + "_" + moneyTypeStr + "_" + baseBetting;
    }

    private MGRoomMiniPoker getRoom(final byte roomId) {
        final short moneyType = this.getMoneyTypeFromRoomId(roomId);
        final long baseBetting = this.getBaseBetting(roomId);
        final String roomName = this.getRoomName(moneyType, baseBetting);
        return (MGRoomMiniPoker) MiniPokerModule.rooms.get(roomName);
    }

    private short getMoneyTypeFromRoomId(final byte roomId) {
        if (0 <= roomId && roomId < 3) {
            return 1;
        }
        return 0;
    }

    private long getBaseBetting(final byte roomId) {
        switch (roomId) {
            case 0: {
                return 100L;
            }
            case 1: {
                return 1000L;
            }
            case 2: {
                return 10000L;
            }
            case 3: {
                return 1000L;
            }
            case 4: {
                return 10000L;
            }
            case 5: {
                return 100000L;
            }
            default: {
                return 0L;
            }
        }
    }

    public static void startX2() {
        MiniPokerModule.ngayX2 = "";
        final MiniPokerX2Msg msg = new MiniPokerX2Msg();
        msg.ngayX2 = MiniPokerModule.ngayX2;
        for (final MGRoom room : MiniPokerModule.rooms.values()) {
            room.sendMessageToRoom(msg);
        }
    }

    public static void stopX2() {
        MiniPokerModule.ngayX2 = MiniGameUtils.calculateTimeX2AsString(MiniGameUtils.getX2Days("mini_poker_days_x2"), MiniGameUtils.getLastDayX2("mini_poker_last_day_x2"), "mini_poker_time_x2");
        final MiniPokerX2Msg msg = new MiniPokerX2Msg();
        msg.ngayX2 = MiniPokerModule.ngayX2;
        for (final MGRoom room : MiniPokerModule.rooms.values()) {
            room.sendMessageToRoom(msg);
        }
        final Calendar cal = Calendar.getInstance();
        final int today = cal.get(Calendar.DAY_OF_WEEK);
        MiniGameUtils.saveLastDayX2("mini_poker_last_day_x2", today);
        final int lastDayX2 = MiniGameUtils.getLastDayX2("mini_poker_last_day_x2");
        final int nextX2Time = MiniGameUtils.calculateTimeX2(MiniGameUtils.getX2Days("mini_poker_days_x2"), lastDayX2, "mini_poker_time_x2");
        BitZeroServer.getInstance().getTaskScheduler().schedule(MiniPokerModule.miniPokerX2Task, nextX2Time, TimeUnit.SECONDS);
    }

    private int getCountTimeBot100() {
        int n = ConfigGame.getIntValue("mini_poker_bot_100", 0);
        if (n == 0) {
            return 0;
        }
        if (BotMinigame.isNight()) {
            n *= 3;
        }
        return n;
    }

    private int getCountTimeBot1000() {
        int n = ConfigGame.getIntValue("mini_poker_bot_1000", 0);
        if (n == 0) {
            return 0;
        }
        if (BotMinigame.isNight()) {
            n *= 5;
        }
        return n;
    }

    private int getCountTimeBot10000() {
        int n = ConfigGame.getIntValue("poke_go_bot_10000", 0);
        if (n == 0) {
            return 0;
        }
        if (BotMinigame.isNight()) {
            n *= 10;
        }
        return n;
    }

    private SlotAutoConfig getSlotAutoConfig() throws Exception {
        if (slotAutoConfig == null)
            loadSlotAutoConfig();
        return slotAutoConfig;
    }

    private void loadSlotAutoConfig() throws Exception {
        GameConfigServiceImpl gameConfigServiceImpl = new GameConfigServiceImpl();
        ResultGameConfigResponse configObj = gameConfigServiceImpl.getGameConfigAdmin("slot_auto_config", "").get(0);
        if (configObj == null)
            throw new Exception("slot_auto_config gameConfig not found");
        slotAutoConfig = gson.fromJson(configObj.value, SlotAutoConfig.class);
    }

    /**
     * Lấy thời gian trong tương lai mà jackpot sẽ nổ
     *
     * @return
     * @throws Exception
     */
    private Calendar getNextTimeWinJackpot() throws Exception {
        if (calNextTimeWinJackpot == null)
            genNextTimeWinJackpot();
        return calNextTimeWinJackpot;
    }

    /**
     * Tạo random 1 thời điểm trong tương lai sẽ nổ jackpot
     *
     * @throws Exception
     */
    private void genNextTimeWinJackpot() throws Exception {
        Calendar calCurrentTime = Calendar.getInstance();
        int minuteToNextWinJackpot = Util.getRandom(getSlotAutoConfig().getMinMinuteToNextBotWinJackpot(), getSlotAutoConfig().getMaxMinuteToNextBotWinJackpot());
        calCurrentTime.add(Calendar.MINUTE, minuteToNextWinJackpot);
        calCurrentTime.add(Calendar.SECOND, Util.getRandom(1, 59));
        calNextTimeWinJackpot = calCurrentTime;
        KingUtil.printLog("genNextTimeWinJackpot() calNextTimeWinJackpot: " + calNextTimeWinJackpot);
    }

    /**
     * Kiểm tra xem thời điểm này có phải là thời điểm nổ jackpot ko?
     *
     * @return
     */
    private boolean isTimeToWinJackpot() throws Exception {
        Calendar calCurrentTime = Calendar.getInstance();
        return calCurrentTime.get(Calendar.HOUR_OF_DAY) == getNextTimeWinJackpot().get(Calendar.HOUR_OF_DAY)
                && calCurrentTime.get(Calendar.MINUTE) == getNextTimeWinJackpot().get(Calendar.MINUTE);
    }

    private void gameLoop() {
        try {
            // Load auto config mỗi tiếng 1 lần - vào thời điểm 0phút 0s
            if (LocalTime.now().getMinute() == 0 && LocalTime.now().getSecond() == 0) {
                loadSlotAutoConfig();
            }

            // Tăng tiền hũ 100
            SlotAutoConfig config = getSlotAutoConfig();
            final MGRoomMiniPoker room100 = (MGRoomMiniPoker) MiniPokerModule.rooms.get(Games.MINI_POKER.getName() + "_vin_100");
            room100.increasePot(Util.getRandom(config.getSlot100().getMinIncreasePerSecond(), config.getSlot100().getMaxIncreasePerSecond()));
            room100.savePot();
            final MGRoomMiniPoker room1k = (MGRoomMiniPoker) MiniPokerModule.rooms.get(Games.MINI_POKER.getName() + "_vin_1000");
            room1k.increasePot(Util.getRandom(config.getSlot1000().getMinIncreasePerSecond(), config.getSlot1000().getMaxIncreasePerSecond()));
            room1k.savePot();
            final MGRoomMiniPoker room10k = (MGRoomMiniPoker) MiniPokerModule.rooms.get(Games.MINI_POKER.getName() + "_vin_10000");
            room10k.increasePot(Util.getRandom(config.getSlot10000().getMinIncreasePerSecond(), config.getSlot10000().getMaxIncreasePerSecond()));
            room10k.savePot();

            // Xu li auto no hu
            if (isTimeToWinJackpot()) {
                // Xử lí nổ hũ
                // Chọn room sẽ nổ hũ
                Random r = new Random();
                int randomVal = r.nextInt(100);
                if (randomVal < 2) {
                    // 1% Nổ hũ 10k
                    room10k.playWinJackpot(BotMinigame.getBots(1, "vin").get(0), 10000L);
                } else if (randomVal < 5) {
                    // 4% Nổ hũ 1k
                    room1k.playWinJackpot(BotMinigame.getBots(1, "vin").get(0), 1000L);
                } else if (randomVal < 46) {
                    // 45% Nổ hũ 100
                    room100.playWinJackpot(BotMinigame.getBots(1, "vin").get(0), 100L);
                }
                // Khởi tạo lịch nổ hũ lần tiếp theo
                genNextTimeWinJackpot();
            }
        } catch (Exception e) {
            KingUtil.printException("Minipoker Module Exception", e);
            GU.sendOperation("Minipoker Module Exception: " + KingUtil.printException(e));
        }
    }

    static {
        MiniPokerModule.rooms = new HashMap<>();
        MiniPokerModule.miniPokerX2Task = new MiniPokerX2Task();
        MiniPokerModule.gameName = Games.MINI_POKER.getName();
    }

    private static final class MiniPokerX2Task implements Runnable {
        @Override
        public void run() {
            MiniPokerModule.startX2();
            final MGRoomMiniPoker room100 = (MGRoomMiniPoker) MiniPokerModule.rooms.get(Games.MINI_POKER.getName() + "_vin_100");
            room100.startHuX2();
            final MGRoomMiniPoker room101 = (MGRoomMiniPoker) MiniPokerModule.rooms.get(Games.MINI_POKER.getName() + "_vin_1000");
            room101.startHuX2();
            Debug.trace("POKEGO START X2");
        }
    }
}
