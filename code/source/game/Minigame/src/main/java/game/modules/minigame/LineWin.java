// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame;

public class LineWin
{
    private int line;
    private double prizeAmount;
    private boolean isJackpot;
    
    public int getLine() {
        return this.line;
    }
    
    public void setLine(final int line) {
        this.line = line;
    }
    
    public boolean isJackpot() {
        return this.isJackpot;
    }
    
    public void setJackpot(final boolean isJackpot) {
        this.isJackpot = isJackpot;
    }
    
    public double getPrizeAmount() {
        return this.prizeAmount;
    }
    
    public void setPrizeAmount(final double prizeAmount) {
        this.prizeAmount = prizeAmount;
    }
    
    @Override
    public String toString() {
        return "LineWin{line=" + this.line + ",amount=" + this.prizeAmount + ",jp=" + this.isJackpot + '}';
    }
}
