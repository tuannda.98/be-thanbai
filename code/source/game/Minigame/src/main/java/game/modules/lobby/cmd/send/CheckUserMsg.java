// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.lobby.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class CheckUserMsg extends BaseMsgEx
{
    public byte type;
    public byte fee;
    
    public CheckUserMsg() {
        super(20018);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        bf.put(this.type);
        bf.put(this.fee);
        return this.packBuffer(bf);
    }
}
