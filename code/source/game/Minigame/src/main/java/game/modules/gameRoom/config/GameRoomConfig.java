// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.gameRoom.config;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class GameRoomConfig {
    public JSONObject config;
    private static GameRoomConfig gameRoom;

    private GameRoomConfig() {
        this.initconfig();
    }

    public static GameRoomConfig instance() {
        if (GameRoomConfig.gameRoom == null) {
            GameRoomConfig.gameRoom = new GameRoomConfig();
        }
        return GameRoomConfig.gameRoom;
    }

    public void initconfig() {
        final String path = System.getProperty("user.dir");
        final File file = new File(path + "/conf/gameroom.json");
        final StringBuilder contents = new StringBuilder();
        try {
            try (InputStreamReader r = new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8);
                 BufferedReader reader = new BufferedReader(r);) {
                String text = null;
                while ((text = reader.readLine()) != null) {
                    contents.append(text).append(System.getProperty("line.separator"));
                }
            }
            this.config = new JSONObject(contents.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static {
        GameRoomConfig.gameRoom = null;
    }
}
