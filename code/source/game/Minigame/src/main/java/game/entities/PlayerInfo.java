// 
// Decompiled by Procyon v0.5.36
// 

package game.entities;

import bitzero.server.entities.User;
import bitzero.util.ExtensionUtility;
import game.utils.DataUtils;

public class PlayerInfo {
    public static final User.PropertyKey<PlayerInfo> PLAYER_INFO = new User.PropertyKey<>("PLAYER_INFO");

    public String nickName;
    public int userId;
    public final String displayName;
    public final String avatarUrl;
    private static final boolean dependOnGame = false;

    public PlayerInfo() {
        this.nickName = "";
        this.userId = 0;
        this.displayName = "";
        this.avatarUrl = "";
    }

    public void setIsHold(final boolean value) {
        ExtensionUtility.instance().setCache(this.userId, "hold", value);
        if (!value) {
            ExtensionUtility.instance().removeKey(this.userId, "hold");
        }
    }

    public static boolean getIsHold(final int userId) {
        if (userId < 0) {
            return false;
        }
        final Boolean isHold = (Boolean) ExtensionUtility.instance().getCache(userId, "hold");
        return isHold != null && isHold;
    }

    public static void setRoomId(final int uId, final int roomId) {
        if (roomId > 0) {
            ExtensionUtility.instance().setCache(uId, "roomId", roomId);
        }
    }

    public static int getHoldRoom(final int uId) {
        final Integer roomId = (Integer) ExtensionUtility.instance().getCache(uId, "roomId");
        return (roomId == null) ? 0 : roomId;
    }

    public static PlayerInfo copyFromDB(final int userId) {
        return (PlayerInfo) DataUtils.copyDataFromDB(PlayerInfo.class.getName() + userId, PlayerInfo.class);
    }

    public void save() {
        DataUtils.saveToDB(this.getClass().getName() + this.userId, this, this.getClass());
    }

    public static PlayerInfo getInfo(final User user) {
        PlayerInfo pInfo = user.getProperty(PLAYER_INFO);
        if (pInfo == null) {
            pInfo = copyFromDB(user.getId());
            if (pInfo == null) {
                pInfo = new PlayerInfo();
                pInfo.userId = user.getId();
                pInfo.nickName = user.getName();
            }
            user.setProperty(PLAYER_INFO, pInfo);
        }
        return pInfo;
    }
}
