// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.gameRoom;

import game.modules.gameRoom.cmd.send.SendGameRoomConfig;
import game.modules.gameRoom.cmd.send.ReconnectGameRoomFailMsg;
import game.entities.PlayerInfo;
import game.modules.gameRoom.entities.GameMoneyInfo;
import game.modules.gameRoom.entities.GameRoomSetting;
import game.modules.gameRoom.entities.GameRoomGroup;
import game.modules.gameRoom.cmd.send.JoinGameRoomFailMsg;
import game.utils.GameUtils;
import game.modules.gameRoom.cmd.rev.JoinGameRoomCmd;
import game.modules.gameRoom.entities.GameServer;
import bitzero.server.extensions.data.DataCmd;
import bitzero.server.exceptions.BZException;
import bitzero.server.core.BZEventParam;
import bitzero.util.common.business.Debug;
import game.modules.gameRoom.entities.GameRoom;
import game.eventHandlers.GameEventParam;
import bitzero.server.entities.User;
import bitzero.server.core.IBZEvent;
import game.eventHandlers.GameEventType;
import bitzero.server.core.BZEventType;
import game.modules.gameRoom.entities.GameRoomManager;
import com.vinplay.vbee.common.rmq.RMQApi;
import com.vinplay.vbee.common.hazelcast.HazelcastLoader;
import bitzero.server.extensions.BaseClientRequestHandler;

import static game.modules.gameRoom.GameRoomModule.GAME_ROOM;

public class GameRoomModule extends BaseClientRequestHandler
{
    public static final User.PropertyKey<Long> LAST_CHAT_TIME = new User.PropertyKey<>("last_chat_time");
    public static final User.PropertyKey<GameRoom> GAME_ROOM = new User.PropertyKey<>("GAME_ROOM");

    public void init() {
        try {
            HazelcastLoader.start();
            RMQApi.start("config/rmq.properties");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        GameRoomManager.instance();
        this.getParentExtension().addEventListener(BZEventType.USER_DISCONNECT, this);
        this.getParentExtension().addEventListener(GameEventType.GAME_ROOM_USER_JOIN, this);
        this.getParentExtension().addEventListener(GameEventType.GAME_ROOM_USER_LEAVE, this);
    }
    
    public void handleServerEvent(final IBZEvent ibzevent) throws BZException {
        if (ibzevent.getType() == GameEventType.GAME_ROOM_USER_JOIN) {
            final User user = (User)ibzevent.getParameter(GameEventParam.USER);
            final GameRoom room = (GameRoom)ibzevent.getParameter(GameEventParam.GAMEROOM);
            final Boolean isReconnect = (Boolean)ibzevent.getParameter(GameEventParam.IS_RECONNECT);
            this.userJoinRoomSuccess(user, room, isReconnect);
        }
        if (ibzevent.getType() == GameEventType.GAME_ROOM_USER_LEAVE) {
            final User user = (User)ibzevent.getParameter(GameEventParam.USER);
            final GameRoom room = (GameRoom)ibzevent.getParameter(GameEventParam.GAMEROOM);
            Debug.trace("Event leave room", user.getName());
            this.userLeaveRoom(user, room);
        }
        if (ibzevent.getType() == BZEventType.USER_DISCONNECT) {
            final User user = (User)ibzevent.getParameter(BZEventParam.USER);
            this.userDisconnected(user);
        }
    }
    
    public void handleClientRequest(final User user, final DataCmd dataCmd) {
        switch (dataCmd.getId()) {
            case 3001: {
                this.joinGameRoom(user, dataCmd);
                break;
            }
            case 3002: {
                this.reconnectGameRoom(user, dataCmd);
                break;
            }
            case 3003: {
                this.requestConfig(user, dataCmd);
                break;
            }
            default: {
                final GameRoom room = user.getProperty(GAME_ROOM);
                if (room != null) {
                    final GameServer gs = room.getGameServer();
                    gs.onGameMessage(user, dataCmd);
                    break;
                }
                Debug.trace("Room null ", user.getName());
                break;
            }
        }
    }
    
    public void joinGameRoom(final User user, final DataCmd dataCmd) {
        final JoinGameRoomCmd cmd = new JoinGameRoomCmd(dataCmd);
        final boolean check = GameUtils.infoCheck(user);
        if (!check) {
            final JoinGameRoomFailMsg msg = new JoinGameRoomFailMsg();
            msg.Error = 1;
            this.send(msg, user);
            return;
        }
        final GameRoomGroup group = GameRoomManager.instance().getGroup(cmd);
        if (group == null) {
            Debug.trace("Khong ton tai ban choi thoa man", cmd.moneyType, cmd.maxUserPerRoom, cmd.moneyBet);
            final JoinGameRoomFailMsg msg2 = new JoinGameRoomFailMsg();
            msg2.Error = 2;
            this.send(msg2, user);
            return;
        }
        final boolean result = this.preJoinRoom(user, group.setting);
        if (!result) {
            Debug.trace("User khong du dieu kien vao ban choi");
            final JoinGameRoomFailMsg msg3 = new JoinGameRoomFailMsg();
            msg3.Error = 3;
            this.send(msg3, user);
            return;
        }
        final int res = group.joinRoom(user);
        if (res != 0) {
            final JoinGameRoomFailMsg msg4 = new JoinGameRoomFailMsg();
            msg4.Error = 4;
            this.send(msg4, user);
        }
    }
    
    public boolean preJoinRoom(final User user, final GameRoomSetting setting) {
        final GameMoneyInfo moneyInfo = new GameMoneyInfo(user, setting);
        final boolean result = moneyInfo.startGameUpdateMoney();
        if (result) {
            user.setProperty(GameMoneyInfo.GAME_MONEY_INFO, moneyInfo);
            return true;
        }
        return false;
    }
    
    public void userJoinRoomSuccess(final User user, final GameRoom room, final boolean isReconnect) {
        final GameServer gs = room.getGameServer();
        if (isReconnect) {
            gs.onGameUserReturn(user);
        }
        else {
            gs.onGameUserEnter(user);
        }
    }
    
    public void userLeaveRoom(final User user, final GameRoom room) {
        final GameServer gs = room.getGameServer();
        gs.onGameUserExit(user);
        Debug.trace("userLeaveRoom", user.getName(), room.getId());
    }
    
    public void userDisconnected(final User user) {
        final GameRoom room = user.getProperty(GAME_ROOM);
        if (room != null) {
            final GameServer gs = room.getGameServer();
            gs.onGameUserExit(user);
            Debug.trace("userDisconnected", user.getName(), room.getId());
        }
    }
    
    private void reconnectGameRoom(final User user, final DataCmd dataCmd) {
        final boolean isHold = PlayerInfo.getIsHold(user.getId());
        if (isHold) {
            final int roomId = PlayerInfo.getHoldRoom(user.getId());
            final GameRoom room = GameRoomManager.instance().getGameRoomById(roomId);
            if (room != null) {
                room.group.joinRoom(user, room, true);
            }
        }
        else {
            final ReconnectGameRoomFailMsg msg = new ReconnectGameRoomFailMsg();
            this.send(msg, user);
        }
    }
    
    private void requestConfig(final User user, final DataCmd dataCmd) {
        final SendGameRoomConfig msg = new SendGameRoomConfig();
        this.send(msg, user);
    }
}
