package game.modules.minigame.entities.pokego;

public class Cell
{
    private int row;
    private int col;
    private Item item;
    
    public Cell(final int row, final int col) {
        this.item = Item.NONE;
        this.row = row;
        this.col = col;
    }
    
    public int getRow() {
        return this.row;
    }
    
    public void setRow(final int row) {
        this.row = row;
    }
    
    public int getCol() {
        return this.col;
    }
    
    public void setCol(final int col) {
        this.col = col;
    }
    
    public Item getItem() {
        return this.item;
    }
    
    public void setItem(final Item item) {
        this.item = item;
    }
}
