// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.player.cmd.rev;

import java.nio.ByteBuffer;
import bitzero.server.extensions.data.DataCmd;
import bitzero.server.extensions.data.BaseCmd;

public class RevBangXepHang extends BaseCmd
{
    public int type;
    
    public RevBangXepHang(final DataCmd dataCmd) {
        super(dataCmd);
        this.type = 1;
        this.unpackData();
    }
    
    public void unpackData() {
        final ByteBuffer bf = this.makeBuffer();
        this.type = this.readByte(bf);
    }
}
