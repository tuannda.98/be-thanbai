// 
// Decompiled by Procyon v0.5.36
// 

package game.utils;

import bitzero.server.config.ConfigHandle;
import bitzero.server.entities.User;
import bitzero.util.socialcontroller.bean.UserInfo;
import casio.king365.GU;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.enums.Games;
import com.vinplay.vbee.common.response.UserResponse;
import game.entities.PlayerInfo;
import game.entities.UserScore;

import java.util.Objects;
import java.util.Random;

public class GameUtils {
    public static boolean dev_mod;
    public static boolean isCheat;
    public static final String gameName;
    public static final String gameServerClassPath = ConfigHandle.instance().get("gameserver.main.path");

    public static boolean infoCheck(final User user) {
        final PlayerInfo pInfo = PlayerInfo.getInfo(user);
        return pInfo != null && !PlayerInfo.getIsHold(pInfo.userId);
    }

    public static void setUserScore(final int userId, final UserScore score, final boolean updateDatabase) {
    }

    public static UserInfo getUserInfo(final String username, final String sessionKey) {
        if (GameUtils.dev_mod) {
            final UserInfo info = new UserInfo();
            int userId = 0;
            try {
                userId = Integer.valueOf(username);
            } catch (Exception e) {
                final Random rd = new Random();
                userId = Math.abs(rd.nextInt() % 100000);
            }
            info.setUsername("vp_" + userId);
            info.setUserId("" + userId);
            return info;
        }
        final UserServiceImpl service = new UserServiceImpl();
        final UserResponse res = service.checkSessionKey(username, sessionKey, Games.MINIGAME);
        if (Objects.equals(res.getErrorCode(), "0")) {
            res.getUser().getId();
            final UserInfo info2 = new UserInfo();
            info2.setUserId(String.valueOf(res.getUser().getId()));
            info2.setUsername(res.getUser().getNickname());
            info2.setStatus(String.valueOf(res.getUser().getDaily()));
            return info2;
        }
        return null;
    }

    public static void sendAlert(final String msg) {
        GU.sendOperation(msg);
    }

    public static void sendAlertAndCall(final String msg) {
        GU.sendOperation(msg);
    }

    public static void sendSMSToUser(final String username, final String message) {
//        if (ConfigGame.getIntValue("enable_alert_msg", 0) == 1) {
//            try {
//                final AlertServiceImpl alert = new AlertServiceImpl();
//                alert.sendSMS2User(username, message);
//            } catch (Exception ex) {
//                ex.printStackTrace();
//            }
//        }
    }

    public static boolean disablePlayMiniGame(final User user) {
        final String status = (String) user.getProperty("dai_ly");
        final int daily;
        return status != null && !status.isEmpty() && (daily = Integer.parseInt(status)) > 0;
    }

    static {
        GameUtils.dev_mod = (ConfigHandle.instance().getLong("dev_mod") == 1L);
        GameUtils.isCheat = (ConfigHandle.instance().getLong("isCheat") == 1L);
        gameName = ConfigHandle.instance().get("games");
    }
}
