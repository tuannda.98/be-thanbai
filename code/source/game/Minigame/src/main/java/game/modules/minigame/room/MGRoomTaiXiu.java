package game.modules.minigame.room;

import bitzero.server.entities.User;
import bitzero.util.common.business.Debug;
import casio.king365.GU;
import casio.king365.core.HCMap;
import casio.king365.util.KingUtil;
import com.hazelcast.core.IMap;
import com.vinplay.dal.dao.LogMoneyUserDao;
import com.vinplay.dal.dao.impl.LogMoneyUserDaoImpl;
import com.vinplay.dal.entities.report.ReportMoneySystemModel;
import com.vinplay.dal.entities.taixiu.ResultTaiXiu;
import com.vinplay.dal.entities.taixiu.TransactionTaiXiu;
import com.vinplay.dal.entities.taixiu.TransactionTaiXiuDetail;
import com.vinplay.dal.service.BroadcastMessageService;
import com.vinplay.dal.service.TaiXiuService;
import com.vinplay.dal.service.impl.AgentServiceImpl;
import com.vinplay.dal.service.impl.BroadcastMessageServiceImpl;
import com.vinplay.dal.service.impl.TaiXiuServiceImpl;
import com.vinplay.usercore.service.UserService;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.enums.Games;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.response.AgentResponse;
import com.vinplay.vbee.common.response.LogUserMoneyResponse;
import com.vinplay.vbee.common.response.MoneyResponse;
import com.vinplay.vbee.common.statics.TransType;
import game.modules.minigame.cmd.rev.BetTaiXiuCmd;
import game.modules.minigame.cmd.send.*;
import game.modules.minigame.entities.BalanceMoneyTX;
import game.modules.minigame.entities.MinigameConstant;
import game.modules.minigame.entities.PotTaiXiu;
import game.modules.minigame.utils.RutLocUtils;
import game.modules.minigame.utils.TaiXiuUtils;
import game.utils.ConfigGame;

import java.util.*;

public class MGRoomTaiXiu extends MGRoom {
    public long referenceId;
    private final short moneyType;
    private String moneyTypeStr;
    private final PotTaiXiu potTai;
    private final PotTaiXiu potXiu;
    private long startTime;
    private short result;
    public boolean bettingRound;
    public boolean enableBetting;
    private ResultTaiXiu resultTX;
    private final TaiXiuService api;
    private final UserService userService;
    private final BroadcastMessageService broadcastMsgService;
    private float tax;
    private BalanceMoneyTX balance;
    private long blackListBetTai;
    private long blackListBetXiu;
    private long whiteListBetTai;
    private long whiteListBetXiu;
    public long nguoichoidatTai;
    public long nguoichoidatXiu;

    public MGRoomTaiXiu(final String name, final long referenceId, final short moneyType) {
        super(name, Games.TAI_XIU);
        this.startTime = 0L;
        this.result = -1;
        this.bettingRound = false;
        this.enableBetting = false;
        this.api = new TaiXiuServiceImpl();
        this.userService = new UserServiceImpl();
        this.broadcastMsgService = new BroadcastMessageServiceImpl();
        this.tax = MinigameConstant.MINIGAME_TAX_VIN;
        this.balance = new BalanceMoneyTX();
        this.blackListBetTai = 0L;
        this.blackListBetXiu = 0L;
        this.whiteListBetTai = 0L;
        this.whiteListBetXiu = 0L;
        this.nguoichoidatTai = 0L;
        this.nguoichoidatXiu = 0L;
        this.moneyType = moneyType;
        this.moneyTypeStr = "xu";
        if (moneyType == 1) {
            this.moneyTypeStr = "vin";
            this.tax = MinigameConstant.MINIGAME_TAX_VIN;
            final ReportMoneySystemModel model = this.api.getReportTX(ConfigGame.getIntValue("interval_reset_balance", 10));
            if (model != null) {
                this.balance = new BalanceMoneyTX(model.moneyWin, model.moneyLost, model.fee, model.dateReset);
                Debug.trace("TAI XIU VIN, win=" + model.moneyWin + ", loss=" + model.moneyLost + ", fee= " + model.fee + ", date reset= " + model.dateReset);
            }
        } else {
            this.tax = MinigameConstant.MINIGAME_TAX_XU;
        }
        this.potTai = new PotTaiXiu();
        this.potXiu = new PotTaiXiu();
        this.referenceId = referenceId;
    }

    public void startNewGame(final long newReferenceId) {
        this.referenceId = newReferenceId;
        this.bettingRound = true;
        this.enableBetting = true;
        this.blackListBetTai = 0L;
        this.blackListBetXiu = 0L;
        this.whiteListBetTai = 0L;
        this.whiteListBetXiu = 0L;
        this.nguoichoidatTai = 0L;
        this.nguoichoidatXiu = 0L;
        this.potTai.renew();
        this.potXiu.renew();
        this.startTime = System.currentTimeMillis();
        Debug.trace(gameName + " type " + moneyType + ": START NEW ROUND " + this.referenceId);
    }

    public void finish() {
        this.resultTX = null;
        this.startTime = System.currentTimeMillis();
        this.bettingRound = false;
    }

    public void disableBetting() {
        this.enableBetting = false;
    }

    public void updateResultDices(final short[] dices, final short result) {
        this.result = result;
        final UpdateResultDicesMsg msg = new UpdateResultDicesMsg();
        msg.result = result;
        msg.dice1 = dices[0];
        msg.dice2 = dices[1];
        msg.dice3 = dices[2];
        this.resultTX = new ResultTaiXiu();
        this.resultTX.referenceId = this.referenceId;
        this.resultTX.dice1 = msg.dice1;
        this.resultTX.dice2 = msg.dice2;
        this.resultTX.dice3 = msg.dice3;
        this.resultTX.result = msg.result;
        this.resultTX.moneyType = this.moneyType;
        this.sendMessageToRoom(msg);
    }

    public short getRemainTime() {
        final long currentTime = System.currentTimeMillis();
        int remainTime = (int) ((currentTime - this.startTime) / 1000L);
        if (remainTime < 0) {
            remainTime = 0;
        } else if (remainTime > 60) {
            remainTime = 60;
        }
        if (this.bettingRound) {
            return (short) (60 - remainTime);
        }
        return (short) (30 - remainTime);
    }

    public void betTaiXiu(final User user, final BetTaiXiuCmd cmd) {
        if (!this.enableBetting) return;
        final BetTaiXiuMsg msg = this.betTaiXiu(user.getName(), cmd.userId, cmd.betValue, cmd.inputTime, cmd.moneyType, cmd.betSide, false);
        this.sendMessageToUser(msg, user);
    }

    private boolean CheckQuota(final String nick_name, final boolean seven_days) {
        try {
            final UserModel userModel = this.userService.getUserByNickName(nick_name);
            if (userModel != null) {
                if (userModel.getDaily() == 1 || userModel.getDaily() == 2) {
                    return true;
                }
                final LogMoneyUserDao logService = new LogMoneyUserDaoImpl();
                long total_giftcode_money = 0L;
                long total_user_receive = 0L;
                long total_agency_receive = 0L;
                final long total_user_transfer = 0L;
                final long total_agency_transfer = 0L;
                final AgentServiceImpl service = new AgentServiceImpl();
                final List<AgentResponse> agents = service.listAgent();
                final ArrayList<String> agentNames = new ArrayList<>();
                if (agents != null && agents.size() > 0) {
                    for (final AgentResponse agent : agents) {
                        agentNames.add(agent.nickName);
                    }
                }
                final List<LogUserMoneyResponse> resultGiftCode = logService.searchAllLogMoneyUser(nick_name, "GIFTCODE", seven_days);
                if (resultGiftCode != null && resultGiftCode.size() > 0) {
                    total_giftcode_money = resultGiftCode.stream().map(trans -> trans.moneyExchange).reduce(total_giftcode_money, Long::sum);
                }
                final List<LogUserMoneyResponse> resulReceive = logService.searchAllLogMoneyUser(nick_name, "RECEIVE", seven_days);
                if (resulReceive != null && resulReceive.size() > 0) {
                    for (final LogUserMoneyResponse trans2 : resulReceive) {
                        boolean matchAgent = false;
                        for (final String s : agentNames) {
                            if (trans2.description.contains(s)) {
                                matchAgent = true;
                                break;
                            }
                        }
                        if (matchAgent) {
                            total_agency_receive += trans2.moneyExchange;
                        } else {
                            total_user_receive += trans2.moneyExchange;
                        }
                    }
                }
                long total_recharge_card_money = 0L;
                final List<LogUserMoneyResponse> resultCard = logService.searchAllLogMoneyUser(nick_name, "CARD", seven_days);
                if (resultCard != null && resultCard.size() > 0) {
                    total_recharge_card_money = resultCard.stream().map(trans -> trans.moneyExchange).reduce(total_recharge_card_money, Long::sum);
                }
                boolean isValid = false;
                if (total_recharge_card_money + total_agency_receive >= 100000L || userModel.getVinTotal() > 100000L) {
                    isValid = true;
                }
                return isValid;
            }
        } catch (Exception ex) {
            Debug.trace("Check quote error : " + ex.getMessage());
        }
        return false;
    }

    public BetTaiXiuMsg betTaiXiu(final String nickname, final int userId, final long betValue, short inputTime, final short moneyType, final short betSide, boolean isBot) {
        if (!isBot && moneyType == 1) {
            if (betSide == 1) {
                this.nguoichoidatTai += betValue;
            } else {
                this.nguoichoidatXiu += betValue;
            }
        }
        inputTime = this.getRemainTime();
        long currentMoney = this.userService.getMoneyUserCache(nickname, this.moneyTypeStr);
        long minVin = this.userService.getAvailableVinMoney(nickname);
        int result = 2;
        if (this.enableBetting) {
            if (betValue >= 100L) {
                if (betValue > minVin) {
                    result = 3;
                } else {
                    final TransactionTaiXiuDetail transTX = new TransactionTaiXiuDetail(this.referenceId, userId, nickname, betValue, betSide, inputTime, moneyType);
                    if ((betSide == 1 && this.potXiu.getTotalBetByUsername(nickname) > 0L) || (betSide == 0 && this.potTai.getTotalBetByUsername(nickname) > 0L)) {
                        result = 5;
                    } else {
                        final String betSideStr = (betSide == 0) ? "X\u1ec9u" : "T\u00e0i";
                        final MoneyResponse res = this.userService.updateMoney(nickname, -betValue, this.moneyTypeStr, "TaiXiu", "T\u00e0i x\u1ec9u Đu Dây: \u0110\u1eb7t c\u01b0\u1ee3c", "Phii\u00ean " + this.referenceId + ": \u0111\u1eb7t " + betSideStr + " (" + inputTime + ")", 0L, this.referenceId, TransType.START_TRANS);
                        if (res.isSuccess()) {
                            if (!this.enableBetting) {
                                result = 1;
                                this.userService.updateMoney(nickname, betValue, this.moneyTypeStr, "TaiXiu", "T\u00e0i x\u1ec9u Đu Dây: Tr\u1ea3 c\u01b0\u1ee3c", "Ho\u00e0n tr\u1ea3 \u0111\u1eb7t c\u01b0\u1ee3c phi\u00ean " + this.referenceId, 0L, this.referenceId, TransType.END_TRANS);
                            } else {
                                isBot = this.isBot(nickname);
                                if (moneyType == 1 && !isBot) {
                                    this.balance.addBet(betValue);
                                    Random rd;
                                    int n;
                                    if (betValue >= ConfigGame.getIntValue("tx_min_money_black_list", 2000000) && ConfigGame.inBlackList(nickname) && (n = (rd = new Random()).nextInt(100)) <= ConfigGame.getIntValue("tx_black_list_percent", 50)) {
                                        Debug.trace("Black list " + nickname + " money= " + betValue + ", bet side= " + betSide);
                                        if (betSide == 1) {
                                            this.blackListBetTai += betValue;
                                        } else {
                                            this.blackListBetXiu += betValue;
                                        }
                                    }
                                    if (betValue >= ConfigGame.getIntValue("tx_min_money_white_list", 2000000) && ConfigGame.inWhiteList(nickname) && (n = (rd = new Random()).nextInt(100)) <= ConfigGame.getIntValue("tx_white_list_percent", 50)) {
                                        Debug.trace("White list " + nickname + " money= " + betValue + ", bet side= " + betSide);
                                        if (betSide == 1) {
                                            this.whiteListBetTai += betValue;
                                        } else {
                                            this.whiteListBetXiu += betValue;
                                        }
                                    }
                                }
                                if (betSide == 1) {
                                    this.potTai.bet(transTX, isBot);
                                } else {
                                    this.potXiu.bet(transTX, isBot);
                                }
                                currentMoney = res.getCurrentMoney();
                                transTX.genTransactionCode();
                                if (!isBot) {
                                    TaiXiuUtils.logBetTaiXiu(transTX);
                                }

                                // Đưa thông tin đặt cược vào cache
                                if(!isBot) {
                                    IMap<String, Long> usersBetTaiXiu = HCMap.getUsersBetTaiXiu();
                                    if (usersBetTaiXiu.containsKey(nickname)) {
                                        usersBetTaiXiu.put(nickname, usersBetTaiXiu.get(nickname) + betValue);
                                    } else {
                                        usersBetTaiXiu.put(nickname, betValue);
                                    }
                                }
                                result = 0;
                            }
                        } else {
                            result = 1;
                        }
                    }
                }
            } else {
                result = 4;
            }
        }
        final BetTaiXiuMsg msg = new BetTaiXiuMsg();
        msg.Error = (byte) result;
        msg.currentMoney = currentMoney;
        return msg;
    }

    public void updateTaiXiuPerSecond(boolean isBalancePot) {
        final UpdateTaiXiuPerSecondMsg msg = new UpdateTaiXiuPerSecondMsg();
        msg.remainTime = this.getRemainTime();
        msg.bettingState = this.bettingRound;
        if(isBalancePot){
            long minPotValue = Math.min(this.getPotTai(), this.getPotXiu());
            msg.potTai = minPotValue;
            msg.potXiu = minPotValue;
        } else {
            msg.potTai = this.getPotTai();
            msg.potXiu = this.getPotXiu();
        }
        msg.numBetTai = this.potTai.getNumBet();
        msg.numBetXiu = this.potXiu.getNumBet();
        this.sendMessageToRoom(msg);
    }

    public void calculatePrize(final long referenceId) {
        final HashMap<String, TransactionTaiXiu> sumTXTMap = new HashMap<>();
        final HashMap<String, TransactionTaiXiu> sumTai = new HashMap<>();
        final HashMap<String, TransactionTaiXiu> sumXiu = new HashMap<>();
        final long tongTienHopLe = Math.min(potTai.getTotalValue(), potXiu.getTotalValue());
        ResultTaiXiu rs = new ResultTaiXiu();
        if (this.resultTX != null) {
            rs = this.resultTX;
        }
        switch (this.result) {
            case 0: {
                long tongTienTaiDaTinh = 0L;
                long tongTienXiuDaTinh = 0L;
                for (final TransactionTaiXiuDetail tran : potXiu.getContributors()) {
                    try {
                        long tienDuocTinh = tran.betValue;
                        if (tongTienXiuDaTinh + tran.betValue > tongTienHopLe) {
                            tienDuocTinh = tongTienHopLe - tongTienXiuDaTinh;
                        }
                        tongTienXiuDaTinh += tienDuocTinh;
                        tran.prize = (long) (tienDuocTinh * (100.0f - this.tax) / 100.0f) + tienDuocTinh;
                        rs.totalPrize += tran.prize;
                        tran.refund = tran.betValue - tienDuocTinh;
                        rs.totalRefundXiu += tran.refund;
                        this.updateSumTran(sumTXTMap, tran);
                        this.updateSumTran(sumXiu, tran);
                        this.saveTransactionDetailTX(tran);
                    } catch (Exception e) {
                        Debug.trace("Error calculate prize user " + tran.username + " error: " + e.getMessage());
                    }
                }
                for (final TransactionTaiXiuDetail tran : potTai.getContributors()) {
                    try {
                        long tienDuocTinh = tran.betValue;
                        if (tongTienTaiDaTinh + tran.betValue > tongTienHopLe) {
                            tienDuocTinh = tongTienHopLe - tongTienTaiDaTinh;
                        }
                        tongTienTaiDaTinh += tienDuocTinh;
                        tran.refund = tran.betValue - tienDuocTinh;
                        rs.totalRefundTai += tran.refund;
                        this.updateSumTran(sumTXTMap, tran);
                        this.updateSumTran(sumTai, tran);
                        this.saveTransactionDetailTX(tran);
                    } catch (Exception e) {
                        Debug.trace("Error calculate prize user " + tran.username + " error: " + e.getMessage());
                    }
                }
                break;
            }
            case 1: {
                long tongTienTaiDaTinh = 0L;
                long tongTienXiuDaTinh = 0L;
                for (final TransactionTaiXiuDetail tran : potTai.getContributors()) {
                    try {
                        long tienDuocTinh = tran.betValue;
                        if (tongTienTaiDaTinh + tran.betValue > tongTienHopLe) {
                            tienDuocTinh = tongTienHopLe - tongTienTaiDaTinh;
                        }
                        tongTienTaiDaTinh += tienDuocTinh;
                        tran.prize = (long) (tienDuocTinh * (100.0f - this.tax) / 100.0f) + tienDuocTinh;
                        rs.totalPrize += tran.prize;
                        tran.refund = tran.betValue - tienDuocTinh;
                        rs.totalRefundTai += tran.refund;
                        this.updateSumTran(sumTXTMap, tran);
                        this.updateSumTran(sumTai, tran);
                        this.saveTransactionDetailTX(tran);
                    } catch (Exception e) {
                        Debug.trace("Error calculate prize user " + tran.username + " error: " + e.getMessage());
                    }
                }
                for (final TransactionTaiXiuDetail tran : potXiu.getContributors()) {
                    try {
                        long tienDuocTinh = tran.betValue;
                        if (tongTienXiuDaTinh + tran.betValue > tongTienHopLe) {
                            tienDuocTinh = tongTienHopLe - tongTienXiuDaTinh;
                        }
                        tongTienXiuDaTinh += tienDuocTinh;
                        tran.refund = tran.betValue - tienDuocTinh;
                        rs.totalRefundXiu += tran.refund;
                        this.updateSumTran(sumTXTMap, tran);
                        this.updateSumTran(sumXiu, tran);
                        this.saveTransactionDetailTX(tran);
                    } catch (Exception e) {
                        Debug.trace("Error calculate prize user " + tran.username + " error: " + e.getMessage());
                    }
                }
                break;
            }
            default:
                Debug.trace("Fuck error TX, room=" + this.moneyTypeStr + ", reference= " + referenceId + ", result= " + this.result);
                break;
        }
        if (this.moneyType == 1) {
            Debug.trace("TX phien= " + referenceId + ", tinh toan xong ket qua");
        }
        rs.totalTai = potTai.getTotalValue();
        rs.numBetTai = potTai.getNumBet();
        rs.totalXiu = potXiu.getTotalValue();
        rs.numBetXiu = potXiu.getNumBet();
        try {
            this.api.saveResultTaiXiu(rs);
        } catch (Exception e2) {
            e2.printStackTrace();
        }

        final UpdateMoneyTXTask taskTai = new UpdateMoneyTXTask(sumTai);
        taskTai.start();
        if (this.moneyType == 1) {
            Debug.trace("TX phien= " + referenceId + ", cap nhat xong ben tai");
        }

        final UpdateMoneyTXTask taskXiu = new UpdateMoneyTXTask(sumXiu);
        taskXiu.start();
        if (this.moneyType == 1) {
            Debug.trace("TX phien= " + referenceId + ", cap nhat xong ben xiu");
        }

        final ArrayList<TransactionTaiXiu> trans = new ArrayList<>(sumTXTMap.values());
        final LogTransactionsTXTask logTransactionsTXTask = new LogTransactionsTXTask(trans);
        logTransactionsTXTask.start();

        if (this.moneyType == 1) {
            final CalculateEndTXTask task = new CalculateEndTXTask(trans);
            task.start();
        }

        // Xóa danh sách đặt cược TX trong cache
        IMap<String, Long> usersBetTaiXiu = HCMap.getUsersBetTaiXiu();
        usersBetTaiXiu.clear();
    }

    private void updateSumTran(final Map<String, TransactionTaiXiu> map, final TransactionTaiXiuDetail tranDetail) {
        if (map.containsKey(tranDetail.username)) {
            final TransactionTaiXiu txt = map.get(tranDetail.username);
            if (txt.betSide == tranDetail.betSide) {
                txt.betValue += tranDetail.betValue;
                txt.totalPrize += tranDetail.prize;
                txt.totalRefund += tranDetail.refund;
                map.put(tranDetail.username, txt);
            }
        } else {
            final TransactionTaiXiu tran = new TransactionTaiXiu();
            tran.referenceId = tranDetail.referenceId;
            tran.userId = tranDetail.userId;
            tran.username = tranDetail.username;
            tran.moneyType = tranDetail.moneyType;
            tran.betSide = tranDetail.betSide;
            tran.betValue = tranDetail.betValue;
            tran.totalPrize = tranDetail.prize;
            tran.totalRefund = tranDetail.refund;
            map.put(tranDetail.username, tran);
        }
    }

    public short calculateBalanceTX(final int type) {
        long totalPrizeBotTai = 0L;
        long totalPrizeBotXiu = 0L;
        long totalPrizeUserTai = 0L;
        long totalPrizeUserXiu = 0L;
        final long tongTienHopLe = (this.potTai.getTotalValue() > this.potXiu.getTotalValue()) ? this.potXiu.getTotalValue() : this.potTai.getTotalBotBet();
        long tongTienXiuDaTinh = 0L;
        long tongTienTaiDaTinh = 0L;
        for (final TransactionTaiXiuDetail tran : this.potXiu.getContributors()) {
            long tienDuocTinh = tran.betValue;
            if (tongTienXiuDaTinh + tran.betValue > tongTienHopLe) {
                tienDuocTinh = tongTienHopLe - tongTienXiuDaTinh;
            }
            tongTienXiuDaTinh += tienDuocTinh;
            if (this.isBot(tran.username)) {
                totalPrizeBotXiu += tienDuocTinh;
            } else {
                totalPrizeUserXiu += tienDuocTinh;
            }
        }
        for (final TransactionTaiXiuDetail tran : this.potTai.getContributors()) {
            long tienDuocTinh = tran.betValue;
            if (tongTienTaiDaTinh + tran.betValue > tongTienHopLe) {
                tienDuocTinh = tongTienHopLe - tongTienTaiDaTinh;
            }
            tongTienTaiDaTinh += tienDuocTinh;
            if (this.isBot(tran.username)) {
                totalPrizeBotTai += tienDuocTinh;
            } else {
                totalPrizeUserTai += tienDuocTinh;
            }
        }
        Debug.trace("Bot tai: " + totalPrizeBotTai + ", bot xiu: " + totalPrizeBotXiu);
        Debug.trace("User tai: " + totalPrizeUserTai + ", user xiu: " + totalPrizeUserXiu);
        short result = -1;
        switch (type) {
            case 1: {
                result = this.tinhCuaThang(totalPrizeBotTai, totalPrizeBotXiu);
                Debug.trace("He thong am, force= " + result);
                break;
            }
            case -2: {
                result = this.tinhCuaThang(-this.blackListBetTai, -this.blackListBetXiu);
                Debug.trace("Black list: " + result);
                break;
            }
            case -3: {
                result = this.tinhCuaThang(this.whiteListBetTai, this.whiteListBetXiu);
                Debug.trace("White list: " + result);
                break;
            }
            case -1: {
                final long totalUserWinSystem = Math.abs(totalPrizeUserTai - totalPrizeUserXiu);
                if (totalUserWinSystem <= this.balance.getMaxWinUser()) {
                    result = this.tinhCuaThang(totalPrizeUserTai, totalPrizeUserXiu);
                    Debug.trace("Nguoi choi am, force = " + result);
                    break;
                }
                Debug.trace("Nguoi choi am nhung so tien an qua lo'n= " + totalUserWinSystem);
                result = this.checkHeThongAm(totalPrizeUserTai, totalPrizeUserXiu, totalPrizeBotTai, totalPrizeBotXiu);
                break;
            }
            default: {
                result = this.checkHeThongAm(totalPrizeUserTai, totalPrizeUserXiu, totalPrizeBotTai, totalPrizeBotXiu);
                break;
            }
        }
        return result;
    }

    private short tinhCuaThang(final long tai, final long xiu) {
        if (tai > xiu) {
            return 1;
        }
        if (tai < xiu) {
            return 0;
        }
        return -1;
    }

    private short checkHeThongAm(final long totalPrizeUserTai, final long totalPrizeUserXiu, final long totalPrizeBotTai, final long totalPrizeBotXiu) {
        short result = -1;
        final long fee = this.balance.getFee();
        final long revenueUser = this.balance.getRevenueUser();
        final long maxUserWin = Math.abs(totalPrizeUserTai - totalPrizeUserXiu);
        if (revenueUser + (long) (maxUserWin * (100.0f - this.tax) / 100.0f) >= -fee * ConfigGame.getFloatValue("tx_min_fee", 1.0f)) {
            result = this.tinhCuaThang(totalPrizeBotTai, totalPrizeBotXiu);
            Debug.trace("Chong he thong am, force= " + result + ", max user win= " + maxUserWin);
        }
        return result;
    }

    public int calculateForceBalance() {
        final boolean hasBlackList = this.blackListBetTai + this.blackListBetXiu > 0L;
        final boolean hasWhiteList = this.whiteListBetTai + this.whiteListBetXiu > 0L;
        return this.balance.isForceBalance(hasBlackList, hasWhiteList);
    }

    private void saveTransactionDetailTX(final TransactionTaiXiuDetail tran) {
        try {
            this.api.saveTransactionTaiXiuDetail(tran);
        } catch (Exception e) {
            Debug.trace("Update transaction detail tai xiu error: " + e.getMessage());
        }
    }

    public void updateTaiXiuInfo(final User user, final short remainTimeRutLoc) {
        final TaiXiuInfoMsg msg = new TaiXiuInfoMsg();
        msg.gameId = 2;
        msg.moneyType = this.moneyType;
        msg.referenceId = this.referenceId;
        msg.remainTime = this.getRemainTime();
        msg.bettingState = this.bettingRound;
        msg.potTai = this.getPotTai();
        msg.potXiu = this.getPotXiu();
        msg.myBetTai = this.getTotalBettingTaiByUsername(user.getName());
        msg.myBetXiu = this.getTotalBettingXiuByUsername(user.getName());
        if (this.resultTX != null) {
            msg.dice1 = (short) this.resultTX.dice1;
            msg.dice2 = (short) this.resultTX.dice2;
            msg.dice3 = (short) this.resultTX.dice3;
        }
        msg.remainTimeRutLoc = remainTimeRutLoc;
        this.sendMessageToUser(msg, user);
    }

    public boolean isBetting() {
        return this.bettingRound;
    }

    public long getPotTai() {
        return this.potTai.getTotalValue();
    }

    public long getBotBetTai() {
        return this.potTai.getTotalBotBet();
    }

    public long getUserBetTai() {
        return this.potTai.getTotalValue() - this.potTai.getTotalBotBet();
    }

    public long getPotXiu() {
        return this.potXiu.getTotalValue();
    }

    public long getBotBetXiu() {
        return this.potXiu.getTotalBotBet();
    }

    public long getUserBetXiu() {
        return this.potXiu.getTotalValue() - this.potXiu.getTotalBotBet();
    }

    public long getTotalBettingTaiByUsername(final String usernname) {
        return this.potTai.getTotalBetByUsername(usernname);
    }

    public long getTotalBettingXiuByUsername(final String username) {
        return this.potXiu.getTotalBetByUsername(username);
    }

    public BalanceMoneyTX getBalanceTX() {
        return this.balance;
    }

    public boolean isBot(final String username) {
        final UserCacheModel model = this.userService.forceGetCachedUser(username);
        return model.isBot();
    }

    public static short getMoneyType(final int roomId) {
        return (short) ((roomId != 0) ? 1 : 0);
    }

    public static String getKeyRoom(final short moneyType) {
        return "" + moneyType + "_" + 2;
    }

    @Override
    public boolean joinRoom(final User user) {
        final boolean result = super.joinRoom(user);
        if (result) {
            user.setProperty("MGROOM_TAI_XIU_INFO", this);
        }
        return result;
    }

    @Override
    public boolean quitRoom(final User user) {
        final boolean result = super.quitRoom(user);
        if (result) {
            user.removeProperty("MGROOM_TAI_XIU_INFO");
        }
        return result;
    }

    private final class UpdateMoneyTXTask extends Thread {
        private Map<String, TransactionTaiXiu> trans;

        private UpdateMoneyTXTask(final Map<String, TransactionTaiXiu> trans) {
            this.trans = trans;
        }

        @Override
        public void run() {
            for (final Map.Entry<String, TransactionTaiXiu> entry : this.trans.entrySet()) {
                try {
                    final String username = entry.getKey();
                    final TransactionTaiXiu txt = entry.getValue();
                    long currentMoney = MGRoomTaiXiu.this.userService.getCurrentMoneyUserCache(username, MGRoomTaiXiu.this.moneyTypeStr);
                    if (txt.totalPrize == 0L && txt.totalRefund == 0L) {
                        MGRoomTaiXiu.this.userService.updateMoney(username, 0L, MGRoomTaiXiu.this.moneyTypeStr, "TaiXiu", "", "", 0L, MGRoomTaiXiu.this.referenceId, TransType.END_TRANS);
                    } else {
                        if (txt.totalPrize > 0L) {
                            TransType transType = TransType.END_TRANS;
                            if (txt.totalRefund > 0L) {
                                transType = TransType.IN_TRANS;
                            }
                            final long fee = (long) (MGRoomTaiXiu.this.tax * txt.totalPrize / (200.0f - MGRoomTaiXiu.this.tax));
                            final MoneyResponse res2 = MGRoomTaiXiu.this.userService.updateMoney(username, txt.totalPrize, MGRoomTaiXiu.this.moneyTypeStr, "TaiXiu", "Thắng tài xỉu Đu Dây", "Phiên " + MGRoomTaiXiu.this.referenceId, fee, MGRoomTaiXiu.this.referenceId, transType);
                            if (res2.isSuccess()) {
                                if (MGRoomTaiXiu.this.moneyType == 1 && !MGRoomTaiXiu.this.isBot(username)) {
                                    MGRoomTaiXiu.this.balance.addWin(txt.totalPrize);
                                    MGRoomTaiXiu.this.balance.addFee(fee);
                                }
                                currentMoney = res2.getCurrentMoney();
                                final long totalExchange = (long) (txt.totalPrize * (100.0f - MGRoomTaiXiu.this.tax) / (200.0f - MGRoomTaiXiu.this.tax));
                                if (MGRoomTaiXiu.this.moneyType == 1 && totalExchange >= BroadcastMessageServiceImpl.MIN_MONEY) {
                                    MGRoomTaiXiu.this.broadcastMsgService.putMessage(Games.TAI_XIU.getId(), username, totalExchange);
                                }
                            }
                        }
                        final MoneyResponse res3;
                        if (txt.totalRefund > 0L && (res3 = MGRoomTaiXiu.this.userService.updateMoney(username, txt.totalRefund, MGRoomTaiXiu.this.moneyTypeStr, "TaiXiu", "Hoàn trả tài xỉu Đu Dây", "Phiên " + MGRoomTaiXiu.this.referenceId, 0L, MGRoomTaiXiu.this.referenceId, TransType.END_TRANS)).isSuccess()) {
                            if (MGRoomTaiXiu.this.moneyType == 1 && !MGRoomTaiXiu.this.isBot(username)) {
                                MGRoomTaiXiu.this.balance.addWin(txt.totalRefund);
                            }
                            currentMoney = res3.getCurrentMoney();
                        }
                    }
                    final UpdatePrizeTaiXiuMsg msg = new UpdatePrizeTaiXiuMsg();
                    msg.moneyType = MGRoomTaiXiu.this.moneyType;
                    msg.totalMoney = txt.totalPrize + txt.totalRefund;
                    msg.currentMoney = currentMoney;
                    MGRoomTaiXiu.this.sendMessageToUser(msg, username);
                } catch (Exception e) {
                    e.printStackTrace();
                    Debug.trace("Update tai xiu money phien " + MGRoomTaiXiu.this.referenceId + " error: " + e.getMessage());
                    KingUtil.printException("MGRoomTaiXiu UpdateMoneyTXTask", e);
                }
            }
        }
    }

    private final class LogTransactionsTXTask extends Thread {
        private List<TransactionTaiXiu> trans;

        private LogTransactionsTXTask(final List<TransactionTaiXiu> trans) {
            this.trans = trans;
        }

        @Override
        public void run() {
            try {
                api.saveTransactionTaiXiu(trans);
            } catch (Exception e) {
                GU.sendOperation("TAIXIU-LogTransactionsTXTask error " + e.getMessage());
            }
        }
    }

    private final class CalculateEndTXTask extends Thread {
        private final List<TransactionTaiXiu> trans;

        private CalculateEndTXTask(final List<TransactionTaiXiu> trans) {
            this.trans = trans;
        }

        @Override
        public void run() {
            try {
                MGRoomTaiXiu.this.api.calculateThanhDu(MGRoomTaiXiu.this.referenceId, this.trans, MGRoomTaiXiu.this.result);
                for (final TransactionTaiXiu tran : this.trans) {
                    if (tran.betValue - tran.totalRefund < 20000L) {
                        continue;
                    }
                    final int soLuotThem = RutLocUtils.getLuotRutLoc(tran.betValue - tran.totalRefund);
                    final int soLuotRut = MGRoomTaiXiu.this.api.updateLuotRutLoc(tran.username, soLuotThem);
                    final UpdateRutLocMsg msg = new UpdateRutLocMsg();
                    msg.soLuotRut = soLuotRut;
                    MGRoomTaiXiu.this.sendMessageToUser(msg, tran.username);
                }
            } catch (Exception e) {
                Debug.trace("Error save tai xiu: " + e.getMessage());
            }
        }
    }
}
