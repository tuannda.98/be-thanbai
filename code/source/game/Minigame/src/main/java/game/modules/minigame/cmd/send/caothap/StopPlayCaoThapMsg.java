// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.send.caothap;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class StopPlayCaoThapMsg extends BaseMsgEx
{
    public byte result;
    public long currentMoney;
    public long moneyExchange;
    
    public StopPlayCaoThapMsg() {
        super(6007);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        bf.put(this.result);
        bf.putLong(this.currentMoney);
        bf.putLong(this.moneyExchange);
        return this.packBuffer(bf);
    }
}
