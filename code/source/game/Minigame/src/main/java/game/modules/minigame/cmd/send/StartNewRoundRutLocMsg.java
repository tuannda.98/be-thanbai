// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class StartNewRoundRutLocMsg extends BaseMsgEx
{
    public int remainTime;
    
    public StartNewRoundRutLocMsg() {
        super(2121);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        bf.putInt(this.remainTime);
        return this.packBuffer(bf);
    }
}
