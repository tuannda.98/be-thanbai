// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.chat.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class ChatMsg extends BaseMsgEx
{
    public String nickname;
    public String mesasge;
    
    public ChatMsg() {
        super(18000);
        this.nickname = "";
        this.mesasge = "";
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        this.putStr(bf, this.nickname);
        this.putStr(bf, this.mesasge);
        return this.packBuffer(bf);
    }
}
