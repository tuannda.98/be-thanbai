// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class LichSuGiaoDichTXMsg extends BaseMsgEx
{
    public String[] data;
    
    public LichSuGiaoDichTXMsg() {
        super(2117);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        this.putStringArray(bf, this.data);
        return super.createData();
    }
}
