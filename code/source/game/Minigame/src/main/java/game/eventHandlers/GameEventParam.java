// 
// Decompiled by Procyon v0.5.36
// 

package game.eventHandlers;

import bitzero.server.core.IBZEventParam;

public enum GameEventParam implements IBZEventParam
{
    USER, 
    GAMEROOM, 
    IS_RECONNECT, 
    USER_SCORE;
}
