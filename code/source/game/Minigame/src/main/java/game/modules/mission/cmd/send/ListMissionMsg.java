// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.mission.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class ListMissionMsg extends BaseMsgEx
{
    public String listMission;
    
    public ListMissionMsg() {
        super(22000);
        this.listMission = "";
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        this.putStr(bf, this.listMission);
        return this.packBuffer(bf);
    }
}
