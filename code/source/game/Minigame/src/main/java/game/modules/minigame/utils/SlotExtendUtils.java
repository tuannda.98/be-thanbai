// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.utils;

import org.apache.log4j.Logger;

public class SlotExtendUtils
{
    private static Logger logger;
    private static String FORMAT_PLAY_SLOT_EXTEND;
    
    public static void log(final long referenceId, final String username, final int betValue, final String matrix, final short result, final short moneyType, final long handleTime, final String ratioTime, final String timeLog) {
        final String matrixStr = matrix.replaceAll(",", " ");
        SlotExtendUtils.logger.debug(String.format(SlotExtendUtils.FORMAT_PLAY_SLOT_EXTEND, referenceId, username, betValue, matrixStr, result, moneyType, handleTime, ratioTime, timeLog));
    }
    
    static {
        SlotExtendUtils.logger = Logger.getLogger("csvSlotExtend");
        SlotExtendUtils.FORMAT_PLAY_SLOT_EXTEND = ", %10d, %15s, %8d, %20s, %5d, %5d, %10d, %15s, %20s";
    }
}
