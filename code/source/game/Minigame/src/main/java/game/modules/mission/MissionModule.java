package game.modules.mission;

import casio.king365.GU;
import com.vinplay.vbee.common.models.userMission.CompleteMissionObj;
import game.modules.mission.cmd.send.RewardMissionMsg;
import game.modules.mission.cmd.rev.RewardMissionCmd;
import com.vinplay.vbee.common.models.userMission.UserMissionResponse;
import bitzero.util.common.business.Debug;
import game.modules.mission.cmd.send.ListMissionMsg;
import bitzero.server.extensions.data.DataCmd;
import bitzero.server.entities.User;
import com.vinplay.usercore.service.impl.UserMissionServiceImpl;
import com.vinplay.usercore.service.UserMissionService;
import bitzero.server.extensions.BaseClientRequestHandler;

public class MissionModule extends BaseClientRequestHandler {
    private final UserMissionService missionService;

    public MissionModule() {
        this.missionService = new UserMissionServiceImpl();
    }

    public void init() {
        super.init();
    }

    public void handleClientRequest(final User user, final DataCmd dataCmd) {
        switch (dataCmd.getId()) {
            case 22000: {
                this.getListMission(user);
                break;
            }
            case 22001: {
                this.rewardMission(user, dataCmd);
                break;
            }
        }
    }

    private void getListMission(final User user) {
        final ListMissionMsg msg = new ListMissionMsg();
        try {
            final UserMissionResponse obj = this.missionService.getUserMission(user.getName());
            msg.listMission = obj.toJson();
        } catch (Exception e) {
            GU.sendOperation("Lỗi get list mission, nick " + user.getName());
            e.printStackTrace();
            Debug.trace(e);
        }
        this.send(msg, user);
    }

    private void rewardMission(final User user, final DataCmd dataCmd) {
        final RewardMissionCmd cmd = new RewardMissionCmd(dataCmd);
        final RewardMissionMsg rewardMsg = new RewardMissionMsg();
        byte errorCode = -100;
        try {
            String moneyType = "vin";
            moneyType = ((cmd.moneyType == 1) ? "vin" : "xu");
            final CompleteMissionObj obj = this.missionService.completeMission(user.getName(), cmd.missionName, moneyType);
            if (obj.isUpdateSuccess()) {
                errorCode = 1;
                rewardMsg.currentMoney = obj.getMoneyUser();
                rewardMsg.prize = (int) obj.getMoneyBonus();
            } else {
                errorCode = this.parseRewardMissionError(obj.getError());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        rewardMsg.Error = errorCode;
        this.send(rewardMsg, user);
        if (errorCode == 1) {
            this.getListMission(user);
        }
        Debug.trace("Mission Module: " + user.getName() + " nhan thuong error_code=" + errorCode);
    }

    private byte parseRewardMissionError(final String errorStr) {
        switch (errorStr) {
            case "1047": {
                return -1;
            }
            case "1048": {
                return -2;
            }
            case "1049": {
                return -3;
            }
            default: {
                return -100;
            }
        }
    }
}
