// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.utils;

import java.util.ArrayList;
import java.util.Random;

public class RutLocUtils
{
    private static final int[] prizes;
    
    public static short[] tiLeGiaiThuong(final long fund) {
        if (100000L <= fund && fund < 2000000L) {
            return new short[] { 0, 0, 0, 0, 0, 0, 100 };
        }
        if (2000000L <= fund && fund < 5000000L) {
            return new short[] { 0, 0, 0, 0, 0, 10, 100 };
        }
        if (5000000L <= fund && fund < 20000000L) {
            return new short[] { 0, 0, 0, 0, 5, 20, 100 };
        }
        if (20000000L <= fund && fund < 50000000L) {
            return new short[] { 0, 0, 1, 2, 10, 35, 100 };
        }
        if (50000000L <= fund) {
            return new short[] { 1, 2, 4, 6, 15, 40, 100 };
        }
        return new short[] { 0, 0, 0, 0, 0, 0, 0 };
    }
    
    public static int getPrize(final short[] tiLe) {
        if (tiLe.length != RutLocUtils.prizes.length) {
            return 0;
        }
        final Random rd = new Random();
        final int n = rd.nextInt(100);
        for (int i = 0; i < tiLe.length; ++i) {
            if (n < tiLe[i]) {
                return RutLocUtils.prizes[i];
            }
        }
        return 0;
    }
    
    public static int[] getPrizes(final long fund, final int maxPrizes) {
        final short[] tiLe = tiLeGiaiThuong(fund);
        final int[] outPrizes = new int[maxPrizes];
        for (int i = 0; i < maxPrizes; ++i) {
            outPrizes[i] = getPrize(tiLe);
        }
        return outPrizes;
    }
    
    public static int[] phanBoGiaiThuong(final int tongSoNguoiRutLocLanTruoc, final int maxPrizes) {
        final int[] phanBo = new int[maxPrizes];
        if (tongSoNguoiRutLocLanTruoc <= maxPrizes) {
            for (int i = 0; i < maxPrizes; phanBo[i] = i++) {}
        }
        else {
            final ArrayList<Integer> list = new ArrayList<>();
            for (int j = 0; j < tongSoNguoiRutLocLanTruoc; ++j) {
                list.add(j);
            }
            final Random rd = new Random();
            for (int k = 0; k < maxPrizes; ++k) {
                final int n = rd.nextInt(list.size());
                phanBo[k] = list.remove(n);
            }
        }
        return phanBo;
    }
    
    public static int getLuotRutLoc(final long moneyExchange) {
        if (20000L <= moneyExchange && moneyExchange < 100000L) {
            return 1;
        }
        if (100000L <= moneyExchange && moneyExchange < 500000L) {
            return 5;
        }
        if (500000L <= moneyExchange && moneyExchange < 10000000L) {
            return 10;
        }
        if (10000000L <= moneyExchange && moneyExchange < 50000000L) {
            return 15;
        }
        if (50000000L <= moneyExchange && moneyExchange < 200000000L) {
            return 25;
        }
        if (200000000L <= moneyExchange) {
            return 40;
        }
        return 0;
    }
    
    private static void testTiLe() {
        final long fund = 50000000L;
        final int maxPrizes = 100000;
        final int[] outPrizes = getPrizes(fund, maxPrizes);
        final int[] thongKe = { 0, 0, 0, 0, 0, 0, 0 };
        for (int i = 0; i < maxPrizes; ++i) {
            for (int j = 0; j < RutLocUtils.prizes.length; ++j) {
                if (outPrizes[i] == RutLocUtils.prizes[j]) {
                    ++thongKe[j];
                }
            }
        }
        for (int i = 0; i < thongKe.length; ++i) {
            System.out.println("" + RutLocUtils.prizes[i] + " \t " + thongKe[i] + " \t " + thongKe[i] / (float)maxPrizes * 100.0f);
        }
    }
    
    private static void testPhanBoGiaiThuong() {
        final int soNguoi = 20;
        final int maxPrizes = 10;
        final int[] result = phanBoGiaiThuong(soNguoi, maxPrizes);
        System.out.println("PHAN BO GIAI THUONG, so nguoi = " + soNguoi + ", tong giai = " + maxPrizes);
        for (int j : result) {
            System.out.println(j);
        }
    }
    
    public static void main(final String[] args) {
        testTiLe();
        testPhanBoGiaiThuong();
    }
    
    static {
        prizes = new int[] { 500000, 200000, 100000, 50000, 20000, 10000, 5000 };
    }
}
