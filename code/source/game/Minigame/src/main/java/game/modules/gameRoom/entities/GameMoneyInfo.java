// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.gameRoom.entities;

import bitzero.server.entities.User;
import bitzero.util.common.business.Debug;
import com.vinplay.usercore.service.MoneyInGameService;
import com.vinplay.usercore.service.impl.MoneyInGameServiceImpl;
import com.vinplay.vbee.common.response.FreezeMoneyResponse;
import com.vinplay.vbee.common.response.MoneyResponse;
import game.entities.UserScore;
import game.utils.GameUtils;

public class GameMoneyInfo {
    public static final User.PropertyKey<GameMoneyInfo> GAME_MONEY_INFO = new User.PropertyKey<>("GAME_MONEY_INFO");
    public String sessionId;
    public final String nickName;
    public final String moneyBet;
    public final int moneyType;
    public final String moneyTypeName;
    public long currentMoney;
    public final long requireMoney;
    public final long outMoney;
    public long freezeMoney;
    public final MoneyInGameService moneyService;
    public int sessionid;
    public boolean moneyCheck;

    public GameMoneyInfo(final User user, final GameRoomSetting setting) {
        this.moneyService = new MoneyInGameServiceImpl();
        this.nickName = user.getName();
        this.moneyType = setting.moneyType;
        this.moneyBet = String.valueOf(setting.moneyBet);
        this.moneyTypeName = ((this.moneyType == 1) ? "vin" : "xu");
        this.requireMoney = setting.requiredMoney;
        this.outMoney = setting.outMoney;
    }

    public boolean startGameUpdateMoney() {
        return this.freezeMoneyBegining();
    }

    public boolean moneyCheck() {
        return this.freezeMoney >= this.outMoney;
    }

    public boolean freezeMoneyBegining() {
        if (GameUtils.isCheat) {
            this.freezeMoney = this.requireMoney;
            this.currentMoney = this.requireMoney;
            return true;
        }
        final FreezeMoneyResponse res = this.moneyService.freezeMoneyInGame(
                this.nickName, GameUtils.gameName, this.moneyBet, this.requireMoney, this.moneyTypeName);
        this.sessionId = res.getSessionId();
        Debug.trace("Freeze:", this.nickName, this.requireMoney, this.moneyTypeName, this.sessionId);
        if (res.isSuccess()) {
            this.currentMoney = res.getCurrentMoney();
            this.freezeMoney = this.requireMoney;
            return true;
        }
        Debug.trace("Error", res.getErrorCode());
        return false;
    }

    public void restoreMoney(final int roomId) {
        if (GameUtils.isCheat) {
            return;
        }
        final FreezeMoneyResponse res = this.moneyService.restoreMoneyInGame(
                this.sessionId, this.nickName, GameUtils.gameName, String.valueOf(roomId), this.moneyTypeName);
        if (!res.isSuccess()) {
            Debug.trace("Error invalid sessionId: ", this.sessionId);
        }
    }

    public boolean outGameUpdate(final int roomId) {
        this.restoreMoney(roomId);
        return true;
    }

    public long chargeMoneyInGame(final UserScore score, final int roomId, final int gameId) {
        Debug.trace("chargeMoneyInGame", this.nickName, score.money, roomId, gameId);
        if (score.money < 0L) {
            return -this.subMoneyInGame(score, roomId, gameId);
        }
        return this.addMoneyInGame(score, roomId, gameId);
    }

    public long subMoneyInGame(final UserScore score, final int roomId, final int gameId) {
        if (GameUtils.isCheat) {
            this.currentMoney += score.money;
            return score.money;
        }
        final MoneyResponse res = this.moneyService.subtractMoneyInGame(
                this.sessionId, this.nickName, GameUtils.gameName, String.valueOf(roomId),
                -score.money, this.moneyTypeName, String.valueOf(gameId));
        this.currentMoney = res.getCurrentMoney();
        this.freezeMoney = res.getFreezeMoney();
        Debug.trace("subMoneyInGame", this.sessionId, res.isSuccess(), res.getErrorCode());
        return res.getSubtractMoney();
    }

    public long addMoneyInGame(final UserScore score, final int roomId, final int gameId) {
        if (GameUtils.isCheat) {
            this.currentMoney += score.money;
            return score.money;
        }
        final MoneyResponse res = this.moneyService.addingMoneyInGame(
                this.sessionId, this.nickName, GameUtils.gameName, String.valueOf(roomId),
                score.money, this.moneyTypeName, this.requireMoney, String.valueOf(gameId), score.wastedMoney);
        this.currentMoney = res.getCurrentMoney();
        this.freezeMoney = res.getFreezeMoney();
        Debug.trace("addMoneyInGame", this.sessionId, res.isSuccess(), res.getErrorCode());
        return score.money;
    }

    public long getFreezeMoney() {
        return this.freezeMoney;
    }
}
