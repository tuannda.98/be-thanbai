// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.config;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class MiniGameConfig {
    public JSONObject config;
    private static MiniGameConfig miniGameConfig;

    private MiniGameConfig() {
        this.initconfig();
    }

    public static MiniGameConfig instance() {
        if (MiniGameConfig.miniGameConfig == null) {
            MiniGameConfig.miniGameConfig = new MiniGameConfig();
        }
        return MiniGameConfig.miniGameConfig;
    }

    public void initconfig() {
        final String path = System.getProperty("user.dir");
        final File file = new File(path + "/conf/minigame.json");
        final StringBuilder contents = new StringBuilder();
        try {
            try (InputStreamReader r = new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8);
                 BufferedReader reader = new BufferedReader(r);) {
                String text = null;
                while ((text = reader.readLine()) != null) {
                    contents.append(text).append(System.getProperty("line.separator"));
                }
            }
            this.config = new JSONObject(contents.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static {
        MiniGameConfig.miniGameConfig = null;
    }
}
