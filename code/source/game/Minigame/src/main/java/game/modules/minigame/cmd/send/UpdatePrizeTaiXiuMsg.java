// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class UpdatePrizeTaiXiuMsg extends BaseMsgEx
{
    public short moneyType;
    public long totalMoney;
    public long currentMoney;
    
    public UpdatePrizeTaiXiuMsg() {
        super(2114);
    }
    
    public byte[] createData() {
        final ByteBuffer buffer = this.makeBuffer();
        buffer.putShort(this.moneyType);
        buffer.putLong(this.totalMoney);
        buffer.putLong(this.currentMoney);
        return this.packBuffer(buffer);
    }
}
