// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.gameRoom.entities;

import game.modules.gameRoom.cmd.rev.JoinGameRoomCmd;
import org.json.JSONException;
import bitzero.util.common.business.CommonHandle;
import org.json.JSONObject;

public class GameRoomSetting
{
    public static final int VIP_ROOM = 1;
    public static final int NORMAL_ROOM = 2;
    public String setting_name;
    public int maxUserPerRoom;
    public long moneyBet;
    public int moneyType;
    public long requiredMoney;
    public long outMoney;
    public int commisionRate;
    public int rule;
    
    public GameRoomSetting(final JSONObject config) {
        this.setting_name = null;
        try {
            this.maxUserPerRoom = config.getInt("maxUserPerRoom");
            this.moneyType = config.getInt("moneyType");
            this.moneyBet = config.getInt("moneyBet");
            this.requiredMoney = config.getLong("requiredMoney");
            this.outMoney = config.getLong("outMoney");
            this.commisionRate = config.getInt("commisionRate");
            this.rule = config.getInt("rule");
            this.setting_name = this.maxUserPerRoom + "_" + this.moneyType + "_" + this.moneyBet + "_" + this.rule;
        }
        catch (JSONException e) {
            CommonHandle.writeErrLog(e);
        }
    }
    
    public GameRoomSetting(final JoinGameRoomCmd cmd) {
        this.setting_name = null;
        this.maxUserPerRoom = cmd.maxUserPerRoom;
        this.moneyType = cmd.moneyType;
        this.moneyBet = cmd.moneyBet;
        this.rule = cmd.rule;
        this.setting_name = this.maxUserPerRoom + "_" + this.moneyType + "_" + this.moneyBet + "_" + this.rule;
    }
    
    public String getSettingName() {
        return this.setting_name;
    }
}
