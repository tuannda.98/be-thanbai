// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.test;

import java.text.ParseException;
import java.util.Date;
import java.util.Calendar;
import java.text.SimpleDateFormat;

public class Test
{
    public static void main(final String[] args) throws ParseException {
        final long startTime = System.currentTimeMillis();
        final SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd 00:30:00");
        final SimpleDateFormat format2 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 1);
        final String rewardTimeStr = format.format(cal.getTime());
        System.out.println(rewardTimeStr);
        final Date rewardTimeD = format2.parse(rewardTimeStr);
        System.out.println(format2.format(rewardTimeD));
        final long rewardTime = rewardTimeD.getTime();
        final long remainTime = rewardTime - startTime;
        final long seconds = remainTime / 1000L;
        System.out.println(seconds);
    }
}
