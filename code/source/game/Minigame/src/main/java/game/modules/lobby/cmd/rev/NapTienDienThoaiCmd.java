// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.lobby.cmd.rev;

import java.nio.ByteBuffer;
import bitzero.server.extensions.data.DataCmd;
import bitzero.server.extensions.data.BaseCmd;

public class NapTienDienThoaiCmd extends BaseCmd
{
    public String mobile;
    public byte amount;
    public byte type;
    
    public NapTienDienThoaiCmd(final DataCmd dataCmd) {
        super(dataCmd);
        this.unpackData();
    }
    
    public void unpackData() {
        final ByteBuffer bf = this.makeBuffer();
        this.mobile = this.readString(bf);
        this.amount = bf.get();
        this.type = bf.get();
    }
}
