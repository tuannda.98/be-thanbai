// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.entities.pokego;

public class AwardsOnLine
{
    private Award award;
    private long money;
    private String lineName;
    
    public AwardsOnLine(final Award award, final long money, final String lineName) {
        this.award = award;
        this.money = money;
        this.lineName = lineName;
    }
    
    public Award getAward() {
        return this.award;
    }
    
    public void setAward(final Award award) {
        this.award = award;
    }
    
    public long getMoney() {
        return this.money;
    }
    
    public void setMoney(final long money) {
        this.money = money;
    }
    
    public String getLineName() {
        return this.lineName;
    }
    
    public void setLineName(final String lineName) {
        this.lineName = lineName;
    }
    
    public String getLineId() {
        return this.lineName.substring(4);
    }
}
