// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.mission.cmd.rev;

import java.nio.ByteBuffer;
import bitzero.server.extensions.data.DataCmd;
import bitzero.server.extensions.data.BaseCmd;

public class RewardMissionCmd extends BaseCmd
{
    public String missionName;
    public byte moneyType;
    
    public RewardMissionCmd(final DataCmd data) {
        super(data);
        this.unpackData();
    }
    
    public void unpackData() {
        final ByteBuffer buffer = this.makeBuffer();
        this.missionName = this.readString(buffer);
        this.moneyType = this.readByte(buffer);
    }
}
