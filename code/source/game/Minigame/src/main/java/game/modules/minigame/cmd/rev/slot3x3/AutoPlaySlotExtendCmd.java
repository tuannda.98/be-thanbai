// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.rev.slot3x3;

import java.nio.ByteBuffer;
import bitzero.server.extensions.data.DataCmd;
import bitzero.server.extensions.data.BaseCmd;

public class AutoPlaySlotExtendCmd extends BaseCmd
{
    public byte autoPlay;
    public long gold;
    
    public AutoPlaySlotExtendCmd(final DataCmd dataCmd) {
        super(dataCmd);
        this.unpackData();
    }
    
    public void unpackData() {
        final ByteBuffer bf = this.makeBuffer();
        this.autoPlay = bf.get();
        this.gold = this.readLong(bf);
    }
}
