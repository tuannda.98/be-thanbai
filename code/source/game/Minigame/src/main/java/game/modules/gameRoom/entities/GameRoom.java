package game.modules.gameRoom.entities;

import java.util.concurrent.ConcurrentHashMap;
import bitzero.server.entities.User;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class GameRoom
{
    public static final AtomicInteger genId;
    public GameRoomGroup group;
    private final GameServer gameServer;
    private final int id;
    public Map<Integer, User> userManager;
    private final Map<String, Object> properties;
    public GameRoomSetting setting;
    
    public GameRoom(final GameRoomSetting setting) {
        this.userManager = new ConcurrentHashMap<>();
        this.properties = new ConcurrentHashMap<>();
        this.setting = setting;
        this.id = GameRoom.genId.getAndIncrement();
        this.gameServer = GameServer.createNewGameServer(this);
    }
    
    public int getUserCount() {
        return this.userManager.size();
    }
    
    public void setProperty(final String key, final Object value) {
        this.properties.put(key, value);
    }
    
    public Object getProperty(final String key) {
        return this.properties.get(key);
    }
    
    public int getId() {
        return this.id;
    }
    
    public GameServer getGameServer() {
        return this.gameServer;
    }
    
    static {
        genId = new AtomicInteger(1);
    }
}
