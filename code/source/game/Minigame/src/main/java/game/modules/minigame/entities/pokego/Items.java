// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.entities.pokego;

import java.util.Random;
import java.util.ArrayList;
import java.util.List;

public class Items
{
    public final List<Item> items;
    
    public Items() {
        int[] config = new int[]{1, 6, 12, 12, 20, 20};
        this.items = new ArrayList<>();
        for (int i = 0; i < config.length; ++i) {
            for (int j = 0; j < config[i]; ++j) {
                this.items.add(Item.findItem((byte)i));
            }
        }
    }
    
    public int size() {
        return this.items.size();
    }
    
    public Item random() {
        final Random rd = new Random();
        final int index = rd.nextInt(this.items.size());
        return this.items.get(index);
    }
}
