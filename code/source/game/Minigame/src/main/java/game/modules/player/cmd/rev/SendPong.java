// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.player.cmd.rev;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class SendPong extends BaseMsgEx
{
    public SendPong() {
        super(1001);
    }
    
    public byte[] createData() {
        final ByteBuffer buffer = this.makeBuffer();
        return this.packBuffer(buffer);
    }
}
