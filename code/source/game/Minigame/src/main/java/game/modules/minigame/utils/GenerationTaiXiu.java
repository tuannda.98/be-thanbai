// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.utils;

import bitzero.util.common.business.Debug;
import game.utils.ConfigGame;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class GenerationTaiXiu {
    private final List<String> listCau;
    private CauTaiXiu cauTX;

    public GenerationTaiXiu() {
        this.listCau = new ArrayList<>();
        this.cauTX = null;
    }

    public void readConfig() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader("config/taixiu.dat"))) {
            String str = null;
            while ((str = reader.readLine()) != null) {
                if (str.isEmpty()) {
                    continue;
                }
                this.listCau.add(str);
            }
        }
    }

    public short[] generateResult(final short forceBetSide) {
        int result = forceBetSide;
        /*final Random rd = new Random();
        if (this.cauTX != null && result == -1) {
            result = this.cauTX.getResultTX();
        }
        if (result == -1) {
            this.cauTX = null;
            final int n = rd.nextInt(ConfigGame.getIntValueBaseMin("tx_num_rd", 1));
            if (n != 0 || this.listCau.size() <= 0) {
                return this.generateDices();
            }
            final int cau = rd.nextInt(this.listCau.size());
            this.cauTX = new CauTaiXiu();
            final String data = this.listCau.get(cau);
            this.cauTX.setData(data);
            Debug.trace("gd= " + this.cauTX.data);
            result = this.cauTX.getResultTX();
        }
        if (result == -1) {
            return this.generateDices();
        }*/
        short[] dices;
        int totalDices;
        int genResult;
        while ((genResult = (((totalDices = (dices = this.generateDices())[0] + dices[1] + dices[2]) > 10) ? 1 : 0)) != result) {
        }
        return dices;
    }

    public short[] generateRandomResult() {
        return this.generateDices();
    }

    private short[] generateDices() {
        return new short[]{(short) (ThreadLocalRandom.current().nextInt(6) + 1), (short) (ThreadLocalRandom.current().nextInt(6) + 1), (short) (ThreadLocalRandom.current().nextInt(6) + 1)};
    }

    public static void main(final String[] agrs) {
    }

    private static class CauTaiXiu {
        public int index;
        private String data;

        private CauTaiXiu() {
            this.index = 0;
        }

        public void setData(final String data) {
            this.data = data;
            this.index = data.length() - 1;
        }

        public int getResultTX() {
            if (this.index < 0 || this.index >= this.data.length()) {
                return -1;
            }
            final int result = Integer.parseInt("" + this.data.charAt(this.index));
            --this.index;
            return result;
        }
    }
}
