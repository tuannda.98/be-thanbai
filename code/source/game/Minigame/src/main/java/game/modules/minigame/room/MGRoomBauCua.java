package game.modules.minigame.room;

import bitzero.server.entities.User;
import bitzero.util.common.business.Debug;
import casio.king365.GU;
import casio.king365.core.HCMap;
import casio.king365.util.KingUtil;
import com.hazelcast.core.IMap;
import com.vinplay.dal.service.BauCuaService;
import com.vinplay.dal.service.BroadcastMessageService;
import com.vinplay.dal.service.MiniGameService;
import com.vinplay.dal.service.impl.BauCuaServiceImpl;
import com.vinplay.dal.service.impl.BroadcastMessageServiceImpl;
import com.vinplay.dal.service.impl.MiniGameServiceImpl;
import com.vinplay.usercore.service.UserService;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.enums.Games;
import com.vinplay.vbee.common.models.minigame.baucua.ResultBauCua;
import com.vinplay.vbee.common.models.minigame.baucua.TransactionBauCua;
import com.vinplay.vbee.common.models.minigame.baucua.TransactionBauCuaDetail;
import com.vinplay.vbee.common.response.MoneyResponse;
import com.vinplay.vbee.common.statics.TransType;
import com.vinplay.vbee.common.utils.CommonUtils;
import game.modules.minigame.cmd.send.baucua.*;
import game.modules.minigame.entities.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeoutException;

public class MGRoomBauCua extends MGRoom {
    public final byte id;
    public long fund;
    private final List<PotBauCua> pots;
    private final byte moneyType;
    private String moneyTypeStr;
    private long referenceId;
    private float tax;
    private final int minBetValue;
    private final UserService userService;
    private final BauCuaService bcService;
    private final MiniGameService mgService;
    private final ConcurrentMap<String, TransactionBauCua> transactionsMap;
    private byte[] dices;
    private byte xPot;
    private byte xValue;
    private ResultBauCua resultBC;
    private List<ResultBauCua> lichSuPhien;
    private List<BotBauCua> botBC;
    private final BroadcastMessageService broadcastMsgService;
    public static final String GAME_NAME = "BauCua";
    private static final byte BETTING_SUCCESS = 1;
    private static final byte BETTING_FAIL = 100;
    private static final byte INVALID_BETTING_STATE = 101;
    private static final byte NOT_ENOUGH_MONEY = 102;

    public MGRoomBauCua(final String name, final int minBetValue, final byte moneyType, final byte id, final long fund) {
        super(name, Games.BAU_CUA);
        this.pots = new ArrayList<>();
        this.tax = MinigameConstant.MINIGAME_TAX_VIN;
        this.userService = new UserServiceImpl();
        this.bcService = new BauCuaServiceImpl();
        this.mgService = new MiniGameServiceImpl();
        this.transactionsMap = new ConcurrentHashMap<>();
        this.resultBC = new ResultBauCua();
        this.lichSuPhien = new ArrayList<>();
        this.botBC = new ArrayList<>();
        this.broadcastMsgService = new BroadcastMessageServiceImpl();
        this.id = id;
        this.fund = fund;
        this.moneyType = moneyType;
        if (moneyType == 1) {
            this.moneyTypeStr = "vin";
        } else if (moneyType == 0) {
            this.moneyTypeStr = "xu";
        }
        this.minBetValue = minBetValue;
        if (this.moneyType == 0) {
            this.tax = MinigameConstant.MINIGAME_TAX_XU;
        }
        for (int i = 0; i < 6; ++i) {
            this.pots.add(new PotBauCua(i));
        }
        this.lichSuPhien = this.bcService.getLichSuPhien(30, id);
        Debug.trace("Lich su phien " + this.buildLichSuPhien());
    }

    public void updateBauCuaInfoToUser(final User user, final byte remainTime, final boolean bettingState) {
        final BauCuaInfoMsg msg = new BauCuaInfoMsg();
        msg.referenceId = this.referenceId;
        msg.remainTime = remainTime;
        msg.potData = this.buildPotData();
        msg.betData = this.buildBetData(user.getName());
        msg.lichSuPhien = this.buildLichSuPhien();
        msg.bettingState = bettingState;
        msg.dice1 = this.resultBC.dices[0];
        msg.dice2 = this.resultBC.dices[1];
        msg.dice3 = this.resultBC.dices[2];
        msg.xPot = this.resultBC.xPot;
        msg.xValue = this.resultBC.xValue;
        msg.room = this.id;
        this.sendMessageToUser(msg, user);
    }

    public void startNewGame(final long newReferenceId) {
        this.referenceId = newReferenceId;
        this.pots.forEach(PotBauCua::renew);
        this.transactionsMap.clear();
        this.resultBC = new ResultBauCua(this.referenceId, this.id, this.minBetValue);
        this.botBC = BotMinigame.getBotBauCua(this.id);
        Debug.trace("BOT BAU CUA ROOM " + this.id + ", size= " + this.botBC.size());
    }

    public void botBet(final int time, final boolean bettingState) {
        this.botBC.stream().filter(b -> b.getTimeBetting() == time)
                .forEach(b -> this.bet(b.getNickname(), b.getBetStr(), bettingState));
    }

    public ResultBetBauCuaMsg bet(final String username, final String betStr, final boolean bettingState) {
        long totalBetValue = 0L;
        final ResultBetBauCuaMsg msg = new ResultBetBauCuaMsg();
        long currentMoney = this.userService.getMoneyUserCache(username, this.moneyTypeStr);
        if (!bettingState) {
            msg.result = 101;
            return msg;
        }
        final String[] arr = betStr.split(",");
        if (arr.length != 6) {
            msg.result = 100;
            return msg;
        }
        final long[] betValues = new long[6];
        try {
            for (int i = 0; i < 6; ++i) {
                betValues[i] = Long.parseLong(arr[i]);
                if (betValues[i] < 0L) {
                    throw new NumberFormatException();
                }
                totalBetValue += betValues[i];
            }
        } catch (NumberFormatException e) {
            Debug.trace("Bet value: " + betStr + " incorrect: " + e.getMessage());
            GU.sendOperation(this.getClass().getName() + "Bet value: " + betStr + " incorrect: " + e.getMessage());
            msg.result = 100;
            return msg;
        }

        if (totalBetValue <= 0L) {
            msg.result = 102;
            return msg;
        }
        long minVin = this.userService.getAvailableVinMoney(username);
        if (totalBetValue > minVin) {
            msg.result = 100;
            return msg;
        }
        final long fee = (long) (totalBetValue * this.tax / 100.0f);
        final MoneyResponse response = this.userService.updateMoney(username, -totalBetValue, this.moneyTypeStr, "BauCua", "B\u1ea7u cua: \u0110\u1eb7t c\u01b0\u1ee3c", "Phi\u00ean " + this.referenceId, fee, this.referenceId, TransType.START_TRANS);
        if (!response.isSuccess()) {
            msg.result = 100;
            return msg;
        }

        // Đưa thông tin đặt cược vào cache
        String nickname = username;
        IMap<String, Long> usersBetBauCua = HCMap.getUsersBetBauCua();
        KingUtil.printLog("dat cuoc bau cua, usersBetBauCua: "+usersBetBauCua.size());
        if (usersBetBauCua.containsKey(nickname)) {
            usersBetBauCua.put(nickname, usersBetBauCua.get(nickname) + totalBetValue);
        } else {
            usersBetBauCua.put(nickname, totalBetValue);
        }
        KingUtil.printLog("dat cuoc bau cua, nickname: "+nickname+", bet value: "+usersBetBauCua.get(nickname));

        this.fund += totalBetValue - fee;
        if (!this.transactionsMap.containsKey(username)) {
            final TransactionBauCua newTransaction = new TransactionBauCua();
            newTransaction.username = username;
            newTransaction.moneyType = this.moneyType;
            newTransaction.referenceId = this.referenceId;
            newTransaction.room = this.minBetValue;
            newTransaction.betValues = betValues;
            this.transactionsMap.put(username, newTransaction);
        } else {
            final TransactionBauCua transaction = this.transactionsMap.get(username);
            for (int i = 0; i < 6; ++i) {
                final long[] arrl = transaction.betValues;
                arrl[i] += betValues[i];
            }
            this.transactionsMap.put(username, transaction);
        }
        final TransactionBauCuaDetail tranDetail = new TransactionBauCuaDetail();
        tranDetail.username = username;
        tranDetail.referenceId = this.referenceId;
        tranDetail.moneyType = this.moneyType;
        tranDetail.room = this.minBetValue;
        tranDetail.betValues = betValues;
        currentMoney = response.getCurrentMoney();
        for (int i = 0; i < 6; ++i) {
            if (betValues[i] > 0L) {
                this.pots.get(i).bet(tranDetail.username, tranDetail.betValues[i]);
            }
        }
        try {
            this.mgService.saveFund(this.name, this.fund);
            this.bcService.saveTransactionBauCuaDetail(tranDetail);
        } catch (IOException | TimeoutException | InterruptedException ex) {
            ex.printStackTrace();
            GU.sendOperation(this.getClass().getName() + " save fund " + ex.getMessage());
        }
        msg.result = (byte) 1;
        msg.currentMoney = currentMoney;
        return msg;
    }

    public void bet(final User user, final String betStr, final boolean bettingState) {
        final ResultBetBauCuaMsg msg = this.bet(user.getName(), betStr, bettingState);
        this.sendMessageToUser(msg, user);
    }

    public void updateBauCuaPerSecond(final byte remainTime, final boolean bettingState) {
        final UpdateBauCuaPerSecondMsg msg = new UpdateBauCuaPerSecondMsg();
        msg.potData = this.buildPotData();
        msg.remainTime = remainTime;
        msg.bettingState = bettingState;
        this.sendMessageToRoom(msg);
    }

    public long calculatePrizes() {
        KingUtil.printLog("calculatePrizes() BC room: "+this);
        KingUtil.printLog("calculatePrizes() 1");
        final int[] tiLe = this.calculateTiLe(this.dices);
        long totalVinPay = 0L;
        final long[] totalBetValuesInRoom = new long[6];
        final long[] totalPrizesInRoom = new long[6];
        KingUtil.printLog("calculatePrizes() 2");
        for (final TransactionBauCua tran : this.transactionsMap.values()) {
            long totalPrize = 0L;
            long totalBetValues = 0L;
            KingUtil.printLog("calculatePrizes() 3");
            for (int i = 0; i < 6; ++i) {
                if (tiLe[i] > 0) {
                    totalPrize += tran.betValues[i] * tiLe[i] + tran.betValues[i];
                    tran.prizes[i] = tran.betValues[i] * tiLe[i] + tran.betValues[i];
                    totalPrizesInRoom[i] += tran.betValues[i] * tiLe[i] + tran.betValues[i];
                }
                totalBetValues += tran.betValues[i];
                totalBetValuesInRoom[i] += tran.betValues[i];
            }
            KingUtil.printLog("calculatePrizes() 4");
            if (totalPrize > 0L) {
                if (this.moneyType == 1) {
                    totalVinPay += totalPrize;
                }
                final MoneyResponse response;
                if ((response = this.userService.updateMoney(tran.username, totalPrize, this.moneyTypeStr, "BauCua", "B\u1ea7u cua: Tr\u1eadn th\u1eafng", "Phi\u00ean " + this.referenceId, 0L, this.referenceId, TransType.END_TRANS)) != null && response.isSuccess()) {
                    final UpdateBauCuaPrizeMsg msg = new UpdateBauCuaPrizeMsg();
                    msg.prize = totalPrize;
                    msg.currentMoney = response.getCurrentMoney();
                    msg.room = this.id;
                    this.sendMessageToUser(msg, tran.username);
                    if (this.moneyType == 1 && totalPrize >= BroadcastMessageServiceImpl.MIN_MONEY) {
                        this.broadcastMsgService.putMessage(Games.BAU_CUA.getId(), tran.username, totalPrize);
                    }
                }
            }
            KingUtil.printLog("calculatePrizes() 5");
            try {
                tran.totalExchange = totalPrize - totalBetValues;
                tran.dices = CommonUtils.arrayByteToString(this.resultBC.dices);
                this.bcService.saveTransactionBauCua(tran);
            } catch (IOException | TimeoutException | InterruptedException ex) {
                ex.printStackTrace();
            }
            this.fund -= totalPrize;
            try {
                this.mgService.saveFund(this.name, this.fund);
            } catch (IOException | TimeoutException | InterruptedException ex4) {
                ex4.printStackTrace();
            }
            KingUtil.printLog("calculatePrizes() 6");
        }
        KingUtil.printLog("calculatePrizes() 7");
        this.resultBC.totalBetValues = totalBetValuesInRoom;
        this.resultBC.totalPrizes = totalPrizesInRoom;
        KingUtil.printLog("calculatePrizes() 8");
        try {
            this.bcService.saveResultBauCua(this.resultBC);
            KingUtil.printLog("calculatePrizes() 9");
            if (this.moneyType == 1) {
                final ArrayList<TransactionBauCua> list = new ArrayList<>(this.transactionsMap.values());
                Debug.trace("TRANSACTION BAU CUA: " + list.size());
                this.bcService.calculteToiChonCa(this.dices, list);
            }
            KingUtil.printLog("calculatePrizes() 10");
        } catch (IOException | TimeoutException | InterruptedException ex7) {
            ex7.printStackTrace();
        }
        KingUtil.printLog("calculatePrizes() 11");
        return totalVinPay;
    }

    public long tryCalculatePrizes(final int[] tiLe) {
        long totalValues = 0L;
        for (final TransactionBauCua tran : this.transactionsMap.values()) {
            for (int i = 0; i < 6; ++i) {
                totalValues += tran.betValues[i] * tiLe[i] + tran.betValues[i];
            }
        }
        return totalValues;
    }

    public String buildPotData() {
        final StringBuilder builder = new StringBuilder();
        this.pots.forEach(pot -> {
            builder.append(pot.getTotalValue());
            builder.append(",");
        });
        if (builder.length() > 0) {
            builder.deleteCharAt(builder.length() - 1);
        }
        return builder.toString();
    }

    public String buildBetData(final String username) {
        final StringBuilder builder = new StringBuilder();
        this.pots.forEach(pot -> {
            builder.append(pot.getTotalBetByUsername(username));
            builder.append(",");
        });
        if (builder.length() > 0) {
            builder.deleteCharAt(builder.length() - 1);
        }
        return builder.toString();
    }

    @Override
    public boolean joinRoom(final User user) {
        final boolean result = super.joinRoom(user);
        if (result) {
            user.setProperty("MGROOM_BAU_CUA_INFO", this);
        }
        return result;
    }

    public float getTax() {
        return this.tax;
    }

    public byte getMoneyType() {
        return this.moneyType;
    }

    private byte[] generateDices() {
        int num = 0;
        do {
            final Random rd = new Random();
            final byte[] dices = {(byte) rd.nextInt(6), (byte) rd.nextInt(6), (byte) rd.nextInt(6)};
            this.xPot = this.randomXPot();
            this.xValue = this.randomXValue();
            final int[] tiLe = this.calculateTiLe(dices);
            final long totalPrizes = this.tryCalculatePrizes(tiLe);
            if (this.fund - totalPrizes > 0L) {
                return dices;
            }
        } while (++num <= 3);
        return this.traGiaiBeNhat();
    }

    private byte[] traGiaiBeNhat() {
        final byte[] dices = new byte[3];
        final ArrayList<Pot> potsTmp = new ArrayList<>();
        final ArrayList<Integer> indexPot = new ArrayList<>();
        for (int j = 0; j < this.pots.size(); ++j) {
            final Pot p = this.pots.get(j);
            boolean added = false;
            for (int i = 0; i < potsTmp.size(); ++i) {
                final Pot pt = potsTmp.get(i);
                if (p.getTotalValue() < pt.getTotalValue()) {
                    potsTmp.add(i, p);
                    indexPot.add(i, j);
                    added = true;
                    break;
                }
                final Random rd;
                final int n;
                if (p.getTotalValue() == pt.getTotalValue() && (n = (rd = new Random()).nextInt(2)) == 0) {
                    potsTmp.add(i, p);
                    indexPot.add(i, j);
                    added = true;
                    break;
                }
            }
            if (!added) {
                potsTmp.add(p);
                indexPot.add(j);
            }
        }
        for (int k = 0; k < 3; ++k) {
            dices[k] = indexPot.get(k).byteValue();
        }
        this.xPot = 1;
        this.xValue = 1;
        return dices;
    }

    private int[] calculateTiLe(final byte[] dices) {
        final int[] tiLe = new int[6];
        for (int i = 0; i < 6; ++i) {
            tiLe[i] = 0;
            for (byte dice : dices) {
                if (i == dice) {
                    ++tiLe[i];
                }
            }
            if (tiLe[i] > 0) {
                if (i == this.xPot) {
                    tiLe[i] *= this.xValue;
                }
            }
        }
        return tiLe;
    }

    public void generateResult() {
        this.dices = this.generateDices();
        this.resultBC.referenceId = this.referenceId;
        this.resultBC.dices = this.dices;
        this.resultBC.xPot = this.xPot;
        this.resultBC.xValue = this.xValue;
        Debug.trace("BAU CUA " + this.id + " DICES: " + this.dices[0] + "," + this.dices[1] + "," + this.dices[2] + ", xPot= " + this.xPot + ", xValue= " + this.xValue);
        final UpdateBauCuaResultMsg msg = new UpdateBauCuaResultMsg();
        msg.dice1 = this.dices[0];
        msg.dice2 = this.dices[1];
        msg.dice3 = this.dices[2];
        msg.xPot = this.xPot;
        msg.xValue = this.xValue;
        this.sendMessageToRoom(msg);
        this.lichSuPhien.add(this.resultBC);
        if (this.lichSuPhien.size() > 30) {
            this.lichSuPhien.remove(0);
        }
    }

    private byte randomXPot() {
        final Random rd = new Random();
        return (byte) rd.nextInt(6);
    }

    private byte randomXValue() {
        final Random rd = new Random();
        final int n = rd.nextInt(20);
        if (n == 0) {
            return 3;
        }
        if (1 <= n && n <= 7) {
            return 2;
        }
        return 1;
    }

    public void getLichSuPhien(final User user) {
        final BauCuaLichSuPhienMsg msg = new BauCuaLichSuPhienMsg();
        msg.data = this.buildLichSuPhien();
        this.sendMessageToUser(msg, user);
    }

    private String buildLichSuPhien() {
        final StringBuilder builder = new StringBuilder();
        this.lichSuPhien.forEach(entry -> {
            builder.append(entry.dices[0]);
            builder.append(",");
            builder.append(entry.dices[1]);
            builder.append(",");
            builder.append(entry.dices[2]);
            builder.append(",");
            builder.append(entry.xPot);
            builder.append(",");
            builder.append(entry.xValue);
            builder.append(",");
        });
        if (builder.length() > 0) {
            builder.deleteCharAt(builder.length() - 1);
        }
        return builder.toString();
    }
}
