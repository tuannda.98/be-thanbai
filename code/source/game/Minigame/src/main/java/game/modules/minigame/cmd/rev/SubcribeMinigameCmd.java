// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.rev;

import java.nio.ByteBuffer;
import bitzero.server.extensions.data.DataCmd;
import bitzero.server.extensions.data.BaseCmd;

public class SubcribeMinigameCmd extends BaseCmd
{
    public short gameId;
    public short roomId;
    
    public SubcribeMinigameCmd(final DataCmd dataCmd) {
        super(dataCmd);
        this.unpackData();
    }
    
    public void unpackData() {
        final ByteBuffer bf = this.makeBuffer();
        this.gameId = this.readShort(bf);
        this.roomId = this.readShort(bf);
    }
}
