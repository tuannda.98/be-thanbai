// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.admin.cmd.rev;

import java.nio.ByteBuffer;
import bitzero.server.extensions.data.DataCmd;
import bitzero.server.extensions.data.BaseCmd;

public class AdminKickUserCmd extends BaseCmd
{
    public String userKicked;
    public byte reason;
    
    public AdminKickUserCmd(final DataCmd dataCmd) {
        super(dataCmd);
        this.unpackData();
    }
    
    public void unpackData() {
        final ByteBuffer bf = this.makeBuffer();
        this.userKicked = this.readString(bf);
        this.reason = bf.get();
    }
}
