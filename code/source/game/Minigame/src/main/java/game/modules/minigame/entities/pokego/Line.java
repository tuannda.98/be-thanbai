// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.entities.pokego;

import java.util.ArrayList;
import java.util.List;

public class Line
{
    private String name;
    private List<Cell> cells;
    
    public Line(final String name) {
        this.cells = new ArrayList<>();
        this.name = name;
    }
    
    public Line(final String name, final List<Cell> cells) {
        this.cells = new ArrayList<>();
        this.name = name;
        this.cells = cells;
    }
    
    public Line(final String name, final int r1, final int c1, final int r2, final int c2, final int r3, final int c3) {
        this.cells = new ArrayList<>();
        this.name = name;
        final Cell cell1 = new Cell(r1, c1);
        this.cells.add(cell1);
        final Cell cell2 = new Cell(r2, c2);
        this.cells.add(cell2);
        final Cell cell3 = new Cell(r3, c3);
        this.cells.add(cell3);
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public List<Cell> getCells() {
        return this.cells;
    }
    
    public void setCells(final List<Cell> cells) {
        this.cells = cells;
    }
    
    public Cell getCell(final int index) {
        return this.cells.get(index);
    }
    
    public Item getItem(final int index) {
        return this.cells.get(index).getItem();
    }
}
