// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.gameRoom.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class JoinGameRoomFailMsg extends BaseMsgEx
{
    public static final byte INFO_ERROR = 1;
    public static final byte ROOM_ERROR = 2;
    public static final byte MONEY_ERROR = 3;
    public static final byte JOIN_ERROR = 4;
    
    public JoinGameRoomFailMsg() {
        super(3004);
    }
    
    public byte[] createData() {
        final ByteBuffer buffer = this.makeBuffer();
        return this.packBuffer(buffer);
    }
}
