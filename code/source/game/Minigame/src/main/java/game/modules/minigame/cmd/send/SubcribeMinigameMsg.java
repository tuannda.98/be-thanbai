// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class SubcribeMinigameMsg extends BaseMsgEx
{
    public int gameId;
    public int moneyType;
    public long potValue;
    
    public SubcribeMinigameMsg() {
        super(2000);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        bf.putInt(this.gameId);
        bf.putInt(this.moneyType);
        return this.packBuffer(bf);
    }
}
