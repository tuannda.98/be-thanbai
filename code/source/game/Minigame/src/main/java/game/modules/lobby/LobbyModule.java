package game.modules.lobby;

import bitzero.server.BitZeroServer;
import bitzero.server.core.BZEventParam;
import bitzero.server.core.BZEventType;
import bitzero.server.core.IBZEvent;
import bitzero.server.entities.User;
import bitzero.server.exceptions.BZException;
import bitzero.server.extensions.BaseClientRequestHandler;
import bitzero.server.extensions.data.DataCmd;
import bitzero.util.ExtensionUtility;
import bitzero.util.common.business.Debug;
import casio.king365.core.HCMap;
import casio.king365.util.KingUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.vinplay.dal.dao.LogMoneyUserDao;
import com.vinplay.dal.dao.impl.LogMoneyUserDaoImpl;
import com.vinplay.dal.service.impl.AgentServiceImpl;
import com.vinplay.dal.service.impl.CacheServiceImpl;
import com.vinplay.dichvuthe.dao.RechargeDao;
import com.vinplay.dichvuthe.dao.impl.RechargeDaoImpl;
import com.vinplay.dichvuthe.response.*;
import com.vinplay.dichvuthe.service.CashOutService;
import com.vinplay.dichvuthe.service.RechargeService;
import com.vinplay.dichvuthe.service.impl.AlertServiceImpl;
import com.vinplay.dichvuthe.service.impl.CashOutServiceImpl;
import com.vinplay.dichvuthe.service.impl.RechargeServiceImpl;
import com.vinplay.dichvuthe.utils.DvtUtils;
import com.vinplay.usercore.dao.impl.OtpDaoImpl;
import com.vinplay.usercore.dao.impl.UserDaoImpl;
import com.vinplay.usercore.entities.TransferMoneyResponse;
import com.vinplay.usercore.entities.VippointResponse;
import com.vinplay.usercore.service.*;
import com.vinplay.usercore.service.impl.*;
import com.vinplay.usercore.utils.GameCommon;
import com.vinplay.usercore.utils.LuckyUtils;
import com.vinplay.usercore.utils.PartnerConfig;
import com.vinplay.usercore.utils.VippointUtils;
import com.vinplay.vbee.common.enums.Games;
import com.vinplay.vbee.common.enums.PhoneCardType;
import com.vinplay.vbee.common.enums.Platform;
import com.vinplay.vbee.common.enums.ProviderType;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.response.*;
import com.vinplay.vbee.common.utils.DateTimeUtils;
import com.vinplay.vbee.common.utils.VinPlayUtils;
import game.entities.QuotaResponse;
import game.modules.lobby.cmd.rev.*;
import game.modules.lobby.cmd.send.*;
import game.modules.minigame.utils.MiniGameUtils;
import game.utils.ConfigGame;
import game.utils.HuVangConfig;
import game.utils.ServerUtil;
import misa.api.service.imp.OtpServiceImp;
import org.bson.Document;
import org.json.JSONException;
import vn.yotel.yoker.util.Util;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class LobbyModule extends BaseClientRequestHandler {
    private static final String CURRENT_COMMAND = "cmd";
    private static final String CURRENT_OBJECT_COMMAND = "obj_cmd";
    private final UpdateCardTransactionTask cardTransactionTask;
    private final UserService userService;
    private final RechargeService rechargeService;
    private final RechargeDao rechargeDAO;
    private final GiftCodeService gfService;
    private final CashOutService cashOutService;
    private final OtpService otpService;
    private final misa.api.service.OtpService otpService2 = new OtpServiceImp();
    private final SecurityService securityService;
    private final VippointService vpService;
    private final Set<User> usersSubJackpot;
    private long countUpdateJackpot;
    private Date eventTimeStart;
    private Date eventTimeEnd;
    private final EventStartTask eventStartTask;
    private final EventEndTask eventEndTask;
    private final EventLuckyStartTask eventLuckyStartTask;
    private final EventLuckyEndTask eventLuckyEndTask;
    private long timeLucky;
    private final EventUnluckyTask eventUnluckyTask;
    private final EventluckyTask eventLuckyTask;
    private final EventX2EndTask eventX2EndTask;
    private final Runnable slotDailyTask;

    public LobbyModule() {
        this.cardTransactionTask = new UpdateCardTransactionTask();
        this.userService = new UserServiceImpl();
        this.rechargeService = new RechargeServiceImpl();
        this.rechargeDAO = new RechargeDaoImpl();
        this.gfService = new GiftCodeServiceImpl();
        this.cashOutService = new CashOutServiceImpl();
        this.otpService = new OtpServiceImpl();
        this.securityService = new SecurityServiceImpl();
        this.vpService = new VippointServiceImpl();
        this.usersSubJackpot = new HashSet<>();
        this.countUpdateJackpot = 0L;
        this.eventTimeStart = null;
        this.eventTimeEnd = null;
        this.eventStartTask = new EventStartTask();
        this.eventEndTask = new EventEndTask();
        this.eventLuckyStartTask = new EventLuckyStartTask();
        this.eventLuckyEndTask = new EventLuckyEndTask();
        this.eventUnluckyTask = new EventUnluckyTask();
        this.eventLuckyTask = new EventluckyTask();
        this.eventX2EndTask = new EventX2EndTask();
        this.slotDailyTask = new SlotDailyTask();
    }

    public void init() {
        super.init();
        this.getParentExtension().addEventListener(BZEventType.USER_DISCONNECT, this);
        TimerTask gameLoopTask = new TimerTask() {
            @Override
            public void run() {
                gameLoop();
            }
        };
        new Timer().scheduleAtFixedRate(gameLoopTask, Util.randInt(5_000, 10_000), 1_000);
//        BitZeroServer.getInstance().getTaskScheduler().scheduleAtFixedRate(this.cardTransactionTask, 30, 30, TimeUnit.SECONDS);
        try {
            this.initVP();
            MongoDBConnectionFactory.init();
        } catch (Exception e) {
            Debug.trace("init vippoint event error " + e);
        }
        try {
            LuckyUtils.initSlotMap();
            DvtUtils.initDVT(true);
        } catch (Exception e) {
            Debug.trace("init slot free error " + e);
        }
        try {
            PartnerConfig.ReadConfig();
//            PartnerConfig.ReadConfig();
        } catch (Exception e) {
            Debug.trace("init partnerconfig event error " + e);
        }
        final long currentTime = System.currentTimeMillis() / 1000L;
        final long endToday = DateTimeUtils.getEndTimeToDayAsLong() / 1000L;
        final int n = (int) (endToday - currentTime);
        BitZeroServer.getInstance().getTaskScheduler().schedule(this.slotDailyTask, n + 5, TimeUnit.SECONDS);
    }

    public void handleClientRequest(final User user, final DataCmd dataCmd) {
        switch (dataCmd.getId()) {
            case 20050: {
                this.getInfo(user, dataCmd);
                break;
            }
            case 20051: {
                this.getMoneyUse(user, dataCmd);
                break;
            }
            case 20000: {
                this.doiPass(user, dataCmd);
                break;
            }
            case 20001: {
                this.doiVippoint(user, dataCmd);
                break;
            }
            case 20002: {
                this.updateUserInfo(user, dataCmd);
                break;
            }
            case 20003: {
                this.updateEmail(user, dataCmd);
                break;
            }
            case 20004: {
                this.updateMobile(user, dataCmd);
                Debug.trace(dataCmd.readString());
                break;
            }
            case 20005: {
                this.activeEmail(user, dataCmd);
                break;
            }
            case 20006: {
                this.activeMobile(user, dataCmd);
                break;
            }
            case 20007: {
                this.updateNewMobile(user, dataCmd);
                break;
            }
            case 20008: {
                this.loginOtp(user, dataCmd);
                break;
            }
            case 20009: {
                this.ketSat(user, dataCmd);
                break;
            }
            case 20010: {
                this.gameConfig(user, dataCmd);
                break;
            }
            case 20011: {
                this.napXu(user, dataCmd);
                break;
            }
            case 20012: {
                this.napTheDienThoai(user, dataCmd);
                break;
            }
            case 20045: {
                this.napVinCard(user, dataCmd);
                break;
            }
            case 20046: {
                this.napMegaCard(user, dataCmd);
                break;
            }
            case 20013: {
                this.napQuaNganHang(user, dataCmd);
                break;
            }
            case 20014: {
                this.chuyenKhoan(user, dataCmd);
                break;
            }
//            case 20015: {
//                this.muaMaThe(user, dataCmd);
//                break;
//            }
//            case 20016: {
//                this.napTienDienThoai(user, dataCmd);
//                break;
//            }
            case 20017: {
                this.giftCode(user, dataCmd);
                break;
            }
            case 20018: {
                this.checkUser(user, dataCmd);
                break;
            }
//            case 20019: {
//                this.checkOtp(user, dataCmd);
//                break;
//            }
            case 20220: {
                this.sendOtp(user, dataCmd);
                break;
            }
            case 20102: {
                this.subscribeJackPot(user);
                break;
            }
            case 20103: {
                this.unSubscribeJackPot(user);
                break;
            }
            case 20104: {
                this.updateHuVang(user);
                break;
            }
            case 20037: {
                this.checkIAP(user, dataCmd);
                break;
            }
            case 20038: {
                this.resultIAP(user, dataCmd);
                break;
            }
            case 20039: {
                this.getEventInfo(user, dataCmd);
                break;
            }
            case 20040: {
                this.requestApiOTP(user, dataCmd);
                break;
            }
            case 20041: {
                this.confirmApiOTP(user, dataCmd);
                break;
            }
            case 20042: {
                this.playVQMM(user);
                break;
            }
            case 20043: {
                this.getVQVip(user);
                break;
            }
            case 20044: {
                this.playVQVip(user);
            }
            case 20200: {
                this.napTheDienThoaiGachthe(user, dataCmd);
                break;
            }
        }
    }

    private void playVQMM(final User user) {
        final LuckyServiceImpl service = new LuckyServiceImpl();
        final LuckyResponse response = service.getResultLuckyRotation(user.getName(), user.getIpAddress());
        final VQMMMsg msg = new VQMMMsg();
        final String errorCode = response.getErrorCode();
        switch (errorCode) {
            case "0": {
                msg.Error = 0;
                break;
            }
            case "3001": {
                msg.Error = 2;
                break;
            }
            case "3002": {
                msg.Error = 3;
                break;
            }
            default: {
                msg.Error = 1;
                break;
            }
        }
        msg.prizeVin = response.getResultVin();
        msg.prizeXu = response.getResultXu();
        msg.prizeSlot = response.getResultSlot();
        msg.remainCount = (short) response.getRotateCount();
        msg.currentMoneyVin = response.getCurrentMoneyVin();
        msg.currentMoneyXu = response.getCurrentMoneyXu();
        this.send(msg, user);
    }

    private void getVQVip(final User user) {
        final LuckyServiceImpl service = new LuckyServiceImpl();
        final LuckyVipResponse response = service.rotateLuckyVip(user.getName(), true);
        final GetVQVipMsg msg = new GetVQVipMsg();
        msg.remainCount = (short) response.getRotateCount();
        this.send(msg, user);
    }

    private void playVQVip(final User user) {
        final LuckyServiceImpl service = new LuckyServiceImpl();
        final LuckyVipResponse response = service.rotateLuckyVip(user.getName(), false);
        final VQVipMsg msg = new VQVipMsg();
        final String errorCode = response.getErrorCode();
        switch (errorCode) {
            case "0": {
                msg.Error = 0;
                break;
            }
            case "1201": {
                msg.Error = 2;
                break;
            }
            case "1202": {
                msg.Error = 3;
                break;
            }
            case "1203": {
                msg.Error = 4;
                break;
            }
            case "1204": {
                msg.Error = 5;
                break;
            }
            default: {
                msg.Error = 1;
                break;
            }
        }
        msg.prizeVin = response.getResultVin();
        msg.prizeMulti = (short) response.getResultMulti();
        msg.remainCount = (short) response.getRotateCount();
        msg.currentMoneyVin = response.getCurrentMoney();
        this.send(msg, user);
    }

    private void requestApiOTP(final User user, final DataCmd dataCmd) {
    }

    private void confirmApiOTP(final User user, final DataCmd dataCmd) {
    }

    private void checkIAP(final User user, final DataCmd dataCmd) {
        final CheckIAPCmd cmd = new CheckIAPCmd(dataCmd);
        final CheckIAPMsg msg = new CheckIAPMsg();
        msg.Error = this.rechargeService.checkRechargeIAP(user.getName(), cmd.productId);
        this.send(msg, user);
    }

    private void resultIAP(final User user, final DataCmd dataCmd) {
        final ResultIAPCmd cmd = new ResultIAPCmd(dataCmd);
        final ResultIAPMsg msg = new ResultIAPMsg();
        Debug.trace("nickname: " + user.getName());
        Debug.trace("signedData: " + cmd.signedData);
        Debug.trace("signature: " + cmd.signature);
        final RechargeIAPResponse rcRes = this.rechargeService.rechargeIAP(user.getName(), cmd.signedData, cmd.signature);
        msg.Error = (byte) rcRes.getCode();
        msg.productId = (byte) rcRes.getProductId();
        msg.currentMoney = rcRes.getCurrentMoney();
        this.send(msg, user);
    }

    private void getInfo(final User user, final DataCmd dataCmd) {
        final GetInfoMsg msg = new GetInfoMsg();
        final UserCacheModel userCache = this.userService.forceGetCachedUser(user.getName());
        try {
            if (userCache != null) {
                msg.Error = 0;
                msg.username = userCache.getUsername();
                msg.cmt = ((userCache.getIdentification() != null) ? userCache.getIdentification() : "");
                msg.mobile = ((userCache.getMobile() != null) ? userCache.getMobile() : "");
                msg.email = ((userCache.getEmail() != null) ? userCache.getEmail() : "");
                msg.mobileSecure = (byte) (userCache.isHasMobileSecurity() ? 1 : 0);
                msg.emailSecure = (byte) (userCache.isHasEmailSecurity() ? 1 : 0);
                msg.appSecure = (byte) (userCache.isHasAppSecurity() ? 1 : 0);
                msg.loginSecure = (byte) (userCache.isHasLoginSecurity() ? 1 : 0);
                msg.moneyLoginotp = userCache.getLoginOtp();
                final ObjectMapper mapper = new ObjectMapper();
                msg.configGame = mapper.writeValueAsString(this.securityService.getListGameBai(userCache.getStatus()));
            } else {
                msg.Error = 1;
            }
        } catch (Exception e) {
            Debug.trace("LobbyModule get info error: " + e);
            msg.Error = 1;
        }
        this.send(msg, user);
    }

    private void getMoneyUse(final User user, final DataCmd dataCmd) {
        final GetMoneyUseMsg msg = new GetMoneyUseMsg();
        msg.moneyUse = this.userService.getMoneyUserCache(user.getName(), "vin");
        this.send(msg, user);
    }

    private void doiPass(final User user, final DataCmd dataCmd) {
        final DoiPassCmd cmd = new DoiPassCmd(dataCmd);
        final byte res = this.securityService.changePassword(user.getName(), cmd.oldPass, cmd.newPass, true);
        if (res == 0) {
            user.setProperty("cmd", dataCmd.getId());
            user.setProperty("obj_cmd", cmd);
        }
        final DoiPassMsg msg = new DoiPassMsg();
        msg.Error = res;
        this.send(msg, user);
    }

    private void doiVippoint(final User user, final DataCmd dataCmd) {
        final DoiVippointCmd cmd = new DoiVippointCmd(dataCmd);
        final byte res = this.vpService.checkCashoutVP(user.getName());
        if (res == 0) {
            user.setProperty("cmd", dataCmd.getId());
            user.setProperty("obj_cmd", cmd);
        }
        final DoiVippointMsg msg = new DoiVippointMsg();
        msg.Error = res;
        this.send(msg, user);
    }

    private void updateUserInfo(final User user, final DataCmd dataCmd) {
        final UpdateUserInfoCmd cmd = new UpdateUserInfoCmd(dataCmd);
        if (Objects.equals(cmd.cmt, "")) {
            cmd.cmt = null;
        }
        if (Objects.equals(cmd.email, "")) {
            cmd.email = null;
        }
        final byte res = this.securityService.updateUserInfo(user.getName(), cmd.cmt, cmd.email, cmd.mobile);
        final UpdateUserInfoMsg msg = new UpdateUserInfoMsg();
        msg.Error = res;
        this.send(msg, user);
    }

    private void updateEmail(final User user, final DataCmd dataCmd) {
        final UpdateEmailCmd cmd = new UpdateEmailCmd(dataCmd);
        final byte res = this.securityService.updateEmail(user.getName(), cmd.email);
        final UpdateEmailMsg msg = new UpdateEmailMsg();
        msg.Error = res;
        this.send(msg, user);
    }

    private void updateMobile(final User user, final DataCmd dataCmd) {
        final UpdateMobileCmd cmd = new UpdateMobileCmd(dataCmd);
        final byte res = this.securityService.updateMobile(user.getName(), cmd.mobile);
        final UpdateMobileMsg msg = new UpdateMobileMsg();
        msg.Error = res;
        this.send(msg, user);
    }

    private void activeEmail(final User user, final DataCmd dataCmd) {
        final byte res = this.securityService.activeEmail(user.getName());
        final ActiveEmailMsg msg = new ActiveEmailMsg();
        msg.Error = res;
        this.send(msg, user);
    }

    private void activeMobile(final User user, final DataCmd dataCmd) {
        final ActiveMobileCmd cmd = new ActiveMobileCmd(dataCmd);
        final byte res = this.securityService.activeMobile(user.getName(), true);
        if (res == 0) {
            user.setProperty("cmd", dataCmd.getId());
            user.setProperty("obj_cmd", cmd);
        }
        final ActiveMobileMsg msg = new ActiveMobileMsg();
        msg.Error = res;
        this.send(msg, user);
    }

    private void updateNewMobile(final User user, final DataCmd dataCmd) {
        final UpdateNewMobileCmd cmd = new UpdateNewMobileCmd(dataCmd);
        final byte res = this.securityService.updateNewMobile(user.getName(), cmd.mobile, true);
        if (res == 0) {
            user.setProperty("cmd", dataCmd.getId());
            user.setProperty("obj_cmd", cmd);
        }
        final UpdateNewMobileMsg msg = new UpdateNewMobileMsg();
        msg.Error = res;
        this.send(msg, user);
    }

    private void loginOtp(final User user, final DataCmd dataCmd) {
        final LoginOtpCmd cmd = new LoginOtpCmd(dataCmd);
        user.setProperty("cmd", dataCmd.getId());
        user.setProperty("obj_cmd", cmd);
        final OtpServiceImpl otpService = new OtpServiceImpl();
        try {
            final int ret = otpService.sendVoiceOtp(user.getName(), "");
            if (ret != 0) {
                Debug.trace("Cannot send OTP message!");
            }
        } catch (Exception e) {
            Debug.trace("LobbyModule error: " + e);
        }
    }

    private void ketSat(final User user, final DataCmd dataCmd) {
        final KetSatCmd cmd = new KetSatCmd(dataCmd);
        final KetSatMsg msgx = new KetSatMsg();
        msgx.Error = 2;
        this.send(msgx, user);
        /*byte res = 1;
        if (cmd.type == 0) {
            final KetSatMsg msg = new KetSatMsg();
            final MoneyResponse moneyres = this.securityService.takeMoneyInSafe(user.getName(), cmd.moneyExchange, true);
            if (moneyres.getErrorCode().equals("0")) {
                res = 0;
                user.setProperty("cmd", dataCmd.getId());
                user.setProperty("obj_cmd", cmd);
            } else if (moneyres.getErrorCode().equals("1002")) {
                res = 2;
            }
            msg.moneyUse = moneyres.getMoneyUse();
            msg.safe = moneyres.getSafeMoney();
            msg.Error = res;
            this.send(msg, user);
        } else if (cmd.type == 1) {
            final ResultKetSatMsg ksMsg = new ResultKetSatMsg();
            final MoneyResponse moneyres = this.securityService.sendMoneyToSafe(user.getName(), cmd.moneyExchange, false);
            if (moneyres.getErrorCode().equals("0")) {
                res = 0;
            } else if (moneyres.getErrorCode().equals("1002")) {
                res = 2;
            }
            ksMsg.moneyUse = moneyres.getMoneyUse();
            ksMsg.safe = moneyres.getSafeMoney();
            ksMsg.currentMoney = moneyres.getCurrentMoney();
            ksMsg.Error = res;
            this.send(ksMsg, user);
        }*/
    }

    private void gameConfig(final User user, final DataCmd dataCmd) {
        final GameConfigCmd cmd = new GameConfigCmd(dataCmd);
        user.setProperty("cmd", dataCmd.getId());
        user.setProperty("obj_cmd", cmd);
    }

    private void napXu(final User user, final DataCmd dataCmd) {
        final NapXuCmd cmd = new NapXuCmd(dataCmd);
        final NapXuResponse response = this.userService.napXu(user.getName(), cmd.moneyVin, true);
        if (response.getResult() == 0) {
            user.setProperty("cmd", dataCmd.getId());
            user.setProperty("obj_cmd", cmd);
        }
        final NapXuMsg msg = new NapXuMsg();
        msg.Error = response.getResult();
        this.send(msg, user);
    }

    private void napTheDienThoai(final User user, final DataCmd dataCmd) {
        final GachTheDienThoaiCmd cmd = new GachTheDienThoaiCmd(dataCmd);
        final byte result = 1;
        RechargeResponse res = null;
        try {
            Debug.trace("Recharge error: " + PartnerConfig.CongGachThe + ":" + cmd.provider + ":" + cmd.serial + ":" + cmd.pin + ":" + cmd.menhgia);
            final Platform platform = Platform.find((String) user.getProperty("pf"));
            cmd.menhgia = cmd.menhgia.replace(".", "");
            cmd.menhgia = cmd.menhgia.replace(",", "");
            switch (PartnerConfig.CongGachThe.trim()) {
                case "GACHTHE":
//                    res = this.rechargeService.rechargeByGachThe(user.getName(), ProviderType.getProviderById(cmd.provider), cmd.serial, cmd.pin, cmd.menhgia, platform.getName(), user.getId());
                    break;
                case "NAPTIENGA":
//                    res = this.rechargeService.rechargeByNapTienGa(user.getName(), ProviderType.getProviderById(cmd.provider), cmd.serial, cmd.pin, cmd.menhgia, platform.getName(), user.getId());
                    break;
                case "MUACARD":
//                    res = this.rechargeService.rechargeByMuaCard(user.getName(), ProviderType.getProviderById(cmd.provider), cmd.serial, cmd.pin, cmd.menhgia, platform.getName(), user.getId());
                    break;
                case "MUACARD24H":
//                    res = this.rechargeService.rechargeByMuaCard24h(user.getName(), ProviderType.getProviderById(cmd.provider), cmd.serial, cmd.pin, cmd.menhgia, platform.getName(), user.getId());
                    break;
                case "ECARD":
//                    res = this.rechargeService.rechargeByECard(user.getName(), ProviderType.getProviderById(cmd.provider), cmd.serial, cmd.pin, cmd.menhgia, platform.getName(), user.getId());
                    break;
                default:
                    res = RechargeResponse.of(-1, 0L, 0, 0L);
                    break;
            }
        } catch (Exception e) {
            Debug.trace("Recharge error: " + e.getMessage());
        }
        final NapTheDienThoaiMsg msg = new NapTheDienThoaiMsg();
        if (res != null) {
            msg.Error = (byte) res.getCode();
            msg.currentMoney = res.getCurrentMoney();
            msg.numFail = res.getFail();
            msg.timeFail = res.getTime();
        } else {
            msg.Error = result;
        }
        Debug.trace("Recharge Gachthe error: " + msg.Error);
        this.send(msg, user);
    }

    private void napTheDienThoaiGachthe(final User user, final DataCmd dataCmd) {
        final GachTheDienThoaiCmd cmd = new GachTheDienThoaiCmd(dataCmd);
        final byte result = 1;
        RechargeResponse res = null;
        try {
            Debug.trace("Recharge error: " + cmd.provider + ":" + cmd.serial + ":" + cmd.pin + ":" + cmd.menhgia);
            final Platform platform = Platform.find((String) user.getProperty("pf"));
            cmd.menhgia = cmd.menhgia.replace(".", "");
            cmd.menhgia = cmd.menhgia.replace(",", "");
//            res = this.rechargeService.rechargeByGachThe(user.getName(), ProviderType.getProviderById(cmd.provider), cmd.serial, cmd.pin, cmd.menhgia, platform.getName(), user.getId());
        } catch (Exception e) {
            Debug.trace("Recharge error: " + e.getMessage());
        }
        final GachTheDienThoaiMsg msg = new GachTheDienThoaiMsg();
        if (res != null) {
            msg.Error = (byte) res.getCode();
            msg.currentMoney = res.getCurrentMoney();
            msg.numFail = res.getFail();
            msg.timeFail = res.getTime();
        } else {
            msg.Error = result;
        }
        Debug.trace("Recharge Gachthe error: " + msg.Error);
        this.send(msg, user);
    }

    private void napVinCard(final User user, final DataCmd dataCmd) {
        final NapVinCardCmd cmd = new NapVinCardCmd(dataCmd);
        final byte result = 1;
        RechargeResponse res = null;
        try {
            final Platform platform = Platform.find((String) user.getProperty("pf"));
//            res = this.rechargeService.rechargeByVinCard(user.getName(), cmd.serial, cmd.pin, platform.getName());
        } catch (Exception e) {
            Debug.trace("Recharge error: " + e.getMessage());
        }
        final NapVinCardMsg msg = new NapVinCardMsg();
        if (res != null) {
            msg.Error = (byte) res.getCode();
            msg.currentMoney = res.getCurrentMoney();
            msg.numFail = res.getFail();
            msg.timeFail = res.getTime();
        } else {
            msg.Error = result;
        }
        this.send(msg, user);
    }

    private void napMegaCard(final User user, final DataCmd dataCmd) {
        final NapMegaCardCmd cmd = new NapMegaCardCmd(dataCmd);
        final byte result = 1;
        RechargeResponse res = null;
        try {
            final Platform platform = Platform.find((String) user.getProperty("pf"));
//            res = this.rechargeService.rechargeByMegaCard(user.getName(), cmd.serial, cmd.pin, platform.getName());
        } catch (Exception e) {
            e.printStackTrace();
            Debug.trace("Recharge error: " + e.getMessage());
        }
        final NapMegaCardMsg msg = new NapMegaCardMsg();
        if (res != null) {
            msg.Error = (byte) res.getCode();
            msg.currentMoney = res.getCurrentMoney();
            msg.numFail = res.getFail();
            msg.timeFail = res.getTime();
        } else {
            msg.Error = result;
        }
        this.send(msg, user);
    }

    private void napQuaNganHang(final User user, final DataCmd dataCmd) {
        final NapQuaNganHangCmd cmd = new NapQuaNganHangCmd(dataCmd);
        final NapQuaNganHangMsg msg = new NapQuaNganHangMsg();
        try {
            final Platform platform = Platform.find((String) user.getProperty("pf"));
//            final I2BResponse res = this.rechargeService.rechargeByBank(user.getName(), cmd.money, cmd.bank, user.getIpAddress(), platform.getName());
//            msg.url = res.getUrl();
//            msg.Error = (byte) res.getCode();
        } catch (Exception e) {
            Debug.trace("LobbyModule nap qua ngan hang error: " + e);
            msg.Error = 1;
            msg.url = "";
        }
        this.send(msg, user);
    }

    private void chuyenKhoan(final User user, final DataCmd dataCmd) {
        final ChuyenKhoanCmd cmd = new ChuyenKhoanCmd(dataCmd);
        TransferMoneyResponse res;
        KingUtil.printLog("chuyenKHoan() cmd: "+cmd.toString());
        /*try {
            int rescheckOtp = otpService.checkOtp(cmd.otp, user.getName(), "0", null);
            if(rescheckOtp != 0){
                res = new TransferMoneyResponse((byte) 22, 0L, 0L);
                final ChuyenKhoanMsg msg = new ChuyenKhoanMsg();
                msg.Error = res.getCode();
                msg.moneyUse = res.getMoneyUse();
                this.send(msg, user);
                return;
            }
        } catch (Exception e){
            KingUtil.printException("chuyenKhoan()", e);
        }*/
        // final QuotaResponse checkQuota = this.CheckQuota(user.getName(), false);
        QuotaResponse checkQuota = this.CheckRechargeForTransferMoney(user.getName());
        KingUtil.printLog("Quota resultt:" + checkQuota);
        if (user.getName().equalsIgnoreCase(cmd.receiver)) {
            res = new TransferMoneyResponse((byte) 22, 0L, 0L);
        } else if (checkQuota.getCode() == 0) {
            res = this.userService.transferMoney(user.getName(), cmd.receiver, cmd.moneyExchange, cmd.description, false);
            if (res.getCode() == 0) {
                user.setProperty("cmd", dataCmd.getId());
                user.setProperty("obj_cmd", cmd);
            }
        } else {
            res = new TransferMoneyResponse((byte) 22, 0L, 0L);
        }
        KingUtil.printLog("TransferMoneyResponse: "+res.toJson());
        final ChuyenKhoanMsg msg = new ChuyenKhoanMsg();
        msg.Error = res.getCode();
        msg.moneyUse = res.getMoneyUse();
        this.send(msg, user);
    }

    private QuotaResponse CheckQuota(final String nick_name, final boolean seven_days) {
        final QuotaResponse response = new QuotaResponse();
        try {
            final UserModel userModel = this.userService.getUserByNickName(nick_name);
            if (userModel != null) {
                if (userModel.getDaily() == 1 || userModel.getDaily() == 2) {
                    response.setCode(0);
                    return response;
                }
                final LogMoneyUserDao logService = new LogMoneyUserDaoImpl();
                long total_giftcode_money = 0L;
                long total_user_receive = 0L;
                long total_agency_receive = 0L;
                long total_user_transfer = 0L;
                long total_agency_transfer = 0L;
                final AgentServiceImpl service = new AgentServiceImpl();
                final List<AgentResponse> agents = service.listAgent();
                final ArrayList<String> agentNames = new ArrayList<>();
                if (agents != null && agents.size() > 0) {
                    for (final AgentResponse agent : agents) {
                        agentNames.add(agent.nickName);
                    }
                }
                final List<LogUserMoneyResponse> resultGiftCode = logService.searchAllLogMoneyUser(nick_name, "GIFTCODE", seven_days);
                if (resultGiftCode != null && resultGiftCode.size() > 0) {
                    total_giftcode_money = resultGiftCode.stream().map(trans -> trans.moneyExchange).reduce(total_giftcode_money, Long::sum);
                }
                final List<LogUserMoneyResponse> resulReceive = logService.searchAllLogMoneyUser(nick_name, "RECEIVE", seven_days);
                if (resulReceive != null && resulReceive.size() > 0) {
                    for (final LogUserMoneyResponse trans2 : resulReceive) {
                        boolean matchAgent = false;
                        for (final String s : agentNames) {
                            if (trans2.description.contains(s)) {
                                matchAgent = true;
                                break;
                            }
                        }
                        if (matchAgent) {
                            total_agency_receive += trans2.moneyExchange;
                        } else {
                            total_user_receive += trans2.moneyExchange;
                        }
                    }
                }
                final List<LogUserMoneyResponse> resulTransfer = logService.searchAllLogMoneyUser(nick_name, "TRANSFER", seven_days);
                if (resulTransfer != null && resulTransfer.size() > 0) {
                    for (final LogUserMoneyResponse trans3 : resulTransfer) {
                        boolean matchAgent2 = false;
                        for (final String s2 : agentNames) {
                            if (trans3.description.contains(s2)) {
                                matchAgent2 = true;
                                break;
                            }
                        }
                        if (matchAgent2) {
                            total_agency_transfer += trans3.moneyExchange;
                        } else {
                            total_user_transfer += trans3.moneyExchange;
                        }
                    }
                }
                long total_recharge_card_money = 0L;
                final List<LogUserMoneyResponse> resultCard = logService.searchAllLogMoneyUser(nick_name, "CARD", seven_days);
                if (resultCard != null && resultCard.size() > 0) {
                    total_recharge_card_money = resultCard.stream().map(trans -> trans.moneyExchange).reduce(total_recharge_card_money, Long::sum);
                }
                long quota = 0L;
                quota += total_recharge_card_money;
                quota += total_agency_receive;
                quota += total_user_receive * 4L;
                quota += total_giftcode_money * 4L;
                quota += total_agency_transfer;
                quota += total_user_transfer;
                Debug.trace("Quota:" + quota);
                final long total_bet_money = -logService.getTotalBetWin(nick_name, "BET", null);
                final long total_win_money = logService.getTotalBetWin(nick_name, "WIN", null);
                final long total_bet_banca_money = -logService.getTotalBetWin(nick_name, "BET", "HamCaMap");
                final long total_bet_slot_money = -logService.getTotalBetWin(nick_name, "BET", "SLOT");
                long finalQuota = total_bet_money - total_bet_banca_money * 9L / 10L - total_bet_slot_money * 9L / 10L;
                final long manual_quota = userModel.getManual_quota();
                finalQuota += manual_quota;
                Debug.trace("Final quota :" + finalQuota);
                if (finalQuota > quota) {
                    response.setCode(0);
                } else {
                    response.setCode(1);
                }
                response.setTotal_agency_receive(total_agency_receive);
                response.setTotal_agency_transfer(total_agency_transfer);
                response.setTotal_bet_money(total_bet_money);
                response.setTotal_giftcode_money(total_giftcode_money);
                response.setTotal_recharge_card_money(total_recharge_card_money);
                response.setTotal_user_receive(total_user_receive);
                response.setTotal_user_transfer(total_user_transfer);
                response.setTotal_win_money(total_win_money);
            }
        } catch (Exception ex) {
            response.setCode(-1);
            Debug.trace("Check quote error : " + ex.getMessage());
        }
        return response;
    }

    private QuotaResponse CheckRechargeForTransferMoney(final String nick_name) {
        final QuotaResponse response = new QuotaResponse();
        try {
            long cashin = userService.getDbCashinMoneyAmount(nick_name);
            if(cashin > 100000)
                response.setCode(0);
            else response.setCode(1);
        } catch (Exception ex) {
            response.setCode(-1);
            Debug.trace("Check quote error : " + ex.getMessage());
        }
        return response;
    }
//
//    private void muaMaThe(final User user, final DataCmd dataCmd) {
//        final MuaMaTheCmd cmd = new MuaMaTheCmd(dataCmd);
//        final MuaMaTheMsg msg = new MuaMaTheMsg();
//        try {
//            final SoftpinResponse res = this.cashOutService.cashOutByCard(user.getName(), ProviderType.getProviderById(cmd.provider), PhoneCardType.getPhoneCardById(cmd.amount), cmd.quantity, true);
//            msg.Error = (byte) res.getCode();
//            if (res.getCode() == 0) {
//                user.setProperty("cmd", dataCmd.getId());
//                user.setProperty("obj_cmd", cmd);
//            }
//        } catch (Exception e) {
//            msg.Error = 1;
//            Debug.trace("Mua ma the error : " + e.getMessage());
//        }
//        this.send(msg, user);
//    }
//
//    private void napTienDienThoai(final User user, final DataCmd dataCmd) {
//        final NapTienDienThoaiCmd cmd = new NapTienDienThoaiCmd(dataCmd);
//        final NapTienDienThoaiMsg msg = new NapTienDienThoaiMsg();
//        try {
//            final CashoutResponse res = this.cashOutService.cashOutByTopUp(user.getName(), cmd.mobile, PhoneCardType.getPhoneCardById(cmd.amount), cmd.type, true);
//            msg.Error = (byte) res.getCode();
//            if (res.getCode() == 0) {
//                user.setProperty("cmd", dataCmd.getId());
//                user.setProperty("obj_cmd", cmd);
//            }
//        } catch (Exception e) {
//            msg.Error = 1;
//            Debug.trace("Mua ma the error : " + e.getMessage());
//        }
//        this.send(msg, user);
//    }

    private void giftCode(final User user, final DataCmd dataCmd) {
        final GiftCodeCmd cmd = new GiftCodeCmd(dataCmd);
        final GiftCodeMsg msg = new GiftCodeMsg();
        try {
            Debug.trace("GiftCode:" + ConfigGame.getValueString("special_gift_code"));
            Debug.trace("GiftCodeBlock:" + ConfigGame.getValueString("special_gift_code_block"));
            Debug.trace("GiftCodeUseCount:" + ConfigGame.getValueString("special_gift_code_use_count"));
            Debug.trace("GiftCodeAmount:" + ConfigGame.getValueString("special_gift_code_amount"));
            final String giftCodes = ConfigGame.getValueString("special_gift_code");
            final String giftCodeAmounts = ConfigGame.getValueString("special_gift_code_amount");
            final String giftCodeBlocks = ConfigGame.getValueString("special_gift_code_block");
            final String giftCodeUseCounts = ConfigGame.getValueString("special_gift_code_use_count");
            if (giftCodes == null || "".equals(giftCodes)) {
                final GiftCodeUpdateResponse response = this.gfService.updateGiftCode(user.getName(), cmd.giftCode);
                if (response.isSuccess()) {
                    msg.currentMoneyVin = response.currentMoneyVin;
                    msg.currentMoneyXu = response.currentMoneyXu;
                    msg.moneyGiftCodeVin = response.moneyGiftCodeVin;
                    msg.moneyGiftCodeXu = response.moneyGiftCodeXu;
                }
                msg.Error = this.parseErrorCodeGiftCode(response.getErrorCode());
            } else if (!giftCodes.contains(",")) {
                final GiftCodeUpdateResponse response = this.gfService.updateGiftCode(user.getName(), cmd.giftCode);
                if (response.isSuccess()) {
                    msg.currentMoneyVin = response.currentMoneyVin;
                    msg.currentMoneyXu = response.currentMoneyXu;
                    msg.moneyGiftCodeVin = response.moneyGiftCodeVin;
                    msg.moneyGiftCodeXu = response.moneyGiftCodeXu;
                }
                msg.Error = this.parseErrorCodeGiftCode(response.getErrorCode());
            } else {
                final String[] codes = giftCodes.split(",");
                final String[] blocks = giftCodeBlocks.split(",");
                final String[] amounts = giftCodeAmounts.split(",");
                final String[] useCounts = giftCodeUseCounts.split(",");
                if (codes.length == amounts.length && codes.length == blocks.length && codes.length == useCounts.length) {
                    int index = -1;
                    for (int i = 0; i < codes.length; ++i) {
                        if (codes[i].equals(cmd.giftCode)) {
                            index = i;
                            break;
                        }
                    }
                    if (index > -1) {
                        if ("0".equals(blocks[index])) {
                            final GiftCodeUpdateResponse response2 = this.gfService.updateSpecialGiftCode(
                                    user.getName(), cmd.giftCode, Integer.parseInt(amounts[index]), Integer.parseInt(useCounts[index]));
                            if (response2.isSuccess()) {
                                msg.currentMoneyVin = response2.currentMoneyVin;
                                msg.currentMoneyXu = response2.currentMoneyXu;
                                msg.moneyGiftCodeVin = response2.moneyGiftCodeVin;
                                msg.moneyGiftCodeXu = response2.moneyGiftCodeXu;
                            }
                            msg.Error = this.parseErrorCodeGiftCode(response2.getErrorCode());
                        } else {
                            msg.Error = 0;
                        }
                    } else {
                        final GiftCodeUpdateResponse response2 = this.gfService.updateGiftCode(user.getName(), cmd.giftCode);
                        if (response2.isSuccess()) {
                            msg.currentMoneyVin = response2.currentMoneyVin;
                            msg.currentMoneyXu = response2.currentMoneyXu;
                            msg.moneyGiftCodeVin = response2.moneyGiftCodeVin;
                            msg.moneyGiftCodeXu = response2.moneyGiftCodeXu;
                        }
                        msg.Error = this.parseErrorCodeGiftCode(response2.getErrorCode());
                    }
                } else {
                    final GiftCodeUpdateResponse response3 = this.gfService.updateGiftCode(user.getName(), cmd.giftCode);
                    if (response3.isSuccess()) {
                        msg.currentMoneyVin = response3.currentMoneyVin;
                        msg.currentMoneyXu = response3.currentMoneyXu;
                        msg.moneyGiftCodeVin = response3.moneyGiftCodeVin;
                        msg.moneyGiftCodeXu = response3.moneyGiftCodeXu;
                    }
                    msg.Error = this.parseErrorCodeGiftCode(response3.getErrorCode());
                }
            }
            if (msg.Error == 2) {
//                try {
//                    final HttpClient client = HttpClientBuilder.create().build();
//                    String url = "";
//                    if ("XXENG".equals(PartnerConfig.Client)) {
//                        url = "https://bot.xxeng.vip/botteleapi";
//                    } else if ("R68".equals(PartnerConfig.Client)) {
//                        url = "https://r99.bar/bottele/r68";
//                    } else {
//                        url = "https://bot.xxeng.vip/botteleapi";
//                    }
//                    final HttpPost request = new HttpPost(url + "/rpadmin/ozawasecret1243/2");
//                    request.addHeader("Content-Type", "application/json");
//                    final JSONObject obj = new JSONObject();
//                    final String sender = user.getName();
//                    String serverName = "xxeng";
//                    final UserModel senderModel = this.userService.getUserByNickName(user.getName());
//                    if (senderModel != null && senderModel.getClient() != null && !senderModel.getClient().equals("") && senderModel.getClient().equals("M")) {
//                        serverName = "manVip";
//                    }
//                    obj.put("nick_name", sender);
//                    obj.put("gift_code", cmd.giftCode);
//                    obj.put("gift_code_money", msg.moneyGiftCodeVin);
//                    obj.put("current_Money", msg.currentMoneyVin);
//                    obj.put("serverName", serverName);
//                    final long date = System.currentTimeMillis();
//                    final int offset = TimeZone.getDefault().getOffset(date);
//                    obj.put("created_time", date + offset);
//                    final StringEntity requestEntity = new StringEntity(obj.toString(), "UTF-8");
//                    request.setEntity(requestEntity);
//                    final HttpResponse response4 = client.execute(request);
//                    final BufferedReader rd = new BufferedReader(new InputStreamReader(response4.getEntity().getContent()));
//                    final StringBuilder result = new StringBuilder();
//                    String line = "";
//                    while ((line = rd.readLine()) != null) {
//                        result.append(line);
//                    }
//                    Debug.trace("LobbyModule bot tele response: " + result);
//                } catch (Exception ex) {
//                    Debug.trace("LobbyModule error: " + ex);
//                }
            }
            Debug.trace("LobbyModule giftcode error: " + msg.Error);
        } catch (Exception e) {
            e.printStackTrace();
            Debug.trace("LobbyModule giftcode error: " + e);
            msg.Error = 1;
        }
        this.send(msg, user);
    }

    private void checkUser(final User user, final DataCmd dataCmd) {
        final CheckUserCmd cmd = new CheckUserCmd(dataCmd);
        final byte type1 = this.userService.checkUser(user.getName());
        final byte type2 = this.userService.checkUser(cmd.nickname);
        final CheckUserMsg msg = new CheckUserMsg();
        if (type2 == -1) {
            msg.Error = 0;
        } else {
            msg.Error = 1;
            msg.fee = this.userService.calFeeTransfer(type1, type2);
        }
        msg.type = type2;
        this.send(msg, user);
    }

    private void sendOtp(final User user, final DataCmd dataCmd) {
        final SendOTPCmd cmd = new SendOTPCmd(dataCmd);
        final SendOTPMsg msg = new SendOTPMsg();
        try {
            final int ret = this.otpService.sendVoiceOtp(user.getName(), "");
            msg.Error = (byte) ret;
        } catch (Exception e) {
            Debug.trace("LobbyModule error: " + e);
        }
        this.send(msg, user);
    }

//    private void checkOtp(final User user, final DataCmd dataCmd) {
//        final OtpCmd cmd = new OtpCmd(dataCmd);
//        final OTPMsg msg = new OTPMsg();
//        try {
//            final Object obj = user.getProperty("cmd");
//            final Object objCmd = user.getProperty("obj_cmd");
//            final short cmdId = (short) obj;
//            if (obj != null && objCmd != null) {
//                int code = 3;
//                if (cmdId == 20027) {
//                    final UpdateNewMobileCmd unmCmd = (UpdateNewMobileCmd) objCmd;
//                    if (cmd.type == 0) {
//                        code = this.otpService.checkOtp(cmd.otp, user.getName(), String.valueOf(cmd.type), unmCmd.mobile);
//                    }
//                } else if (cmdId == 20006) {
//                    if (cmd.type == 0) {
//                        code = this.otpService.checkOtp(cmd.otp, user.getName(), String.valueOf(cmd.type), null);
//                    }
//                } else {
//                    code = this.otpService.checkOtp(cmd.otp, user.getName(), String.valueOf(cmd.type), null);
//                }
//                Debug.trace("Verify otp code: " + code);
//                if (code == 0) {
//                    msg.Error = 0;
//                    this.send(msg, user);
//                    int res = 1;
//                    switch (cmdId) {
//                        case 20000: {
//                            final DoiPassCmd pCmd = (DoiPassCmd) objCmd;
//                            final ResultDoiPassMsg pMsg = new ResultDoiPassMsg();
//                            res = this.securityService.changePassword(user.getName(), pCmd.oldPass, pCmd.newPass, false);
//                            pMsg.Error = ((res == 0) ? ((byte) 0) : ((byte) 1));
//                            this.send(pMsg, user);
//                            break;
//                        }
//                        case 20001: {
//                            final ResultDoiVippointMsg vpMsg = new ResultDoiVippointMsg();
//                            final VippointResponse vpRes = this.vpService.cashoutVP(user.getName());
//                            if (vpRes.getErrorCode().equals("0")) {
//                                vpMsg.currentMoney = vpRes.getCurrentMoney();
//                                vpMsg.moneyAdd = vpRes.getMoneyAdd();
//                                vpMsg.Error = 0;
//                            } else {
//                                vpMsg.Error = 1;
//                            }
//                            this.send(vpMsg, user);
//                            break;
//                        }
//                        case 20006: {
//                            final ResultActiveMobileMsg amMsg = new ResultActiveMobileMsg();
//                            res = this.securityService.activeMobile(user.getName(), false);
//                            amMsg.Error = ((res == 0) ? ((byte) 0) : ((byte) 1));
//                            this.send(amMsg, user);
//                            break;
//                        }
//                        case 20007: {
//                            final UpdateNewMobileCmd unmCmd2 = (UpdateNewMobileCmd) objCmd;
//                            user.setProperty("cmd", 20027);
//                            user.setProperty("obj_cmd", unmCmd2);
//                            final ResultUpdateNewMobileMsg unmMsg = new ResultUpdateNewMobileMsg();
//                            unmMsg.Error = 0;
//                            this.send(unmMsg, user);
//                            break;
//                        }
//                        case 20027: {
//                            final UpdateNewMobileCmd anmCmd = (UpdateNewMobileCmd) objCmd;
//                            final ResultActiveNewMobileMsg anmMsg = new ResultActiveNewMobileMsg();
//                            res = this.securityService.updateNewMobile(user.getName(), anmCmd.mobile, false);
//                            anmMsg.Error = ((res == 0) ? ((byte) 0) : ((byte) 1));
//                            this.send(anmMsg, user);
//                            break;
//                        }
//                        case 20008: {
//                            final LoginOtpCmd loCmd = (LoginOtpCmd) objCmd;
//                            res = this.securityService.loginWithOTP(user.getName(), loCmd.money, loCmd.type);
//                            final LoginOtpMsg loMsg = new LoginOtpMsg();
//                            loMsg.Error = ((res == 0) ? ((byte) 0) : ((byte) 1));
//                            this.send(loMsg, user);
//                            break;
//                        }
//                        case 20010: {
//                            final GameConfigCmd gcCmd = (GameConfigCmd) objCmd;
//                            res = this.securityService.configGame(user.getName(), gcCmd.games);
//                            final GameConfigMsg gcMsg = new GameConfigMsg();
//                            gcMsg.Error = ((res == 0) ? ((byte) 0) : ((byte) 1));
//                            this.send(gcMsg, user);
//                            break;
//                        }
//                        case 20009: {
//                            final KetSatCmd ksCmd = (KetSatCmd) objCmd;
//                            final ResultKetSatMsg ksMsg = new ResultKetSatMsg();
//                            if (ksCmd.type == 0) {
//                                final MoneyResponse moneyres = this.securityService.takeMoneyInSafe(user.getName(), ksCmd.moneyExchange, false);
//                                if (moneyres.getErrorCode().equals("0")) {
//                                    res = 0;
//                                } else if (moneyres.getErrorCode().equals("1002")) {
//                                    res = 2;
//                                }
//                                ksMsg.moneyUse = moneyres.getMoneyUse();
//                                ksMsg.safe = moneyres.getSafeMoney();
//                                ksMsg.currentMoney = moneyres.getCurrentMoney();
//                            }
//                            ksMsg.Error = (byte) res;
//                            this.send(ksMsg, user);
//                            break;
//                        }
//                        case 20011: {
//                            final NapXuCmd nxCmd = (NapXuCmd) objCmd;
//                            final NapXuResponse response = this.userService.napXu(user.getName(), nxCmd.moneyVin, false);
//                            final ResultNapXuMsg rnxMsg = new ResultNapXuMsg();
//                            rnxMsg.Error = response.getResult();
//                            rnxMsg.currentMoneyVin = response.getCurrentMoneyVin();
//                            rnxMsg.currentMoneyXu = response.getCurrentMoneyXu();
//                            this.send(rnxMsg, user);
//                            break;
//                        }
//                        case 20014: {
////                            final ChuyenKhoanCmd ckCmd = (ChuyenKhoanCmd) objCmd;
////                            final TransferMoneyResponse tmres = this.userService.transferMoney(user.getName(), ckCmd.receiver, ckCmd.moneyExchange, ckCmd.description, false);
////                            final ResultChuyenKhoanMsg ckmsg = new ResultChuyenKhoanMsg();
////                            if (tmres.getCode() == 0) {
////                                ckmsg.Error = 0;
////                                final game.modules.minigame.cmd.send.UpdateUserInfoMsg uimsg = new game.modules.minigame.cmd.send.UpdateUserInfoMsg();
////                                uimsg.moneyType = 1;
////                                uimsg.currentMoney = tmres.getCurrentMoneyReceive();
////                                MiniGameUtils.sendMessageToUser(uimsg, tmres.getNicknameReceive());
////                                try {
////                                    final HttpClient client = HttpClientBuilder.create().build();
////                                    String url = "";
////                                    if ("XXENG".equals(PartnerConfig.Client)) {
////                                        url = "https://bot.xxeng.vip/botteleapi";
////                                    } else if ("R68".equals(PartnerConfig.Client)) {
////                                        url = "https://r99.bar/bottele/r68";
////                                    } else {
////                                        url = "https://bot.xxeng.vip/botteleapi";
////                                    }
////                                    final HttpPost request = new HttpPost(url + "/rpadmin/ozawasecret1243/3");
////                                    request.addHeader("Content-Type", "application/json");
////                                    final JSONObject objSend = new JSONObject();
////                                    final String sender = user.getName();
////                                    String serverName = "xxeng";
////                                    final UserModel senderModel = this.userService.getUserByNickName(user.getName());
////                                    if (senderModel != null && senderModel.getClient() != null && !senderModel.getClient().equals("") && senderModel.getClient().equals("M")) {
////                                        serverName = "manVip";
////                                    }
////                                    objSend.put("sender_nick_name", sender);
////                                    final String receiver = ckCmd.receiver;
////                                    objSend.put("receiver_nick_name", receiver);
////                                    objSend.put("money", ckCmd.moneyExchange);
////                                    objSend.put("description", ckCmd.description);
////                                    objSend.put("serverName", serverName);
////                                    final long date = System.currentTimeMillis();
////                                    final int offset = TimeZone.getDefault().getOffset(date);
////                                    objSend.put("created_time", date + offset);
////                                    if (ckCmd.moneyExchange >= 2000000L) {
////                                        objSend.put("need_check", true);
////                                        final QuotaResponse checkQuota = this.CheckQuota(user.getName(), true);
////                                        objSend.put("total_angecy_receive", checkQuota.getTotal_agency_receive());
////                                        objSend.put("total_agency_transfer", checkQuota.getTotal_agency_transfer());
////                                        objSend.put("total_bet_money", checkQuota.getTotal_bet_money());
////                                        objSend.put("total_giftcode_money", checkQuota.getTotal_giftcode_money());
////                                        objSend.put("total_recharge_money", checkQuota.getTotal_recharge_card_money());
////                                        objSend.put("total_user_receive", checkQuota.getTotal_user_receive());
////                                        objSend.put("total_user_transfer", checkQuota.getTotal_user_transfer());
////                                        objSend.put("total_win_money", checkQuota.getTotal_win_money());
////                                    }
////                                    final StringEntity requestEntity = new StringEntity(objSend.toString(), "UTF-8");
////                                    request.setEntity(requestEntity);
////                                    final HttpResponse response2 = client.execute(request);
////                                    final BufferedReader rd = new BufferedReader(new InputStreamReader(response2.getEntity().getContent()));
////                                    final StringBuilder result = new StringBuilder();
////                                    String line = "";
////                                    while ((line = rd.readLine()) != null) {
////                                        result.append(line);
////                                    }
////                                    Debug.trace("LobbyModule bot tele response: " + result);
////                                } catch (Exception ex) {
////                                    Debug.trace("LobbyModule error: " + ex);
////                                }
////                                final UserModel userReceive = this.userService.getUserByNickName(ckCmd.receiver);
////                                Label_2249:
////                                {
////                                    if (userReceive != null) {
////                                        if (userReceive.getDaily() != 1) {
////                                            if (userReceive.getDaily() != 2) {
////                                                break Label_2249;
////                                            }
////                                        }
////                                        try {
////                                            final HttpClient httpClient = HttpClientBuilder.create().build();
////                                            String url2 = "";
////                                            if ("XXENG".equals(PartnerConfig.Client)) {
////                                                url2 = "https://bot.xxeng.vip/botteleapi";
////                                            } else if ("R68".equals(PartnerConfig.Client)) {
////                                                url2 = "https://r99.bar/bottele/r68";
////                                            } else {
////                                                url2 = "https://bot.xxeng.vip/botteleapi";
////                                            }
////                                            final HttpPost request2 = new HttpPost(url2 + "/rpadmin/ozawasecret1243/10");
////                                            request2.addHeader("Content-Type", "application/json");
////                                            final JSONObject objSend2 = new JSONObject();
////                                            final String sender2 = user.getName();
////                                            String serverName2 = "xxeng";
////                                            final UserModel senderModel2 = this.userService.getUserByNickName(user.getName());
////                                            if (senderModel2 != null && senderModel2.getClient() != null && !senderModel2.getClient().equals("") && senderModel2.getClient().equals("M")) {
////                                                serverName2 = "manVip";
////                                            }
////                                            objSend2.put("sender_nick_name", sender2);
////                                            final String receiver2 = ckCmd.receiver;
////                                            objSend2.put("receiver_nick_name", receiver2);
////                                            objSend2.put("receiver_mobile", userReceive.getMobile());
////                                            objSend2.put("money", ckCmd.moneyExchange);
////                                            objSend2.put("description", ckCmd.description);
////                                            objSend2.put("is_agent", true);
////                                            objSend2.put("serverName", serverName2);
////                                            final long currentMoneyReceive = userReceive.getCurrentMoney("vin");
////                                            objSend2.put("previous_money", currentMoneyReceive - ckCmd.moneyExchange);
////                                            objSend2.put("current_money", currentMoneyReceive);
////                                            final long date2 = System.currentTimeMillis();
////                                            final int offset2 = TimeZone.getDefault().getOffset(date2);
////                                            objSend2.put("created_time", date2 + offset2);
////                                            final StringEntity requestEntity2 = new StringEntity(objSend2.toString(), "UTF-8");
////                                            request2.setEntity(requestEntity2);
////                                            final HttpResponse response3 = httpClient.execute(request2);
////                                            final BufferedReader rd2 = new BufferedReader(new InputStreamReader(response3.getEntity().getContent()));
////                                            final StringBuilder result2 = new StringBuilder();
////                                            String line2 = "";
////                                            while ((line2 = rd2.readLine()) != null) {
////                                                result2.append(line2);
////                                            }
////                                            Debug.trace("LobbyModule bot tele response: " + result2);
////                                        } catch (Exception ex2) {
////                                            Debug.trace("LobbyModule error: " + ex2);
////                                        }
////                                    }
////                                }
////                            } else {
////                                ckmsg.Error = 1;
////                            }
////                            ckmsg.moneyUse = tmres.getMoneyUse();
////                            ckmsg.currentMoney = tmres.getCurrentMoney();
////                            this.send(ckmsg, user);
//                            break;
//                        }
//                        case 20015: {
//                            final MuaMaTheCmd mmtCmd = (MuaMaTheCmd) objCmd;
//                            final SoftpinResponse sfres = this.cashOutService.cashOutByCard(user.getName(), ProviderType.getProviderById(mmtCmd.provider), PhoneCardType.getPhoneCardById(mmtCmd.amount), mmtCmd.quantity, false);
//                            final ResultMuaMaTheMsg mtmsg = new ResultMuaMaTheMsg();
//                            mtmsg.Error = (byte) sfres.getCode();
//                            mtmsg.currentMoney = sfres.getCurrentMoney();
//                            mtmsg.softpin = sfres.getSoftpin();
//                            this.send(mtmsg, user);
//                            break;
//                        }
//                        case 20016: {
//                            final NapTienDienThoaiCmd ntdtCmd = (NapTienDienThoaiCmd) objCmd;
//                            final CashoutResponse cres = this.cashOutService.cashOutByTopUp(user.getName(), ntdtCmd.mobile, PhoneCardType.getPhoneCardById(ntdtCmd.amount), ntdtCmd.type, false);
//                            final ResultNapTienDienThoaiMsg ntdtmsg = new ResultNapTienDienThoaiMsg();
//                            ntdtmsg.Error = (byte) cres.getCode();
//                            ntdtmsg.currentMoney = cres.getCurrentMoney();
//                            this.send(ntdtmsg, user);
//                            break;
//                        }
//                    }
//                } else {
//                    msg.Error = (byte) code;
//                    this.send(msg, user);
//                }
//            } else {
//                msg.Error = 2;
//                this.send(msg, user);
//            }
//        } catch (Exception e) {
//            msg.Error = 2;
//            this.send(msg, user);
//            Debug.trace("LobbyModule check otp error: " + e);
//        }
//    }

    private byte parseErrorCodeGiftCode(final String errorCode) {
        if (errorCode.equals("0")) {
            return 2;
        }
        if (errorCode.equals("10002")) {
            return 1;
        }
        if (errorCode.equals("10003")) {
            return 3;
        }
        if (errorCode.equals("10004")) {
            return 4;
        }
        return 0;
    }

    public void handleServerEvent(final IBZEvent ibzevent) throws BZException {
        if (ibzevent.getType() == BZEventType.USER_DISCONNECT) {
            final User user = (User) ibzevent.getParameter(BZEventParam.USER);
            this.unSubscribeJackPot(user);
        }
    }

    private void subscribeJackPot(final User user) {
        synchronized (this.usersSubJackpot) {
            this.usersSubJackpot.add(user);
        }
    }

    private void unSubscribeJackPot(final User user) {
        synchronized (this.usersSubJackpot) {
            this.usersSubJackpot.remove(user);
        }
    }

    public void updateJackpot() {
        try {
            final CacheServiceImpl cacheService = new CacheServiceImpl();
            final int miniPoker100 = cacheService.getValueInt(Games.MINI_POKER.getName() + "_vin_100");
            final int miniPoker101 = cacheService.getValueInt(Games.MINI_POKER.getName() + "_vin_1000");
            final int miniPoker102 = cacheService.getValueInt(Games.MINI_POKER.getName() + "_vin_10000");
            final int pokeGo100 = cacheService.getValueInt(Games.POKE_GO.getName() + "_vin_100");
            final int pokeGo101 = cacheService.getValueInt(Games.POKE_GO.getName() + "_vin_1000");
            final int pokeGo102 = cacheService.getValueInt(Games.POKE_GO.getName() + "_vin_10000");
            final int khoBau100 = cacheService.getValueInt(Games.KHO_BAU.getName() + "_vin_100");
            final int khoBau101 = cacheService.getValueInt(Games.KHO_BAU.getName() + "_vin_1000");
            final int khoBau102 = cacheService.getValueInt(Games.KHO_BAU.getName() + "_vin_10000");
            final int ndv100 = cacheService.getValueInt(Games.NU_DIEP_VIEN.getName() + "_vin_100");
            final int ndv101 = cacheService.getValueInt(Games.NU_DIEP_VIEN.getName() + "_vin_1000");
            final int ndv102 = cacheService.getValueInt(Games.NU_DIEP_VIEN.getName() + "_vin_10000");
            final int avengers100 = cacheService.getValueInt(Games.AVENGERS.getName() + "_vin_100");
            final int avengers101 = cacheService.getValueInt(Games.AVENGERS.getName() + "_vin_1000");
            final int avengers102 = cacheService.getValueInt(Games.AVENGERS.getName() + "_vin_10000");
            final int vqv100 = cacheService.getValueInt(Games.VUONG_QUOC_VIN.getName() + "_vin_100");
            final int vqv101 = cacheService.getValueInt(Games.VUONG_QUOC_VIN.getName() + "_vin_1000");
            final int vqv102 = cacheService.getValueInt(Games.VUONG_QUOC_VIN.getName() + "_vin_10000");
            final int fish100 = cacheService.getValueInt(Games.HAM_CA_MAP.getName() + "_vin_100");
            final int fish101 = cacheService.getValueInt(Games.HAM_CA_MAP.getName() + "_vin_1000");
            final UpdateJackpotMsg msg = new UpdateJackpotMsg();
            msg.potMiniPoker100 = miniPoker100;
            msg.potMiniPoker1000 = miniPoker101;
            msg.potMiniPoker10000 = miniPoker102;
            msg.potPokeGo100 = pokeGo100;
            msg.potPokeGo1000 = pokeGo101;
            msg.potPokeGo10000 = pokeGo102;
            msg.potKhoBau100 = khoBau100;
            msg.potKhoBau1000 = khoBau101;
            msg.potKhoBau10000 = khoBau102;
            msg.potNDV100 = ndv100;
            msg.potNDV1000 = ndv101;
            msg.potNDV10000 = ndv102;
            msg.potAvengers100 = avengers100;
            msg.potAvengers1000 = avengers101;
            msg.potAvengers10000 = avengers102;
            msg.vqv100 = vqv100;
            msg.vqv1000 = vqv101;
            msg.vqv10000 = vqv102;
            msg.fish100 = fish100;
            msg.fish1000 = fish101;
            for (final User user : this.usersSubJackpot) {
                if (user == null) {
                    continue;
                }
                this.send(msg, user);
            }
        } catch (Exception e) {
            Debug.trace("Update jackpot exception: " + e.getMessage());
        }
    }

    public void updateHuVang(final User user) {
        final HuVangMsg msg = new HuVangMsg();
        msg.huBaCay = HuVangConfig.instance().getThoiGianHuVang(Games.BA_CAY.getName());
        msg.huBaiCao = HuVangConfig.instance().getThoiGianHuVang(Games.BAI_CAO.getName());
        msg.huBinh = HuVangConfig.instance().getThoiGianHuVang(Games.BINH.getName());
        msg.huSam = HuVangConfig.instance().getThoiGianHuVang(Games.SAM.getName());
        msg.huTLMN = HuVangConfig.instance().getThoiGianHuVang(Games.TLMN.getName());
        this.send(msg, user);
    }

    private void gameLoop() {
        ++this.countUpdateJackpot;
        if (this.countUpdateJackpot >= ConfigGame.getIntValue("update_jackpot_time")) {
            this.updateJackpot();
            this.countUpdateJackpot = 0L;
        }
    }

    private void getEventInfo(final User user, final DataCmd dataCmd) {
        final GetEventVPInfoMsg msg = new GetEventVPInfoMsg();
        final IMap<String, String> map = HCMap.getCachedConfig();
        final String sLucky = map.get("VIPPOINT_EVENT_LUCKY");
        if (sLucky != null && sLucky.equals("1")) {
            final Date now = new Date();
            try {
                final String currentDate = VinPlayUtils.getDateTimeStr(now).substring(0, 11);
                final String etLuckyToday = currentDate + VippointUtils.END_LUCKY_TIME;
                final Date eventLuckyTimeEnd = VinPlayUtils.getDateTime(etLuckyToday);
                if (now.getTime() < eventLuckyTimeEnd.getTime()) {
                    msg.status = 1;
                    msg.time = (eventLuckyTimeEnd.getTime() - now.getTime()) / 1000L;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            msg.status = 0;
            msg.time = 0L;
        }
        this.send(msg, user);
    }

    private void initVP() throws JSONException, SQLException, ParseException {
        VippointUtils.init();
        final Calendar cal = Calendar.getInstance();
        final Date now = new Date();
        this.eventTimeStart = VinPlayUtils.getDateTime(VippointUtils.START);
        this.eventTimeEnd = VinPlayUtils.getDateTime(VippointUtils.END);
        final Date eventX2End = VippointUtils.END_X2_TIME;
        Date eventLuckyTimeStart = VinPlayUtils.getDateTime(VippointUtils.START.substring(0, 11) + VippointUtils.START_LUCKY_TIME);
        Date eventLuckyTimeEnd = VinPlayUtils.getDateTime(VippointUtils.START.substring(0, 11) + VippointUtils.END_LUCKY_TIME);
        this.timeLucky = eventLuckyTimeEnd.getTime() - eventLuckyTimeStart.getTime();
        Debug.trace("time lucky: " + this.timeLucky);
        final HazelcastInstance instance = HazelcastClientFactory.getInstance();
        final IMap<String, String> map = HCMap.getCachedConfig();
        if (now.getTime() < this.eventTimeStart.getTime()) {
            Debug.trace("event chua dien ra: " + new Date());
            map.put("VIPPOINT_EVENT_STATUS", "0");
            map.put("VIPPOINT_EVENT_X2_STATUS", "0");
            map.put("VIPPOINT_EVENT_LUCKY", "0");
            BitZeroServer.getInstance().getTaskScheduler().schedule(this.eventStartTask, this.calculateRemainTime(this.eventTimeStart), TimeUnit.SECONDS);
            BitZeroServer.getInstance().getTaskScheduler().schedule(this.eventEndTask, this.calculateRemainTime(this.eventTimeEnd), TimeUnit.SECONDS);
            BitZeroServer.getInstance().getTaskScheduler().schedule(this.eventX2EndTask, this.calculateRemainTime(eventX2End), TimeUnit.SECONDS);
            BitZeroServer.getInstance().getTaskScheduler().schedule(this.eventLuckyStartTask, this.calculateRemainTime(eventLuckyTimeStart), TimeUnit.SECONDS);
            BitZeroServer.getInstance().getTaskScheduler().schedule(this.eventLuckyEndTask, this.calculateRemainTime(eventLuckyTimeEnd), TimeUnit.SECONDS);
        } else if (now.getTime() < this.eventTimeEnd.getTime()) {
            Debug.trace("event dang dien ra");
            map.put("VIPPOINT_EVENT_STATUS", "1");
            if (now.getTime() < eventX2End.getTime()) {
                Debug.trace("event x2 dang dien ra");
                map.put("VIPPOINT_EVENT_X2_STATUS", "1");
                BitZeroServer.getInstance().getTaskScheduler().schedule(this.eventX2EndTask, this.calculateRemainTime(eventX2End), TimeUnit.SECONDS);
            } else {
                Debug.trace("event x2 da ket thuc");
                map.put("VIPPOINT_EVENT_X2_STATUS", "0");
            }
            BitZeroServer.getInstance().getTaskScheduler().schedule(this.eventEndTask, this.calculateRemainTime(this.eventTimeEnd), TimeUnit.SECONDS);
            final String currentDate = VinPlayUtils.getDateTimeStr(now).substring(0, 11);
            final String stLuckyToday = currentDate + VippointUtils.START_LUCKY_TIME;
            final String etLuckyToday = currentDate + VippointUtils.END_LUCKY_TIME;
            eventLuckyTimeStart = VinPlayUtils.getDateTime(stLuckyToday);
            eventLuckyTimeEnd = VinPlayUtils.getDateTime(etLuckyToday);
            if (now.getTime() < eventLuckyTimeStart.getTime()) {
                Debug.trace("event lucky chua dien ra");
                map.put("VIPPOINT_EVENT_LUCKY", "0");
                BitZeroServer.getInstance().getTaskScheduler().schedule(this.eventLuckyStartTask, this.calculateRemainTime(eventLuckyTimeStart), TimeUnit.SECONDS);
                BitZeroServer.getInstance().getTaskScheduler().schedule(this.eventLuckyEndTask, this.calculateRemainTime(eventLuckyTimeEnd), TimeUnit.SECONDS);
            } else if (now.getTime() < eventLuckyTimeEnd.getTime()) {
                Debug.trace("event lucky dang dien ra");
                final List<Date> unluckyTime = VippointUtils.randomUnluckyTime(this.eventTimeStart, this.eventTimeEnd);
                Debug.trace("unluckyTime: ");
                for (final Object dt : unluckyTime) {
                    if (now.getTime() < ((Date) dt).getTime()) {
                        BitZeroServer.getInstance().getTaskScheduler().schedule(this.eventUnluckyTask, this.calculateRemainTime((Date) dt) / 60, TimeUnit.MINUTES);
                        Debug.trace("OK: " + VinPlayUtils.getDateTimeStr((Date) dt));
                    } else {
                        Debug.trace("NOK: " + VinPlayUtils.getDateTimeStr((Date) dt));
                    }
                }
                final List<Date> luckyTime = VippointUtils.randomLuckyTime(this.eventTimeStart, this.eventTimeEnd);
                Debug.trace("luckyTime: ");
                for (final Date dt2 : luckyTime) {
                    if (now.getTime() < dt2.getTime()) {
                        BitZeroServer.getInstance().getTaskScheduler().schedule(this.eventLuckyTask, this.calculateRemainTime(dt2) / 60, TimeUnit.MINUTES);
                        Debug.trace("OK: " + VinPlayUtils.getDateTimeStr(dt2));
                    } else {
                        Debug.trace("NOK: " + VinPlayUtils.getDateTimeStr(dt2));
                    }
                }
                final String sLucky = map.get("VIPPOINT_EVENT_LUCKY");
                if (sLucky != null && sLucky.equals("0")) {
                    this.sendMsgToAllUser((byte) 1, eventLuckyTimeEnd.getTime() - now.getTime());
                }
                map.put("VIPPOINT_EVENT_LUCKY", "1");
                cal.setTime(eventLuckyTimeStart);
                cal.add(Calendar.DATE, 1);
                eventLuckyTimeStart = cal.getTime();
                BitZeroServer.getInstance().getTaskScheduler().schedule(this.eventLuckyStartTask, this.calculateRemainTime(eventLuckyTimeStart), TimeUnit.SECONDS);
                BitZeroServer.getInstance().getTaskScheduler().schedule(this.eventLuckyEndTask, this.calculateRemainTime(eventLuckyTimeEnd), TimeUnit.SECONDS);
            } else {
                Debug.trace("event lucky da het");
                final String sLucky2 = map.get("VIPPOINT_EVENT_LUCKY");
                if (sLucky2 != null && sLucky2.equals("1")) {
                    this.sendMsgToAllUser((byte) 0, 0L);
                }
                map.put("VIPPOINT_EVENT_LUCKY", "0");
                cal.setTime(eventLuckyTimeStart);
                cal.add(Calendar.DATE, 1);
                eventLuckyTimeStart = cal.getTime();
                BitZeroServer.getInstance().getTaskScheduler().schedule(this.eventLuckyStartTask, this.calculateRemainTime(eventLuckyTimeStart), TimeUnit.SECONDS);
                cal.setTime(eventLuckyTimeEnd);
                cal.add(Calendar.DATE, 1);
                eventLuckyTimeEnd = cal.getTime();
                BitZeroServer.getInstance().getTaskScheduler().schedule(this.eventLuckyEndTask, this.calculateRemainTime(eventLuckyTimeEnd), TimeUnit.SECONDS);
            }
        } else {
            Debug.trace("event da ket thuc");
            map.put("VIPPOINT_EVENT_STATUS", "0");
            map.put("VIPPOINT_EVENT_X2_STATUS", "0");
            final String sLucky3 = map.get("VIPPOINT_EVENT_LUCKY");
            if (sLucky3 != null && sLucky3.equals("1")) {
                this.sendMsgToAllUser((byte) 0, 0L);
            }
            map.put("VIPPOINT_EVENT_LUCKY", "0");
        }
    }

    private int calculateRemainTime(final Date runTime) {
        int time = 0;
        final Date now = new Date();
        if (runTime.getTime() > now.getTime()) {
            time = (int) ((runTime.getTime() - now.getTime()) / 1000L);
        }
        return time;
    }

    private void sendMsgToAllUser(final byte status, final long time) {
        final GetEventVPInfoMsg msg = new GetEventVPInfoMsg();
        msg.status = status;
        msg.time = time / 1000L;
        ServerUtil.sendMsgToAllUsers(msg);
    }

    public int getEsmsOTP(final String nickname, final String mobile, final String type) throws Exception {
        int code = 1;
        final HazelcastInstance client = HazelcastClientFactory.getInstance();
        final IMap<String, UserModel> userMap = client.getMap("users");
        UserModel model = null;
        if (userMap.containsKey(nickname)) {
            model = userMap.get(nickname);
            final UserCacheModel userCacheModel = (UserCacheModel) model;
        } else {
            final UserDaoImpl dao = new UserDaoImpl();
            model = dao.getUserByNickName(nickname);
        }
        if (model != null) {
            if (model.getMobile() != null && !model.getMobile().isEmpty() && model.isHasMobileSecurity()) {
                final OtpDaoImpl otpDao = new OtpDaoImpl();
                final String mobile2 = this.revertMobile(model.getMobile());
                String otp = null;
                try {
                    otp = VinPlayUtils.genOtpSMS(model.getMobile(), "");
                    Debug.trace("Lobby OTP: " + model.getMobile());
                } catch (Exception e) {
                    Debug.trace("Mobile: " + model.getMobile() + "---");
                }
                otpDao.updateOtpSMS(model.getMobile(), otp, "OZZ OTP");
                final AlertServiceImpl service = new AlertServiceImpl();
                final String content = String.format(GameCommon.MESSAGE_OTP_SUCCESS, otp, VinPlayUtils.getCurrentDate());
                service.SendSMSRutCuoc(mobile2, content);
                code = 0;
            } else {
                code = 4;
            }
        } else {
            Debug.trace("Model is ");
            code = 2;
        }
        return code;
    }

    public String revertMobile(final String mobile) {
        if (mobile.startsWith("84")) {
            return "0" + mobile.substring(2);
        }
        return mobile;
    }

    private static final class EventStartTask implements Runnable {
        @Override
        public void run() {
            Debug.trace("vippoint event start: " + new Date());
            final HazelcastInstance instance = HazelcastClientFactory.getInstance();
            final IMap<String, String> map = HCMap.getCachedConfig();
            map.put("VIPPOINT_EVENT_STATUS", "1");
            map.put("VIPPOINT_EVENT_X2_STATUS", "1");
        }
    }

    private final class EventEndTask implements Runnable {
        @Override
        public void run() {
            Debug.trace("vippoint event end: " + new Date());
            final HazelcastInstance instance = HazelcastClientFactory.getInstance();
            final IMap<String, String> map = HCMap.getCachedConfig();
            map.put("VIPPOINT_EVENT_STATUS", "0");
            map.put("VIPPOINT_EVENT_X2_STATUS", "0");
            final String sLucky = map.get("VIPPOINT_EVENT_LUCKY");
            if (sLucky != null && sLucky.equals("1")) {
                LobbyModule.this.sendMsgToAllUser((byte) 0, 0L);
            }
            map.put("VIPPOINT_EVENT_LUCKY", "0");
        }
    }

    private static final class EventX2EndTask implements Runnable {
        @Override
        public void run() {
            Debug.trace("event x2 end: " + new Date());
            final HazelcastInstance instance = HazelcastClientFactory.getInstance();
            final IMap<String, String> map = HCMap.getCachedConfig();
            map.put("VIPPOINT_EVENT_X2_STATUS", "0");
        }
    }

    private final class EventLuckyStartTask implements Runnable {
        @Override
        public void run() {
            final Date now = new Date();
            if (now.getTime() < LobbyModule.this.eventTimeEnd.getTime()) {
                Debug.trace("event lucky start: " + now);
                try {
                    VippointUtils.init();
                    final HazelcastInstance instance = HazelcastClientFactory.getInstance();
                    final IMap<String, String> map = HCMap.getCachedConfig();
                    final String sLucky = map.get("VIPPOINT_EVENT_LUCKY");
                    if (sLucky != null && sLucky.equals("0")) {
                        Debug.trace("send message to all user: " + now);
                        LobbyModule.this.sendMsgToAllUser((byte) 1, LobbyModule.this.timeLucky);
                    }
                    map.put("VIPPOINT_EVENT_LUCKY", "1");
                    final List<Date> unluckyTime = VippointUtils.randomUnluckyTime(LobbyModule.this.eventTimeStart, LobbyModule.this.eventTimeEnd);
                    Debug.trace("unluckyTime: ");
                    for (final Object dt : unluckyTime) {
                        if (now.getTime() < ((Date) dt).getTime()) {
                            BitZeroServer.getInstance().getTaskScheduler().schedule(LobbyModule.this.eventUnluckyTask, LobbyModule.this.calculateRemainTime((Date) dt) / 60, TimeUnit.MINUTES);
                            Debug.trace("OK: " + VinPlayUtils.getDateTimeStr((Date) dt));
                        } else {
                            Debug.trace("NOK: " + VinPlayUtils.getDateTimeStr((Date) dt));
                        }
                    }
                    final List<Date> luckyTime = VippointUtils.randomLuckyTime(LobbyModule.this.eventTimeStart, LobbyModule.this.eventTimeEnd);
                    Debug.trace("luckyTime: ");
                    for (final Date dt2 : luckyTime) {
                        if (now.getTime() < dt2.getTime()) {
                            BitZeroServer.getInstance().getTaskScheduler().schedule(LobbyModule.this.eventLuckyTask, LobbyModule.this.calculateRemainTime(dt2) / 60, TimeUnit.MINUTES);
                            Debug.trace("OK: " + VinPlayUtils.getDateTimeStr(dt2));
                        } else {
                            Debug.trace("NOK: " + VinPlayUtils.getDateTimeStr(dt2));
                        }
                    }
                    BitZeroServer.getInstance().getTaskScheduler().schedule(LobbyModule.this.eventLuckyStartTask, 24, TimeUnit.HOURS);
                } catch (Exception e) {
                    Debug.trace("event lucky start error: " + e);
                }
            }
        }
    }

    private final class EventLuckyEndTask implements Runnable {
        @Override
        public void run() {
            final Date now = new Date();
            if (now.getTime() < LobbyModule.this.eventTimeEnd.getTime()) {
                Debug.trace("event lucky end: " + now);
                final HazelcastInstance instance = HazelcastClientFactory.getInstance();
                final IMap<String, String> map = HCMap.getCachedConfig();
                final String sLucky = map.get("VIPPOINT_EVENT_LUCKY");
                if (sLucky != null && sLucky.equals("1")) {
                    Debug.trace("send message to all user: " + now);
                    LobbyModule.this.sendMsgToAllUser((byte) 0, 0L);
                }
                map.put("VIPPOINT_EVENT_LUCKY", "0");
                BitZeroServer.getInstance().getTaskScheduler().schedule(LobbyModule.this.eventLuckyEndTask, 24, TimeUnit.HOURS);
            }
        }
    }

    private final class EventUnluckyTask implements Runnable {
        @Override
        public void run() {
            Debug.trace("sub vippoint: " + new Date());
            final EventVPUnluckyMsg msg = new EventVPUnluckyMsg();
            ServerUtil.sendMsgToAllUsers(msg);
            final List<String> users = LobbyModule.this.vpService.subVippointEvent();
            for (final String nickname : users) {
                final HasNewMailMsg mailMsg = new HasNewMailMsg();
                MiniGameUtils.sendMessageToUser(mailMsg, nickname);
            }
        }
    }

    private final class EventluckyTask implements Runnable {
        @Override
        public void run() {
            Debug.trace("add vippoint: " + new Date());
            final List<String> users = LobbyModule.this.vpService.addVippointEvent();
            for (final String nickname : users) {
                final HasNewMailMsg mailMsg = new HasNewMailMsg();
                MiniGameUtils.sendMessageToUser(mailMsg, nickname);
            }
        }
    }

    private final class SlotDailyTask implements Runnable {
        @Override
        public void run() {
            try {
                LuckyUtils.initSlotMap();
                DvtUtils.initDVT(false);
            } catch (Exception e) {
                e.printStackTrace();
                Debug.trace("init slot free errot: " + e);
            }
            BitZeroServer.getInstance().getTaskScheduler().schedule(LobbyModule.this.slotDailyTask, 24, TimeUnit.HOURS);
        }
    }

    private final class UpdateCardTransactionTask implements Runnable {
        @Override
        public void run() {
            try {
                final List<Document> pendingTrans = LobbyModule.this.rechargeDAO.getRechargeByGachtheRecently();
                Debug.trace("update trans error: " + pendingTrans.size());
                if (pendingTrans.size() > 0) {
                    final NapTheDienThoaiMsg msg = new NapTheDienThoaiMsg();
                    pendingTrans.forEach(document -> {
                        if (document.getInteger("code") == 0) {
                            msg.Error = Byte.parseByte(document.getInteger("code").toString());
                            msg.currentMoney = document.getLong("current_money") + document.getLong("amount");
                            msg.numFail = 0;
                            msg.timeFail = 0L;
                        } else {
                            msg.Error = 35;
                        }
                        Debug.trace(("Send Noti Recharge Gachthe error: " + msg.Error));
                        User user = ExtensionUtility.getExtension().getApi().getUserByName(document.getString("nick_name"));
                        if (user != null) {
                            LobbyModule.this.rechargeDAO.UpdateGachtheTransctionsSent(document.getString("request_id"));
                            LobbyModule.this.send(msg, user);
                        } else {
                            User user2 = ExtensionUtility.getExtension().getApi().getUserByName(document.getString("username"));
                            if (user2 != null) {
                                LobbyModule.this.rechargeDAO.UpdateGachtheTransctionsSent(document.getString("request_id"));
                                LobbyModule.this.send(msg, user2);
                            } else {
                                User user3 = ExtensionUtility.globalUserManager.getUserByName(document.getString("username"));
                                LobbyModule.this.rechargeDAO.UpdateGachtheTransctionsSent(document.getString("request_id"));
                                LobbyModule.this.send(msg, user3);
                            }
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
