// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.lobby.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class NapQuaNganHangMsg extends BaseMsgEx
{
    public String url;
    
    public NapQuaNganHangMsg() {
        super(20013);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        this.putStr(bf, this.url);
        return this.packBuffer(bf);
    }
}
