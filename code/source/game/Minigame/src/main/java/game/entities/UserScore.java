// 
// Decompiled by Procyon v0.5.36
// 

package game.entities;

public class UserScore
{
    public int moneyType;
    public long money;
    public long wastedMoney;
    public int winCount;
    public int lostCount;
    
    public UserScore() {
        this.moneyType = -1;
    }
}
