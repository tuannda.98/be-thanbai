// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.lobby.cmd.rev;

import java.nio.ByteBuffer;
import bitzero.server.extensions.data.DataCmd;
import bitzero.server.extensions.data.BaseCmd;

public class UpdateUserInfoCmd extends BaseCmd
{
    public String cmt;
    public String email;
    public String mobile;
    
    public UpdateUserInfoCmd(final DataCmd dataCmd) {
        super(dataCmd);
        this.unpackData();
    }
    
    public void unpackData() {
        final ByteBuffer bf = this.makeBuffer();
        this.cmt = this.readString(bf);
        this.email = this.readString(bf);
        this.mobile = this.readString(bf);
    }
}
