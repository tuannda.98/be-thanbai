// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.lobby.cmd.send;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class KetSatMsg extends BaseMsgEx
{
    public long moneyUse;
    public long safe;
    
    public KetSatMsg() {
        super(20009);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        bf.putLong(this.moneyUse);
        bf.putLong(this.safe);
        return this.packBuffer(bf);
    }
}
