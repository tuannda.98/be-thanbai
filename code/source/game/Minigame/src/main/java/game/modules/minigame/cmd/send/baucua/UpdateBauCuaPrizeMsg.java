// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.send.baucua;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class UpdateBauCuaPrizeMsg extends BaseMsgEx
{
    public long prize;
    public long currentMoney;
    public byte room;
    
    public UpdateBauCuaPrizeMsg() {
        super(5009);
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        bf.putLong(this.prize);
        bf.putLong(this.currentMoney);
        bf.put(this.room);
        return this.packBuffer(bf);
    }
}
