// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.send.pokego;

import java.nio.ByteBuffer;
import game.BaseMsgEx;

public class ResultPokeGoMsg extends BaseMsgEx
{
    public byte result;
    public String matrix;
    public String linesWin;
    public long prize;
    public long currentMoney;
    
    public ResultPokeGoMsg() {
        super(7001);
        this.matrix = "";
        this.linesWin = "";
    }
    
    public byte[] createData() {
        final ByteBuffer bf = this.makeBuffer();
        bf.put(this.result);
        this.putStr(bf, this.matrix);
        this.putStr(bf, this.linesWin);
        bf.putLong(this.prize);
        bf.putLong(this.currentMoney);
        return this.packBuffer(bf);
    }
}
