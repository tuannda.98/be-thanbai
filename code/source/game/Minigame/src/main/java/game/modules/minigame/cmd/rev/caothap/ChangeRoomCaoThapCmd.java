// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.minigame.cmd.rev.caothap;

import java.nio.ByteBuffer;
import bitzero.server.extensions.data.DataCmd;
import bitzero.server.extensions.data.BaseCmd;

public class ChangeRoomCaoThapCmd extends BaseCmd
{
    public byte roomLeavedId;
    public byte roomJoinedId;
    
    public ChangeRoomCaoThapCmd(final DataCmd dataCmd) {
        super(dataCmd);
        this.unpackData();
    }
    
    public void unpackData() {
        final ByteBuffer bf = this.makeBuffer();
        this.roomLeavedId = this.readByte(bf);
        this.roomJoinedId = this.readByte(bf);
    }
}
