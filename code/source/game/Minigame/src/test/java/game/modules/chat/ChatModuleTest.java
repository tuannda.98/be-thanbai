//package game.modules.chat;
//
//import bitzero.server.entities.User;
//import bitzero.server.extensions.data.DataCmd;
//import com.vinplay.dal.service.impl.ChatLobbyServiceImpl;
//import org.apache.commons.lang3.RandomStringUtils;
//import org.apache.commons.lang3.mutable.MutableInt;
//import org.junit.jupiter.api.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Mockito;
//import org.powermock.core.classloader.annotations.PrepareForTest;
//import org.powermock.modules.junit4.PowerMockRunner;
//
//import java.util.concurrent.CountDownLatch;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertNotNull;
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.ArgumentMatchers.anyString;
//import static org.powermock.api.mockito.PowerMockito.*;
//
//@RunWith(PowerMockRunner.class)
//@PrepareForTest({ChatModule.class, ChatLobbyServiceImpl.class})
//class ChatModuleTest {
//
//
//    @Test
//    public void subscribe_basic_test_concurrent_map() throws Exception {
//        ChatModule chatModule = spy(new ChatModule());
//        ChatLobbyServiceImpl chatService = mock(ChatLobbyServiceImpl.class);
//        chatModule.setChatService(chatService);
//        doReturn(0L).when(chatService).getBanTime(anyString());
//        doNothing().when(chatModule).sendMessageToUser(any(), any());
//        assertNotNull(chatModule);
////        PowerMockito.doAnswer(invocation -> {
////            return null;
////        }).when(chatModule, "sendChatLobbyInfo", ArgumentMatchers.any());
////        PowerMockito.doNothing().when(chatModule, "send", ArgumentMatchers.any(), ArgumentMatchers.any());
//        User user = mock(User.class);
//        System.out.println(user);
//        Mockito.lenient().when(user.getName()).thenReturn("NickName");
//        DataCmd dataCmd = mock(DataCmd.class);
//        // subscribe
//        Mockito.lenient().when(dataCmd.getId()).thenReturn((short) 18001);
//        chatModule.handleClientRequest(user, dataCmd);
//
//        // unsubscribe
//        Mockito.lenient().when(dataCmd.getId()).thenReturn((short) 18002);
//        chatModule.handleClientRequest(user, dataCmd);
//    }
//
//    @Test
//    public void subscribe_basic_test_multi_thread_concurrent_map() throws Exception {
//        int numberOfThreads = 200;
//        ExecutorService service = Executors.newFixedThreadPool(10);
//        CountDownLatch latch = new CountDownLatch(numberOfThreads);
//
//        ChatModule chatModule = spy(new ChatModule());
//        ChatLobbyServiceImpl chatService = mock(ChatLobbyServiceImpl.class);
//        chatModule.setChatService(chatService);
//        doReturn(0L).when(chatService).getBanTime(anyString());
//        doNothing().when(chatModule).sendMessageToUser(any(), any());
//
//        MutableInt exceptionCount = new MutableInt(0);
//        for (int i = 0; i < numberOfThreads; i++) {
//            service.submit(() -> {
//                try {
//                    User user = mock(User.class);
//                    Mockito.lenient().when(user.getName()).thenReturn(RandomStringUtils.randomAlphanumeric(10));
//                    DataCmd dataCmd = mock(DataCmd.class);
//
//                    // subscribe
//                    Mockito.lenient().when(dataCmd.getId()).thenReturn((short) 18001);
//                    chatModule.handleClientRequest(user, dataCmd);
//
//                    // unsubscribe
//                    Mockito.lenient().when(dataCmd.getId()).thenReturn((short) 18002);
//                    chatModule.handleClientRequest(user, dataCmd);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    exceptionCount.increment();
//                }
//                latch.countDown();
//            });
//        }
//        latch.await();
//        assertEquals(0, exceptionCount.intValue());
//    }
//
//
//}