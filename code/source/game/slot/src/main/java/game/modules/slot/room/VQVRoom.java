/*
 * Decompiled with CFR 0.144.
 *
 * Could not load the following classes:
 *  bitzero.server.BitZeroServer
 *  bitzero.server.entities.User
 *  bitzero.server.extensions.data.BaseMsg
 *  bitzero.server.util.TaskScheduler
 *  bitzero.util.common.business.Debug
 *  com.vinplay.dal.service.BroadcastMessageService
 *  com.vinplay.dal.service.MiniGameService
 *  com.vinplay.dal.service.SlotMachineService
 *  com.vinplay.dal.service.impl.BroadcastMessageServiceImpl
 *  com.vinplay.dal.service.impl.CacheServiceImpl
 *  com.vinplay.usercore.service.UserService
 *  com.vinplay.vbee.common.enums.Games
 *  com.vinplay.vbee.common.models.UserModel
 *  com.vinplay.vbee.common.models.cache.SlotFreeDaily
 *  com.vinplay.vbee.common.models.cache.UserCacheModel
 *  com.vinplay.vbee.common.models.slot.SlotFreeSpin
 *  com.vinplay.vbee.common.response.MoneyResponse
 *  com.vinplay.vbee.common.statics.TransType
 *  com.vinplay.vbee.common.utils.CommonUtils
 *  com.vinplay.vbee.common.utils.DateTimeUtils
 */
package game.modules.slot.room;

import bitzero.server.BitZeroServer;
import bitzero.server.entities.User;
import bitzero.util.common.business.Debug;
import casio.king365.util.KingUtil;
import com.vinplay.dal.service.impl.BroadcastMessageServiceImpl;
import com.vinplay.dal.service.impl.CacheServiceImpl;
import com.vinplay.vbee.common.enums.Games;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.models.cache.SlotFreeDaily;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.models.slot.SlotFreeSpin;
import com.vinplay.vbee.common.response.MoneyResponse;
import com.vinplay.vbee.common.statics.TransType;
import com.vinplay.vbee.common.utils.CommonUtils;
import com.vinplay.vbee.common.utils.DateTimeUtils;
import game.modules.slot.VQVModule;
import game.modules.slot.cmd.send.vqv.*;
import game.modules.slot.entities.slot.*;
import game.modules.slot.entities.slot.vqv.AwardsOnLine;
import game.modules.slot.entities.slot.vqv.Line;
import game.modules.slot.entities.slot.vqv.*;
import game.modules.slot.utils.SlotUtils;
import game.modules.slot.utils.VQVUtils;
import game.util.ConfigGame;
import game.util.GameUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class VQVRoom
        extends SlotRoom {
    private final Runnable gameLoopTask = new SlotRoom.GameLoopTask();
    private VQVLines lines = new VQVLines();
    private long lastTimeUpdatePotToRoom = 0L;
    private long lastTimeUpdateFundToRoom = 0L;
    private ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);
    private List<Integer> boxValues = new ArrayList<Integer>();

    public VQVRoom(VQVModule module, byte id, String name, short moneyType, long pot, long fund, int betValue, long initPotValue) {
        super(id, name, betValue, moneyType, pot, fund, initPotValue);
        this.gameName = Games.VUONG_QUOC_VIN.getName();
        this.cacheFreeName = String.valueOf(this.gameName) + betValue;
        this.module = module;
        this.moneyTypeStr = this.moneyType == 1 ? "vin" : "xu";
        CacheServiceImpl cacheService = new CacheServiceImpl();
        cacheService.setValue(name, (int) pot);
        this.betValue = betValue;
        this.initPotValue = initPotValue;
        BitZeroServer.getInstance().getTaskScheduler().scheduleAtFixedRate(this.gameLoopTask, 10, 1, TimeUnit.SECONDS);
        this.boxValues.add(10);
        this.boxValues.add(10);
        this.boxValues.add(10);
        this.boxValues.add(15);
        this.boxValues.add(20);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void forceStopAutoPlay(User user) {
        super.forceStopAutoPlay(user);
        Map map = this.usersAuto;
        synchronized (map) {
            this.usersAuto.remove(user.getName());
            ForceStopAutoPlayVQVMsg msg = new ForceStopAutoPlayVQVMsg();
            SlotUtils.sendMessageToUser(msg, user);
        }
    }

    private void broadcastBigWin(String username, byte result, long totalPrizes) {
        BigWinVQVMsg bigWinMsg = new BigWinVQVMsg();
        bigWinMsg.username = username;
        bigWinMsg.type = result;
        bigWinMsg.betValue = (short) this.betValue;
        bigWinMsg.totalPrizes = totalPrizes;
        bigWinMsg.timestamp = DateTimeUtils.getCurrentTime();
        this.module.sendMsgToAllUsers(bigWinMsg);
    }

    public synchronized ResultSlotMsg play(String username, String linesStr) {
        long referenceId = this.module.getNewReferenceId();
        SlotFreeSpin freeSpin = this.slotService.getLuotQuayFreeSlot(this.cacheFreeName, username);
        int luotQuayFree = freeSpin.getNum();
        int ratioFree = freeSpin.getRatio();
        if (luotQuayFree > 0) {
            linesStr = freeSpin.getLines();
            return this.playFree(username, linesStr, ratioFree, referenceId);
        }
        return this.playNormal(username, linesStr, referenceId);
    }

    public synchronized ResultSlotMsg playNormal(String username, String linesStr, long referenceId) {
        KingUtil.printLog("VQV play username: " + username + ", betValue: " + this.betValue);
        long startTime = System.currentTimeMillis();
        String currentTimeStr = DateTimeUtils.getCurrentTime();
        short result = 0;
        String[] lineArr = linesStr.split(",");
        long currentMoney = this.userService.getMoneyUserCache(username, this.moneyTypeStr);
        UserCacheModel u = this.userService.forceGetCachedUser(username);
        long totalBetValue = lineArr.length * this.betValue;
        ResultSlotMsg msg = new ResultSlotMsg();
        if (lineArr.length > 0 && !linesStr.isEmpty()) {
            if (totalBetValue > 0L) {
                long minVin = this.userService.getAvailableVinMoney(username);
                if (totalBetValue <= minVin) {
                    long fee = totalBetValue * 2L / 100L;
                    MoneyResponse moneyRes = this.userService.updateMoney(username, -totalBetValue, this.moneyTypeStr, this.gameName, "Quay Tho Dan", "\u0110\u1eb7t c\u01b0\u1ee3c Tho Dan", fee, Long.valueOf(referenceId), TransType.START_TRANS);
                    if (moneyRes != null && moneyRes.isSuccess()) {
                        long moneyToPot = totalBetValue * 1L / 100L;
                        long moneyToFund = totalBetValue - fee - moneyToPot;
                        this.fund += moneyToFund;
                        this.pot += moneyToPot;
                        boolean enoughPair = false;
                        ArrayList<AwardsOnLine> awardsOnLines = new ArrayList<AwardsOnLine>();
                        long totalPrizes = 0L;
                        long soTienNoHuKhongTruQuy = 0L;
                        long tienThuongX2 = 0L;
                        int countFreeSpin = 0;
                        int ratio = 0;
                        int ratioBonus = 0;
                        int loopCount = 0;
                        block4:
                        while (!enoughPair) {
                            loopCount++;
                            Random rd;
                            int soLanNoHu;
                            int n;
                            result = 0;
                            awardsOnLines.clear();
                            totalPrizes = 0L;
                            soTienNoHuKhongTruQuy = 0L;
                            tienThuongX2 = 0L;
                            String linesWin = "";
                            String prizesOnLine = "";
                            String haiSao = "";
                            countFreeSpin = 0;
                            ratio = 0;
                            ratioBonus = 0;
                            boolean forceNoHu = false;
//                            if (lineArr.length >= 5 && (soLanNoHu = ConfigGame.getIntValue(String.valueOf(this.gameName) + "_so_lan_no_hu")) > 0 && this.fund > this.initPotValue * 2L && (n = (rd = new Random()).nextInt(soLanNoHu)) == 0) {
//                                forceNoHu = true;
//                            }
                            if (betValue == 100) {
                                if (lineArr.length >= 5 && (soLanNoHu = ConfigGame.getIntValue(this.gameName + "_so_lan_no_hu_100")) > 0 && this.fund > this.initPotValue * 2L && (n = (rd = new Random()).nextInt(soLanNoHu)) == 0) {
                                    forceNoHu = true;
                                }
                            } else if (betValue == 1000) {
                                if (lineArr.length >= 5 && (soLanNoHu = ConfigGame.getIntValue(this.gameName + "_so_lan_no_hu_1000")) > 0 && this.fund > this.initPotValue * 2L && (n = (rd = new Random()).nextInt(soLanNoHu)) == 0) {
                                    forceNoHu = true;
                                }
                            } else {
                                if (lineArr.length >= 5 && (soLanNoHu = ConfigGame.getIntValue(this.gameName + "_so_lan_no_hu_10000")) > 0 && this.fund > this.initPotValue * 2L && (n = (rd = new Random()).nextInt(soLanNoHu)) == 0) {
                                    forceNoHu = true;
                                }
                            }
                            VQVItem[][] matrix = forceNoHu ? VQVUtils.generateMatrixNoHu(lineArr) : VQVUtils.generateMatrix();
                            for (String entry2 : lineArr) {
                                ArrayList<VQVAward> awardList = new ArrayList<VQVAward>();
                                Line line = VQVUtils.getLine(this.lines, matrix, Integer.parseInt(entry2));
                                VQVUtils.calculateLine(line, awardList);
                                for (VQVAward award : awardList) {
                                    long moneyOnLine = 0L;
                                    if (award.getRatio() > 0.0f) {
                                        moneyOnLine = (long) (award.getRatio() * (float) this.betValue);
                                    } else if (award == VQVAward.PENTA_JACKPOT) {
                                        if (result != 3) {
                                            if (this.huX2) {
                                                moneyOnLine = this.pot * 2L;
                                                tienThuongX2 = this.pot;
                                                soTienNoHuKhongTruQuy += this.pot;
                                            } else {
                                                moneyOnLine = this.pot;
                                            }
                                            result = 3;
                                            soTienNoHuKhongTruQuy += this.pot - this.initPotValue;
                                        } else {
                                            moneyOnLine = this.initPotValue;
                                        }
                                    } else if (award.getRatio() == -2.0f) {
                                        if (ratioBonus > 0) continue block4;
                                        ratioBonus = 1;
                                        if (award == VQVAward.QUADRA_BONUS) {
                                            ratioBonus = 5;
                                        }
                                        MiniGameSlotResponse response = this.generatePickStars(ratioBonus);
                                        moneyOnLine = response.getTotalPrize();
                                        haiSao = response.getPrizes();
                                        if (result != 3) {
                                            result = 5;
                                        }
                                    } else if (award.getRatio() == -3.0f) {
                                        if (countFreeSpin > 0) continue block4;
                                        ++countFreeSpin;
                                        ratio = 1;
                                        if (award.getDuplicate() == VQVAward.PENTA_SCATTER.getDuplicate()) {
                                            ratio = 15;
                                        } else if (award.getDuplicate() == VQVAward.QUADRA_SCATTER.getDuplicate()) {
                                            ratio = 5;
                                        }
                                    }
                                    AwardsOnLine aol = new AwardsOnLine(award, moneyOnLine, line.getName());
                                    awardsOnLines.add(aol);
                                }
                            }
                            StringBuilder builderLinesWin = new StringBuilder();
                            StringBuilder builderPrizesOnLine = new StringBuilder();
                            for (AwardsOnLine entry2 : awardsOnLines) {
                                //if (!u.isBot() && (entry2.getAward() == VQVAward.PENTA_JACKPOT || entry2.getAward() == VQVAward.QUADRA_JACKPOT || entry2.getAward() == VQVAward.TRIPLE_JACKPOT)) continue block4;
                                if (betValue == 100) {
                                    if (ConfigGame.getIntValue(this.gameName + "_cho_bot_no_hu_100") == 0) // cho cả người và bot nổ hũ
                                    {
                                        if ((entry2.getAward() == VQVAward.PENTA_JACKPOT || entry2.getAward() == VQVAward.QUADRA_JACKPOT || entry2.getAward() == VQVAward.TRIPLE_JACKPOT))
                                            continue block4;
                                    } else if (ConfigGame.getIntValue(this.gameName + "_cho_bot_no_hu_100") == 1) // chỉ cho bot nổ hũ
                                    {
                                        if ((entry2.getAward() == VQVAward.PENTA_JACKPOT || entry2.getAward() == VQVAward.QUADRA_JACKPOT || entry2.getAward() == VQVAward.TRIPLE_JACKPOT) && !u.isBot())
                                            continue block4;
                                    } else if (ConfigGame.getIntValue(this.gameName + "_cho_bot_no_hu_100") == -1) // chỉ cho người nổ hũ
                                    {
                                        if ((entry2.getAward() == VQVAward.PENTA_JACKPOT || entry2.getAward() == VQVAward.QUADRA_JACKPOT || entry2.getAward() == VQVAward.TRIPLE_JACKPOT) && u.isBot())
                                            continue block4;
                                    } else {
                                        if ((entry2.getAward() == VQVAward.PENTA_JACKPOT || entry2.getAward() == VQVAward.QUADRA_JACKPOT || entry2.getAward() == VQVAward.TRIPLE_JACKPOT) && !u.isBot())
                                            continue block4;
                                    }
                                } else if (betValue == 1000) {
                                    if (ConfigGame.getIntValue(this.gameName + "_cho_bot_no_hu_1000") == 0) // cho cả người và bot nổ hũ
                                    {
                                        if ((entry2.getAward() == VQVAward.PENTA_JACKPOT || entry2.getAward() == VQVAward.QUADRA_JACKPOT || entry2.getAward() == VQVAward.TRIPLE_JACKPOT))
                                            continue block4;
                                    } else if (ConfigGame.getIntValue(this.gameName + "_cho_bot_no_hu_1000") == 1) // chỉ cho bot nổ hũ
                                    {
                                        if ((entry2.getAward() == VQVAward.PENTA_JACKPOT || entry2.getAward() == VQVAward.QUADRA_JACKPOT || entry2.getAward() == VQVAward.TRIPLE_JACKPOT) && !u.isBot())
                                            continue block4;
                                    } else if (ConfigGame.getIntValue(this.gameName + "_cho_bot_no_hu_1000") == -1) // chỉ cho người nổ hũ
                                    {
                                        if ((entry2.getAward() == VQVAward.PENTA_JACKPOT || entry2.getAward() == VQVAward.QUADRA_JACKPOT || entry2.getAward() == VQVAward.TRIPLE_JACKPOT) && u.isBot())
                                            continue block4;
                                    } else {
                                        if ((entry2.getAward() == VQVAward.PENTA_JACKPOT || entry2.getAward() == VQVAward.QUADRA_JACKPOT || entry2.getAward() == VQVAward.TRIPLE_JACKPOT) && !u.isBot())
                                            continue block4;
                                    }
                                } else {
                                    if (ConfigGame.getIntValue(this.gameName + "_cho_bot_no_hu_10000") == 0) // cho cả người và bot nổ hũ
                                    {
                                        if ((entry2.getAward() == VQVAward.PENTA_JACKPOT || entry2.getAward() == VQVAward.QUADRA_JACKPOT || entry2.getAward() == VQVAward.TRIPLE_JACKPOT))
                                            continue block4;
                                    } else if (ConfigGame.getIntValue(this.gameName + "_cho_bot_no_hu_10000") == 1) // chỉ cho bot nổ hũ
                                    {
                                        if ((entry2.getAward() == VQVAward.PENTA_JACKPOT || entry2.getAward() == VQVAward.QUADRA_JACKPOT || entry2.getAward() == VQVAward.TRIPLE_JACKPOT) && !u.isBot())
                                            continue block4;
                                    } else if (ConfigGame.getIntValue(this.gameName + "_cho_bot_no_hu_10000") == -1) // chỉ cho người nổ hũ
                                    {
                                        if ((entry2.getAward() == VQVAward.PENTA_JACKPOT || entry2.getAward() == VQVAward.QUADRA_JACKPOT || entry2.getAward() == VQVAward.TRIPLE_JACKPOT) && u.isBot())
                                            continue block4;
                                    } else {
                                        if ((entry2.getAward() == VQVAward.PENTA_JACKPOT || entry2.getAward() == VQVAward.QUADRA_JACKPOT || entry2.getAward() == VQVAward.TRIPLE_JACKPOT) && !u.isBot())
                                            continue block4;
                                    }
                                }
                                totalPrizes += entry2.getMoney();
                                builderLinesWin.append(",");
                                builderLinesWin.append(entry2.getLineId());
                                builderPrizesOnLine.append(",");
                                builderPrizesOnLine.append(entry2.getMoney());
                            }
                            if (builderLinesWin.length() > 0) {
                                builderLinesWin.deleteCharAt(0);
                            }
                            if (builderPrizesOnLine.length() > 0) {
                                builderPrizesOnLine.deleteCharAt(0);
                            }
                            // if (result == 3 ? this.fund - (totalPrizes - soTienNoHuKhongTruQuy) < 0L : this.fund - totalPrizes < this.initPotValue * 2L && totalPrizes - totalBetValue >= 0L) continue;
                            if (result == 3) {
                                if (this.fund - (totalPrizes - soTienNoHuKhongTruQuy) < 0L)
                                    continue;
                            } else {
                                if (loopCount < 10) {
                                    // Nếu loop dc < 10 lần thì check điều kiện kiểu cũ
                                    if (this.fund - totalPrizes < this.initPotValue * 2L && totalPrizes - totalBetValue >= 0L)
                                        continue;
                                } else {
                                    if (totalPrizes - totalBetValue > 0L)
                                        continue;
                                }
                            }
                            enoughPair = true;
                            String matrixStr = VQVUtils.matrixToString(matrix);
                            if (totalPrizes > 0L) {
                                if (result == 3) {
                                    if (this.huX2) {
                                        result = 4;
                                    }
                                    this.noHuX2();
                                    this.pot = this.initPotValue;
                                    this.fund -= totalPrizes - soTienNoHuKhongTruQuy;
                                    if (this.moneyType == 1) {
                                        GameUtils.sendSMSToUser(username, "Chuc mung " + username + " da no hu game Tho Dan phong " + this.betValue + ". So tien no hu: " + totalPrizes + " " + "GS");
                                    }
                                    this.slotService.logNoHu(referenceId, this.gameName, username, this.betValue, linesStr, matrixStr, builderLinesWin.toString(), builderPrizesOnLine.toString(), totalPrizes, result, currentTimeStr);
                                } else {
                                    this.fund -= totalPrizes;
                                    if (result == 0) {
                                        result = totalPrizes >= (long) (this.betValue * 100) ? (short) 2 : 1;
                                    }
                                }
                            }
                            long moneyExchange = totalPrizes - tienThuongX2;
                            String des = "Quay Tho Dan";
                            if (tienThuongX2 > 0L) {
                                this.userService.updateMoney(username, tienThuongX2, this.moneyTypeStr, this.gameName, des, "Th\u01b0\u1edfng h\u0169 X2", 0L, null, TransType.NO_VIPPOINT);
                            }
                            if ((moneyRes = this.userService.updateMoney(username, totalPrizes, this.moneyTypeStr, this.gameName, des, this.buildDescription(totalBetValue, totalPrizes, result), 0L, Long.valueOf(referenceId), TransType.END_TRANS)) != null && moneyRes.isSuccess()) {
                                currentMoney = moneyRes.getCurrentMoney();
                                if (this.moneyType == 1 && moneyExchange >= (long) BroadcastMessageServiceImpl.MIN_MONEY) {
                                    this.broadcastMsgService.putMessage(Games.VUONG_QUOC_VIN.getId(), username, moneyExchange - totalBetValue);
                                }
                            }
                            linesWin = builderLinesWin.toString();
                            prizesOnLine = builderPrizesOnLine.toString();
                            msg.referenceId = referenceId;
                            msg.matrix = matrixStr;
                            msg.linesWin = linesWin;
                            msg.prize = totalPrizes;
                            msg.haiSao = haiSao;
                            if (countFreeSpin > 0) {
                                msg.isFreeSpin = 1;
                                msg.ratio = (byte) ratio;
                                this.slotService.setLuotQuayFreeSlot(this.cacheFreeName, username, linesStr, countFreeSpin, ratio);
                            } else {
                                msg.isFreeSpin = 0;
                            }
                            try {
                                this.slotService.logVQV(referenceId, username, this.betValue, linesStr, linesWin, prizesOnLine, result, totalPrizes, currentTimeStr);
                                if (result == 3 || result == 4) {
                                    this.slotService.addTop("Tho Dan", username, this.betValue, totalPrizes, currentTimeStr, result);
                                }
                                if (result == 3 || result == 2 || result == 4) {
                                    this.broadcastBigWin(username, (byte) result, totalPrizes);
                                }
                            } catch (InterruptedException award) {
                            } catch (TimeoutException award) {
                            } catch (IOException award) {
                                // empty catch block
                            }
                            this.saveFund();
                            this.savePot();
                        }
                    } else {
                        result = 102;
                    }
                } else {
                    result = 102;
                }
            } else {
                result = 101;
            }
        } else {
            result = 101;
        }
        msg.result = (byte) result;
        msg.currentMoney = currentMoney;
        long endTime = System.currentTimeMillis();
        long handleTime = endTime - startTime;
        String ratioTime = CommonUtils.getRatioTime(handleTime);
        SlotUtils.logVQV(referenceId, username, this.betValue, msg.matrix, msg.haiSao, result, handleTime, ratioTime, currentTimeStr);
        return msg;
    }

    public synchronized ResultSlotMsg playFree(String username, String linesStr, int ratio, long referenceId) {
        long startTime = System.currentTimeMillis();
        String currentTimeStr = DateTimeUtils.getCurrentTime();
        short result = 0;
        String[] lineArr = linesStr.split(",");
        long currentMoney = this.userService.getMoneyUserCache(username, this.moneyTypeStr);
        long totalBetValue = lineArr.length * this.betValue;
        ResultSlotMsg msg = new ResultSlotMsg();
        if (lineArr.length > 0 && !linesStr.isEmpty()) {
            if (totalBetValue > 0L) {
                boolean enoughPair = false;
                ArrayList<AwardsOnLine> awardsOnLines = new ArrayList<AwardsOnLine>();
                long totalPrizes = 0L;
                block4:
                while (!enoughPair) {
                    Random rd;
                    MoneyResponse moneyRes;
                    int soLanNoHu;
                    String des232;
                    long moneyExchange;
                    int n;
                    result = 0;
                    awardsOnLines.clear();
                    totalPrizes = 0L;
                    String linesWin = "";
                    String prizesOnLine = "";
                    String haiSao = "";
                    boolean forceNoHu = false;
                    if (lineArr.length >= 5 && (soLanNoHu = ConfigGame.getIntValue(this.gameName + "_so_lan_no_hu")) > 0 && this.fund > this.initPotValue * 2L && (n = (rd = new Random()).nextInt(soLanNoHu)) == 0) {
                        forceNoHu = true;
                    }
                    VQVItem[][] matrix = forceNoHu ? VQVUtils.generateMatrixNoHu(lineArr) : VQVUtils.generateMatrix();
                    for (String entry2 : lineArr) {
                        ArrayList<VQVAward> awardList = new ArrayList<VQVAward>();
                        Line line = VQVUtils.getLine(this.lines, matrix, Integer.parseInt(entry2));
                        VQVUtils.calculateLine(line, awardList);
                        for (VQVAward award : awardList) {
                            long moneyOnLine = 0L;
                            if (award.getRatio() <= 0.0f) continue block4;
                            moneyOnLine = (long) (award.getRatio() * (float) this.betValue);
                            AwardsOnLine aol = new AwardsOnLine(award, moneyOnLine, line.getName());
                            awardsOnLines.add(aol);
                        }
                    }
                    StringBuilder builderLinesWin = new StringBuilder();
                    StringBuilder builderPrizesOnLine = new StringBuilder();
                    for (AwardsOnLine entry2 : awardsOnLines) {
                        if (entry2.getAward() == VQVAward.PENTA_JACKPOT && !this.checkDieuKienNo(username))
                            continue block4;
                        totalPrizes += entry2.getMoney();
                        builderLinesWin.append(",");
                        builderLinesWin.append(entry2.getLineId());
                        builderPrizesOnLine.append(",");
                        builderPrizesOnLine.append(entry2.getMoney());
                    }
                    if (builderLinesWin.length() > 0) {
                        builderLinesWin.deleteCharAt(0);
                    }
                    if (builderPrizesOnLine.length() > 0) {
                        builderPrizesOnLine.deleteCharAt(0);
                    }
                    if (this.fund - (totalPrizes *= ratio) < this.initPotValue * 2L && totalPrizes - totalBetValue >= 0L)
                        continue;
                    enoughPair = true;
                    if (totalPrizes > 0L) {
                        if (result == 3) {
                            if (this.huX2) {
                                result = 4;
                            }
                            this.noHuX2();
                            this.pot = this.initPotValue;
                            this.fund -= totalPrizes;
                        } else {
                            this.fund -= totalPrizes;
                            if (result == 0) {
                                result = totalPrizes >= (long) (this.betValue * 100) ? (short) 2 : 1;
                            }
                        }
                    }
                    if ((moneyExchange = totalPrizes) > 0L && (moneyRes = this.userService.updateMoney(username, totalPrizes, this.moneyTypeStr, this.gameName, des232 = "Tho Dan - Free", this.buildDescription(totalBetValue, totalPrizes, result), 0L, null, TransType.VIPPOINT)) != null && moneyRes.isSuccess()) {
                        currentMoney = moneyRes.getCurrentMoney();
                        if (this.moneyType == 1 && moneyExchange >= (long) BroadcastMessageServiceImpl.MIN_MONEY) {
                            this.broadcastMsgService.putMessage(Games.VUONG_QUOC_VIN.getId(), username, moneyExchange - totalBetValue);
                        }
                    }
                    this.slotService.updateLuotQuaySlotFree(this.cacheFreeName, username);
                    linesWin = builderLinesWin.toString();
                    prizesOnLine = builderPrizesOnLine.toString();
                    msg.referenceId = referenceId;
                    msg.matrix = VQVUtils.matrixToString(matrix);
                    msg.linesWin = linesWin;
                    msg.prize = totalPrizes / (long) ratio;
                    msg.haiSao = "";
                    msg.isFreeSpin = 0;
                    msg.ratio = (byte) ratio;
                    try {
                        this.slotService.logVQV(referenceId, username, this.betValue, linesStr, linesWin, prizesOnLine, result, totalPrizes, currentTimeStr);
                        if (result == 3 || result == 4) {
                            this.slotService.addTop(this.gameName, username, this.betValue, totalPrizes, currentTimeStr, result);
                        }
                        if (result == 3 || result == 2 || result == 4) {
                            this.broadcastBigWin(username, (byte) result, totalPrizes);
                        }
                    } catch (InterruptedException des232_0) {
                    } catch (TimeoutException des232_1) {
                    } catch (IOException des232_2) {
                        // empty catch block
                    }
                    this.saveFund();
                    this.savePot();
                }
            } else {
                result = 101;
            }
        } else {
            result = 101;
        }
        msg.result = (byte) result;
        msg.currentMoney = currentMoney;
        long endTime = System.currentTimeMillis();
        long handleTime = endTime - startTime;
        String ratioTime = CommonUtils.getRatioTime(handleTime);
        SlotUtils.logVQV(referenceId, username, this.betValue, msg.matrix, msg.haiSao, result, handleTime, ratioTime, currentTimeStr);
        return msg;
    }

    public synchronized ResultSlotMsg playFreeDaily(String username) {
        long startTime = System.currentTimeMillis();
        String currentTimeStr = DateTimeUtils.getCurrentTime();
        long refernceId = this.module.getNewReferenceId();
        short result = 0;
        String linesStr = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20";
        String[] lineArr = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20".split(",");
        long currentMoney = this.userService.getMoneyUserCache(username, this.moneyTypeStr);
        ResultSlotMsg msg = new ResultSlotMsg();
        boolean enoughPair = false;
        ArrayList<AwardsOnLine> awardsOnLines = new ArrayList<AwardsOnLine>();
        long totalPrizes = 0L;
        block4:
        while (!enoughPair) {
            result = 0;
            awardsOnLines.clear();
            totalPrizes = 0L;
            String linesWin = "";
            String prizesOnLine = "";
            String haiSao = "";
            VQVItem[][] matrix = VQVUtils.generateMatrix();
            for (String entry2 : lineArr) {
                ArrayList<VQVAward> awardList = new ArrayList<VQVAward>();
                Line line = VQVUtils.getLine(this.lines, matrix, Integer.parseInt(entry2));
                VQVUtils.calculateLine(line, awardList);
                for (VQVAward award : awardList) {
                    long money = 0L;
                    if (award.getRatio() <= 0.0f) continue block4;
                    money = (long) (award.getRatio() * (float) this.betValue);
                    AwardsOnLine aol = new AwardsOnLine(award, money, line.getName());
                    awardsOnLines.add(aol);
                }
            }
            StringBuilder builderLinesWin = new StringBuilder();
            StringBuilder builderPrizesOnLine = new StringBuilder();
            for (AwardsOnLine entry2 : awardsOnLines) {
                totalPrizes += entry2.getMoney();
                builderLinesWin.append(",");
                builderLinesWin.append(entry2.getLineId());
                builderPrizesOnLine.append(",");
                builderPrizesOnLine.append(entry2.getMoney());
            }
            if (builderLinesWin.length() > 0) {
                builderLinesWin.deleteCharAt(0);
            }
            if (builderPrizesOnLine.length() > 0) {
                builderPrizesOnLine.deleteCharAt(0);
            }
            if (this.fund - totalPrizes < 0L || totalPrizes > (long) ConfigGame.getIntValue("max_prize_free_daily", 2000))
                continue;
            enoughPair = true;
            boolean updated = this.slotService.updateLuotQuayFreeDaily(this.gameName, username, this.betValue);
            if (!updated) {
                result = 103;
            } else {
                MoneyResponse moneyRes;
                String des;
                long moneyExchange = totalPrizes;
                if (moneyExchange > 0L && (moneyRes = this.userService.updateMoney(username, totalPrizes, this.moneyTypeStr, "ThoDanVqFree", des = "Tho Dan Free", "C\u01b0\u1ee3c: 0, Th\u1eafng: " + totalPrizes, 0L, null, TransType.NO_VIPPOINT)) != null && moneyRes.isSuccess()) {
                    currentMoney = moneyRes.getCurrentMoney();
                }
            }
            linesWin = builderLinesWin.toString();
            prizesOnLine = builderPrizesOnLine.toString();
            msg.referenceId = refernceId;
            msg.matrix = VQVUtils.matrixToString(matrix);
            msg.linesWin = linesWin;
            msg.prize = totalPrizes;
            msg.haiSao = "";
            try {
                this.slotService.logVQV(refernceId, username, this.betValue, "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20", linesWin, prizesOnLine, result, totalPrizes, currentTimeStr);
            } catch (InterruptedException moneyExchange) {
            } catch (TimeoutException moneyExchange) {
            } catch (IOException moneyExchange) {
            }
        }
        msg.result = (byte) result;
        msg.currentMoney = currentMoney;
        long endTime = System.currentTimeMillis();
        long handleTime = endTime - startTime;
        String ratioTime = CommonUtils.getRatioTime(handleTime);
        SlotUtils.logVQV(refernceId, username, this.betValue, msg.matrix, msg.haiSao, result, handleTime, ratioTime, currentTimeStr);
        return msg;
    }

    private MiniGameSlotResponse generatePickStars(int ratio) {
        MiniGameSlotResponse response = new MiniGameSlotResponse();
        int totalMoney = 0;
        ArrayList<PickStarGift> gifts = new ArrayList<PickStarGift>();
        PickStarGifts pickStarGifts = new PickStarGifts();
        String responsePickStars = "";
        boolean totalKeys = true;
        block5:
        for (int numPicks = 10; numPicks > 0; --numPicks) {
            PickStarGiftItem gift = pickStarGifts.pickRandomAndRandomGift();
            switch (gift) {
                case GOLD: {
                    totalMoney += 4 * this.betValue * 1;
                    gifts.add(new PickStarGift(PickStarGiftItem.GOLD, 0));
                    continue block5;
                }
                case KEY: {
                    gifts.add(new PickStarGift(PickStarGiftItem.KEY, 0));
                    continue block5;
                }
                case BOX: {
                    int boxValue = this.randomBoxValue();
                    totalMoney += boxValue * this.betValue * 1;
                    gifts.add(new PickStarGift(PickStarGiftItem.BOX, boxValue));
                    break;
                }
            }
        }
        totalMoney *= ratio;
        responsePickStars = responsePickStars + ratio;
        for (PickStarGift pickStarGift : gifts) {
            if (responsePickStars.length() == 0) {
                responsePickStars = responsePickStars + pickStarGift.getName();
                continue;
            }
            responsePickStars = responsePickStars + "," + pickStarGift.getName();
        }
        response.setTotalPrize(totalMoney);
        response.setPrizes(responsePickStars);
        return response;
    }

    private int randomBoxValue() {
        Random rd = new Random();
        int n = rd.nextInt(this.boxValues.size());
        return this.boxValues.get(n);
    }

    public short play(User user, String linesStr) {
        String username = user.getName();
        int numFree = 0;
        if (user.getProperty("numFreeDaily") != null) {
            numFree = (Integer) user.getProperty("numFreeDaily");
        }
        ResultSlotMsg msg = null;
        VQVFreeDailyMsg freeDailyMsg = new VQVFreeDailyMsg();
        if (numFree > 0) {
            msg = this.playFreeDaily(username);
            freeDailyMsg.remain = (byte) (--numFree);
            if (numFree > 0) {
                user.setProperty("numFreeDaily", numFree);
            } else {
                user.removeProperty("numFreeDaily");
            }
        } else {
            msg = this.play(username, linesStr);
        }
        if (this.isUserMinimize(user)) {
            MinimizeResultVQVMsg miniMsg = new MinimizeResultVQVMsg();
            miniMsg.prize = msg.prize;
            miniMsg.curretMoney = msg.currentMoney;
            miniMsg.result = msg.result;
            SlotUtils.sendMessageToUser(miniMsg, user);
        } else {
            SlotUtils.sendMessageToUser(msg, user);
            SlotUtils.sendMessageToUser(freeDailyMsg, user);
        }
        return msg.result;
    }

    private void saveFund() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - this.lastTimeUpdateFundToRoom >= 60000L) {
            try {
                this.mgService.saveFund(this.name, this.fund);
            } catch (IOException | InterruptedException | TimeoutException ex2) {
                Exception ex;
                Exception e = ex = ex2;
                Debug.trace(new Object[]{this.gameName + ": update fund error ", e.getMessage()});
            }
            this.lastTimeUpdateFundToRoom = currentTime;
        }
    }

    public void savePot() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - this.lastTimeUpdatePotToRoom >= 3000L) {
            this.lastTimeUpdatePotToRoom = currentTime;
            try {
                this.mgService.savePot(this.name, this.pot, this.huX2);
            } catch (IOException | InterruptedException | TimeoutException ex2) {
                Exception ex;
                Exception e = ex = ex2;
                Debug.trace(new Object[]{this.gameName + ": update pot poker error ", e.getMessage()});
            }
            byte x2 = (byte) (this.huX2 ? 1 : 0);
            ((VQVModule) this.module).updatePot(this.id, this.pot, x2);
        }
    }

    @Override
    public void playWinJackpot(String username, long betAmount, long referenceId) {
        KingUtil.printLog("VQVRoom playWinJackpot() username: " + username + ", betAmount: " + betAmount + ", referenceId: " + referenceId + ", pot: " + this.pot);
        long startTime = System.currentTimeMillis();
        String currentTimeStr = DateTimeUtils.getCurrentTime();
        short result = 0;
        String linesStr = "";
        StringBuilder linesStrBuilder = new StringBuilder();
        for (int i = 1; i <= 19; i++)
            linesStrBuilder.append(i + ",");
        linesStrBuilder.append(20);
        linesStr = linesStrBuilder.toString();
        String[] lineArr = linesStr.split(",");
        long currentMoney = this.userService.getMoneyUserCache(username, this.moneyTypeStr);
        UserCacheModel u = this.userService.forceGetCachedUser(username);
        long totalBetValue = lineArr.length * this.betValue;
        ResultSlotMsg msg = new ResultSlotMsg();
        long fee = totalBetValue * 2L / 100L;
        MoneyResponse moneyRes = this.userService.updateMoney(username, -totalBetValue, this.moneyTypeStr, this.gameName, "Quay Tho Dan", "\u0110\u1eb7t c\u01b0\u1ee3c Tho Dan", fee, Long.valueOf(referenceId), TransType.START_TRANS);
        if (moneyRes != null && moneyRes.isSuccess()) {
            long moneyToPot = totalBetValue * 1L / 100L;
            long moneyToFund = totalBetValue - fee - moneyToPot;
            // this.fund += moneyToFund;
            this.pot += moneyToPot;
            boolean enoughPair = false;
            ArrayList<AwardsOnLine> awardsOnLines = new ArrayList<AwardsOnLine>();
            long totalPrizes = 0L;
            long soTienNoHuKhongTruQuy = 0L;
            long tienThuongX2 = 0L;
            int countFreeSpin = 0;
            int ratio = 0;
            int ratioBonus = 0;
            block4:
            while (!enoughPair) {
                Random rd;
                int soLanNoHu;
                int n;
                result = 0;
                awardsOnLines.clear();
                totalPrizes = 0L;
                soTienNoHuKhongTruQuy = 0L;
                tienThuongX2 = 0L;
                String linesWin = "";
                String prizesOnLine = "";
                String haiSao = "";
                countFreeSpin = 0;
                ratio = 0;
                ratioBonus = 0;
                boolean forceNoHu = true;
                VQVItem[][] matrix = forceNoHu ? VQVUtils.generateMatrixNoHu(lineArr) : VQVUtils.generateMatrix();
                for (String entry2 : lineArr) {
                    ArrayList<VQVAward> awardList = new ArrayList<VQVAward>();
                    Line line = VQVUtils.getLine(this.lines, matrix, Integer.parseInt(entry2));
                    VQVUtils.calculateLine(line, awardList);
                    for (VQVAward award : awardList) {
                        long moneyOnLine = 0L;
                        if (award.getRatio() > 0.0f) {
                            moneyOnLine = (long) (award.getRatio() * (float) this.betValue);
                        } else if (award == VQVAward.PENTA_JACKPOT) {
                            if (result != 3) {
                                if (this.huX2) {
                                    moneyOnLine = this.pot * 2L;
                                    tienThuongX2 = this.pot;
                                    soTienNoHuKhongTruQuy += this.pot;
                                } else {
                                    moneyOnLine = this.pot;
                                }
                                result = 3;
                                soTienNoHuKhongTruQuy += this.pot - this.initPotValue;
                            } else {
                                moneyOnLine = this.initPotValue;
                            }
                        } else if (award.getRatio() == -2.0f) {
                            if (ratioBonus > 0) continue block4;
                            ratioBonus = 1;
                            if (award == VQVAward.QUADRA_BONUS) {
                                ratioBonus = 5;
                            }
                            MiniGameSlotResponse response = this.generatePickStars(ratioBonus);
                            moneyOnLine = response.getTotalPrize();
                            haiSao = response.getPrizes();
                            if (result != 3) {
                                result = 5;
                            }
                        } else if (award.getRatio() == -3.0f) {
                            if (countFreeSpin > 0) continue block4;
                            ++countFreeSpin;
                            ratio = 1;
                            if (award.getDuplicate() == VQVAward.PENTA_SCATTER.getDuplicate()) {
                                ratio = 15;
                            } else if (award.getDuplicate() == VQVAward.QUADRA_SCATTER.getDuplicate()) {
                                ratio = 5;
                            }
                        }
                        AwardsOnLine aol = new AwardsOnLine(award, moneyOnLine, line.getName());
                        awardsOnLines.add(aol);
                    }
                }
                StringBuilder builderLinesWin = new StringBuilder();
                StringBuilder builderPrizesOnLine = new StringBuilder();
                for (AwardsOnLine entry2 : awardsOnLines) {
                    totalPrizes += entry2.getMoney();
                    builderLinesWin.append(",");
                    builderLinesWin.append(entry2.getLineId());
                    builderPrizesOnLine.append(",");
                    builderPrizesOnLine.append(entry2.getMoney());
                }
                if (builderLinesWin.length() > 0) {
                    builderLinesWin.deleteCharAt(0);
                }
                if (builderPrizesOnLine.length() > 0) {
                    builderPrizesOnLine.deleteCharAt(0);
                }
                enoughPair = true;
                String matrixStr = VQVUtils.matrixToString(matrix);
                if (totalPrizes > 0L) {
                    if (result == 3) {
                        if (this.huX2) {
                            result = 4;
                        }
                        this.noHuX2();
                        this.pot = this.initPotValue;
                        // this.fund -= totalPrizes - soTienNoHuKhongTruQuy;
                        if (this.moneyType == 1) {
                            GameUtils.sendSMSToUser(username, "Chuc mung " + username + " da no hu game Tho Dan phong " + this.betValue + ". So tien no hu: " + totalPrizes + " " + "GS");
                        }
                        this.slotService.logNoHu(referenceId, this.gameName, username, this.betValue, linesStr, matrixStr, builderLinesWin.toString(), builderPrizesOnLine.toString(), totalPrizes, result, currentTimeStr);
                    } else {
                        // this.fund -= totalPrizes;
                        if (result == 0) {
                            result = totalPrizes >= (long) (this.betValue * 100) ? (short) 2 : 1;
                        }
                    }
                }
                long moneyExchange = totalPrizes - tienThuongX2;
                String des = "Quay Tho Dan";
                if (tienThuongX2 > 0L) {
                    this.userService.updateMoney(username, tienThuongX2, this.moneyTypeStr, this.gameName, des, "Th\u01b0\u1edfng h\u0169 X2", 0L, null, TransType.NO_VIPPOINT);
                }
                if ((moneyRes = this.userService.updateMoney(username, totalPrizes, this.moneyTypeStr, this.gameName, des, this.buildDescription(totalBetValue, totalPrizes, result), 0L, Long.valueOf(referenceId), TransType.END_TRANS)) != null && moneyRes.isSuccess()) {
                    currentMoney = moneyRes.getCurrentMoney();
                    if (this.moneyType == 1 && moneyExchange >= (long) BroadcastMessageServiceImpl.MIN_MONEY) {
                        this.broadcastMsgService.putMessage(Games.VUONG_QUOC_VIN.getId(), username, moneyExchange - totalBetValue);
                    }
                }
                linesWin = builderLinesWin.toString();
                prizesOnLine = builderPrizesOnLine.toString();
                msg.referenceId = referenceId;
                msg.matrix = matrixStr;
                msg.linesWin = linesWin;
                msg.prize = totalPrizes;
                msg.haiSao = haiSao;
                if (countFreeSpin > 0) {
                    msg.isFreeSpin = 1;
                    msg.ratio = (byte) ratio;
                    this.slotService.setLuotQuayFreeSlot(this.cacheFreeName, username, linesStr, countFreeSpin, ratio);
                } else {
                    msg.isFreeSpin = 0;
                }
                try {
                    this.slotService.logVQV(referenceId, username, this.betValue, linesStr, linesWin, prizesOnLine, result, totalPrizes, currentTimeStr);
                    if (result == 3 || result == 4) {
                        this.slotService.addTop("Tho Dan", username, this.betValue, totalPrizes, currentTimeStr, result);
                    }
                    if (result == 3 || result == 2 || result == 4) {
                        this.broadcastBigWin(username, (byte) result, totalPrizes);
                    }
                } catch (InterruptedException award) {
                } catch (TimeoutException award) {
                } catch (IOException award) {
                    // empty catch block
                }
                // this.saveFund();
                this.savePot();
            }
        } else {
            result = 102;
        }
        msg.result = (byte) result;
        msg.currentMoney = currentMoney;
        long endTime = System.currentTimeMillis();
        long handleTime = endTime - startTime;
        String ratioTime = CommonUtils.getRatioTime(handleTime);
        SlotUtils.logVQV(referenceId, username, this.betValue, msg.matrix, msg.haiSao, result, handleTime, ratioTime, currentTimeStr);
    }

    private boolean checkDieuKienNo(String username) {
        try {
            UserModel u = this.userService.getUserByUserName(username);
            return u.isBot();
        } catch (Exception u) {
            return false;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    protected void gameLoop() {
        ArrayList<AutoUser> usersPlay = new ArrayList<AutoUser>();
        Map map = this.usersAuto;
        synchronized (map) {
            for (AutoUser user : this.usersAuto.values()) {
                boolean play = user.incCount();
                if (!play) continue;
                usersPlay.add(user);
            }
        }
        int numThreads = usersPlay.size() / 100 + 1;
        for (int i = 1; i <= numThreads; ++i) {
            int fromIndex = (i - 1) * 100;
            int toIndex = i * 100;
            if (toIndex > usersPlay.size()) {
                toIndex = usersPlay.size();
            }
            ArrayList<AutoUser> tmp = new ArrayList<AutoUser>(usersPlay.subList(fromIndex, toIndex));
            SlotRoom.PlayListAutoUserTask task = new SlotRoom.PlayListAutoUserTask(tmp);
            this.executor.execute(task);
        }
        usersPlay.clear();
    }

    @Override
    protected void playListAuto(List<AutoUser> users) {
        for (AutoUser user : users) {
            short result = this.play(user.getUser(), user.getLines());
            if (result == 3 || result == 4 || result == 101 || result == 102 || result == 100) {
                this.forceStopAutoPlay(user.getUser());
                continue;
            }
            if (result == 0) {
                user.setMaxCount(4);
                continue;
            }
            if (result == 5) {
                user.setMaxCount(15);
                continue;
            }
            user.setMaxCount(8);
        }
        users.clear();
    }

    @Override
    public boolean joinRoom(User user) {
        KingUtil.printLog("VQV joinRoom user: " + user.getName());
        boolean result = super.joinRoom(user);
        SlotFreeDaily model = this.slotService.getLuotQuayFreeDaily(this.gameName, user.getName(), this.betValue);
        if (model != null && model.getRotateFree() > 0) {
            user.setProperty("numFreeDaily", model.getRotateFree());
            VQVFreeDailyMsg freeDailyMsg = new VQVFreeDailyMsg();
            freeDailyMsg.remain = (byte) model.getRotateFree();
            SlotUtils.sendMessageToUser(freeDailyMsg, user);
        } else {
            user.removeProperty("numFreeDaily");
        }
        if (result) {
            user.setProperty(("MGROOM_" + this.gameName + "_INFO"), this);
        }
        return result;
    }

}

