/*
 * Decompiled with CFR 0.144.
 *
 * Could not load the following classes:
 *  org.json.JSONObject
 */
package game.modules.slot.config;

import org.json.JSONObject;

import java.io.*;

public class MiniGameConfig {
    public JSONObject config;
    private static MiniGameConfig miniGameConfig = null;

    private MiniGameConfig() {
        this.initconfig();
    }

    public static MiniGameConfig instance() {
        if (miniGameConfig == null) {
            miniGameConfig = new MiniGameConfig();
        }
        return miniGameConfig;
    }

    public void initconfig() {
        String path = System.getProperty("user.dir");
        File file = new File(String.valueOf(path) + "/conf/slot.json");
        StringBuffer contents = new StringBuffer();
        try {
            try (InputStreamReader r = new InputStreamReader((InputStream) new FileInputStream(file), "UTF-8")) {
                BufferedReader reader = new BufferedReader(r);
                String text = null;
                while ((text = reader.readLine()) != null) {
                    contents.append(text).append(System.getProperty("line.separator"));
                }
            }
            this.config = new JSONObject(contents.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

