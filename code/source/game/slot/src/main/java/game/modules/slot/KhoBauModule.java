/*
 * Decompiled with CFR 0.144.
 *
 * Could not load the following classes:
 *  bitzero.server.BitZeroServer
 *  bitzero.server.core.BZEventParam
 *  bitzero.server.core.BZEventType
 *  bitzero.server.core.IBZEvent
 *  bitzero.server.core.IBZEventListener
 *  bitzero.server.core.IBZEventParam
 *  bitzero.server.core.IBZEventType
 *  bitzero.server.entities.User
 *  bitzero.server.exceptions.BZException
 *  bitzero.server.extensions.BZExtension
 *  bitzero.server.extensions.data.BaseMsg
 *  bitzero.server.extensions.data.DataCmd
 *  bitzero.server.util.TaskScheduler
 *  bitzero.util.common.business.Debug
 *  com.vinplay.dal.service.MiniGameService
 *  com.vinplay.dal.service.SlotMachineService
 *  com.vinplay.dal.service.impl.CacheServiceImpl
 *  com.vinplay.usercore.service.UserService
 *  com.vinplay.vbee.common.enums.Games
 *  com.vinplay.vbee.common.exceptions.KeyNotFoundException
 *  com.vinplay.vbee.common.utils.CommonUtils
 */
package game.modules.slot;

import bitzero.server.BitZeroServer;
import bitzero.server.core.BZEventParam;
import bitzero.server.core.BZEventType;
import bitzero.server.core.IBZEvent;
import bitzero.server.entities.User;
import bitzero.server.exceptions.BZException;
import bitzero.server.extensions.data.DataCmd;
import bitzero.util.common.business.Debug;
import casio.king365.util.KingUtil;
import com.vinplay.dal.service.impl.CacheServiceImpl;
import com.vinplay.vbee.common.enums.Games;
import com.vinplay.vbee.common.exceptions.KeyNotFoundException;
import com.vinplay.vbee.common.utils.CommonUtils;
import game.modules.slot.cmd.rev.khobau.*;
import game.modules.slot.cmd.send.khobau.KhoBauInfoMsg;
import game.modules.slot.cmd.send.khobau.UpdatePotKhoBauMsg;
import game.modules.slot.room.KhoBauRoom;
import game.modules.slot.room.SlotRoom;
import game.modules.slot.utils.SlotUtils;
import game.util.ConfigSlotGame;

import java.util.concurrent.TimeUnit;

public class KhoBauModule
        extends SlotModule {
    private static long referenceId = 1L;
    private String fullLines = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20";
    //    private Runnable x2Task = new X2Task(this);
    private Runnable x2Task = new X2Task();
    private byte[] x2Arr = new byte[3];

    public KhoBauModule() {
        this.gameName = Games.KHO_BAU.getName();
    }

    public void init() {
        super.init();
        long[] funds = new long[3];
        int[] initPotValues = new int[6];
        try {
            String initPotValuesStr = ConfigSlotGame.getValueString("KhoBau_init_pot_values");
            String[] arr = initPotValuesStr.split(",");
            for (int i = 0; i < arr.length; ++i) {
                initPotValues[i] = Integer.parseInt(arr[i]);
            }
            this.pots = this.service.getPots(Games.KHO_BAU.getName());
            Debug.trace(("KHO_BAU POTS: " + CommonUtils.arrayLongToString(this.pots)));
            funds = this.service.getFunds(Games.KHO_BAU.getName());
            Debug.trace(("KHO_BAU FUNDS: " + CommonUtils.arrayLongToString(funds)));
        } catch (Exception e) {
            Debug.trace(new Object[]{"Init POKE GO error ", e});
        }
        if (pots[0] < initPotValues[0]) pots[0] = initPotValues[0];
        if (pots[1] < initPotValues[1]) pots[1] = initPotValues[1];
        if (pots[2] < initPotValues[2]) pots[2] = initPotValues[2];
        this.rooms.put(this.gameName + "_vin_100", new KhoBauRoom(this, (byte) 0, this.gameName + "_vin_100", (short) 1, this.pots[0], funds[0], 100, initPotValues[0]));
        this.rooms.put(this.gameName + "_vin_1000", new KhoBauRoom(this, (byte) 1, this.gameName + "_vin_1000", (short) 1, this.pots[1], funds[1], 1000, initPotValues[1]));
        this.rooms.put(this.gameName + "_vin_10000", new KhoBauRoom(this, (byte) 2, this.gameName + "_vin_10000", (short) 1, this.pots[2], funds[2], 10000, initPotValues[2]));
        Debug.trace("INIT KHO BAU DONE");
        this.getParentExtension().addEventListener(BZEventType.USER_DISCONNECT, this);
        referenceId = this.slotService.getLastReferenceId(this.gameName);
        Debug.trace(("START KHO_BAU REFERENCE ID= " + referenceId));
        CacheServiceImpl sv = new CacheServiceImpl();
        try {
            sv.removeKey(this.gameName + "_last_day_x2");
        } catch (KeyNotFoundException e2) {
            Debug.trace("KEY NOT FOUND");
        }
        int lastDayFinish = SlotUtils.getLastDayX2(this.gameName);
        this.ngayX2 = SlotUtils.calculateTimePokeGoX2AsString(this.gameName, SlotUtils.getX2Days(this.gameName), lastDayFinish);
        int nextX2Time = SlotUtils.calculateTimePokeGoX2(this.gameName, SlotUtils.getX2Days(this.gameName), lastDayFinish);
        Debug.trace((this.gameName + " Ngay X2: " + this.ngayX2 + ", remain time = " + nextX2Time));
        if (nextX2Time >= 0) {
            BitZeroServer.getInstance().getTaskScheduler().schedule(this.x2Task, nextX2Time, TimeUnit.SECONDS);
        } else {
            this.startX2();
        }
        BitZeroServer.getInstance().getTaskScheduler().scheduleAtFixedRate(this.gameLoopTask, 10, 1, TimeUnit.SECONDS);
        try {
            KingUtil.printLog("KhoBau getNextTimeWinJackpot(): " + getNextTimeWinJackpot());
        } catch (Exception e) {
            e.printStackTrace();
            KingUtil.printException("KhoBau", e);
        }
    }

    @Override
    public long getNewReferenceId() {
        return ++referenceId;
    }

    @Override
    SlotRoom getSlotRoomByBet(int betAmount) {
        if (betAmount == 100)
            return this.rooms.get(this.gameName + "_vin_100");
        if (betAmount == 1000)
            return this.rooms.get(this.gameName + "_vin_1000");
        if (betAmount == 10000)
            return this.rooms.get(this.gameName + "_vin_10000");
        return null;
    }

    public void updatePot(byte id, long value, byte x2) {
        this.pots[id] = value;
        this.x2Arr[id] = x2;
        long currentTime = System.currentTimeMillis();
        if (currentTime - this.lastTimeUpdatePotToRoom >= 3000L) {
            UpdatePotKhoBauMsg msg = this.getPotsInfo();
            this.lastTimeUpdatePotToRoom = System.currentTimeMillis();
            SendMsgToAlLUsersThread t = new SendMsgToAlLUsersThread(msg);
//            SendMsgToAlLUsersThread t = new SendMsgToAlLUsersThread(this, msg);
            t.start();
        }
    }

    public UpdatePotKhoBauMsg getPotsInfo() {
        UpdatePotKhoBauMsg msg = new UpdatePotKhoBauMsg();
        msg.value100 = this.pots[0];
        msg.value1000 = this.pots[1];
        msg.value10000 = this.pots[2];
        msg.x2Room100 = this.x2Arr[0];
        msg.x2Room1000 = this.x2Arr[1];
        return msg;
    }

    public void handleServerEvent(IBZEvent ibzevent) throws BZException {
        if (ibzevent.getType() == BZEventType.USER_DISCONNECT) {
            User user = (User) ibzevent.getParameter(BZEventParam.USER);
            this.userDis(user);
        }
    }

    private void userDis(User user) {
        KhoBauRoom room = (KhoBauRoom) user.getProperty("MGROOM_KHO_BAU_INFO");
        if (room != null) {
            room.quitRoom(user);
            room.stopAutoPlay(user);
        }
    }

    @Override
    public void handleClientRequest(User user, DataCmd dataCmd) {
        switch (dataCmd.getId()) {
            case 2003: {
                this.subScribe(user, dataCmd);
                break;
            }
            case 2004: {
                this.unSubScribe(user, dataCmd);
                break;
            }
            case 2005: {
                this.changeRoom(user, dataCmd);
                break;
            }
            case 2006: {
                this.autoPlay(user, dataCmd);
                break;
            }
            case 2001: {
                this.playKhoBau(user, dataCmd);
                break;
            }
            case 2013: {
                this.minimize(user, dataCmd);
            }
        }
    }

    public void updatePotToUser(User user) {
        UpdatePotKhoBauMsg msg = this.getPotsInfo();
        SlotUtils.sendMessageToUser(msg, user);
    }

    protected void subScribe(User user, DataCmd dataCmd) {
        SubscribePokeGoCmd cmd = new SubscribePokeGoCmd(dataCmd);
        KhoBauRoom room = (KhoBauRoom) this.getRoom(cmd.roomId);
        if (room != null) {
            room.joinRoom(user);
            room.userMaximize(user);
            this.updatePotToUser(user);
            KhoBauInfoMsg msg = new KhoBauInfoMsg();
            msg.ngayX2 = this.ngayX2;
            msg.remain = 0;
            msg.currentMoney = this.userService.getMoneyUserCache(user.getName(), "vin");
            this.send(msg, user);
        } else {
            Debug.trace(("KHO_BAU SUBSCRIBE: room " + cmd.roomId + " not found"));
        }
    }

    protected void unSubScribe(User user, DataCmd dataCmd) {
        UnSubscribePokeGoCmd cmd = new UnSubscribePokeGoCmd(dataCmd);
        KhoBauRoom room = (KhoBauRoom) this.getRoom(cmd.roomId);
        if (room != null) {
            room.stopAutoPlay(user);
            room.quitRoom(user);
        } else {
            Debug.trace(("KHO_BAU UNSUBSCRIBE: room " + cmd.roomId + " not found"));
        }
    }

    protected void minimize(User user, DataCmd dataCmd) {
        MinimizeKhoBauCmd cmd = new MinimizeKhoBauCmd(dataCmd);
        KhoBauRoom room = (KhoBauRoom) this.getRoom(cmd.roomId);
        if (room != null) {
            room.quitRoom(user);
            room.userMinimize(user);
        } else {
            Debug.trace(("KHO_BAU MINIMIZE: room " + cmd.roomId + " not found"));
        }
    }

    protected void changeRoom(User user, DataCmd dataCmd) {
        ChangeRoomKhoBauCmd cmd = new ChangeRoomKhoBauCmd(dataCmd);
        KhoBauRoom roomLeaved = (KhoBauRoom) this.getRoom(cmd.roomLeavedId);
        KhoBauRoom roomJoined = (KhoBauRoom) this.getRoom(cmd.roomJoinedId);
        if (roomLeaved != null && roomJoined != null) {
            roomLeaved.stopAutoPlay(user);
            roomLeaved.quitRoom(user);
            roomJoined.joinRoom(user);
            this.updatePotToUser(user);
        } else {
            Debug.trace(("KHO_BAU: change room error, leaved= " + cmd.roomLeavedId + ", joined= " + cmd.roomJoinedId));
        }
    }

    private void playKhoBau(User user, DataCmd dataCmd) {
        PlayKhoBauCmd cmd = new PlayKhoBauCmd(dataCmd);
        KhoBauRoom room = (KhoBauRoom) user.getProperty("MGROOM_KHO_BAU_INFO");
        if (room != null) {
            room.play(user, cmd.lines);
        }
    }

    private void autoPlay(User user, DataCmd dataCMD) {
        AutoPlayKhoBauCmd cmd = new AutoPlayKhoBauCmd(dataCMD);
        KhoBauRoom room = (KhoBauRoom) user.getProperty("MGROOM_KHO_BAU_INFO");
        if (room != null) {
            if (cmd.autoPlay == 1) {
                short result = room.play(user, cmd.lines);
                if (result != 3 && result != 4 && result != 101 && result != 102 && result != 100) {
                    room.autoPlay(user, cmd.lines, result);
                } else {
                    room.forceStopAutoPlay(user);
                }
            } else {
                room.stopAutoPlay(user);
            }
        }
    }

    @Override
    protected String getRoomName(short moneyType, long baseBetting) {
        String moneyTypeStr = "xu";
        if (moneyType == 1) {
            moneyTypeStr = "vin";
        }
        return Games.KHO_BAU.getName() + "_" + moneyTypeStr + "_" + baseBetting;
    }

    @Override
    protected void gameLoop() {
        try {
            autoFund();
        } catch (Exception ex) {
            KingUtil.printException("SlotModule exception", ex);
            Debug.trace(ex.getMessage());
        }
    }
}
