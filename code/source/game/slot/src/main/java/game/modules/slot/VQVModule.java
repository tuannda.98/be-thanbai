/*
 * Decompiled with CFR 0.144.
 *
 * Could not load the following classes:
 *  bitzero.server.BitZeroServer
 *  bitzero.server.core.BZEventParam
 *  bitzero.server.core.BZEventType
 *  bitzero.server.core.IBZEvent
 *  bitzero.server.core.IBZEventListener
 *  bitzero.server.core.IBZEventParam
 *  bitzero.server.core.IBZEventType
 *  bitzero.server.entities.User
 *  bitzero.server.exceptions.BZException
 *  bitzero.server.extensions.BZExtension
 *  bitzero.server.extensions.data.BaseMsg
 *  bitzero.server.extensions.data.DataCmd
 *  bitzero.server.util.TaskScheduler
 *  bitzero.util.ExtensionUtility
 *  bitzero.util.common.business.Debug
 *  com.vinplay.dal.service.MiniGameService
 *  com.vinplay.dal.service.SlotMachineService
 *  com.vinplay.dal.service.impl.CacheServiceImpl
 *  com.vinplay.usercore.service.UserService
 *  com.vinplay.vbee.common.enums.Games
 *  com.vinplay.vbee.common.exceptions.KeyNotFoundException
 *  com.vinplay.vbee.common.models.slot.SlotFreeSpin
 *  com.vinplay.vbee.common.utils.CommonUtils
 */
package game.modules.slot;

import bitzero.server.BitZeroServer;
import bitzero.server.core.BZEventParam;
import bitzero.server.core.BZEventType;
import bitzero.server.core.IBZEvent;
import bitzero.server.entities.User;
import bitzero.server.exceptions.BZException;
import bitzero.server.extensions.data.BaseMsg;
import bitzero.server.extensions.data.DataCmd;
import bitzero.util.ExtensionUtility;
import bitzero.util.common.business.Debug;
import casio.king365.util.KingUtil;
import com.vinplay.dal.service.impl.CacheServiceImpl;
import com.vinplay.vbee.common.enums.Games;
import com.vinplay.vbee.common.exceptions.KeyNotFoundException;
import com.vinplay.vbee.common.models.slot.SlotFreeSpin;
import com.vinplay.vbee.common.utils.CommonUtils;
import game.modules.slot.cmd.rev.ndv.SubscribeNDVCmd;
import game.modules.slot.cmd.rev.vqv.*;
import game.modules.slot.cmd.send.vqv.UpdatePotVQVMsg;
import game.modules.slot.cmd.send.vqv.VQVInfoMsg;
import game.modules.slot.room.SlotRoom;
import game.modules.slot.room.VQVRoom;
import game.modules.slot.utils.SlotUtils;
import game.util.ConfigSlotGame;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class VQVModule
        extends SlotModule {
    private static long referenceId = 1L;
    private String fullLines = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20";
    private Runnable x2Task = new SlotModule.X2Task();
    private byte[] x2Arr = new byte[3];
    private List<User> users = new ArrayList<User>();
    private int countUpdateJackpot = 0;

    public VQVModule() {
        this.gameName = Games.VUONG_QUOC_VIN.getName();
    }

    public void init() {
        super.init();
        long[] funds = new long[3];
        int[] initPotValues = new int[6];
        try {
            String initPotValuesStr = ConfigSlotGame.getValueString(this.gameName + "_init_pot_values");
            String[] arr = initPotValuesStr.split(",");
            for (int i = 0; i < arr.length; ++i) {
                initPotValues[i] = Integer.parseInt(arr[i]);
            }
            this.pots = this.service.getPots(this.gameName);
            Debug.trace((this.gameName + " POTS: " + CommonUtils.arrayLongToString(this.pots)));
            funds = this.service.getFunds(this.gameName);
            Debug.trace((this.gameName + " FUNDS: " + CommonUtils.arrayLongToString(funds)));
        } catch (Exception e) {
            Debug.trace(new Object[]{"Init " + this.gameName + " error ", e});
        }
        if (pots[0] < initPotValues[0]) pots[0] = initPotValues[0];
        if (pots[1] < initPotValues[1]) pots[1] = initPotValues[1];
        if (pots[2] < initPotValues[2]) pots[2] = initPotValues[2];
        this.rooms.put(this.gameName + "_vin_100", new VQVRoom(this, (byte) 0, this.gameName + "_vin_100", (short) 1, this.pots[0], funds[0], 100, initPotValues[0]));
        this.rooms.put(this.gameName + "_vin_1000", new VQVRoom(this, (byte) 1, this.gameName + "_vin_1000", (short) 1, this.pots[1], funds[1], 1000, initPotValues[1]));
        this.rooms.put(this.gameName + "_vin_10000", new VQVRoom(this, (byte) 2, this.gameName + "_vin_10000", (short) 1, this.pots[2], funds[2], 10000, initPotValues[2]));
        Debug.trace(("INIT " + this.gameName + " DONE"));
        this.getParentExtension().addEventListener(BZEventType.USER_DISCONNECT, this);
        referenceId = this.slotService.getLastReferenceId(this.gameName);
        Debug.trace(("START " + this.gameName + " REFERENCE ID= " + referenceId));
        CacheServiceImpl sv = new CacheServiceImpl();
        try {
            sv.removeKey(this.gameName + "_last_day_x2");
        } catch (KeyNotFoundException e2) {
            Debug.trace("KEY NOT FOUND");
        }
        int lastDayFinish = SlotUtils.getLastDayX2(this.gameName);
        this.ngayX2 = SlotUtils.calculateTimePokeGoX2AsString(this.gameName, SlotUtils.getX2Days(this.gameName), lastDayFinish);
        int nextX2Time = SlotUtils.calculateTimePokeGoX2(this.gameName, SlotUtils.getX2Days(this.gameName), lastDayFinish);
        Debug.trace((this.gameName + " Ngay X2: " + this.ngayX2 + ", remain time = " + nextX2Time));
        if (nextX2Time >= 0) {
            BitZeroServer.getInstance().getTaskScheduler().schedule(this.x2Task, nextX2Time, TimeUnit.SECONDS);
        } else {
            this.startX2();
        }
        BitZeroServer.getInstance().getTaskScheduler().scheduleAtFixedRate(this.gameLoopTask, 10, 1, TimeUnit.SECONDS);
        try {
            KingUtil.printLog("VQV getNextTimeWinJackpot(): " + getNextTimeWinJackpot());
        } catch (Exception e) {
            e.printStackTrace();
            KingUtil.printException("VQVModule", e);
        }
    }

    @Override
    public long getNewReferenceId() {
        return ++referenceId;
    }

    public void updatePot(byte id, long value, byte x2) {
        this.pots[id] = value;
        this.x2Arr[id] = x2;
        long currentTime = System.currentTimeMillis();
        if (currentTime - this.lastTimeUpdatePotToRoom >= 3000L) {
            UpdatePotVQVMsg msg = this.getPotsInfo();
            this.lastTimeUpdatePotToRoom = System.currentTimeMillis();
            SlotModule.SendMsgToAlLUsersThread t = new SlotModule.SendMsgToAlLUsersThread(msg);
            t.start();
        }
    }

    public UpdatePotVQVMsg getPotsInfo() {
        UpdatePotVQVMsg msg = new UpdatePotVQVMsg();
        msg.value100 = this.pots[0];
        msg.value1000 = this.pots[1];
        msg.value10000 = this.pots[2];
        msg.x2Room100 = this.x2Arr[0];
        msg.x2Room1000 = this.x2Arr[1];
        return msg;
    }

    public void handleServerEvent(IBZEvent ibzevent) throws BZException {
        if (ibzevent.getType() == BZEventType.USER_DISCONNECT) {
            User user = (User) ibzevent.getParameter(BZEventParam.USER);
            this.userDis(user);
        }
    }

    private void userDis(User user) {
        VQVRoom room = (VQVRoom) user.getProperty(("MGROOM_" + this.gameName + "_INFO"));
        if (room != null) {
            room.quitRoom(user);
            room.stopAutoPlay(user);
        }
    }

    @Override
    public void handleClientRequest(User user, DataCmd dataCmd) {
        switch (dataCmd.getId()) {
            case 5003: {
                this.subScribe(user, dataCmd);
                break;
            }
            case 5004: {
                this.unSubScribe(user, dataCmd);
                break;
            }
            case 5005: {
                this.changeRoom(user, dataCmd);
                break;
            }
            case 5006: {
                this.autoPlay(user, dataCmd);
                break;
            }
            case 5001: {
                this.playVQV(user, dataCmd);
                break;
            }
            case 5013: {
                this.minimize(user, dataCmd);
            }
        }
    }

    public void updatePotToUser(User user) {
        UpdatePotVQVMsg msg = this.getPotsInfo();
        SlotUtils.sendMessageToUser(msg, user);
    }

    public void updateUserInfo(User user, VQVRoom room) {
        VQVInfoMsg msg = new VQVInfoMsg();
        msg.ngayX2 = this.ngayX2;
        msg.remain = 0;
        msg.currentRoom = room.getId();
        SlotFreeSpin freeSpin = this.slotService.getLuotQuayFreeSlot(String.valueOf(this.gameName) + room.getBetValue(), user.getName());
        if (freeSpin != null && freeSpin.getLines() != null) {
            msg.freeSpin = (byte) freeSpin.getNum();
            msg.lines = freeSpin.getLines();
        }
        msg.currentMoney = this.userService.getMoneyUserCache(user.getName(), "vin");
        this.send(msg, user);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected void subScribe(User user, DataCmd dataCmd) {
        KingUtil.printLog("VQV user: " + user.getName() + ", subScribe()");
        SubscribeNDVCmd cmd = new SubscribeNDVCmd(dataCmd);
        if (cmd.roomId == -1) {
            this.updatePotToUser(user);
            synchronized (this.users) {
                this.users.add(user);
                return;
            }
        }
        synchronized (this.users) {
            this.users.remove(user);
        }
        quitUserFromAllRoom(user);
        VQVRoom room = (VQVRoom) this.getRoom(cmd.roomId);
        if (room != null) {
            room.joinRoom(user);
            room.userMaximize(user);
            this.updatePotToUser(user);
            this.updateUserInfo(user, room);
        } else {
            Debug.trace((this.gameName + " SUBSCRIBE: room " + cmd.roomId + " not found"));
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected void unSubScribe(User user, DataCmd dataCmd) {
        KingUtil.printLog("VQV user: " + user.getName() + ", unSubScribe()");
        UnSubscribeVQVCmd cmd = new UnSubscribeVQVCmd(dataCmd);
        if (cmd.roomId == -1) {
            this.updatePotToUser(user);
            synchronized (this.users) {
                this.users.remove(user);
                return;
            }
        }
        VQVRoom room = (VQVRoom) this.getRoom(cmd.roomId);
        if (room != null) {
            room.stopAutoPlay(user);
            room.quitRoom(user);
        } else {
            Debug.trace((this.gameName + " UNSUBSCRIBE: room " + cmd.roomId + " not found"));
        }
    }

    protected void minimize(User user, DataCmd dataCmd) {
        KingUtil.printLog("VQV user: " + user.getName() + ", minimize()");
        MinimizeVQVCmd cmd = new MinimizeVQVCmd(dataCmd);
        VQVRoom room = (VQVRoom) this.getRoom(cmd.roomId);
        if (room != null) {
            room.quitRoom(user);
            room.userMinimize(user);
        } else {
            Debug.trace((this.gameName + " MINIMIZE: room " + cmd.roomId + " not found"));
        }
    }

    protected void changeRoom(User user, DataCmd dataCmd) {
        KingUtil.printLog("VQV user: " + user.getName() + ", changeRoom()");
        ChangeRoomVQVCmd cmd = new ChangeRoomVQVCmd(dataCmd);
        VQVRoom roomLeaved = (VQVRoom) this.getRoom(cmd.roomLeavedId);
        VQVRoom roomJoined = (VQVRoom) this.getRoom(cmd.roomJoinedId);
        if (roomLeaved != null && roomJoined != null) {
            roomLeaved.stopAutoPlay(user);
            roomLeaved.quitRoom(user);
            roomJoined.joinRoom(user);
            this.updatePotToUser(user);
            this.updateUserInfo(user, roomJoined);
        } else {
            Debug.trace((this.gameName + ": change room error, leaved= " + cmd.roomLeavedId + ", joined= " + cmd.roomJoinedId));
        }
    }

    protected void quitUserFromAllRoom(User user) {
        for (Map.Entry<String, SlotRoom> entry : this.rooms.entrySet()) {
            VQVRoom room = (VQVRoom) entry.getValue();
            room.quitRoom(user);
        }
    }

    private void playVQV(User user, DataCmd dataCmd) {
        PlayVQVCmd cmd = new PlayVQVCmd(dataCmd);
        VQVRoom room = (VQVRoom) user.getProperty(("MGROOM_" + this.gameName + "_INFO"));
        if (room != null) {
            room.play(user, cmd.lines);
        }
    }

    private void autoPlay(User user, DataCmd dataCMD) {
        AutoPlayVQVCmd cmd = new AutoPlayVQVCmd(dataCMD);
        VQVRoom room = (VQVRoom) user.getProperty(("MGROOM_" + this.gameName + "_INFO"));
        if (room != null) {
            if (cmd.autoPlay == 1) {
                short result = room.play(user, cmd.lines);
                if (result != 3 && result != 4 && result != 101 && result != 102 && result != 100) {
                    room.autoPlay(user, cmd.lines, result);
                } else {
                    room.forceStopAutoPlay(user);
                }
            } else {
                room.stopAutoPlay(user);
            }
        }
    }

    @Override
    protected String getRoomName(short moneyType, long baseBetting) {
        String moneyTypeStr = "xu";
        if (moneyType == 1) {
            moneyTypeStr = "vin";
        }
        return this.gameName + "_" + moneyTypeStr + "_" + baseBetting;
    }

    public void sendMessageToRoomLobby(BaseMsg msg) {
        ArrayList<User> usersCopy = new ArrayList<User>(this.users);
        for (User user : usersCopy) {
            ExtensionUtility.getExtension().send(msg, user);
        }
    }

    @Override
    SlotRoom getSlotRoomByBet(int betAmount) {
        if (betAmount == 100)
            return this.rooms.get(this.gameName + "_vin_100");
        if (betAmount == 1000)
            return this.rooms.get(this.gameName + "_vin_1000");
        if (betAmount == 10000)
            return this.rooms.get(this.gameName + "_vin_10000");
        return null;
    }

    @Override
    protected void gameLoop() {
        try {
            autoFund();
        } catch (Exception ex) {
            KingUtil.printException("VQVModule exception", ex);
            Debug.trace(ex.getMessage());
        }
    }
}
