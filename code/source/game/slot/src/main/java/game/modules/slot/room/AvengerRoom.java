/*
 * Decompiled with CFR 0.144.
 *
 * Could not load the following classes:
 *  bitzero.server.BitZeroServer
 *  bitzero.server.entities.User
 *  bitzero.server.extensions.data.BaseMsg
 *  bitzero.server.util.TaskScheduler
 *  bitzero.util.common.business.Debug
 *  com.vinplay.dal.service.BroadcastMessageService
 *  com.vinplay.dal.service.MiniGameService
 *  com.vinplay.dal.service.SlotMachineService
 *  com.vinplay.dal.service.impl.BroadcastMessageServiceImpl
 *  com.vinplay.dal.service.impl.CacheServiceImpl
 *  com.vinplay.usercore.service.UserService
 *  com.vinplay.vbee.common.enums.Games
 *  com.vinplay.vbee.common.models.cache.SlotFreeDaily
 *  com.vinplay.vbee.common.models.cache.UserCacheModel
 *  com.vinplay.vbee.common.models.slot.SlotFreeSpin
 *  com.vinplay.vbee.common.response.MoneyResponse
 *  com.vinplay.vbee.common.statics.TransType
 *  com.vinplay.vbee.common.utils.CommonUtils
 *  com.vinplay.vbee.common.utils.DateTimeUtils
 */
package game.modules.slot.room;

import bitzero.server.BitZeroServer;
import bitzero.server.entities.User;
import bitzero.util.common.business.Debug;
import casio.king365.util.KingUtil;
import com.vinplay.dal.service.impl.BroadcastMessageServiceImpl;
import com.vinplay.dal.service.impl.CacheServiceImpl;
import com.vinplay.vbee.common.enums.Games;
import com.vinplay.vbee.common.models.cache.SlotFreeDaily;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.models.slot.SlotFreeSpin;
import com.vinplay.vbee.common.response.MoneyResponse;
import com.vinplay.vbee.common.statics.TransType;
import com.vinplay.vbee.common.utils.CommonUtils;
import com.vinplay.vbee.common.utils.DateTimeUtils;
import game.modules.slot.AvengerModule;
import game.modules.slot.cmd.send.avengers.*;
import game.modules.slot.entities.slot.AutoUser;
import game.modules.slot.entities.slot.AwardsOnLine;
import game.modules.slot.entities.slot.Line;
import game.modules.slot.entities.slot.MiniGameSlotResponse;
import game.modules.slot.entities.slot.avengers.*;
import game.modules.slot.utils.AvengersUtils;
import game.modules.slot.utils.SlotUtils;
import game.util.ConfigGame;
import game.util.GameUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class AvengerRoom
        extends SlotRoom {
    private final Runnable gameLoopTask = new GameLoopTask();
    //    private final Runnable gameLoopTask = new GameLoopTask(this);
    private AvengersLines lines = new AvengersLines();
    private long lastTimeUpdatePotToRoom = 0L;
    private long lastTimeUpdateFundToRoom = 0L;
    private ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);

    public AvengerRoom(AvengerModule module, byte id, String name, short moneyType, long pot, long fund, int betValue, long initPotValue) {
        super(id, name, betValue, moneyType, pot, fund, initPotValue);
        this.module = module;
        this.moneyType = moneyType;
        this.gameName = Games.AVENGERS.getName();
        this.cacheFreeName = String.valueOf(this.gameName) + betValue;
        CacheServiceImpl cacheService = new CacheServiceImpl();
        cacheService.setValue(name, (int) pot);
        this.betValue = betValue;
        this.initPotValue = initPotValue;
        BitZeroServer.getInstance().getTaskScheduler().scheduleAtFixedRate(this.gameLoopTask, 10, 1, TimeUnit.SECONDS);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void forceStopAutoPlay(User user) {
        super.forceStopAutoPlay(user);
        synchronized (this.usersAuto) {
            this.usersAuto.remove(user.getName());
            ForceStopAutoPlayAvengersMsg msg = new ForceStopAutoPlayAvengersMsg();
            SlotUtils.sendMessageToUser(msg, user);
        }
    }

    public ResultAvengersMsg play(String username, String linesStr) {
        long referenceId = this.module.getNewReferenceId();
        SlotFreeSpin freeSpin = this.slotService.getLuotQuayFreeSlot(this.cacheFreeName, username);
        int luotQuayFree = freeSpin.getNum();
        int ratioFree = freeSpin.getRatio();
        if (luotQuayFree > 0) {
            linesStr = freeSpin.getLines();
            return this.playFree(username, linesStr, freeSpin.getItemsWild(), ratioFree, referenceId);
        }
        return this.playNormal(username, linesStr, referenceId);
    }

    public ResultAvengersMsg playNormal(String username, String linesStr, long referenceId) {
        long startTime = System.currentTimeMillis();
        String currentTimeStr = DateTimeUtils.getCurrentTime();
        short result = 0;
        String[] lineArr = linesStr.split(",");
        long currentMoney = this.userService.getMoneyUserCache(username, this.moneyTypeStr);
        UserCacheModel u = this.userService.forceGetCachedUser(username);
        long totalBetValue = lineArr.length * this.betValue;
        ResultAvengersMsg msg = new ResultAvengersMsg();
        if (lineArr.length > 0 && !linesStr.isEmpty()) {
            if (totalBetValue > 0L) {
                long minVin = this.userService.getAvailableVinMoney(username);
                if (totalBetValue <= minVin) {
                    long fee = totalBetValue * 2L / 100L;
                    MoneyResponse moneyRes = this.userService.updateMoney(username, -totalBetValue, this.moneyTypeStr, this.gameName, "Si\u00eau Anh H\u00f9ng", "\u0110\u1eb7t c\u01b0\u1ee3c Si\u00eau Anh H\u00f9ng", fee, Long.valueOf(referenceId), TransType.START_TRANS);
                    if (moneyRes != null && moneyRes.isSuccess()) {
                        long moneyToPot = totalBetValue * 1L / 100L;
                        long moneyToFund = totalBetValue - fee - moneyToPot;
                        this.fund += moneyToFund;
                        this.pot += moneyToPot;
                        boolean enoughPair = false;
                        ArrayList<AwardsOnLine<AvengersAward>> awardsOnLines = new ArrayList<AwardsOnLine<AvengersAward>>();
                        long totalPrizes = 0L;
                        long soTienNoHuKhongTruQuy = 0L;
                        long tienThuongX2 = 0L;
                        int countScatter = 0;
                        int countBonus = 0;
                        MiniGameSlotResponse miniGameSlot = null;
                        block4:
                        while (!enoughPair) {
                            int n;
                            int soLanNoHu;
                            Random rd;
                            result = 0;
                            awardsOnLines.clear();
                            totalPrizes = 0L;
                            soTienNoHuKhongTruQuy = 0L;
                            tienThuongX2 = 0L;
                            String linesWin = "";
                            String prizesOnLine = "";
                            miniGameSlot = null;
                            countScatter = 0;
                            countBonus = 0;
                            boolean forceNoHu = false;
//                            if (lineArr.length >= 5 && (soLanNoHu = ConfigGame.getIntValue(String.valueOf(this.gameName) + "_so_lan_no_hu")) > 0 && this.fund > this.initPotValue * 2L && (n = (rd = new Random()).nextInt(soLanNoHu)) == 0) {
//                                forceNoHu = true;
//                            }
                            if (betValue == 100) {
                                if (lineArr.length >= 5 && (soLanNoHu = ConfigGame.getIntValue(this.gameName + "_so_lan_no_hu_100")) > 0 && this.fund > this.initPotValue * 2L && (n = (rd = new Random()).nextInt(soLanNoHu)) == 0) {
                                    forceNoHu = true;
                                }
                            } else if (betValue == 1000) {
                                if (lineArr.length >= 5 && (soLanNoHu = ConfigGame.getIntValue(this.gameName + "_so_lan_no_hu_1000")) > 0 && this.fund > this.initPotValue * 2L && (n = (rd = new Random()).nextInt(soLanNoHu)) == 0) {
                                    forceNoHu = true;
                                }
                            } else {
                                if (lineArr.length >= 5 && (soLanNoHu = ConfigGame.getIntValue(this.gameName + "_so_lan_no_hu_10000")) > 0 && this.fund > this.initPotValue * 2L && (n = (rd = new Random()).nextInt(soLanNoHu)) == 0) {
                                    forceNoHu = true;
                                }
                            }
                            AvengersItem[][] matrix = forceNoHu ? AvengersUtils.generateMatrixNoHu(lineArr) : AvengersUtils.generateMatrix();
                            for (int i = 0; i < 3; ++i) {
                                for (int j = 0; j < 5; ++j) {
                                    if (matrix[i][j] == AvengersItem.SCATTER) {
                                        ++countScatter;
                                        continue;
                                    }
                                    if (matrix[i][j] != AvengersItem.BONUS) continue;
                                    ++countBonus;
                                }
                            }
                            if (countBonus >= 3 || countScatter >= 3) {
                                Random rd2 = new Random();
                                int tiLeAn = lineArr.length * 100 / 25;
                                int n2 = rd2.nextInt(100);
                                if (n2 >= tiLeAn) continue;
                            }
                            if (countBonus >= 3) {
                                miniGameSlot = AvengersUtils.addMiniGameSlot(this.betValue, countBonus);
                                AvengersAward award = AvengersAwards.getAward(AvengersItem.BONUS, countBonus);
                                AwardsOnLine<AvengersAward> aol = new AwardsOnLine<AvengersAward>(award, miniGameSlot.getTotalPrize(), "line0");
                                awardsOnLines.add(aol);
                                result = 5;
                            }
                            AvengersItem[][] matrixWild = AvengersUtils.revertMatrix(matrix);
                            for (String entry2 : lineArr) {
                                ArrayList<AvengersAward> awardList = new ArrayList<AvengersAward>();
                                Line line = AvengersUtils.getLine(this.lines, matrixWild, Integer.parseInt(entry2));
                                AvengersUtils.calculateLine(line, awardList);
                                for (AvengersAward award2 : awardList) {
                                    long moneyOnLine = 0L;
                                    if (award2.getRatio() > 0.0f) {
                                        moneyOnLine = (long) (award2.getRatio() * (float) this.betValue);
                                    } else if (award2 == AvengersAward.PENTA_JACK_POT) {
                                        if (result == 3) {
                                            moneyOnLine = this.initPotValue;
                                        } else {
                                            if (this.huX2) {
                                                moneyOnLine = this.pot * 2L;
                                                tienThuongX2 = this.pot;
                                                soTienNoHuKhongTruQuy += this.pot;
                                            } else {
                                                moneyOnLine = this.pot;
                                            }
                                            result = 3;
                                            soTienNoHuKhongTruQuy += this.pot - this.initPotValue;
                                        }
                                    }
                                    AwardsOnLine<AvengersAward> aol2 = new AwardsOnLine<AvengersAward>(award2, moneyOnLine, line.getName());
                                    awardsOnLines.add(aol2);
                                }
                            }
                            StringBuilder builderLinesWin = new StringBuilder();
                            StringBuilder builderPrizesOnLine = new StringBuilder();
                            for (AwardsOnLine entry2 : awardsOnLines) {
                                if (betValue == 100) {
                                    if (ConfigGame.getIntValue(this.gameName + "_cho_bot_no_hu_100") == 0) // cho cả người và bot nổ hũ
                                    {
                                        if ((entry2.getAward() == AvengersAward.PENTA_JACK_POT || entry2.getAward() == AvengersAward.QUADAR_JACK_POT || entry2.getAward() == AvengersAward.TRIPLE_JACK_POT))
                                            continue block4;
                                    } else if (ConfigGame.getIntValue(this.gameName + "_cho_bot_no_hu_100") == 1) // chỉ cho bot nổ hũ
                                    {
                                        if ((entry2.getAward() == AvengersAward.PENTA_JACK_POT || entry2.getAward() == AvengersAward.QUADAR_JACK_POT || entry2.getAward() == AvengersAward.TRIPLE_JACK_POT) && !u.isBot())
                                            continue block4;
                                    } else if (ConfigGame.getIntValue(this.gameName + "_cho_bot_no_hu_100") == -1) // chỉ cho người nổ hũ
                                    {
                                        if ((entry2.getAward() == AvengersAward.PENTA_JACK_POT || entry2.getAward() == AvengersAward.QUADAR_JACK_POT || entry2.getAward() == AvengersAward.TRIPLE_JACK_POT) && u.isBot())
                                            continue block4;
                                    } else {
                                        if ((entry2.getAward() == AvengersAward.PENTA_JACK_POT || entry2.getAward() == AvengersAward.QUADAR_JACK_POT || entry2.getAward() == AvengersAward.TRIPLE_JACK_POT) && !u.isBot())
                                            continue block4;
                                    }
                                } else if (betValue == 1000) {
                                    if (ConfigGame.getIntValue(this.gameName + "_cho_bot_no_hu_1000") == 0) // cho cả người và bot nổ hũ
                                    {
                                        if ((entry2.getAward() == AvengersAward.PENTA_JACK_POT || entry2.getAward() == AvengersAward.QUADAR_JACK_POT || entry2.getAward() == AvengersAward.TRIPLE_JACK_POT))
                                            continue block4;
                                    } else if (ConfigGame.getIntValue(this.gameName + "_cho_bot_no_hu_1000") == 1) // chỉ cho bot nổ hũ
                                    {
                                        if ((entry2.getAward() == AvengersAward.PENTA_JACK_POT || entry2.getAward() == AvengersAward.QUADAR_JACK_POT || entry2.getAward() == AvengersAward.TRIPLE_JACK_POT) && !u.isBot())
                                            continue block4;
                                    } else if (ConfigGame.getIntValue(this.gameName + "_cho_bot_no_hu_1000") == -1) // chỉ cho người nổ hũ
                                    {
                                        if ((entry2.getAward() == AvengersAward.PENTA_JACK_POT || entry2.getAward() == AvengersAward.QUADAR_JACK_POT || entry2.getAward() == AvengersAward.TRIPLE_JACK_POT) && u.isBot())
                                            continue block4;
                                    } else {
                                        if ((entry2.getAward() == AvengersAward.PENTA_JACK_POT || entry2.getAward() == AvengersAward.QUADAR_JACK_POT || entry2.getAward() == AvengersAward.TRIPLE_JACK_POT) && !u.isBot())
                                            continue block4;
                                    }
                                } else {
                                    if (ConfigGame.getIntValue(this.gameName + "_cho_bot_no_hu_10000") == 0) // cho cả người và bot nổ hũ
                                    {
                                        if ((entry2.getAward() == AvengersAward.PENTA_JACK_POT || entry2.getAward() == AvengersAward.QUADAR_JACK_POT || entry2.getAward() == AvengersAward.TRIPLE_JACK_POT))
                                            continue block4;
                                    } else if (ConfigGame.getIntValue(this.gameName + "_cho_bot_no_hu_10000") == 1) // chỉ cho bot nổ hũ
                                    {
                                        if ((entry2.getAward() == AvengersAward.PENTA_JACK_POT || entry2.getAward() == AvengersAward.QUADAR_JACK_POT || entry2.getAward() == AvengersAward.TRIPLE_JACK_POT) && !u.isBot())
                                            continue block4;
                                    } else if (ConfigGame.getIntValue(this.gameName + "_cho_bot_no_hu_10000") == -1) // chỉ cho người nổ hũ
                                    {
                                        if ((entry2.getAward() == AvengersAward.PENTA_JACK_POT || entry2.getAward() == AvengersAward.QUADAR_JACK_POT || entry2.getAward() == AvengersAward.TRIPLE_JACK_POT) && u.isBot())
                                            continue block4;
                                    } else {
                                        if ((entry2.getAward() == AvengersAward.PENTA_JACK_POT || entry2.getAward() == AvengersAward.QUADAR_JACK_POT || entry2.getAward() == AvengersAward.TRIPLE_JACK_POT) && !u.isBot())
                                            continue block4;
                                    }
                                }
                                // bot or not if (entry2.getAward() == AvengersAward.PENTA_JACK_POT && !u.isBot()) continue block4;
                                totalPrizes += entry2.getMoney();
                                builderLinesWin.append(",");
                                builderLinesWin.append(entry2.getLineId());
                                builderPrizesOnLine.append(",");
                                builderPrizesOnLine.append(entry2.getMoney());
                            }
                            if (builderLinesWin.length() > 0) {
                                builderLinesWin.deleteCharAt(0);
                            }
                            if (builderPrizesOnLine.length() > 0) {
                                builderPrizesOnLine.deleteCharAt(0);
                            }
                            if (result == 3 ? this.fund - (totalPrizes - soTienNoHuKhongTruQuy) < 0L : this.fund - totalPrizes < this.initPotValue * 2L && totalPrizes - totalBetValue >= 0L)
                                continue;
                            enoughPair = true;
                            String matrixStr = AvengersUtils.matrixToString(matrix);
                            if (totalPrizes > 0L) {
                                if (result == 3) {
                                    if (this.huX2) {
                                        result = 4;
                                    }
                                    this.noHuX2();
                                    this.pot = this.initPotValue;
                                    this.fund -= totalPrizes - soTienNoHuKhongTruQuy;
                                    if (this.moneyType == 1) {
                                        GameUtils.sendSMSToUser(username, "Chuc mung " + username + " da no hu game Sieu anh hung phong " + this.betValue + ". So tien no hu: " + totalPrizes + " vin");
                                    }
                                    this.slotService.logNoHu(referenceId, this.gameName, username, this.betValue, linesStr, matrixStr, builderLinesWin.toString(), builderPrizesOnLine.toString(), totalPrizes, result, currentTimeStr);
                                } else {
                                    this.fund -= totalPrizes;
                                    if (result == 0) {
                                        result = totalPrizes >= (long) (this.betValue * 100) ? (short) 2 : 1;
                                    }
                                }
                            }
                            msg.freeSpin = (byte) this.setFreeSpin(username, linesStr, countScatter);
                            long moneyExchange = totalPrizes - tienThuongX2;
                            String des = "Quay Si\u00eau Anh H\u00f9ng ";
                            if (tienThuongX2 > 0L) {
                                this.userService.updateMoney(username, tienThuongX2, this.moneyTypeStr, this.gameName, "Quay Si\u00eau Anh H\u00f9ng ", "Th\u01b0\u1edfng h\u0169 X2", 0L, null, TransType.NO_VIPPOINT);
                            }
                            if ((moneyRes = this.userService.updateMoney(username, moneyExchange, this.moneyTypeStr, this.gameName, "Quay Si\u00eau Anh H\u00f9ng ", this.buildDescription(totalBetValue, totalPrizes, result), 0L, Long.valueOf(referenceId), TransType.END_TRANS)) != null && moneyRes.isSuccess()) {
                                currentMoney = moneyRes.getCurrentMoney();
                                if (this.moneyType == 1 && moneyExchange - (long) this.betValue >= (long) BroadcastMessageServiceImpl.MIN_MONEY) {
                                    this.broadcastMsgService.putMessage(Games.AVENGERS.getId(), username, moneyExchange - (long) this.betValue);
                                }
                            }
                            linesWin = builderLinesWin.toString();
                            prizesOnLine = builderPrizesOnLine.toString();
                            msg.referenceId = referenceId;
                            msg.matrix = AvengersUtils.matrixToString(matrix);
                            msg.linesWin = linesWin;
                            msg.prize = totalPrizes;
                            msg.isFreeSpin = false;
                            if (miniGameSlot != null) {
                                msg.haiSao = miniGameSlot.getPrizes();
                            }
                            try {
                                this.slotService.logAvengers(referenceId, username, this.betValue, linesStr, linesWin, prizesOnLine, result, totalPrizes, currentTimeStr);
                                if (result == 3 || result == 4) {
                                    this.slotService.addTop(this.gameName, username, this.betValue, totalPrizes, currentTimeStr, result);
                                }
                                if (result == 3 || result == 2 || result == 4) {
                                    BigWinAvengersMsg bigWinMsg = new BigWinAvengersMsg();
                                    bigWinMsg.username = username;
                                    bigWinMsg.type = (byte) result;
                                    bigWinMsg.betValue = (short) this.betValue;
                                    bigWinMsg.totalPrizes = totalPrizes;
                                    bigWinMsg.timestamp = DateTimeUtils.getCurrentTime();
                                    this.module.sendMsgToAllUsers(bigWinMsg);
                                }
                            } catch (InterruptedException bigWinMsg) {
                            } catch (TimeoutException bigWinMsg) {
                            } catch (IOException bigWinMsg) {
                                // empty catch block
                            }
                            this.saveFund();
                            this.savePot();
                        }
                    }
                } else {
                    result = 102;
                }
            } else {
                result = 101;
            }
        } else {
            result = 101;
        }
        msg.result = (byte) result;
        msg.currentMoney = currentMoney;
        long endTime = System.currentTimeMillis();
        long handleTime = endTime - startTime;
        String ratioTime = CommonUtils.getRatioTime(handleTime);
        SlotUtils.logAvengers(referenceId, username, this.betValue, msg.matrix, msg.haiSao, result, handleTime, ratioTime, currentTimeStr);
        return msg;
    }

    public void playWinJackpot(String username, long betAmount, long referenceId) {
        KingUtil.printLog("AvengerRoom playWinJackpot() username: " + username + ", betAmount: " + betAmount + ", referenceId: " + referenceId + ", pot: " + this.pot);
        StringBuilder linesStrBuilder = new StringBuilder();
        String linesStr = "";
        for (int i = 1; i <= 24; i++)
            linesStrBuilder.append(i + ",");
        linesStrBuilder.append(25);
        linesStr = linesStrBuilder.toString();
        long startTime = System.currentTimeMillis();
        String currentTimeStr = DateTimeUtils.getCurrentTime();
        short result = 0;
        String[] lineArr = linesStr.split(",");
        long currentMoney = this.userService.getMoneyUserCache(username, this.moneyTypeStr);
        UserCacheModel u = this.userService.forceGetCachedUser(username);
        long totalBetValue = lineArr.length * this.betValue;
        ResultAvengersMsg msg = new ResultAvengersMsg();
        long fee = totalBetValue * 2L / 100L;
        long moneyToPot = totalBetValue * 1L / 100L;
        this.pot += moneyToPot;
        boolean enoughPair = false;
        ArrayList<AwardsOnLine<AvengersAward>> awardsOnLines = new ArrayList<AwardsOnLine<AvengersAward>>();
        long totalPrizes = 0L;
        long soTienNoHuKhongTruQuy = 0L;
        long tienThuongX2 = 0L;
        int countScatter = 0;
        int countBonus = 0;
        MiniGameSlotResponse miniGameSlot = null;
        MoneyResponse moneyRes;
        block4:
        while (!enoughPair) {
            int n;
            int soLanNoHu;
            Random rd;
            result = 0;
            awardsOnLines.clear();
            totalPrizes = 0L;
            soTienNoHuKhongTruQuy = 0L;
            tienThuongX2 = 0L;
            String linesWin = "";
            String prizesOnLine = "";
            miniGameSlot = null;
            countScatter = 0;
            countBonus = 0;
            boolean forceNoHu = true;
            AvengersItem[][] matrix = forceNoHu ? AvengersUtils.generateMatrixNoHu(lineArr) : AvengersUtils.generateMatrix();
            for (int i = 0; i < 3; ++i) {
                for (int j = 0; j < 5; ++j) {
                    if (matrix[i][j] == AvengersItem.SCATTER) {
                        ++countScatter;
                        continue;
                    }
                    if (matrix[i][j] != AvengersItem.BONUS) continue;
                    ++countBonus;
                }
            }
            if (countBonus >= 3 || countScatter >= 3) {
                Random rd2 = new Random();
                int tiLeAn = lineArr.length * 100 / 25;
                int n2 = rd2.nextInt(100);
                if (n2 >= tiLeAn) continue;
            }
            if (countBonus >= 3) {
                miniGameSlot = AvengersUtils.addMiniGameSlot(this.betValue, countBonus);
                AvengersAward award = AvengersAwards.getAward(AvengersItem.BONUS, countBonus);
                AwardsOnLine<AvengersAward> aol = new AwardsOnLine<AvengersAward>(award, miniGameSlot.getTotalPrize(), "line0");
                awardsOnLines.add(aol);
                result = 5;
            }
            AvengersItem[][] matrixWild = AvengersUtils.revertMatrix(matrix);
            for (String entry2 : lineArr) {
                ArrayList<AvengersAward> awardList = new ArrayList<AvengersAward>();
                Line line = AvengersUtils.getLine(this.lines, matrixWild, Integer.parseInt(entry2));
                AvengersUtils.calculateLine(line, awardList);
                for (AvengersAward award2 : awardList) {
                    long moneyOnLine = 0L;
                    if (award2.getRatio() > 0.0f) {
                        moneyOnLine = (long) (award2.getRatio() * (float) this.betValue);
                    } else if (award2 == AvengersAward.PENTA_JACK_POT) {
                        if (result == 3) {
                            moneyOnLine = this.initPotValue;
                        } else {
                            if (this.huX2) {
                                moneyOnLine = this.pot * 2L;
                                tienThuongX2 = this.pot;
                                soTienNoHuKhongTruQuy += this.pot;
                            } else {
                                moneyOnLine = this.pot;
                            }
                            result = 3;
                            soTienNoHuKhongTruQuy += this.pot - this.initPotValue;
                        }
                    }
                    AwardsOnLine<AvengersAward> aol2 = new AwardsOnLine<AvengersAward>(award2, moneyOnLine, line.getName());
                    awardsOnLines.add(aol2);
                }
            }
            StringBuilder builderLinesWin = new StringBuilder();
            StringBuilder builderPrizesOnLine = new StringBuilder();
            for (AwardsOnLine entry2 : awardsOnLines) {
                totalPrizes += entry2.getMoney();
                builderLinesWin.append(",");
                builderLinesWin.append(entry2.getLineId());
                builderPrizesOnLine.append(",");
                builderPrizesOnLine.append(entry2.getMoney());
            }
            if (builderLinesWin.length() > 0) {
                builderLinesWin.deleteCharAt(0);
            }
            if (builderPrizesOnLine.length() > 0) {
                builderPrizesOnLine.deleteCharAt(0);
            }
            enoughPair = true;
            String matrixStr = AvengersUtils.matrixToString(matrix);
            if (totalPrizes > 0L) {
                if (result == 3) {
                    KingUtil.printLog("win jackpot, result: " + result);
                    if (this.huX2) {
                        result = 4;
                    }
                    this.noHuX2();
                    this.pot = this.initPotValue;
                    if (this.moneyType == 1) {
                        GameUtils.sendSMSToUser(username, "Chuc mung " + username + " da no hu game Sieu anh hung phong " + this.betValue + ". So tien no hu: " + totalPrizes + " vin");
                    }
                    this.slotService.logNoHu(referenceId, this.gameName, username, this.betValue, linesStr, matrixStr, builderLinesWin.toString(), builderPrizesOnLine.toString(), totalPrizes, result, currentTimeStr);
                } else {
                    if (result == 0) {
                        result = totalPrizes >= (long) (this.betValue * 100) ? (short) 2 : 1;
                    }
                }
            }
            msg.freeSpin = (byte) this.setFreeSpin(username, linesStr, countScatter);
            long moneyExchange = totalPrizes - tienThuongX2;
            String des = "Quay Si\u00eau Anh H\u00f9ng ";
            if (tienThuongX2 > 0L) {
                this.userService.updateMoney(username, tienThuongX2, this.moneyTypeStr, this.gameName, "Quay Si\u00eau Anh H\u00f9ng ", "Th\u01b0\u1edfng h\u0169 X2", 0L, null, TransType.NO_VIPPOINT);
            }
            if ((moneyRes = this.userService.updateMoney(username, moneyExchange, this.moneyTypeStr, this.gameName, "Quay Si\u00eau Anh H\u00f9ng ", this.buildDescription(totalBetValue, totalPrizes, result), 0L, Long.valueOf(referenceId), TransType.END_TRANS)) != null && moneyRes.isSuccess()) {
                currentMoney = moneyRes.getCurrentMoney();
                if (this.moneyType == 1 && moneyExchange - (long) this.betValue >= (long) BroadcastMessageServiceImpl.MIN_MONEY) {
                    this.broadcastMsgService.putMessage(Games.AVENGERS.getId(), username, moneyExchange - (long) this.betValue);
                }
            }
            linesWin = builderLinesWin.toString();
            prizesOnLine = builderPrizesOnLine.toString();
            msg.referenceId = referenceId;
            msg.matrix = AvengersUtils.matrixToString(matrix);
            msg.linesWin = linesWin;
            msg.prize = totalPrizes;
            msg.isFreeSpin = false;
            if (miniGameSlot != null) {
                msg.haiSao = miniGameSlot.getPrizes();
            }
            try {
                this.slotService.logAvengers(referenceId, username, this.betValue, linesStr, linesWin, prizesOnLine, result, totalPrizes, currentTimeStr);
                if (result == 3 || result == 4) {
                    this.slotService.addTop(this.gameName, username, this.betValue, totalPrizes, currentTimeStr, result);
                }
                if (result == 3 || result == 2 || result == 4) {
                    BigWinAvengersMsg bigWinMsg = new BigWinAvengersMsg();
                    bigWinMsg.username = username;
                    bigWinMsg.type = (byte) result;
                    bigWinMsg.betValue = (short) this.betValue;
                    bigWinMsg.totalPrizes = totalPrizes;
                    bigWinMsg.timestamp = DateTimeUtils.getCurrentTime();
                    this.module.sendMsgToAllUsers(bigWinMsg);
                }
            } catch (Exception e) {
                KingUtil.printException("AvengerRoom playWinJackpot() Exception", e);
            }
            // this.saveFund();
            this.savePot();
        }
        KingUtil.printLog("pot after winjackpot: " + this.pot);
        msg.result = (byte) result;
        msg.currentMoney = currentMoney;
        long endTime = System.currentTimeMillis();
        long handleTime = endTime - startTime;
        String ratioTime = CommonUtils.getRatioTime(handleTime);
        SlotUtils.logAvengers(referenceId, username, this.betValue, msg.matrix, msg.haiSao, result, handleTime, ratioTime, currentTimeStr);
    }

    public ResultAvengersMsg playFreeDaily(String username, long referenceId) {
        String linesStr = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25";
        long startTime = System.currentTimeMillis();
        String currentTimeStr = DateTimeUtils.getCurrentTime();
        short result = 0;
        String[] lineArr = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25".split(",");
        long currentMoney = this.userService.getMoneyUserCache(username, this.moneyTypeStr);
        UserCacheModel u = this.userService.forceGetCachedUser(username);
        ResultAvengersMsg msg = new ResultAvengersMsg();
        boolean enoughPair = false;
        ArrayList<AwardsOnLine<AvengersAward>> awardsOnLines = new ArrayList<AwardsOnLine<AvengersAward>>();
        long totalPrizes = 0L;
        long tienThuongX2 = 0L;
        int countScatter = 0;
        int countBonus = 0;
        block4:
        while (!enoughPair) {
            result = 0;
            awardsOnLines.clear();
            totalPrizes = 0L;
            String linesWin = "";
            String prizesOnLine = "";
            countScatter = 0;
            countBonus = 0;
            AvengersItem[][] matrix = AvengersUtils.generateMatrix();
            for (int i = 0; i < 3; ++i) {
                for (int j = 0; j < 5; ++j) {
                    if (matrix[i][j] == AvengersItem.SCATTER) {
                        ++countScatter;
                        continue;
                    }
                    if (matrix[i][j] != AvengersItem.BONUS) continue;
                    ++countBonus;
                }
            }
            if (countBonus >= 3 || countScatter >= 3) continue;
            AvengersItem[][] matrixWild = AvengersUtils.revertMatrix(matrix);
            for (String entry2 : lineArr) {
                ArrayList<AvengersAward> awardList = new ArrayList<AvengersAward>();
                Line line = AvengersUtils.getLine(this.lines, matrixWild, Integer.parseInt(entry2));
                AvengersUtils.calculateLine(line, awardList);
                for (AvengersAward award : awardList) {
                    long moneyOnLine = 0L;
                    if (award.getRatio() > 0.0f) {
                        moneyOnLine = (long) (award.getRatio() * (float) this.betValue);
                    } else if (award == AvengersAward.PENTA_JACK_POT) continue block4;
                    AwardsOnLine<AvengersAward> aol = new AwardsOnLine<AvengersAward>(award, moneyOnLine, line.getName());
                    awardsOnLines.add(aol);
                }
            }
            StringBuilder builderLinesWin = new StringBuilder();
            StringBuilder builderPrizesOnLine = new StringBuilder();
            for (AwardsOnLine entry2 : awardsOnLines) {
                if (entry2.getAward() == AvengersAward.PENTA_JACK_POT && !u.isBot()) continue block4;
                totalPrizes += entry2.getMoney();
                builderLinesWin.append(",");
                builderLinesWin.append(entry2.getLineId());
                builderPrizesOnLine.append(",");
                builderPrizesOnLine.append(entry2.getMoney());
            }
            if (builderLinesWin.length() > 0) {
                builderLinesWin.deleteCharAt(0);
            }
            if (builderPrizesOnLine.length() > 0) {
                builderPrizesOnLine.deleteCharAt(0);
            }
            if (totalPrizes > (long) ConfigGame.getIntValue("max_prize_free_daily", 2000)) continue;
            enoughPair = true;
            boolean updated = this.slotService.updateLuotQuayFreeDaily(this.gameName, username, this.betValue);
            if (!updated) {
                Debug.trace((username + " luot quay free " + this.gameName + " khong hop le"));
                result = 103;
                continue;
            }
            long moneyExchange = totalPrizes - 0L;
            if (moneyExchange > 0L) {
                String des = "Quay Si\u00eau Anh H\u00f9ng Free ";
                MoneyResponse moneyRes = this.userService.updateMoney(username, moneyExchange, this.moneyTypeStr, "SieuAnhHungVqFree", "Quay Si\u00eau Anh H\u00f9ng Free ", "C\u01b0\u1ee3c: 0, Th\u1eafng: " + totalPrizes, 0L, null, TransType.NO_VIPPOINT);
                if (moneyRes != null && moneyRes.isSuccess()) {
                    currentMoney = moneyRes.getCurrentMoney();
                }
            }
            linesWin = builderLinesWin.toString();
            prizesOnLine = builderPrizesOnLine.toString();
            msg.referenceId = referenceId;
            msg.matrix = AvengersUtils.matrixToString(matrix);
            msg.linesWin = linesWin;
            msg.prize = totalPrizes;
            msg.isFreeSpin = false;
            try {
                this.slotService.logAvengers(referenceId, username, this.betValue, "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25", linesWin, prizesOnLine, result, totalPrizes, currentTimeStr);
            } catch (InterruptedException des) {
            } catch (TimeoutException des) {
            } catch (IOException des) {
            }
        }
        msg.result = (byte) result;
        msg.currentMoney = currentMoney;
        long endTime = System.currentTimeMillis();
        long handleTime = endTime - startTime;
        String ratioTime = CommonUtils.getRatioTime(handleTime);
        SlotUtils.logAvengers(referenceId, username, this.betValue, msg.matrix, msg.haiSao, result, handleTime, ratioTime, currentTimeStr);
        return msg;
    }

    public ResultAvengersMsg playFree(String username, String linesStr, String itemsWild, int ratio, long referenceId) {
        long startTime = System.currentTimeMillis();
        String currentTimeStr = DateTimeUtils.getCurrentTime();
        short result = 0;
        String[] lineArr = linesStr.split(",");
        long currentMoney = this.userService.getMoneyUserCache(username, this.moneyTypeStr);
        long totalBetValue = 0L;
        ResultAvengersMsg msg = new ResultAvengersMsg();
        long fee = 0L;
        long moneyToPot = 0L;
        long moneyToFund = 0L;
        this.fund += 0L;
        this.pot += 0L;
        boolean enoughPair = false;
        ArrayList<AwardsOnLine<AvengersFreeSpinAward>> awardsOnLines = new ArrayList<AwardsOnLine<AvengersFreeSpinAward>>();
        long totalPrizes = 0L;
        while (!enoughPair) {
            SlotFreeSpin freeSpin;
            result = 0;
            awardsOnLines.clear();
            totalPrizes = 0L;
            String linesWin = "";
            String prizesOnLine = "";
            String haiSao = "";
            AvengersItem[][] matrix = AvengersUtils.generateMatrixFreeSpin(itemsWild);
            for (String entry : lineArr) {
                ArrayList<AvengersFreeSpinAward> awardList = new ArrayList<AvengersFreeSpinAward>();
                Line line = AvengersUtils.getLine(this.lines, matrix, Integer.parseInt(entry));
                AvengersUtils.calculateFreeSpinLine(line, awardList);
                for (AvengersFreeSpinAward award : awardList) {
                    long moneyOnLine = 0L;
                    if (!(award.getRatio() > 0.0f)) continue;
                    moneyOnLine = (long) (award.getRatio() * (float) this.betValue);
                    AwardsOnLine<AvengersFreeSpinAward> aol = new AwardsOnLine<AvengersFreeSpinAward>(award, moneyOnLine, line.getName());
                    awardsOnLines.add(aol);
                }
            }
            StringBuilder builderLinesWin = new StringBuilder();
            StringBuilder builderPrizesOnLine = new StringBuilder();
            for (AwardsOnLine entry2 : awardsOnLines) {
                totalPrizes += entry2.getMoney();
                builderLinesWin.append(",");
                builderLinesWin.append(entry2.getLineId());
                builderPrizesOnLine.append(",");
                builderPrizesOnLine.append(entry2.getMoney());
            }
            if (builderLinesWin.length() > 0) {
                builderLinesWin.deleteCharAt(0);
            }
            if (builderPrizesOnLine.length() > 0) {
                builderPrizesOnLine.deleteCharAt(0);
            }
            int tmpPrizes = (int) totalPrizes;
            if (result == 3 ? this.fund - totalPrizes < 0L : this.fund - (totalPrizes *= ratio) < this.initPotValue * 2L && totalPrizes - 0L >= 0L)
                continue;
            enoughPair = true;
            if (totalPrizes > 0L) {
                this.fund -= totalPrizes;
                if (result == 0) {
                    result = 1;
                }
            }
            long moneyExchange = totalPrizes - 0L;
            String des = "Si\u00eau Anh H\u00f9ng - Free";
            MoneyResponse moneyRes = this.userService.updateMoney(username, moneyExchange, this.moneyTypeStr, this.gameName, "Si\u00eau Anh H\u00f9ng - Free", this.buildDescription(0L, totalPrizes, result), 0L, null, TransType.VIPPOINT);
            if (moneyRes != null && moneyRes.isSuccess()) {
                currentMoney = moneyRes.getCurrentMoney();
                if (this.moneyType == 1 && moneyExchange >= (long) BroadcastMessageServiceImpl.MIN_MONEY) {
                    this.broadcastMsgService.putMessage(Games.AVENGERS.getId(), username, moneyExchange);
                }
                this.slotService.addPrizes(this.cacheFreeName, username, tmpPrizes);
            }
            if ((freeSpin = this.slotService.updateLuotQuaySlotFree(this.cacheFreeName, username)).getNum() == 0) {
                AvengersTotalFreeSpin totalFreeSpinMsg = new AvengersTotalFreeSpin();
                totalFreeSpinMsg.prize = freeSpin.getPrizes();
                totalFreeSpinMsg.ratio = (byte) ratio;
                SlotUtils.sendMessageToUser(totalFreeSpinMsg, username);
            }
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 3; ++i) {
                for (int j = 0; j < 5; ++j) {
                    if (matrix[i][j] != AvengersItem.WILD) continue;
                    sb.append(i + "," + j + ",");
                }
            }
            if (sb.length() > 0) {
                sb.deleteCharAt(sb.length() - 1);
            }
            this.slotService.setItemsWild(this.cacheFreeName, username, sb.toString());
            linesWin = builderLinesWin.toString();
            prizesOnLine = builderPrizesOnLine.toString();
            msg.referenceId = referenceId;
            msg.matrix = AvengersUtils.matrixToString(matrix);
            msg.linesWin = linesWin;
            msg.prize = totalPrizes;
            msg.haiSao = "";
            msg.freeSpin = (byte) freeSpin.getNum();
            msg.isFreeSpin = true;
            msg.itemsWild = sb.toString();
            msg.ratioFree = (byte) ratio;
            try {
                this.slotService.logAvengers(referenceId, username, this.betValue, linesStr, linesWin, prizesOnLine, result, totalPrizes, currentTimeStr);
            } catch (InterruptedException i) {
            } catch (TimeoutException i) {
            } catch (IOException i) {
                // empty catch block
            }
            this.saveFund();
            this.savePot();
        }
        msg.result = (byte) result;
        msg.currentMoney = currentMoney;
        long endTime = System.currentTimeMillis();
        long handleTime = endTime - startTime;
        String ratioTime = CommonUtils.getRatioTime(handleTime);
        SlotUtils.logAvengers(referenceId, username, this.betValue, msg.matrix, msg.haiSao, result, handleTime, ratioTime, currentTimeStr);
        return msg;
    }

    private int setFreeSpin(String nickName, String lines, int countFreeSpin) {
        int soLuot = 0;
        switch (countFreeSpin) {
            case 3: {
                soLuot = 8;
                this.slotService.setLuotQuayFreeSlot(this.cacheFreeName, nickName, lines, soLuot, 1);
                break;
            }
            case 4: {
                soLuot = 8;
                this.slotService.setLuotQuayFreeSlot(this.cacheFreeName, nickName, lines, soLuot, 2);
                break;
            }
            case 5: {
                soLuot = 8;
                this.slotService.setLuotQuayFreeSlot(this.cacheFreeName, nickName, lines, soLuot, 3);
            }
        }
        return soLuot;
    }

    public short play(User user, String linesStr) {
        String username = user.getName();
        int numFree = 0;
        if (user.getProperty("numFreeDaily") != null) {
            numFree = (Integer) user.getProperty("numFreeDaily");
        }
        ResultAvengersMsg msg = null;
        AvengersFreeDailyMsg freeDailyMsg = new AvengersFreeDailyMsg();
        if (numFree > 0) {
            msg = this.playFreeDaily(username, this.module.getNewReferenceId());
            freeDailyMsg.remain = (byte) (--numFree);
            if (numFree > 0) {
                user.setProperty("numFreeDaily", numFree);
            } else {
                user.removeProperty("numFreeDaily");
            }
        } else {
            msg = this.play(username, linesStr);
        }
        if (this.isUserMinimize(user)) {
            MinimizeResultAvengerMsg miniMsg = new MinimizeResultAvengerMsg();
            miniMsg.prize = msg.prize;
            miniMsg.curretMoney = msg.currentMoney;
            miniMsg.result = msg.result;
            SlotUtils.sendMessageToUser(miniMsg, user);
        } else {
            SlotUtils.sendMessageToUser(msg, user);
            SlotUtils.sendMessageToUser(freeDailyMsg, user);
        }
        return msg.result;
    }

    private void saveFund() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - this.lastTimeUpdateFundToRoom >= 60000L) {
            try {
                this.mgService.saveFund(this.name, this.fund);
            } catch (IOException | InterruptedException | TimeoutException ex2) {
                Exception ex;
                Exception e = ex = ex2;
                Debug.trace(new Object[]{this.gameName + ": update fund error ", e.getMessage()});
            }
            this.lastTimeUpdateFundToRoom = currentTime;
        }
    }

    public void savePot() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - this.lastTimeUpdatePotToRoom >= 3000L) {
            this.lastTimeUpdatePotToRoom = currentTime;
            try {
                this.mgService.savePot(this.name, this.pot, this.huX2);
            } catch (IOException | InterruptedException | TimeoutException ex2) {
                Exception ex;
                Exception e = ex = ex2;
                Debug.trace(new Object[]{this.gameName + ": update pot error ", e.getMessage()});
            }
            UpdatePotAvengersMsg msg = new UpdatePotAvengersMsg();
            msg.value = this.pot;
            msg.x2 = (byte) (this.huX2 ? 1 : 0);
            AvengerModule avengerModule = (AvengerModule) this.module;
            msg.valueRoom1 = avengerModule.getPotValue(100);
            msg.valueRoom2 = avengerModule.getPotValue(1000);
            msg.valueRoom3 = avengerModule.getPotValue(10000);
            this.sendMessageToRoom(msg);
        }
    }

    public void updatePot(User user, long room100, long room1k, long room10k) {
        UpdatePotAvengersMsg msg = new UpdatePotAvengersMsg();
        msg.value = this.pot;
        msg.x2 = (byte) (this.huX2 ? 1 : 0);
        msg.valueRoom1 = room100;
        msg.valueRoom2 = room1k;
        msg.valueRoom3 = room10k;
        SlotUtils.sendMessageToUser(msg, user);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    protected void gameLoop() {
        ArrayList<AutoUser> usersPlay = new ArrayList<AutoUser>();
        synchronized (this.usersAuto) {
            for (AutoUser user : this.usersAuto.values()) {
                boolean play = user.incCount();
                if (!play) continue;
                usersPlay.add(user);
            }
        }
        int numThreads = usersPlay.size() / 100 + 1;
        for (int i = 1; i <= numThreads; ++i) {
            int fromIndex = (i - 1) * 100;
            int toIndex = i * 100;
            if (toIndex > usersPlay.size()) {
                toIndex = usersPlay.size();
            }
            ArrayList<AutoUser> tmp = new ArrayList<AutoUser>(usersPlay.subList(fromIndex, toIndex));
            PlayListAutoUserTask task = new PlayListAutoUserTask(tmp);
//            PlayListAutoUserTask task = new PlayListAutoUserTask(this, tmp);
            this.executor.execute(task);
        }
        usersPlay.clear();
    }

    @Override
    protected void playListAuto(List<AutoUser> users) {
        for (AutoUser user : users) {
            short result = this.play(user.getUser(), user.getLines());
            if (result == 3 || result == 4 || result == 101 || result == 102 || result == 100) {
                this.forceStopAutoPlay(user.getUser());
                continue;
            }
            if (result == 0) {
                user.setMaxCount(5);
                continue;
            }
            if (result == 5) {
                user.setMaxCount(20);
                continue;
            }
            user.setMaxCount(8);
        }
        users.clear();
    }

    @Override
    public boolean joinRoom(User user) {
        boolean result = super.joinRoom(user);
        SlotFreeDaily model = this.slotService.getLuotQuayFreeDaily(this.gameName, user.getName(), this.betValue);
        AvengersFreeDailyMsg freeDailyMsg = new AvengersFreeDailyMsg();
        if (model != null && model.getRotateFree() > 0) {
            user.setProperty("numFreeDaily", model.getRotateFree());
            freeDailyMsg.remain = (byte) model.getRotateFree();
        } else {
            user.removeProperty("numFreeDaily");
        }
        SlotUtils.sendMessageToUser(freeDailyMsg, user);
        if (result) {
            user.setProperty(("MGROOM_" + this.gameName + "_INFO"), this);
        }
        return result;
    }
}
