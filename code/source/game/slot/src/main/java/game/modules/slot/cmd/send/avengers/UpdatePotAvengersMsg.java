/*
 * Decompiled with CFR 0.144.
 * 
 * Could not load the following classes:
 *  bitzero.server.extensions.data.BaseMsg
 */
package game.modules.slot.cmd.send.avengers;

import bitzero.server.extensions.data.BaseMsg;
import java.nio.ByteBuffer;

public class UpdatePotAvengersMsg
extends BaseMsg {
    public long value;
    public byte x2 = 0;
    public long valueRoom1;
    public long valueRoom2;
    public long valueRoom3;

    public UpdatePotAvengersMsg() {
        super((short)4002);
    }

    public byte[] createData() {
        ByteBuffer bf = this.makeBuffer();
        bf.putLong(this.value);
        bf.put(this.x2);
        bf.putLong(this.valueRoom1);
        bf.putLong(this.valueRoom2);
        bf.putLong(this.valueRoom3);
        return this.packBuffer(bf);
    }
}

