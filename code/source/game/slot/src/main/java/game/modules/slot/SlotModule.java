package game.modules.slot;

import bitzero.server.BitZeroServer;
import bitzero.server.extensions.BaseClientRequestHandler;
import bitzero.server.extensions.data.BaseMsg;
import casio.king365.GU;
import casio.king365.util.KingUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vinplay.dal.entities.config.SlotAutoConfig;
import com.vinplay.dal.service.MiniGameService;
import com.vinplay.dal.service.SlotMachineService;
import com.vinplay.dal.service.impl.MiniGameServiceImpl;
import com.vinplay.dal.service.impl.SlotMachineServiceImpl;
import com.vinplay.usercore.service.UserService;
import com.vinplay.usercore.service.impl.GameConfigServiceImpl;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.response.ResultGameConfigResponse;
import game.modules.slot.cmd.send.khobau.PokeGoX2Msg;
import game.modules.slot.entities.BotMinigame;
import game.modules.slot.room.SlotRoom;
import game.modules.slot.utils.SlotUtils;
import game.util.ConfigSlotGame;
import vn.yotel.yoker.util.Util;

import java.time.LocalTime;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public abstract class SlotModule
        extends BaseClientRequestHandler {
    protected Map<String, SlotRoom> rooms = new HashMap<String, SlotRoom>();
    protected MiniGameService service = new MiniGameServiceImpl();
    protected SlotMachineService slotService = new SlotMachineServiceImpl();
    protected UserService userService = new UserServiceImpl();
    protected boolean eventX2 = false;
    protected String ngayX2;
    protected long[] pots = new long[3];
    protected long lastTimeUpdatePotToRoom = 0L;
    protected int countBot100;
    protected int countBot1000;
    protected int countBot10000;
    protected GameLoopTask gameLoopTask = new GameLoopTask();
    protected String gameName;
    protected X2Task x2Task = new X2Task();

    protected static final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    protected static SlotAutoConfig slotAutoConfig;
    protected Calendar calNextTimeWinJackpot;

    protected abstract String getRoomName(short var1, long var2);

    protected abstract void gameLoop();

    public abstract long getNewReferenceId();

    public void startX2() {
        this.eventX2 = true;
        this.ngayX2 = "";
    }

    public void stopX2() {
        this.eventX2 = false;
        this.ngayX2 = SlotUtils.calculateTimePokeGoX2AsString(this.gameName, SlotUtils.getX2Days(this.gameName), SlotUtils.getLastDayX2(this.gameName));
        PokeGoX2Msg msg = new PokeGoX2Msg();
        msg.ngayX2 = this.ngayX2;
        for (SlotRoom room : this.rooms.values()) {
            room.sendMessageToRoom(msg);
        }
        Calendar cal = Calendar.getInstance();
        int today = cal.get(7);
        SlotUtils.saveLastDayX2(this.gameName, today);
        int lastDayX2 = SlotUtils.getLastDayX2(this.gameName);
        int nextX2Time = SlotUtils.calculateTimePokeGoX2(this.gameName, SlotUtils.getX2Days(this.gameName), lastDayX2);
        BitZeroServer.getInstance().getTaskScheduler().schedule((Runnable) this.x2Task, nextX2Time, TimeUnit.SECONDS);
    }

    protected int getCountTimeBot(String name) {
        int n = ConfigSlotGame.getIntValue(name, 0);
        if (n == 0) {
            return 0;
        }
        if (BotMinigame.isNight()) {
            n *= 3;
        }
        return n;
    }

    protected SlotRoom getRoom(byte roomId) {
        short moneyType = this.getMoneyTypeFromRoomId(roomId);
        long baseBetting = this.getBaseBetting(roomId);
        String roomName = this.getRoomName(moneyType, baseBetting);
        SlotRoom room = this.rooms.get(roomName);
        return room;
    }

    protected short getMoneyTypeFromRoomId(byte roomId) {
        if (roomId >= 0 && roomId < 3)
            return 1;
        return 0;
    }

    protected long getBaseBetting(byte roomId) {
        switch (roomId) {
            case 0: {
                return 100L;
            }
            case 1: {
                return 1000L;
            }
            case 2: {
                return 10000L;
            }
            case 3: {
                return 1000L;
            }
            case 4: {
                return 10000L;
            }
            case 5: {
                return 100000L;
            }
        }
        return 0L;
    }

    protected void sendMessageToTS(BaseMsg msg) {
        for (SlotRoom room : this.rooms.values()) {
            room.sendMessageToRoom(msg);
        }
    }

    public void sendMsgToAllUsers(BaseMsg msg) {
        SendMsgToAlLUsersThread t = new SendMsgToAlLUsersThread(msg);
        t.start();
    }

    protected final class X2Task
            implements Runnable {
        protected X2Task() {
        }

        @Override
        public void run() {
            SlotModule.this.startX2();
            SlotRoom room100 = SlotModule.this.rooms.get(SlotModule.this.gameName + "_vin_100");
            room100.startHuX2();
            SlotRoom room101 = SlotModule.this.rooms.get(SlotModule.this.gameName + "_vin_1000");
            room101.startHuX2();
        }
    }

    protected final class SendMsgToAlLUsersThread
            extends Thread {
        private BaseMsg msg;

        protected SendMsgToAlLUsersThread(BaseMsg msg) {
            this.msg = msg;
        }

        @Override
        public void run() {
            SlotModule.this.sendMessageToTS(this.msg);
        }
    }

    protected final class GameLoopTask
            implements Runnable {
        protected GameLoopTask() {
        }

        @Override
        public void run() {
            SlotModule.this.gameLoop();
        }
    }

    void loadSlotAutoConfig() throws Exception {
        GameConfigServiceImpl gameConfigServiceImpl = new GameConfigServiceImpl();
        ResultGameConfigResponse configObj = gameConfigServiceImpl.getGameConfigAdmin("slot_auto_config", "").get(0);
        if (configObj == null)
            throw new Exception("slot_auto_config gameConfig not found");
        slotAutoConfig = gson.fromJson(configObj.value, SlotAutoConfig.class);
    }

    private SlotAutoConfig getSlotAutoConfig() throws Exception {
        if (slotAutoConfig == null)
            loadSlotAutoConfig();
        return slotAutoConfig;
    }

    abstract SlotRoom getSlotRoomByBet(int betAmount);

    /**
     * Kiểm tra xem thời điểm này có phải là thời điểm nổ jackpot ko?
     *
     * @return
     */
    private boolean isTimeToWinJackpot() throws Exception {
        Calendar calCurrentTime = Calendar.getInstance();
        return calCurrentTime.get(Calendar.HOUR_OF_DAY) == getNextTimeWinJackpot().get(Calendar.HOUR_OF_DAY)
                && calCurrentTime.get(Calendar.MINUTE) == getNextTimeWinJackpot().get(Calendar.MINUTE);
    }

    /**
     * Lấy thời gian trong tương lai mà jackpot sẽ nổ
     *
     * @return
     * @throws Exception
     */
    public Calendar getNextTimeWinJackpot() throws Exception {
        if (calNextTimeWinJackpot == null)
            genNextTimeWinJackpot();
        return calNextTimeWinJackpot;
    }

    /**
     * Tạo random 1 thời điểm trong tương lai sẽ nổ jackpot
     *
     * @throws Exception
     */
    private void genNextTimeWinJackpot() throws Exception {
        Calendar calCurrentTime = Calendar.getInstance();
        int minuteToNextWinJackpot = Util.getRandom(getSlotAutoConfig().getMinMinuteToNextBotWinJackpot(), getSlotAutoConfig().getMaxMinuteToNextBotWinJackpot());
        calCurrentTime.add(Calendar.MINUTE, minuteToNextWinJackpot);
        calCurrentTime.add(Calendar.SECOND, Util.getRandom(1, 59));
        calNextTimeWinJackpot = calCurrentTime;
        KingUtil.printLog("gamename: " + gameName + "genNextTimeWinJackpot() calNextTimeWinJackpot: " + calNextTimeWinJackpot);
    }

    protected void autoFund() {
        try {
            // Load auto config mỗi tiếng 1 lần - vào thời điểm 0phút 0s
            if (LocalTime.now().getMinute() == 0 && LocalTime.now().getSecond() == 0) {
                loadSlotAutoConfig();
            }

            // Tăng tiền hũ 100
            SlotAutoConfig config = getSlotAutoConfig();
            final SlotRoom room100 = getSlotRoomByBet(100);
            room100.increasePot(Util.getRandom(config.getSlot100().getMinIncreasePerSecond(), config.getSlot100().getMaxIncreasePerSecond()));
            room100.savePot();
            final SlotRoom room1k = getSlotRoomByBet(1000);
            room1k.increasePot(Util.getRandom(config.getSlot1000().getMinIncreasePerSecond(), config.getSlot1000().getMaxIncreasePerSecond()));
            room1k.savePot();
            final SlotRoom room10k = getSlotRoomByBet(10000);
            room10k.increasePot(Util.getRandom(config.getSlot10000().getMinIncreasePerSecond(), config.getSlot10000().getMaxIncreasePerSecond()));
            room10k.savePot();

            // Xu li auto no hu
            if (isTimeToWinJackpot()) {
                KingUtil.printLog("Winjackpot... gamename: " + gameName);
                // Xử lí nổ hũ
                // Chọn room sẽ nổ hũ
                Random r = new Random();
                int randomVal = r.nextInt(100);
                if (randomVal < 4) {
                    // 3% Nổ hũ 10k
                    room10k.playWinJackpot(BotMinigame.getBots(1, "vin").get(0), 10000L, getNewReferenceId());
                } else if (randomVal < 16) {
                    // 25% Nổ hũ 1k
                    room1k.playWinJackpot(BotMinigame.getBots(1, "vin").get(0), 1000L, getNewReferenceId());
                } else {
                    // Còn lại nổ hũ 100
                    room100.playWinJackpot(BotMinigame.getBots(1, "vin").get(0), 100L, getNewReferenceId());
                }
                // Khởi tạo lịch nổ hũ lần tiếp theo
                genNextTimeWinJackpot();
            }
        } catch (Exception e) {
            KingUtil.printException(gameName + " exception ", e);
            GU.sendOperation(gameName + " exception: " + KingUtil.printException(e));
        }
    }
}

