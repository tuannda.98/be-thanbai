/*
 * Decompiled with CFR 0.144.
 *
 * Could not load the following classes:
 *  bitzero.server.BitZeroServer
 *  bitzero.server.entities.User
 *  bitzero.server.extensions.BaseClientRequestHandler
 *  bitzero.server.extensions.data.BaseMsg
 *  bitzero.server.extensions.data.DataCmd
 *  bitzero.server.util.TaskScheduler
 *  bitzero.util.common.business.Debug
 *  com.vinplay.dal.service.impl.CacheServiceImpl
 *  com.vinplay.vbee.common.enums.Games
 *  org.json.simple.JSONObject
 */
package game.modules.slot;

import bitzero.server.BitZeroServer;
import bitzero.server.entities.User;
import bitzero.server.extensions.BaseClientRequestHandler;
import bitzero.server.extensions.data.DataCmd;
import bitzero.util.common.business.Debug;
import com.vinplay.dal.service.impl.CacheServiceImpl;
import com.vinplay.vbee.common.enums.Games;
import game.modules.slot.cmd.send.hall.ListAutoPlayInfoMsg;
import game.modules.slot.cmd.send.hall.UpdateJackpotsMsg;
import org.json.simple.JSONObject;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class HallSlotModule
        extends BaseClientRequestHandler {
    private final Set<User> usersSub = new HashSet<>();
    private final Runnable updateJackpotsTask = new UpdateJackpotsTask();

    public HallSlotModule() {
        BitZeroServer.getInstance().getTaskScheduler().scheduleAtFixedRate(this.updateJackpotsTask, 10, 4, TimeUnit.SECONDS);
    }

    public void handleClientRequest(User user, DataCmd dataCmd) {
        switch (dataCmd.getId()) {
            case 10001: {
                this.subScribe(user, dataCmd);
                break;
            }
            case 10002: {
                this.unSubscribe(user, dataCmd);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected void subScribe(User user, DataCmd dataCmd) {
        synchronized (this.usersSub) {
            this.usersSub.add(user);
        }

        UpdateJackpotsMsg msg = new UpdateJackpotsMsg();
        msg.json = this.buildJsonJackpots();
        this.send(msg, user);

        ListAutoPlayInfoMsg listAutoMsg = new ListAutoPlayInfoMsg();
        if (user.getProperty(("auto_" + Games.KHO_BAU.getName())) != null) {
            listAutoMsg.autoKhoBau = ((Boolean) user.getProperty(("auto_" + Games.KHO_BAU.getName()))).booleanValue();
        }

        if (user.getProperty(("auto_" + Games.NU_DIEP_VIEN.getName())) != null) {
            listAutoMsg.autoNDV = ((Boolean) user.getProperty(("auto_" + Games.NU_DIEP_VIEN.getName()))).booleanValue();
        }

        if (user.getProperty(("auto_" + Games.AVENGERS.getName())) != null) {
            listAutoMsg.autoAvenger = ((Boolean) user.getProperty(("auto_" + Games.AVENGERS.getName()))).booleanValue();
        }

        if (user.getProperty(("auto_" + Games.VUONG_QUOC_VIN.getName())) != null) {
            listAutoMsg.autoVQV = ((Boolean) user.getProperty(("auto_" + Games.VUONG_QUOC_VIN.getName()))).booleanValue();
        }

        this.send(listAutoMsg, user);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected void unSubscribe(User user, DataCmd dataCmd) {
        synchronized (this.usersSub) {
            this.usersSub.remove(user);
        }
    }

    private String buildJsonJackpots() {
        JSONObject json = new JSONObject();
        JSONObject jsonKhoBau = this.buildGameSlotInfo(Games.KHO_BAU.getName());
        json.put("kb", jsonKhoBau);
        JSONObject jsonNDV = this.buildGameSlotInfo(Games.NU_DIEP_VIEN.getName());
        json.put("ndv", jsonNDV);
        JSONObject jsonAvenger = this.buildGameSlotInfo(Games.AVENGERS.getName());
        json.put("sah", jsonAvenger);
        JSONObject jsonVQV = this.buildGameSlotInfo(Games.VUONG_QUOC_VIN.getName());
        json.put("vqv", jsonVQV);
        final JSONObject jsonFootBall = this.buildGameSlotInfo(Games.POKE_GO.getName());
        json.put("football", jsonFootBall);
        final JSONObject jsonCaoThap = new JSONObject();
        JSONObject caothap_room1000 = this.buildRoomSlotInfo("cao_thap", 1000);
        jsonCaoThap.put("1000", caothap_room1000);
        JSONObject caothap_room10000 = this.buildRoomSlotInfo("cao_thap", 10000);
        jsonCaoThap.put("10000", caothap_room10000);
        JSONObject caothap_room50000 = this.buildRoomSlotInfo("cao_thap", 50000);
        jsonCaoThap.put("50000", caothap_room50000);
        JSONObject caothap_room100000 = this.buildRoomSlotInfo("cao_thap", 100000);
        jsonCaoThap.put("100000", caothap_room100000);
        JSONObject caothap_room500000 = this.buildRoomSlotInfo("cao_thap", 500000);
        jsonCaoThap.put("500000", caothap_room500000);
        json.put("caothap", jsonCaoThap);
        return json.toJSONString();
    }

    private JSONObject buildGameSlotInfo(String gameName) {
        JSONObject jsonGame = new JSONObject();
        try {
            JSONObject room100 = this.buildRoomSlotInfo(gameName, 100);
            jsonGame.put("100", room100);
            JSONObject room101 = this.buildRoomSlotInfo(gameName, 1000);
            jsonGame.put("1000", room101);
            JSONObject room102 = this.buildRoomSlotInfo(gameName, 10000);
            jsonGame.put("10000", room102);
        } catch (Exception e) {
            Debug.trace(("Hall Slot get jackpots " + gameName + " error: " + e.getMessage()));
        }
        return jsonGame;
    }

    private JSONObject buildRoomSlotInfo(String gameName, int room) {
        CacheServiceImpl cacheService = new CacheServiceImpl();
        JSONObject jsonValue = new JSONObject();
        try {
            int pot = cacheService.getValueInt(gameName + "_vin_" + room);
            jsonValue.put("p", pot);
            int x2 = cacheService.getValueInt(gameName + "_vin_" + room + "_x2");
            jsonValue.put("x2", x2);
        } catch (Exception e) {
            Debug.trace(("Hall Slot get jackpots " + gameName + " - " + room + " error: " + e.getMessage()));
        }
        return jsonValue;
    }

    private class UpdateJackpotsTask
            implements Runnable {
        private UpdateJackpotsTask() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            try {
                UpdateJackpotsMsg msg = new UpdateJackpotsMsg();
                msg.json = buildJsonJackpots();

                for (User user : new HashSet<>(HallSlotModule.this.usersSub)) {
                    if (user == null) continue;

                    send(msg, user);
                }

            } catch (Exception e) {
                Debug.trace(("Update slot exception: " + e.getMessage()));
            }
        }
    }
}
