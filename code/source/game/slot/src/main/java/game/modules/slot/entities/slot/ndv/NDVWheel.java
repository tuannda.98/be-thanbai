// 
// Decompiled by Procyon v0.5.36
// 

package game.modules.slot.entities.slot.ndv;

import java.util.Random;
import java.util.ArrayList;
import java.util.List;

public class NDVWheel
{
    private List<NDVItem> items;
    
    public NDVWheel() {
        this.items = new ArrayList<NDVItem>();
    }
    
    public void addItem(final NDVItem item) {
        this.items.add(item);
    }
    
    public NDVItem random() {
        final Random rd = new Random();
        final int n = rd.nextInt(this.items.size());
        return this.items.remove(n);
    }
    
    public void remove(final int n) {
        if (n >= 0 && n < this.items.size()) {
            this.items.remove(n);
        }
    }
}
