/*
 * Decompiled with CFR 0.144.
 *
 * Could not load the following classes:
 *  bitzero.server.BitZeroServer
 *  bitzero.server.entities.User
 *  bitzero.server.extensions.data.BaseMsg
 *  bitzero.server.util.TaskScheduler
 *  bitzero.util.common.business.Debug
 *  com.vinplay.dal.service.BroadcastMessageService
 *  com.vinplay.dal.service.MiniGameService
 *  com.vinplay.dal.service.SlotMachineService
 *  com.vinplay.dal.service.impl.BroadcastMessageServiceImpl
 *  com.vinplay.dal.service.impl.CacheServiceImpl
 *  com.vinplay.usercore.service.UserService
 *  com.vinplay.vbee.common.enums.Games
 *  com.vinplay.vbee.common.models.UserModel
 *  com.vinplay.vbee.common.models.cache.SlotFreeDaily
 *  com.vinplay.vbee.common.models.cache.UserCacheModel
 *  com.vinplay.vbee.common.response.MoneyResponse
 *  com.vinplay.vbee.common.statics.TransType
 *  com.vinplay.vbee.common.utils.CommonUtils
 *  com.vinplay.vbee.common.utils.DateTimeUtils
 */
package game.modules.slot.room;

import bitzero.server.BitZeroServer;
import bitzero.server.entities.User;
import bitzero.util.common.business.Debug;
import casio.king365.util.KingUtil;
import com.vinplay.dal.service.impl.BroadcastMessageServiceImpl;
import com.vinplay.dal.service.impl.CacheServiceImpl;
import com.vinplay.vbee.common.enums.Games;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.models.cache.SlotFreeDaily;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.response.MoneyResponse;
import com.vinplay.vbee.common.statics.TransType;
import com.vinplay.vbee.common.utils.CommonUtils;
import com.vinplay.vbee.common.utils.DateTimeUtils;
import game.modules.slot.KhoBauModule;
import game.modules.slot.cmd.send.khobau.*;
import game.modules.slot.entities.slot.*;
import game.modules.slot.entities.slot.khobau.AwardsOnLine;
import game.modules.slot.entities.slot.khobau.Line;
import game.modules.slot.entities.slot.khobau.*;
import game.modules.slot.utils.KhoBauUtils;
import game.modules.slot.utils.SlotUtils;
import game.util.ConfigGame;
import game.util.GameUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class KhoBauRoom
        extends SlotRoom {
    private final Runnable gameLoopTask = new GameLoopTask();
    //    private final Runnable gameLoopTask = new GameLoopTask(this);
    private KhoBauLines lines = new KhoBauLines();
    private long lastTimeUpdatePotToRoom = 0L;
    private long lastTimeUpdateFundToRoom = 0L;
    private ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);
    private List<Integer> boxValues = new ArrayList<Integer>();

    public KhoBauRoom(KhoBauModule module, byte id, String name, short moneyType, long pot, long fund, int betValue, long initPotValue) {
        super(id, name, betValue, moneyType, pot, fund, initPotValue);
        this.gameName = Games.KHO_BAU.getName();
        this.module = module;
        this.moneyTypeStr = this.moneyType == 1 ? "vin" : "xu";
        CacheServiceImpl cacheService = new CacheServiceImpl();
        cacheService.setValue(name, (int) pot);
        this.betValue = betValue;
        this.initPotValue = initPotValue;
        BitZeroServer.getInstance().getTaskScheduler().scheduleAtFixedRate(this.gameLoopTask, 10, 1, TimeUnit.SECONDS);
        this.boxValues.add(10);
        this.boxValues.add(10);
        this.boxValues.add(10);
        this.boxValues.add(15);
        this.boxValues.add(20);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void forceStopAutoPlay(User user) {
        super.forceStopAutoPlay(user);
        synchronized (this.usersAuto) {
            this.usersAuto.remove(user.getName());
            ForceStopAutoPlayPokeGoMsg msg = new ForceStopAutoPlayPokeGoMsg();
            SlotUtils.sendMessageToUser(msg, user);
        }
    }

    public ResultSlotMsg play(String username, String linesStr) {
        long startTime = System.currentTimeMillis();
        String currentTimeStr = DateTimeUtils.getCurrentTime();
        long referenceId = this.module.getNewReferenceId();
        short result = 0;
        String[] lineArr = linesStr.split(",");
        long currentMoney = this.userService.getMoneyUserCache(username, this.moneyTypeStr);
        UserCacheModel u = this.userService.forceGetCachedUser(username);
        long totalBetValue = lineArr.length * this.betValue;
        ResultSlotMsg msg = new ResultSlotMsg();
        if (lineArr.length > 0 && !linesStr.isEmpty()) {
            if (totalBetValue > 0L) {
                long minVin = this.userService.getAvailableVinMoney(username);
                if (totalBetValue <= minVin) {
                    long fee = totalBetValue * 2L / 100L;
                    MoneyResponse moneyRes = this.userService.updateMoney(username, -totalBetValue, this.moneyTypeStr, Games.KHO_BAU.getName(), "Quay Kho B\u00e1u", "\u0110\u1eb7t c\u01b0\u1ee3c Kho B\u00e1u", fee, Long.valueOf(referenceId), TransType.START_TRANS);
                    if (moneyRes != null && moneyRes.isSuccess()) {
                        long moneyToPot = totalBetValue * 1L / 100L;
                        long moneyToFund = totalBetValue - fee - moneyToPot;
                        this.fund += moneyToFund;
                        this.pot += moneyToPot;
                        boolean enoughPair = false;
                        ArrayList<AwardsOnLine> awardsOnLines = new ArrayList<AwardsOnLine>();
                        long totalPrizes = 0L;
                        long soTienNoHuKhongTruQuy = 0L;
                        long tienThuongX2 = 0L;
                        block4:
                        while (!enoughPair) {
                            int n;
                            Random rd;
                            int soLanNoHu;
                            result = 0;
                            awardsOnLines.clear();
                            totalPrizes = 0L;
                            soTienNoHuKhongTruQuy = 0L;
                            tienThuongX2 = 0L;
                            String linesWin = "";
                            String prizesOnLine = "";
                            String haiSao = "";
                            boolean forceNoHu = false;
//                            if (lineArr.length >= 5 && (soLanNoHu = ConfigGame.getIntValue("KhoBau_so_lan_no_hu")) > 0 && this.fund > this.initPotValue * 2L && (n = (rd = new Random()).nextInt(soLanNoHu)) == 0) {
//                                forceNoHu = true;
//                            }
                            if (betValue == 100) {
                                if (lineArr.length >= 5 && (soLanNoHu = ConfigGame.getIntValue(this.gameName + "_so_lan_no_hu_100")) > 0 && this.fund > this.initPotValue * 2L && (n = (rd = new Random()).nextInt(soLanNoHu)) == 0) {
                                    forceNoHu = true;
                                }
                            } else if (betValue == 1000) {
                                if (lineArr.length >= 5 && (soLanNoHu = ConfigGame.getIntValue(this.gameName + "_so_lan_no_hu_1000")) > 0 && this.fund > this.initPotValue * 2L && (n = (rd = new Random()).nextInt(soLanNoHu)) == 0) {
                                    forceNoHu = true;
                                }
                            } else {
                                if (lineArr.length >= 5 && (soLanNoHu = ConfigGame.getIntValue(this.gameName + "_so_lan_no_hu_10000")) > 0 && this.fund > this.initPotValue * 2L && (n = (rd = new Random()).nextInt(soLanNoHu)) == 0) {
                                    forceNoHu = true;
                                }
                            }
                            KhoBauItem[][] matrix = forceNoHu ? KhoBauUtils.generateMatrixNoHu(lineArr) : KhoBauUtils.generateMatrix();
                            for (String entry2 : lineArr) {
                                ArrayList<KhoBauAward> awardList = new ArrayList<KhoBauAward>();
                                Line line = KhoBauUtils.getLine(this.lines, matrix, Integer.parseInt(entry2));
                                KhoBauUtils.calculateLine(line, awardList);
                                for (KhoBauAward award : awardList) {
                                    long moneyOnLine = 0L;
                                    if (award.getRatio() > 0.0f) {
                                        moneyOnLine = (long) (award.getRatio() * (float) this.betValue);
                                    } else if (award == KhoBauAward.PENTA_POUCH) {
                                        for (AwardsOnLine e : awardsOnLines) {
                                            if (e.getAward() != KhoBauAward.PENTA_POUCH) continue;
                                            continue block4;
                                        }
                                        if (this.huX2) {
                                            moneyOnLine = this.pot * 2L;
                                            tienThuongX2 = this.pot;
                                            soTienNoHuKhongTruQuy += this.pot;
                                        } else {
                                            moneyOnLine = this.pot;
                                        }
                                        result = 3;
                                        soTienNoHuKhongTruQuy += this.pot - this.initPotValue;
                                    } else {
                                        MiniGameSlotResponse response = this.generatePickStars();
                                        moneyOnLine = response.getTotalPrize();
                                        haiSao = response.getPrizes();
                                        if (result != 3) {
                                            result = 5;
                                        }
                                    }
                                    AwardsOnLine aol = new AwardsOnLine(award, moneyOnLine, line.getName());
                                    awardsOnLines.add(aol);
                                }
                            }
                            StringBuilder builderLinesWin = new StringBuilder();
                            StringBuilder builderPrizesOnLine = new StringBuilder();
                            for (AwardsOnLine entry2 : awardsOnLines) {
//                                if ((entry2.getAward() == KhoBauAward.PENTA_POUCH || entry2.getAward() == KhoBauAward.QUADRA_POUCH || entry2.getAward() == KhoBauAward.TRIPLE_POUCH) && !u.isBot()) continue block4;
                                if (betValue == 100) {
                                    if (ConfigGame.getIntValue(this.gameName + "_cho_bot_no_hu_100") == 0) // cho cả người và bot nổ hũ
                                    {
                                        if ((entry2.getAward() == KhoBauAward.PENTA_POUCH || entry2.getAward() == KhoBauAward.QUADRA_POUCH || entry2.getAward() == KhoBauAward.TRIPLE_POUCH))
                                            continue block4;
                                    } else if (ConfigGame.getIntValue(this.gameName + "_cho_bot_no_hu_100") == 1) // chỉ cho bot nổ hũ
                                    {
                                        if ((entry2.getAward() == KhoBauAward.PENTA_POUCH || entry2.getAward() == KhoBauAward.QUADRA_POUCH || entry2.getAward() == KhoBauAward.TRIPLE_POUCH) && !u.isBot())
                                            continue block4;
                                    } else if (ConfigGame.getIntValue(this.gameName + "_cho_bot_no_hu_100") == -1) // chỉ cho người nổ hũ
                                    {
                                        if ((entry2.getAward() == KhoBauAward.PENTA_POUCH || entry2.getAward() == KhoBauAward.QUADRA_POUCH || entry2.getAward() == KhoBauAward.TRIPLE_POUCH) && u.isBot())
                                            continue block4;
                                    } else {
                                        if ((entry2.getAward() == KhoBauAward.PENTA_POUCH || entry2.getAward() == KhoBauAward.QUADRA_POUCH || entry2.getAward() == KhoBauAward.TRIPLE_POUCH) && !u.isBot())
                                            continue block4;
                                    }
                                } else if (betValue == 1000) {
                                    if (ConfigGame.getIntValue(this.gameName + "_cho_bot_no_hu_1000") == 0) // cho cả người và bot nổ hũ
                                    {
                                        if ((entry2.getAward() == KhoBauAward.PENTA_POUCH || entry2.getAward() == KhoBauAward.QUADRA_POUCH || entry2.getAward() == KhoBauAward.TRIPLE_POUCH))
                                            continue block4;
                                    } else if (ConfigGame.getIntValue(this.gameName + "_cho_bot_no_hu_1000") == 1) // chỉ cho bot nổ hũ
                                    {
                                        if ((entry2.getAward() == KhoBauAward.PENTA_POUCH || entry2.getAward() == KhoBauAward.QUADRA_POUCH || entry2.getAward() == KhoBauAward.TRIPLE_POUCH) && !u.isBot())
                                            continue block4;
                                    } else if (ConfigGame.getIntValue(this.gameName + "_cho_bot_no_hu_1000") == -1) // chỉ cho người nổ hũ
                                    {
                                        if ((entry2.getAward() == KhoBauAward.PENTA_POUCH || entry2.getAward() == KhoBauAward.QUADRA_POUCH || entry2.getAward() == KhoBauAward.TRIPLE_POUCH) && u.isBot())
                                            continue block4;
                                    } else {
                                        if ((entry2.getAward() == KhoBauAward.PENTA_POUCH || entry2.getAward() == KhoBauAward.QUADRA_POUCH || entry2.getAward() == KhoBauAward.TRIPLE_POUCH) && !u.isBot())
                                            continue block4;
                                    }
                                } else {
                                    if (ConfigGame.getIntValue(this.gameName + "_cho_bot_no_hu_10000") == 0) // cho cả người và bot nổ hũ
                                    {
                                        if ((entry2.getAward() == KhoBauAward.PENTA_POUCH || entry2.getAward() == KhoBauAward.QUADRA_POUCH || entry2.getAward() == KhoBauAward.TRIPLE_POUCH))
                                            continue block4;
                                    } else if (ConfigGame.getIntValue(this.gameName + "_cho_bot_no_hu_10000") == 1) // chỉ cho bot nổ hũ
                                    {
                                        if ((entry2.getAward() == KhoBauAward.PENTA_POUCH || entry2.getAward() == KhoBauAward.QUADRA_POUCH || entry2.getAward() == KhoBauAward.TRIPLE_POUCH) && !u.isBot())
                                            continue block4;
                                    } else if (ConfigGame.getIntValue(this.gameName + "_cho_bot_no_hu_10000") == -1) // chỉ cho người nổ hũ
                                    {
                                        if ((entry2.getAward() == KhoBauAward.PENTA_POUCH || entry2.getAward() == KhoBauAward.QUADRA_POUCH || entry2.getAward() == KhoBauAward.TRIPLE_POUCH) && u.isBot())
                                            continue block4;
                                    } else {
                                        if ((entry2.getAward() == KhoBauAward.PENTA_POUCH || entry2.getAward() == KhoBauAward.QUADRA_POUCH || entry2.getAward() == KhoBauAward.TRIPLE_POUCH) && !u.isBot())
                                            continue block4;
                                    }
                                }
                                totalPrizes += entry2.getMoney();
                                builderLinesWin.append(",");
                                builderLinesWin.append(entry2.getLineId());
                                builderPrizesOnLine.append(",");
                                builderPrizesOnLine.append(entry2.getMoney());
                            }
                            if (builderLinesWin.length() > 0) {
                                builderLinesWin.deleteCharAt(0);
                            }
                            if (builderPrizesOnLine.length() > 0) {
                                builderPrizesOnLine.deleteCharAt(0);
                            }
                            if (result == 3 ? this.fund - (totalPrizes - soTienNoHuKhongTruQuy) < 0L : this.fund - totalPrizes < this.initPotValue * 2L && totalPrizes - totalBetValue >= 0L)
                                continue;
                            enoughPair = true;
                            String matrixStr = KhoBauUtils.matrixToString(matrix);
                            if (totalPrizes > 0L) {
                                if (result == 3) {
                                    if (this.huX2) {
                                        result = 4;
                                    }
                                    this.noHuX2();
                                    this.pot = this.initPotValue;
                                    this.fund -= totalPrizes - soTienNoHuKhongTruQuy;
                                    if (this.moneyType == 1) {
                                        GameUtils.sendSMSToUser(username, "Chuc mung " + username + " da no hu game Kho Bau phong " + this.betValue + ". So tien no hu: " + totalPrizes + " vin");
                                    }
                                    this.slotService.logNoHu(referenceId, this.gameName, username, this.betValue, linesStr, matrixStr, builderLinesWin.toString(), builderPrizesOnLine.toString(), totalPrizes, result, currentTimeStr);
                                } else {
                                    this.fund -= totalPrizes;
                                    if (result == 0) {
                                        result = totalPrizes >= (long) (this.betValue * 100) ? (short) 2 : 1;
                                    }
                                }
                            }
                            long moneyExchange = totalPrizes - tienThuongX2;
                            String des = "Quay Kho B\u00e1u ";
                            if (tienThuongX2 > 0L) {
                                this.userService.updateMoney(username, tienThuongX2, this.moneyTypeStr, Games.KHO_BAU.getName(), "Quay Kho B\u00e1u ", "Th\u01b0\u1edfng h\u0169 X2", 0L, null, TransType.NO_VIPPOINT);
                            }
                            if ((moneyRes = this.userService.updateMoney(username, totalPrizes, this.moneyTypeStr, Games.KHO_BAU.getName(), "Quay Kho B\u00e1u ", this.buildDescription(totalBetValue, totalPrizes, result), 0L, Long.valueOf(referenceId), TransType.END_TRANS)) != null && moneyRes.isSuccess()) {
                                currentMoney = moneyRes.getCurrentMoney();
                                if (this.moneyType == 1 && moneyExchange >= (long) BroadcastMessageServiceImpl.MIN_MONEY) {
                                    this.broadcastMsgService.putMessage(Games.KHO_BAU.getId(), username, moneyExchange - totalBetValue);
                                }
                            }
                            linesWin = builderLinesWin.toString();
                            prizesOnLine = builderPrizesOnLine.toString();
                            msg.referenceId = referenceId;
                            msg.matrix = KhoBauUtils.matrixToString(matrix);
                            msg.linesWin = linesWin;
                            msg.prize = totalPrizes;
                            msg.haiSao = haiSao;
                            try {
                                this.slotService.logKhoBau(referenceId, username, this.betValue, linesStr, linesWin, prizesOnLine, result, totalPrizes, currentTimeStr);
                                if (result == 3 || result == 4) {
                                    this.slotService.addTop(Games.KHO_BAU.getName(), username, this.betValue, totalPrizes, currentTimeStr, result);
                                }
                                if (result == 3 || result == 2 || result == 4) {
                                    BigWinKhoBauMsg bigWinMsg = new BigWinKhoBauMsg();
                                    bigWinMsg.username = username;
                                    bigWinMsg.type = (byte) result;
                                    bigWinMsg.betValue = (short) this.betValue;
                                    bigWinMsg.totalPrizes = totalPrizes;
                                    bigWinMsg.timestamp = DateTimeUtils.getCurrentTime();
                                    this.module.sendMsgToAllUsers(bigWinMsg);
                                }
                            } catch (InterruptedException bigWinMsg) {
                            } catch (TimeoutException bigWinMsg) {
                            } catch (IOException bigWinMsg) {
                                // empty catch block
                            }
                            this.saveFund();
                            this.savePot();
                        }
                    } else {
                        result = 102;
                    }
                } else {
                    result = 102;
                }
            } else {
                result = 101;
            }
        } else {
            result = 101;
        }
        msg.result = (byte) result;
        msg.currentMoney = currentMoney;
        long endTime = System.currentTimeMillis();
        long handleTime = endTime - startTime;
        String ratioTime = CommonUtils.getRatioTime(handleTime);
        SlotUtils.logKhoBau(referenceId, username, this.betValue, msg.matrix, msg.haiSao, result, handleTime, ratioTime, currentTimeStr);
        return msg;
    }

    public ResultSlotMsg playFreeDaily(String username) {
        long startTime = System.currentTimeMillis();
        String currentTimeStr = DateTimeUtils.getCurrentTime();
        long refernceId = this.module.getNewReferenceId();
        short result = 0;
        String linesStr = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20";
        String[] lineArr = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20".split(",");
        long currentMoney = this.userService.getMoneyUserCache(username, this.moneyTypeStr);
        ResultSlotMsg msg = new ResultSlotMsg();
        boolean enoughPair = false;
        ArrayList<AwardsOnLine> awardsOnLines = new ArrayList<AwardsOnLine>();
        long totalPrizes = 0L;
        block4:
        while (!enoughPair) {
            result = 0;
            awardsOnLines.clear();
            totalPrizes = 0L;
            String linesWin = "";
            String prizesOnLine = "";
            String haiSao = "";
            KhoBauItem[][] matrix = KhoBauUtils.generateMatrix();
            for (String entry2 : lineArr) {
                ArrayList<KhoBauAward> awardList = new ArrayList<KhoBauAward>();
                Line line = KhoBauUtils.getLine(this.lines, matrix, Integer.parseInt(entry2));
                KhoBauUtils.calculateLine(line, awardList);
                for (KhoBauAward award : awardList) {
                    long money = 0L;
                    if (award.getRatio() <= 0.0f) continue block4;
                    money = (long) (award.getRatio() * (float) this.betValue);
                    AwardsOnLine aol = new AwardsOnLine(award, money, line.getName());
                    awardsOnLines.add(aol);
                }
            }
            StringBuilder builderLinesWin = new StringBuilder();
            StringBuilder builderPrizesOnLine = new StringBuilder();
            for (AwardsOnLine entry2 : awardsOnLines) {
                totalPrizes += entry2.getMoney();
                builderLinesWin.append(",");
                builderLinesWin.append(entry2.getLineId());
                builderPrizesOnLine.append(",");
                builderPrizesOnLine.append(entry2.getMoney());
            }
            if (builderLinesWin.length() > 0) {
                builderLinesWin.deleteCharAt(0);
            }
            if (builderPrizesOnLine.length() > 0) {
                builderPrizesOnLine.deleteCharAt(0);
            }
            if (this.fund - totalPrizes < 0L || totalPrizes > (long) ConfigGame.getIntValue("max_prize_free_daily", 2000))
                continue;
            enoughPair = true;
            boolean updated = this.slotService.updateLuotQuayFreeDaily(this.gameName, username, this.betValue);
            if (!updated) {
                result = 103;
            } else {
                long moneyExchange = totalPrizes;
                if (moneyExchange > 0L) {
                    String des = "Quay Kho B\u00e1u Free";
                    MoneyResponse moneyRes = this.userService.updateMoney(username, totalPrizes, this.moneyTypeStr, "KhoBauVqFree", "Quay Kho B\u00e1u Free", "C\u01b0\u1ee3c: 0, Th\u1eafng: " + totalPrizes, 0L, null, TransType.NO_VIPPOINT);
                    if (moneyRes != null && moneyRes.isSuccess()) {
                        currentMoney = moneyRes.getCurrentMoney();
                    }
                }
            }
            linesWin = builderLinesWin.toString();
            prizesOnLine = builderPrizesOnLine.toString();
            msg.referenceId = refernceId;
            msg.matrix = KhoBauUtils.matrixToString(matrix);
            msg.linesWin = linesWin;
            msg.prize = totalPrizes;
            msg.haiSao = "";
            try {
                this.slotService.logKhoBau(refernceId, username, this.betValue, "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20", linesWin, prizesOnLine, result, totalPrizes, currentTimeStr);
            } catch (InterruptedException moneyExchange) {
            } catch (TimeoutException moneyExchange) {
            } catch (IOException moneyExchange) {
            }
        }
        msg.result = (byte) result;
        msg.currentMoney = currentMoney;
        long endTime = System.currentTimeMillis();
        long handleTime = endTime - startTime;
        String ratioTime = CommonUtils.getRatioTime(handleTime);
        SlotUtils.logKhoBau(refernceId, username, this.betValue, msg.matrix, msg.haiSao, result, handleTime, ratioTime, currentTimeStr);
        return msg;
    }

    private MiniGameSlotResponse generatePickStars() {
        MiniGameSlotResponse response = new MiniGameSlotResponse();
        int totalMoney = 0;
        ArrayList<PickStarGift> gifts = new ArrayList<PickStarGift>();
        PickStarGifts pickStarGifts = new PickStarGifts();
        String responsePickStars = "";
        int totalKeys = 1;
        block5:
        for (int numPicks = 10; numPicks > 0; --numPicks) {
            PickStarGiftItem gift = pickStarGifts.pickRandomAndRandomGift();
            switch (gift) {
                case GOLD: {
                    totalMoney += 4 * this.betValue * totalKeys;
                    gifts.add(new PickStarGift(PickStarGiftItem.GOLD, 0));
                    continue block5;
                }
                case KEY: {
                    ++numPicks;
                    ++totalKeys;
                    gifts.add(new PickStarGift(PickStarGiftItem.KEY, 0));
                    continue block5;
                }
                case BOX: {
                    int boxValue = this.randomBoxValue();
                    totalMoney += boxValue * this.betValue * totalKeys;
                    gifts.add(new PickStarGift(PickStarGiftItem.BOX, boxValue));
                    break;
                }
            }
        }
        for (PickStarGift pickStarGift : gifts) {
            if (responsePickStars.length() == 0) {
                responsePickStars = pickStarGift.getName();
                continue;
            }
            responsePickStars = responsePickStars + "," + pickStarGift.getName();
        }
        response.setTotalPrize(totalMoney);
        response.setPrizes(responsePickStars);
        return response;
    }

    private int randomBoxValue() {
        Random rd = new Random();
        int n = rd.nextInt(this.boxValues.size());
        return this.boxValues.get(n);
    }

    public short play(User user, String linesStr) {
        String username = user.getName();
        int numFree = 0;
        if (user.getProperty("numFreeDaily") != null) {
            numFree = (Integer) user.getProperty("numFreeDaily");
        }
        ResultSlotMsg msg = null;
        KhoBauFreeDailyMsg freeDailyMsg = new KhoBauFreeDailyMsg();
        if (numFree > 0) {
            msg = this.playFreeDaily(username);
            freeDailyMsg.remain = (byte) (--numFree);
            if (numFree > 0) {
                user.setProperty("numFreeDaily", numFree);
            } else {
                user.removeProperty("numFreeDaily");
            }
        } else {
            msg = this.play(username, linesStr);
        }
        if (this.isUserMinimize(user)) {
            MinimizeResultKhoBauMsg miniMsg = new MinimizeResultKhoBauMsg();
            miniMsg.prize = msg.prize;
            miniMsg.curretMoney = msg.currentMoney;
            miniMsg.result = msg.result;
            SlotUtils.sendMessageToUser(miniMsg, user);
        } else {
            SlotUtils.sendMessageToUser(msg, user);
            SlotUtils.sendMessageToUser(freeDailyMsg, user);
        }
        return msg.result;
    }

    private void saveFund() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - this.lastTimeUpdateFundToRoom >= 60000L) {
            try {
                this.mgService.saveFund(this.name, this.fund);
            } catch (IOException | InterruptedException | TimeoutException ex2) {
                Exception ex;
                Exception e = ex = ex2;
                Debug.trace(new Object[]{"KHO BAU: update fund kho bau error ", e.getMessage()});
            }
            this.lastTimeUpdateFundToRoom = currentTime;
        }
    }

    public void savePot() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - this.lastTimeUpdatePotToRoom >= 3000L) {
            this.lastTimeUpdatePotToRoom = currentTime;
            try {
                this.mgService.savePot(this.name, this.pot, this.huX2);
            } catch (IOException | InterruptedException | TimeoutException ex2) {
                Exception ex;
                Exception e = ex = ex2;
                Debug.trace(new Object[]{this.gameName + ": update pot poker error ", e.getMessage()});
            }
            byte x2 = (byte) (this.huX2 ? 1 : 0);
            ((KhoBauModule) this.module).updatePot(this.id, this.pot, x2);
        }
    }

    @Override
    public void playWinJackpot(String username, long betAmount, long referenceId) {
        KingUtil.printLog("KhoBauRoom playWinJackpot() username: " + username + ", betAmount: " + betAmount + ", referenceId: " + referenceId + ", pot: " + this.pot);
        long startTime = System.currentTimeMillis();
        String currentTimeStr = DateTimeUtils.getCurrentTime();
        short result = 0;
        StringBuilder linesStrBuilder = new StringBuilder();
        String linesStr = "";
        for (int i = 1; i <= 19; i++)
            linesStrBuilder.append(i + ",");
        linesStrBuilder.append(20);
        linesStr = linesStrBuilder.toString();
        String[] lineArr = linesStr.split(",");
        long currentMoney = this.userService.getMoneyUserCache(username, this.moneyTypeStr);
        UserCacheModel u = this.userService.forceGetCachedUser(username);
        long totalBetValue = lineArr.length * this.betValue;
        ResultSlotMsg msg = new ResultSlotMsg();
        long fee = totalBetValue * 2L / 100L;
        MoneyResponse moneyRes = this.userService.updateMoney(username, -totalBetValue, this.moneyTypeStr, Games.KHO_BAU.getName(), "Quay Kho B\u00e1u", "\u0110\u1eb7t c\u01b0\u1ee3c Kho B\u00e1u", fee, Long.valueOf(referenceId), TransType.START_TRANS);
        if (moneyRes != null && moneyRes.isSuccess()) {
            long moneyToPot = totalBetValue * 1L / 100L;
            long moneyToFund = totalBetValue - fee - moneyToPot;
            // this.fund += moneyToFund;
            this.pot += moneyToPot;
            boolean enoughPair = false;
            ArrayList<AwardsOnLine> awardsOnLines = new ArrayList<AwardsOnLine>();
            long totalPrizes = 0L;
            long soTienNoHuKhongTruQuy = 0L;
            long tienThuongX2 = 0L;
            block4:
            while (!enoughPair) {
                int n;
                Random rd;
                int soLanNoHu;
                result = 0;
                awardsOnLines.clear();
                totalPrizes = 0L;
                soTienNoHuKhongTruQuy = 0L;
                tienThuongX2 = 0L;
                String linesWin = "";
                String prizesOnLine = "";
                String haiSao = "";
                boolean forceNoHu = true;
                KhoBauItem[][] matrix = forceNoHu ? KhoBauUtils.generateMatrixNoHu(lineArr) : KhoBauUtils.generateMatrix();
                for (String entry2 : lineArr) {
                    ArrayList<KhoBauAward> awardList = new ArrayList<KhoBauAward>();
                    Line line = KhoBauUtils.getLine(this.lines, matrix, Integer.parseInt(entry2));
                    KhoBauUtils.calculateLine(line, awardList);
                    for (KhoBauAward award : awardList) {
                        long moneyOnLine = 0L;
                        if (award.getRatio() > 0.0f) {
                            moneyOnLine = (long) (award.getRatio() * (float) this.betValue);
                        } else if (award == KhoBauAward.PENTA_POUCH) {
                            for (AwardsOnLine e : awardsOnLines) {
                                if (e.getAward() != KhoBauAward.PENTA_POUCH) continue;
                                continue block4;
                            }
                            if (this.huX2) {
                                moneyOnLine = this.pot * 2L;
                                tienThuongX2 = this.pot;
                                soTienNoHuKhongTruQuy += this.pot;
                            } else {
                                moneyOnLine = this.pot;
                            }
                            result = 3;
                            soTienNoHuKhongTruQuy += this.pot - this.initPotValue;
                        } else {
                            MiniGameSlotResponse response = this.generatePickStars();
                            moneyOnLine = response.getTotalPrize();
                            haiSao = response.getPrizes();
                            if (result != 3) {
                                result = 5;
                            }
                        }
                        AwardsOnLine aol = new AwardsOnLine(award, moneyOnLine, line.getName());
                        awardsOnLines.add(aol);
                    }
                }
                StringBuilder builderLinesWin = new StringBuilder();
                StringBuilder builderPrizesOnLine = new StringBuilder();
                for (AwardsOnLine entry2 : awardsOnLines) {
                    totalPrizes += entry2.getMoney();
                    builderLinesWin.append(",");
                    builderLinesWin.append(entry2.getLineId());
                    builderPrizesOnLine.append(",");
                    builderPrizesOnLine.append(entry2.getMoney());
                }
                if (builderLinesWin.length() > 0) {
                    builderLinesWin.deleteCharAt(0);
                }
                if (builderPrizesOnLine.length() > 0) {
                    builderPrizesOnLine.deleteCharAt(0);
                }
                enoughPair = true;
                String matrixStr = KhoBauUtils.matrixToString(matrix);
                if (totalPrizes > 0L) {
                    if (result == 3) {
                        if (this.huX2) {
                            result = 4;
                        }
                        this.noHuX2();
                        this.pot = this.initPotValue;
                        // this.fund -= totalPrizes - soTienNoHuKhongTruQuy;
                        if (this.moneyType == 1) {
                            GameUtils.sendSMSToUser(username, "Chuc mung " + username + " da no hu game Kho Bau phong " + this.betValue + ". So tien no hu: " + totalPrizes + " vin");
                        }
                        this.slotService.logNoHu(referenceId, this.gameName, username, this.betValue, linesStr, matrixStr, builderLinesWin.toString(), builderPrizesOnLine.toString(), totalPrizes, result, currentTimeStr);
                    } else {
                        //this.fund -= totalPrizes;
                        if (result == 0) {
                            result = totalPrizes >= (long) (this.betValue * 100) ? (short) 2 : 1;
                        }
                    }
                }
                long moneyExchange = totalPrizes - tienThuongX2;
                String des = "Quay Kho B\u00e1u ";
                if (tienThuongX2 > 0L) {
                    this.userService.updateMoney(username, tienThuongX2, this.moneyTypeStr, Games.KHO_BAU.getName(), "Quay Kho B\u00e1u ", "Th\u01b0\u1edfng h\u0169 X2", 0L, null, TransType.NO_VIPPOINT);
                }
                if ((moneyRes = this.userService.updateMoney(username, totalPrizes, this.moneyTypeStr, Games.KHO_BAU.getName(), "Quay Kho B\u00e1u ", this.buildDescription(totalBetValue, totalPrizes, result), 0L, Long.valueOf(referenceId), TransType.END_TRANS)) != null && moneyRes.isSuccess()) {
                    currentMoney = moneyRes.getCurrentMoney();
                    if (this.moneyType == 1 && moneyExchange >= (long) BroadcastMessageServiceImpl.MIN_MONEY) {
                        this.broadcastMsgService.putMessage(Games.KHO_BAU.getId(), username, moneyExchange - totalBetValue);
                    }
                }
                linesWin = builderLinesWin.toString();
                prizesOnLine = builderPrizesOnLine.toString();
                msg.referenceId = referenceId;
                msg.matrix = KhoBauUtils.matrixToString(matrix);
                msg.linesWin = linesWin;
                msg.prize = totalPrizes;
                msg.haiSao = haiSao;
                try {
                    this.slotService.logKhoBau(referenceId, username, this.betValue, linesStr, linesWin, prizesOnLine, result, totalPrizes, currentTimeStr);
                    if (result == 3 || result == 4) {
                        this.slotService.addTop(Games.KHO_BAU.getName(), username, this.betValue, totalPrizes, currentTimeStr, result);
                    }
                    if (result == 3 || result == 2 || result == 4) {
                        BigWinKhoBauMsg bigWinMsg = new BigWinKhoBauMsg();
                        bigWinMsg.username = username;
                        bigWinMsg.type = (byte) result;
                        bigWinMsg.betValue = (short) this.betValue;
                        bigWinMsg.totalPrizes = totalPrizes;
                        bigWinMsg.timestamp = DateTimeUtils.getCurrentTime();
                        this.module.sendMsgToAllUsers(bigWinMsg);
                    }
                } catch (InterruptedException bigWinMsg) {
                } catch (TimeoutException bigWinMsg) {
                } catch (IOException bigWinMsg) {
                    // empty catch block
                }
                // this.saveFund();
                this.savePot();
            }
        } else {
            result = 102;
        }
        msg.result = (byte) result;
        msg.currentMoney = currentMoney;
        long endTime = System.currentTimeMillis();
        long handleTime = endTime - startTime;
        String ratioTime = CommonUtils.getRatioTime(handleTime);
        SlotUtils.logKhoBau(referenceId, username, this.betValue, msg.matrix, msg.haiSao, result, handleTime, ratioTime, currentTimeStr);
    }

    private boolean checkDieuKienNo(String username) {
        try {
            UserModel u = this.userService.getUserByUserName(username);
            return u.isBot();
        } catch (Exception u) {
            return false;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    protected void gameLoop() {
        ArrayList<AutoUser> usersPlay = new ArrayList<AutoUser>();
        synchronized (this.usersAuto) {
            for (AutoUser user : this.usersAuto.values()) {
                boolean play = user.incCount();
                if (!play) continue;
                usersPlay.add(user);
            }
        }
        int numThreads = usersPlay.size() / 100 + 1;
        for (int i = 1; i <= numThreads; ++i) {
            int fromIndex = (i - 1) * 100;
            int toIndex = i * 100;
            if (toIndex > usersPlay.size()) {
                toIndex = usersPlay.size();
            }
            ArrayList<AutoUser> tmp = new ArrayList<AutoUser>(usersPlay.subList(fromIndex, toIndex));
            PlayListAutoUserTask task = new PlayListAutoUserTask(tmp);
//            PlayListAutoUserTask task = new PlayListAutoUserTask(this, tmp);
            this.executor.execute(task);
        }
        usersPlay.clear();
    }

    @Override
    protected void playListAuto(List<AutoUser> users) {
        for (AutoUser user : users) {
            short result = this.play(user.getUser(), user.getLines());
            if (result == 3 || result == 4 || result == 101 || result == 102 || result == 100) {
                this.forceStopAutoPlay(user.getUser());
                continue;
            }
            if (result == 0) {
                user.setMaxCount(4);
                continue;
            }
            if (result == 5) {
                user.setMaxCount(15);
                continue;
            }
            user.setMaxCount(8);
        }
        users.clear();
    }

    @Override
    public boolean joinRoom(User user) {
        boolean result = super.joinRoom(user);
        SlotFreeDaily model = this.slotService.getLuotQuayFreeDaily(this.gameName, user.getName(), this.betValue);
        if (model != null && model.getRotateFree() > 0) {
            user.setProperty("numFreeDaily", model.getRotateFree());
            KhoBauFreeDailyMsg freeDailyMsg = new KhoBauFreeDailyMsg();
            freeDailyMsg.remain = (byte) model.getRotateFree();
            SlotUtils.sendMessageToUser(freeDailyMsg, user);
        } else {
            user.removeProperty("numFreeDaily");
        }
        if (result) {
            user.setProperty("MGROOM_KHO_BAU_INFO", this);
        }
        return result;
    }
}
