package game.bacay.server;

import bitzero.server.core.BZEvent;
import bitzero.server.entities.User;
import bitzero.server.extensions.data.BaseMsg;
import bitzero.server.extensions.data.DataCmd;
import bitzero.util.ExtensionUtility;
import bitzero.util.common.business.CommonHandle;
import bitzero.util.common.business.Debug;
import casio.king365.util.KingUtil;
import game.bacay.server.cmd.receive.*;
import game.bacay.server.cmd.send.*;
import game.bacay.server.logic.BacayRule;
import game.bacay.server.logic.GroupCard;
import game.entities.PlayerInfo;
import game.entities.UserScore;
import game.eventHandlers.GameEventParam;
import game.eventHandlers.GameEventType;
import game.modules.bot.Bot;
import game.modules.bot.BotManager;
import game.modules.gameRoom.cmd.send.SendNoHu;
import game.modules.gameRoom.entities.*;
import game.utils.GameUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;

import static game.modules.gameRoom.GameRoomModule.GAME_ROOM;
import static game.modules.gameRoom.entities.GameMoneyInfo.GAME_MONEY_INFO;

public class BaCayGameServer
        extends GameServer {
    private final GameManager gameManager = new GameManager(this);
    private final List<GamePlayer> playerList = new ArrayList<>(8);
    private int playingCount = 0;
    private volatile int serverState = 0;
    private volatile int playerCount;

    private StringBuilder gameLog = new StringBuilder();

    private int chuongChair = 0;
    private int newChuongChair = -1;
    private ThongTinThangLon thongTinNoHu = null;

    public BaCayGameServer(GameRoom room) {
        super(room);
        int i = 0;
        while (i < 8) {
            GamePlayer gp = new GamePlayer();
            gp.chair = i++;
            this.playerList.add(gp);
        }
        KingUtil.printLog("Tao moi room, thong tin: "+room.setting.toString());
    }

    @Override
    public void onGameMessage(User user, DataCmd dataCmd) {
        synchronized (this) {
            switch (dataCmd.getId()) {
                case 3101: {
                    this.moBai(user, dataCmd);
                    break;
                }
                case 3106: {
                    this.keCua(user, dataCmd);
                    break;
                }
                case 3104: {
                    this.yeuCauDanhBien(user, dataCmd);
                    break;
                }
                case 3108: {
                    this.dongYDanhBien(user, dataCmd);
                    break;
                }
                case 3109: {
                    this.datCuoc(user, dataCmd);
                    break;
                }
                case 3112: {
                    this.vaoGa(user, dataCmd);
                    break;
                }
                case 3111: {
                    this.pOutRoom(user, dataCmd);
                    break;
                }
                case 3102: {
                    this.pBatDau(user, dataCmd);
                    break;
                }
                case 3115: {
                    this.pCheatCards(user, dataCmd);
                    break;
                }
                case 3116: {
                    this.pDangKyChoiTiep(user, dataCmd);
                }

            }
        }
    }

    @Override
    public void onGameUserExit(User user) {
        synchronized (this) {
            Integer chair = user.getProperty(USER_CHAIR);
            if (chair == null) {
                Debug.trace("User exit chair null", user.getName());
                return;
            }

            GamePlayer gp = this.getPlayerByChair(chair);
            if (gp == null) {
                Debug.trace("User exit GamePlayer null", user.getName());
                return;
            }

            if (gp.isPlaying()) {
                gp.reqQuitRoom = true;
                ++gp.tuDongChoiNhanh;
                this.gameLog.append("DIS<").append(chair).append(">");
            } else {
                this.removePlayerAtChair(chair, !user.isConnected());
                if (this.gameRoom.userManager.size() < 2) {
                    this.gameManager.cancelAutoStart();
                }
            }
        }

        if (this.gameRoom.userManager.size() == 0) {
            this.resetPlayDisconnect();
        }
    }

    @Override
    public void onGameUserReturn(User user) {
        if (user == null) {
            return;
        }

        if (this.gameRoom.setting.maxUserPerRoom != 8) {
            return;
        }

        synchronized (this) {
            for (int i = 0; i < 8; ++i) {
                GamePlayer gp = this.playerList.get(i);
                if (gp.getPlayerStatus() == 0 || gp.pInfo == null || gp.pInfo.userId != user.getId()) continue;

                this.gameLog.append("RE<").append(i).append(">");

                GameMoneyInfo moneyInfo = user.getProperty(GAME_MONEY_INFO);
                if (moneyInfo != null && !gp.gameMoneyInfo.sessionId.equals(moneyInfo.sessionId)) {
                    Debug.trace("onGameUserReturn", user.getName());
                    moneyInfo.restoreMoney(this.gameRoom.getId());
                }

                user.setProperty(USER_CHAIR, gp.chair);
                gp.user = user;
                gp.reqQuitRoom = false;
                user.setProperty(GAME_MONEY_INFO, gp.gameMoneyInfo);
                this.sendGameInfo(gp.chair);
                break;
            }
        }
    }

    @Override
    public void onGameUserDis(User user) {
        synchronized (this) {
            Integer chair = user.getProperty(USER_CHAIR);
            if (chair == null) {
                return;
            }
            GamePlayer gp = this.getPlayerByChair(chair);
            if (gp == null) {
                return;
            }

            if (gp.isPlaying()) {
                gp.reqQuitRoom = true;
                ++gp.tuDongChoiNhanh;
                this.gameLog.append("DIS<").append(chair).append(">");
            } else {
                GameRoomManager.instance().leaveRoom(user, this.gameRoom);
            }
        }
    }

    @Override
    public void onGameUserEnter(User user) {
        if (user == null) {
            return;
        }

        PlayerInfo pInfo = PlayerInfo.getInfo(user);
        if (pInfo == null) {
            return;
        }

        GameMoneyInfo moneyInfo = user.getProperty(GAME_MONEY_INFO);
        if (moneyInfo == null) {
            return;
        }

        synchronized (this) {
            if (this.gameRoom.setting.maxUserPerRoom == 8) {
                for (int i = 0; i < 8; ++i) {
                    GamePlayer gp = this.playerList.get(i);
                    if (gp.getPlayerStatus() == 0
                            || gp.pInfo == null
                            || !gp.pInfo.nickName.equalsIgnoreCase(user.getName())) {
                        continue;
                    }

                    this.gameLog.append("RE<").append(i).append(">");
                    if (moneyInfo != null && !gp.gameMoneyInfo.sessionId.equals(moneyInfo.sessionId)) {
                        Debug.trace("onUserEnter exists in room", user.getName());
                        moneyInfo.restoreMoney(this.gameRoom.getId());
                    }

                    user.setProperty(USER_CHAIR, gp.chair);
                    gp.user = user;
                    gp.reqQuitRoom = false;
                    user.setProperty(GAME_MONEY_INFO, gp.gameMoneyInfo);
                    if (this.serverState == 1) {
                        this.sendGameInfo(gp.chair);
                    } else {
                        this.notifyUserEnter(gp);
                    }
                    return;
                }
            }

            for (int i = 0; i < 8; ++i) {
                GamePlayer gp = this.playerList.get(i);
                if (gp.getPlayerStatus() != 0) continue;

                if (this.serverState == 0) {
                    gp.setPlayerStatus(2);
                } else {
                    gp.setPlayerStatus(1);
                }

                gp.takeChair(user, pInfo, moneyInfo);
                ++this.playerCount;
                if (this.playerCount == 1) {
                    this.doiChuong(gp, false);
                    this.chuongChair = gp.chair;
                }
                this.notifyUserEnter(gp);
                break;
            }
        }
    }

    @Override
    public void onNoHu(ThongTinThangLon info) {
        this.thongTinNoHu = info;
    }

    @Override
    public void choNoHu(String nickName) {
        for (int i = 0; i < 8; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (gp.getUser() == null || !gp.getUser().getName().equalsIgnoreCase(nickName)) continue;
            this.gameManager.getGameMatch().suit.noHuAt(gp.chair);
        }
    }

    @Override
    protected Runnable getGameLoopTask() {
        return gameManager::gameLoop;
    }

    private void resetPlayDisconnect() {
        for (int i = 0; i < 8; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (gp.pInfo == null) continue;
            gp.pInfo.setIsHold(false);
        }
    }

    private void datCuoc(User user, DataCmd data) {
        RevDatCuoc cmd = new RevDatCuoc(data);
        this.datCuoc(user, cmd.rate);
    }

    private void datCuoc(User user, int rate) {
        GamePlayer gp = this.getPlayerByUser(user);
        GamePlayer chuong = this.getPlayerByChair(this.chuongChair);
        if (gp != null && gp.isPlaying() && !gp.camChuong && chuong != null && chuong.isPlaying() && chuong.camChuong) {
            int e = this.kiemTraDatCuoc(gp, rate);
            if (e == 0) {
                this.datCuocThanhCong(gp, chuong, rate);
                this.notifyDatCuocThanhCong(gp, rate);
            } else {
                SendDatCuoc msg = new SendDatCuoc();
                msg.Error = (byte) e;
                this.send(msg, user);
            }
        }
    }

    private void datCuocThanhCong(GamePlayer gp, GamePlayer chuong, int rate) {
        gp.spRes.cuocChuong = rate;
    }

    private void notifyDatCuocThanhCong(GamePlayer gp, int rate) {
        this.gameLog.append("DC<");
        this.gameLog.append(gp.chair).append("/");
        this.gameLog.append(rate);
        this.gameLog.append(">");
        SendDatCuoc msg = new SendDatCuoc();
        msg.chair = gp.chair;
        msg.rate = rate;
        this.send(msg);
    }

    private int kiemTraDatCuoc(GamePlayer gp, int rate) {
        if (rate < 1 || rate > 4) {
            return 3;
        }
        if (gp.spRes.daCuocChuong()) {
            return 1;
        }
        if (!this.kiemTraDieuKienSoDu(gp, rate * 4)) {
            return 2;
        }
        return 0;
    }

    private boolean kiemTraDieuKienSoDu(GamePlayer gp, int rate) {
        long maxLost = gp.spRes.tongTienDatCuocTreo() * this.getMoneyBet();
        long require = rate * this.getMoneyBet() + maxLost;
        if (gp.gameMoneyInfo.freezeMoney >= require) {
            return true;
        }
        long more = require - gp.gameMoneyInfo.freezeMoney;
        boolean res = gp.gameMoneyInfo.addFreezeMoney(more, this.gameRoom.getId(), this.gameManager.getGameMatch().id);
        if (res) {
            return gp.gameMoneyInfo.freezeMoney >= require;
        }
        return false;
    }

    private void vaoGa(User user, DataCmd data) {
        if (this.playingCount <= 2) {
            return;
        }

        SendVaoGa msg = new SendVaoGa();
        GamePlayer gp = this.getPlayerByUser(user);
        if (gp == null
                || !gp.isPlaying()
                || gp.camChuong) {
            return;
        }


        if (gp.spRes.cuocGa != 0) {
            msg.Error = 1;
            this.send(msg, user);
            return;
        }

        if (!this.kiemTraVaoGa(gp)) {
            msg.Error = 2;
            this.send(msg, user);
        }

        msg.chair = gp.chair;
        msg.tienVaoGa = 3L * this.getMoneyBet();
        this.gameLog.append("VG<");
        this.gameLog.append(gp.chair).append("/").append(3);
        this.gameLog.append(">");
        this.send(msg);
    }

    private boolean kiemTraVaoGa(GamePlayer gp) {
        if (this.kiemTraDieuKienSoDu(gp, 3)) {
            gp.spRes.cuocGa = 3;
            return true;
        }
        return false;
    }

    private void dongYDanhBien(User user, DataCmd data) {
        RevDongYDanhBien cmd = new RevDongYDanhBien(data);
        this.dongYDanhBien(user, cmd.chair);
    }

    private void dongYDanhBien(User user, int chair) {
        GamePlayer me = this.getPlayerByUser(user);
        GamePlayer enemy = this.getPlayerByChair(chair);
        int rate = me.spRes.duocYeuCauDanhBien[chair];
        if (isNullOrNotPlayingOrCamChuong(me, enemy)
                || !this.kiemTraQuyenDongYDanhBien(me, rate)
                || !this.kiemTraQuyenDongYDanhBien(enemy, rate)
                || rate == 0) {
            return;
        }

        enemy.spRes.cuocDanhBien[me.chair] = rate;
        me.spRes.cuocDanhBien[enemy.chair] = rate;
        this.gameLog.append("SL<");
        this.gameLog.append(enemy.chair).append(";");
        this.gameLog.append(me.chair).append(";");
        this.gameLog.append(rate);
        this.gameLog.append(">");
        SendDongYDanhBien msg1 = new SendDongYDanhBien();
        msg1.chair = me.chair;
        this.send(msg1, enemy.getUser());
        SendDongYDanhBien msg2 = new SendDongYDanhBien();
        msg2.chair = enemy.chair;
        this.send(msg2, me.getUser());
    }

    private boolean kiemTraQuyenYeuCauDanhBien(GamePlayer gp, int rate) {
        if (rate < 1 || rate > 2) {
            return false;
        }

        long tongTienCuoc = gp.spRes.tongTienDatCuocTreo();
        long require = tongTienCuoc + (rate * 4) * this.getMoneyBet();
        if (gp.gameMoneyInfo.freezeMoney >= require) {
            return true;
        }

        long more = require - gp.gameMoneyInfo.freezeMoney;
        if (gp.gameMoneyInfo.addFreezeMoney(more, this.gameRoom.getId(), this.gameManager.getGameMatch().id)) {
            return gp.gameMoneyInfo.freezeMoney >= require;
        }

        return false;
    }

    private boolean kiemTraQuyenDongYDanhBien(GamePlayer gp, int rate) {
        if (gp.spRes.cuocChuong == 0) {
            return false;
        }
        return this.kiemTraDieuKienSoDu(gp, rate * 4);
    }

    private void yeuCauDanhBien(User user, DataCmd data) {
        RevDanhBien cmd = new RevDanhBien(data);
        this.yeuCauDanhBien(user, cmd.chair, cmd.rate);
    }

    private void yeuCauDanhBien(User user, int chair, int rate) {
        GamePlayer enemy = this.getPlayerByChair(chair);
        GamePlayer me = this.getPlayerByUser(user);
        if (isNullOrNotPlayingOrCamChuong(enemy, me)
                || !this.kiemTraQuyenYeuCauDanhBien(me, rate)
                || !this.kiemTraQuyenYeuCauDanhBien(enemy, rate)) {

            SendYeuCauDanhBien msg = new SendYeuCauDanhBien();
            msg.chair = chair;
            msg.rate = rate;
            msg.Error = 2;
            this.send(msg, me.getUser());
        }

        if (enemy.spRes.duocYeuCauDanhBien[enemy.chair] != 0
                || me.spRes.duocYeuCauDanhBien[enemy.chair] != 0) {
            SendYeuCauDanhBien msg = new SendYeuCauDanhBien();
            msg.Error = 1;
            msg.chair = enemy.chair;
            msg.rate = rate;
            this.send(msg, me.getUser());
        }

        if (enemy.getUser() == null || enemy.getUser().isBot()) {
            enemy.spRes.duocYeuCauDanhBien[me.chair] = rate;
            enemy.yeuCauBotDanhBien = me.chair;
            return;
        }

        SendYeuCauDanhBien msg = new SendYeuCauDanhBien();
        msg.chair = me.chair;
        msg.rate = rate;
        enemy.spRes.duocYeuCauDanhBien[me.chair] = rate;
        this.send(msg, enemy.getUser());
    }

    private boolean isNullOrNotPlayingOrCamChuong(GamePlayer... gamePlayers) {
        for (GamePlayer gamePlayer : gamePlayers) {
            if (gamePlayer == null
                    || !gamePlayer.isPlaying()
                    || gamePlayer.camChuong) {
                return true;
            }
        }
        return false;
    }

    private void keCua(User user, DataCmd data) {
        RevKeCua cmd = new RevKeCua(data);
        this.keCua(user, cmd.chair, cmd.rate);
    }

    private void keCua(User user, int chair, int rate) {

        if (!this.checkChair(chair)) return;

        GamePlayer me = this.getPlayerByUser(user);
        GamePlayer ally = this.getPlayerByChair(chair);
        if (isNullOrNotPlayingOrCamChuong(me, ally)) {
            return;
        }

        int e = this.kiemTraDieuKienKeCua(me, chair, rate);
        if (e == 0) {
            this.gameLog.append("KC<");
            this.gameLog.append(me.chair).append(";");
            this.gameLog.append(ally.chair).append(";");
            this.gameLog.append(rate);
            this.gameLog.append(">");
            me.spRes.cuocKeCua[chair] = rate;
        }
        this.notifyKeCua(me, chair, rate, e);
    }

    private int kiemTraDieuKienKeCua(GamePlayer gp, int chair, int rate) {
        if (gp.spRes.cuocKeCua[chair] != 0) {
            return 1;
        }
        long totalBet = gp.spRes.tongTienDatCuocTreo();
        long require = totalBet * this.getMoneyBet() + (rate * 4) * this.getMoneyBet();

        if (gp.gameMoneyInfo.freezeMoney >= require) {
            return 0;
        }

        long more = require - gp.gameMoneyInfo.freezeMoney;

        if (!gp.gameMoneyInfo.addFreezeMoney(more, this.gameRoom.getId(), this.gameManager.getGameMatch().id)) {
            return 2;
        }

        if (gp.gameMoneyInfo.freezeMoney < require) {
            return 0;
        }

        return 2;
    }

    private void notifyKeCua(GamePlayer gp, int chair, int rate, int e) {
        SendKeCua msg = new SendKeCua();
        msg.fromChair = gp.chair;
        msg.toChair = chair;
        msg.rate = rate;
        msg.Error = (byte) e;
        if (e == 0) {
            this.send(msg);
        } else {
            this.send(msg, gp.getUser());
        }
    }

    private boolean checkChair(int chair) {
        return chair >= 0 && chair < 8;
    }

    private void moBai(User user, DataCmd data) {
        GamePlayer gp = this.getPlayerByUser(user);
        if (gp != null) {
            gp.moBai = true;
            this.notifyMoBai(gp);
        }
        this.hoanThanhMoBai();
    }

    private void hoanThanhMoBai() {
        for (int i = 0; i < 8; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying() || !gp.dangChoMoBai()) continue;
            return;
        }
        this.gameManager.hoanThanhMoBai();
    }

    private void notifyMoBai(GamePlayer gp) {
        if (!gp.isPlaying()
                || gp.spInfo.handCards == null) {
            return;
        }

        SendMoBaiSuccess msg = new SendMoBaiSuccess();
        msg.chair = gp.chair;
        msg.cards = gp.spInfo.handCards.toByteArray();
        msg.bo = gp.spInfo.handCards.kiemTraBo();
        this.send(msg);
    }

    GamePlayer getPlayerByChair(int i) {
        if (i >= 0 && i < 8) {
            return this.playerList.get(i);
        }
        return null;
    }

    long getMoneyBet() {
        return gameRoom.setting.moneyBet;
    }

    private void logEndGame() {
        GameUtils.logEndGame(this.gameManager.getGameMatch().id, this.gameLog.toString(), this.gameManager.getGameMatch().logTime);
    }

    int getPlayingCount() {
        return this.playingCount;
    }

    private boolean checkPlayerChair(int chair) {
        return chair >= 0 && chair < 8;
    }

    private GamePlayer timNguoiNhieuTien() {
        GamePlayer maxPlayer = null;
        long maxMoney = 0L;
        for (int i = 0; i < 8; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.hasUser()) continue;
            if (maxPlayer == null) {
                maxPlayer = gp;
                maxMoney = gp.gameMoneyInfo.getCurrentMoneyFromCache();
                continue;
            }
            long currentMoney = gp.gameMoneyInfo.getCurrentMoneyFromCache();
            if (maxMoney >= currentMoney) continue;
            maxMoney = currentMoney;
            maxPlayer = gp;
        }
        return maxPlayer;
    }

    private void doiChuong(GamePlayer gp, boolean notify) {
        this.chuongChair = this.newChuongChair = gp.chair;
        gp.camChuong = true;

        for (int i = 0; i < 8; ++i) {
            if (i == gp.chair) continue;
            this.getPlayerByChair(i).camChuong = false;
        }

        if (notify) {
            SendDoiChuong msg = new SendDoiChuong();
            msg.chair = this.chuongChair;
            this.send(msg);
        }
    }

    private boolean kiemTraDoiChuong(boolean force) {
        GamePlayer newChuong = this.getPlayerByChair(this.newChuongChair);
        if (newChuong != null && newChuong.hasUser()) {
            if (this.chuongChair == newChuong.chair) {
                return true;
            }
            long newChuongCurrentMoney = newChuong.gameMoneyInfo.getCurrentMoneyFromCache();
            if (newChuongCurrentMoney >= 100L * this.getMoneyBet()) {
                this.doiChuong(newChuong, true);
                return true;
            }
        }

        if (force) {
            newChuong = this.timNguoiNhieuTien();
            if (newChuong == null || this.chuongChair == newChuong.chair) {
                return true;
            }
            this.doiChuong(newChuong, true);
            return true;
        }
        return false;
    }

    private void sendMsgToPlayingUser(BaseMsg msg) {
        for (int i = 0; i < 8; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying()) continue;
            this.send(msg, gp.getUser());
        }
    }

    void send(BaseMsg msg) {
        for (int i = 0; i < 8; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (gp.getUser() == null) continue;
            ExtensionUtility.getExtension().send(msg, gp.getUser());
        }
    }

    private void chiabai() {
        this.gameLog.append("CB<");
        SendDealCard msg = new SendDealCard();
        msg.gameId = this.gameManager.getGameMatch().id;
        for (int i = 0; i < 8; ++i) {
            GamePlayer gp = this.playerList.get(i);
            if (!gp.isPlaying()) continue;
            User user = gp.getUser();
            msg.cards = gp.spInfo.handCards.toByteArray();
            this.gameLog.append(gp.chair).append("/");
            this.gameLog.append(gp.spInfo.handCards.toString()).append("/");
            this.gameLog.append(gp.spInfo.handCards.kiemTraBo()).append(";");
            this.send(msg, user);
        }
        this.gameLog.append(">");
    }

    private GroupCard getThenRemoveRandomGroupCard(List<GroupCard> cards) {
        if (cards.isEmpty()) {
            return null;
        }

        int index = BotManager.instance().getRandomNumber(cards.size());
        GroupCard groupCard = cards.get(index);
        cards.remove(index);
        return groupCard;
    }

    void chiaBaiNgauNhien2() {
        long bet = this.getMoneyBet();
        int moneyType = this.gameRoom.setting.moneyType;
        List<GroupCard> cards = this.gameManager.getGameMatch().suit.dealCards(true);
        for (int i = 0; i < 8; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            GroupCard gc = cards.get(i);
            if (!gc.isNoHu() || moneyType != 1 || bet < 1000L || gp.hasUser() && gp.getUser().isBot()) {
                gp.addCards(cards.get(i));
                continue;
            }
            gp.addCards(cards.get(8));
        }
        this.chiabai();
    }

    void chiaBaiCanBang2() {
        List<GroupCard> cards = this.gameManager.getGameMatch().suit.dealCards(false);
        Collections.sort(cards, GroupCard.groupCardComparator);

        misa.utils.debug.Debug.trace("Chia bai can bang: ", cards.get(0), cards.get(2), cards.get(3));

        LinkedList<GroupCard> highGroups = new LinkedList<>();
        for (int i = 0; i < 4; ++i) {
            highGroups.add(cards.get(i));
        }

        LinkedList<GroupCard> lowGroups = new LinkedList<>();
        for (int i = 4; i < 8; ++i) {
            lowGroups.add(cards.get(i));
        }

        for (int i = 0; i < 8; ++i) {
            GamePlayer gamePlayer = this.getPlayerByChair(i);
            if (gamePlayer == null) continue;

            if (gamePlayer.hasUser()
                    && (gamePlayer.getUser()).isBot()) {
                boolean isUp = BotManager.instance().balanceMode == 1;
                if (isUp) {
                    GroupCard groupCard = this.getThenRemoveRandomGroupCard(highGroups);
                    if (groupCard == null) {
                        groupCard = this.getThenRemoveRandomGroupCard(lowGroups);
                    }
                    gamePlayer.addCards(groupCard);
                    continue;
                }

                GroupCard groupCard = this.getThenRemoveRandomGroupCard(lowGroups);
                if (groupCard == null) {
                    groupCard = this.getThenRemoveRandomGroupCard(highGroups);
                }
                gamePlayer.addCards(groupCard);
                continue;
            }

            boolean isUp = BotManager.instance().balanceMode == 1;
            if (isUp) {
                GroupCard groupCard = this.getThenRemoveRandomGroupCard(lowGroups);
                if (groupCard == null) {
                    groupCard = this.getThenRemoveRandomGroupCard(highGroups);
                }
                gamePlayer.addCards(groupCard);
                continue;
            }

            GroupCard groupCard = this.getThenRemoveRandomGroupCard(highGroups);
            if (groupCard == null) {
                groupCard = this.getThenRemoveRandomGroupCard(lowGroups);
            }
            gamePlayer.addCards(groupCard);
        }

        this.chiabai();
    }

    void start() {
        this.gameLog.setLength(0);
        this.gameLog.append("MC<");
        this.playingCount = 0;
        this.serverState = 1;
        for (int i = 0; i < 8; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            gp.tuDongChoiNhanh = 0;
            gp.prepareNewGame();
            if (!this.coTheChoiTiep(gp)) continue;
            gp.setPlayerStatus(3);
            ++this.playingCount;
            gp.pInfo.setIsHold(true);
            PlayerInfo.setRoomId(gp.pInfo.nickName, this.gameRoom.getId());
            this.gameLog.append(gp.pInfo.nickName).append("/");
            this.gameLog.append(i).append(";");
            gp.choiTiepVanSau = false;
        }
        this.gameLog.append(this.getMoneyBet()).append(";");
        this.gameLog.append(this.chuongChair).append(";");
        this.gameLog.append(this.gameRoom.setting.moneyType);
        this.gameLog.append(">");
        this.moiVaoCuoc();
        this.logStartGame();
    }

    void botJoinRoom() {
        if (this.gameRoom.setting.moneyType == 1 && this.playerCount < 2) {
            int x = BotManager.instance().getRandomNumber(10);
            BotManager.instance().regJoinRoom(this.gameRoom, x);
        }
    }

    void botStartGame() {
        if (!GameUtils.isBot || this.gameRoom.setting.moneyType != 1 || this.gameRoom.setting.password.length() > 0) {
            return;
        }

        int botCount = 0;
        int userCount = 0;
        Calendar cal = Calendar.getInstance();
        int hour = cal.get(11);
        for (int i = 0; i < 8; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            User user = gp.getUser();
            if (user != null && user.isBot()) {
                int x;
                ++botCount;
                Bot bot = BotManager.instance().getBotByName(user.getName());
                ++bot.count;
                int num = 7;
                if (hour < 11 || hour > 23) {
                    num = 5;
                }
                if (bot.count < 5 && this.playerCount < num || (x = BotManager.instance().getRandomNumber(5)) != 0)
                    continue;
                gp.yeuCauBotRoiPhong = x = BotManager.instance().getRandomNumber(30) + 5;
                --botCount;
                continue;
            }
            if (user == null) continue;
            ++userCount;
        }
        int num = 5;
        if (hour < 11 || hour > 23) {
            num = 3;
        }
        int x = BotManager.instance().getRandomNumber(1);
        if (this.playerCount <= num && x == 0) {
            int after = GameUtils.rd.nextInt(15) + 15;
            BotManager.instance().regJoinRoom(this.gameRoom, after);
        }
    }

    private void logStartGame() {
        for (int i = 0; i < 8; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying()) continue;
            GameUtils.logStartGame(this.gameManager.getGameMatch().id, gp.pInfo.nickName, this.gameManager.getGameMatch().logTime, this.gameRoom.setting.moneyType);
        }
    }

    private void moiVaoCuoc() {
        SendMoiDatCuoc msg = new SendMoiDatCuoc();
        msg.countDownTime = (byte) (4 * this.playingCount);
        this.sendMsgToPlayingUser(msg);
    }

    private int demSoNguoiChoiTiep() {
        int count = 0;
        for (int i = 0; i < 8; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!this.coTheChoiTiep(gp)) continue;
            ++count;
        }
        return count;
    }

    void kiemTraTuDongBatDau(int after) {
        if (this.gameManager.getGameState() != GameState.GS_NO_START) {
            return;
        }

        if (this.demSoNguoiChoiTiep() < 2) {
            this.gameManager.cancelAutoStart();
        } else {
            this.gameManager.makeAutoStart(after);
        }
    }

    private boolean coTheChoiTiep(GamePlayer gp) {
        return gp.hasUser() && gp.canPlayNextGame();
    }

    private void removePlayerAtChair(int chair, boolean disconnect) {
        if (!this.checkPlayerChair(chair)) {
            return;
        }

        GamePlayer gp = this.playerList.get(chair);
        gp.choiTiepVanSau = true;
        this.notifyUserExit(gp, disconnect);
        if (gp.user != null) {
            gp.user.removeProperty(USER_CHAIR);
            gp.user.removeProperty(GAME_ROOM);
            gp.user.removeProperty(GAME_MONEY_INFO);
        }

        Debug.trace("removePlayerAtChair", chair, gp.pInfo.nickName, this.gameManager.getGameMatch().id);
        if (gp.gameMoneyInfo != null) {
            gp.gameMoneyInfo.restoreMoney(this.gameRoom.getId());
        }
        gp.user = null;
        gp.pInfo = null;
        gp.gameMoneyInfo = null;
        gp.setPlayerStatus(0);
        --this.playerCount;
        if (gp.camChuong) {
            this.kiemTraDoiChuong(true);
        }
    }

    private void notifyUserEnter(GamePlayer gamePlayer) {
        User user = gamePlayer.getUser();
        if (user == null) {
            return;
        }

        SendNewUserJoin msg = new SendNewUserJoin();
        msg.money = gamePlayer.gameMoneyInfo.currentMoney;
        msg.uStatus = gamePlayer.getPlayerStatus();
        msg.setBaseInfo(gamePlayer.pInfo);
        msg.uChair = gamePlayer.chair;
        this.sendMsgExceptMe(msg, user);
        this.notifyJoinRoomSuccess(gamePlayer);
    }

    private void notifyJoinRoomSuccess(GamePlayer gamePlayer) {
        SendJoinRoomSuccess msg = new SendJoinRoomSuccess();
        msg.chuongChair = this.chuongChair;
        msg.uChair = gamePlayer.chair;
        msg.roomId = this.gameRoom.getId();
        msg.moneyType = this.gameRoom.setting.moneyType;
        msg.gameId = this.gameManager.getGameMatch().id;
        msg.moneyBet = this.gameRoom.setting.moneyBet;
        for (int i = 0; i < 8; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            msg.playerStatus[i] = (byte) gp.getPlayerStatus();
            msg.playerList[i] = gp.getPlayerInfo();
            msg.moneyInfoList[i] = gp.gameMoneyInfo;
        }
        msg.gameAction = (byte) this.gameManager.getGameAction().id;
        msg.countDownTime = (byte) this.gameManager.countDown;
        this.send(msg, gamePlayer.getUser());
    }

    private void notifyUserExit(GamePlayer gamePlayer, boolean disconnect) {
        if (gamePlayer.pInfo == null) {
            return;
        }

        gamePlayer.pInfo.setIsHold(false);
        SendUserExitRoom msg = new SendUserExitRoom();
        msg.nChair = (byte) gamePlayer.chair;
        msg.nickName = gamePlayer.pInfo.nickName;
        this.send(msg);
    }

    private GamePlayer getPlayerByUser(User user) {
        Integer chair = (Integer) user.getProperty(USER_CHAIR);
        if (chair == null) {
            return null;
        }

        GamePlayer gp = this.getPlayerByChair(chair);
        if (gp != null && gp.pInfo != null && gp.pInfo.nickName.equalsIgnoreCase(user.getName())) {
            return gp;
        }

        return null;
    }

    private void sendGameInfo(int chair) {
        GamePlayer gamePlayer = this.getPlayerByChair(chair);
        SendGameInfo msg = new SendGameInfo();
        msg.gameState = this.gameManager.getGameState().id;
        msg.isAutoStart = this.gameManager.isAutoStart;
        msg.gameAction = this.gameManager.getGameAction().id;
        msg.countdownTime = this.gameManager.countDown;
        msg.chair = (byte) gamePlayer.chair;
        msg.chuongChair = (byte) this.chuongChair;
        msg.roomId = this.gameRoom.getId();
        msg.comissionRate = this.gameRoom.setting.commisionRate;
        msg.jackpotRate = this.gameRoom.setting.rule;
        msg.moneyType = this.gameRoom.setting.moneyType;
        msg.gameId = this.gameManager.getGameMatch().id;
        msg.moneyBet = this.getMoneyBet();
        msg.initPrivateInfo(gamePlayer);
        for (int i = 0; i < 8; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (gp.hasUser()) {
                msg.pInfos[i] = gp;
                msg.hasInfoAtChair[i] = true;
                continue;
            }
            msg.hasInfoAtChair[i] = false;
        }
        this.send(msg, gamePlayer.getUser());
        this.resendEndGame(gamePlayer);
    }

    private void pOutRoom(User user, DataCmd dataCmd) {
        GamePlayer gp = this.getPlayerByUser(user);
        this.pOutRoom(gp);
    }

    private void pOutRoom(GamePlayer gp) {
        if (gp == null) {
            return;
        }

        if (gp.getPlayerStatus() == 3) {
            gp.reqQuitRoom = !gp.reqQuitRoom;
            this.notifyRegisterOutRoom(gp);
        } else {
            GameRoomManager.instance().leaveRoom(gp.getUser(), this.gameRoom);
        }
    }

    private void notifyRegisterOutRoom(GamePlayer gp) {
        SendNotifyReqQuitRoom msg = new SendNotifyReqQuitRoom();
        msg.chair = (byte) gp.chair;
        msg.reqQuitRoom = gp.reqQuitRoom;
        this.send(msg);
    }

    private void dispatchAddEventScore(User user, UserScore score) {
        if (user == null) {
            return;
        }

        score.moneyType = this.gameRoom.setting.moneyType;
        UserScore newScore = score.clone();
        HashMap<GameEventParam, Object> evtParams = new HashMap<GameEventParam, Object>();
        evtParams.put(GameEventParam.USER, user);
        evtParams.put(GameEventParam.USER_SCORE, newScore);
        ExtensionUtility.dispatchEvent(new BZEvent(GameEventType.EVENT_ADD_SCORE, evtParams));
    }

    private void notifyKickRoom(GamePlayer gp, int reason) {
        SendKickRoom msg = new SendKickRoom();
        msg.reason = (byte) reason;
        this.send(msg, gp.getUser());
    }

    void pPrepareNewGame() {
        SendUpdateMatch msg = new SendUpdateMatch();
        for (int i = 0; i < 8; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (gp.getPlayerStatus() != 0) {
                if (GameUtils.isMainTain) {
                    gp.reqQuitRoom = true;
                    this.notifyKickRoom(gp, 2);
                }

                if (!this.coTheChoiTiep(gp)) {
                    if (!gp.checkMoneyCanPlay()) {
                        this.notifyKickRoom(gp, 1);
                    }
                    if (gp.getUser() != null && this.gameRoom != null) {
                        GameRoom gameRoom = gp.getUser().getProperty(GAME_ROOM);
                        if (gameRoom == this.gameRoom) {
                            GameRoomManager.instance().leaveRoom(gp.getUser(), this.gameRoom);
                        }
                    } else {
                        this.removePlayerAtChair(i, false);
                    }
                    msg.hasInfoAtChair[i] = false;
                } else {
                    msg.hasInfoAtChair[i] = true;
                    msg.pInfos[i] = gp;
                }
                gp.setPlayerStatus(2);
                continue;
            }
            msg.hasInfoAtChair[i] = false;
        }

        for (int i = 0; i < 8; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!msg.hasInfoAtChair[i]) continue;
            msg.chair = (byte) i;
            this.send(msg, gp.getUser());
        }
        this.kiemTraDoiChuong(false);
        this.gameManager.prepareNewGame();
        this.serverState = 0;
    }

    private void pBatDau(User user, DataCmd dataCmd) {
        int nextGamePlayerCount = this.demSoNguoiChoiTiep();
        if (nextGamePlayerCount >= 2) {
            this.gameManager.makeAutoStart(0);
        }
    }

    private void pCheatCards(User user, DataCmd dataCmd) {
        if (!GameUtils.isCheat) {
            return;
        }
        RevCheatCard cmd = new RevCheatCard(dataCmd);
        if (cmd.isCheat) {
            this.gameManager.getGameMatch().isCheat = true;
            this.gameManager.getGameMatch().suit.setOrder(cmd.cards);
        } else {
            this.gameManager.getGameMatch().isCheat = false;
            this.gameManager.getGameMatch().suit.initCard();
        }
    }

    private void pDangKyChoiTiep(User user, DataCmd dataCmd) {
        GamePlayer gp = this.getPlayerByUser(user);
        if (gp != null) {
            gp.choiTiepVanSau = true;
        }
    }

    void endGame() {
        for (int i = 0; i < 8; ++i) {
            GamePlayer gp1 = this.getPlayerByChair(i);
            if (!gp1.isPlaying()) continue;
            for (int j = i + 1; j < 8; ++j) {
                GamePlayer gp2 = this.getPlayerByChair(j);
                if (!gp2.isPlaying()) continue;
                this.soBaiHaiNguoiChoi(gp1, gp2);
            }
        }
        this.soGa();
        this.timNguoiDoiChuong();
        this.tinhTienKetThuc();
        this.notifyEndGame();
        this.logEndGame();
        this.kiemTraNoHuThangLon();
    }

    private void soGa() {
        int maxChair = -1;
        GroupCard maxCard = null;
        int countGa = 0;
        for (int i = 0; i < 8; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying() || gp.spRes.cuocGa <= 0) continue;
            ++countGa;
            gp.spRes.thangGa = -3;
            if (maxChair == -1) {
                maxChair = i;
                maxCard = gp.spInfo.handCards;
                continue;
            }
            GroupCard currentCard = gp.spInfo.handCards;
            int v = BacayRule.soSanhBai(currentCard, maxCard);
            if (v <= 0) continue;
            maxCard = currentCard;
            maxChair = i;
        }
        if (maxChair != -1) {
            GamePlayer winGa = this.getPlayerByChair(maxChair);
            winGa.spRes.thangGa = (countGa - 1) * 3;
        }
    }

    private void timNguoiDoiChuong() {
        GroupCard gc = null;
        for (int i = 0; i < 8; ++i) {
            GroupCard newGc;
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying() || (newGc = gp.spInfo.handCards).kiemTraBo() != 1 || gc != null && BacayRule.soSanhBai(newGc, gc) <= 0)
                continue;
            gc = newGc;
            this.newChuongChair = i;
        }
    }

    private void tinhTienKetThuc() {
        long tienChuong = 0L;
        long chuongRate = this.getMoneyBet();
        GamePlayer gpChuong = this.getPlayerByChair(this.chuongChair);
        if (gpChuong != null && gpChuong.isPlaying()) {
            tienChuong = this.tinhTienChuong(gpChuong);
            if (tienChuong < 0L) {
                chuongRate = -this.hieuChinhTiLeTienChuong(tienChuong);
            }
            this.hieuChinhTienThangThuaChuong(chuongRate);
        }
        this.tinhTraTien();
    }

    private long tinhTienChuong(GamePlayer gp) {
        long tienChuong = gp.spRes.calculateThangChuong(true);
        if (tienChuong < 0L) {
            UserScore score = new UserScore();
            score.money = tienChuong * this.getMoneyBet();
            score.winCount = 0;
            score.lostCount += 0;
            try {
                tienChuong = gp.gameMoneyInfo.chargeMoneyInGame(score, this.gameRoom.getId(), this.gameManager.getGameMatch().id);
            } catch (MoneyException e) {
                tienChuong = 0L;
                if (!(e instanceof NotEnoughMoneyException)) {
                    CommonHandle.writeErrLog(("ERROR WHEN CHARGE MONEY INGAME" + gp.gameMoneyInfo.toString()), e);
                }
                gp.reqQuitRoom = true;
            }
            gp.spRes.tongTienCuoiVan = gp.spRes.tienThangChuong = tienChuong;
            score.money = tienChuong;
            this.dispatchAddEventScore(gp.getUser(), score);
        }
        return tienChuong;
    }

    private void hieuChinhTienThangThuaChuong(long chuongRate) {
        for (int i = 0; i < 8; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying()) continue;
            if (!gp.camChuong) {
                gp.spRes.tinhTienThangTongNguoiChoi(chuongRate, this.getMoneyBet());
                continue;
            }
            gp.spRes.tinhTienThangTongCuaChuong(chuongRate);
        }
    }

    private long hieuChinhTiLeTienChuong(long tienChuong) {
        long sum = 0L;
        long rate = 0L;
        for (int i = 0; i < 8; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying() || gp.camChuong) continue;
            sum += gp.spRes.calculateThangChuong(false);
        }
        double doubleRate = 1.0 * (double) tienChuong / (double) sum;
        rate = Math.round(doubleRate);
        if ((double) rate > doubleRate) {
            --rate;
        }
        return rate;
    }

    private void tinhTraTien() {
        for (int i = 0; i < 8; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying()) continue;
            UserScore score = new UserScore();
            score.money = gp.spRes.tongTienCuoiVan;
            if (gp.camChuong && score.money < 0L) continue;
            if (score.money >= 0L) {
                score.wastedMoney = (long) ((double) (score.money * this.gameRoom.setting.commisionRate) / 100.0);
                score.money -= score.wastedMoney;
                ++score.winCount;
            } else {
                score.wastedMoney = 0L;
                ++score.lostCount;
            }
            try {
                score.money = gp.gameMoneyInfo.chargeMoneyInGame(score, this.gameRoom.getId(), this.gameManager.getGameMatch().id);
            } catch (MoneyException e) {
                score.money = 0L;
                if (!(e instanceof NotEnoughMoneyException)) {
                    CommonHandle.writeErrLog(("ERROR WHEN CHARGE MONEY INGAME: |" + gp.gameMoneyInfo.sessionId + "|" + this.gameManager.getGameMatch().id), e);
                }
                gp.reqQuitRoom = true;
            }
            gp.spRes.tongTienCuoiVan = score.money;
            this.dispatchAddEventScore(gp.getUser(), score);
        }
    }

    private void notifyEndGame() {
        SendEndGame msg = new SendEndGame();
        for (int i = 0; i < 8; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (gp.isPlaying()) {
                msg.tongCuocGa.add(gp.spRes.tienThangGa);
                msg.tongThangDatCuoc.add(gp.spRes.tienThangChuong);
                msg.tongThangDanhBien.add(gp.spRes.tongTienDanhBien());
                msg.tongThangKeCua.add(gp.spRes.tongTienKeCua());
                msg.tongKetThangThua.add(gp.spRes.tongTienCuoiVan);
                msg.currentMoneyList.add(gp.gameMoneyInfo.getCurrentMoneyFromCache());
                msg.gamePlayers[i] = gp;
                msg.playerStatus[i] = (byte) gp.getPlayerStatus();
                continue;
            }

            msg.playerStatus[i] = (byte) gp.getPlayerStatus();
        }

        this.gameLog.append("KT<");
        this.gameLog.append(0).append(";");
        for (int i = 0; i < 8; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying()) continue;

            msg.result = gp.spRes;
            this.gameLog.append(gp.chair).append("/");
            this.gameLog.append(gp.spRes.tongTienCuoiVan).append("/");
            this.gameLog.append(gp.spInfo.handCards).append(";");
            SendEndGame newMsg = new SendEndGame();
            newMsg.copyData(msg);
            this.send(newMsg, gp.getUser());
        }
        this.gameLog.append(">");
    }

    private boolean dispatchEventThangLon(GamePlayer gp, boolean isNoHu) {
        return GameUtils.dispatchEventThangLon(gp.getUser(), this.gameRoom, this.gameManager.getGameMatch().id,
                gp.gameMoneyInfo, this.getMoneyBet(), isNoHu, gp.getHandCards());
    }

    private void kiemTraNoHuThangLon() {
        for (int i = 0; i < 8; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying()) continue;

            if (gp.spInfo.kiemTraNoHu()) {
                if (!this.dispatchEventThangLon(gp, true)) continue;
                this.gameManager.countDown += 5;
                continue;
            }
            this.dispatchEventThangLon(gp, false);
        }
    }

    private void resendEndGame(GamePlayer reconnectPlayer) {
        if (!reconnectPlayer.isPlaying() || this.gameManager.getGameState() != GameState.GS_GAME_END) {
            return;
        }

        SendEndGame msg = new SendEndGame();
        for (int i = 0; i < 8; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (gp.isPlaying()) {
                msg.tongCuocGa.add(gp.spRes.tienThangGa);
                msg.tongThangDatCuoc.add(gp.spRes.tienThangChuong);
                msg.tongThangDanhBien.add(gp.spRes.tongTienDanhBien());
                msg.tongThangKeCua.add(gp.spRes.tongTienKeCua());
                msg.tongKetThangThua.add(gp.spRes.tongTienCuoiVan);
                msg.currentMoneyList.add(gp.gameMoneyInfo.getCurrentMoneyFromCache());
                msg.gamePlayers[i] = gp;
                msg.playerStatus[i] = (byte) gp.getPlayerStatus();
                continue;
            }
            msg.playerStatus[i] = (byte) gp.getPlayerStatus();
        }
        SendEndGame newMsg = new SendEndGame();
        msg.result = reconnectPlayer.spRes;
        newMsg.copyData(msg);
        this.send(newMsg, reconnectPlayer.getUser());
    }

    private void soBaiHaiNguoiChoi(GamePlayer gp1, GamePlayer gp2) {
        if (gp1.camChuong || gp2.camChuong) {
            this.soBaiVoiChuong(gp1, gp2);
        } else {
            this.soBaiDanhBien(gp1, gp2);
        }
    }

    private void soBaiVoiChuong(GamePlayer gp1, GamePlayer gp2) {
        if (gp1.camChuong) {
            this.soChuong(gp2, gp1);
        } else {
            this.soChuong(gp1, gp2);
        }
    }

    private void soChuong(GamePlayer gp, GamePlayer chuong) {
        GroupCard gc = gp.spInfo.handCards;
        GroupCard chuongGc = chuong.spInfo.handCards;
        sResultInfo res = gp.spRes;
        sResultInfo chuongRes = chuong.spRes;
        int v = BacayRule.soSanhBai(gc, chuongGc);
        if (v >= 0 && gc.kiemTraBo() == 1) {
            this.newChuongChair = gp.chair;
        }
        if (res.cuocChuong == 0) {
            this.datCuocThanhCong(gp, chuong, 1);
        }
        res.thangChuong = v * res.cuocChuong;
        chuongRes.thangChuong -= v * res.cuocChuong;
        for (int i = 0; i < 8; ++i) {
            GamePlayer gpKe;
            if (i == gp.chair || i == chuong.chair || !(gpKe = this.getPlayerByChair(i)).isPlaying() || gpKe.spRes.cuocKeCua[gp.chair] == 0)
                continue;
            gpKe.spRes.thangKeCua[gp.chair] = v * gpKe.spRes.cuocKeCua[gp.chair];
            chuong.spRes.thangChuong -= gpKe.spRes.thangKeCua[gp.chair];
        }
    }

    private void soBaiDanhBien(GamePlayer gp1, GamePlayer gp2) {
        GroupCard gc1 = gp1.spInfo.handCards;
        GroupCard gc2 = gp2.spInfo.handCards;
        sResultInfo res1 = gp1.spRes;
        sResultInfo res2 = gp2.spRes;
        int v = BacayRule.soSanhBai(gc1, gc2);
        if (gp1.spRes.cuocDanhBien[gp2.chair] > 0 && gp2.spRes.cuocDanhBien[gp1.chair] > 0) {
            res1.thangBien[gp2.chair] = v * res1.cuocDanhBien[gp2.chair];
            res2.thangBien[gp1.chair] = -v * res2.cuocDanhBien[gp1.chair];
        }
    }

    void botAutoPlay() {
        for (int i = 0; i < 8; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (gp.isPlaying()
                    && gp.getUser() != null
                    && gp.getUser().isBot()
                    && !gp.camChuong
                    && this.gameManager.getGameAction() == GameAction.CHIA_BAI) {

                if (gp.yeuCauBotRoiPhong == this.gameManager.countDown) {
                    this.pOutRoom(gp);
                }
                if (!gp.vaoCuoc) {
                    if (BotManager.instance().getRandomNumber(2) == 1) {
                        gp.vaoCuoc = true;
                        int rate = BotManager.instance().getRandomNumber(4) + 1;
                        this.datCuoc(gp.getUser(), rate);
                    }
                } else if (!gp.vaoGa) {
                    if (BotManager.instance().getRandomNumber(2) == 1) {
                        gp.vaoGa = true;
                        this.vaoGa(gp.getUser(), null);
                    }
                } else if (this.playingCount > 2) {
                    int chair = gp.lastChair++ % 8;
                    GamePlayer gamePlayer = this.getPlayerByChair(chair);
                    int ran2 = BotManager.instance().getRandomNumber(5);
                    int rate = BotManager.instance().getRandomNumber(2) + 1;
                    if (gamePlayer != null && gamePlayer.isPlaying() && gamePlayer.chair != gp.chair && !gamePlayer.camChuong && ran2 == 1) {
                        this.keCua(gp.getUser(), gamePlayer.chair, rate);
                    }
                    if (gp.yeuCauBotDanhBien >= 0 && gp.yeuCauBotDanhBien < 8) {
                        int x = BotManager.instance().getRandomNumber(5);
                        if (x == 0) {
                            this.dongYDanhBien(gp.getUser(), gp.yeuCauBotDanhBien);
                            gp.yeuCauBotDanhBien = -1;
                        } else if (x == 1) {
                            gp.yeuCauBotDanhBien = -1;
                        }
                    }
                }
            }

            if (!gp.isPlaying()
                    || gp.getUser() == null
                    || !gp.getUser().isBot()
                    || this.gameManager.getGameAction() != GameAction.MO_BAI
                    || (BotManager.instance().getRandomNumber(5)) != 1
                    || this.gameManager.countDown > 14)
                continue;

            this.moBai(gp.getUser(), null);
        }
    }

    void notifyNoHu() {
        try {
            if (this.thongTinNoHu != null) {
                for (int i = 0; i < 8; ++i) {
                    GamePlayer gp = this.getPlayerByChair(i);
                    if (!gp.isPlaying() || !gp.gameMoneyInfo.sessionId.equalsIgnoreCase(this.thongTinNoHu.moneySessionId)
                            || !gp.gameMoneyInfo.nickName.equalsIgnoreCase(this.thongTinNoHu.nickName))
                        continue;
                    gp.gameMoneyInfo.currentMoney = this.thongTinNoHu.currentMoney;
                    break;
                }
                SendNoHu msg = new SendNoHu();
                msg.info = this.thongTinNoHu;
                for (Map.Entry entry : this.gameRoom.userManager.entrySet()) {
                    User u = (User) entry.getValue();
                    if (u == null) continue;
                    this.send(msg, u);
                }
            }
        } catch (Exception e) {
            CommonHandle.writeErrLog(e);
        } finally {
            this.thongTinNoHu = null;
        }
    }

    @Override
    public String toString() {
        try {
            JSONObject json = this.toJONObject();
            if (json != null) {
                return json.toString();
            }
            return "{}";
        } catch (Exception e) {
            return "{}";
        }
    }

    @Override
    public JSONObject toJONObject() {
        try {
            JSONObject json = new JSONObject();
            json.put("gameState", this.gameManager.getGameState().id);
            json.put("gameAction", this.gameManager.getGameAction().id);
            JSONArray arr = new JSONArray();
            for (int i = 0; i < 8; ++i) {
                GamePlayer gp = this.getPlayerByChair(i);
                arr.put(gp.toJSONObject());
            }
            json.put("players", arr);
            return json;
        } catch (Exception e) {
            return null;
        }
    }
}
