/*
 * Decompiled with CFR 0.144.
 *
 * Could not load the following classes:
 *  game.modules.gameRoom.entities.GameRoomIdGenerator
 *  game.utils.GameUtils
 */
package game.bacay.server.logic;

import game.modules.gameRoom.entities.GameRoomIdGenerator;
import game.utils.GameUtils;

public class GameRoomTable {
    public CardSuit suit = new CardSuit();
    public int id = GameRoomIdGenerator.getId();
    public boolean isCheat = false;
    public long logTime = System.currentTimeMillis();

    public void reset() {
        this.id = GameRoomIdGenerator.getId();
        this.logTime = System.currentTimeMillis();
        if (!this.isCheat || !GameUtils.isCheat) {
            this.suit.setRandom();
        }
    }
}
