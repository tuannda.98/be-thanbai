/*
 * Decompiled with CFR 0.144.
 *
 * Could not load the following classes:
 *  bitzero.server.entities.User
 *  bitzero.server.extensions.data.BaseMsg
 *  bitzero.util.common.business.Debug
 *  game.modules.bot.BotManager
 *  game.modules.gameRoom.entities.GameRoom
 *  game.modules.gameRoom.entities.GameRoomSetting
 *  game.utils.GameUtils
 */
package game.bacay.server;

import game.bacay.server.cmd.send.SendUpdateAutoStart;
import game.bacay.server.logic.GameRoomTable;
import game.modules.bot.BotManager;
import game.utils.GameUtils;

enum GameState {
    GS_NO_START(0), GS_GAME_PLAYING(1), GS_GAME_END(2);

    final byte id;

    GameState(int i) {
        id = (byte) i;
    }
}

enum GameAction {
    NO_ACTION(0), CHIA_BAI(1), MO_BAI(2);

    final byte id;

    GameAction(int i) {
        id = (byte) i;
    }
}

public class GameManager {

    private GameState gameState = GameState.GS_NO_START;

    private GameAction gameAction = GameAction.NO_ACTION;
    protected int countDown = 0;
    protected boolean isAutoStart = false;

    private final GameRoomTable gameRoomTable = new GameRoomTable();

    private final BaCayGameServer gameServer;

    public GameManager(BaCayGameServer gameServer) {
        this.gameServer = gameServer;
    }

    public void prepareNewGame() {
        this.gameRoomTable.reset();
        this.isAutoStart = false;
    }

    public void gameLoop() {
        synchronized (gameServer) {
            if (gameState == GameState.GS_NO_START && this.isAutoStart) {
                --this.countDown;
                if (this.countDown <= 0) {
                    this.gameState = GameState.GS_GAME_PLAYING;
                    this.gameServer.start();
                    this.gameAction = GameAction.CHIA_BAI;
                    this.countDown = 4 * this.gameServer.getPlayingCount();
                    this.gameServer.botStartGame();
                }
                return;
            }

            if (gameState == GameState.GS_GAME_PLAYING) {
                if (this.gameAction != GameAction.NO_ACTION) {
                    --this.countDown;
                    if (GameUtils.isBot) {
                        this.gameServer.botAutoPlay();
                    }
                    if (this.countDown <= 0) {
                        if (this.gameAction == GameAction.CHIA_BAI) {
                            this.chiaBai();
                        } else if (this.gameAction == GameAction.MO_BAI) {
                            this.gameServer.endGame();
                            this.gameState = GameState.GS_GAME_END;
                            this.countDown = 12;
                            this.gameAction = GameAction.NO_ACTION;
                        }
                    }
                }
                return;
            }

            if (gameState == GameState.GS_GAME_END) {
                --this.countDown;
                if (this.countDown == 5) {
                    this.gameServer.notifyNoHu();
                }
                if (this.countDown <= 0) {
                    this.gameState = GameState.GS_NO_START;
                    this.gameServer.pPrepareNewGame();
                }
                return;
            }

            ++this.countDown;
            this.gameServer.kiemTraTuDongBatDau(5);
            if (this.countDown % 11 == 10) {
                this.gameServer.botJoinRoom();
            }
        }
    }

    public void hoanThanhMoBai() {
        countDown = 0;
    }


    private void notifyAutoStartToUsers(int after) {
        SendUpdateAutoStart msg = new SendUpdateAutoStart();
        msg.isAutoStart = this.isAutoStart;
        msg.autoStartTime = (byte) after;
        this.gameServer.send(msg);
    }

    public void cancelAutoStart() {
        this.isAutoStart = false;
        this.notifyAutoStartToUsers(0);
        this.countDown = 0;
    }

    public void makeAutoStart(int after) {
        if (gameState != GameState.GS_NO_START) {
            return;
        }

        if (!this.isAutoStart) {
            this.countDown = after;
        } else if (after < this.countDown) {
            this.countDown = after;
        } else {
            after = this.countDown;
        }

        this.isAutoStart = true;
        this.notifyAutoStartToUsers(after);
    }

    private void chiaBaiNgauNhien() {
        this.gameServer.chiaBaiNgauNhien2();
        this.gameAction = GameAction.MO_BAI;
        this.countDown = 20;
    }

    private void chiaBaiCanBang() {
        this.gameServer.chiaBaiCanBang2();
        this.gameAction = GameAction.MO_BAI;
        this.countDown = 20;
    }

    private void chiaBai() {
        if (BotManager.instance().balanceMode == 0) {
            this.chiaBaiNgauNhien();
            return;
        }

        boolean isUp = BotManager.instance().balanceMode == 1;
        int x = BotManager.instance().getRandomNumber(3);
        if (x == 0 || isUp) {
            this.chiaBaiCanBang();
        } else {
            this.chiaBaiNgauNhien();
        }
    }

    public GameAction getGameAction() {
        return gameAction;
    }

    public GameRoomTable getGameMatch() {
        return gameRoomTable;
    }

    public GameState getGameState() {
        return gameState;
    }
}
