/*
 * Decompiled with CFR 0.144.
 *
 * Could not load the following classes:
 *  bitzero.server.entities.User
 *  game.entities.PlayerInfo
 *  game.modules.gameRoom.entities.GameMoneyInfo
 *  org.json.JSONObject
 */
package game.poker.server;

import bitzero.server.entities.User;
import game.entities.PlayerInfo;
import game.modules.gameRoom.entities.GameMoneyInfo;
import game.poker.server.logic.GroupCard;
import org.json.JSONObject;

public class GamePlayer {
    public static final int THANG_SAM = 1;
    public static final int THANG_TRANG = 2;
    public static final int THANG_THUONG = 3;

    public int chair;
    public long timeJoinRoom = 0L;
    public int countToOutRoom = 0;
    public boolean reqQuitRoom = false;
    public volatile boolean standUp = false;
    public boolean choiTiepVanSau = true;
    public User user = null;
    public PlayerInfo pInfo = null;
    public GameMoneyInfo gameMoneyInfo = null;
    public sPlayerInfo spInfo = new sPlayerInfo();
    public PokerGameServer gameServer;
    public boolean autoBuyIn = true;
    public int lastGameId = -1;
    public boolean requireBigBlind = false;
    public volatile long lastTimeBuyIn = 0L;

    private volatile PlayerStatus playerStatus = PlayerStatus.NO_LOGIN;

    public void prepareNewGame() {
        this.spInfo.pokerInfo.clearNewGame();
    }

    public byte[] getHandCards() {
        return this.spInfo.handCards.toByteArray();
    }

    public void setPlayerStatus(PlayerStatus playerStatus) {
        this.playerStatus = playerStatus;
    }

    public PlayerStatus getPlayerStatus() {
        return this.playerStatus;
    }

    public User getUser() {
        return this.user;
    }

    public PlayerInfo getPlayerInfo() {
        return this.pInfo;
    }

    public void addCards(GroupCard groupCard) {
        this.spInfo.handCards = groupCard;
    }

    public void takeChair(User user, PlayerInfo pInfo, GameMoneyInfo moneyInfo) {
        this.user = user;
        this.pInfo = pInfo;
        this.gameMoneyInfo = moneyInfo;
        this.reqQuitRoom = false;
        user.setProperty(PokerGameServer.USER_CHAIR, this.chair);
    }

    public boolean isPlaying() {
        return this.playerStatus == PlayerStatus.PLAY;
    }

    public boolean canPlayNextGame() {
        return !this.reqQuitRoom && this.checkMoneyCanPlay();
    }

    public boolean hasUser() {
        return this.playerStatus != PlayerStatus.NO_LOGIN;
    }

    public boolean checkMoneyCanPlay() {
        return this.gameMoneyInfo != null && this.gameMoneyInfo.canPlayNextGame();
    }

    public String toString() {
        try {
            JSONObject json = this.toJSONObject();
            if (json != null) {
                return json.toString();
            }
            return "{}";
        } catch (Exception e) {
            return "{}";
        }
    }

    public JSONObject toJSONObject() {
        try {
            JSONObject json = new JSONObject();
            json.put("reqQuitRoom", this.reqQuitRoom);
            json.put("playerStatus", this.playerStatus.id);
            if (this.gameMoneyInfo != null) {
                json.put("gameMoneyInfo", this.gameMoneyInfo.toJSONObject());
            }
            if (this.spInfo.handCards != null) {
                json.put("handCards", this.spInfo.handCards.toString());
            } else {
                json.put("handCards", "");
            }
            return json;
        } catch (Exception e) {
            return null;
        }
    }
}
