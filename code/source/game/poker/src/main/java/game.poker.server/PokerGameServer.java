/*
 * Decompiled with CFR 0.144.
 *
 * Could not load the following classes:
 *  bitzero.server.BitZeroServer
 *  bitzero.server.core.BZEvent
 *  bitzero.server.core.IBZEvent
 *  bitzero.server.core.IBZEventType
 *  bitzero.server.entities.User
 *  bitzero.server.extensions.data.BaseMsg
 *  bitzero.server.extensions.data.DataCmd
 *  bitzero.server.util.TaskScheduler
 *  bitzero.util.ExtensionUtility
 *  bitzero.util.common.business.CommonHandle
 *  bitzero.util.common.business.Debug
 *  com.vinplay.usercore.service.MoneyInGameService
 *  game.entities.PlayerInfo
 *  game.entities.UserScore
 *  game.eventHandlers.GameEventParam
 *  game.eventHandlers.GameEventType
 *  game.modules.gameRoom.cmd.send.SendNoHu
 *  game.modules.gameRoom.entities.GameMoneyInfo
 *  game.modules.gameRoom.entities.GameRoom
 *  game.modules.gameRoom.entities.GameRoomManager
 *  game.modules.gameRoom.entities.GameRoomSetting
 *  game.modules.gameRoom.entities.GameServer
 *  game.modules.gameRoom.entities.ListGameMoneyInfo
 *  game.modules.gameRoom.entities.MoneyException
 *  game.modules.gameRoom.entities.ThongTinThangLon
 *  game.utils.GameUtils
 *  org.json.JSONArray
 *  org.json.JSONObject
 *  org.slf4j.Logger
 *  org.slf4j.LoggerFactory
 */
package game.poker.server;

import bitzero.server.core.BZEvent;
import bitzero.server.entities.User;
import bitzero.server.extensions.data.BaseMsg;
import bitzero.server.extensions.data.DataCmd;
import bitzero.util.ExtensionUtility;
import bitzero.util.common.business.CommonHandle;
import bitzero.util.common.business.Debug;
import casio.king365.util.KingUtil;
import game.entities.PlayerInfo;
import game.entities.UserScore;
import game.eventHandlers.GameEventParam;
import game.eventHandlers.GameEventType;
import game.modules.gameRoom.cmd.send.SendNoHu;
import game.modules.gameRoom.entities.*;
import game.poker.server.cmd.receive.RevBuyIn;
import game.poker.server.cmd.receive.RevCheatCard;
import game.poker.server.cmd.receive.RevTakeTurn;
import game.poker.server.cmd.send.*;
import game.poker.server.logic.*;
import game.utils.GameUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static game.modules.gameRoom.GameRoomModule.GAME_ROOM;
import static game.modules.gameRoom.entities.GameMoneyInfo.GAME_MONEY_INFO;
import static game.poker.server.GameAction.*;
import static game.poker.server.GameState.*;
import static game.poker.server.logic.Turn.FOLD;

public class PokerGameServer
        extends GameServer {

    public GameManager gameMgr = new GameManager(this);
    public final List<GamePlayer> playerList = new ArrayList<>(9);
    public int playingCount = 0;
    public int winType;
    public volatile int groupIndex;
    public volatile int playerCount;
    protected StringBuilder gameLog = new StringBuilder();
    public final Logger logger = LoggerFactory.getLogger("debug");
    public List<Turn> cheatTurns = new LinkedList<>();
    public int turnIndex = 0;
    public ThongTinThangLon thongTinNoHu = null;

    public PokerGameServer(GameRoom room) {
        super(room);
        int i = 0;
        while (i < 9) {
            GamePlayer gp = new GamePlayer();
            gp.gameServer = this;
            gp.spInfo.pokerInfo = new PokerPlayerInfo(gp);
            gp.chair = i++;
            this.playerList.add(gp);
        }
    }

    @Override
    public synchronized void onGameMessage(User user, DataCmd dataCmd) {
        this.logger.info("onGameMessage: ", dataCmd.getId(), user.getName());
        switch (dataCmd.getId()) {
            case 3111: {
                this.pOutRoom(user);
                break;
            }
            case 3102: {
                this.buyIn(user, dataCmd);
                break;
            }
            case 3101: {
                this.registerTakeTurn(user, dataCmd);
                break;
            }
            case 3115: {
                this.pCheatCards(dataCmd);
                break;
            }
            case 3116: {
                this.pDangKyChoiTiep(user);
                break;
            }
            case 3108: {
                this.pShowCard(user);
                break;
            }
            case 3113: {
                this.standUp(user);
            }
        }
    }

    private void standUp(User user) {
        GamePlayer gp = this.getPlayerByUser(user);
        if (gp != null) {
            gp.standUp = !gp.standUp;
            SendStandUp msg = new SendStandUp();
            msg.standUp = gp.standUp;
            this.send(msg, user);
            PokerPlayerInfo info = gp.spInfo.pokerInfo;
            if (!(!info.fold && gp.isPlaying() || this.gameMgr.isAutoStart)) {
                this.requestBuyIn(gp);
            }
        }
    }

    private void pShowCard(User user) {
        SendShowCard msg = new SendShowCard();
        GamePlayer gp = this.getPlayerByUser(user);
        if (gp != null && gp.isPlaying() && this.gameMgr.gameState == GS_GAME_END && !gp.spInfo.pokerInfo.fold) {
            msg.chair = (byte) gp.chair;
            this.send(msg);
        }
    }

    private void buyIn(User user, DataCmd data) {
        RevBuyIn cmd = new RevBuyIn(data);
        this.buyIn(user, cmd.moneyBuyIn, cmd.autoBuyIn);
    }

    private void buyIn(User user, long moneyBuyIn, boolean autoBuyIn) {
        GamePlayer gp = this.getPlayerByUser(user);
        this.buyIn(gp, moneyBuyIn, autoBuyIn);
    }

    private boolean checkBuyInMoney(long moneyBuyIn) {
        return moneyBuyIn >= 40L * this.getMoneyBet() && moneyBuyIn <= 400L * this.getMoneyBet();
    }

    private boolean canBuyIn(GamePlayer gp) {
        if (gp == null) {
            return false;
        }
        long delta = System.currentTimeMillis() - gp.lastTimeBuyIn;
        if (delta < 1000L) {
            return false;
        }
        PokerPlayerInfo info = gp.spInfo.pokerInfo;
        if (info == null) {
            return false;
        }
        boolean flag = gp.standUp && (!gp.isPlaying() || info.fold || this.gameMgr.gameState == GS_GAME_END);
        if (this.gameMgr.gameState == GS_NO_START) //allow game player buyin when game no start
            flag = true;

        return info.currentMoney < 2L * this.getMoneyBet() || flag;
    }

    private boolean buyIn(GamePlayer gp, long moneyBuyIn, boolean autoBuyIn) {
        if (!this.canBuyIn(gp)) {
            return false;
        }

        if (!this.checkBuyInMoney(moneyBuyIn)) {
            GameRoomManager.instance().leaveRoom(gp.getUser(), this.gameRoom);
            this.notifyKickRoom(gp, (byte) 3);
            return false;
        }
        SendBuyIn msg = new SendBuyIn();
        ListGameMoneyInfo.instance().removeGameMoneyInfo(gp.gameMoneyInfo, this.gameRoom.getId());
        boolean result = gp.gameMoneyInfo.freezeMoneyBegining(moneyBuyIn);
        if (result) {
            msg.buyInMoney = gp.gameMoneyInfo.freezeMoney;
            PokerPlayerInfo pokerInfo = gp.spInfo.pokerInfo;
            pokerInfo.currentMoney = msg.buyInMoney;
            pokerInfo.lastBuyInMoney = msg.buyInMoney;
            Debug.trace("buyIn: Freeze=;Current=;", gp.pInfo.nickName, gp.gameMoneyInfo.freezeMoney, pokerInfo.currentMoney);
            msg.chair = gp.chair;
            gp.autoBuyIn = autoBuyIn;
            this.send(msg);
            gp.standUp = false;
            gp.lastTimeBuyIn = System.currentTimeMillis();
            return true;
        }
        GameRoomManager.instance().leaveRoom(gp.getUser(), this.gameRoom);
        this.notifyKickRoom(gp, (byte) 3);
        return false;
    }

    private boolean buyInAuTo(GamePlayer gp) {
        if (this.canBuyIn(gp)) {
            if (!this.checkBuyInMoney(gp.spInfo.pokerInfo.lastBuyInMoney) || !gp.autoBuyIn) {
                Debug.trace("buyInAuTo failed", gp.gameMoneyInfo.freezeMoney, gp.autoBuyIn);
                return false;
            }
            PokerPlayerInfo pokerInfo = gp.spInfo.pokerInfo;
            this.buyIn(gp, pokerInfo.lastBuyInMoney, true);
            return true;
        }
        Debug.trace("buyInAuTo failed gameplayer null");
        return false;
    }

    private GameManager getGameManager() {
        return this.gameMgr;
    }

    protected GamePlayer getPlayerByChair(int i) {
        if (i >= 0 && i < 9) {
            return this.playerList.get(i);
        }
        return null;
    }

    private long getMoneyBet() {
        return this.gameRoom.setting.moneyBet;
    }

    private byte getPlayerCount() {
        return (byte) this.playerCount;
    }

    private boolean checkPlayerChair(int chair) {
        return chair >= 0 && chair < 9;
    }

    @Override
    public synchronized void onGameUserExit(User user) {
        Integer chair = user.getProperty(USER_CHAIR);
        if (chair == null) {
            Debug.trace("onGameUserExit", "chair null", user.getName());
            return;
        }
        GamePlayer gp = this.getPlayerByChair(chair);
        if (gp == null) {
            return;
        }
        if (gp.isPlaying()) {
            gp.reqQuitRoom = true;
            this.gameLog.append("DIS<").append(chair).append(">");
        } else {
            this.removePlayerAtChair(chair, !user.isConnected());
        }
        if (this.gameRoom.userManager.size() == 0) {
            this.resetPlayDisconnect();
            this.destroy();
        }
        if (this.gameRoom.userManager.size() <= 1) {
            this.gameMgr.gameTable.previousId = -1;
        }
    }

    private void resetPlayDisconnect() {
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (gp.pInfo == null) continue;
            gp.pInfo.setIsHold(false);
        }
    }

    @Override
    public void onGameUserDis(User user) {
        Integer chair = user.getProperty(USER_CHAIR);
        if (chair == null) {
            Debug.trace("onGameUserExit", "chair null", user.getName());
            return;
        }
        GamePlayer gp = this.getPlayerByChair(chair);
        if (gp == null) {
            return;
        }
        if (gp.isPlaying()) {
            gp.reqQuitRoom = true;
            this.gameLog.append("DIS<").append(chair).append(">");
        } else {
            GameRoomManager.instance().leaveRoom(user, this.gameRoom);
        }
    }

    @Override
    public synchronized void onGameUserReturn(User user) {
        if (user == null) {
            return;
        }
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.playerList.get(i);
            if (gp.getPlayerStatus() == PlayerStatus.NO_LOGIN || gp.pInfo == null || gp.pInfo.userId != user.getId())
                continue;
            this.gameLog.append("RE<").append(i).append(">");
            GameMoneyInfo moneyInfo = user.getProperty(GAME_MONEY_INFO);
            if (moneyInfo != null && !Objects.equals(gp.gameMoneyInfo.sessionId, moneyInfo.sessionId)) {
                ListGameMoneyInfo.instance().removeGameMoneyInfo(moneyInfo, -1);
            }
            user.setProperty(USER_CHAIR, gp.chair);
            gp.user = user;
            gp.reqQuitRoom = false;
            user.setProperty(GAME_MONEY_INFO, gp.gameMoneyInfo);
            gp.user.setProperty(USER_CHAIR, gp.chair);
            this.sendGameInfo(gp.chair);
            break;
        }
    }

    @Override
    public synchronized void onGameUserEnter(User user) {
        if (user == null) {
            return;
        }
        PlayerInfo pInfo = PlayerInfo.getInfo(user);
        GameMoneyInfo moneyInfo = user.getProperty(GAME_MONEY_INFO);
        if (moneyInfo == null) {
            return;
        }
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.playerList.get(i);
            if (gp.getPlayerStatus() == PlayerStatus.NO_LOGIN || gp.pInfo == null || gp.pInfo.userId != user.getId())
                continue;
            this.gameLog.append("RE<").append(i).append(">");
            if (!Objects.equals(gp.gameMoneyInfo.sessionId, moneyInfo.sessionId)) {
                ListGameMoneyInfo.instance().removeGameMoneyInfo(moneyInfo, -1);
            }
            user.setProperty(USER_CHAIR, gp.chair);
            gp.user = user;
            gp.reqQuitRoom = false;
            user.setProperty(GAME_MONEY_INFO, gp.gameMoneyInfo);
            gp.user.setProperty(USER_CHAIR, gp.chair);
            if (this.gameMgr.gameState == GS_GAME_PLAYING) {
                this.sendGameInfo(gp.chair);
            } else {
                this.notifyUserEnter(gp);
            }
            return;
        }

        if (this.gameRoom.setting.maxUserPerRoom == 9) {
            for (int i = 0; i < 9; ++i) {
                GamePlayer gp = this.playerList.get(i);
                if (gp.getPlayerStatus() != PlayerStatus.NO_LOGIN) continue;
                if (this.gameMgr.gameState == GS_NO_START) {
                    gp.setPlayerStatus(PlayerStatus.SIT);
                } else {
                    gp.setPlayerStatus(PlayerStatus.VIEW);
                }
                gp.takeChair(user, pInfo, moneyInfo);
                ++this.playerCount;
                if (this.playerCount == 1) {
                    this.gameMgr.roomCreatorUserId = user.getId();
                    this.gameMgr.roomOwnerChair = i;
                    this.init();
                }
                this.notifyUserEnter(gp);
                break;
            }
        }

        if (this.gameRoom.setting.maxUserPerRoom == 2) {
            for (int i = 0; i < 9; ++i) {
                GamePlayer gp = this.playerList.get(i);
                if (i != 0 && i != 1 || gp.getPlayerStatus() != PlayerStatus.NO_LOGIN) continue;
                if (this.gameMgr.gameState == GS_NO_START) {
                    gp.setPlayerStatus(PlayerStatus.SIT);
                } else {
                    gp.setPlayerStatus(PlayerStatus.VIEW);
                }
                gp.takeChair(user, pInfo, moneyInfo);
                ++this.playerCount;
                if (this.playerCount == 1) {
                    this.gameMgr.roomCreatorUserId = user.getId();
                    this.gameMgr.roomOwnerChair = i;
                    this.init();
                }
                this.notifyUserEnter(gp);
                break;
            }
        }
        this.kiemTraTuDongBatDau(2);
    }

    private int getNumTotalPlayer() {
        return this.playerCount;
    }

    private void sendMsgToPlayingUser(BaseMsg msg) {
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying()) continue;
            this.send(msg, gp.getUser());
        }
    }

    protected void send(BaseMsg msg) {
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (gp.getUser() == null) continue;
            ExtensionUtility.getExtension().send(msg, gp.getUser());
        }
    }

    protected void chiabai() {
        this.gameLog.append("CB<");
        SendDealPrivateCard msg = new SendDealPrivateCard();
        msg.gameId = this.gameMgr.gameTable.id;
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.playerList.get(i);
            if (!gp.isPlaying()) continue;

            User user = gp.getUser();
            msg.cards = gp.spInfo.handCards.toByteArray();
            msg.card_name = gp.spInfo.handCards.kiemtraBo().id;
            this.gameLog.append(gp.chair).append("/");
            this.gameLog.append(gp.spInfo.handCards.toString()).append("/");
            this.gameLog.append(gp.spInfo.handCards.kiemtraBo()).append(";");
            Debug.trace("chiabai:", gp.pInfo.nickName, gp.spInfo.handCards);
            this.send(msg, user);
        }
        this.gameLog.append(">");
        this.gameLog.append("PC<");
        PokerGameInfo gameInfo = this.gameMgr.gameTable.pokerGameInfo;
        this.gameLog.append(gameInfo.publicCard);
        this.gameLog.append(">");
    }

    protected synchronized void start() {
        KingUtil.printLog("Start match " + toJONObject());
        this.gameLog.setLength(0);
        this.gameLog.append("BD<");
        this.playingCount = 0;
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!this.coTheChoiTiep(gp)) continue;
            gp.setPlayerStatus(PlayerStatus.PLAY);
            ++this.playingCount;
            gp.pInfo.setIsHold(true);
            PlayerInfo.setRoomId(gp.pInfo.nickName, this.gameRoom.getId());
            this.gameLog.append(gp.pInfo.nickName).append("/");
            this.gameLog.append(i).append(";");
            gp.choiTiepVanSau = false;
            if (this.gameMgr.gameTable.previousId > 0 && gp.lastGameId != this.gameMgr.gameTable.previousId) {
                gp.requireBigBlind = true;
            }
            gp.lastGameId = this.gameMgr.gameTable.id;
        }
        this.gameLog.append(this.gameRoom.setting.moneyType).append(";");
        this.gameLog.append(">");
        this.clearInfoNewGame();
        this.logStartGame();
    }

    private void clearInfoNewGame() {
        PokerGameInfo gameInfo = this.gameMgr.gameTable.pokerGameInfo;
        gameInfo.clearNewGame();
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying()) continue;
            PokerPlayerInfo pokerInfo = gp.spInfo.pokerInfo;
            pokerInfo.clearNewGame();
        }
    }

    private void logStartGame() {
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying()) continue;
            Debug.trace("logStartGame", gp.chair, gp.pInfo.nickName);
            GameUtils.logStartGame(this.gameMgr.gameTable.id, gp.pInfo.nickName, this.gameMgr.gameTable.logTime, this.gameRoom.setting.moneyType);
        }
    }

    private int demSoNguoiChoiTiep() {
        int count = 0;
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!this.coTheChoiTiep(gp)) continue;
            ++count;
        }
        return count;
    }

    private int demSoNguoiDangChoi() {
        int count = 0;
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            ++count;
        }
        return count;
    }

    protected void kiemTraTuDongBatDau(int after) {
        if (this.gameMgr.gameState != GS_NO_START) return;

        int count = this.demSoNguoiChoiTiep();
        if (count < 2) {

            this.gameMgr.cancelAutoStart();
        } else {

            this.gameMgr.makeAutoStart(after);
            if (count > 2) {
                // this.xuLiDanhCap(count);
            }
        }
    }

    private void xuLiDanhCap(int count) {
        int j;
        GamePlayer gp2;
        int i;
        GamePlayer gp1;
        boolean checkIp = false;
        for (i = 0; i < 9; ++i) {
            gp1 = this.getPlayerByChair(i);
            if (gp1.getUser() == null) continue;
            for (j = i + 1; j < 9; ++j) {
                gp2 = this.getPlayerByChair(j);
                if (gp2.getUser() == null || gp1.getUser().getIpAddress().equalsIgnoreCase(gp2.getUser().getIpAddress()))
                    continue;
                checkIp = true;
            }
        }
        if (!checkIp) {
            return;
        }
        for (i = 0; i < 9; ++i) {
            gp1 = this.getPlayerByChair(i);
            if (gp1.getUser() == null) continue;
            for (j = i + 1; j < 9; ++j) {
                gp2 = this.getPlayerByChair(j);
                if (gp2.getUser() == null || this.kiemTraDuocDanhCungNhau(gp1, gp2, true) || --count != 2) continue;
                return;
            }
        }
    }

    private boolean kiemTraDuocDanhCungNhau(GamePlayer gp1, GamePlayer gp2, boolean checkIp) {
        Debug.trace("kiemTraDuocDanhCungNhau1", gp1.getUser().getName(), gp1.reqQuitRoom, gp1.timeJoinRoom, checkIp, gp1.getUser().getIpAddress());
        Debug.trace("kiemTraDuocDanhCungNhau2", gp2.getUser().getName(), gp2.reqQuitRoom, gp2.timeJoinRoom, checkIp, gp2.getUser().getIpAddress());
        if (gp1.reqQuitRoom || gp2.reqQuitRoom) {
            return false;
        }
        if (checkIp && gp1.getUser().getIpAddress().equalsIgnoreCase(gp2.getUser().getIpAddress())) {
            if (gp1.timeJoinRoom > gp2.timeJoinRoom) {
                gp1.reqQuitRoom = true;
                GameRoomManager.instance().leaveRoom(gp1.getUser(), this.gameRoom);
            } else {
                gp2.reqQuitRoom = true;
                GameRoomManager.instance().leaveRoom(gp2.getUser(), this.gameRoom);
            }
            return false;
        }
        long delta = Math.abs(gp1.timeJoinRoom - gp2.timeJoinRoom);
        if ((double) delta < 1500.0) {
            if (gp1.timeJoinRoom > gp2.timeJoinRoom) {
                GameRoomManager.instance().leaveRoom(gp1.getUser(), this.gameRoom);
                gp1.reqQuitRoom = true;
            } else {
                GameRoomManager.instance().leaveRoom(gp2.getUser(), this.gameRoom);
                gp2.reqQuitRoom = true;
            }
            return false;
        }
        return true;
    }

    protected boolean coTheChoiTiep(GamePlayer gp) {
        return gp.user != null
                && gp.user.isConnected()
                && gp.canPlayNextGame()
                && gp.spInfo.pokerInfo.currentMoney >= 2L * this.gameRoom.setting.moneyBet;
    }

    private boolean coTheOLaiBan(GamePlayer gp) {
        return gp.user != null && gp.user.isConnected() && !gp.reqQuitRoom;
    }

    private synchronized void removePlayerAtChair(int chair, boolean disconnect) {
        if (!this.checkPlayerChair(chair)) {
            Debug.trace("removePlayerAtChair error", chair);
            return;
        }
        GamePlayer gp = this.playerList.get(chair);
        gp.standUp = false;
        gp.choiTiepVanSau = true;
        gp.countToOutRoom = 0;
        gp.lastGameId = -1;
        this.notifyUserExit(gp, disconnect);
        if (gp.user != null) {
            gp.user.removeProperty(USER_CHAIR);
            gp.user.removeProperty(GAME_ROOM);
            gp.user.removeProperty(GAME_MONEY_INFO);
        }
        gp.spInfo.pokerInfo.clearOutGame();
        gp.user = null;
        gp.pInfo = null;
        if (gp.gameMoneyInfo != null) {
            ListGameMoneyInfo.instance().removeGameMoneyInfo(gp.gameMoneyInfo, this.gameRoom.getId());
        }
        gp.gameMoneyInfo = null;
        gp.setPlayerStatus(PlayerStatus.NO_LOGIN);
        --this.playerCount;
        this.kiemTraTuDongBatDau(2);
    }

    private void notifyUserEnter(GamePlayer gamePlayer) {
        User user = gamePlayer.getUser();
        if (user == null) {
            return;
        }
        gamePlayer.timeJoinRoom = System.currentTimeMillis();
        SendNewUserJoin msg = new SendNewUserJoin();
        msg.money = gamePlayer.gameMoneyInfo.currentMoney;
        msg.uStatus = gamePlayer.getPlayerStatus().id;
        msg.setBaseInfo(gamePlayer.pInfo);
        msg.uChair = gamePlayer.chair;
        this.sendMsgExceptMe(msg, user);
        this.notifyJoinRoomSuccess(gamePlayer);
        if (GameUtils.isBot && !user.isPlayer()) {
            this.buyIn(gamePlayer, 40L * this.getMoneyBet(), true);
        }
    }

    private void notifyJoinRoomSuccess(GamePlayer gamePlayer) {
        SendJoinRoomSuccess msg = new SendJoinRoomSuccess();
        msg.uChair = gamePlayer.chair;
        msg.roomId = this.gameRoom.getId();
        msg.comission = this.gameRoom.setting.commisionRate;
        msg.comissionJackpot = this.gameRoom.setting.rule;
        msg.moneyType = this.gameRoom.setting.moneyType;
        msg.rule = this.gameRoom.setting.rule;
        msg.gameId = this.gameMgr.gameTable.id;
        msg.moneyBet = this.gameRoom.setting.moneyBet;
        msg.roomOwner = (byte) this.gameMgr.roomOwnerChair;
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            msg.playerStatus[i] = (byte) gp.getPlayerStatus().id;
            msg.playerList[i] = gp.getPlayerInfo();
            msg.moneyInfoList[i] = gp.spInfo.pokerInfo;
            if (gp.getUser() == null || gp.spInfo.handCards == null) continue;
            msg.handCardSize[i] = 2;
        }
        msg.gameAction = this.gameMgr.gameState.id;
        msg.gameAction = this.gameMgr.nextAction.id;
        msg.curentChair = (byte) this.gameMgr.currentChair();
        msg.countDownTime = (byte) this.gameMgr.countDown;
        this.send(msg, gamePlayer.getUser());
    }

    private void notifyUserExit(GamePlayer gamePlayer, boolean disconnect) {
        if (gamePlayer.pInfo != null) {
            Debug.trace(gamePlayer.pInfo.nickName, gamePlayer.chair, "exit room ", disconnect);
            gamePlayer.pInfo.setIsHold(false);
            SendUserExitRoom msg = new SendUserExitRoom();
            msg.nChair = (byte) gamePlayer.chair;
            msg.nickName = gamePlayer.pInfo.nickName;
            this.send(msg);
        } else {
            Debug.trace(gamePlayer.chair, "exit room playerInfo null");
        }
    }

    private GamePlayer getPlayerByUser(User user) {
        Integer chair = user.getProperty(USER_CHAIR);
        Debug.trace("getPlayerByUser: ", user.getName(), chair);
        if (chair != null) {
            GamePlayer gp = this.getPlayerByChair(chair);
            if (gp != null && gp.pInfo != null && gp.pInfo.nickName.equalsIgnoreCase(user.getName())) {
                return gp;
            }
            return null;
        }
        return null;
    }

    private void sendGameInfo(int chair) {
        KingUtil.printLog("sendGameInfo() BEGIN");
        System.out.println("sendGameInfo() BEGIN");
        GamePlayer me = this.getPlayerByChair(chair);
        if (me != null) {
            SendGameInfo msg = new SendGameInfo();
            msg.pokerGameInfo = this.gameMgr.gameTable.pokerGameInfo;
            msg.currentChair = this.gameMgr.currentChair;
            msg.gameState = this.gameMgr.gameState.id;
            msg.gameAction = this.gameMgr.nextAction.id;
            msg.countdownTime = this.gameMgr.countDown;
            msg.maxUserPerRoom = this.gameRoom.setting.maxUserPerRoom;
            msg.moneyType = this.gameRoom.setting.moneyType;
            msg.roomBet = this.gameRoom.setting.moneyBet;
            msg.gameId = this.gameMgr.gameTable.id;
            msg.roomId = this.gameRoom.getId();
            Round round = this.gameMgr.gameTable.getLastRound();
            if (round != null) {
                msg.roundId = round.roundId;
            }
            for (int i = 0; i < 9; ++i) {
                GamePlayer gp = this.getPlayerByChair(i);
                if (gp.isPlaying()) {
                    msg.hasInfoAtChair[i] = true;
                    msg.pInfos[i] = gp;
                    continue;
                }
                msg.hasInfoAtChair[i] = false;
            }
            msg.initPrivateInfo(me);
            KingUtil.printLog("sendGameInfo() 123 msg: " + msg);
            System.out.println("sendGameInfo() 123 msg: " + msg);
            // this.send((BaseMsg)msg, me.getUser());
        }
    }

    private boolean canQuitRoom(GamePlayer gp) {
        if (gp.isPlaying()) {
            return gp.spInfo.pokerInfo.fold || gp.spInfo.pokerInfo.registerFold;
        }
        return true;
    }

    private void pOutRoom(User user) {
        Debug.trace("pOutRoom", user.getName());
        GamePlayer gp = this.getPlayerByUser(user);
        if (gp != null) {
            if (gp.isPlaying()) {
                gp.reqQuitRoom = !gp.reqQuitRoom;
                this.notifyRegisterOutRoom(gp);
            } else {
                GameRoomManager.instance().leaveRoom(user, this.gameRoom);
            }
        }
    }

    private void notifyRegisterOutRoom(GamePlayer gp) {
        SendNotifyReqQuitRoom msg = new SendNotifyReqQuitRoom();
        msg.chair = (byte) gp.chair;
        msg.reqQuitRoom = gp.reqQuitRoom;
        this.send(msg);
    }

    private void registerTakeTurn(User user, DataCmd dataCmd) {
        RevTakeTurn cmd = new RevTakeTurn(dataCmd);
        this.registerTakeTurn(user, cmd);
    }

    protected synchronized boolean registerTakeTurn(User user, RevTakeTurn cmd) {
        KingUtil.printLog("registerTakeTurn u: " + user.getName() + ", RevTakeTurn: " + cmd);
        if (this.gameMgr.gameState != GS_GAME_PLAYING
                || this.gameMgr.nextAction != IN_ROUND
                || this.gameMgr.countDown < 1) {

            return false;
        }

        Debug.trace("registerTakeTurn", user.getName());
        GamePlayer gp = this.getPlayerByUser(user);
        if (gp == null || gp.chair != this.gameMgr.currentChair) {
            return false;
        }

        PokerGameInfo gameInfo = this.gameMgr.gameTable.pokerGameInfo;
        if (!gp.spInfo.pokerInfo.register(cmd, gameInfo)) {
            Debug.trace("register take turn failed");
            return false;
        }

        gp.countToOutRoom = 0;
        this.takeTurn();
        return true;
    }

    protected void endGame() {
        this.gameMgr.gameState = GS_GAME_END;
        this.gameMgr.countDown = (int) (10.0 + Math.ceil(1.5 * (double) this.playingCount));
        Debug.trace("endGame");
        this.gomTienVaoHu();
        this.sortUserRanking();
        this.tinhTraTien();
        this.traTien();
        this.notifyEndGame();
        this.kiemTraNoHuThangLon();
    }

    private void notifyEndGame() {
        KingUtil.printLog("notifyEndGame()");
        int i;
        PokerGameInfo gameInfo = this.gameMgr.gameTable.pokerGameInfo;
        PokerResult result = gameInfo.resultPoker;
        List<PokerRank> listRank = result.ranking;
        SendEndGame msg = new SendEndGame();
        msg.moneyPot = gameInfo.potMoney;
        msg.publicCard = gameInfo.publicCard.toByteArray();
        for (i = 0; i < listRank.size(); ++i) {
            PokerRank rank = listRank.get(i);
            msg.ketQuaTinhTien[rank.chair] = rank.finalMoney;
            msg.winLost[rank.chair] = rank.win;
            msg.rank[rank.chair] = rank.fold ? 10L : (long) rank.rank;
            msg.maxCards[rank.chair] = rank.cards.toByteArray();
            msg.groupCardName[rank.chair] = rank.cards.kiemtraBo().id;
        }
        for (i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            msg.hasInfoAtChair[gp.chair] = 0;
            if (gp.isPlaying()) {
                String un = gp.pInfo.nickName;
                PokerPlayerInfo pokerInfo = gp.spInfo.pokerInfo;
                msg.hasInfoAtChair[gp.chair] = pokerInfo.getStatus();
                msg.moneyArray[gp.chair] = pokerInfo.currentMoney;
                msg.balanceMoney[gp.chair] = gp.gameMoneyInfo.getCurrentMoneyFromCache();
                msg.cards[gp.chair] = !pokerInfo.fold ? gp.spInfo.handCards.toByteArray() : new byte[0];
                // msg.cards[gp.chair] = gp.spInfo.handCards.toByteArray();
                KingUtil.printLog("notifyEndGame() xi: " + i + ", name: " + un + ", fold: " + gp.spInfo.pokerInfo.fold + ", gp card: " + gp.spInfo.handCards + ", card bytearray: " + gp.spInfo.handCards.toByteArrayStr());
            }
        }
        msg.countdown = this.gameMgr.countDown;
        try {
            KingUtil.printLog("SendEndGame msg: " + msg);
        } catch (Exception e) {
            KingUtil.printLog("Exception: " + KingUtil.printException(e));
        }
        this.send(msg);
        this.logKetQua(msg);
    }

    private void logKetQua(SendEndGame msg) {
        this.gameLog.append("KT<");
        this.gameLog.append(0).append(";");
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying()) continue;
            this.gameLog.append(gp.chair).append("/").append(msg.ketQuaTinhTien[gp.chair]).append("/").append(gp.spInfo.maxCards).append(";");
        }
        this.gameLog.append(">");
        GameUtils.logEndGame(this.gameMgr.gameTable.id, this.gameLog.toString(), this.gameMgr.gameTable.logTime);
    }

    private void traTien() {
        PokerGameInfo gameInfo = this.gameMgr.gameTable.pokerGameInfo;
        PokerResult result = gameInfo.resultPoker;
        List<PokerRank> listRank = result.ranking;
        for (PokerRank pokerRank : listRank) {
            UserScore score;
            GamePlayer gp = this.getPlayerByChair(pokerRank.chair);
            PokerPlayerInfo info = gp.spInfo.pokerInfo;
            pokerRank.finalMoney = pokerRank.totalMoneyWin;
            pokerRank.win = pokerRank.finalMoney > info.totalBet;
            if (pokerRank.finalMoney > 0L) {
                score = new UserScore();
                score.money = pokerRank.finalMoney;
                if (score.money > info.totalBet) {
                    long moneyWin = score.money - info.totalBet;
                    score.wastedMoney = (long) ((double) (moneyWin * (long) this.gameRoom.setting.commisionRate) / 100.0);
                    score.money -= score.wastedMoney;
                    score.winCount = 1;
                    if (this.gameRoom.setting.moneyType == 1) {
                        GameMoneyInfo.moneyService.addVippoint(gp.gameMoneyInfo.nickName, moneyWin, "vin");
                    }
                } else {
                    long moneyLost = info.totalBet - score.money;
                    score.lostCount = 1;
                    if (this.gameRoom.setting.moneyType == 1) {
                        GameMoneyInfo.moneyService.addVippoint(gp.gameMoneyInfo.nickName, moneyLost, "vin");
                    }
                }
                try {
                    pokerRank.finalMoney = gp.gameMoneyInfo.chargeMoneyInGame(score, this.gameRoom.getId(), this.gameMgr.gameTable.id);
                } catch (MoneyException e) {
                    CommonHandle.writeErrLog("ERROR WHEN CHARGE MONEY INGAME" + gp.gameMoneyInfo.toString());
                    gp.reqQuitRoom = true;
                }
                score.money = pokerRank.finalMoney;
                gp.spInfo.pokerInfo.currentMoney += pokerRank.finalMoney;
                this.dispatchAddEventScore(gp.getUser(), score);
                continue;
            }
            if (this.gameRoom.setting.moneyType == 1) {
                GameMoneyInfo.moneyService.addVippoint(gp.gameMoneyInfo.nickName, info.totalBet, "vin");
            }
            score = new UserScore();
            score.lostCount = 1;
            this.dispatchAddEventScore(gp.getUser(), score);
        }
    }

    private void tinhTraTien() {
        PokerGameInfo gameInfo = this.gameMgr.gameTable.pokerGameInfo;
        PokerResult result = gameInfo.resultPoker;
        List<PokerRank> listRank = result.ranking;

        LinkedList<PokerRank> winList = new LinkedList<>();
        LinkedList<PokerRank> lostList = new LinkedList<>();

        int index = 0;
        int size = listRank.size();
        while (index < size) {
            try {
                winList.clear();
                lostList.clear();
                PokerRank myRank = listRank.get(index);
                for (int i2 = index; i2 < size; ++i2) {
                    PokerRank otherRank = listRank.get(i2);
                    if (myRank.rank < otherRank.rank || otherRank.fold) {
                        lostList.add(otherRank);
                        continue;
                    }
                    winList.add(otherRank);
                }
                long winUnit = myRank.totalBet;
                int winSize = winList.size();
                long totalLost = 0L;
                for (int i3 = index; i3 < listRank.size(); ++i3) {
                    PokerRank rank2 = listRank.get(i3);
                    long lostMoney = rank2.totalBet;
                    if (rank2.totalBet > winUnit) {
                        lostMoney = winUnit;
                    }
                    rank2.totalBet -= lostMoney;
                    totalLost += lostMoney;
                }
                long winShare = totalLost;
                if (winSize != 0) {
                    winShare = (long) Math.floor((double) totalLost * 1.0 / (double) winSize);
                    for (PokerRank winRank : winList) {
                        winRank.totalMoneyWin += winShare;
                    }
                }
                ++index;
            } catch (Exception e) {
                CommonHandle.writeErrLog(e);
                CommonHandle.writeErrLog(this.gameLog.toString());
                break;
            }
        }
        Debug.trace("============================================tinhTraTien3=======================================================");
    }

    public void dispatchAddEventScore(User user, UserScore score) {
        if (user == null) {
            return;
        }

        Debug.trace("Change money user:", user.getName(), GameUtils.toJsonString(score));
        score.moneyType = this.gameRoom.setting.moneyType;
        UserScore newScore = score.clone();
        HashMap<GameEventParam, Object> evtParams = new HashMap<>();
        evtParams.put(GameEventParam.USER, user);
        evtParams.put(GameEventParam.USER_SCORE, newScore);
        ExtensionUtility.dispatchEvent(new BZEvent(GameEventType.EVENT_ADD_SCORE, evtParams));
    }

    private void sortUserRanking() {
        ArrayList<PokerRank> listRank = new ArrayList<>();
        PokerGameInfo gameInfo = this.gameMgr.gameTable.pokerGameInfo;
        PokerResult result = gameInfo.resultPoker;
        for (int i2 = 0; i2 < 9; ++i2) {
            GamePlayer gp = this.getPlayerByChair(i2);
            if (!gp.isPlaying()) continue;
            PokerPlayerInfo playerInfo = gp.spInfo.pokerInfo;
            PokerRank rank = new PokerRank();
            rank.fold = playerInfo.fold;
            rank.chair = gp.chair;
            gp.spInfo.maxCards = rank.cards = PokerRule.findMaxGroup(gp.spInfo.handCards, gameInfo.publicCard);
            rank.totalBet = playerInfo.totalBet;
            listRank.add(rank);
        }
        Collections.sort(listRank);

        int rank = 1;
        PokerRank prev = null;
        for (PokerRank current : listRank) {
            if (prev == null || PokerRule.soSanhBoBai(prev.cards, current.cards) != 0) {
                // empty if block
            }
            prev = current;
            prev.rank = ++rank;
        }
        result.ranking = listRank;
        for (int i = 0; i < result.ranking.size(); ++i) {
            PokerRank r = result.ranking.get(i);
            Debug.trace("sortUserRanking:", r);
        }
    }

    private boolean dispatchEventThangLon(GamePlayer gp, boolean isNoHu) {
        return GameUtils.dispatchEventThangLon(gp.getUser(), this.gameRoom, this.gameMgr.gameTable.id, gp.gameMoneyInfo, this.getMoneyBet(), isNoHu, gp.getHandCards());
    }

    private void kiemTraNoHuThangLon() {
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying()) continue;
            if (gp.spInfo.kiemTraNoHu()) {
                if (!this.dispatchEventThangLon(gp, true)) continue;
                this.gameMgr.countDown += 5;
                continue;
            }
            this.dispatchEventThangLon(gp, false);
        }
    }

    private void notifyKickRoom(GamePlayer gp, byte reason) {
        SendKickRoom msg = new SendKickRoom();
        msg.reason = reason;
        this.send(msg, gp.getUser());
    }

    private boolean checkMoneyPlayer(GamePlayer gp) {
        return gp.spInfo.pokerInfo.currentMoney >= 2L * this.gameRoom.setting.moneyBet;
    }

    protected synchronized void pPrepareNewGame() {
        this.tuDongBuyIn();
        SendUpdateMatch msg = new SendUpdateMatch();
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (gp.getPlayerStatus() != PlayerStatus.NO_LOGIN) {
                if (GameUtils.isMainTain || !this.coTheOLaiBan(gp) && gp.isPlaying()) {
                    if (GameUtils.isMainTain) {
                        this.notifyKickRoom(gp, (byte) 2);
                    }
                    if (gp.getUser() != null && this.gameRoom != null) {
                        GameRoom gameRoom = gp.getUser().getProperty(GAME_ROOM);
                        if (gameRoom == this.gameRoom) {
                            GameRoomManager.instance().leaveRoom(gp.getUser());
                        }
                    } else {
                        this.removePlayerAtChair(i, false);
                    }
                    msg.hasInfoAtChair[i] = false;
                } else {
                    if (!this.checkMoneyPlayer(gp)) {
                        this.requestBuyIn(gp);
                    }
                    msg.hasInfoAtChair[i] = true;
                    msg.pInfos[i] = gp;
                }
                gp.setPlayerStatus(PlayerStatus.SIT);
            } else {
                msg.hasInfoAtChair[i] = false;
            }
            gp.prepareNewGame();
        }

        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!msg.hasInfoAtChair[i]) continue;
            msg.chair = (byte) i;
            this.send(msg, gp.getUser());
        }
    }

    private synchronized void requestBuyIn(GamePlayer gp) {
        if (this.canBuyIn(gp)) {
            if (!gp.standUp) {
                ++gp.countToOutRoom;
            }
            if (this.gameMgr.gameState == GS_GAME_END) {
                this.kiemTraTuDongBatDau(2);
            }
            gp.spInfo.pokerInfo.currentMoney = 0L;
            SendRequestBuyIn msg = new SendRequestBuyIn();
            this.send(msg, gp.getUser());
        }
    }

    private void tuDongBuyIn() {
        for (int i = 0; i < 9; ++i) {
            boolean flag2;
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.hasUser()) continue;
            if (gp.countToOutRoom >= 3) {
                gp.reqQuitRoom = true;
                continue;
            }
            PokerPlayerInfo pokerPlayerInfo = gp.spInfo.pokerInfo;
            if (gp.standUp) {
                pokerPlayerInfo.currentMoney = 0L;
                continue;
            }
            GameMoneyInfo moneyInfo = gp.gameMoneyInfo;
            boolean flag1 = moneyInfo.currentMoney >= 40L * this.gameRoom.setting.moneyBet;
            flag2 = pokerPlayerInfo.currentMoney < 2L * this.gameRoom.setting.moneyBet;
            if (!flag1 || !flag2) continue;
            this.buyInAuTo(gp);
        }
    }

    private synchronized void tudongChoi1() {
        if (this.gameMgr.gameState != GS_GAME_PLAYING) {
            return;
        }
        GamePlayer gp = this.getPlayerByChair(this.gameMgr.currentChair);
        if (gp != null && !gp.getUser().isPlayer()) {
            Debug.trace("TU DONG CHOI =========================", gp.pInfo.nickName, " BEGIN =====================");
            PokerPlayerInfo pokerInfo = gp.spInfo.pokerInfo;
            Round round = this.gameMgr.gameTable.getCurrentRound();
            PokerGameInfo gameInfo = this.gameMgr.gameTable.pokerGameInfo;
            Random rd = new Random();
            boolean res = false;
            while (!res) {
                int action = Math.abs(rd.nextInt() % 5);
                RevTakeTurn cmd = new RevTakeTurn(new DataCmd(new byte[20]));
                if (action == 0) {
                    cmd.fold = true;
                }
                if (action == 1) {
                    cmd.check = true;
                }
                if (action == 2) {
                    cmd.callAll = true;
                }
                if (action == 4) {
                    cmd.allIn = true;
                }
                if (action == 3) {
                    cmd.raise = gameInfo.lastRaise + gameInfo.bigBlindMoney;
                }
                res = this.registerTakeTurn(gp.getUser(), cmd);
            }
            Debug.trace("TU DONG CHOI **********************", gp.pInfo.nickName, " END **********************");
        }
    }

    protected synchronized void tudongChoi() {
        if (this.gameMgr.gameState != GS_GAME_PLAYING) {
            return;
        }
        GamePlayer gp = this.getPlayerByChair(this.gameMgr.currentChair);
        Debug.trace("TU DONG CHOI =========================", gp.pInfo.nickName, " BEGIN =====================");
        ++gp.countToOutRoom;
        PokerPlayerInfo pokerInfo = gp.spInfo.pokerInfo;
        pokerInfo.registerFold = true;
        this.changeTurn();
        Debug.trace("TU DONG CHOI **********************", gp.pInfo.nickName, " END **********************");
    }

    protected void botAutoPlay() {
        if (!GameUtils.dev_mod) {
            return;
        }
        GamePlayer gp = this.getPlayerByChair(this.gameMgr.currentChair);
        Debug.trace("TU DONG CHOI =========================", gp.pInfo.nickName, " BEGIN =====================");
        if (this.cheatTurns.size() == 0) {
            PokerPlayerInfo pokerInfo = gp.spInfo.pokerInfo;
            pokerInfo.registerFold = true;
            this.changeTurn();
            Debug.trace("TU DONG CHOI **********************", gp.pInfo.nickName, " END **********************");
        } else {
            Turn turn = this.cheatTurns.get(this.turnIndex % this.cheatTurns.size());
            Debug.trace("Turn:", this.turnIndex, turn.chair, turn.action, turn.raiseAmount);
            ++this.turnIndex;
            PokerPlayerInfo pokerInfo = gp.spInfo.pokerInfo;
            pokerInfo.register(turn);
            this.changeTurn();
            Debug.trace("TU DONG CHOI ********************************************", "**");
        }
    }

    private void pCheatCards(DataCmd dataCmd) {
        if (!GameUtils.isCheat) {
            return;
        }
        RevCheatCard cmd = new RevCheatCard(dataCmd);
        if (cmd.isCheat) {
            this.configGame(cmd.cards, cmd.moneyArray, cmd.chair);
        } else {
            this.gameMgr.gameTable.isCheat = false;
            this.gameMgr.gameTable.suit.initCard();
        }
    }

    private void configGame(byte[] cards, long[] moneyArray, int dealer) {
        this.gameMgr.gameTable.isCheat = true;
        this.gameMgr.gameTable.suit.setOrder(cards);
        this.gameMgr.gameTable.moneyArray = moneyArray;
        this.gameMgr.gameTable.dealer = dealer;
    }

    private void pDangKyChoiTiep(User user) {
        GamePlayer gp = this.getPlayerByUser(user);
        if (gp != null) {
            gp.choiTiepVanSau = true;
        }
    }

    @Override
    public synchronized void onNoHu(ThongTinThangLon info) {
        this.thongTinNoHu = info;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected void notifyNoHu() {
        try {
            if (this.thongTinNoHu != null) {
                for (int i = 0; i < 9; ++i) {
                    GamePlayer gp = this.getPlayerByChair(i);
                    if (!gp.gameMoneyInfo.sessionId.equalsIgnoreCase(this.thongTinNoHu.moneySessionId) || !gp.gameMoneyInfo.nickName.equalsIgnoreCase(this.thongTinNoHu.nickName))
                        continue;
                    gp.gameMoneyInfo.currentMoney = this.thongTinNoHu.currentMoney;
                    break;
                }
                SendNoHu msg = new SendNoHu();
                msg.info = this.thongTinNoHu;
                for (Map.Entry entry : this.gameRoom.userManager.entrySet()) {
                    User u = (User) entry.getValue();
                    if (u == null) continue;
                    this.send(msg, u);
                }
            }
        } catch (Exception e) {
            CommonHandle.writeErrLog(e);
        } finally {
            this.thongTinNoHu = null;
        }
    }

    @Override
    public void choNoHu(String nickName) {
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (gp.getUser() == null || !gp.getUser().getName().equalsIgnoreCase(nickName)) continue;
            this.gameMgr.gameTable.suit.noHuAt(gp.chair);
        }
    }

    @Override
    protected Runnable getGameLoopTask() {
        return gameMgr::gameLoop;
    }

    protected void selectDealer() {
        SendSelectDealer msg = new SendSelectDealer();
        PokerGameInfo pokerGameInfo = this.gameMgr.gameTable.pokerGameInfo;
        int from = (pokerGameInfo.dealer + 1) % 9;
        if (this.gameMgr.gameTable.isCheat) {
            from = this.gameMgr.gameTable.dealer < 0 ? from : this.gameMgr.gameTable.dealer;
            for (int i = 0; i < 9; ++i) {
                GamePlayer gp = this.getPlayerByChair(i);
                if (!gp.isPlaying()) continue;
                this.gameMgr.gameTable.moneyArray[i] = gp.spInfo.pokerInfo.currentMoney = this.gameMgr.gameTable.moneyArray[i] == 0L ? gp.spInfo.pokerInfo.currentMoney : this.gameMgr.gameTable.moneyArray[i];
            }
        }

        int newDealer = -1;
        int newSmallBlind = -1;
        int newBigBlind = -1;
        int startChair = -1;
        int i = from;
        while (startChair == -1 && i < 20) {
            int currentChair = i % 9;
            GamePlayer gp = this.getPlayerByChair(currentChair);
            if (gp.isPlaying()) {
                if (newDealer == -1) {
                    newDealer = currentChair;
                } else if (newSmallBlind == -1) {
                    newSmallBlind = currentChair;
                } else if (newBigBlind == -1) {
                    newBigBlind = currentChair;
                } else {
                    startChair = currentChair;
                }
            }
            ++i;
        }

        this.gameMgr.currentChair = startChair;
        pokerGameInfo.dealer = newDealer;
        pokerGameInfo.smallBlind = newSmallBlind;
        pokerGameInfo.bigBlind = newBigBlind;
        pokerGameInfo.bigBlindMoney = 2L * this.gameRoom.setting.moneyBet;

        // pokerGameInfo.potMoney = this.room.setting.moneyBet + pokerGameInfo.bigBlindMoney;
        this.gameLog.append("SD<");
        this.gameLog.append("dl/").append(pokerGameInfo.dealer).append(";");
        this.gameLog.append("sb/").append(pokerGameInfo.smallBlind).append(";");
        this.gameLog.append("bb/").append(pokerGameInfo.bigBlind).append(">");
        this.initSmallAndBigBlind(msg);
        this.notifyDealder(msg);
    }

    private void initSmallAndBigBlind(SendSelectDealer msg) {
        KingUtil.printLog("initSmallAndBigBlind() BEGIN");
        Debug.trace("============VAO CUOC SMALL BLIND:", "begin");
        PokerGameInfo pokerGameInfo = this.gameMgr.gameTable.pokerGameInfo;
        GamePlayer sPlayer = this.getPlayerByChair(pokerGameInfo.smallBlind);
        PokerPlayerInfo smallBlindInfo = sPlayer.spInfo.pokerInfo;
        if (!sPlayer.requireBigBlind) {
            smallBlindInfo.registerMoney(this.gameRoom.setting.moneyBet);
        } else {
            smallBlindInfo.registerMoney(2L * this.gameRoom.setting.moneyBet);
            sPlayer.requireBigBlind = false;
            msg.requireBigBlinds[sPlayer.chair] = true;
        }
        Round round = this.gameMgr.gameTable.getCurrentRound();
        Turn turn = smallBlindInfo.take(pokerGameInfo, round, true);
        this.logTakeTurn(sPlayer, turn);
        GamePlayer bPlayer = this.getPlayerByChair(pokerGameInfo.bigBlind);
        PokerPlayerInfo bigBlindInfo = bPlayer.spInfo.pokerInfo;
        bigBlindInfo.registerMoney(2L * this.gameRoom.setting.moneyBet);
        if (bPlayer.requireBigBlind) {
            bPlayer.requireBigBlind = false;
            msg.requireBigBlinds[bPlayer.chair] = true;
        }
        turn = bigBlindInfo.take(pokerGameInfo, round, true);
        this.logTakeTurn(bPlayer, turn);
        Debug.trace("============VAO CUOC SMALL BLIND:", "end");
        Debug.trace("============VAO CUOC REQUIRE BIG BLIND:", "begin");
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying() || !gp.requireBigBlind) continue;
            PokerPlayerInfo info = gp.spInfo.pokerInfo;
            info.registerMoney(2L * this.gameRoom.setting.moneyBet);
            gp.requireBigBlind = false;
            msg.requireBigBlinds[gp.chair] = true;
            turn = info.take(pokerGameInfo, round, true);
            ++round.requireBigBlindCount;
            this.logTakeTurn(gp, turn);
        }
        Debug.trace("============VAO CUOC REQUIRE BIG BLIND:", "end");
        KingUtil.printLog("initSmallAndBigBlind() END ");
    }

    private void notifyDealder(SendSelectDealer msg) {
        PokerGameInfo pokerGameInfo = this.gameMgr.gameTable.pokerGameInfo;
        msg.smallBlind = pokerGameInfo.smallBlind;
        msg.bigBlind = pokerGameInfo.bigBlind;
        msg.dealer = pokerGameInfo.dealer;
        msg.gameId = this.gameMgr.gameTable.id;
        if (this.gameMgr.gameTable.isCheat) {
            msg.isCheat = true;
        }
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.hasUser()) continue;
            msg.hasInfoAtChair[i] = true;
            msg.gamePlayers[i] = gp;
            msg.moneyArray[i] = gp.spInfo.pokerInfo.currentMoney;
        }
        this.send(msg);
    }

    protected void newRound() {
        Round round = this.gameMgr.gameTable.getCurrentRound();
        if (round.roundId == 3) {

            this.endGame();
            return;
        }

        round = this.gameMgr.gameTable.makeRound();
        Debug.trace("======================NEW ROUND", round.roundId, "=========================");
        if (round.roundId != 0) {
            this.gameMgr.currentChair = this.findFirstChairNewRound();
        }
        PokerGameInfo pokerGameInfo = this.gameMgr.gameTable.pokerGameInfo;
        this.gameLog.append("NR<");
        this.gameLog.append(round.roundId).append(";");
        this.gameLog.append(pokerGameInfo.getGroupCardPublic(round.roundId)).append(">");
        this.gomTienVaoHu();
        this.notifyNewRound(round);
    }

    private int findFirstChairNewRound() {
        PokerGameInfo pokerGameInfo = this.gameMgr.gameTable.pokerGameInfo;
        int from = pokerGameInfo.smallBlind;
        int startChair = -1;
        int i = from;
        while (true) {
            int currentChair = i % 9;
            GamePlayer gp = this.getPlayerByChair(currentChair);
            if (gp.isPlaying()) {
                PokerPlayerInfo info = gp.spInfo.pokerInfo;
                if (!info.fold && !info.allIn) {
                    startChair = currentChair;
                    break;
                }
            }
            ++i;
        }
        Debug.trace("findFirstChairNewRound", startChair);
        return startChair;
    }

    private void gomTienVaoHu() {
        PokerGameInfo gameInfo = this.gameMgr.gameTable.pokerGameInfo;
        long totalBet = 0L;
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying()) continue;
            PokerPlayerInfo info = gp.spInfo.pokerInfo;
            totalBet += info.moneyBet;
            info.totalBet += info.moneyBet;
            info.clearNewRound();
        }
        gameInfo.clearNewRound();
    }

    private void notifyNewRound(Round round) {
        PokerGameInfo gameInfo = this.gameMgr.gameTable.pokerGameInfo;
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying()) continue;
            SendNewRound msg = new SendNewRound();
            msg.potMoney = gameInfo.potMoney;
            msg.roundId = round.roundId;
            msg.cards = gameInfo.getPublicCard(round.roundId);
            byte[] publicCard = gameInfo.getCurrentPublicCard(round.roundId);
            GroupCard gc = PokerRule.findMaxGroup(gp.spInfo.handCards, new GroupCard(publicCard));
            msg.cards_name = gc.kiemtraBo().id;
            this.send(msg, gp.getUser());
        }
        this.gameMgr.nextAction = CHANGE_TURN;
        this.gameMgr.countDown = 1;
    }

    protected synchronized void changeTurn() {
        Turn turn = this.takeTurn();
        if (turn == null) {
            this.notifyChangeTurn();
        }
    }

    private void notifyChangeTurn() {
        this.gameMgr.countDown = 20;
        this.gameMgr.nextAction = IN_ROUND;
        SendChangeTurn msg = new SendChangeTurn();
        Round round = this.gameMgr.gameTable.getCurrentRound();
        msg.roundId = round.roundId;
        msg.curentChair = this.gameMgr.currentChair;
        msg.countDownTime = this.gameMgr.countDown;
        PokerGameInfo gameInfo = this.gameMgr.gameTable.pokerGameInfo;
        msg.potMoney = gameInfo.potMoney;
        Debug.trace("notifyChangeTurn", GameUtils.toJsonString((msg)));
        this.send(msg);
    }

    protected synchronized Turn takeTurn() {
        PokerGameInfo potInfo = this.gameMgr.gameTable.pokerGameInfo;
        Round round = this.gameMgr.gameTable.getCurrentRound();
        GamePlayer gp = this.getPlayerByChair(this.gameMgr.currentChair);
        PokerPlayerInfo pokerInfo = gp.spInfo.pokerInfo;
        Turn turn = pokerInfo.takeTurn(potInfo, round);
        if (turn != null) {
            pokerInfo.clearNewTurn();
            this.notifyTakeTurn(turn);
            if (this.checkEndGameByFoldAndAllIn()) {
                this.endGame();
            } else if (!this.checkNewRound()) {
                this.gameMgr.nextAction = CHANGE_TURN;
                this.gameMgr.countDown = 1;
            }
        }
        return turn;
    }

    private void notifyTakeTurn(Turn turn) {
        GamePlayer gp = this.getPlayerByChair(this.gameMgr.currentChair);
        if (turn.action == FOLD) {
            gp.spInfo.pokerInfo.fold = true;
        }
        PokerGameInfo gameInfo = this.gameMgr.gameTable.pokerGameInfo;
        PokerPlayerInfo pokerInfo = gp.spInfo.pokerInfo;
        SendTakeTurn msg = new SendTakeTurn();
        msg.action = turn.action;
        msg.chair = this.gameMgr.currentChair;
        msg.currentBet = pokerInfo.moneyBet;
        msg.currentMoney = pokerInfo.currentMoney;
        msg.maxBet = gameInfo.maxBetMoney;
        msg.raiseAmount = turn.raiseAmount;
        msg.raiseStep = Math.max(gameInfo.lastRaise, gameInfo.bigBlindMoney);
        msg.raiseBlock = gameInfo.raiseBlock;

        Debug.trace("notifyTakeTurn:", GameUtils.toJsonString((msg)));
        this.findNextChair();
        this.logTakeTurn(gp, turn);
        this.send(msg);
        KingUtil.printLog("notifyTakeTurn() msg: " + msg);
    }

    private void logTakeTurn(GamePlayer gp, Turn turn) {
        this.gameLog.append("TT<");
        this.gameLog.append(gp.chair).append(";");
        this.gameLog.append(turn.action).append(";-");
        this.gameLog.append(turn.raiseAmount);
        this.gameLog.append(">");
    }

    private boolean checkEndGameByFoldAndAllIn() {
        KingUtil.printLog("checkEndGameByFoldAndAllIn() loop");
        Debug.trace("checkEndGameByFoldAndAllIn");
        PokerGameInfo potInfo = this.gameMgr.gameTable.pokerGameInfo;
        int foldCount = 0;
        int allInCount = 0;
        int followCount = 0;
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying()) continue;
            KingUtil.printLog("gp: " + gp.spInfo.pokerInfo);
            if (gp.spInfo.pokerInfo.allIn) {
                ++allInCount;
                continue;
            }
            if (gp.spInfo.pokerInfo.fold) {
                ++foldCount;
                continue;
            }
            if (gp.spInfo.pokerInfo.moneyBet != potInfo.maxBetMoney) continue;
            ++followCount;
        }
        if (allInCount == this.playingCount - foldCount || allInCount == this.playingCount - foldCount - 1 && followCount == 1) {
            Debug.trace("Tat ca all in", "allInCount=", allInCount, "playingCount=", this.playingCount, "foldCount=", foldCount);
            return true;
        }
        if (foldCount == this.playingCount - 1) {
            Debug.trace("Tat ca up bai", "allInCount=", allInCount, "playingCount=", this.playingCount, "foldCount=", foldCount);
            return true;
        }
        return false;
    }

    private boolean checkNewRound() {
        Round round = this.gameMgr.gameTable.getCurrentRound();
        PokerGameInfo potInfo = this.gameMgr.gameTable.pokerGameInfo;
        int allInCount = 0;
        int foldCount = 0;
        int callCount = 0;
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying()) continue;
            PokerPlayerInfo info = gp.spInfo.pokerInfo;
            if (info.fold) {
                ++foldCount;
                continue;
            }
            if (info.allIn) {
                ++allInCount;
                continue;
            }
            if (info.moneyBet != potInfo.maxBetMoney) continue;
            ++callCount;
        }
        boolean flag;
        int size = round.turns.size();
        if (round.roundId == 0) {
            flag = size >= this.playingCount + 2 + round.requireBigBlindCount;
        } else {
            flag = size >= this.playingCount - this.gameMgr.gameTable.stopInRound;
        }

        if (flag && allInCount + foldCount + callCount == this.playingCount) {
            this.gameMgr.nextAction = CHANGE_ROUND;
            this.gameMgr.countDown = 1;
            this.gameMgr.gameTable.stopInRound = foldCount + allInCount;
            return true;
        }
        return false;
    }

    private void findNextChair() {
        int from;
        for (int i = from = this.gameMgr.currentChair + 1; i < 9 + from; ++i) {
            int currentChair = i % 9;
            GamePlayer gp = this.getPlayerByChair(currentChair);
            if (!gp.isPlaying() || gp.spInfo.pokerInfo.fold || gp.spInfo.pokerInfo.allIn) continue;
            this.gameMgr.currentChair = currentChair;
            Debug.trace("Find next chair", this.gameMgr.currentChair);
            break;
        }
    }

    @Override
    public String toString() {
        try {
            JSONObject json = this.toJONObject();
            if (json != null) {
                return json.toString();
            }
            return "{}";
        } catch (Exception e) {
            return "{}";
        }
    }

    @Override
    public JSONObject toJONObject() {
        try {
            JSONObject json = new JSONObject();
            if (this.gameMgr.gameTable.pokerGameInfo.publicCard != null) {
                json.put("pokerGameInfo", this.gameMgr.gameTable.pokerGameInfo.publicCard.toString());
            }
            json.put("gameState", this.gameMgr.gameState.id);
            json.put("gameAction", this.gameMgr.nextAction.id);
            JSONArray arr = new JSONArray();
            for (int i = 0; i < 9; ++i) {
                GamePlayer gp = this.getPlayerByChair(i);
                arr.put(gp.toJSONObject());
            }
            json.put("players", arr);
            return json;
        } catch (Exception e) {
            return null;
        }
    }
}
