package game.poker.server;

public enum GameState {
    GS_NO_START(0), GS_GAME_PLAYING(1), GS_GAME_END(3);

    final byte id;

    GameState(int i) {
        id = (byte) i;
    }
}
