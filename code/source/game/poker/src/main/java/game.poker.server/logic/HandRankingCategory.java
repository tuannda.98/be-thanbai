package game.poker.server.logic;

public enum HandRankingCategory {
    EG_SANH_VUA(0),
    EG_THUNGPHASANH(1),
    EG_TUQUY(2),
    EG_CULU(3),
    EG_THUNG(4),
    EG_SANH(5),
    EG_XAMCHI(6),
    EG_2DOIKHACNHAU(7),
    EG_MOTDOI(8),
    EG_RAC(9),

    NO_GROUP(10),
    NONE(11);

    public final byte id;

    HandRankingCategory(int id) {
        this.id = (byte) id;
    }
}