package game.poker.server;

public enum PlayerStatus {
    NO_LOGIN(0), VIEW(1), SIT(2), PLAY(3);

    public final int id;

    PlayerStatus(int i) {
        id = (byte) i;
    }
}
