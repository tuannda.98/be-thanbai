/*
 * Decompiled with CFR 0.144.
 *
 * Could not load the following classes:
 *  bitzero.server.extensions.data.BaseMsg
 */
package game.poker.server;

import bitzero.util.common.business.Debug;
import game.poker.server.cmd.send.SendUpdateAutoStart;
import game.poker.server.logic.GameTable;
import game.poker.server.logic.GroupCard;

import java.util.List;

import static game.poker.server.GameAction.*;
import static game.poker.server.GameState.GS_NO_START;

public class GameManager {
    public static final int DEM_LA = 1;

    public int roomOwnerChair = 9;
    public int roomCreatorUserId;
    public int rCuocLon = 1;

    public volatile GameState gameState = GS_NO_START;
    public volatile GameAction nextAction = NO_ACTION;

    public volatile int countDown = 0;
    public volatile boolean isAutoStart = false;
    public volatile int currentChair = -1;
    public volatile int lastRaiseChair = -1;
    public final GameTable gameTable;

    public final PokerGameServer gameServer;

    public GameManager(PokerGameServer gameServer) {
        this.gameServer = gameServer;
        this.gameTable = new GameTable();
    }

    public int getGameState() {
        return this.gameState.id;
    }

    private void prepareNewGame() {
        this.gameTable.reset();
        this.isAutoStart = false;
        this.gameServer.kiemTraTuDongBatDau(2);
    }

    public void gameLoop() {
        switch (this.gameState) {
            case GS_NO_START:
                if (this.isAutoStart) {
                    --this.countDown;
                    if (this.countDown <= 0) changeGameState(GameState.GS_GAME_PLAYING);
                } else {

                    if (this.gameServer.playerCount > 1) this.gameServer.kiemTraTuDongBatDau(2);
                }
                break;
            case GS_GAME_PLAYING:
                --this.countDown;

                if (this.nextAction == IN_ROUND && this.countDown == 19) this.botPlay();

                if (this.countDown <= 0) serverAct();
                break;
            case GS_GAME_END:
                --this.countDown;

                if (this.countDown == 5) this.gameServer.notifyNoHu();

                if (this.countDown <= 0) changeGameState(GS_NO_START);
                break;
            default:
                Debug.warn("changeGameState do not handle " + gameState);
                break;
        }

    }

    private void changeGameState(GameState gameState) {
        info("changeGameState to: " + gameState);
        this.gameState = gameState;
        switch (this.gameState) {

            case GS_NO_START:
                this.gameServer.pPrepareNewGame();
                this.prepareNewGame();
                break;

            case GS_GAME_PLAYING:
                this.isAutoStart = false;
                this.gameServer.start();
                this.nextAction = SELECT_DEALER;
                this.countDown = 0;
                break;

            case GS_GAME_END:
                this.gameServer.endGame();
                break;

            default:
                Debug.warn("changeGameState do not handle " + gameState);
                break;
        }
    }

    protected void serverAct() {
        info("act: " + this.nextAction);
        switch (this.nextAction) {
            case SELECT_DEALER:
                this.gameServer.selectDealer();
                this.nextAction = DEAL_PRIVATE_CARD;
                this.countDown = 3;
                break;
            case DEAL_PRIVATE_CARD:
                this.chiaBai();
                this.nextAction = CHANGE_TURN;
                this.countDown = (int) Math.ceil(0.5 * (double) this.gameServer.playingCount) + 2;
                break;
            case CHANGE_TURN:
                this.gameServer.changeTurn();
                break;
            case IN_ROUND:
                this.gameServer.tudongChoi();
                break;
            case CHANGE_ROUND:
                this.gameServer.newRound();
                break;
            default:
                Debug.warn("act do not handle " + this.nextAction);
                break;
        }
    }

    private void notifyAutoStartToUsers(int after) {
        SendUpdateAutoStart msg = new SendUpdateAutoStart();
        msg.isAutoStart = this.isAutoStart;
        msg.autoStartTime = (byte) after;
        this.gameServer.send(msg);
    }

    public void cancelAutoStart() {
        this.isAutoStart = false;
        this.notifyAutoStartToUsers(0);
    }

    public void makeAutoStart(int after) {
        if (this.gameState != GS_NO_START) return;

        if (!this.isAutoStart || after < this.countDown) {
            this.countDown = after;
        }
        this.isAutoStart = true;
        this.notifyAutoStartToUsers(this.countDown);
    }

    private void chiaBai() {
        List<GroupCard> cards = this.gameTable.suit.dealCards();
        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.gameServer.playerList.get(i);
            gp.addCards(cards.get(i));
        }
        this.gameTable.pokerGameInfo.addPublicCard(cards.get(9));
        this.gameServer.chiabai();
    }

    private void botPlay() {
        this.gameServer.botAutoPlay();
    }

    public int currentChair() {
        return this.currentChair;
    }

    public boolean canOutRoom() {
        return this.getGameState() == 0;
    }

    private void info(Object... objs) {
        Debug.info(GameManager.class, ": ", gameTable.id, ": ", objs);
    }
}
