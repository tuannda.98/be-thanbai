package game.poker.server;

public enum GameAction {
    NO_ACTION(-1), SELECT_DEALER(0), DEAL_PRIVATE_CARD(1), DEAL_COMMUNITY_CARD(2), CHANGE_ROUND(3), IN_ROUND(4), CHANGE_TURN(5);

    final byte id;

    GameAction(int i) {
        id = (byte) i;
    }
}
