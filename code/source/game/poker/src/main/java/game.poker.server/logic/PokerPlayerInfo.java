/*
 * Decompiled with CFR 0.144.
 *
 * Could not load the following classes:
 *  bitzero.server.entities.User
 *  bitzero.util.common.business.CommonHandle
 *  bitzero.util.common.business.Debug
 *  game.entities.PlayerInfo
 *  game.entities.UserScore
 *  game.modules.gameRoom.entities.GameMoneyInfo
 *  game.modules.gameRoom.entities.GameRoom
 *  game.modules.gameRoom.entities.MoneyException
 *  game.utils.GameUtils
 */
package game.poker.server.logic;

import bitzero.util.common.business.CommonHandle;
import bitzero.util.common.business.Debug;
import casio.king365.util.KingUtil;
import game.entities.UserScore;
import game.modules.gameRoom.entities.MoneyException;
import game.poker.server.GamePlayer;
import game.poker.server.cmd.receive.RevTakeTurn;
import game.utils.GameUtils;

import java.util.LinkedList;
import java.util.List;

public class PokerPlayerInfo {
    public boolean registerCall;
    public boolean registerCallAll;
    public boolean registerCheck;
    public boolean registerFold;
    public boolean allIn;
    public boolean fold;
    public long registerMoney;
    public List<Turn> listTurn = new LinkedList<>();
    public long moneyBet = 0L;
    public long totalBet = 0L;
    public long currentMoney = 0L;
    public long lastBuyInMoney = 0L;
    public GamePlayer gamePlayer;

    public PokerPlayerInfo(GamePlayer gp) {
        this.gamePlayer = gp;
        this.clearOutGame();
    }

    public void clearNewRound() {
        this.registerCall = false;
        this.registerCheck = false;
        this.registerCallAll = false;
        this.registerMoney = 0L;
        this.moneyBet = 0L;
    }

    public void clearNewGame() {
        this.registerCall = false;
        this.registerCheck = false;
        this.registerCallAll = false;
        this.registerFold = false;
        this.registerMoney = 0L;
        this.listTurn.clear();
        this.moneyBet = 0L;
        this.totalBet = 0L;
        this.fold = false;
        this.allIn = false;
    }

    public void clearOutGame() {
        this.clearNewGame();
        this.currentMoney = 0L;
    }

    public void registerFold() {
        this.registerFold = true;
    }

    public void registerCheck() {
        this.registerCheck = true;
    }

    public void cancelCheck() {
        this.registerCheck = false;
    }

    public void cancelFold() {
        this.registerFold = false;
    }

    public void registerMoney(long amount) {
        this.registerMoney = amount;
        if (this.moneyBet + this.registerMoney > this.currentMoney) {
            this.registerMoney = this.currentMoney - this.moneyBet;
        }
    }

    public void allIn() {
        this.registerMoney = this.currentMoney - this.moneyBet;
    }

    public Turn takeTurn(PokerGameInfo potInfo, Round round) {
        if (this.registerCallAll) {
            this.registerMoney = potInfo.maxBetMoney - this.moneyBet;
            if (this.registerMoney == 0L) {
                this.registerCheck = true;
            } else if (this.registerMoney > this.currentMoney) {
                this.allIn = true;
            }
        }
        if (this.registerCheck) {
            if (this.moneyBet == potInfo.maxBetMoney) {
                Debug.trace("Dang ky check", this.moneyBet, potInfo.maxBetMoney);
                return this.check(potInfo, round);
            }
            Debug.trace("Dang ky check khong du tien cuoc", this.moneyBet, potInfo.maxBetMoney);
            return null;
        }
        if (this.registerFold || this.fold) {
            return this.fold(potInfo, round);
        }
        if (this.registerCall) {
            if (this.registerMoney != potInfo.maxBetMoney - this.moneyBet) {
                this.registerMoney = potInfo.maxBetMoney - this.moneyBet;
            }
        } else if (this.allIn) {
            this.registerMoney = this.currentMoney;
        }
        boolean flag = this.registerMoney > 0L || this.registerMoney == 0L && this.currentMoney == 0L;
        if (flag && this.registerMoney <= this.currentMoney) {
            return this.take(potInfo, round, false);
        }
        return null;
    }

    public Turn fold(PokerGameInfo potInfo, Round round) {
        Debug.trace("fold1: PokerInfo=", this.toString());
        Debug.trace("fold1: PotInfo=", GameUtils.toJSONObject(potInfo));
        Turn turn = Turn.makeTurn(this, 0, 0L);
        round.addTurn(turn);
        this.listTurn.add(turn);
        Debug.trace("fold2: PokerInfo=", this.toString());
        Debug.trace("fold3: PotInfo=", GameUtils.toJSONObject(potInfo));
        return turn;
    }

    public Turn check(PokerGameInfo potInfo, Round round) {
        Debug.trace("check");
        Turn turn = Turn.makeTurn(this, 1, 0L);
        round.addTurn(turn);
        return turn;
    }

    public Turn allIn(PokerGameInfo potInfo, Round round, long newMoneyBet) {
        Debug.trace("allIn1: PokerInfo=", this.toString());
        Debug.trace("allIn1: PotInfo=", GameUtils.toJSONObject(potInfo));
        Turn turn = Turn.makeTurn(this, 4, this.registerMoney);
        round.addTurn(turn);
        this.listTurn.add(turn);
        this.allIn = true;
        potInfo.raiseBlock = false;
        this.moneyBet = newMoneyBet;
        if (this.moneyBet > potInfo.maxBetMoney) {
            potInfo.maxBetMoney = this.moneyBet;
            potInfo.lastRaise = this.moneyBet;
        }
        this.currentMoney = 0L;
        this.registerMoney = 0L;
        Debug.trace("allIn2: PokerInfo=", this.toString());
        Debug.trace("allIn2: PotInfo=", GameUtils.toJSONObject(potInfo));
        return turn;
    }

    public Turn call(PokerGameInfo potInfo, Round round, long newMoneyBet) {
        Debug.trace("follow1: PokerInfo=", this.toString());
        Debug.trace("follow2: PotInfo=", GameUtils.toJSONObject(potInfo));
        Turn turn = Turn.makeTurn(this, 2, this.registerMoney);
        round.addTurn(turn);
        this.listTurn.add(turn);
        this.moneyBet = newMoneyBet;
        potInfo.maxBetMoney = newMoneyBet;
        this.currentMoney -= this.registerMoney;
        this.registerMoney = 0L;
        Debug.trace("follow2: PokerInfo=", this.toString());
        Debug.trace("follow2: PotInfo=", GameUtils.toJSONObject(potInfo));
        return turn;
    }

    public Turn raise(PokerGameInfo potInfo, Round round, long newMoneyBet) {
        Debug.trace("raise1: PokerInfo=", this.toString());
        Debug.trace("raise1: PotInfo=", GameUtils.toJSONObject(potInfo));
        Turn turn = Turn.makeTurn(this, 3, this.registerMoney);
        round.addTurn(turn);
        this.listTurn.add(turn);
        potInfo.lastRaise = this.moneyBet = newMoneyBet;
        potInfo.maxBetMoney = newMoneyBet;
        this.currentMoney -= this.registerMoney;
        this.registerMoney = 0L;
        Debug.trace("raise2: PokerInfo=", this.toString());
        Debug.trace("raise2: PotInfo=", GameUtils.toJSONObject(potInfo));
        return turn;
    }

    public Turn take(PokerGameInfo potInfo, Round round, boolean isBlind) {
        boolean flag;
        long money = this.registerMoney;
        if (this.registerMoney > 0L) {
            this.registerMoney = this.chargeMoney(this.registerMoney);
        }
        KingUtil.printLog("PokerPlayerInfo take(), potInfo.potMoney 1: " + potInfo.potMoney);
        potInfo.potMoney += this.registerMoney;
        KingUtil.printLog("PokerPlayerInfo take(), potInfo.potMoney 2: " + potInfo.potMoney);
        long newMoneyBet = this.moneyBet + this.registerMoney;
        if (this.registerMoney == 0L && this.currentMoney == 0L) {
            this.allIn = true;
        }
        if ((this.allIn || money != this.registerMoney) && this.registerMoney > 0L) {
            return this.allIn(potInfo, round, newMoneyBet);
        }
        if (newMoneyBet == potInfo.maxBetMoney) {
            return this.call(potInfo, round, newMoneyBet);
        }
        boolean bl = flag = isBlind || newMoneyBet >= potInfo.maxBetMoney + potInfo.bigBlindMoney;
        if (!potInfo.raiseBlock && newMoneyBet >= potInfo.maxBetMoney + potInfo.lastRaise && flag) {
            return this.raise(potInfo, round, newMoneyBet);
        }
        return this.fold(potInfo, round);
    }

    public long chargeMoney(long money) {
        if (money <= 0L) {
            return 0L;
        }
        UserScore score = new UserScore();
        score.moneyType = this.gamePlayer.gameMoneyInfo.moneyType;
        score.money = -money;
        try {
            score.money = this.gamePlayer.gameMoneyInfo.chargeMoneyInGame(score, this.gamePlayer.gameServer.getGameRoom().getId(), this.gamePlayer.gameServer.gameMgr.gameTable.id);
            this.gamePlayer.gameServer.dispatchAddEventScore(this.gamePlayer.getUser(), score);
            return -score.money;
        } catch (MoneyException e) {
            CommonHandle.writeErrLog("ERROR WHEN CHARGE MONEY INGAME" + this.gamePlayer.gameMoneyInfo.toString());
            this.gamePlayer.reqQuitRoom = true;
            return 0L;
        }
    }

    @Override
    public String toString() {
        return "PokerPlayerInfo{" +
                "registerCall=" + registerCall +
                ", registerCallAll=" + registerCallAll +
                ", registerCheck=" + registerCheck +
                ", registerFold=" + registerFold +
                ", allIn=" + allIn +
                ", fold=" + fold +
                ", registerMoney=" + registerMoney +
                ", listTurn=" + listTurn +
                ", moneyBet=" + moneyBet +
                ", totalBet=" + totalBet +
                ", currentMoney=" + currentMoney +
                ", lastBuyInMoney=" + lastBuyInMoney +
                ", gamePlayer=" + gamePlayer +
                '}';
    }

    public boolean register(RevTakeTurn cmd, PokerGameInfo potInfo) {
        if (this.fold || this.allIn) {
            Debug.trace("Da fold hoac all in truoc do");
            return false;
        }
        this.registerFold = cmd.fold;
        this.registerCheck = cmd.check;
        this.registerCallAll = cmd.callAll;
        this.registerCall = cmd.follow;
        this.allIn = cmd.allIn;
        if (cmd.raise > 0L) {
            if (cmd.raise + this.moneyBet < potInfo.maxBetMoney + potInfo.lastRaise) {
                Debug.trace("Vi pham quy tac raise: cmd.raise, info.moneyBet, potInfo.maxBetMoney, potInfo.lastRaise",
                        cmd.raise, this.moneyBet, potInfo.maxBetMoney, potInfo.lastRaise);
                return false;
            }
            if (cmd.raise > this.currentMoney) {
                Debug.trace("Raise qua so tien minh co:", cmd.raise, this.currentMoney);
                return false;
            }
            this.registerMoney = cmd.raise;
        }
        return true;
    }

    public byte getStatus() {
        if (this.fold) {
            return 1;
        }
        if (this.allIn) {
            return 2;
        }
        return 3;
    }

    public void clearNewTurn() {
        this.registerCall = false;
        this.registerCheck = false;
        this.registerCallAll = false;
    }

    public void register(Turn turn) {
        if (turn.action == 0) {
            this.registerFold = true;
        }
        if (turn.action == 1) {
            this.registerCheck = true;
        }
        if (turn.action == 2) {
            this.registerCallAll = true;
        }
        if (turn.action == 3) {
            this.registerMoney = turn.raiseAmount;
        }
        if (turn.action == 4) {
            this.allIn = true;
        }
    }
}
