package game.poker.server;

import bitzero.server.BitZeroServer;
import bitzero.server.api.APIManager;
import bitzero.server.entities.User;
import bitzero.server.entities.managers.IExtensionManager;
import bitzero.server.extensions.BaseBZExtension;
import game.modules.gameRoom.entities.*;
import game.utils.GameUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static game.poker.server.GameAction.DEAL_PRIVATE_CARD;
import static game.poker.server.GameAction.SELECT_DEALER;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class GameManagerTest {

    @InjectMocks
    private static GameManager gameManager;

    private static PokerGameServer gameServer;

    private static GameRoom gameRoom;

    private static BitZeroServer bitZeroServer = spy(BitZeroServer.getInstance());

    private static ListGameMoneyInfo listGameMoneyInfo;

    private static GameRoomManager gameRoomManager;

    private static BaseBZExtension baseBZExtension;

    static {
        try {
            gameRoom = new GameRoom(new GameRoomSetting(new JSONObject("{\n" +
                    "        'moneyType':1,\n" +
                    "        'moneyBet':200000,\n" +
                    "        'requiredMoney':0,\n" +
                    "        'outMoney':0,\n" +
                    "        'maxUserPerRoom':9,\n" +
                    "        'commisionRate':2,\n" +
                    "        'rule':0,\n" +
                    "\t\t'numberOfInitialRoom':5, \n" +
                    "\t\t'roomName':'San Bang Tat Ca'\n" +
                    "    }")), 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private static List<User> users = new ArrayList<>();

    @BeforeAll
    static void setup() {
        PokerGameServer gameServer = spy(new PokerGameServer(gameRoom));
        // Mock scope
        try (MockedStatic mocked = mockStatic(GameServer.class)) {

            // Mocking
            mocked.when(() -> GameServer.createNewGameServer(gameRoom)).thenReturn(gameServer);
        }

        gameManager = spy(new GameManager(gameServer));
        gameServer.gameMgr = gameManager;
        try (MockedStatic mocked = mockStatic(GameUtils.class)) {

            // Mocking
            mocked.when(() -> GameUtils.logStartGame(anyInt(), anyString(), anyLong(), anyInt())).then(invocation -> {
                System.out.println("Invoke " + invocation.getMethod());
                return null;
            });
        }

        for (int i = 0; i < 2; i++) {
            User user = new User("test_" + i, null);
            user.setConnected(true);

            GameUtils.enable_payment = false;
            GameMoneyInfo moneyInfo = mock(GameMoneyInfo.class);

//            when(moneyInfo.getMoneyUseInGame()).thenReturn(99999999l);
//            when(moneyInfo.moneyCheckInGame()).thenReturn(true);
//            when(moneyInfo.canPlayNextGame()).thenReturn(true);
//            when(moneyInfo.freezeMoneyBegining(anyInt())).thenReturn(true);

            user.setProperty(GameMoneyInfo.GAME_MONEY_INFO, moneyInfo);

            users.add(user);

//            verify(baseBZExtension).send(any(BaseMsg.class), any(User.class));
        }
    }

    MockedStatic<BitZeroServer> bitZeroServerMockedStatic;
    MockedStatic<ListGameMoneyInfo> listGameMoneyInfoMockedStatic;
    MockedStatic<GameRoomManager> gameRoomManagerMockedStatic;
    MockedStatic<GameMoneyInfo> gameMoneyInfoMockedStatic;

    @BeforeEach
    void before() {
        this.gameServer = spy(gameManager.gameServer);
        bitZeroServerMockedStatic = mockStatic(BitZeroServer.class);
        listGameMoneyInfoMockedStatic = mockStatic(ListGameMoneyInfo.class);
        gameRoomManagerMockedStatic = mockStatic(GameRoomManager.class);
        gameMoneyInfoMockedStatic = mockStatic(GameMoneyInfo.class);

        bitZeroServerMockedStatic.when(BitZeroServer::getInstance).thenReturn(bitZeroServer);
        assertEquals(bitZeroServer, BitZeroServer.getInstance());

        APIManager apiManager = mock(APIManager.class);
        when(bitZeroServer.getAPIManager()).thenReturn(apiManager);
        assertEquals(apiManager, BitZeroServer.getInstance().getAPIManager());

        IExtensionManager iExtensionManager = mock(IExtensionManager.class);
        when(bitZeroServer.getExtensionManager()).thenReturn(iExtensionManager);
        assertEquals(iExtensionManager, BitZeroServer.getInstance().getExtensionManager());
//
        baseBZExtension = mock(BaseBZExtension.class);
//            IExtensionManager iExtensionManager = spy(bitZeroServer.getExtensionManager());
        when(iExtensionManager.getMainExtension()).thenReturn(baseBZExtension);
        assertEquals(baseBZExtension, iExtensionManager.getMainExtension());
        assertEquals(baseBZExtension, BitZeroServer.getInstance().getExtensionManager().getMainExtension());

        listGameMoneyInfo = mock(ListGameMoneyInfo.class);
        listGameMoneyInfoMockedStatic.when(ListGameMoneyInfo::instance).thenReturn(listGameMoneyInfo);

        gameRoomManager = mock(GameRoomManager.class);
        gameRoomManagerMockedStatic.when(GameRoomManager::instance).thenReturn(gameRoomManager);
//        when(gameRoomManager.)
        usersJoinRoom();
    }

    @AfterEach
    void after() {
        bitZeroServerMockedStatic.close();
        listGameMoneyInfoMockedStatic.close();
    }

    private void usersJoinRoom() {
        users.forEach(user -> {
            gameManager.gameServer.onGameUserEnter(user);
            when(gameServer.coTheChoiTiep(gameServer.playerList.stream().filter(gamePlayer -> gamePlayer.getUser() == user).findFirst().get())).thenReturn(true);
        });

        assertEquals(users.size(), this.gameManager.gameServer.playerCount);
    }

    @Test
    void act_SELECT_DEALER() {

        gameManager.gameServer.start();

        gameManager.nextAction = SELECT_DEALER;
        gameManager.serverAct();

        verify(gameManager.gameServer).selectDealer();

        assertEquals(DEAL_PRIVATE_CARD, gameManager.nextAction);
        assertEquals(3, gameManager.countDown);
    }

    @Test
    void act_DEAL_PRIVATE_CARD() {

        gameManager.gameServer.start();

        gameManager.nextAction = DEAL_PRIVATE_CARD;
        gameManager.serverAct();

        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.gameManager.gameServer.playerList.get(i);
            Assertions.assertEquals(2, gp.spInfo.handCards.cards.size());
        }

        Assertions.assertEquals((int) Math.ceil(0.5 * (double) this.gameManager.gameServer.playingCount) + 2, gameManager.countDown);

        assertTrue(gameManager.gameServer.gameLog.toString().contains("CB<"));

        assertTrue(gameManager.gameServer.gameLog.toString().contains("PC<"));
    }

    @Test
    void gameLoop() {
    }

    @Test
    void newRound() {
    }

    @Test
    void cancelAutoStart() {
    }

    @Test
    void makeAutoStart() {
    }

    @Test
    void canOutRoom() {
    }
}