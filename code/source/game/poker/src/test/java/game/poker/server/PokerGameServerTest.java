package game.poker.server;

import bitzero.server.BitZeroServer;
import bitzero.server.api.APIManager;
import bitzero.server.entities.User;
import bitzero.server.entities.managers.IExtensionManager;
import bitzero.server.extensions.BaseBZExtension;
import bitzero.server.extensions.data.DataCmd;
import game.modules.gameRoom.entities.*;
import game.poker.server.cmd.receive.RevTakeTurn;
import game.utils.GameUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.MockedStatic;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static game.poker.server.GameAction.*;
import static game.poker.server.GameState.GS_GAME_PLAYING;
import static game.poker.server.GameState.GS_NO_START;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class PokerGameServerTest {

    @Spy
    @InjectMocks
    private static PokerGameServer pokerGameServer;

    private static GameRoom gameRoom;

    private static BitZeroServer bitZeroServer = spy(BitZeroServer.getInstance());

    private static ListGameMoneyInfo listGameMoneyInfo;

    private static BaseBZExtension baseBZExtension;

    private static GameManager gameManager;

    private static List<User> users = new ArrayList<>();
    private static List<GamePlayer> gamePlayers = new ArrayList<>();
    private static MockedStatic<GameRoomIdGenerator> gameRoomIdGeneratorMockedStatic;

    @BeforeAll
    static void setup() {
        try (MockedStatic mocked = mockStatic(GameServer.class)) {

            mocked.when(() -> GameServer.createNewGameServer(gameRoom)).thenReturn(pokerGameServer);
        }

        gameRoomIdGeneratorMockedStatic = mockStatic(GameRoomIdGenerator.class);
        gameRoomIdGeneratorMockedStatic.when(() -> GameRoomIdGenerator.getId()).thenReturn(999);

        try {
            gameRoom = new GameRoom(new GameRoomSetting(new JSONObject("{\n" +
                    "        'moneyType':1,\n" +
                    "        'moneyBet':1000,\n" +
                    "        'requiredMoney':0,\n" +
                    "        'outMoney':0,\n" +
                    "        'maxUserPerRoom':9,\n" +
                    "        'commisionRate':2,\n" +
                    "        'rule':0,\n" +
                    "\t\t'numberOfInitialRoom':5, \n" +
                    "\t\t'roomName':'San Bang Tat Ca'\n" +
                    "    }")), 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < 2; i++) {
            User user = new User("test_" + i, null);

            GameMoneyInfo moneyInfo = new GameMoneyInfo(user, gameRoom.getId(), gameRoom.setting);
            GameUtils.enable_payment = false;
            moneyInfo.startGameUpdateMoney();

            user.setProperty(GameMoneyInfo.GAME_MONEY_INFO, moneyInfo);
            user.setConnected(true);

            users.add(user);

//            verify(baseBZExtension).send(any(BaseMsg.class), any(User.class));
        }
    }

    @AfterAll
    static void afterAll() {
        gameRoomIdGeneratorMockedStatic.close();
    }

    private MockedStatic<BitZeroServer> bitZeroServerMockedStatic;
    private MockedStatic<ListGameMoneyInfo> listGameMoneyInfoMockedStatic;

    @BeforeEach
    void before() {

        doNothing().when(pokerGameServer).init();

        pokerGameServer.init(gameRoom);
        gameManager = spy(new GameManager(pokerGameServer));
        pokerGameServer.gameMgr = gameManager;


        bitZeroServerMockedStatic = mockStatic(BitZeroServer.class);
        listGameMoneyInfoMockedStatic = mockStatic(ListGameMoneyInfo.class);

        bitZeroServerMockedStatic.when(BitZeroServer::getInstance).thenReturn(bitZeroServer);
        assertEquals(bitZeroServer, BitZeroServer.getInstance());

        APIManager apiManager = mock(APIManager.class);
        when(bitZeroServer.getAPIManager()).thenReturn(apiManager);
        assertEquals(apiManager, BitZeroServer.getInstance().getAPIManager());

        IExtensionManager iExtensionManager = mock(IExtensionManager.class);
        when(bitZeroServer.getExtensionManager()).thenReturn(iExtensionManager);
        assertEquals(iExtensionManager, BitZeroServer.getInstance().getExtensionManager());
//
        baseBZExtension = mock(BaseBZExtension.class);
//            IExtensionManager iExtensionManager = spy(bitZeroServer.getExtensionManager());
        when(iExtensionManager.getMainExtension()).thenReturn(baseBZExtension);
        assertEquals(baseBZExtension, iExtensionManager.getMainExtension());
        assertEquals(baseBZExtension, BitZeroServer.getInstance().getExtensionManager().getMainExtension());

        listGameMoneyInfo = mock(ListGameMoneyInfo.class);
        listGameMoneyInfoMockedStatic.when(ListGameMoneyInfo::instance).thenReturn(listGameMoneyInfo);
        usersJoinRoom();
    }

    @AfterEach
    void afterEach() {
        bitZeroServerMockedStatic.close();
        listGameMoneyInfoMockedStatic.close();
    }

    private void usersJoinRoom() {
        pokerGameServer.playerList.forEach(gamePlayer -> {
            gamePlayers.add(spy(gamePlayer));
//            doAnswer(invocation -> {
//                gamePlayers.add(spy((GamePlayer) invocation.getMock()));
//                return invocation.callRealMethod();
//            }).when(spied).takeChair(any(), any(), any());
        });

        gamePlayers.forEach(gamePlayer -> {
            if (gamePlayer.chair < users.size()) {
                lenient().doReturn(true).when(gamePlayer).checkMoneyCanPlay();
            }
        });
        pokerGameServer.playerList.clear();
        gamePlayers.forEach(gamePlayer -> gamePlayer.spInfo.pokerInfo.gamePlayer = gamePlayer);
        pokerGameServer.playerList.addAll(gamePlayers);
        users.forEach(user -> {
            pokerGameServer.onGameUserEnter(user);
        });

        assertEquals(users.size(), this.pokerGameServer.playerCount);
    }

    @Test
    void start() {

        pokerGameServer.start();

        assertFalse(gameManager.isAutoStart);
    }

    @Test
    void chiaBai() {

        pokerGameServer.start();

        gameManager.nextAction = SELECT_DEALER;
        gameManager.serverAct();

        gameManager.nextAction = DEAL_PRIVATE_CARD;
        gameManager.serverAct();

        for (int i = 0; i < 9; ++i) {
            GamePlayer gp = this.pokerGameServer.playerList.get(i);
            assertEquals(2, gp.spInfo.handCards.cards.size());
        }

        assertEquals((int) Math.ceil(0.5 * (double) this.pokerGameServer.playingCount) + 2, gameManager.countDown);

        assertTrue(pokerGameServer.gameLog.toString().contains("CB<"));

        assertTrue(pokerGameServer.gameLog.toString().contains("PC<"));
    }

    @Test
    void changeTurn() {

        pokerGameServer.start();

        gameManager.nextAction = SELECT_DEALER;
        gameManager.serverAct();
        printGameStatus();

        gameManager.nextAction = DEAL_PRIVATE_CARD;
        gameManager.serverAct();
        printGameStatus();

        {
            RevTakeTurn cmd = new RevTakeTurn(new DataCmd(new byte[20]));
            cmd.check = true;
            GamePlayer gp = pokerGameServer.getPlayerByChair(this.gameManager.currentChair);
            pokerGameServer.registerTakeTurn(gp.user, cmd);
            printGameStatus();
        }

//        List<Turn> turn = new ArrayList<>();
//        doAnswer(invocation -> {
//            turn.add((Turn) invocation.callRealMethod());
//            return turn.get(0);
//        }).when(gameServer).takeTurn();

        gameManager.nextAction = CHANGE_TURN;
        gameManager.serverAct();
        printGameStatus();

        verify(pokerGameServer).changeTurn();
        verify(pokerGameServer).takeTurn();
//        verify(gameServer).notifyTakeTurn(turn.get(0));

        {
            RevTakeTurn cmd = new RevTakeTurn(new DataCmd(new byte[20]));
            cmd.check = true;
            pokerGameServer.registerTakeTurn(users.get(0), cmd);
            pokerGameServer.registerTakeTurn(users.get(1), cmd);
            printGameStatus();
        }
    }

    @Test
    void fullMatch() {
        gameManager.isAutoStart = true;

        System.out.println("\n1. BEFORE start");
        printGameStatus();
        assertEquals(GS_NO_START, gameManager.gameState);

        // trigger start
        gameManager.countDown = 0;
        gameManager.gameLoop();

        System.out.println("\n2. AFTER start, BEFORE " + gameManager.nextAction);
        printGameStatus();
        assertEquals(GS_GAME_PLAYING, gameManager.gameState);
        assertEquals(SELECT_DEALER, gameManager.nextAction);

        // trigger select dealer
        gameManager.gameLoop();
        System.out.println("\n3. AFTER select dealer, BEFORE " + gameManager.nextAction);
        printGameStatus();
        assertEquals(GS_GAME_PLAYING, gameManager.gameState);
        assertEquals(DEAL_PRIVATE_CARD, gameManager.nextAction);
        assertEquals(1, gameManager.currentChair);

        // trigger deal private card
        gameManager.countDown = 0;
        gameManager.gameLoop();
        System.out.println("\n4. AFTER deal private card, BEFORE " + gameManager.nextAction + ", countDown " + gameManager.countDown);
        printGameStatus();
        assertEquals(GS_GAME_PLAYING, gameManager.gameState);
        assertEquals(CHANGE_TURN, gameManager.nextAction);
        assertEquals(1, gameManager.currentChair);

        // trigger change turn
        gameManager.countDown = 0;
        gameManager.gameLoop();
        System.out.println("\n5. AFTER change turn 1, BEFORE " + gameManager.nextAction + ", countDown " + gameManager.countDown);
        printGameStatus();
        assertEquals(GS_GAME_PLAYING, gameManager.gameState);
        assertEquals(IN_ROUND, gameManager.nextAction);
        assertEquals(1, gameManager.currentChair);

        // trigger follow player 1 (small blind)
        {
            RevTakeTurn cmd = new RevTakeTurn(new DataCmd(new byte[20]));
            cmd.follow = true;
            GamePlayer gp = pokerGameServer.getPlayerByChair(this.gameManager.currentChair);
            assertEquals(1, gameManager.currentChair);
            pokerGameServer.registerTakeTurn(gp.user, cmd);
            assertEquals(0, gameManager.currentChair);
            assertEquals(1, gameManager.countDown);
            assertEquals(CHANGE_TURN, gameManager.nextAction);
            System.out.println("\n6.1.0 AFTER " + gp.getUser().getName() + " follow 1, BEFORE " + gameManager.nextAction + ", countDown " + gameManager.countDown);
            printGameStatus();

            gameManager.gameLoop();
            assertEquals(IN_ROUND, gameManager.nextAction);
            assertEquals(20, gameManager.countDown);
            System.out.println("\n6.1.1 AFTER " + gp.getUser().getName() + " follow 1, then gameLoop, BEFORE " + gameManager.nextAction + ", countDown " + gameManager.countDown);
            printGameStatus();
        }

        // trigger check (new round, big blind)
        {
            RevTakeTurn cmd = new RevTakeTurn(new DataCmd(new byte[20]));
            cmd.check = true;
            GamePlayer gp = pokerGameServer.getPlayerByChair(this.gameManager.currentChair);
            pokerGameServer.registerTakeTurn(gp.user, cmd);
            assertEquals(1, gameManager.currentChair);
            System.out.println("\n6.2.0 AFTER " + gp.getUser().getName() + " check 1, BEFORE " + gameManager.nextAction + ", countDown " + gameManager.countDown);
            printGameStatus();

            gameManager.gameLoop();
            System.out.println("\n6.2.1 AFTER " + gp.getUser().getName() + " check 1, then gameLoop, BEFORE " + gameManager.nextAction + ", countDown " + gameManager.countDown);
            printGameStatus();
        }

        // trigger check for next player (small blind)
        {
            RevTakeTurn cmd = new RevTakeTurn(new DataCmd(new byte[20]));
            cmd.check = true;
            GamePlayer gp = pokerGameServer.getPlayerByChair(this.gameManager.currentChair);
            pokerGameServer.registerTakeTurn(gp.user, cmd);
            System.out.println("\n6.3.0 AFTER " + gp.getUser().getName() + " check 1, BEFORE " + gameManager.nextAction + ", countDown " + gameManager.countDown);
            printGameStatus();
//            assertEquals(0, gameManager.currentChair);

            gameManager.gameLoop();
            System.out.println("\n6.3.1 AFTER " + gp.getUser().getName() + " check 1, then gameLoop, BEFORE " + gameManager.nextAction + ", countDown " + gameManager.countDown);
            printGameStatus();
        }

        // trigger check for next player (big blind)
        {
            RevTakeTurn cmd = new RevTakeTurn(new DataCmd(new byte[20]));
            cmd.check = true;
            GamePlayer gp = pokerGameServer.getPlayerByChair(this.gameManager.currentChair);
            pokerGameServer.registerTakeTurn(gp.user, cmd);
            System.out.println("\n6.4.0 AFTER " + gp.getUser().getName() + " check 2, BEFORE " + gameManager.nextAction + ", countDown " + gameManager.countDown);
            printGameStatus();
//            assertEquals(1, gameManager.currentChair);

            gameManager.gameLoop();
            System.out.println("\n6.4.1 AFTER " + gp.getUser().getName() + " check 2, then gameLoop, BEFORE " + gameManager.nextAction + ", countDown " + gameManager.countDown);
            printGameStatus();
        }

//        cmd.raise = gameManager.gameTable.pokerGameInfo.bigBlindMoney;

    }

    void printGameStatus() {
        JSONObject json = new JSONObject();
        try {

            if (gameManager.gameTable.pokerGameInfo.publicCard != null) {
                json.put("pokerGameInfo", gameManager.gameTable.pokerGameInfo.publicCard.toString());
            }

            json.put("gameState", gameManager.gameState.id);
            json.put("gameStateName", gameManager.gameState);
            json.put("gameAction", gameManager.nextAction.id);
            json.put("gameActionName", gameManager.nextAction);

            json.put("round", gameManager.gameTable.getCurrentRound().roundId);
            json.put("currentChair", gameManager.currentChair);

            JSONArray arr = new JSONArray();
            for (int i = 0; i < 9; ++i) {
                GamePlayer gp = pokerGameServer.getPlayerByChair(i);
                if (gp == null || gp.getUser() == null) continue;

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("player", gp.chair + ": " + gp.getUser().getName() + "-" + gp.getPlayerStatus());
                arr.put(jsonObject);
            }
            json.put("players", arr);
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println(json);
    }
}
