package game.binh.server.logic;

public enum HandRankingCategory {
    SANH_RONG(0, true, 72, 72, 72),
    DONG_MAU_MUOI_BA(1, true, 30, 30, 30),
    DONG_MAU_MUOI_HAI(2, true, 24, 24, 24),
    BA_THUNG(3, true, 18, 18, 18),
    BA_SANH(4, true, 18, 18, 18),
    SAU_DOI_LUC_PHE_BON(5, true, 18, 18, 18),

    EM_NORMAL(6, true, 1, 1, 1),
    EM_BINHLUNG(7, true, 1, 1, 1),

    EG_THUNG_PHA_SANH(8, false, 10, 20, 1),
    EG_TUQUY(9, false, 8, 16, 1),
    EG_CULU(10, false, 1, 4, 1),
    EG_THUNG(11, false, 1, 1, 1),
    EG_SANH(12, false, 1, 1, 1),
    EG_SAM_CHI(13, false, 1, 1, 4),
    EG_HAI_DOI_KHAC_NHAU(14, false, 1, 1, 1),
    EG_MOT_DOI(15, false, 1, 1, 1),
    EG_RAC(16, false, 1, 1, 1),

    NO_GROUP(17, false, 1, 1, 1),
    NONE(18, false, 1, 1, 1);

    public final byte id;
    public final boolean isHandGroup; // false is row group
    public final int[] rateLines = new int[3];

    HandRankingCategory(int id, boolean isHandGroup, int rateLine1, int rateLine2, int rateLine3) {
        this.id = (byte) id;
        this.isHandGroup = isHandGroup;
        this.rateLines[0] = rateLine1;
        this.rateLines[1] = rateLine2;
        this.rateLines[2] = rateLine3;
    }

    public int getRate(int line) {
        return rateLines[line-1];
    }
}
