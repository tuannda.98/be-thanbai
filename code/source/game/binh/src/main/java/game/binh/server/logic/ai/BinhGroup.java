/*
 * Decompiled with CFR 0.144.
 */
package game.binh.server.logic.ai;

import game.binh.server.logic.Card;
import game.binh.server.logic.GroupCard;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

public class BinhGroup {
    public static final Comparator<BinhGroup> SORT_COMPARATOR = (c1, c2) -> {
        if (c1.getScore() < c2.getScore()) {
            return 1;
        }
        if (c1.getScore() > c2.getScore()) {
            return -1;
        }
        return 0;
    };
    private final GroupCard chi1;
    private final GroupCard chi2;
    private final GroupCard chi3;
    private final int score;

    public static void main(String[] args) {
        String x = "#S:Jr10rQrKbAb$|#D:Ac2t3c4b5r$|#D:3t3b10t$|327";
        BinhGroup binhGroup = new BinhGroup(x);
        System.out.println(binhGroup);
//        GroupCard gc1 = new GroupCard(binhGroup.getOrder());
//        gc1.DecreaseSort();
//        System.out.println(gc1);
//        GroupCard gc = new GroupCard(binhGroup.getRandom());
//        gc.DecreaseSort();
//        System.out.println(gc);

//        binhGroup.chi1.kiemtraBo(0);
//        System.out.println(binhGroup.chi1);
//        System.out.println(binhGroup.chi1.hasA());
//        binhGroup.chi2.kiemtraBo(0);
//        System.out.println(binhGroup.chi2);
//        System.out.println(binhGroup.chi2.hasA());
//        System.out.println();
//        System.out.println(BinhRule.SoSanhChi1(binhGroup.chi1, binhGroup.chi2));
//        System.out.println(BinhRule.SoSanhChi2(binhGroup.chi1, binhGroup.chi2));

        binhGroup.chi1.xepGiam();
        System.out.println(binhGroup.chi1.sanhDai());
        System.out.println(binhGroup.chi1.sanhNhi());
        binhGroup.chi2.xepGiam();
        System.out.println(binhGroup.chi2.sanhDai());
        System.out.println(binhGroup.chi2.sanhNhi());
    }

    public String toString() {
        return this.chi1 + "|" +
                this.chi2 + "|" +
                this.chi3 + "|" +
                this.score + "|";
    }

    public BinhGroup(String input) {
        String[] xx = input.split("\\|");
        this.chi1 = new GroupCard(xx[0]);
        this.chi2 = new GroupCard(xx[1]);
        this.chi3 = new GroupCard(xx[2]);
        this.score = Integer.parseInt(xx[3]);
    }

    public BinhGroup(GroupCard gc, int score) {
        int i;
        this.chi1 = new GroupCard();
        this.chi2 = new GroupCard();
        this.chi3 = new GroupCard();
        int index = 0;
        for (i = 0; i < 5; ++i) {
            this.chi1.addCard(gc.Cards().get(index++));
        }
        for (i = 0; i < 5; ++i) {
            this.chi1.addCard(gc.Cards().get(index++));
        }
        for (i = 0; i < 3; ++i) {
            this.chi1.addCard(gc.Cards().get(index++));
        }
        this.score = score;
    }

    public int getScore() {
        return this.score;
    }

    public int[] getOrder() {
        int i;
        Card c;
        int index = 0;
        int[] order = new int[13];
        for (i = 0; i < this.chi1.GetNumOfCards(); ++i) {
            c = this.chi1.Cards().get(i);
            order[index++] = c.ID;
        }
        for (i = 0; i < this.chi2.GetNumOfCards(); ++i) {
            c = this.chi2.Cards().get(i);
            order[index++] = c.ID;
        }
        for (i = 0; i < this.chi3.GetNumOfCards(); ++i) {
            c = this.chi3.Cards().get(i);
            order[index++] = c.ID;
        }
        return order;
    }

    public int[] getRandom() {
        int[] order = this.getOrder();
        LinkedList<Integer> ids = new LinkedList<>();
        for (int j : order) {
            ids.add(j);
        }
        Collections.shuffle(ids);
        int[] random = new int[13];
        for (int i = 0; i < random.length; ++i) {
            random[i] = ids.get(i);
        }
        return random;
    }

    public boolean isJackpot() {
        return this.score == 1005;
    }

    public GroupCard getRandomGroupCard() {
        return new GroupCard(this.getRandom());
    }

    public GroupCard getOrderGroupCard() {
        return new GroupCard(this.getOrder());
    }

}

