package game.binh.server.logic;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static game.binh.server.logic.HandRankingCategory.*;

public class BinhRule {
    private static final Logger logger = LoggerFactory.getLogger("BinhLogic");

    public static int SoSanhChi2(GroupCard gc1, GroupCard gc2) {
        if (gc1.kiemtraBo(1).id < gc2.kiemtraBo(1).id) {
            return 1;
        }
        if (gc1.kiemtraBo(1).id > gc2.kiemtraBo(1).id) {
            return -1;
        }

        switch (gc1.kiemtraBo(1)) {
            case EG_THUNG_PHA_SANH:
            case EG_SANH:
                if (gc1.sanhDai()) {
                    if (gc2.sanhDai()) {
                        return 0;
                    } else {
                        return 1;
                    }
                } else if (gc2.sanhDai()) {
                    return -1;
                }

                if (gc1.sanhNhi()) {
                    if (gc2.sanhDai()) {
                        return -1;
                    } else if (gc2.sanhNhi()) {
                        return 0;
                    } else {
                        return 1;
                    }
                } else if (gc2.sanhNhi()) {
                    return -1;
                }

                for (int i = gc1.cards.size() - 1; i >= 0; --i) {
                    Card c1 = gc1.cards.get(i);
                    Card c2 = gc2.cards.get(i);
                    if (c1.SO > c2.SO) {
                        return 1;
                    } else if (c1.SO == c2.SO) {
                        return 0;
                    } else {
                        return -1;
                    }
                }
                break;
            case EG_TUQUY:
            case EG_CULU:
            case EG_THUNG:
            case EG_SAM_CHI:
            case EG_HAI_DOI_KHAC_NHAU:
            case EG_MOT_DOI:
            case EG_RAC:
                int smaller = gc1.cards.size();
                if (gc1.cards.size() > gc2.cards.size()) {
                    smaller = gc2.cards.size();
                }
                for (int i = 0; i < smaller; ++i) {
                    Card c1 = gc1.cards.get(i);
                    Card c2 = gc2.cards.get(i);
                    if (c1.SO > c2.SO) {
                        return 1;
                    }
                    if (c1.SO >= c2.SO) continue;
                    return -1;
                }
                break;
            default:
                break;
        }
        return 0;
    }

    public static int SoSanhChi1(GroupCard gc1, GroupCard gc2) {
        if (gc1.kiemtraBo(0).id < gc2.kiemtraBo(0).id) {
            return 1;
        }
        if (gc1.kiemtraBo(0).id > gc2.kiemtraBo(0).id) {
            return -1;
        }

        switch (gc1.kiemtraBo(0)) {
            case EG_THUNG_PHA_SANH:
            case EG_SANH:
                if (gc1.sanhDai()) {
                    if (gc2.sanhDai()) {
                        return 0;
                    } else {
                        return 1;
                    }
                } else if (gc2.sanhDai()) {
                    return -1;
                }

                if (gc1.sanhNhi()) {
                    if (gc2.sanhDai()) {
                        return -1;
                    } else if (gc2.sanhNhi()) {
                        return 0;
                    } else {
                        return 1;
                    }
                } else if (gc2.sanhNhi()) {
                    return -1;
                }

                for (int i = gc1.cards.size() - 1; i >= 0; --i) {
                    Card c1 = gc1.cards.get(i);
                    Card c2 = gc2.cards.get(i);
                    if (c1.SO > c2.SO) {
                        return 1;
                    } else if (c1.SO == c2.SO) {
                        return 0;
                    } else {
                        return -1;
                    }
                }

                break;
            case EG_TUQUY:
            case EG_CULU:
            case EG_THUNG:
            case EG_SAM_CHI:
            case EG_HAI_DOI_KHAC_NHAU:
            case EG_MOT_DOI:
            case EG_RAC:
                int smaller = gc1.cards.size();
                if (gc1.cards.size() > gc2.cards.size()) {
                    smaller = gc2.cards.size();
                }
                for (int i = 0; i < smaller; ++i) {
                    Card c1 = gc1.cards.get(i);
                    Card c2 = gc2.cards.get(i);
                    if (c1.SO > c2.SO) {
                        return 1;
                    }
                    if (c1.SO >= c2.SO) continue;
                    return -1;
                }
                break;
            default:
                break;
        }
        return 0;
    }

    // rule == 0
    public static SoSanhChi BinhChiMode1(GroupCard gc1, GroupCard gc2, int chi) {
        HandRankingCategory kind1 = gc1.kiemtraBo(0);
        HandRankingCategory kind2 = gc2.kiemtraBo(0);

        SoSanhChi sc = new SoSanhChi();
        int kq = BinhRule.SoSanhChi1(gc1, gc2);
        int rate = Math.max(kind1.getRate(chi), kind2.getRate(chi));

        sc.chiCount1 = kq * rate;
        sc.chiCount2 = -1 * kq * rate;
        return sc;
    }

    // rule == 1
    public static SoSanhChi BinhChiMode2(GroupCard gc1, GroupCard gc2, int chi) {
        int kq = BinhRule.SoSanhChi2(gc1, gc2);
        HandRankingCategory kind1 = gc1.kiemtraBo(1);
        HandRankingCategory kind2 = gc2.kiemtraBo(1);
        SoSanhChi sc = new SoSanhChi();
        int rate = 1;
        if (kind1 == EG_SAM_CHI || kind2 == EG_SAM_CHI) {
            if (chi == 3) {
                rate = 6;
                if (gc1.hasA() && kind1 == EG_SAM_CHI || kind2 == EG_SAM_CHI && gc2.hasA()) {
                    rate = 20;
                }
            }
        } else {
            rate = Math.max(kind1.getRate(chi), kind2.getRate(chi));
        }

        sc.chiCount1 = kq * rate;
        sc.chiCount2 = -1 * kq * rate;
        return sc;
    }

    public static int demChiPhatBinhLung2(PlayerCard pc, int chi) {
        return pc.getChi(chi).kiemtraBo(1).getRate(chi);
    }

    public static int demChiPhatBinhLung1(PlayerCard pc, int chi) {
        GroupCard gc1 = pc.getChi(chi);
        HandRankingCategory kind1 = gc1.kiemtraBo(0);
        if (kind1 == EG_SAM_CHI) {
            if (chi == 3) {
                if (gc1.hasA()) {
                    return 20;
                }
                return 6;
            }
        }
        return kind1.getRate(chi);
    }

    public static SoSanhChi BinhChiLungMode1(PlayerCard pc1, PlayerCard pc2, int chi) {
        SoSanhChi sc = new SoSanhChi();
        HandRankingCategory playerCardKind1 = pc1.GetPlayerCardsKind(0);
        HandRankingCategory playerCardKind2 = pc2.GetPlayerCardsKind(0);
        if (playerCardKind1 == EM_NORMAL && playerCardKind2 == EM_BINHLUNG) {
            sc.chiCount1 = BinhRule.demChiPhatBinhLung1(pc1, chi);
            sc.chiCount2 = -sc.chiCount1;
        }
        if (playerCardKind1 == EM_BINHLUNG && playerCardKind2 == EM_NORMAL) {
            sc.chiCount1 = -BinhRule.demChiPhatBinhLung1(pc2, chi);
            sc.chiCount2 = -sc.chiCount1;
        }
        return sc;
    }

    public static SoSanhChi BinhChiLungMode2(PlayerCard pc1, PlayerCard pc2, int chi) {
        SoSanhChi sc = new SoSanhChi();
        HandRankingCategory playerCardKind1 = pc1.GetPlayerCardsKind(1);
        HandRankingCategory playerCardKind2 = pc2.GetPlayerCardsKind(1);
        if (playerCardKind1 == EM_NORMAL && playerCardKind2 == EM_BINHLUNG) {
            sc.chiCount1 = BinhRule.demChiPhatBinhLung2(pc1, chi);
            sc.chiCount2 = -sc.chiCount1;
        }
        if (playerCardKind1 == EM_BINHLUNG && playerCardKind2 == EM_NORMAL) {
            sc.chiCount1 = -BinhRule.demChiPhatBinhLung2(pc2, chi);
            sc.chiCount2 = -sc.chiCount1;
        }
        return sc;
    }

    public static SoSanhChi BinhLungMode1(PlayerCard pc1, PlayerCard pc2) {
        SoSanhChi sc = new SoSanhChi();
        SoSanhChi sc1 = BinhRule.BinhChiLungMode1(pc1, pc2, 1);
        SoSanhChi sc2 = BinhRule.BinhChiLungMode1(pc1, pc2, 2);
        SoSanhChi sc3 = BinhRule.BinhChiLungMode1(pc1, pc2, 3);
        sc.chiCount1 = 2 * (sc1.chiCount1 + sc2.chiCount1 + sc3.chiCount1);
        sc.chiCount2 = 2 * (sc1.chiCount2 + sc2.chiCount2 + sc3.chiCount2);
        return sc;
    }

    public static SoSanhChi BinhLungMode2(PlayerCard pc1, PlayerCard pc2) {
        SoSanhChi sc = new SoSanhChi();
        SoSanhChi sc1 = BinhRule.BinhChiLungMode2(pc1, pc2, 1);
        SoSanhChi sc2 = BinhRule.BinhChiLungMode2(pc1, pc2, 2);
        SoSanhChi sc3 = BinhRule.BinhChiLungMode2(pc1, pc2, 3);
        sc.chiCount1 = 2 * (sc1.chiCount1 + sc2.chiCount1 + sc3.chiCount1);
        sc.chiCount2 = 2 * (sc1.chiCount2 + sc2.chiCount2 + sc3.chiCount2);
        return sc;
    }

    public static GroupCard timBaCaiSanh(GroupCard gc) {
        GroupCard baSanh = new GroupCard();
        ArrayList<Integer> listSo = new ArrayList<>();
        for (int i = 0; i < gc.cards.size(); ++i) {
            Card c = gc.cards.get(i);
            listSo.add(c.SO);
        }
        List<Integer> listSanh = BinhRule.timTatCaSanh(listSo);
        if (listSanh == null) {
            return null;
        }
        boolean[] used = new boolean[gc.cards.size()];
        block1:
        for (int x : listSanh) {
            for (int j = 0; j < gc.cards.size(); ++j) {
                if (used[j]) continue;
                Card c = gc.cards.get(j);
                if (c.SO != x) continue;
                baSanh.cards.add(c);
                used[j] = true;
                continue block1;
            }
        }
        return baSanh;
    }

    private static List<Integer> subtract(List<Integer> parent, List<Integer> sub) {
        ArrayList<Integer> copy = new ArrayList<>(parent.size());
        copy.addAll(parent);
        for (Integer x : sub) {
            copy.remove(x);
        }
        return copy;
    }

    private static boolean checkSanh(List<Integer> listSo) {
        int prev = -10;
        int begin = -10;
        for (int cur : listSo) {
            if (begin == -10) {
                begin = cur;
                prev = cur;
                continue;
            }
            if (!BinhRule.isNextTo(begin, prev, cur)) {
                return false;
            }
            prev = cur;
        }
        return true;
    }

    private static List<Integer> timTatCaSanh(List<Integer> listSo) {
        Collections.sort(listSo);
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < listSo.size(); ++i) {
            List<Integer> sanh51 = BinhRule.timSanh5(i, listSo);
            if (sanh51 == null) continue;
            result.clear();
            result.addAll(sanh51);
            List<Integer> newList = BinhRule.subtract(listSo, sanh51);
            for (int j = 0; j < newList.size(); ++j) {
                List<Integer> sanh52 = BinhRule.timSanh5(j, newList);
                if (sanh52 == null) continue;
                result.addAll(sanh52);
                List<Integer> lastList = BinhRule.subtract(newList, sanh52);
                if (BinhRule.checkSanh(lastList)) {
                    result.addAll(lastList);
                    return result;
                }
                result = BinhRule.subtract(result, sanh52);
            }
        }
        return null;
    }

    private static List<Integer> timSanh5(int from, List<Integer> listSo) {
        ArrayList<Integer> longest = new ArrayList<>();
        int size = listSo.size();
        int prev = -10;
        int begin = -10;
        for (int i = from; i < from + size; ++i) {
            int index = i % size;
            int cur = listSo.get(index);
            if (longest.size() == 0) {
                begin = cur;
                longest.add(cur);
                prev = cur;
            } else if (BinhRule.isNextTo(begin, prev, cur)) {
                longest.add(cur);
                prev = cur;
            }
            if (longest.size() == 5) break;
        }
        if (longest.size() == 5) {
            return longest;
        }
        return null;
    }

    private static boolean isNextTo(int begin, int prev, int next) {
        if (next == prev + 1) {
            return true;
        }
        return begin == 2 && next == 14 || begin == 14 && next == 2 && prev != 2;
    }

    public static boolean isMauBinh(HandRankingCategory kind) {
        return kind.id >= 0 && kind.id <= 5;
    }

    public static long GetPlayerCardMauBinhRate(HandRankingCategory kind) {
        int rate = 1;
        switch (kind) {
            case SANH_RONG: {
                rate = 72;
                break;
            }
            case DONG_MAU_MUOI_BA: {
                rate = 30;
                break;
            }
            case DONG_MAU_MUOI_HAI: {
                rate = 24;
                break;
            }
            case BA_THUNG:
            case BA_SANH:
            case SAU_DOI_LUC_PHE_BON: {
                rate = 18;
                break;
            }
            default:
                break;
        }
        return rate;
    }

    public static long getSoLaThangAt(int soLaAt) {
        if (soLaAt == 0) {
            return -4L;
        }
        if (soLaAt == 2) {
            return 4L;
        }
        if (soLaAt == 3) {
            return 8L;
        }
        if (soLaAt == 4) {
            return 12L;
        }
        return 0L;
    }
}
