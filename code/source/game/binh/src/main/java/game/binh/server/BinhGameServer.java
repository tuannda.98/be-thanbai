/*
 * Decompiled with CFR 0.144.
 *
 * Could not load the following classes:
 *  bitzero.server.BitZeroServer
 *  bitzero.server.core.BZEvent
 *  bitzero.server.core.IBZEvent
 *  bitzero.server.core.IBZEventType
 *  bitzero.server.entities.User
 *  bitzero.server.extensions.data.BaseMsg
 *  bitzero.server.extensions.data.DataCmd
 *  bitzero.server.util.TaskScheduler
 *  bitzero.util.ExtensionUtility
 *  bitzero.util.common.business.CommonHandle
 *  com.vinplay.usercore.service.UserService
 *  com.vinplay.vbee.common.response.MoneyResponse
 *  com.vinplay.vbee.common.statics.TransType
 *  game.entities.PlayerInfo
 *  game.entities.UserScore
 *  game.eventHandlers.GameEventParam
 *  game.eventHandlers.GameEventType
 *  game.modules.bot.Bot
 *  game.modules.bot.BotManager
 *  game.modules.gameRoom.cmd.send.SendNoHu
 *  game.modules.gameRoom.config.GameRoomConfig
 *  game.modules.gameRoom.entities.GameMoneyInfo
 *  game.modules.gameRoom.entities.GameRoom
 *  game.modules.gameRoom.entities.GameRoomManager
 *  game.modules.gameRoom.entities.GameRoomSetting
 *  game.modules.gameRoom.entities.GameServer
 *  game.modules.gameRoom.entities.ListGameMoneyInfo
 *  game.modules.gameRoom.entities.MoneyException
 *  game.modules.gameRoom.entities.ThongTinThangLon
 *  game.utils.GameUtils
 *  game.utils.LoggerUtils
 *  net.sf.json.JSONObject
 *  org.json.JSONArray
 *  org.json.JSONObject
 */
package game.binh.server;

import bitzero.server.core.BZEvent;
import bitzero.server.entities.User;
import bitzero.server.extensions.data.BaseMsg;
import bitzero.server.extensions.data.DataCmd;
import bitzero.util.ExtensionUtility;
import bitzero.util.common.business.CommonHandle;
import com.vinplay.vbee.common.statics.TransType;
import game.binh.server.cmd.receive.RevBinhSoChi;
import game.binh.server.cmd.receive.RevCheatCard;
import game.binh.server.cmd.send.*;
import game.binh.server.logic.*;
import game.binh.server.logic.ai.BinhAuto;
import game.entities.PlayerInfo;
import game.entities.UserScore;
import game.eventHandlers.GameEventParam;
import game.eventHandlers.GameEventType;
import game.modules.bot.Bot;
import game.modules.bot.BotManager;
import game.modules.gameRoom.cmd.send.SendNoHu;
import game.modules.gameRoom.config.GameRoomConfig;
import game.modules.gameRoom.entities.*;
import game.utils.GameUtils;
import game.utils.LoggerUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static game.binh.server.logic.HandRankingCategory.EM_BINHLUNG;
import static game.binh.server.logic.HandRankingCategory.EM_NORMAL;
import static game.modules.gameRoom.GameRoomModule.GAME_ROOM;
import static game.modules.gameRoom.entities.GameMoneyInfo.GAME_MONEY_INFO;

public class BinhGameServer
        extends GameServer {
    private final GameManager gameMgr = new GameManager(this);
    private final List<GamePlayer> playerList = new ArrayList<>(4);
    private int playingCount = 0;
    private volatile int serverState = 0;
    private volatile int playerCount;
    private ThongTinThangLon thongTinNoHu = null;
    private final StringBuilder gameLog = new StringBuilder();

    public BinhGameServer(GameRoom room) {
        super(room);
        int i = 0;
        while (i < 4) {
            GamePlayer gp = new GamePlayer();
            gp.chair = i++;
            this.playerList.add(gp);
        }
        BinhAuto.instance();
    }

    @Override
    public void onGameMessage(User user, DataCmd dataCmd) {
        synchronized (this) {
            switch (dataCmd.getId()) {
                case 3101: {
                    this.soChi(user, dataCmd);
                    break;
                }
                case 3111: {
                    this.pOutRoom(user, dataCmd);
                    break;
                }
                case 3102: {
                    this.pBatDau(user, dataCmd);
                    break;
                }
                case 3115: {
                    this.pCheatCards(user, dataCmd);
                    break;
                }
                case 3116: {
                    this.pDangKyChoiTiep(user, dataCmd);
                    break;
                }
                case 3104: {
                    this.binhSoChiTuDong(user, dataCmd);
                    break;
                }
                case 3106: {
                    this.baoBinh(user, dataCmd);
                    break;
                }
                case 3108: {
                    this.xepLai(user, dataCmd);
                }
            }
        }
    }

    private void xepLai(User user, DataCmd cmd) {
        if (this.gameMgr.gameAction != 2) {
            return;
        }
        GamePlayer gp = this.getPlayerByUser(user);
        if (gp != null) {
            gp.sochi = false;
            SendXepLai msg = new SendXepLai();
            msg.chair = gp.chair;
            this.send(msg);
        }
    }

    private void baoBinh(User user, DataCmd dataCmd) {
        if (this.gameMgr.gameAction != 2) {
            return;
        }
        GamePlayer gp = this.getPlayerByUser(user);
        SendBinhSoChiSuccess msg = new SendBinhSoChiSuccess();
        if (gp != null) {
            msg.chair = gp.chair;
            HandRankingCategory kind = gp.spInfo.autoSort(this.gameRoom.setting.rule);
            if (BinhRule.isMauBinh(kind)) {
                this.send(msg);
                gp.sochi = true;
                this.kiemTraHoanThanhSoChi();
                this.logSoChi(gp, true);
            } else {
                msg.Error = 1;
                this.send(msg, user);
            }
        } else {
            msg.Error = 2;
            this.send(msg, user);
        }
    }

    private void soChi(User user, DataCmd dataCmd) {
        if (this.gameMgr.gameAction != 2) {
            return;
        }
        RevBinhSoChi cmd = new RevBinhSoChi(dataCmd);
        GamePlayer gp = this.getPlayerByUser(user);
        SendBinhSoChiSuccess msg = new SendBinhSoChiSuccess();
        if (gp == null) {
            msg.Error = 2;
            this.send(msg, user);
            return;
        }
        msg.chair = gp.chair;
        GroupCard chi1 = new GroupCard(cmd.chi1);
        chi1.kiemtraBo(this.gameRoom.setting.rule);
        GroupCard chi2 = new GroupCard(cmd.chi2);
        chi2.kiemtraBo(this.gameRoom.setting.rule);
        GroupCard chi3 = new GroupCard(cmd.chi3);
        chi3.kiemtraBo(this.gameRoom.setting.rule);
        boolean checkValidCard = gp.spInfo.checkCardValid(chi1, chi2, chi3);
        if (checkValidCard) {
            gp.spInfo.sortedCard.ApplyNew3GroupCards(chi1, chi2, chi3, this.gameRoom.setting.rule);
            gp.kiemTraMauBinh(this.gameRoom.setting.rule);
            this.send(msg);
            gp.sochi = true;
            this.kiemTraHoanThanhSoChi();
            this.logSoChi(gp, false);
        } else {
            msg.Error = 1;
            LoggerUtils.error("binh", "so chi ERROR", chi1, chi2, chi3);
            this.send(msg, user);
        }
    }

    public GamePlayer getPlayerByChair(int i) {
        if (i >= 0 && i < 4) {
            return this.playerList.get(i);
        }
        return null;
    }

    public long getMoneyBet() {
        return this.gameMgr.gameServer.gameRoom.setting.moneyBet;
    }

    public boolean checkPlayerChair(int chair) {
        return chair >= 0 && chair < 4;
    }

    @Override
    public void onGameUserDis(User user) {
        synchronized (this) {

            Integer chair = user.getProperty(USER_CHAIR);
            if (chair == null) {
                return;
            }
            GamePlayer gp = this.getPlayerByChair(chair);
            if (gp == null) {
                return;
            }
            if (gp.isPlaying()) {
                gp.reqQuitRoom = true;
                ++gp.tuDongChoi;
                this.gameLog.append("DIS<").append(chair).append(">");
            } else {
                GameRoomManager.instance().leaveRoom(user, this.gameRoom);
            }
        }
    }

    @Override
    public void onGameUserExit(User user) {
        Integer chair = user.getProperty(USER_CHAIR);
        if (chair == null) {
            return;
        }

        synchronized (this) {

            GamePlayer gp = this.getPlayerByChair(chair);
            if (gp == null) {
                return;
            }
            if (gp.isPlaying()) {
                gp.reqQuitRoom = true;
                ++gp.tuDongChoi;
                this.gameLog.append("DIS<").append(chair).append(">");
            } else {
                this.removePlayerAtChair(chair, !user.isConnected());
            }
            if (this.gameRoom.userManager.size() == 0) {
                this.resetPlayDisconnect();
                this.destroy();
            }
        }
    }

    private void resetPlayDisconnect() {
        for (int i = 0; i < 4; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (gp.pInfo == null) continue;
            gp.pInfo.setIsHold(false);
        }
    }

    @Override
    public void onGameUserReturn(User user) {
        if (user == null) {
            return;
        }
        synchronized (this) {
            if (this.gameRoom.setting.maxUserPerRoom == 4) {
                for (int i = 0; i < 4; ++i) {
                    GamePlayer gp = getPlayerByChair(i);
                    if (gp.getPlayerStatus() == 0 || gp.pInfo == null || !gp.pInfo.nickName.equalsIgnoreCase(user.getName()))
                        continue;
                    this.gameLog.append("RE<").append(i).append(">");
                    GameMoneyInfo moneyInfo = user.getProperty(GAME_MONEY_INFO);
                    if (moneyInfo != null && !gp.gameMoneyInfo.sessionId.equals(moneyInfo.sessionId)) {
                        moneyInfo.restoreMoney(this.gameRoom.getId());
                    }
                    user.setProperty(USER_CHAIR, gp.chair);
                    gp.user = user;
                    gp.tuDongChoi = 0;
                    gp.reqQuitRoom = false;
                    user.setProperty(GAME_MONEY_INFO, gp.gameMoneyInfo);
                    this.sendGameInfo(gp.chair);
                    return;
                }
            }
            user.removeProperty(GAME_ROOM);
        }
    }

    @Override
    public void onGameUserEnter(User user) {
        if (user == null) {
            return;
        }

        PlayerInfo pInfo = PlayerInfo.getInfo(user);
        GameMoneyInfo moneyInfo = user.getProperty(GAME_MONEY_INFO);
        if (moneyInfo == null) {
            return;
        }

        synchronized (this) {
            for (int i = 0; i < 4; ++i) {
                GamePlayer gp = getPlayerByChair(i);
                if (gp.getPlayerStatus() == 0 || gp.pInfo == null || !gp.pInfo.nickName.equalsIgnoreCase(user.getName()))
                    continue;
                this.gameLog.append("RE<").append(i).append(">");
                if (!gp.gameMoneyInfo.sessionId.equals(moneyInfo.sessionId)) {
                    moneyInfo.restoreMoney(this.gameRoom.getId());
                }
                user.setProperty(USER_CHAIR, gp.chair);
                gp.user = user;
                gp.tuDongChoi = 0;
                gp.reqQuitRoom = false;
                user.setProperty(GAME_MONEY_INFO, gp.gameMoneyInfo);
                if (this.serverState == 1) {
                    this.sendGameInfo(gp.chair);
                } else {
                    this.notifyUserEnter(gp);
                }
                return;
            }
            for (int i = 0; i < 4; ++i) {
                GamePlayer gp = getPlayerByChair(i);
                if (gp.getPlayerStatus() != 0) continue;
                if (this.serverState == 0) {
                    gp.setPlayerStatus(2);
                } else {
                    gp.setPlayerStatus(1);
                }
                gp.takeChair(user, pInfo, moneyInfo);
                ++this.playerCount;
                if (this.playerCount == 1) {
                    this.gameMgr.roomCreatorUserId = user.getId();
                    this.gameMgr.roomOwnerChair = i;
                    this.init();
                }
                this.notifyUserEnter(gp);
                break;
            }
            this.kiemTraTuDongBatDau(5);
        }
    }

    @Override
    public void onNoHu(ThongTinThangLon info) {
        this.thongTinNoHu = info;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void notifyNoHu() {
        try {
            if (this.thongTinNoHu != null) {
                for (int i = 0; i < 4; ++i) {
                    GamePlayer gp = this.getPlayerByChair(i);
                    if (gp.gameMoneyInfo == null || !gp.gameMoneyInfo.sessionId.equalsIgnoreCase(this.thongTinNoHu.moneySessionId) || !gp.gameMoneyInfo.nickName.equalsIgnoreCase(this.thongTinNoHu.nickName))
                        continue;
                    gp.gameMoneyInfo.currentMoney = this.thongTinNoHu.currentMoney;
                    break;
                }
                SendNoHu msg = new SendNoHu();
                msg.info = this.thongTinNoHu;
                for (Map.Entry entry : this.gameRoom.userManager.entrySet()) {
                    User u = (User) entry.getValue();
                    if (u == null) continue;
                    this.send(msg, u);
                }
            }
        } catch (Exception e) {
            CommonHandle.writeErrLog(e);
        } finally {
            this.thongTinNoHu = null;
        }
    }

    public void send(BaseMsg msg) {
        for (int i = 0; i < 4; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (gp.getUser() == null) continue;
            ExtensionUtility.getExtension().send(msg, gp.getUser());
        }
    }

    public void chiabai() {
        this.gameLog.append("CB<");
        SendDealCard msg = new SendDealCard();
        msg.gameId = this.gameMgr.gameRoomTable.id;
        for (int i = 0; i < 4; ++i) {
            GamePlayer gp = getPlayerByChair(i);
            if (!gp.isPlaying()) continue;
            User user = gp.getUser();
            msg.cards = gp.spInfo.handCards.toByteArray();
            if (!user.isBot()) {
                msg.maubinh = gp.kiemTraMauBinh(this.gameRoom.setting.rule).id;
            } else {
                GroupCard gc = new GroupCard(gp.getHandCards());
                gp.spInfo.handCards.handRankingCategory = gc.kiemtraBo(this.gameRoom.setting.rule);
                msg.maubinh = gp.spInfo.handCards.handRankingCategory.id;
            }
            this.gameLog.append(gp.chair).append("/");
            this.gameLog.append(gp.spInfo.handCards.toString()).append("/");
            this.gameLog.append(msg.maubinh).append(";");
            this.send(msg, user);
        }
        this.gameLog.append(">");
    }

    public void start() {
        this.gameLog.setLength(0);
        this.gameLog.append("BD<");
        this.playingCount = 0;
        this.serverState = 1;
        for (int i = 0; i < 4; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            gp.tuDongChoi = 0;
            if (!this.coTheChoiTiep(gp)) continue;

            gp.setPlayerStatus(3);
            if (gp.pInfo == null) continue;

            ++this.playingCount;
            gp.pInfo.setIsHold(true);
            PlayerInfo.setRoomId(gp.pInfo.nickName, this.gameRoom.getId());
            this.gameLog.append(gp.pInfo.nickName).append("/");
            this.gameLog.append(i).append(";");
            gp.choiTiepVanSau = false;
        }
        this.gameLog.append(this.gameRoom.setting.moneyType);
        this.gameLog.append(">");
        this.logStartGame();
        this.gameMgr.gameAction = 1;
        this.gameMgr.countDown = 0;
        this.botStartGame();
    }

    private void logStartGame() {
        for (int i = 0; i < 4; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying()) continue;
            GameUtils.logStartGame(this.gameMgr.gameRoomTable.id, gp.pInfo.nickName, this.gameMgr.gameRoomTable.logTime, this.gameRoom.setting.moneyType);
        }
    }

    private void logEndGame() {
        this.gameLog.append("KT<");
        this.gameLog.append(0).append(";");
        for (int i = 0; i < 4; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying()) continue;
            KetQuaSoBai kq = gp.spRes.getResultWithPlayer(gp.chair);
            this.gameLog.append(gp.chair).append("/").append(kq.moneyCommon).append("/").append(gp.spInfo.sortedCard.fullCard).append(";");
        }
        this.gameLog.append(">");
        GameUtils.logEndGame(this.gameMgr.gameRoomTable.id, this.gameLog.toString(), this.gameMgr.gameRoomTable.logTime);
    }

    private int demSoNguoiChoiTiep() {
        int count = 0;
        for (int i = 0; i < 4; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!this.coTheChoiTiep(gp)) continue;
            ++count;
        }
        return count;
    }

    protected void kiemTraTuDongBatDau(int after) {
        if (this.gameMgr.gameState == 0) {
            if (this.demSoNguoiChoiTiep() < 2) {
                this.gameMgr.cancelAutoStart();
            } else {
                this.gameMgr.makeAutoStart(after);
            }
        }
    }

    private boolean coTheChoiTiep(GamePlayer gp) {
        return gp.hasUser() && gp.canPlayNextGame();
    }

    private void removePlayerAtChair(int chair, boolean disconnect) {
        if (!this.checkPlayerChair(chair)) {
            return;
        }

        GamePlayer gp = getPlayerByChair(chair);
        gp.choiTiepVanSau = true;
        this.notifyUserExit(gp, disconnect);
        if (gp.user != null) {
            gp.user.removeProperty(USER_CHAIR);
            gp.user.removeProperty(GAME_ROOM);
            gp.user.removeProperty(GAME_MONEY_INFO);
        }
        gp.user = null;
        gp.pInfo = null;
        if (gp.gameMoneyInfo != null) {
            gp.gameMoneyInfo.restoreMoney(this.gameRoom.getId());
        }
        gp.gameMoneyInfo = null;
        gp.setPlayerStatus(0);
        gp.boSoChi = 0;
        --this.playerCount;
        this.kiemTraTuDongBatDau(5);
    }

    private void notifyUserEnter(GamePlayer gamePlayer) {
        User user = gamePlayer.getUser();
        if (user == null) {
            return;
        }
        SendNewUserJoin msg = new SendNewUserJoin();
        msg.money = gamePlayer.gameMoneyInfo.currentMoney;
        msg.uStatus = gamePlayer.getPlayerStatus();
        msg.setBaseInfo(gamePlayer.pInfo);
        msg.uChair = gamePlayer.chair;
        this.sendMsgExceptMe(msg, user);
        this.notifyJoinRoomSuccess(gamePlayer);
    }

    private void notifyJoinRoomSuccess(GamePlayer gamePlayer) {
        SendJoinRoomSuccess msg = new SendJoinRoomSuccess();
        msg.uChair = gamePlayer.chair;
        msg.roomId = this.gameRoom.getId();
        msg.moneyType = this.gameMgr.gameServer.gameRoom.setting.moneyType;
        msg.gameId = this.gameMgr.gameRoomTable.id;
        msg.moneyBet = this.gameMgr.gameServer.gameRoom.setting.moneyBet;
        msg.rule = this.gameRoom.setting.rule;
        for (int i = 0; i < 4; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            msg.playerStatus[i] = (byte) gp.getPlayerStatus();
            msg.playerList[i] = gp.getPlayerInfo();
            msg.moneyInfoList[i] = gp.gameMoneyInfo;
        }
        msg.gameState = (byte) this.gameMgr.gameState;
        msg.gameAction = (byte) this.gameMgr.gameAction;
        msg.countDownTime = (byte) this.gameMgr.countDown;
        this.send(msg, gamePlayer.getUser());
    }

    private void notifyUserExit(GamePlayer gamePlayer, boolean disconnect) {
        if (gamePlayer.pInfo != null) {
            gamePlayer.pInfo.setIsHold(false);
            SendUserExitRoom msg = new SendUserExitRoom();
            msg.nChair = (byte) gamePlayer.chair;
            msg.nickName = gamePlayer.pInfo.nickName;
            this.send(msg);
        }
    }

    private GamePlayer getPlayerByUser(User user) {
        Integer chair = user.getProperty(USER_CHAIR);
        if (chair != null) {
            GamePlayer gp = this.getPlayerByChair(chair);
            if (gp != null && gp.pInfo != null && gp.pInfo.nickName.equalsIgnoreCase(user.getName())) {
                return gp;
            }
            return null;
        }
        return null;
    }

    private void sendGameInfo(int chair) {
        GamePlayer gamePlayer = this.getPlayerByChair(chair);
        SendGameInfo msg = new SendGameInfo();
        msg.gameState = this.gameMgr.gameState;
        msg.gameAction = this.gameMgr.gameAction;
        msg.countdownTime = this.gameMgr.countDown;
        msg.chair = (byte) gamePlayer.chair;
        msg.roomId = this.gameRoom.getId();
        msg.rule = this.gameRoom.setting.rule;
        msg.gameId = this.gameMgr.gameRoomTable.id;
        msg.moneyBet = this.getMoneyBet();
        msg.moneyType = this.gameRoom.setting.moneyType;
        for (int i = 0; i < 4; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (gp.hasUser()) {
                msg.pInfos[i] = gp;
                msg.hasInfoAtChair[i] = true;
                continue;
            }
            msg.hasInfoAtChair[i] = false;
        }
        this.send(msg, gamePlayer.getUser());
    }

    private void pOutRoom(User user, DataCmd dataCmd) {
        GamePlayer gp = this.getPlayerByUser(user);
        this.pOutRoom(gp);
    }

    private void pOutRoom(GamePlayer gp) {
        if (gp != null) {
            if (gp.getPlayerStatus() == 3) {
                gp.reqQuitRoom = !gp.reqQuitRoom;
                this.notifyRegisterOutRoom(gp);
            } else {
                GameRoomManager.instance().leaveRoom(gp.getUser(), this.gameRoom);
            }
        }
    }

    private void notifyRegisterOutRoom(GamePlayer gp) {
        SendNotifyReqQuitRoom msg = new SendNotifyReqQuitRoom();
        msg.chair = (byte) gp.chair;
        msg.reqQuitRoom = gp.reqQuitRoom;
        this.send(msg);
    }

    private void logSoChi(GamePlayer gp, boolean baoBinh) {
        if (baoBinh) {
            this.gameLog.append("BB<");
        } else {
            this.gameLog.append("SC<");
        }
        this.gameLog.append(gp.chair).append(";");
        this.gameLog.append(gp.spInfo.getKind(this.gameRoom.setting.rule)).append(";");
        this.gameLog.append(gp.spInfo.sortedCard.ChiMot()).append(";");
        this.gameLog.append(gp.spInfo.sortedCard.ChiHai()).append(";");
        this.gameLog.append(gp.spInfo.sortedCard.ChiBa()).append(">");
    }

    private void binhSoChiTuDong(User user, DataCmd dataCmd) {
        if (this.gameMgr.gameAction != 2) {
            return;
        }
        RevBinhSoChi cmd = new RevBinhSoChi(dataCmd);
        GamePlayer gp = this.getPlayerByUser(user);
        if (gp == null) {
            return;
        }
        GroupCard chi1 = new GroupCard(cmd.chi1);
        chi1.kiemtraBo(this.gameRoom.setting.rule);
        GroupCard chi2 = new GroupCard(cmd.chi2);
        chi2.kiemtraBo(this.gameRoom.setting.rule);
        GroupCard chi3 = new GroupCard(cmd.chi3);
        chi3.kiemtraBo(this.gameRoom.setting.rule);
        boolean checkValidCard = gp.spInfo.checkCardValid(chi1, chi2, chi3);
        if (checkValidCard) {
            gp.spInfo.sortedCard.ApplyNew3GroupCards(chi1, chi2, chi3, this.gameRoom.setting.rule);
            gp.kiemTraMauBinh(this.gameRoom.setting.rule);
            if (gp.spInfo.sortedCard.kiemTraBinhLung(this.gameRoom.setting.rule)) {
                gp.reqQuitRoom = true;
            }
        }
    }

    private void kiemTraHoanThanhSoChi() {
        for (int i = 0; i < 4; ++i) {
            GamePlayer gamePlayer = this.getPlayerByChair(i);
            if (!gamePlayer.dangChoSoChi()) continue;
            return;
        }
        this.gameMgr.countDown = 0;
    }

    public void endGame() {
        for (int i = 0; i < 4; ++i) {
            GamePlayer gp1 = this.getPlayerByChair(i);
            if (!gp1.isPlaying()) continue;

            this.kiemTraKickKhoiPhongVanSauViKhongSoChi(gp1);
            HandRankingCategory kind1 = gp1.kiemTraMauBinh(this.gameRoom.setting.rule);
            for (int j = i + 1; j < 4; ++j) {
                GamePlayer gp2 = this.getPlayerByChair(j);
                if (!gp2.isPlaying()) continue;

                HandRankingCategory kind2 = gp2.kiemTraMauBinh(this.gameRoom.setting.rule);
                if (kind1 == EM_NORMAL && kind2 == EM_NORMAL) {
                    this.soChiThongThuong(gp1, gp2);
                    continue;
                }
                this.soChiMauBinh(gp1, gp2);
            }
        }

        this.tinhThangThuaSapLang();
        if (this.gameRoom.setting.rule == 1) {
            this.tinhThangThuaAt();
        }

        this.tinhTienThucSu();
        this.notifyEndGame();
        this.logEndGame();
    }

    private void tinhThangThuaAt() {
        if (this.playingCount == 4) {
            int i;
            GamePlayer gp;
            for (i = 0; i < 4; ++i) {
                gp = this.getPlayerByChair(i);
                if (!gp.spInfo.hasTuQuyAt(this.gameRoom.setting.rule)) continue;
                return;
            }
            for (i = 0; i < 4; ++i) {
                gp = this.getPlayerByChair(i);
                long tienAt = BinhRule.getSoLaThangAt(gp.spInfo.demSoAt());
                for (int j = 0; j < 4; ++j) {
                    GamePlayer gp1 = this.getPlayerByChair(j);
                    KetQuaSoBai kq = gp1.spRes.getResultWithPlayer(i);
                    kq.moneyAt = tienAt;
                }
            }
        }
    }

    private void tinhThangThuaSapLang() {
        if (this.playingCount == 4) {
            for (int i = 0; i < 4; ++i) {
                int soNhaThangSap = 0;
                int soNhaThuaSap = 0;
                GamePlayer gp = this.getPlayerByChair(i);
                for (int j = 0; j < 4; ++j) {
                    if (j == gp.chair) continue;
                    KetQuaSoBai kq = gp.spRes.getResultWithPlayer(j);
                    if (kq.moneySap > 0L) {
                        ++soNhaThangSap;
                    }
                    if (kq.moneySap >= 0L) continue;
                    ++soNhaThuaSap;
                }
                if (soNhaThangSap == 3) {
                    this.thangThuaSapLang(gp);
                }
                if (soNhaThuaSap != 3) continue;
                this.thangThuaSapLang(gp);
            }
        }
    }

    private void thangThuaSapLang(GamePlayer gp) {
        gp.sapLang = true;
        for (int j = 0; j < 4; ++j) {
            if (j == gp.chair) continue;
            GamePlayer gpThangThua = this.getPlayerByChair(j);
            if (gpThangThua.sapLang) continue;
            gp.thangThuaSapLang(gpThangThua);
        }
    }

    private void kiemTraKickKhoiPhongVanSauViKhongSoChi(GamePlayer gp) {
        if (!gp.sochi) {
            if (gp.tuDongChoi > 0) {
                if (gp.getUser() != null && !gp.getUser().isBot()) {
                    gp.autoSort(this.gameRoom.setting.rule);
                }
            } else {
                ++gp.boSoChi;
                if (gp.boSoChi >= 2) {
                    gp.reqQuitRoom = true;
                }
            }
        } else {
            gp.boSoChi = 0;
        }
    }

    private void tinhTienThucSu() {
        UserScore score = new UserScore();
        long moneyLostTotal = 0L;
        long soChiThang = 0L;
        for (int i = 0; i < 4; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying()) continue;
            KetQuaSoBai kq = gp.spRes.getResultWithPlayer(gp.chair);
            kq.calculateMoneyCommon();
            if (kq.moneyCommon < 0L) {
                score.money = kq.moneyCommon * this.getMoneyBet();
                try {
                    score.money = kq.moneyCommon = gp.gameMoneyInfo.chargeMoneyInGame(score, this.gameRoom.getId(), this.gameMgr.gameRoomTable.id);
                } catch (MoneyException e) {
                    kq.moneyCommon = 0L;
                    if (!(e instanceof NotEnoughMoneyException)) {
                        CommonHandle.writeErrLog("ERROR WHEN CHARGE MONEY INGAME" + gp.gameMoneyInfo.toString(), e);
                    }
                    gp.reqQuitRoom = true;
                }
                moneyLostTotal -= kq.moneyCommon;
                score.winCount = 0;
                score.lostCount = 1;
                this.capNhatKetQuaTinhTienChung(gp, kq);
                this.dispatchAddEventScore(gp.getUser(), score);
                continue;
            }
            soChiThang += kq.moneyCommon;
        }
        long moneyWinTotal = 0L;
        for (int i = 0; i < 4; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying()) continue;
            KetQuaSoBai kq = gp.spRes.getResultWithPlayer(gp.chair);
            if (kq.moneyCommon < 0L) continue;
            kq.moneyCommon = Math.round((double) kq.moneyCommon / (double) soChiThang * (double) moneyLostTotal);
            String moneyTypeName = this.gameRoom.setting.moneyType == 1 ? "vin" : "xu";
            long currentMoney = GameMoneyInfo.userService.getCurrentMoneyUserCache(gp.user.getName(), moneyTypeName);
            if (kq.moneyCommon > currentMoney) {
                kq.moneyCommon = currentMoney;
            }
            score.money = kq.moneyCommon;
            score.wastedMoney = (long) ((double) (score.money * (long) this.gameRoom.setting.commisionRate) / 100.0);
            score.money -= score.wastedMoney;
            score.winCount = 1;
            score.lostCount = 0;
            try {
                score.money = kq.moneyCommon = gp.gameMoneyInfo.chargeMoneyInGame(score, this.gameRoom.getId(), this.gameMgr.gameRoomTable.id);
                moneyWinTotal += score.money + score.wastedMoney;
            } catch (MoneyException e) {
                kq.moneyCommon = 0L;
                score.money = 0L;
                if (!(e instanceof NotEnoughMoneyException)) {
                    CommonHandle.writeErrLog("ERROR WHEN CHARGE MONEY INGAME" + gp.gameMoneyInfo.toString(), e);
                }
                gp.reqQuitRoom = true;
            }
            this.capNhatKetQuaTinhTienChung(gp, kq);
            this.dispatchAddEventScore(gp.getUser(), score);
        }
        long remain = moneyLostTotal - moneyWinTotal;
        if (remain > 0L) {
            for (int i = 0; i < 4; ++i) {
                long moneyBack;
                GamePlayer gp = this.getPlayerByChair(i);
                KetQuaSoBai kq = gp.spRes.getResultWithPlayer(gp.chair);
                if (kq.moneyCommon >= 0L) continue;
                score.money = -Math.round((double) kq.moneyCommon / (double) moneyLostTotal * (double) remain);
                try {
                    kq.moneyCommon += gp.gameMoneyInfo.chargeMoneyInGame(score, this.gameRoom.getId(), this.gameMgr.gameRoomTable.id);
                } catch (MoneyException e) {
                    score.money = 0L;
                    if (!(e instanceof NotEnoughMoneyException)) {
                        CommonHandle.writeErrLog("ERROR WHEN CHARGE MONEY INGAME" + gp.gameMoneyInfo.toString(), e);
                    }
                    gp.reqQuitRoom = true;
                }
                this.capNhatKetQuaTinhTienChung(gp, kq);
                this.dispatchAddEventScore(gp.getUser(), score);
            }
        }
        this.truTienHoa();
    }

    private void truTienHoa() {
        for (int i = 0; i < 4; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying()) continue;
            KetQuaSoBai kq = gp.spRes.getResultWithPlayer(gp.chair);
            if (kq.moneyCommon == 0L) continue;
            return;
        }
        UserScore score = new UserScore();
        score.money = (long) (-Math.floor((double) this.gameRoom.setting.moneyBet * ((double) this.gameRoom.setting.commisionRate / 100.0)));
        for (int i = 0; i < 4; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying()) continue;
            KetQuaSoBai kq = gp.spRes.getResultWithPlayer(gp.chair);
            try {
                kq.moneyCommon = gp.gameMoneyInfo.chargeMoneyInGame(score, this.gameRoom.getId(), this.gameMgr.gameRoomTable.id);
                if (kq.moneyCommon != 0L) {
                    String mamaName = "simacula";
                    BotManager.instance().userService.updateMoney(mamaName, -kq.moneyCommon, "vin", GameUtils.gameName, "Binh_Hoa", "Binh_Hoa", -kq.moneyCommon, null, TransType.NO_VIPPOINT);
                }
            } catch (MoneyException e) {
                kq.moneyCommon = 0L;
                if (!(e instanceof NotEnoughMoneyException)) {
                    CommonHandle.writeErrLog("ERROR WHEN CHARGE MONEY INGAME" + gp.gameMoneyInfo.toString(), e);
                }
                gp.reqQuitRoom = true;
            }
            this.capNhatKetQuaTinhTienChung(gp, kq);
        }
    }

    private void capNhatKetQuaTinhTienChung(GamePlayer me, KetQuaSoBai kq) {
        for (int i = 0; i < 4; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying() || me.chair == i) continue;
            KetQuaSoBai kq1 = gp.spRes.getResultWithPlayer(me.chair);
            kq1.moneyCommon = kq.moneyCommon;
        }
    }

    private void dispatchAddEventScore(User user, UserScore score) {
        if (user == null) {
            return;
        }
        score.moneyType = this.gameRoom.setting.moneyType;
        UserScore newScore = score.clone();
        HashMap<GameEventParam, Object> evtParams = new HashMap<>();
        evtParams.put(GameEventParam.USER, user);
        evtParams.put(GameEventParam.USER_SCORE, newScore);
        ExtensionUtility.dispatchEvent(new BZEvent(GameEventType.EVENT_ADD_SCORE, evtParams));
    }

    private void notifyEndGame() {
        SendEndGame msgView = new SendEndGame();
        int soChi = 0;
        int sapCaBaChi = 0;
        int count = 5;
        if (this.gameRoom.setting.rule == 1) {
            count += 3;
        }

        int c = 0;
        for (int i = 0; i < 4; ++i) {
            GamePlayer gp;
            gp = this.getPlayerByChair(i);
            if (!gp.isPlaying()) continue;
            if (gp.spInfo.getKind(this.gameRoom.setting.rule) == EM_NORMAL && ++c == 2) {
                soChi = 7;
            }
            if (gp.spRes.getResultWithPlayer(gp.chair).moneySap == 0L) continue;
            sapCaBaChi = 3;
        }

        count += soChi + sapCaBaChi;
        for (int i = 0; i < 4; ++i) {
            SendEndGame msg = new SendEndGame();
            GamePlayer gp1 = this.getPlayerByChair(i);
            if (!gp1.isPlaying()) continue;

            msg.moneyArray[i] = gp1.gameMoneyInfo.currentMoney;
            msgView.moneyArray[i] = gp1.gameMoneyInfo.currentMoney;
            msgView.ketqua.add(gp1.spRes.getResultWithPlayer(gp1.chair));
            for (int j = 0; j < 4; ++j) {
                GamePlayer gp2 = this.getPlayerByChair(j);
                if (!gp2.isPlaying()) continue;
                msg.ketqua.add(gp1.spRes.getResultWithPlayer(j));
            }
            msg.countdownsochi = count;
            this.send(msg, gp1.getUser());
        }

        msgView.countdownsochi = count;
        for (int i = 0; i < 4; ++i) {
            GamePlayer gp;
            gp = this.getPlayerByChair(i);
            if (gp.getPlayerStatus() != 1) continue;
            this.send(msgView, gp.getUser());
        }
        this.gameMgr.gameState = 2;
        this.gameMgr.countDown = count;
        this.kiemTraNoHuThangLon();
    }

    private boolean dispatchEventThangLon(GamePlayer gp, boolean isNoHu) {
        return GameUtils.dispatchEventThangLon(gp.getUser(), this.gameRoom, this.gameMgr.gameRoomTable.id, gp.gameMoneyInfo, this.getMoneyBet(), isNoHu, gp.getHandCards());
    }

    private void kiemTraNoHuThangLon() {
        for (int i = 0; i < 4; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!gp.isPlaying()) continue;
            if (gp.spInfo.handCards.isNoHu(this.gameRoom.setting.rule)) {
                boolean result = this.dispatchEventThangLon(gp, true);
                if (!result) continue;
                this.gameMgr.countDown += 5;
                continue;
            }
            KetQuaSoBai kq = gp.spRes.getResultWithPlayer(gp.chair);
            if (kq.moneyCommon < GameRoomConfig.instance().getBigWin()) continue;
            this.dispatchEventThangLon(gp, false);
        }
    }

    private void soSanhTungChi(
            GamePlayer gamePlayer1, GamePlayer gamePlayer2,
            KetQuaSoBai kq11, KetQuaSoBai kq12,
            KetQuaSoBai kq22, KetQuaSoBai kq21,
            KetQuaTinhSap kqSap, int chi) {
        SoSanhChi soSanhChi = this.gameRoom.setting.rule == 0 ?
                BinhRule.BinhChiMode1(gamePlayer1.spInfo.sortedCard.getChi(chi), gamePlayer2.spInfo.sortedCard.getChi(chi), chi)
                : BinhRule.BinhChiMode2(gamePlayer1.spInfo.sortedCard.getChi(chi), gamePlayer2.spInfo.sortedCard.getChi(chi), chi);

        kq12.moneyInChi[chi - 1] = soSanhChi.chiCount2;
        kq21.moneyInChi[chi - 1] = soSanhChi.chiCount1;

        kq11.moneyInChi[chi - 1] = kq11.moneyInChi[chi - 1] + kq21.moneyInChi[chi - 1];
        kq22.moneyInChi[chi - 1] = kq22.moneyInChi[chi - 1] + kq12.moneyInChi[chi - 1];

        if (soSanhChi.motSapHai()) {
            ++kqSap.tinhSap1;
        } else if (soSanhChi.haiSapMot()) {
            ++kqSap.tinhSap2;
        }

        kqSap.tongChiThang += soSanhChi.chiCount1;
    }

    private void soChiThongThuong(GamePlayer gp1, GamePlayer gp2) {
        KetQuaSoBai kq11 = gp1.spRes.getResultWithPlayer(gp1.chair);
        kq11.initCard(gp1, this.gameRoom.setting.rule);

        KetQuaSoBai kq12 = gp1.spRes.getResultWithPlayer(gp2.chair);
        kq12.initCard(gp2, this.gameRoom.setting.rule);

        KetQuaSoBai kq22 = gp2.spRes.getResultWithPlayer(gp2.chair);
        kq22.initCard(gp2, this.gameRoom.setting.rule);

        KetQuaSoBai kq21 = gp2.spRes.getResultWithPlayer(gp1.chair);
        kq21.initCard(gp1, this.gameRoom.setting.rule);

        KetQuaTinhSap kqSap = new KetQuaTinhSap();
        this.soSanhTungChi(gp1, gp2, kq11, kq12, kq22, kq21, kqSap, 1);
        this.soSanhTungChi(gp1, gp2, kq11, kq12, kq22, kq21, kqSap, 2);
        this.soSanhTungChi(gp1, gp2, kq11, kq12, kq22, kq21, kqSap, 3);

        if (kqSap.tinhSap1 == 3) {
            kq11.moneySap += kqSap.tongChiThang;
            kq22.moneySap -= kqSap.tongChiThang;
            kq12.moneySap = -kqSap.tongChiThang;
            kq21.moneySap = kqSap.tongChiThang;
        }

        if (kqSap.tinhSap2 == 3) {
            int tongChiThang2 = -kqSap.tongChiThang;
            kq22.moneySap += tongChiThang2;
            kq11.moneySap -= tongChiThang2;
            kq21.moneySap = -tongChiThang2;
            kq12.moneySap = tongChiThang2;
        }
    }

    private void soChiMauBinh(GamePlayer gp1, GamePlayer gp2) {
        KetQuaSoBai kq11 = gp1.spRes.getResultWithPlayer(gp1.chair);
        kq11.initCard(gp1, this.gameRoom.setting.rule);
        KetQuaSoBai kq12 = gp1.spRes.getResultWithPlayer(gp2.chair);
        kq12.initCard(gp2, this.gameRoom.setting.rule);
        KetQuaSoBai kq22 = gp2.spRes.getResultWithPlayer(gp2.chair);
        kq22.initCard(gp2, this.gameRoom.setting.rule);
        KetQuaSoBai kq21 = gp2.spRes.getResultWithPlayer(gp1.chair);
        kq21.initCard(gp1, this.gameRoom.setting.rule);
        HandRankingCategory kind1 = gp1.kiemTraMauBinh(this.gameRoom.setting.rule);
        HandRankingCategory kind2 = gp2.kiemTraMauBinh(this.gameRoom.setting.rule);
        PlayerCard pc1 = gp1.spInfo.sortedCard;
        PlayerCard pc2 = gp2.spInfo.sortedCard;
        if (kind1 == EM_BINHLUNG && kind2 == EM_NORMAL
                || kind1 == EM_NORMAL && kind2 == EM_BINHLUNG) {
            SoSanhChi sc = this.gameRoom.setting.rule == 0 ? BinhRule.BinhLungMode1(pc1, pc2) : BinhRule.BinhLungMode2(pc1, pc2);
            kq11.moneyCommon += sc.chiCount1;
            kq22.moneyCommon += sc.chiCount2;
            kq21.moneyCommon = sc.chiCount1;
            kq12.moneyCommon = sc.chiCount2;
        }
        if (BinhRule.isMauBinh(kind1)) {
            long moneyWin = BinhRule.GetPlayerCardMauBinhRate(kind1);
            kq12.moneyCommon = -moneyWin;
            kq21.moneyCommon = moneyWin;
            kq11.moneyCommon -= kq12.moneyCommon;
            kq22.moneyCommon -= kq21.moneyCommon;
        }
        if (BinhRule.isMauBinh(kind2)) {
            long moneyWin;
            kq12.moneyCommon = moneyWin = BinhRule.GetPlayerCardMauBinhRate(kind2);
            kq21.moneyCommon = -moneyWin;
            kq11.moneyCommon -= kq12.moneyCommon;
            kq22.moneyCommon -= kq21.moneyCommon;
        }
    }

    private byte[] kiemTraMauBinh() {
        byte[] ketQuaMauBinh = new byte[4];
        for (int i = 0; i < 4; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            ketQuaMauBinh[i] = (gp.isPlaying() ? gp.spInfo.getKind(this.gameRoom.setting.rule).id : 6);
        }
        return ketQuaMauBinh;
    }

    private boolean[] hasInfoAt() {
        boolean[] hasInfoAt = new boolean[4];
        for (int i = 0; i < 4; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            hasInfoAt[i] = gp.isPlaying();
        }
        return hasInfoAt;
    }

    private void notifyKickRoom(GamePlayer gp, int reason) {
        SendKickRoom msg = new SendKickRoom();
        msg.reason = (byte) reason;
        this.send(msg, gp.getUser());
    }

    public void pPrepareNewGame() {
        this.gameMgr.gameState = 0;
        SendUpdateMatch msg = new SendUpdateMatch();
        for (int i = 0; i < 4; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (gp.getPlayerStatus() != 0) {
                if (GameUtils.isMainTain) {
                    gp.reqQuitRoom = true;
                    this.notifyKickRoom(gp, 2);
                }
                if (!this.coTheChoiTiep(gp)) {
                    if (!gp.checkMoneyCanPlay()) {
                        this.notifyKickRoom(gp, 1);
                    }
                    if (gp.getUser() != null && this.gameRoom != null) {
                        GameRoom gameRoom = gp.getUser().getProperty(GAME_ROOM);
                        if (gameRoom == this.gameRoom) {
                            GameRoomManager.instance().leaveRoom(gp.getUser());
                        }
                    } else {
                        this.removePlayerAtChair(i, false);
                    }
                    msg.hasInfoAtChair[i] = false;
                } else {
                    msg.hasInfoAtChair[i] = true;
                    msg.pInfos[i] = gp;
                }
                gp.setPlayerStatus(2);
            } else {
                msg.hasInfoAtChair[i] = false;
            }
            gp.prepareNewGame();
        }
        for (int i = 0; i < 4; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (!msg.hasInfoAtChair[i]) continue;
            msg.chair = (byte) i;
            this.send(msg, gp.getUser());
        }
        this.gameMgr.prepareNewGame();
        this.serverState = 0;
    }

    private void pBatDau(User user, DataCmd dataCmd) {
        int nextGamePlayerCount = this.demSoNguoiChoiTiep();
        if (nextGamePlayerCount >= 2) {
            this.gameMgr.makeAutoStart(0);
        }
    }

    private void pCheatCards(User user, DataCmd dataCmd) {
        if (!GameUtils.isCheat) {
            return;
        }
        RevCheatCard cmd = new RevCheatCard(dataCmd);
        if (cmd.isCheat) {
            this.gameMgr.gameRoomTable.isCheat = true;
            this.gameMgr.gameRoomTable.suit.setOrder(cmd.cards);
        } else {
            this.gameMgr.gameRoomTable.suit.initCard();
            this.gameMgr.gameRoomTable.isCheat = false;
        }
    }

    private void pDangKyChoiTiep(User user, DataCmd dataCmd) {
        GamePlayer gp = this.getPlayerByUser(user);
        if (gp != null) {
            gp.choiTiepVanSau = true;
        }
    }

    public void botAutoPlay() {
        for (int i = 0; i < 4; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (this.gameMgr.countDown > 0 && gp.yeuCauBotRoiPhong == this.gameMgr.countDown && gp.getUser() != null && gp.getUser().isBot()) {
                this.pOutRoom(gp);
                gp.yeuCauBotRoiPhong = -1;
            }
            if (this.gameMgr.countDown >= 60 - 10) {
                return;
            }
            if (!gp.isPlaying() || gp.getUser() == null || !gp.getUser().isBot() || gp.sochi || BotManager.instance().getRandomNumber(10) != 1 && this.gameMgr.countDown > 25)
                continue;
            gp.sochi = true;
            SendBinhSoChiSuccess msg = new SendBinhSoChiSuccess();
            msg.chair = gp.chair;
            this.send(msg);
            this.kiemTraHoanThanhSoChi();
        }
    }

    @Override
    public void choNoHu(String nickName) {
        for (int i = 0; i < 4; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            if (gp.getUser() == null || !gp.getUser().getName().equalsIgnoreCase(nickName)) continue;
            this.gameMgr.gameRoomTable.suit.noHuAt(gp.chair);
        }
    }

    @Override
    protected Runnable getGameLoopTask() {
        return gameMgr::gameLoop;
    }

    public void botJoinRoom() {
        if (this.gameRoom.setting.moneyType == 1 && this.playerCount < 2) {
            int x = BotManager.instance().getRandomNumber(10);
            BotManager.instance().regJoinRoom(this.gameRoom, x);
        }
    }

    private void botStartGame() {
        if (!GameUtils.isBot || this.gameRoom.setting.moneyType != 1 || this.gameRoom.setting.password.length() > 0) {
            return;
        }
        int botCount = 0;
        int userCount = 0;
        boolean flag = false;
        for (int i = 0; i < 4; ++i) {
            GamePlayer gp = this.getPlayerByChair(i);
            User user = gp.getUser();
            if (user != null && user.isBot()) {
                int x;
                ++botCount;
                Bot bot = BotManager.instance().getBotByName(user.getName());
                ++bot.count;
                if (bot.count < 5 || this.playerCount < 4 || (x = BotManager.instance().getRandomNumber(2)) != 0 || flag)
                    continue;
                flag = true;
                gp.yeuCauBotRoiPhong = x = BotManager.instance().getRandomNumber(20);
                continue;
            }
            if (user == null) continue;
            ++userCount;
        }
        if (botCount >= 3) {
            int random;
            GamePlayer gp;
            int out = BotManager.instance().getRandomNumber(2);
            if ((out == 0 || botCount == 4 || userCount == 3) && (gp = this.getPlayerByChair(random = BotManager.instance().getRandomNumber(4))).getUser() != null && gp.getUser().isBot()) {
                this.pOutRoom(gp);
            }
        } else {
            int x = BotManager.instance().getRandomNumber(2);
            if (this.playerCount < 3 && x == 0) {
                int after = GameUtils.rd.nextInt(15) + 15;
                BotManager.instance().regJoinRoom(this.gameRoom, after);
            }
        }
    }

    @Override
    public String toString() {
        try {
            JSONObject json = this.toJONObject();
            if (json != null) {
                return json.toString();
            }
            return "{}";
        } catch (Exception e) {
            return "{}";
        }
    }

    @Override
    public JSONObject toJONObject() {
        try {
            JSONObject json = new JSONObject();
            json.put("gameState", this.gameMgr.gameState);
            json.put("gameAction", this.gameMgr.gameAction);
            JSONArray arr = new JSONArray();
            for (int i = 0; i < 4; ++i) {
                GamePlayer gp = this.getPlayerByChair(i);
                arr.put(gp.toJSONObject());
            }
            json.put("players", arr);
            return json;
        } catch (Exception e) {
            return null;
        }
    }
}
