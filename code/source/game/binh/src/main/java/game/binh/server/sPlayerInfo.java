/*
 * Decompiled with CFR 0.144.
 */
package game.binh.server;

import game.binh.server.logic.Card;
import game.binh.server.logic.GroupCard;
import game.binh.server.logic.HandRankingCategory;
import game.binh.server.logic.PlayerCard;

public class sPlayerInfo {
    public GroupCard handCards;
    public PlayerCard sortedCard = new PlayerCard();

    public void clearInfo() {
        this.handCards = null;
        this.sortedCard = new PlayerCard();
    }

    public String toString() {
        return "sorted Card: " + this.sortedCard.toString() + "\n";
    }

    public HandRankingCategory getKind(int rule) {
        return this.sortedCard.GetPlayerCardsKind(rule);
    }

    public boolean checkSubCard(GroupCard chi, GroupCard handscard, GroupCard newHandCard) {
        int count = 0;
        block0:
        for (Card c : chi.cards) {
            for (Card c1 : this.handCards.cards) {
                if (c.ID != c1.ID) continue;
                newHandCard.addCard(c1);
                ++count;
                continue block0;
            }
        }
        return count == chi.GetNumOfCards();
    }

    public boolean checkCardValid(GroupCard chi1, GroupCard chi2, GroupCard chi3) {
        if (chi1.cards.size() != 5 && chi2.cards.size() != 5 && chi3.cards.size() != 3) {
            return false;
        }
        GroupCard newHandCard = new GroupCard();
        for (Card c : chi1.cards) {
            newHandCard.addCard(c);
        }
        for (Card c : chi2.cards) {
            newHandCard.addCard(c);
        }
        for (Card c : chi3.cards) {
            newHandCard.addCard(c);
        }
        int count = 0;

        if (this.handCards == null || this.handCards.cards == null) return false;

        block3:
        for (int i = 0; i < newHandCard.cards.size(); ++i) {
            Card c = newHandCard.cards.get(i);
            for (int j = 0; j < this.handCards.cards.size(); ++j) {
                Card c1 = this.handCards.cards.get(j);
                if (c == null || c1 == null || c.ID != c1.ID) continue;
                ++count;
                continue block3;
            }
        }
        if (count == this.handCards.cards.size()) {
            this.handCards = newHandCard;
            return true;
        }
        return false;
    }

    public HandRankingCategory autoSort(int rule) {
        if (rule == 0) {
            this.sortedCard.autoSort1();
        }
        if (rule == 1) {
            this.sortedCard.autoSort2();
        }
        return this.getKind(rule);
    }

    public boolean hasTuQuyAt(int rule) {
        if (this.sortedCard.GetPlayerCardsKind(rule) == HandRankingCategory.EM_NORMAL
                || this.sortedCard.GetPlayerCardsKind(rule) == HandRankingCategory.EM_BINHLUNG) {
            for (int i = 1; i <= 2; ++i) {
                GroupCard chi = this.sortedCard.getChi(i);
                if (!chi.coTuQuyAt()) continue;
                return true;
            }
        }
        return false;
    }

    int demSoAt() {
        if (this.handCards != null) {
            return this.handCards.demSoAt();
        }
        return 0;
    }
}
