/*
 * Decompiled with CFR 0.144.
 *
 * Could not load the following classes:
 *  bitzero.server.entities.User
 *  bitzero.server.extensions.data.BaseMsg
 *  game.modules.bot.BotManager
 *  game.modules.gameRoom.entities.GameRoom
 *  game.modules.gameRoom.entities.GameRoomSetting
 *  game.utils.GameUtils
 */
package game.binh.server;

import game.binh.server.cmd.send.SendUpdateAutoStart;
import game.binh.server.logic.GameRoomTable;
import game.binh.server.logic.GroupCard;
import game.binh.server.logic.ai.BinhGroup;
import game.modules.bot.BotManager;
import game.utils.GameUtils;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class GameManager {
    public static final int DEM_LA = 1;
    public static final int GS_NO_START = 0;
    public static final int GS_GAME_PLAYING = 1;
    public static final int GS_GAME_END = 2;

    public static final int NO_ACTION = 0;
    public static final int CHIA_BAI = 1;
    public static final int BINH_SO_CHI = 2;
    public static final int HIEN_KET_QUA = 3;

    public int roomOwnerChair = 4;
    public int roomCreatorUserId;
    public int gameState = 0;
    public int gameAction = 0;
    public int countDown = 0;
    public boolean isAutoStart = false;
    public final GameRoomTable gameRoomTable = new GameRoomTable();
    public final BinhGameServer gameServer;
    public GameLogic logic = new GameLogic();

    public GameManager(BinhGameServer binhGameServer) {
        gameServer = binhGameServer;
    }

    public int getGameState() {
        return this.gameState;
    }

    public void prepareNewGame() {
        this.gameRoomTable.reset();
        this.isAutoStart = false;
        this.gameServer.kiemTraTuDongBatDau(5);
    }

    public void gameLoop() {
        synchronized (this.gameServer) {
            if (this.gameState == 0 && this.isAutoStart) {
                --this.countDown;
                if (this.countDown <= 0) {
                    this.gameState = 1;
                    this.gameServer.start();
                }
            } else if (this.gameState == 1) {
                if (this.gameAction != 0) {
                    --this.countDown;
                    if (GameUtils.isBot && this.gameAction == 2) {
                        this.gameServer.botAutoPlay();
                    }
                    if (this.countDown <= 0) {
                        if (this.gameAction == 1) {
                            this.chiaBai();
                        } else if (this.gameAction == 2) {
                            this.gameAction = 3;
                            this.gameServer.endGame();
                        }
                    }
                }
            } else if (this.gameState == 2) {
                --this.countDown;
                if (this.countDown == 5) {
                    this.gameServer.notifyNoHu();
                }
                if (this.countDown <= 0) {
                    this.gameServer.pPrepareNewGame();
                }
            } else {
                ++this.countDown;
                this.gameServer.kiemTraTuDongBatDau(5);
                if (this.countDown % 11 == 10) {
                    this.gameServer.botJoinRoom();
                }
            }
        }
    }

    public void notifyAutoStartToUsers(int after) {
        SendUpdateAutoStart msg = new SendUpdateAutoStart();
        msg.isAutoStart = this.isAutoStart;
        msg.autoStartTime = (byte) after;
        this.gameServer.send(msg);
    }

    public void cancelAutoStart() {
        this.isAutoStart = false;
        this.notifyAutoStartToUsers(0);
    }

    public void makeAutoStart(int after) {
        if (this.gameState != 0) {
            return;
        }
        if (!this.isAutoStart) {
            this.countDown = after;
        } else if (after < this.countDown) {
            this.countDown = after;
        } else {
            after = this.countDown;
        }
        this.isAutoStart = true;
        this.notifyAutoStartToUsers(after);
    }

    public void chiaBai() {
        int botCount = 0;
        for (int i = 0; i < 4; ++i) {
            GamePlayer gp = this.gameServer.getPlayerByChair(i);
            if (gp.getUser() == null || !gp.getUser().isBot()) continue;
            ++botCount;
        }
        if (botCount == 0) {
            this.chiaBaiNgauNhien();
            return;
        }

        if (BotManager.instance().balanceMode == 0) {
            int x = BotManager.instance().getRandomNumber(2);
            if (x == 0 && this.gameServer.getGameRoom().setting.moneyBet >= 5000L) {
                this.chiaBaiCanBang(true, botCount);
            } else {
                this.chiaBaiNgauNhien();
            }
            return;
        }

        boolean isUp = BotManager.instance().balanceMode == 1;
        int x = BotManager.instance().getRandomNumber(3);
        if (isUp && this.gameServer.getGameRoom().setting.moneyBet >= 5000L) {
            x = 1;
        }
        if (x != 0) {
            this.chiaBaiCanBang(isUp, botCount);
        } else {
            this.chiaBaiNgauNhien();
        }
    }

    public void chiaBaiNgauNhien() {
        boolean canJackpot = this.gameServer.getGameRoom().setting.moneyBet < 1000L;
        List<BinhGroup> cards = this.gameRoomTable.suit.dealCards(this.gameServer.getGameRoom().setting.rule, canJackpot);
        if (this.gameRoomTable.suit.cheat == 0) {
            Collections.shuffle(cards);
        }
        for (int i = 0; i < 4; ++i) {
            GamePlayer gp = this.gameServer.getPlayerByChair(i);
            if (gp.getUser() != null && gp.getUser().isBot()) {
                gp.addCards(cards.get(i).getOrderGroupCard(), this.gameRoomTable.isCheat, this.gameServer.getGameRoom().setting.rule);
                continue;
            }

            gp.addCards(cards.get(i).getRandomGroupCard(), this.gameRoomTable.isCheat, this.gameServer.getGameRoom().setting.rule);
        }

        this.gameServer.chiabai();
        this.gameAction = 2;
        this.countDown = 66;
    }

    public void chiaBaiCanBang(boolean isUp, int botCount) {
        boolean canJackpot = this.gameServer.getGameRoom().setting.moneyBet < 1000L;
        List<BinhGroup> cards = this.gameRoomTable.suit.dealCards(this.gameServer.getGameRoom().setting.rule, canJackpot);
        cards.sort(BinhGroup.SORT_COMPARATOR);

        LinkedList<BinhGroup> high = new LinkedList<>();
        LinkedList<BinhGroup> low = new LinkedList<>();
        int highSize = 4 - botCount;
        if (isUp) {
            highSize = botCount;
        }
        for (int i = 0; i < 4; ++i) {
            if (i < highSize) {
                high.add(cards.get(i));
                continue;
            }
            low.add(cards.get(i));
        }
        Collections.shuffle(high);
        Collections.shuffle(low);
        int lowCount = 0;
        int highCount = 0;
        for (int i = 0; i < 4; ++i) {
            GroupCard gc;
            GamePlayer gp = this.gameServer.getPlayerByChair(i);
            if (gp.getUser() != null && gp.getUser().isBot()) {
                if (isUp) {
                    gc = high.get(highCount++).getOrderGroupCard();
                    gp.addCards(gc, this.gameRoomTable.isCheat, this.gameServer.getGameRoom().setting.rule);
                    continue;
                }
                gc = low.get(lowCount++).getOrderGroupCard();
                gp.addCards(gc, this.gameRoomTable.isCheat, this.gameServer.getGameRoom().setting.rule);
                continue;
            }
            if (isUp) {
                gc = low.get(lowCount++).getOrderGroupCard();
                gp.addCards(gc, this.gameRoomTable.isCheat, this.gameServer.getGameRoom().setting.rule);
                continue;
            }
            gc = high.get(highCount++).getOrderGroupCard();
            gp.addCards(gc, this.gameRoomTable.isCheat, this.gameServer.getGameRoom().setting.rule);
        }
        this.gameServer.chiabai();
        this.gameAction = 2;
        this.countDown = 66;
    }
}
