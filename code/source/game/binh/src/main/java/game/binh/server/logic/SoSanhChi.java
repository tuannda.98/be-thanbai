/*
 * Decompiled with CFR 0.144.
 */
package game.binh.server.logic;

public class SoSanhChi {
    public int chiCount1 = 0;
    public int chiCount2 = 0;

    public boolean motSapHai() {
        return this.chiCount2 > this.chiCount1;
    }

    public boolean haiSapMot() {
        return this.chiCount1 > this.chiCount2;
    }

    public String toString() {
        return "chiCount1" + this.chiCount1 + "/" +
                "chiCount2" + this.chiCount2 + "/";
    }
}

