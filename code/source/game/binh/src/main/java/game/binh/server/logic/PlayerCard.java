/*
 * Decompiled with CFR 0.144.
 */
package game.binh.server.logic;

import static game.binh.server.logic.HandRankingCategory.*;

public class PlayerCard {
    public GroupCard fullCard;
    public GroupCard chi1 = new GroupCard();
    public GroupCard chi2 = new GroupCard();
    public GroupCard chi3 = new GroupCard();

    public HandRankingCategory GetPlayerCardsKind(int roomType) {
        HandRankingCategory handRankingCategory = this.fullCard.kiemtraBo(roomType);
        if (handRankingCategory == SANH_RONG
                || handRankingCategory == DONG_MAU_MUOI_BA
                || handRankingCategory == DONG_MAU_MUOI_HAI) {
            return handRankingCategory;
        }

        if (this.isBaThung(roomType)) {
            return BA_THUNG;
        }

        if (this.isBaSanh(roomType)) {
            return BA_SANH;
        }

        if (this.isLucPheBon(roomType)) {
            return SAU_DOI_LUC_PHE_BON;
        }

        if (roomType == 0) {
            if (BinhRule.SoSanhChi1(this.chi1, this.chi2) < 0
                    || BinhRule.SoSanhChi1(this.chi2, this.chi3) < 0) {
                return EM_BINHLUNG;
            }
            return EM_NORMAL;
        }

        if (BinhRule.SoSanhChi2(this.chi1, this.chi2) < 0
                || BinhRule.SoSanhChi2(this.chi2, this.chi3) < 0) {
            return EM_BINHLUNG;
        }
        return EM_NORMAL;
    }

    private boolean isBaThung(int roomType) {
        return (this.chi1.kiemtraBo(roomType) == EG_THUNG || this.chi1.kiemtraBo(roomType) == EG_THUNG_PHA_SANH)
                && (this.chi2.kiemtraBo(roomType) == EG_THUNG || this.chi2.kiemtraBo(roomType) == EG_THUNG_PHA_SANH)
                && this.chi3.kiemTraBoMauBinhBaThungChiBa() == EG_THUNG;
    }

    private boolean isBaSanh(int roomType) {
        return (this.chi1.kiemtraBo(roomType) == EG_SANH || this.chi1.kiemtraBo(roomType) == EG_THUNG_PHA_SANH)
                && (this.chi2.kiemtraBo(roomType) == EG_SANH || this.chi2.kiemtraBo(roomType) == EG_THUNG_PHA_SANH)
                && this.chi3.kiemTraBoMauBinhBaSanhChiBa() == EG_SANH;
    }

    private boolean isLucPheBon(int roomType) {
        return this.fullCard.kiemtraBo(roomType) == SAU_DOI_LUC_PHE_BON;
    }

    public GroupCard ChiBa() {
        return this.chi3;
    }

    public GroupCard ChiHai() {
        return this.chi2;
    }

    public GroupCard getChi(int chi) {
        switch (chi) {
            case 1: {
                return this.chi1;
            }
            case 2: {
                return this.chi2;
            }
            case 3: {
                return this.chi3;
            }
        }
        return null;
    }

    public GroupCard ChiMot() {
        return this.chi1;
    }

    public void autoSort1() {
        HandRankingCategory handRankingCategory = this.fullCard.kiemtraBo(0);
        if (handRankingCategory == SANH_RONG) {
            this.fullCard.xepTangTuHai();
            return;
        }

        if (handRankingCategory == SAU_DOI_LUC_PHE_BON) {
            this.sortLucPheBon(0);
            return;
        }

        if (handRankingCategory == BA_THUNG) {
            this.sortBaThung(0);
            return;
        }

        if (handRankingCategory == BA_SANH) {
            this.sortBaSanh(0);
            return;
        }

        this.sortChi(0);
    }

    public void autoSort2() {
        HandRankingCategory handRankingCategory = this.fullCard.kiemtraBo(1);
        if (handRankingCategory == SANH_RONG || handRankingCategory == DONG_MAU_MUOI_BA || handRankingCategory == DONG_MAU_MUOI_HAI) {
            this.fullCard.xepTangTuHai();
            return;
        }

        if (handRankingCategory == SAU_DOI_LUC_PHE_BON) {
            this.sortLucPheBon(1);
            return;
        }

        if (handRankingCategory == BA_THUNG) {
            this.sortBaThung(1);
            return;
        }

        if (handRankingCategory == BA_SANH) {
            this.sortBaSanh(1);
            return;
        }

        this.sortChi(1);
    }

    private void sortLucPheBon(int rule) {
        this.fullCard.sortLucPheBon();
        this.ApplyNewGroupCards(this.fullCard, rule);
    }

    public void ApplyNewGroupCards(GroupCard gc, int rule) {
        int i;
        this.fullCard = gc;
        this.chi1.reset();
        this.chi2.reset();
        this.chi3.reset();
        for (i = 0; i < 5; ++i) {
            this.chi1.addCard(gc.cards.get(i));
        }
        for (i = 5; i < 10; ++i) {
            this.chi2.addCard(gc.cards.get(i));
        }
        for (i = 10; i < 13; ++i) {
            this.chi3.addCard(gc.cards.get(i));
        }
        this.chi1.kiemtraBo(rule);
        this.chi2.kiemtraBo(rule);
        this.chi3.kiemtraBo(rule);
    }

    private void sortBaThung(int rule) {
        this.fullCard.sortBaThung();
        this.ApplyNewGroupCards(this.fullCard, rule);
    }

    private void sortBaSanh(int rule) {
        this.fullCard.sortBaSanh();
        this.ApplyNewGroupCards(this.fullCard, rule);
    }

    public boolean kiemTraBinhLung(int rule) {
        if (rule == 0) {
            return BinhRule.SoSanhChi1(this.chi1, this.chi2) < 0
                    || BinhRule.SoSanhChi1(this.chi2, this.chi3) < 0;
        }
        return BinhRule.SoSanhChi2(this.chi1, this.chi2) < 0 || BinhRule.SoSanhChi2(this.chi2, this.chi3) < 0;
    }

    public void ApplyNew3GroupCards(GroupCard chi1, GroupCard chi2, GroupCard chi3, int rule) {
        this.chi1 = chi1;
        this.chi2 = chi2;
        this.chi3 = chi3;
        this.fullCard.cards.clear();
        this.fullCard.cards.addAll(this.chi1.cards);
        this.fullCard.cards.addAll(this.chi2.cards);
        this.fullCard.cards.addAll(this.chi3.cards);
        this.fullCard.kiemTraBoNoSort(rule);
    }

    private void sortChi(int rule) {
        GroupCard gc = this.fullCard.copy();
        GroupCard chi1 = gc.getGroup(1);
        GroupCard chi2 = gc.getGroup(2);
        GroupCard chi3 = gc.getGroup(3);
        chi1.kiemtraBo(rule);
        chi2.kiemtraBo(rule);
        chi3.kiemtraBo(rule);
        if (chi3.GetNumOfCards() == 3 && chi2.GetNumOfCards() == 5 && chi1.GetNumOfCards() == 5) {
            this.ApplyNew3GroupCards(chi1, chi2, chi3, rule);
            if (this.kiemTraBinhLung(rule)) {
                this.ApplyNew3GroupCards(chi2, chi1, chi3, rule);
            }
        }
    }

    public String toString() {
        return this.chi1 + "|" +
                this.chi2 + "|" +
                this.chi3 + "|";
    }
}
