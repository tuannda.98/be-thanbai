/*
 * Decompiled with CFR 0.144.
 * 
 * Could not load the following classes:
 *  game.modules.gameRoom.entities.GameRoomIdGenerator
 *  game.utils.GameUtils
 */
package game.binh.server.logic;

import game.modules.gameRoom.entities.GameRoomIdGenerator;
import game.utils.GameUtils;

public class GameRoomTable {
    public final CardSuit suit = new CardSuit();
    public int id = GameRoomTable.getID();
    public boolean isCheat = false;
    public long logTime = System.currentTimeMillis();

    private static int getID() {
        return GameRoomIdGenerator.getId();
    }

    public void reset() {
        this.id = GameRoomTable.getID();
        this.logTime = System.currentTimeMillis();
        if (!this.isCheat || !GameUtils.isCheat) {
            this.suit.setRandom();
        }
    }
}

