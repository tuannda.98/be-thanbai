/*
 * Decompiled with CFR 0.150.
 *
 * Could not load the following classes:
 *  bitzero.util.common.business.Debug
 */
package game.sam.server.logic.ai;

import bitzero.util.common.business.Debug;
import game.sam.server.logic.Card;
import game.sam.server.logic.GroupCard;

import java.util.ArrayList;
import java.util.List;

public class SamAI {
    public int mychair = 0;
    public boolean modeBaoSam = false;
    public final List<SamCard> myCards = new ArrayList<>();
    public int enemyCardNumMin = 10;
    public int numGroup = 0;
    public boolean newRound = false;
    public final List<SamCard> lastCards = new ArrayList<>();
    public static final int MAX_CARDS = 10;

    public void initStart(int chair, GroupCard gc) {
        this.mychair = chair;
        this.modeBaoSam = false;
        this.numGroup = 0;
        this.enemyCardNumMin = 10;
        this.myCards.clear();
        for (int i = 0; i < gc.cards.size(); ++i) {
            this.myCards.add(new SamCard(gc.cards.get(i)));
        }
    }

    public void throwMyCard(int chair, int cardsize, GroupCard playCard) {
        Card c;
        int i;
        this.lastCards.clear();
        for (i = 0; i < playCard.cards.size(); ++i) {
            c = playCard.cards.get(i);
            this.lastCards.add(new SamCard(c));
        }
        if (this.mychair == chair) {
            block1:
            for (i = 0; i < playCard.cards.size(); ++i) {
                c = playCard.cards.get(i);
                for (int j = 0; j < this.myCards.size(); ++j) {
                    SamCard s = this.myCards.get(j);
                    if (c.ID != s.id) continue;
                    this.myCards.remove(j);
                    continue block1;
                }
            }
        } else {
            this.enemyCardNumMin = Math.min(cardsize, this.enemyCardNumMin);
        }
    }

    public boolean thinkBaoSam() {
        double kk = this.countDiemBoBaiBaoSam(new ArrayList<>());
        if (kk < 0.5) {
            this.modeBaoSam = true;
            return true;
        }
        return false;
    }

    public List<SamCard> thinkCardDanh() {
        ArrayList<SamCard> res = new ArrayList<>();
        this.numGroup = 0;
        this.countDiemBoBai(new ArrayList<>());
        if (this.newRound) {
            ArrayList<SamCard> res2 = new ArrayList<>();
            if (!this.modeBaoSam) {
                int kk = this.chonBoBaiDanh();
                for (SamCard myCard : this.myCards) {
                    if (myCard.group != kk) continue;
                    res2.add(myCard);
                }
                res = res2;
            } else {
                int kk = this.chonBoBaiDanhBaoSam();
                for (SamCard myCard : this.myCards) {
                    if (myCard.group != kk) continue;
                    res2.add(myCard);
                }
                res = res2;
            }
        } else {
            int i;
            ArrayList<SamCard> inCards = new ArrayList<>();
            ArrayList<SamCard> cardDown = new ArrayList<>();
            for (i = 0; i < this.lastCards.size(); ++i) {
                inCards.add(this.lastCards.get(i));
            }
            for (i = 0; i < this.myCards.size(); ++i) {
                cardDown.add(this.myCards.get(i));
            }
            List<SamCard> res2 = this.timCardChatDuoc(inCards, cardDown);
            res.addAll(res2);
        }
        return res;
    }

    public int chonBoBaiDanh() {
        double xs2;
        int start;
        boolean has2;
        int i;
        int num = this.numGroup;
        double diem = -1.0;
        int select = 0;
        int count = 0;
        int num2 = 0;
        for (SamCard myCard : this.myCards) {
            if (myCard.so != 12) continue;
            ++num2;
        }
        int numYeu = 0;
        for (i = 1; i <= num; ++i) {
            has2 = true;
            start = -1;
            count = 0;
            for (SamCard myCard : this.myCards) {
                if (myCard.group == i && myCard.so != 12) {
                    has2 = false;
                }
                if (myCard.group != i) continue;
                ++count;
                if (start != -1) continue;
                start = myCard.so;
            }
            if (count <= 0 || num2 > 0 && !has2 && count + num2 == this.myCards.size()) continue;
            double xs1 = this.xacXuatBiChan(count, start, false);
            xs2 = this.xacXuatChanDuoc(count, start, false);
            if (!(xs1 >= 0.2)) continue;
            ++numYeu;
        }
        for (i = 1; i <= num; ++i) {
            double temp;
            has2 = true;
            start = -1;
            count = 0;
            for (SamCard myCard : this.myCards) {
                if (myCard.group == i && myCard.so != 12) {
                    has2 = false;
                }
                if (myCard.group != i) continue;
                ++count;
                if (start != -1) continue;
                start = myCard.so;
            }
            if (count <= 0 || num2 > 0 && !has2 && count + num2 == this.myCards.size()) continue;
            double xs1 = this.xacXuatBiChan(count, start, false);
            xs2 = this.xacXuatChanDuoc(count, start, false);
            if (numYeu <= 1 || this.enemyCardNumMin <= 2) {
                temp = -xs1 + (double) count * 0.05;
                if (!(temp > diem)) continue;
                diem = temp;
                select = i;
                continue;
            }
            temp = -xs2 + (double) count * 0.22;
            if (!(temp > diem)) continue;
            diem = temp;
            select = i;
        }
        return select;
    }

    public int chonBoBaiDanhBaoSam() {
        int num = this.numGroup;
        double diem = -10.0;
        int select = 0;
        int count = 0;
        int num2 = 0;
        for (SamCard card : this.myCards) {
            if (card.so != 12) continue;
            ++num2;
        }
        for (int i = 1; i <= num; ++i) {
            boolean has2 = true;
            int start = -1;
            count = 0;
            for (SamCard myCard : this.myCards) {
                if (myCard.group == i && myCard.so != 12) {
                    has2 = false;
                }
                if (myCard.group != i) continue;
                ++count;
                if (start != -1) continue;
                start = myCard.so;
            }
            if (count <= 0 || num2 > 0 && !has2 && count + num2 == this.myCards.size()) continue;
            double xs1 = this.xacXuatBiChan(count, start, false);
            double xs2 = this.xacXuatChanDuoc(count, start, false);
            double temp = -xs1;
            if (!(temp > diem)) continue;
            diem = temp;
            select = i;
        }
        return select;
    }

    public List<SamCard> timCardChatDuoc(List<SamCard> inCards, List<SamCard> cardHandon) {
        double diem = 100.0;
        List<SamCard> res2 = new ArrayList<>();
        for (int i = 0; i < cardHandon.size(); ++i) {
            double diemRes;
            SamCard select = cardHandon.get(i);
            List<SamCard> res = SamLogicHelper.recommend(inCards, cardHandon, select);
            if (res.size() < inCards.size() || !((diemRes = this.enemyCardNumMin <= inCards.size() ? (double) (-res.get(0).id) : this.countDiemBoBai(res)) < diem))
                continue;
            diem = diemRes;
            res2 = res;
        }
        return res2;
    }

    public double countDiemBoBai(List<SamCard> cardOut) {
        int i;
        this.numGroup = 0;
        for (i = 0; i < this.myCards.size(); ++i) {
            this.myCards.get(i).mark = false;
            this.myCards.get(i).group = -1;
        }
        if (cardOut != null && cardOut.size() != 0) {
            for (i = 0; i < cardOut.size(); ++i) {
                for (SamCard myCard : this.myCards) {
                    if (myCard.id != cardOut.get(i).id) continue;
                    myCard.mark = true;
                    myCard.group = -1;
                }
            }
        }
        return this.xepBo();
    }

    public double countDiemBoBaiBaoSam(List<SamCard> cardOut) {
        int i;
        this.numGroup = 0;
        for (i = 0; i < this.myCards.size(); ++i) {
            this.myCards.get(i).mark = false;
            this.myCards.get(i).group = -1;
        }
        if (cardOut != null && cardOut.size() != 0) {
            for (i = 0; i < cardOut.size(); ++i) {
                for (SamCard myCard : this.myCards) {
                    if (myCard.id != cardOut.get(i).id) continue;
                    myCard.mark = true;
                    myCard.group = -1;
                }
            }
        }
        double kk = this.xepBoBaoSam();
        double diemMax = 0.0;
        int maxGroup = -1;
        for (int group = 1; group <= this.numGroup; ++group) {
            int start = -1;
            int soLa = 0;
            for (SamCard myCard : this.myCards) {
                if (myCard.group != group) continue;
                if (start == -1) {
                    start = myCard.so;
                }
                ++soLa;
            }
            if (soLa == 0) continue;
            double temp = this.countDiemBaoSam(soLa, start, false);
            Debug.trace(new Object[]{"countDiemBaoSam", temp, soLa, start});
            if (!(temp > diemMax)) continue;
            maxGroup = group;
            diemMax = temp;
        }
        Debug.trace(new Object[]{"Diem bao sam:", kk, diemMax, kk - diemMax});
        return kk - diemMax;
    }

    public double xepBo() {
        double temp;
        int j;
        int k;
        int countHasPush;
        int i;
        int i2;
        ++this.numGroup;
        int startNum = this.numGroup;
        int start = 0;
        double sum = 0.0;
        int[] doneGroup = new int[10];
        boolean[] curMark = new boolean[10];
        int[] curGroup = new int[10];
        for (i2 = 0; i2 < this.myCards.size(); ++i2) {
            curMark[i2] = this.myCards.get(i2).mark;
            curGroup[i2] = this.myCards.get(i2).group;
        }
        for (i2 = 0; i2 < this.myCards.size(); ++i2) {
            if (this.myCards.get(i2).mark) continue;
            start = i2;
            break;
        }
        for (i2 = 0; i2 < this.myCards.size(); ++i2) {
            if (this.myCards.get(i2).mark) continue;
            sum += 1.0;
        }
        int numSo = 0;
        for (SamCard card : this.myCards) {
            if (card.mark || card.so != this.myCards.get(start).so)
                continue;
            ++numSo;
        }
        int numSanh = 0;
        int sanhSo = this.myCards.get(start).so;
        for (i = 0; i < this.myCards.size(); ++i) {
            if (this.myCards.get(i).mark || this.myCards.get(i).so != sanhSo || this.myCards.get(i).so == 12) continue;
            ++numSanh;
            ++sanhSo;
        }
        for (i = 1; i <= numSo; ++i) {
            this.numGroup = startNum;
            countHasPush = 0;
            for (k = 0; k < this.myCards.size(); ++k) {
                this.myCards.get(k).mark = curMark[k];
                this.myCards.get(k).group = curGroup[k];
            }
            this.myCards.get(start).mark = true;
            this.myCards.get(start).group = this.numGroup;
            ++countHasPush;
            for (j = 0; j < this.myCards.size(); ++j) {
                if (this.myCards.get(j).mark || this.myCards.get(j).so != this.myCards.get(start).so || countHasPush >= i || j == start)
                    continue;
                ++countHasPush;
                this.myCards.get(j).mark = true;
                this.myCards.get(j).group = this.numGroup;
            }
            int countNum = 0;
            for (SamCard myCard : this.myCards) {
                if (myCard.mark) continue;
                ++countNum;
            }
            this.countDiem(i, this.myCards.get(start).so);
            temp = countNum > 0 ? this.countDiem(i, this.myCards.get(start).so) + this.xepBo() : this.countDiem(i, this.myCards.get(start).so);
            if (!(temp < sum)) continue;
            sum = temp;
            for (int iCard = 0; iCard < this.myCards.size(); ++iCard) {
                doneGroup[iCard] = this.myCards.get(iCard).group;
            }
        }
        for (i = 3; i <= numSanh; ++i) {
            countHasPush = 0;
            this.numGroup = startNum;
            for (k = 0; k < this.myCards.size(); ++k) {
                this.myCards.get(k).mark = curMark[k];
                this.myCards.get(k).group = curGroup[k];
            }
            this.myCards.get(start).mark = true;
            this.myCards.get(start).group = this.numGroup;
            ++countHasPush;
            sanhSo = this.myCards.get(start).so;
            ++sanhSo;
            for (j = 0; j < this.myCards.size(); ++j) {
                if (this.myCards.get(j).mark || this.myCards.get(j).so != sanhSo || countHasPush >= i || this.myCards.get(j).so == 12 || j == start)
                    continue;
                ++countHasPush;
                ++sanhSo;
                this.myCards.get(j).mark = true;
                this.myCards.get(j).group = this.numGroup;
            }
            temp = this.countDiem(i, this.myCards.get(start).so) + this.xepBo();
            if (!(temp < sum)) continue;
            sum = temp;
            for (j = 0; j < this.myCards.size(); ++j) {
                doneGroup[j] = this.myCards.get(j).group;
            }
        }
        for (int j2 = 0; j2 < this.myCards.size(); ++j2) {
            this.myCards.get(j2).group = doneGroup[j2];
        }
        return sum;
    }

    public double xepBoBaoSam() {
        double temp;
        int ki;
        int countNum;
        int j;
        int k;
        int countHasPush;
        int i;
        int i2;
        ++this.numGroup;
        int startNum = this.numGroup;
        int start = 0;
        double sum = 0.0;
        boolean[] curMark = new boolean[10];
        int[] curGroup = new int[10];
        for (i2 = 0; i2 < this.myCards.size(); ++i2) {
            curMark[i2] = this.myCards.get(i2).mark;
            curGroup[i2] = this.myCards.get(i2).group;
        }
        for (i2 = 0; i2 < this.myCards.size(); ++i2) {
            if (this.myCards.get(i2).mark) continue;
            start = i2;
            break;
        }
        for (i2 = 0; i2 < this.myCards.size(); ++i2) {
            if (this.myCards.get(i2).mark) continue;
            sum += 1.0;
        }
        int numSo = 0;
        for (SamCard myCard : this.myCards) {
            if (myCard.mark || myCard.so != this.myCards.get(start).so)
                continue;
            ++numSo;
        }
        int numSanh = 0;
        int sanhSo = this.myCards.get(start).so;
        for (i = 0; i < this.myCards.size(); ++i) {
            if (this.myCards.get(i).mark || this.myCards.get(i).so != sanhSo || this.myCards.get(i).so == 12) continue;
            ++numSanh;
            ++sanhSo;
        }
        for (i = 1; i <= numSo; ++i) {
            this.numGroup = startNum;
            countHasPush = 0;
            for (k = 0; k < this.myCards.size(); ++k) {
                this.myCards.get(k).mark = curMark[k];
                this.myCards.get(k).group = curGroup[k];
            }
            this.myCards.get(start).mark = true;
            this.myCards.get(start).group = this.numGroup;
            ++countHasPush;
            for (j = 0; j < this.myCards.size(); ++j) {
                if (this.myCards.get(j).mark || this.myCards.get(j).so != this.myCards.get(start).so || countHasPush >= i || j == start)
                    continue;
                ++countHasPush;
                this.myCards.get(j).mark = true;
                this.myCards.get(j).group = this.numGroup;
            }
            countNum = 0;
            for (ki = 0; ki < this.myCards.size(); ++ki) {
                if (this.myCards.get(ki).mark) continue;
                ++countNum;
            }
            temp = countNum > 0 ? this.countDiemBaoSam(i, this.myCards.get(start).so, false) + this.xepBoBaoSam() : this.countDiemBaoSam(i, this.myCards.get(start).so, false);
            if (!(temp < sum)) continue;
            sum = temp;
        }
        for (i = 3; i <= numSanh; ++i) {
            countHasPush = 0;
            this.numGroup = startNum;
            for (k = 0; k < this.myCards.size(); ++k) {
                this.myCards.get(k).mark = curMark[k];
                this.myCards.get(k).group = curGroup[k];
            }
            this.myCards.get(start).mark = true;
            this.myCards.get(start).group = this.numGroup;
            ++countHasPush;
            sanhSo = this.myCards.get(start).so;
            ++sanhSo;
            for (j = 0; j < this.myCards.size(); ++j) {
                if (this.myCards.get(j).mark || this.myCards.get(j).so != sanhSo || countHasPush >= i || this.myCards.get(j).so == 12 || j == start)
                    continue;
                ++countHasPush;
                ++sanhSo;
                this.myCards.get(j).mark = true;
                this.myCards.get(j).group = this.numGroup;
            }
            countNum = 0;
            for (ki = 0; ki < this.myCards.size(); ++ki) {
                if (this.myCards.get(ki).mark) continue;
                ++countNum;
            }
            temp = countNum > 0 ? this.countDiemBaoSam(i, this.myCards.get(start).so, true) + this.xepBoBaoSam() : this.countDiemBaoSam(i, this.myCards.get(start).so, true);
            if (!(temp < sum)) continue;
            sum = temp;
        }
        return sum;
    }

    public double countDiem(int soLa, int start) {
        if (soLa == 1) {
            return (double) (12 - start) / 12.0;
        }
        if (soLa == 2) {
            return (double) (12 - start) / 24.0;
        }
        return (double) (12 - start) / 12.0 / (double) soLa;
    }

    public double countDiemBaoSam(int soLa, int start, boolean isSanh) {
        return this.xacXuatBiChan(soLa, start, isSanh);
    }

    public double xacXuatBiChan(int b, int c, boolean a) {
        boolean isSanh = true;
        isSanh = !a;
        if (b == 1 && c == 12) {
            return 0.15;
        }
        if (b == 1 && c == 11) {
            return 0.75;
        }
        if (b == 1) {
            return 0.95 + (double) (12 - c) / 12.0 * 0.05;
        }
        if (b == 2 && c == 12) {
            return 0.01;
        }
        if (b == 2 && c == 11) {
            return 0.4;
        }
        if (b == 2 && c == 10) {
            return 0.75;
        }
        if (b == 2 && c == 9) {
            return 0.9;
        }
        if (b == 2) {
            return 1.0;
        }
        if (b == 3 && c == 12) {
            return 0.0;
        }
        if (b == 3 && c == 11) {
            return 0.2;
        }
        if (b == 3 && c == 10) {
            return 0.35;
        }
        if (b == 3 && !isSanh) {
            return 0.5;
        }
        if (b == 3 && isSanh) {
            return 0.8 * (double) (11 - c - b + 1) / 12.0;
        }
        if (b == 4 && !isSanh) {
            return 0.95;
        }
        if (b == 4 && isSanh) {
            return 0.7 * (double) (11 - c - b + 1) / 12.0;
        }
        if (b == 5) {
            return 0.5 * (double) (11 - c - b + 1) / 12.0;
        }
        return 0.2 * (double) (11 - c - b + 1) / 12.0;
    }

    public double xacXuatChanDuoc(int b, int c, boolean a) {
        return (double) c / 12.0;
    }

    public double xacXuatTuonBai(int b, int c, boolean a) {
        return 0.0;
    }
}

