/*
 * Decompiled with CFR 0.150.
 */
package game.sam.server.logic.ai;

import java.util.ArrayList;
import java.util.List;

public class SamLogicHelper {
    public static List<SamCard> recommend(List<SamCard> inCards, List<SamCard> cardHandon, SamCard select) {
        int len = inCards.size();
        CardGroup groupA = new CardGroup(inCards);
        int num2 = 0;
        for (SamCard card : cardHandon) {
            if (card.so != 12) continue;
            ++num2;
        }
        List<SamCard> recommend = new ArrayList<>();
        if (num2 + inCards.size() >= cardHandon.size() && select.so != 12 && num2 > 0) {
            return recommend;
        }
        switch (groupA.typeGroup) {
            case 1: {
                if (inCards.size() == 1 && inCards.get(0).so == 12) {
                    ArrayList<SamCard> res = new ArrayList<>();
                    for (SamCard samCard : cardHandon) {
                        if (samCard.so != select.so) continue;
                        res.add(samCard);
                    }
                    if (res.size() == 4) {
                        return res;
                    }
                    if (select.so > groupA.cards.get(0).so) {
                        return recommend;
                    }
                    return recommend;
                }
                if (inCards.size() != 1) break;
                if (select.so > groupA.cards.get(0).so) {
                    recommend.add(new SamCard(select.id));
                }
                return recommend;
            }
            case 2:
            case 3: {
                ArrayList<SamCard> res = new ArrayList<>();
                res.add(select);
                for (SamCard samCard : cardHandon) {
                    if (samCard.so != select.so || samCard.id == select.id || res.size() >= inCards.size())
                        continue;
                    res.add(samCard);
                }
                if (res.size() == inCards.size() && res.get(len - 1).so > inCards.get(len - 1).so) {
                    return res;
                }
                return recommend;
            }
            case 5: {
                return recommend;
            }
            case 4: {
                recommend = SamLogicHelper.timsanhdocchatduoc(inCards, cardHandon, select);
                return recommend;
            }
        }
        return recommend;
    }

    public static List<SamCard> timsanhdocchatduoc(List<SamCard> cards, List<SamCard> handOn, SamCard select) {
        CardGroup group = new CardGroup(cards);
        List<SamCard> cardArray = group.makeSanhArray();
        int len = cardArray.size();
        ArrayList<SamCard> res = new ArrayList<>();
        res.add(select);
        ArrayList<SamCard> res2;
        for (int j = -2; j <= 9; ++j) {
            int countSanh = 0;
            res2 = new ArrayList<>();
            boolean holdSelect = false;
            if ((double) (j + len - 1) <= Math.floor(cardArray.get(len - 1).id / 4)) continue;
            for (int i = 0; i < cardArray.size(); ++i) {
                boolean res3 = false;
                for (SamCard samCard : handOn) {
                    int kkk;
                    int kk = SamCard.convertSo(samCard.so);
                    if (kk != (kkk = SamCard.convertSo(j + i)) || j + i >= 12) continue;
                    res3 = true;
                    if (samCard.so == select.so) {
                        res2.add(select);
                        holdSelect = true;
                        break;
                    }
                    res2.add(samCard);
                    break;
                }
                if (!res3) continue;
                ++countSanh;
            }
            if (countSanh != cardArray.size() || !holdSelect) continue;
            res = res2;
            break;
        }
        return res;
    }
}

