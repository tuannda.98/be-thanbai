/*
 * Decompiled with CFR 0.150.
 */
package game.sam.server.logic;

import java.util.Arrays;

public class LogicTest {
    public static void main(String[] args) {
        LogicTest.testGroupCard();
    }

    public static void testGroupCard() {
        Card[] card1 = new Card[]{
                new Card(0, 0),
                new Card(1, 0),
                new Card(2, 0),
                new Card(3, 2),
                new Card(4, 1)};
        Card[] card2 = new Card[]{
                new Card(0, 0),
                new Card(2, 1),
                new Card(2, 0),
                new Card(3, 2),
                new Card(4, 1)};
        GroupCard gc1 = new GroupCard(Arrays.asList(card1));
        System.out.println(gc1);
        GroupCard gc2 = new GroupCard(Arrays.asList(card2));
        System.out.println(gc2);
        System.out.println(gc2.chatDuoc(gc1));
    }

    public static void testCard() {
        for (int i = 0; i < 4; i = (byte) (i + 1)) {
            Card c = new Card(i);
            System.out.println(c);
        }
    }
}
