/*
 * Decompiled with CFR 0.150.
 */
package game.sam.server.logic.ai;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class CardGroup {
    public static final int TYPENONE = -1;
    public static final int TYPEMOTLA = 1;
    public static final int TYPEDOI = 2;
    public static final int TYPEBALA = 3;
    public static final int TYPESANH = 4;
    public static final int TYPETUQUY = 5;
    public static final int TYPEHAITUQUY = 6;
    public final List<SamCard> cards;
    public int typeGroup;
    final Comparator<SamCard> sortTangDan = Comparator.comparingInt(o -> o.id);

    public CardGroup(List<SamCard> cards) {
        this.cards = cards;
        this.typeGroup = -1;
        this.initCards();
    }

    public void initCards() {
        int i;
        if (this.cards.size() == 0) {
            return;
        }
        int size = this.cards.size();
        this.cards.sort(this.sortTangDan);
        this.typeGroup = -1;
        if (size == 1) {
            this.typeGroup = 1;
            return;
        }
        if (size == 2) {
            if (this.cards.get(0).so == this.cards.get(1).so) {
                this.typeGroup = 2;
                return;
            }
            return;
        }
        if (size == 3) {
            if (this.cards.get(0).so == this.cards.get(1).so && this.cards.get(0).so == this.cards.get(2).so) {
                this.typeGroup = 3;
                return;
            }
        } else if (size == 4) {
            if (this.cards.get(0).so == this.cards.get(1).so
                    && this.cards.get(0).so == this.cards.get(2).so
                    && this.cards.get(0).so == this.cards.get(3).so) {
                this.typeGroup = 5;
                return;
            }
        } else if (size == 8
                && this.cards.get(0).so == this.cards.get(1).so
                && this.cards.get(0).so == this.cards.get(2).so
                && this.cards.get(0).so == this.cards.get(3).so
                && this.cards.get(4).so == this.cards.get(5).so
                && this.cards.get(4).so == this.cards.get(6).so
                && this.cards.get(4).so == this.cards.get(7).so) {
            this.typeGroup = 6;
            return;
        }
        boolean sanh = false;
        int so = this.cards.get(0).so;
        int countSanh = 0;
        for (int i2 = 1; i2 < this.cards.size(); ++i2) {
            if (this.cards.get(i2).so - 1 != so) continue;
            so = this.cards.get(i2).so;
            ++countSanh;
        }
        if (countSanh == this.cards.size() - 1 && countSanh >= 2 && this.cards.get(this.cards.size() - 1).so != 12) {
            sanh = true;
        }
        ArrayList<SamCard> cardSortNguoc = new ArrayList<>();
        for (i = 0; i < this.cards.size(); ++i) {
            if (this.cards.get(i).so == 11 || this.cards.get(i).so == 12) continue;
            cardSortNguoc.add(this.cards.get(i));
        }
        for (i = 0; i < this.cards.size(); ++i) {
            cardSortNguoc.add(0, this.cards.get(i));
        }
        for (i = 0; i < this.cards.size(); ++i) {
            cardSortNguoc.add(0, this.cards.get(i));
        }
        countSanh = 0;
        so = cardSortNguoc.get(0).so;
        for (i = 1; i < cardSortNguoc.size(); ++i) {
            if (SamCard.convertSo(cardSortNguoc.get(i).so) != SamCard.convertSo(so + 1)) continue;
            so = cardSortNguoc.get(i).so;
            ++countSanh;
        }
        if (countSanh == this.cards.size() - 1 && countSanh >= 2) {
            sanh = true;
        }
        if (sanh) {
            this.typeGroup = 4;
        }
    }

    public Boolean isSanhNguoc() {
        for (SamCard card : this.cards) {
            if (card.so != 12) continue;
            return true;
        }
        return false;
    }

    public List<SamCard> makeSanhArray() {
        if (this.isSanhNguoc()) {
            return this.makeSanhNguocArray();
        }
        ArrayList<SamCard> res = new ArrayList<>(this.cards);
        return res;
    }

    public List<SamCard> makeSanhNguocArray() {
        int i;
        ArrayList<SamCard> res = new ArrayList<>();
        for (i = 0; i < this.cards.size(); ++i) {
            if (this.cards.get(i).so == 11 || this.cards.get(i).so == 12) continue;
            res.add(this.cards.get(i));
        }
        for (i = 0; i < this.cards.size(); ++i) {
            if (this.cards.get(i).so != 12) continue;
            res.add(0, this.cards.get(i));
        }
        for (i = 0; i < this.cards.size(); ++i) {
            if (this.cards.get(i).so != 11) continue;
            res.add(0, this.cards.get(i));
        }
        return res;
    }

    public Boolean isSanhToiCot() {
        if (this.typeGroup != 4) {
            return false;
        }
        List<SamCard> res = this.makeSanhArray();
        return res.get((int) (res.size() - 1)).so == 11;
    }
}

