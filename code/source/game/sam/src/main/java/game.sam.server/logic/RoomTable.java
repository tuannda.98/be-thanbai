/*
 * Decompiled with CFR 0.150.
 *
 * Could not load the following classes:
 *  bitzero.util.common.business.Debug
 *  game.modules.gameRoom.entities.GameRoomIdGenerator
 *  game.utils.GameUtils
 */
package game.sam.server.logic;

import bitzero.util.common.business.Debug;
import game.modules.gameRoom.entities.GameRoomIdGenerator;
import game.utils.GameUtils;

import java.util.LinkedList;

public class RoomTable {
    public final CardSuit suit = new CardSuit();
    public final LinkedList<Round> gameRounds = new LinkedList<>();
    public boolean baosam = false;
    public boolean toitrang = false;
    public int id = RoomTable.getID();
    public boolean isCheat = false;
    public long logTime = System.currentTimeMillis();

    private static int getID() {
        return GameRoomIdGenerator.getId();
    }

    public void makeRound() {
        Round round = new Round();
        this.gameRounds.add(round);
    }

    public Round getCurrentRound() {
        if (this.gameRounds.size() == 0) {
            Debug.trace("Make first round in game");
            this.makeRound();
        }
        return this.gameRounds.getLast();
    }

    public Round getLastRound() {
        if (this.gameRounds.size() == 0) {
            return null;
        }
        return this.gameRounds.getLast();
    }

    public void reset() {
        this.id = RoomTable.getID();
        this.logTime = System.currentTimeMillis();
        if (!this.isCheat || !GameUtils.isCheat) {
            this.suit.setRandom();
        }
        this.gameRounds.clear();
        this.baosam = false;
        this.toitrang = false;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.id).append("/");
        sb.append("Baosam").append(this.baosam).append("/");
        sb.append("Toitrang").append(this.toitrang).append("/");
        for (Round r : this.gameRounds) {
            sb.append(r.toString()).append("/");
        }
        return sb.toString();
    }

    public boolean isNewRound() {
        Round r = this.getLastRound();
        if (r != null) {
            return r.turns.size() == 0;
        }
        return true;
    }
}
