package game.sam.server.logic;

public enum WhiteWin {
    SANH(1),
    NAM_DOI(2),
    TU_HEO(3),
    DONG_MAU(4);
    public final int id;

    WhiteWin(int id) {
        this.id = id;
    }
}
