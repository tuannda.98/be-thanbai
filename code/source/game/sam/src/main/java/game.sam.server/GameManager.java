/*
 * Decompiled with CFR 0.150.
 */
package game.sam.server;

import game.sam.server.cmd.send.SendChonSam;
import game.sam.server.cmd.send.SendFirstTurnDecision;
import game.sam.server.cmd.send.SendToiTrang;
import game.sam.server.cmd.send.SendUpdateAutoStart;
import game.sam.server.logic.Card;
import game.sam.server.logic.GroupCard;
import game.sam.server.logic.RoomTable;

import java.util.List;

public class GameManager {
    private enum GameState {
        GS_NO_START(0), GS_GAME_PLAYING(1), GS_GAME_END(3);

        int id;

        GameState(int i) {
            id = i;
        }
    }

    private static final int DEM_LA = 1;

    private static final int NO_ACTION = -1;
    private static final int KIEM_TRA_DI_DAU = 0;
    private static final int CHIA_BAI = 1;
    private static final int BAO_SAM = 2;
    private static final int TOI_TRANG = 3;
    private static final int CHOI_BAI = 4;
    private static final int MOI_DI_DAU = 5;

    protected int roomOwnerChair = 5;
    protected int roomCreatorUserId;
    private int rCuocLon = 1;
    private GameState gameState = GameState.GS_NO_START;
    protected int gameAction = -1;
    protected int countDown = 0;
    private boolean isAutoStart = false;
    protected volatile int currentChair;
    protected volatile int prevChair;
    protected volatile int nextChair;
    protected final RoomTable roomTable = new RoomTable();
    protected final SamGameServer gameServer;
    protected final GameLogic logic = new GameLogic();

    public GameManager(SamGameServer gameServer) {
        this.gameServer = gameServer;
    }


    protected boolean isGameStateNoStart() {
        return this.gameState == GameState.GS_NO_START;
    }

    protected boolean isGameStatePlaying() {
        return this.gameState == GameState.GS_GAME_PLAYING;
    }

    private boolean isGameStateEnd() {
        return this.gameState == GameState.GS_GAME_END;
    }

    protected int getGameStateId() {
        return this.gameState.id;
    }

    protected void setGameStateEndGame() {
        this.gameState = GameState.GS_GAME_END;
    }

    protected void setGameStateNoStart() {
        this.gameState = GameState.GS_NO_START;
    }

    protected void prepareNewGame() {
        this.roomTable.reset();
        this.isAutoStart = false;
        this.gameServer.kiemTraTuDongBatDau(5);
        this.gameServer.botJoinRoom();
    }

    protected void gameLoop() {
        synchronized (this.gameServer) {
            if (this.isGameStateNoStart() && this.isAutoStart) {
                --this.countDown;
                if (this.countDown <= 0) {
                    this.gameState = GameState.GS_GAME_PLAYING;
                    this.gameServer.start();
                }
            } else if (this.isGameStatePlaying()) {
                if (this.gameAction != -1) {
                    --this.countDown;
                    if (this.gameAction == 2 && this.countDown == 5) {
                        this.gameServer.botBaoSam();
                    }
                    if (this.gameAction == 4 && this.countDown == 17) {
                        this.gameServer.botAutoPlay();
                    }
                    if (this.countDown <= 0) {
                        if (this.gameAction == 0) {
                            this.kiemTraDiDau();
                        } else if (this.gameAction == 1) {
                            this.chiaBai();
                        } else if (this.gameAction == 2) {
                            if (this.countDown == 5) {
                                this.gameServer.botBaoSam();
                            }
                            this.chonSam();
                        } else if (this.gameAction == 5) {
                            this.moididau(20);
                        } else if (this.gameAction == 3) {
                            this.toitrang();
                        } else if (this.gameAction == 4) {
                            this.tudongChoi();
                        }
                    }
                }
            } else if (this.isGameStateEnd()) {
                --this.countDown;
                if (this.countDown == 5) {
                    this.gameServer.notifyNoHu();
                }
                if (this.countDown <= 0) {
                    this.gameServer.pPrepareNewGame();
                }
            } else {
                this.gameServer.botJoinRoom();
            }
        }
    }

    private void notifyAutoStartToUsers(int after) {
        SendUpdateAutoStart msg = new SendUpdateAutoStart();
        msg.isAutoStart = this.isAutoStart;
        msg.autoStartTime = (byte) after;
        this.gameServer.sendMsg(msg);
    }

    protected void cancelAutoStart() {
        this.isAutoStart = false;
        this.notifyAutoStartToUsers(0);
    }

    protected void makeAutoStart(int after) {
        if (!this.isGameStateNoStart()) {
            return;
        }
        if (!this.isAutoStart) {
            this.countDown = after;
        } else if (after < this.countDown) {
            this.countDown = after;
        } else {
            after = this.countDown;
        }
        this.isAutoStart = true;
        this.notifyAutoStartToUsers(after);
    }

    private void kiemTraDiDau() {
        SendFirstTurnDecision msg = new SendFirstTurnDecision();
        int firstChair = this.gameServer.isNeedRandomFirstTurn();
        if (firstChair >= 0) {
            msg.isRandom = false;
            this.countDown = 0;
        } else {
            msg.isRandom = true;
            msg.cards = this.logic.genFirstTurn();
            this.xacDinhDiDau(msg.cards);
            this.countDown = 3;
        }
        this.currentChair = this.logic.firstTurn;
        msg.chair = (byte) this.logic.firstTurn;
        this.gameServer.logQuyetDinhDiDau(msg);
        this.gameServer.sendMsgToPlayingUser(msg);
        this.gameAction = 1;
    }

    private void xacDinhDiDau(byte[] cards) {
        boolean flag = false;
        for (int i = 0; i < 5; ++i) {
            GamePlayer gp = this.gameServer.getPlayerByChair(i);
            if (!gp.isPlaying()) continue;
            if (!flag) {
                this.logic.firstTurn = i;
                flag = true;
                continue;
            }
            if (Card.sosanhLabai(cards[i], cards[this.logic.firstTurn]) <= 0) continue;
            this.logic.firstTurn = i;
        }
    }

    private void chiaBai() {
        List<GroupCard> cards = this.roomTable.suit.dealCards();
        for (int i = 0; i < 5; ++i) {
            GamePlayer gp = this.gameServer.playerList.get(i);
            gp.addCards(cards.get(i));
        }
        this.gameServer.chiabai();
        this.gameAction = 2;
        this.countDown = 12;
    }

    private void moididau(int countdown) {
        this.gameServer.notifyChangeTurn(true);
        this.gameAction = 4;
        this.countDown = countdown;
    }

    private void chonSam() {
        byte chair = this.gameServer.getBaoSam();
        if (chair >= 0 && chair < 5) {
            GamePlayer gp = this.gameServer.getPlayerByChair(chair);
            if (gp.kiemTraToiTrang() != null) {
                this.roomTable.toitrang = true;
                this.gameServer.endGame();
            } else {
                SendChonSam msg = new SendChonSam();
                msg.baosam = true;
                msg.chair = chair;
                this.gameServer.sendMsg(msg);
                this.gameAction = 5;
                this.countDown = 3;
            }
        } else {
            SendChonSam msg = new SendChonSam();
            msg.baosam = false;
            msg.chair = (byte) this.currentChair;
            this.gameServer.sendMsg(msg);
            this.gameAction = 5;
            this.countDown = 0;
        }
    }

    private void toitrang() {
        byte chair = this.gameServer.getToiTrang();
        if (chair >= 0) {
            SendToiTrang msg = new SendToiTrang();
            msg.chair = chair;
            this.gameServer.sendMsg(msg);
            this.roomTable.toitrang = true;
            this.gameServer.endGame();
        } else {
            this.moididau(20);
        }
    }

    private void tudongChoi() {
        this.gameServer.tudongChoi();
    }

    protected int currentChair() {
        return this.currentChair;
    }

    private boolean canOutRoom() {
        return this.getGameStateId() == 0;
    }
}
