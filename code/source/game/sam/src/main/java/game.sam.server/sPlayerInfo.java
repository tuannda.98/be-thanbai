/*
 * Decompiled with CFR 0.150.
 */
package game.sam.server;

import game.sam.server.logic.GroupCard;
import java.util.ArrayList;
import java.util.List;

public class sPlayerInfo {
    public GroupCard handCards;
    public final List<GroupCard> thrownCards = new ArrayList<>();
    public boolean isNoHu = false;

    public boolean isEndCards() {
        return this.handCards.cards.size() == 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Trentay: ").append(this.handCards.toString()).append("\n");
        sb.append("DanhRa: ");
        for (GroupCard gc : this.thrownCards) {
            sb.append(gc.toString()).append("\n");
        }
        return sb.toString();
    }

    public boolean kiemTraNoHu() {
        if (this.isNoHu) {
            this.isNoHu = false;
            return true;
        }
        this.isNoHu = this.handCards.isNoHu();
        return this.isNoHu;
    }
}

