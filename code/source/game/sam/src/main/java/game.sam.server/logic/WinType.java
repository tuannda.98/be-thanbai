package game.sam.server.logic;

public enum WinType {
    KHONG_CHOI(1),
    THANG_THONG_THUONG(2),
    THANG_SAM(3),
    THANG_CHAN_SAM(4),
    THANG_BAT_TREO(5),
    THANG_TRANG_SAM_DINH(6),
    THANG_TRANG_TU_HEO(7),
    THANG_TRANG_NAM_DOI(8),
    THANG_TRANG_DONG_MAU(9),
    THANG_DEN_BAO_MOT(10),

    THUA_DEN_BAO_MOT(11),
    KET_QUA_HOA(12),
    THUA_THONG_THUONG(13),
    THUA_TREO(14),
    THUA_TOI_TRANG(15),
    THUA_DEN_CHAN_SAM(16);

    public final byte id;

    WinType(int id) {
        this.id = (byte) id;
    }
}
