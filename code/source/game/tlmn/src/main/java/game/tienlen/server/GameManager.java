/*
 * Decompiled with CFR 0.144.
 *
 * Could not load the following classes:
 *  bitzero.server.extensions.data.BaseMsg
 */
package game.tienlen.server;

import casio.king365.GU;
import casio.king365.util.KingUtil;
import game.tienlen.server.cmd.send.SendFirstTurnDecision;
import game.tienlen.server.cmd.send.SendToiTrang;
import game.tienlen.server.cmd.send.SendUpdateAutoStart;
import game.tienlen.server.logic.Card;
import game.tienlen.server.logic.Gamble;
import game.tienlen.server.logic.GroupCard;

import java.util.List;

public class GameManager {
    protected static final int DEM_LA = 1;
    protected static final int GS_NO_START = 0;
    protected static final int GS_GAME_PLAYING = 1;
    protected static final int GS_GAME_END = 3;
    protected static final int NO_ACTION = -1;
    protected static final int KIEM_TRA_DI_DAU = 0;
    protected static final int CHIA_BAI = 1;
    protected static final int TOI_TRANG = 2;
    protected static final int CHOI_BAI = 3;
    protected static final int MOI_DI_DAU = 4;
    protected static final int CHO_BON_DOI_THONG = 5;
    protected static final int DANH_BON_DOI_THONG = 6;
    protected int roomOwnerChair = 4;
    protected int roomCreatorUserId;
    protected int rCuocLon = 1;
    protected int gameState = 0;
    protected int gameAction = -1;
    protected int countDown = 0;
    protected boolean isAutoStart = false;
    protected volatile int currentChair;
    protected volatile int prevChair;
    protected volatile int prevChairThrowCard = 4;
    protected volatile int nextChair;
    protected Gamble game = new Gamble();
    protected final TienlenGameServer gameServer;
    protected GameLogic logic = new GameLogic();

    public GameManager(TienlenGameServer gameServer) {
        this.gameServer = gameServer;
    }

    protected int getGameState() {
        return this.gameState;
    }

    void prepareNewGame() {
        this.game.reset();
        this.isAutoStart = false;
        this.prevChairThrowCard = 4;
        this.gameServer.kiemTraTuDongBatDau(5);
    }

    void gameLoop() {
        synchronized (gameServer) {
            if (this.gameState == 0 && this.isAutoStart) {
                --this.countDown;
                if (this.countDown <= 0) {
                    this.gameState = 1;
                    this.gameServer.start();
                }
            } else if (this.gameState == 1) {
                if (this.gameAction != -1) {
                    --this.countDown;
                    if (this.countDown <= 0) {
                        if (this.gameAction == 0) {
                            this.kiemTraDiDau();
                        } else if (this.gameAction == 1) {
                            this.chiaBai();
                        } else if (this.gameAction == 2) {
                            this.toitrang();
                        } else if (this.gameAction == 3) {
                            this.tudongChoi();
                        } else if (this.gameAction == 5) {
                            this.gameServer.changeTurnNewRound();
                        } else if (this.gameAction == 6) {
                            this.gameServer.ketThucDanhBonDoiThong();
                        }
                    }
                }
            } else if (this.gameState == 3) {
                --this.countDown;
                if (this.countDown == 5) {
                    this.gameServer.notifyNoHu();
                }
                if (this.countDown <= 0) {
                    this.gameServer.pPrepareNewGame();
                }
            }
        }
    }

    private void notifyAutoStartToUsers(int after) {
        SendUpdateAutoStart msg = new SendUpdateAutoStart();
        msg.isAutoStart = this.isAutoStart;
        msg.autoStartTime = (byte) after;
        this.gameServer.send(msg);
    }

    void cancelAutoStart() {
        this.isAutoStart = false;
        this.notifyAutoStartToUsers(0);
    }

    void makeAutoStart(int after) {
        if (this.gameState != 0) {
            return;
        }
        if (!this.isAutoStart) {
            this.countDown = after;
        } else if (after < this.countDown) {
            this.countDown = after;
        } else {
            after = this.countDown;
        }
        this.isAutoStart = true;
        this.notifyAutoStartToUsers(after);
    }

    private void kiemTraDiDau() {
        SendFirstTurnDecision msg = new SendFirstTurnDecision();
        int firstChair = this.gameServer.isNeedRandomFirstTurn();
        if (firstChair >= 0) {
            msg.isRandom = false;
            this.countDown = 0;
        } else {
            msg.isRandom = true;
            msg.cards = this.logic.genFirstTurn();
            this.xacDinhDiDau(msg.cards);
            this.countDown = 3;
        }
        this.currentChair = this.logic.firstTurn;
        msg.chair = (byte) this.logic.firstTurn;
        for (int i = 0; i < 5; ++i) {
            GamePlayer gp = gameServer.getPlayerByChair(i);
            if(gp == null) {
                msg.playerStatus[i] = 0;
                continue;
            }
            msg.playerStatus[i] = (byte) gp.getPlayerStatus();
        }
        this.gameServer.logQuyetDinhDiDau(msg);
        this.gameServer.sendMsgToPlayingUser(msg);
        this.gameAction = 1;
    }

    private void xacDinhDiDau(byte[] cards) {
        boolean flag = false;
        for (int i = 0; i < 4; ++i) {
            GamePlayer gp = this.gameServer.getPlayerByChair(i);
            if (!gp.isPlaying()) continue;
            if (!flag) {
                this.logic.firstTurn = i;
                flag = true;
                continue;
            }
            if (Card.sosanhLabai(cards[i], cards[this.logic.firstTurn]) <= 0) continue;
            this.logic.firstTurn = i;
        }
    }

    private void chiaBai() {
        List<GroupCard> cards = this.game.suit.dealCards();
        for (int i = 0; i < 4; ++i) {
            GamePlayer gp = this.gameServer.playerList.get(i);
            gp.addCards(cards.get(i));
        }
        this.gameServer.chiabai();
        this.gameAction = 2;
        this.countDown = 2;
    }

    private void moididau(int countdown) {
        this.gameServer.notifyChangeTurn(true);
        this.gameAction = 3;
        this.countDown = countdown;
    }

    private void toitrang() {
        byte chair = this.gameServer.getToiTrang();
        if (chair >= 0) {
            SendToiTrang msg = new SendToiTrang();
            msg.chair = chair;
            this.gameServer.send(msg);
            this.game.toitrang = true;
            this.gameServer.endGame();
        } else {
            this.moididau(20);
        }
    }

    private void tudongChoi() {
        this.gameServer.tudongChoi();
    }

    int currentChair() {
        return this.currentChair;
    }

    private boolean canOutRoom() {
        return this.getGameState() == 0;
    }
}
