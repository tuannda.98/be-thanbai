package monitor.controller;

import game.modules.gameRoom.entities.GameRoomManager;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("room")
public class RoomController {
    @GetMapping("index")
    public String ListRoom() {
        GameRoomManager ins = GameRoomManager.instance();
        return ins.toString();
    }
}