/*
 * Decompiled with CFR 0.150.
 *
 * Could not load the following classes:
 *  bitzero.server.entities.User
 *  bitzero.server.extensions.data.BaseMsg
 *  bitzero.server.extensions.data.DataCmd
 *  bitzero.util.ExtensionUtility
 *  bitzero.util.common.business.CommonHandle
 *  org.json.JSONObject
 */
package game.modules.gameRoom.entities;

import bitzero.server.BitZeroServer;
import bitzero.server.entities.User;
import bitzero.server.extensions.data.BaseMsg;
import bitzero.server.extensions.data.DataCmd;
import bitzero.util.ExtensionUtility;
import bitzero.util.common.business.CommonHandle;
import game.utils.GameUtils;
import game.utils.NotificationUtils;
import org.json.JSONObject;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public abstract class GameServer {
    public static final User.PropertyKey<Integer> USER_CHAIR = new User.PropertyKey<>("user_chair");
    protected final GameRoom gameRoom;
    protected volatile boolean isRegisterLoop = false;
    protected ScheduledFuture<?> task;

    public GameServer(GameRoom room) {
        this.gameRoom = room;
    }

    public static GameServer createNewGameServer(GameRoom room) {
        String gameServerClassPath = GameUtils.gameServerClassPath;
        try {
            GameServer mainServer = (GameServer) Class.forName(gameServerClassPath).getDeclaredConstructor(GameRoom.class).newInstance(room);
            mainServer.init();
            return mainServer;
        } catch (Exception e) {
            CommonHandle.writeErrLog(e);
            return null;
        }
    }

    public abstract void onGameUserReturn(User var1);

    public abstract void onGameUserEnter(User var1);

    public abstract void onGameUserExit(User var1);

    public abstract void onGameUserDis(User var1);

    public abstract void onGameMessage(User var1, DataCmd var2);

    public abstract String toString();

    public abstract JSONObject toJONObject();

    public abstract void onNoHu(ThongTinThangLon var1);

    public abstract void choNoHu(String var1);

    protected abstract Runnable getGameLoopTask();

    protected Runnable monitoredRunnable() {
        return () -> {
            ScheduledFuture future = BitZeroServer.getInstance().getTaskScheduler().schedule(() -> {
                NotificationUtils.sendOperation("[CẢNH BÁO]Xử lý game loop QUÁ chậm, > 3s: " + this.toJONObject());
            }, 3, TimeUnit.SECONDS);
            LocalDateTime start = LocalDateTime.now();

            getGameLoopTask().run();

            Duration duration = Duration.between(start, LocalDateTime.now());
            long durationInMs = duration.toMillis();
            if (durationInMs > 500) {
                NotificationUtils.sendOperation("[CẢNH BÁO]Xử lý game loop chậm: " + this.toJONObject() + " - " + durationInMs + " ms.");
            }

            if (!future.isDone()) {
                future.cancel(true);
            }
        };
    }

    public void init() {
        synchronized (this) {
            if (!this.isRegisterLoop) {
                this.task = BitZeroServer.getInstance().getTaskScheduler().scheduleAtFixedRate(
                        monitoredRunnable(), 0, 1, TimeUnit.SECONDS);
                this.isRegisterLoop = true;
            }
        }
    }

    public void destroy() {
        this.task.cancel(false);
        this.isRegisterLoop = false;
    }

    public void sendMsgExceptMe(BaseMsg msg, User user) {
        if (user == null || this.gameRoom == null) return;

        this.gameRoom.userManager.values()
                .stream()
                .filter(user1 -> !user1.getName().equalsIgnoreCase(user.getName()))
                .forEach(user1 -> ExtensionUtility.getExtension().send(msg, user1));
    }

    public void send(BaseMsg cmd, User user) {
        if (user != null) {
            ExtensionUtility.getExtension().send(cmd, user);
        }
    }

    public boolean isPlaying() {
        return false;
    }

    public GameRoom getGameRoom() {
        return gameRoom;
    }
}
