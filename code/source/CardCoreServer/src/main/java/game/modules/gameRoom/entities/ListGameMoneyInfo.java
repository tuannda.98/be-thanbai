package game.modules.gameRoom.entities;

import bitzero.server.BitZeroServer;
import bitzero.util.common.business.CommonHandle;
import bitzero.util.common.business.Debug;
import casio.king365.GU;
import com.vinplay.usercore.service.impl.MoneyInGameServiceImpl;
import com.vinplay.vbee.common.models.FreezeModel;
import game.entities.Constant;
import game.utils.GameUtils;
import game.utils.NotificationUtils;

import java.text.ParseException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ListGameMoneyInfo {
    private static ListGameMoneyInfo ins = null;

    public static ListGameMoneyInfo instance() {
        if (ins == null) {
            ins = new ListGameMoneyInfo();
            ins.init();
            ins.startFreezeMoneyMaintenanceThread();
        }
        return ins;
    }

    private void startFreezeMoneyMaintenanceThread() {
        BitZeroServer.getInstance().getTaskScheduler().scheduleAtFixedRate(() -> {
            Debug.info("FreezeMoneyMaintenanceThread start: " + GameUtils.gameName);
            MoneyInGameServiceImpl moneyService = new MoneyInGameServiceImpl();

            try {
                List<FreezeModel> freezeModels = moneyService.getListFreeze(
                        GameUtils.gameName, "", Constant.MONEY_NAME_VIN, "", "", 0, true);

                freezeModels.stream()
                        .filter(freezeModel -> {
                            GameRoom room = GameRoomManager.instance().getGameRoomById(Integer.parseInt(freezeModel.getRoomId()));
                            if (room == null) return true;
                            return !room.hasUser(freezeModel.getNickname());
                        })
                        .filter(freezeModel -> freezeModel.getLastActiveTime() == null || Duration.between(freezeModel.getLastActiveTime(), LocalDateTime.now()).toMinutes() > 30)
                        .forEach(freezeModel -> moneyService.restoreFreeze(freezeModel.getSessionId()));
            } catch (ParseException e) {
                GU.sendOperation(GameUtils.gameName + "-FreezeMoneyMaintenanceThread " + e.getMessage());
            }

            Debug.info("FreezeMoneyMaintenanceThread done: " + GameUtils.gameName);
        }, 3, 30, TimeUnit.SECONDS);
    }

    public void removeGameMoneyInfo(GameMoneyInfo info, int roomId) {
        try {
            info.restoreMoney(roomId);
        } catch (Exception e) {
            CommonHandle.writeErrLog(e);
            NotificationUtils.sendOperation("removeGameMoneyInfo: " + e);
        }
    }

    private ListGameMoneyInfo() {
    }

    private void init() {
        MoneyInGameServiceImpl moneyService = new MoneyInGameServiceImpl();
        try {
            Debug.trace("restoreFreeze start: " + GameUtils.gameName);
            moneyService.restoreFreeze("*", GameUtils.gameName, "*", "*", 120);
            Debug.trace("restoreFreeze done: " + GameUtils.gameName);
        } catch (ParseException e) {
            CommonHandle.writeErrLog(e);
        }
    }
}
