package game.modules.gameRoom.entities;

import bitzero.server.core.BZEvent;
import bitzero.server.entities.User;
import bitzero.util.ExtensionUtility;
import bitzero.util.common.business.CommonHandle;
import com.vinplay.usercore.service.UserService;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import game.eventHandlers.GameEventParam;
import game.eventHandlers.GameEventType;
import game.modules.bot.BotManager;
import game.modules.gameRoom.cmd.rev.JoinGameRoomCmd;
import game.modules.gameRoom.cmd.send.JoinGameRoomFailMsg;
import game.modules.gameRoom.config.GameRoomConfig;
import game.utils.GameUtils;
import game.utils.LoggerUtils;
import game.xocdia.conf.XocDiaGameUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static game.modules.gameRoom.GameRoomModule.GAME_ROOM;
import static game.modules.gameRoom.entities.GameMoneyInfo.GAME_MONEY_INFO;

public class GameRoomManager {
    public static final String FREE_GROUP = "FIGHTING_GROUP";
    public static final int FREE_PLAYERS = 2;
    private static GameRoomManager ins = null;
    public final List<GameRoomSetting> roomSettingList = new ArrayList<>();
    public final ConcurrentHashMap<String, GameRoomGroup> gameRoomGroups = new ConcurrentHashMap<>();
    public final ConcurrentHashMap<Integer, GameRoom> allGameRooms = new ConcurrentHashMap<>();
    private final UserService userService = new UserServiceImpl();

    public static GameRoomManager instance() {
        if (ins == null) {
            ins = new GameRoomManager();
        }
        return ins;
    }

    private GameRoomManager() {
        this.init();
    }

    public void init() {
        JSONObject config = GameRoomConfig.instance().config;
        try {
            JSONArray roomConfigList = config.getJSONArray("roomList");
            GameRoomSetting setting = null;
            for (int i = 0; i < roomConfigList.length(); ++i) {
                JSONObject roomConfig = roomConfigList.getJSONObject(i);
                setting = new GameRoomSetting(roomConfig);
                this.roomSettingList.add(setting);

                GameRoomGroup group = new GameRoomGroup(setting);
                this.gameRoomGroups.put(setting.getSettingName(), group);
                this.createInitialRoom(group);
            }

            if (setting != null) {
                GameRoomSetting freeSetting = new GameRoomSetting(setting);
                freeSetting.maxUserPerRoom = 2;
                GameRoomGroup freeGroup = new GameRoomGroup(freeSetting);
                this.gameRoomGroups.put(FREE_GROUP, freeGroup);
            }
        } catch (JSONException e) {
            CommonHandle.writeErrLog(e);
        }
    }

    public void createInitialRoom(GameRoomGroup group) {
        for (int i = 0; i < group.setting.numberOfInitialRoom; ++i) {
            GameRoomSetting newSetting = new GameRoomSetting(group.setting);
            GameRoom room = new GameRoom(newSetting);
            this.allGameRooms.put(room.getId(), room);
            room.group = group;
            group.recycleRoom(room);
        }
    }

    public GameRoomGroup getGroup(GameRoomSetting setting) {
        return this.gameRoomGroups.get(setting.getSettingName());
    }

    public GameRoomGroup getGroupBySetting(String settingName) {
        return this.gameRoomGroups.get(settingName);
    }

    public GameRoom getGameRoomById(int roomId) {
        return this.allGameRooms.get(roomId);
    }

    public GameRoom createNewGameRoom(GameRoomSetting setting) {
        GameRoomSetting newSetting = new GameRoomSetting(setting);
        GameRoom room = new GameRoom(newSetting);
        this.allGameRooms.put(room.getId(), room);
        return room;
    }

    public GameRoom createNewGameRoom(GameRoomSetting setting, int roomId) {
        GameRoomSetting newSetting = new GameRoomSetting(setting);
        GameRoom room = new GameRoom(newSetting, roomId);
        this.allGameRooms.put(room.getId(), room);
        return room;
    }

    public GameRoom createEmptyGameRoom(GameRoomSetting setting) {
        GameRoomGroup freeGroups = this.gameRoomGroups.get(FREE_GROUP);
        GameRoom room = freeGroups.getEmptyRoom();
        room.setting = new GameRoomSetting(setting);
        this.allGameRooms.put(room.getId(), room);
        return room;
    }

    public GameRoomGroup getGroup(JoinGameRoomCmd cmd) {
        GameRoomSetting setting = new GameRoomSetting(cmd);
        return this.getGroup(setting);
    }

    public int getUserCount(GameRoomSetting setting) {
        return this.getGroup(setting).getUserCount();
    }

    public JSONObject toJSONObject() {
        try {
            JSONObject json = new JSONObject();
            for (GameRoomSetting setting : this.roomSettingList) {
                GameRoomGroup group = this.getGroup(setting);
                json.put(setting.getSettingName(), group.toJSONObject());
            }
            return json;
        } catch (Exception e) {
            return null;
        }
    }

    public String toString() {
        JSONObject json = this.toJSONObject();
        if (json != null) {
            return json.toString();
        }
        return "{}";
    }

    public int getRoomCount() {
        return this.allGameRooms.size();
    }

    public List<GameRoomSetting> getRoomConfigList() {
        return new ArrayList<>(this.roomSettingList);
    }

    //https://stackoverflow.com/a/51242340
    private static final Map<Integer, Object> locks = new ConcurrentHashMap<>();

    public void joinRoom(User user, GameRoom room, boolean isReconnect) {
        LoggerUtils.debug("tour", user.getName(), "joinRoom", "roomId = ", room.getId(), "is Full?", room.isFull(), "isReconect", isReconnect);
        synchronized (locks.computeIfAbsent(room.getId(), k -> new Object())) {
            if (room.isFull()) {
                boolean hasUserInside = room.hasUser(user.getName());
                if (hasUserInside) {
                    room.userManager.put(user.getName(), user);
                    user.setProperty(GAME_ROOM, room);
                    if (room.group != null) {
                        room.group.userManager.put(user.getName(), user);
                        room.group.recycleRoom(room);
                    }
                    this.dispatchEventJoinRoom(user, room, true);
                } else {
                    JoinGameRoomFailMsg msg = new JoinGameRoomFailMsg();
                    msg.Error = 9;
                    ExtensionUtility.instance().send(msg, user);
                    GameMoneyInfo moneyInfo = user.getProperty(GAME_MONEY_INFO);
                    if (moneyInfo != null) {
                        moneyInfo.restoreMoney(room.getId());
                    }
                }
            } else {
                room.userManager.put(user.getName(), user);
                user.setProperty(GAME_ROOM, room);
                if (room.group != null) {
                    room.group.userManager.put(user.getName(), user);
                    room.group.recycleRoom(room);
                }
                GameMoneyInfo moneyInfo = user.getProperty(GAME_MONEY_INFO);
                if (moneyInfo != null) {
                    moneyInfo.roomId = room.getId();
                }
                this.dispatchEventJoinRoom(user, room, isReconnect);
            }
        }
    }

    public void leaveRoom(User user, GameRoom room) {
        user.removeProperty(GAME_ROOM);
        room.userManager.remove(user.getName());
        if (room.group != null) {
            room.group.userManager.remove(user.getName());
            room.group.recycleRoom(room);
        }
        this.dispatchEventLeaveRoom(user, room);
    }

    public void leaveRoom(User user) {
        GameRoom room = user.getProperty(GAME_ROOM);
        if (room != null) {
            this.leaveRoom(user, room);
        }
    }

    private void dispatchEventJoinRoom(User user, GameRoom room, boolean isReconnect) {
        HashMap<GameEventParam, Object> evtParams = new HashMap<>();
        evtParams.put(GameEventParam.GAMEROOM, room);
        evtParams.put(GameEventParam.USER, user);
        evtParams.put(GameEventParam.IS_RECONNECT, isReconnect);
        ExtensionUtility.dispatchEvent(new BZEvent(GameEventType.GAME_ROOM_USER_JOIN, evtParams));
        LoggerUtils.debug("tour", "dispatchEventJoinRoom", user.getName(), "room", room.getId(), "isReconnect", isReconnect);
    }

    private void dispatchEventLeaveRoom(User user, GameRoom room) {
        HashMap<GameEventParam, Object> evtParams = new HashMap<>();
        evtParams.put(GameEventParam.GAMEROOM, room);
        evtParams.put(GameEventParam.USER, user);
        ExtensionUtility.dispatchEvent(new BZEvent(GameEventType.GAME_ROOM_USER_LEAVE, evtParams));
    }

    public GameRoom getGameRoomToJoin(int roomId, String password, User user) {
        boolean checkPass;
        JoinGameRoomFailMsg msg = new JoinGameRoomFailMsg();
        GameRoom room = this.allGameRooms.get(roomId);
        if (room == null) {
            msg.Error = 7;
            ExtensionUtility.instance().send(msg, user);
            return null;
        }
        if (room.isFull()) {
            msg.Error = 9;
            ExtensionUtility.instance().send(msg, user);
            return null;
        }
        boolean bl = checkPass = room.setting.password.length() == 0 || room.setting.password.equalsIgnoreCase(password);
        if (checkPass) {
            return room;
        }
        msg.Error = 8;
        ExtensionUtility.instance().send(msg, user);
        LoggerUtils.info("game_room", "joinGameRoom", user.getName(), roomId, password, "error:", msg.Error);
        return null;
    }

    public GameRoomGroup findSuitableGroup(User user, GameRoomSetting gameRoomSetting) {
        GameRoomGroup group;
        long moneyRequire;
        GameRoomSetting setting;
        int i;
        int moneyType = gameRoomSetting.moneyType;
        long maxUserPerRoom = gameRoomSetting.maxUserPerRoom;
        int rule = gameRoomSetting.rule;
        if (XocDiaGameUtils.isXocDia() && moneyType == 1) {
            return this.findSuitableGroupXocDia(user, moneyType);
        }
        String moneyTypeName = moneyType == 1 ? "vin" : "xu";
        long money = this.userService.getMoneyUserCache(user.getName(), moneyTypeName);
        long maxMoney = -1L;
        GameRoomGroup fitGroup = null;
        for (i = 0; i < this.roomSettingList.size(); ++i) {
            setting = this.roomSettingList.get(i);
            moneyRequire = setting.requiredMoney;
            if (moneyRequire == 0L && GameUtils.gameName.equalsIgnoreCase("Poker")) {
                moneyRequire = 40L * setting.moneyBet;
            } else if (moneyRequire == 0L && GameUtils.gameName.equalsIgnoreCase("Lieng")) {
                moneyRequire = 5L * setting.moneyBet;
            }
            if (setting.moneyType != moneyType || (long) setting.maxUserPerRoom != maxUserPerRoom || setting.rule != rule || moneyRequire > money)
                continue;
            group = this.gameRoomGroups.get(setting.setting_name);
            if (group.freeRooms.size() <= 0 || moneyRequire <= maxMoney) continue;
            maxMoney = moneyRequire;
            fitGroup = group;
        }
        if (fitGroup != null) {
            return fitGroup;
        }
        for (i = 0; i < this.roomSettingList.size(); ++i) {
            setting = this.roomSettingList.get(i);
            moneyRequire = setting.requiredMoney;
            if (moneyRequire == 0L && GameUtils.gameName.equalsIgnoreCase("Poker")) {
                moneyRequire = 40L * setting.moneyBet;
            } else if (moneyRequire == 0L && GameUtils.gameName.equalsIgnoreCase("Lieng")) {
                moneyRequire = 5L * setting.moneyBet;
            }
            if (setting.moneyType != moneyType || (long) setting.maxUserPerRoom != maxUserPerRoom || setting.rule != rule || moneyRequire > money)
                continue;
            group = this.gameRoomGroups.get(setting.setting_name);
            if (moneyRequire <= maxMoney) continue;
            maxMoney = moneyRequire;
            fitGroup = group;
        }
        return fitGroup;
    }

    public GameRoomGroup findSuitableGroup(User user, int moneyType) {
        GameRoomGroup group;
        long moneyRequire;
        GameRoomSetting setting;
        int i;
        if (XocDiaGameUtils.isXocDia() && moneyType == 1) {
            return this.findSuitableGroupXocDia(user, moneyType);
        }
        String moneyTypeName = moneyType == 1 ? "vin" : "xu";
        long money = this.userService.getMoneyUserCache(user.getName(), moneyTypeName);
        long maxMoney = -1L;
        GameRoomGroup fitGroup = null;
        for (i = 0; i < this.roomSettingList.size(); ++i) {
            setting = this.roomSettingList.get(i);
            moneyRequire = setting.requiredMoney;
            if (moneyRequire == 0L && GameUtils.gameName.equalsIgnoreCase("Poker")) {
                moneyRequire = 40L * setting.moneyBet;
            } else if (moneyRequire == 0L && GameUtils.gameName.equalsIgnoreCase("Lieng")) {
                moneyRequire = 5L * setting.moneyBet;
            }
            if (setting.moneyType != moneyType || moneyRequire > money) continue;
            group = this.gameRoomGroups.get(setting.setting_name);
            if (group.freeRooms.size() <= 0 || moneyRequire <= maxMoney) continue;
            maxMoney = moneyRequire;
            fitGroup = group;
        }
        if (fitGroup != null) {
            return fitGroup;
        }
        for (i = 0; i < this.roomSettingList.size(); ++i) {
            setting = this.roomSettingList.get(i);
            moneyRequire = setting.requiredMoney;
            if (moneyRequire == 0L && GameUtils.gameName.equalsIgnoreCase("Poker")) {
                moneyRequire = 40L * setting.moneyBet;
            } else if (moneyRequire == 0L && GameUtils.gameName.equalsIgnoreCase("Lieng")) {
                moneyRequire = 5L * setting.moneyBet;
            }
            if (setting.moneyType != moneyType || moneyRequire > money) continue;
            group = this.gameRoomGroups.get(setting.setting_name);
            if (moneyRequire <= maxMoney) continue;
            maxMoney = moneyRequire;
            fitGroup = group;
        }
        return fitGroup;
    }

    public GameRoomGroup findSuitableGroupXocDia(User user, int moneyType) {
        GameRoomGroup fitGroup;
        block6:
        {
            int rule = XocDiaGameUtils.getRuleJoin();
            fitGroup = null;
            String moneyTypeName = "vin";
            long money = this.userService.getMoneyUserCache(user.getName(), moneyTypeName);
            if (money < 100L) break block6;
            if (rule == 1) {
                for (GameRoomSetting setting : this.roomSettingList) {
                    if (setting.moneyType != moneyType || setting.requiredMoney > money || rule != setting.rule)
                        continue;
                    fitGroup = this.gameRoomGroups.get(setting.setting_name);
                    break;
                }
            } else {
                GameRoomSetting setting;
                int i;
                long maxMoney = -1L;
                for (i = 0; i < this.roomSettingList.size(); ++i) {
                    setting = this.roomSettingList.get(i);
                    if (setting.moneyType != moneyType || setting.requiredMoney > money || rule != setting.rule)
                        continue;
                    GameRoomGroup group = this.gameRoomGroups.get(setting.setting_name);
                    if (group.freeRooms.size() <= 0 || setting.requiredMoney <= maxMoney) continue;
                    maxMoney = setting.requiredMoney;
                    fitGroup = group;
                }
                if (fitGroup != null) {
                    return fitGroup;
                }
                rule = 1;
                for (i = 0; i < this.roomSettingList.size(); ++i) {
                    setting = this.roomSettingList.get(i);
                    if (setting.moneyType != moneyType || setting.requiredMoney > money || rule != setting.rule)
                        continue;
                    fitGroup = this.gameRoomGroups.get(setting.setting_name);
                    break;
                }
            }
        }
        return fitGroup;
    }

    public void destroyGameRoom(int roomId) {
        GameRoom room = this.allGameRooms.get(roomId);
        if (room != null) {
            if (room.group != null) {
                room.group.destroyGameRoom(room);
            }
            this.allGameRooms.remove(roomId);
            BotManager.instance().destroyGameRoom(room);
        }
    }
}
