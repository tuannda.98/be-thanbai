/*
 * Decompiled with CFR 0.150.
 *
 * Could not load the following classes:
 *  bitzero.server.BitZeroServer
 *  bitzero.server.core.BZEventParam
 *  bitzero.server.core.BZEventType
 *  bitzero.server.core.IBZEvent
 *  bitzero.server.core.IBZEventListener
 *  bitzero.server.core.IBZEventParam
 *  bitzero.server.core.IBZEventType
 *  bitzero.server.entities.User
 *  bitzero.server.exceptions.BZException
 *  bitzero.server.extensions.BaseClientRequestHandler
 *  bitzero.server.extensions.data.DataCmd
 *  bitzero.util.ExtensionUtility
 *  bitzero.util.common.business.CommonHandle
 *  org.json.JSONObject
 */
package game.modules.gameRoom;

import bitzero.server.BitZeroServer;
import bitzero.server.core.BZEventParam;
import bitzero.server.core.BZEventType;
import bitzero.server.core.IBZEvent;
import bitzero.server.entities.User;
import bitzero.server.extensions.BaseClientRequestHandler;
import bitzero.server.extensions.data.DataCmd;
import bitzero.util.ExtensionUtility;
import bitzero.util.common.business.CommonHandle;
import casio.king365.util.KingUtil;
import com.google.common.base.Throwables;
import game.entities.PlayerInfo;
import game.eventHandlers.GameEventParam;
import game.eventHandlers.GameEventType;
import game.modules.bot.BotManager;
import game.modules.gameRoom.cmd.rev.*;
import game.modules.gameRoom.cmd.send.*;
import game.modules.gameRoom.config.GameRoomConfig;
import game.modules.gameRoom.config.HuVangConfig;
import game.modules.gameRoom.entities.*;
import game.modules.gameRoom.fight.FightingManager;
import game.modules.tour.control.TourManager;
import game.utils.*;
import game.xocdia.conf.XocDiaConfig;
import game.xocdia.conf.XocDiaGameUtils;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static game.modules.gameRoom.entities.GameMoneyInfo.GAME_MONEY_INFO;

public class GameRoomModule
        extends BaseClientRequestHandler {
    public static final User.PropertyKey<Long> LAST_CHAT_TIME = new User.PropertyKey<>("last_chat_time");
    public static final User.PropertyKey<GameRoom> GAME_ROOM = new User.PropertyKey<>("GAME_ROOM");
    public static final User.PropertyKey<GameRoom> NEW_JOIN_ROOM = new User.PropertyKey<>("NEW_JOIN_ROOM");
    public static final User.PropertyKey<GameRoomSetting> GAME_ROOM_SETTING = new User.PropertyKey<>("GAME_ROOM_SETTING");
    public static final User.PropertyKey<User> ENEMY_USER = new User.PropertyKey<>("ENEMY_USER");

    private final Runnable gameRoomLoopTask = new GameLoopTask();

    private void gameRoomLoop() {
        try {
            if (GameUtils.isHuVang) {
                ThongTinHuVang.instance().addMoneyInLoop();
            }
            if (GameUtils.gameName.equalsIgnoreCase("PokerTour")) {
                int today = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
                if (TourManager.instance().lastDay != today) {
                    TourManager.instance().lastDay = today;
                    TourManager.instance().init();
                    TourManager.instance().giveDailyPrize(today);
                }
                TourManager.instance().registerTourForBot();
                TourManager.instance().updateVipTourPlayers();
            }
        } catch (Exception e) {
            CommonHandle.writeInfoLog(e);
        }
    }

    @Override
    public void init() {
        KingUtil.printLog("Hello CardCoreServer GameRoomModule init() 1");
        try {
            if (GameUtils.isBot) {
                BotManager.instance();
            }
            BossManager.instance();
            GameRoomManager.instance();
            ListGameMoneyInfo.instance();
            BanUserManager.instance();
            this.getParentExtension().addEventListener(BZEventType.USER_DISCONNECT, this);
            this.getParentExtension().addEventListener(GameEventType.GAME_ROOM_USER_JOIN, this);
            this.getParentExtension().addEventListener(GameEventType.GAME_ROOM_USER_LEAVE, this);
            this.getParentExtension().addEventListener(GameEventType.THANG_LON, this);
            BitZeroServer.getInstance().getTaskScheduler().scheduleAtFixedRate(this.gameRoomLoopTask, 3, 60, TimeUnit.SECONDS);
            BossManager.instance().initialBoss();
        } catch (Exception e) {
            e.printStackTrace();
        }

        KingUtil.printLog("Hello CardCoreServer GameRoomModule init() 10");
    }

    @Override
    public void handleServerEvent(IBZEvent ibzevent) {
        try {
            GameRoom room;
            User user;
            if (ibzevent.getType() == GameEventType.GAME_ROOM_USER_JOIN) {
                user = (User) ibzevent.getParameter(GameEventParam.USER);
                room = (GameRoom) ibzevent.getParameter(GameEventParam.GAMEROOM);
                Boolean isReconnect = (Boolean) ibzevent.getParameter(GameEventParam.IS_RECONNECT);
                this.userJoinRoomSuccess(user, room, isReconnect);
            }
            if (ibzevent.getType() == GameEventType.THANG_LON) {
                user = (User) ibzevent.getParameter(GameEventParam.USER);
                room = (GameRoom) ibzevent.getParameter(GameEventParam.GAMEROOM);
                ThongTinThangLon info = (ThongTinThangLon) ibzevent.getParameter(GameEventParam.THONG_TIN_THANG_LON);
                this.xuLiThangLon(user, room, info);
            }
            if (ibzevent.getType() == GameEventType.GAME_ROOM_USER_LEAVE) {
                user = (User) ibzevent.getParameter(GameEventParam.USER);
                room = (GameRoom) ibzevent.getParameter(GameEventParam.GAMEROOM);
                this.userLeaveRoom(user, room);
            }
            if (ibzevent.getType() == BZEventType.USER_DISCONNECT) {
                user = (User) ibzevent.getParameter(BZEventParam.USER);
                this.userDisconnected(user);
            }
        } catch (Exception e) {
            NotificationUtils.sendOperation(Throwables.getStackTraceAsString(e));
        }
    }

    @Override
    public void handleClientRequest(User user, DataCmd dataCmd) {
        try {
            switch (dataCmd.getId()) {
                case 3001: {
                    this.joinGameRoom(user, dataCmd);
                    break;
                }
                case 3002: {
                    this.reconnectGameRoom(user, dataCmd);
                    break;
                }
                case 3003: {
                    this.requestConfig(user, dataCmd);
                    break;
                }
                case 3008: {
                    this.onChatRoom(user, dataCmd);
                    break;
                }
                case 3009: {
                    this.thongTinHuVang(user, dataCmd);
                    break;
                }
                case 3010: {
                    this.listInvite(user, dataCmd);
                    break;
                }
                case 3012: {
                    this.accept(user, dataCmd);
                    break;
                }
                case 3011: {
                    this.invite(user, dataCmd);
                    break;
                }
                case 3013: {
                    this.create_room(user, dataCmd);
                    break;
                }
                case 3014: {
                    this.getRoomList(user, dataCmd);
                    break;
                }
                case 3015: {
                    this.joinGameRoomById(user, dataCmd);
                    break;
                }
                case 3016: {
                    this.getGameRoomById(user, dataCmd);
                    break;
                }
                case 3017: {
                    this.getXocDiaConfig(user, dataCmd);
                    break;
                }
                default: {
                    this.sendMessageToGameServer(user, dataCmd);
                }
            }
        } catch (Exception e) {
            NotificationUtils.sendOperation(Throwables.getStackTraceAsString(e));
        }
    }

    private void sendMessageToGameServer(User user, DataCmd dataCmd) {
        GameRoom room = user.getProperty(GAME_ROOM);
        if (room != null) {
            GameServer gs = room.getGameServer();
            gs.onGameMessage(user, dataCmd);
        }
    }

    private void getXocDiaConfig(User user, DataCmd dataCmd) {
        XocDiaConfigMsg msg = new XocDiaConfigMsg();
        msg.fundVipMinRegis = XocDiaConfig.fundVipMinRegis;
        this.send(msg, user);
    }

    private void getGameRoomById(User user, DataCmd data) {
        GetRoomInfoById cmd = new GetRoomInfoById(data);
        LoggerUtils.info("game_room", "getGameRoomById", user.getName(), cmd.roomId);
        GameRoom room = GameRoomManager.instance().getGameRoomById(cmd.roomId);
        SendGameRoomInfo msg = new SendGameRoomInfo();
        if (room != null) {
            msg.room = room;
        } else {
            msg.Error = 1;
        }
        this.send(msg, user);
    }

    private void joinGameRoomById(User user, DataCmd data) {
        JoinRoomByRoomId cmd = new JoinRoomByRoomId(data);
        GameRoom room = GameRoomManager.instance().getGameRoomToJoin(cmd.roomId, cmd.password, user);
        if (room != null) {
            if (room.isFull()) {
                this.notifyJoinRoomFail(user, (byte) 9, false);
                return;
            }
            if (BanUserManager.instance().isBan(room.getId(), user.getName())) {
                this.notifyJoinRoomFail(user, (byte) 10, false);
                return;
            }
            boolean check = this.preJoinRoom(user, room.getId(), false);
            if (!check) {
                return;
            }
            boolean moneyCheck = this.checkMoneyJoinRoom(room.getId(), user, room.setting, -1L);
            if (moneyCheck) {
                GameRoomManager.instance().joinRoom(user, room, false);
            } else {
                this.notifyJoinRoomFail(user, (byte) 3, false);
            }
        }
    }

    private void create_room(User user, DataCmd dataCmd) {
        CreateGameRoomCmd cmd = new CreateGameRoomCmd(dataCmd);
        if (cmd.maxUserPerRoom > 2 && !GameCommonUtils.canCreateRoom()) {
            this.notifyCreateJoinRoomFail(user, (byte) 14);
            return;
        }
        GameRoomSetting setting = new GameRoomSetting(cmd);
        long moneyRequire = -1L;
        if (XocDiaGameUtils.isXocDia()) {
            int error = XocDiaGameUtils.isCanCreateBoss(setting, cmd.moneyRequire, user.getName());
            if (error == 0) {
                moneyRequire = cmd.moneyRequire;
                setting.roomName = user.getName();
            } else {
                this.notifyCreateJoinRoomFail(user, (byte) error);
                return;
            }
        }
        this.joinGameRoom(user, setting, moneyRequire, true, false);
    }

    private void invite(User user, DataCmd dataCmd) {
        boolean check = BlockingData.instance().preventSpamInvite(user.getName());
        if (check) {
            return;
        }
        RevInvite cmd = new RevInvite(dataCmd);
        GameRoom room = user.getProperty(GAME_ROOM);
        if (room != null && cmd.users != null) {
            for (int i = 0; i < cmd.users.length; ++i) {
                User u = ExtensionUtility.globalUserManager.getUserByName(cmd.users[i]);
                if (u == null || u.getProperty(GAME_ROOM) != null) continue;
                SendInvite msg = new SendInvite();
                msg.inviter = user.getName();
                msg.maxUserPerRoom = room.setting.maxUserPerRoom;
                msg.roomID = room.getId();
                msg.moneyBet = room.setting.moneyBet;
                msg.rule = room.setting.rule;
                this.send(msg, u);
            }
        }
    }

    private void accept(User user, DataCmd dataCmd) {
        RevAcceptInvite cmd = new RevAcceptInvite(dataCmd);
        User u = ExtensionUtility.globalUserManager.getUserByName(cmd.inviter);
        if (user != null) {
            GameRoom room1 = user.getProperty(GAME_ROOM);
            GameRoom room2 = u.getProperty(GAME_ROOM);
            if (room1 == null && room2 != null && this.checkJoinRoom(user, room2)) {
                GameRoomManager.instance().joinRoom(user, room2, false);
            }
        }
    }

    private boolean checkJoinRoom(User user, GameRoom room) {
        if (room.isFull()) {
            return false;
        }
        if (user.getProperty(GAME_ROOM) != null) {
            return false;
        }
        GameMoneyInfo moneyInfo = new GameMoneyInfo(user, room.getId(), room.setting);
        boolean result = moneyInfo.startGameUpdateMoney();
        if (result) {
            user.setProperty(GAME_MONEY_INFO, moneyInfo);
            return true;
        }
        return false;
    }

    private void listInvite(User user, DataCmd dataCmd) {
        GameRoom room = user.getProperty(GAME_ROOM);
        if (room == null || room.setting.moneyType != 1 || room.isFull() || room.isLocked()) {
            return;
        }
        long expected = room.setting.requiredMoney;
        if (GameUtils.gameName.equalsIgnoreCase("Poker")) {
            expected = room.setting.moneyBet * 40L;
        }
        if (GameUtils.gameName.equalsIgnoreCase("Lieng")) {
            expected = room.setting.moneyBet * 5L;
        }
        List<User> users = ExtensionUtility.globalUserManager.getAllUsers();
        List<User> listBotInvite = BotManager.instance().getListInviteBot(NumberUtils.randomIntLimit(3, 6));
        users.addAll(listBotInvite);
        ArrayList<String> names = new ArrayList<>();
        ArrayList<Long> moneyList = new ArrayList<>();
        int c = 0;
        for (User u : users) {
            long money;
            GameRoom r = u.getProperty(GAME_ROOM);
            if (r != null || (money = GameMoneyInfo.userService.getMoneyUserCache(u.getName(), "vin")) < expected)
                continue;
            names.add(u.getName());
            moneyList.add(money);
            if (++c != 10) continue;
            break;
        }
        SendListInvite msg = new SendListInvite();
        msg.listMoney = new long[c];
        msg.listName = new String[c];
        for (int i = 0; i < c; ++i) {
            msg.listName[i] = names.get(i);
            msg.listMoney[i] = moneyList.get(i);
        }
        this.send(msg, user);
    }

    private void notifyJoinRoomFail(User user, byte error, boolean isCreate) {
        if (!isCreate) {
            JoinGameRoomFailMsg msg = new JoinGameRoomFailMsg();
            msg.Error = error;
            this.send(msg, user);
        } else {
            this.notifyCreateJoinRoomFail(user, error);
        }
    }

    private void notifyCreateJoinRoomFail(User user, byte error) {
        CreateGameRoomFailMsg msg = new CreateGameRoomFailMsg();
        msg.Error = error;
        this.send(msg, user);
    }

    private boolean preJoinRoom(User user, int roomId, boolean isCreate) {
        boolean check;
        boolean isHold = PlayerInfo.getIsHold(user.getName());
        boolean isBoss = BossManager.instance().checkBossName(user.getName());
        if (isHold && (!isBoss || BossManager.instance().checkBossNameAndId(user.getName(), roomId))) {
            roomId = isBoss ? roomId : -1;
            this.reconnectGameRoomUser(user, roomId);
            return false;
        }
        if (GameUtils.isMainTain) {
            this.notifyJoinRoomFail(user, (byte) 6, isCreate);
            return false;
        }
        long last = user.getLastJoinRoomTime();
        long now = System.currentTimeMillis();
        long interval = now - last;
        if (interval < GameRoomConfig.instance().getJoinRoomIntervalTime()) {
            this.notifyJoinRoomFail(user, (byte) 5, isCreate);
            return false;
        }
        user.setLastJoinRoomTime(now);
        if (!isBoss && !(check = GameUtils.infoCheck(user))) {
            this.notifyJoinRoomFail(user, (byte) 1, isCreate);
            return false;
        }
        return true;
    }

    private void joinGameRoom(User user, GameRoomSetting setting, long moneyRequire, boolean createRoom, boolean isBossJoin) {
        boolean check = this.preJoinRoom(user, -1, createRoom);
        if (!check) {
            return;
        }
        GameRoomGroup group = GameRoomManager.instance().getGroup(setting);
        if (group == null) {
            LoggerUtils.debug("game_room", "Finding Group: ", setting.moneyType, setting.maxUserPerRoom);
            group = GameRoomManager.instance().findSuitableGroup(user, setting);
            if (group != null) {
                LoggerUtils.debug("game_room", "Finding Group: ", group.setting.moneyType, group.setting.maxUserPerRoom);
            }
        }
        if (group == null) {
            this.notifyJoinRoomFail(user, (byte) 4, createRoom);
            return;
        }
        boolean result = this.checkMoneyJoinRoom(-1, user, group.setting, moneyRequire);
        if (!result) {
            this.notifyJoinRoomFail(user, (byte) 3, createRoom);
            return;
        }
        boolean isBossCreate = moneyRequire > 0L;
        int res = group.joinRoom(user, setting, isBossCreate, isBossJoin);
        if (res != 0) {
            this.notifyJoinRoomFail(user, (byte) 4, createRoom);
            GameMoneyInfo info = user.getProperty(GAME_MONEY_INFO);
            info.restoreMoney(info.roomId);
        }
    }

    private void joinGameRoom(User user, DataCmd dataCmd) {
        JoinGameRoomCmd cmd = new JoinGameRoomCmd(dataCmd);
        KingUtil.printLog("CardCoreServer joinGameRoom() user: " + user.getName() + ", cmd: " + cmd);
        GameRoomSetting setting = new GameRoomSetting(cmd);
        LoggerUtils.debug("game_room", "joinGameRoom", cmd.moneyType, cmd.maxUserPerRoom, setting.maxUserPerRoom, setting.moneyType, setting.rule);
        this.joinGameRoom(user, setting, -1L, false, BossManager.instance().checkBossName(user.getName()));
    }

    private boolean checkMoneyJoinRoom(int roomId, User user, GameRoomSetting setting, long moneyRequire) {
        boolean result;
        GameMoneyInfo moneyInfo = new GameMoneyInfo(user, roomId, setting);
        KingUtil.printLog("checkMoneyJoinRoom() moneyInfo: " + moneyInfo);
        if (moneyRequire > 0L) {
            moneyInfo.requireMoney = moneyRequire;
        }
        result = moneyInfo.startGameUpdateMoney();
        KingUtil.printLog("checkMoneyJoinRoom(), result: " + result);
        if (result) {
            user.setProperty(GAME_MONEY_INFO, moneyInfo);
            return true;
        }
        return false;
    }

    private void userJoinRoomSuccess(User user, GameRoom room, boolean isReconnect) {
        LoggerUtils.debug("tour", "userJoinRoomSuccess", user.getName(), "room", room.getId(), "isReconnect", isReconnect);
        GameServer gs = room.getGameServer();
        if (isReconnect) {
            gs.onGameUserReturn(user);
            LoggerUtils.debug("tour", "userJoinRoomSuccess onGameUserReturn", user.getName(), "room", room.getId(), "isReconnect", isReconnect);
        } else {
            GameMoneyInfo info = user.getProperty(GAME_MONEY_INFO);
            if (info != null) {
                info.roomId = room.getId();
            }
            gs.onGameUserEnter(user);
            LoggerUtils.debug("tour", "userJoinRoomSuccess onGameUserEnter", user.getName(), "room", room.getId(), "isReconnect", isReconnect);
            User enemy = (User) user.getProperty(ENEMY_USER);
            if (enemy != null) {
                user.removeProperty(ENEMY_USER);
                enemy.removeProperty(ENEMY_USER);
                FightingManager.instance().removeOnFightUser(enemy);
                FightingManager.instance().removeOnFightUser(user);
                GameRoomManager.instance().joinRoom(enemy, room, false);
            }
        }
    }

    private void userLeaveRoom(User user, GameRoom room) {
        GameServer gs = room.getGameServer();
        gs.onGameUserExit(user);
        if (GameUtils.isBot && user.isBot()) {
            BotManager.instance().releaseBot(user);
        }
        GameRoom newRoom = user.getProperty(NEW_JOIN_ROOM);
        if (newRoom != null) {
            GameRoomManager.instance().joinRoom(user, newRoom, false);
            LoggerUtils.debug("tour", "userLeaveRoom Join New Room", user.getName(), "room", room.getId());
            user.removeProperty(NEW_JOIN_ROOM);
        } else {
            FightingManager.instance().addOnFightUser(user);
        }
    }

    private void userDisconnected(User user) {
        GameRoom room = user.getProperty(GAME_ROOM);
        if (room != null) {
            GameServer gs = room.getGameServer();
            gs.onGameUserDis(user);
        }
    }

    private void reconnectGameRoom(User user, DataCmd dataCmd) {
        boolean isHold = PlayerInfo.getIsHold(user.getName());
        if (isHold && !BossManager.instance().checkBossName(user.getName())) {
            this.reconnectGameRoomUser(user, -1);
        } else {
            ReconnectGameRoomFailMsg msg = new ReconnectGameRoomFailMsg();
            this.send(msg, user);
        }
    }

    private void reconnectGameRoomUser(User user, int roomId) {
        GameRoom room;
        if (roomId == -1) {
            roomId = PlayerInfo.getHoldRoom(user.getName());
        }
        if ((room = GameRoomManager.instance().getGameRoomById(roomId)) != null) {
            GameRoomManager.instance().joinRoom(user, room, true);
        }
    }

    private void requestConfig(User user, DataCmd dataCmd) {
        GameRoomConfigMsg msg = new GameRoomConfigMsg();
        this.send(msg, user);
    }

    private void thongTinHuVang(User user, DataCmd dataCmd) {
        if (!GameUtils.isHuVang) {
            return;
        }
        JSONObject json = HuVangConfig.instance().getConfigCurrentGame();
        try {
            SendThongTinHu msg = new SendThongTinHu();
            int remain = HuVangConfig.instance().kiemTraHuVangTheoThoiGian(json);
            if (remain >= 600 || remain == 0) {
                json = HuVangConfig.instance().timHuVangDangChay();
            }
            if (json != null) {
                msg.gameName = json.getString("gameName");
                msg.remainTime = HuVangConfig.instance().kiemTraHuVangTheoThoiGian(json);
                msg.goldAmmount = ThongTinHuVang.instance().getGoldAmount(msg.gameName);
                this.send(msg, user);
            }
        } catch (Exception e) {
            if (json != null) {
                CommonHandle.writeErrLog(json.toString());
            }
            CommonHandle.writeErrLog(e);
        }
    }

    private void xuLiThangLon(User user, GameRoom room, ThongTinThangLon info) {
        try {
            if (info.noHu) {
                this.congTienThangHu(user, room, info);
            }
        } catch (Exception e) {
        }
    }

    private void congTienThangHu(User user, GameRoom room, ThongTinThangLon info) {
        info = ThongTinHuVang.instance().addMoneyForUser(user, info);
        if (info != null) {
            GameServer gs = room.getGameServer();
            gs.onNoHu(info);
        }
    }

    private void onChatRoom(User user, DataCmd data) {
        long delta;
        Long lastChat = user.getProperty(LAST_CHAT_TIME);
        long now = System.currentTimeMillis();
        if (lastChat != null && now - lastChat < 3000L) {
            return;
        }
        user.setProperty(LAST_CHAT_TIME, now);
        ChatRoomCmd cmd = new ChatRoomCmd(data);
        GameRoom room = user.getProperty(GAME_ROOM);
        Integer chair = (Integer) user.getProperty(GameServer.USER_CHAIR);
        if (chair == null) {
            chair = 0;
        }
        if (room != null) {
            ChatRoomMsg msg = new ChatRoomMsg();
            msg.chair = chair;
            msg.isIcon = cmd.isIcon;
            msg.content = cmd.content;
            msg.nickName = user.getName();
            for (Map.Entry<String, User> entry : room.userManager.entrySet()) {
                User u = entry.getValue();
                if (u == null || u.isBot() || !u.isConnected()) continue;
                this.send(msg, u);
            }
        }
    }

    private void getRoomList(User user, DataCmd data) {
        RevGetRoomList cmd = new RevGetRoomList(data);
        GameRoomSetting setting = new GameRoomSetting(cmd);
        GameRoomGroup group = GameRoomManager.instance().getGroup(setting);
        if (cmd.from < 0 || cmd.to < 0 || cmd.from > cmd.to) {
            cmd.from = 0;
            cmd.to = 50;
        }
        if (cmd.to > cmd.from + 50) {
            cmd.to = cmd.from + 50;
        }
        if (group != null) {
            int from = -1;
            int to = -1;
            SendRoomList msg = new SendRoomList();
            for (GameRoom room : group.freeRooms) {
                ++to;
                if (++from < cmd.from) continue;
                if (to > cmd.to) break;
                msg.roomList.add(room);
            }
            for (GameRoom room : group.lockedRooms) {
                ++to;
                if (++from < cmd.from) continue;
                if (to > cmd.to) break;
                msg.roomList.add(room);
            }
            for (GameRoom room : group.busyRooms) {
                ++to;
                if (++from < cmd.from) continue;
                if (to > cmd.to) break;
                msg.roomList.add(room);
            }
            for (GameRoom room : group.emptyRooms) {
                ++to;
                if (++from < cmd.from) continue;
                if (to > cmd.to) break;
                msg.roomList.add(room);
                break;
            }
            this.send(msg, user);
        } else {
            int from = -1;
            int to = -1;
            SendRoomList msg = new SendRoomList();
            block4:
            for (Map.Entry<String, GameRoomGroup> entry : GameRoomManager.instance().gameRoomGroups.entrySet()) {
                group = entry.getValue();
                if (group.setting.moneyType != setting.moneyType || group.setting.rule != setting.rule && !XocDiaGameUtils.isXocDia() || group.setting.maxUserPerRoom != setting.maxUserPerRoom && !XocDiaGameUtils.isXocDia())
                    continue;
                for (GameRoom room : group.freeRooms) {
                    ++to;
                    if (++from < cmd.from) continue;
                    if (to > cmd.to) break;
                    msg.roomList.add(room);
                }
                for (GameRoom room : group.lockedRooms) {
                    ++to;
                    if (++from < cmd.from) continue;
                    if (to > cmd.to) break;
                    msg.roomList.add(room);
                }
                for (GameRoom room : group.busyRooms) {
                    ++to;
                    if (++from < cmd.from) continue;
                    if (to > cmd.to) break;
                    msg.roomList.add(room);
                }
                for (GameRoom room : group.emptyRooms) {
                    ++to;
                    if (++from < cmd.from) continue;
                    if (to > cmd.to) continue block4;
                    msg.roomList.add(room);
                    continue block4;
                }
            }
            this.send(msg, user);
        }
    }

    private class GameLoopTask
            implements Runnable {
        private GameLoopTask() {
        }

        @Override
        public void run() {
            GameRoomModule.this.gameRoomLoop();
        }
    }
}
