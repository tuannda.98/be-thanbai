/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  bitzero.server.entities.User
 */
package game.modules.bot;

import bitzero.server.entities.User;
import lombok.ToString;

@ToString
public class Bot {
    public User user = null;
    public int count = 0;
    public int lastRoomId = -1;
    public boolean isFree = true;

    public Bot(User user) {
        this.user = user;
    }
}

