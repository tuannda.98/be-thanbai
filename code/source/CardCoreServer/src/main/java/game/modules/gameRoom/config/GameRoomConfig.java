/*
 * Decompiled with CFR 0.150.
 *
 * Could not load the following classes:
 *  bitzero.util.common.business.CommonHandle
 *  org.json.JSONException
 *  org.json.JSONObject
 */
package game.modules.gameRoom.config;

import bitzero.util.common.business.CommonHandle;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class GameRoomConfig {
    public JSONObject config;
    private static GameRoomConfig gameRoom = null;

    private GameRoomConfig() {
        this.getConfigFromFile();
    }

    public static GameRoomConfig instance() {
        if (gameRoom == null) {
            gameRoom = new GameRoomConfig();
        }
        return gameRoom;
    }

    public long getJoinRoomIntervalTime() {
        try {
            return this.config.getLong("join_room_interval_time");
        } catch (JSONException e) {
            CommonHandle.writeErrLog(e);
            return 10000L;
        }
    }

    public long getBigWin() {
        try {
            return this.config.getLong("big_win");
        } catch (JSONException e) {
            CommonHandle.writeErrLog(e);
            return 100000L;
        }
    }

    public void getConfigFromFile() {
        String path = System.getProperty("user.dir");
        File file = new File(path + "/conf/gameroom.json");
        StringBuffer contents = new StringBuffer();
        try (FileInputStream fileInputStream = new FileInputStream(file);
             InputStreamReader r = new InputStreamReader(fileInputStream, StandardCharsets.UTF_8);
             BufferedReader reader = new BufferedReader(r)) {
            String text = null;
            while ((text = reader.readLine()) != null) {
                contents.append(text).append(System.getProperty("line.separator"));
            }
            this.config = new JSONObject(contents.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
