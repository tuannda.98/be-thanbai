/*
 * Decompiled with CFR 0.150.
 *
 * Could not load the following classes:
 *  bitzero.util.common.business.CommonHandle
 *  bitzero.util.datacontroller.business.DataController
 */
package game.modules.gameRoom.entities;

import bitzero.util.common.business.CommonHandle;
import bitzero.util.datacontroller.business.DataController;
import game.utils.GameUtils;

import java.util.concurrent.atomic.AtomicInteger;

public class GameRoomIdGenerator {
    public static AtomicInteger idGen = null;

    private static GameRoomIdGenerator ins = null;

    /**
     * Use
     *
     * @return getId()
     */
    @Deprecated
    public static GameRoomIdGenerator instance() {
        if (ins == null) {
            ins = new GameRoomIdGenerator();
        }
        return ins;
    }

    public static synchronized int getId() {
        if (idGen == null) loadFromDB();
        int id = idGen.getAndIncrement();
        saveToDB();
        return id;
    }

    private static void loadFromDB() {
        Integer lastId = null;
        try {
            lastId = (Integer) DataController.getController().get(getKey());
        } catch (Exception e) {
            CommonHandle.writeErrLog(e);
        }
        if (idGen == null) {
            int fromId = 1;
            if (lastId != null) {
                fromId = lastId;
            }
            idGen = new AtomicInteger(fromId);
            saveToDB();
        }
    }

    private static void saveToDB() {
        int id = idGen.get();
        try {
            DataController.getController().set(getKey(), id);
        } catch (Exception e) {
            CommonHandle.writeErrLog("Error when save last game id to DB: " + id, e);
        }
    }

    private static String getKey() {
        StringBuilder sb = new StringBuilder();
        sb.append(GameUtils.gameName);
        sb.append(GameRoomIdGenerator.class.getSimpleName());
        return sb.toString();
    }
}
