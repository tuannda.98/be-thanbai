/*
 * Decompiled with CFR 0.150.
 *
 * Could not load the following classes:
 *  bitzero.server.entities.User
 *  com.vinplay.usercore.service.MoneyInGameService
 *  com.vinplay.usercore.service.UserService
 *  com.vinplay.usercore.service.impl.MoneyInGameServiceImpl
 *  com.vinplay.usercore.service.impl.UserServiceImpl
 *  com.vinplay.vbee.common.enums.FreezeInGame
 *  com.vinplay.vbee.common.response.FreezeMoneyResponse
 *  com.vinplay.vbee.common.response.MoneyResponse
 *  org.json.JSONException
 *  org.json.JSONObject
 *  scala.collection.mutable.StringBuilder
 */
package game.modules.gameRoom.entities;

import bitzero.server.entities.User;
import casio.king365.util.KingUtil;
import com.vinplay.usercore.service.MoneyInGameService;
import com.vinplay.usercore.service.UserService;
import com.vinplay.usercore.service.impl.MoneyInGameServiceImpl;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.enums.FreezeInGame;
import com.vinplay.vbee.common.response.FreezeMoneyResponse;
import com.vinplay.vbee.common.response.MoneyResponse;
import game.entities.UserScore;
import game.utils.DataUtils;
import game.utils.GameUtils;
import game.utils.LoggerUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

public class GameMoneyInfo {
    public static final User.PropertyKey<GameMoneyInfo> GAME_MONEY_INFO = new User.PropertyKey<>("GAME_MONEY_INFO");

    public static final long MIN_BUY_IN_RATE_POKER = 40L;
    public static final long MIN_BUY_IN_RATE_LIENG = 5L;
    public static MoneyInGameService moneyService = new MoneyInGameServiceImpl();
    public static Random rd = new Random();
    public static UserService userService = new UserServiceImpl();
    public String sessionId = "";
    public int countToRemove = 0;
    public int roomId;
    public String nickName;
    public String moneyBet;
    public int moneyType;
    public String moneyTypeName;
    public long currentMoney = 0L;
    public long requireMoney;
    public long outMoney;
    public long freezeMoney;
    private static final boolean dependOnGame = false;

    public boolean moneyCheckInGame() {
        return this.getMoneyUseInGame() >= this.outMoney;
    }

    public long getMoneyUseInGame() {
        if (GameUtils.gameName.equals("Sam") || GameUtils.gameName.equals("Tlmn")) {
            return this.freezeMoney;
        }
        return userService.getMoneyUserCache(this.nickName, this.moneyTypeName) + this.freezeMoney;
    }

    public MoneyResponse updateMoney(long money, int roomId, int gameId, long fee, boolean bExactly) {
        synchronized (this) {
            MoneyResponse res;
            if (money > 0L) {
                res = moneyService.addingMoneyInGame(
                        this.sessionId, this.nickName, GameUtils.gameName, String.valueOf(roomId), money, this.moneyTypeName,
                        this.requireMoney, String.valueOf(gameId), fee);
            } else if (bExactly) {
                res = moneyService.subtractMoneyInGameExactly(
                        this.sessionId, this.nickName, GameUtils.gameName, String.valueOf(roomId), Math.abs(money), this.moneyTypeName,
                        String.valueOf(gameId));
            } else {
                res = moneyService.subtractMoneyInGame(
                        this.sessionId, this.nickName, GameUtils.gameName, String.valueOf(roomId),
                        Math.abs(money), this.moneyTypeName, String.valueOf(gameId));
            }

            if (res.isSuccess()) {
                this.currentMoney = res.getCurrentMoney();
                this.freezeMoney = res.getFreezeMoney();
            }
            return res;
        }
    }

    public MoneyResponse updateMoneyByFreeze(long money, int roomId, int gameId, long fee) {
        synchronized (this) {
            MoneyResponse res = moneyService.updateMoneyInGameByFreeze(
                    this.sessionId, this.nickName, GameUtils.gameName, String.valueOf(roomId),
                    money, this.moneyTypeName, String.valueOf(gameId), fee);
            if (res.isSuccess()) {
                this.currentMoney = res.getCurrentMoney();
                this.freezeMoney = res.getFreezeMoney();
            }
            return res;
        }
    }

    public MoneyResponse addFreezeMoney(long money, int roomId, int gameId, FreezeInGame type) {
        synchronized (this) {
            MoneyResponse res = moneyService.addFreezeMoneyInGame(
                    this.sessionId, this.nickName, GameUtils.gameName, String.valueOf(roomId), String.valueOf(gameId),
                    money, this.moneyTypeName, type);
            if (res.isSuccess()) {
                this.currentMoney = res.getCurrentMoney();
                this.freezeMoney = res.getFreezeMoney();
            }
            return res;
        }

    }

    public GameMoneyInfo(User user, int roomId, GameRoomSetting setting) {
        this.nickName = user.getName();
        this.moneyType = setting.moneyType;
        this.moneyBet = String.valueOf(setting.moneyBet);
        this.moneyTypeName = this.moneyType == 1 ? "vin" : "xu";
        this.requireMoney = setting.requiredMoney;
        this.outMoney = setting.outMoney;
        this.roomId = roomId;
    }

    public long getCurrentMoneyFromCache() {
        if (!GameUtils.enable_payment) {
            return this.currentMoney;
        }
        this.currentMoney = userService.getCurrentMoneyUserCache(this.nickName, this.moneyTypeName);
        return this.currentMoney;
    }

    public boolean startGameUpdateMoney() {
        synchronized (this) {
            return this.freezeMoneyBegining();
        }
    }

    public boolean canPlayNextGame() {
        return this.moneyCheckInGame() && this.freezeMoney > 0L;
    }

    private boolean freezeMoneyBegining() {
        if (!GameUtils.enable_payment) {
            Random rd = new Random();
            this.sessionId = GameUtils.gameName + Math.abs(rd.nextInt(1000000000));
            this.freezeMoney = this.requireMoney;
            this.currentMoney = this.requireMoney * 10L;
            return true;
        }

        FreezeMoneyResponse res = moneyService.freezeMoneyInGame(
                this.nickName, GameUtils.gameName, String.valueOf(this.roomId), this.requireMoney, this.moneyTypeName);
        KingUtil.printLog("freezeMoneyBegining() res: " + res);
        LoggerUtils.info("game_money", "freezeMoneyBegining(void): isSucess?", res.isSuccess(), res.getErrorCode(), "sessionId:", res.getSessionId(), "currentMoney:", res.getCurrentMoney());
        if (res.isSuccess()) {
            this.sessionId = res.getSessionId();
            this.currentMoney = res.getCurrentMoney();
            this.freezeMoney = this.requireMoney;
            return true;
        }
        return false;
    }

    public boolean freezeMoneyBegining(long money) {
        if (!GameUtils.enable_payment) {
            Random rd = new Random();
            this.sessionId = GameUtils.gameName + Math.abs(rd.nextInt(1000000000));
            this.requireMoney = money;
            this.freezeMoney = money;
            this.currentMoney = money * 10L;
            return true;
        }
        synchronized (this) {
            long now = System.currentTimeMillis();
            this.requireMoney = money;
            FreezeMoneyResponse res = moneyService.freezeMoneyInGame(
                    this.nickName, GameUtils.gameName, String.valueOf(this.roomId), this.requireMoney, this.moneyTypeName);
            LoggerUtils.info("game_money", "freezeMoneyBegining(long): isSucess?", res.getErrorCode(), res.isSuccess(), "sessionId:", res.getSessionId(), "currentMoney:", res.getCurrentMoney());
            if (res.isSuccess()) {
                this.sessionId = res.getSessionId();
                this.currentMoney = res.getCurrentMoney();
                this.freezeMoney = this.requireMoney;
                return true;
            }
            return false;
        }
    }

    public boolean addFreezeMoney(long money, int roomId, int matchId) {
        if (!GameUtils.enable_payment) {
            this.freezeMoney += money;
        }
        synchronized (this) {
            MoneyResponse res = moneyService.addFreezeMoneyInGame(
                    this.sessionId, this.nickName, GameUtils.gameName, String.valueOf(roomId), String.valueOf(matchId),
                    money, this.moneyTypeName, FreezeInGame.MORE);
            LoggerUtils.info("game_money", "addFreezeMoney(long): isSucess?", res.getErrorCode(), res.isSuccess(), "sessionId:", this.sessionId, "currentMoney:", res.getCurrentMoney(), "freezeMoney:", res.getFreezeMoney());
            if (res.isSuccess()) {
                this.currentMoney = res.getCurrentMoney();
                this.freezeMoney = res.getFreezeMoney();
                return true;
            }
            return false;
        }
    }

    public boolean restoreMoney(int roomId) {
        if (!GameUtils.enable_payment) {
            return true;
        }

        synchronized (this) {
            FreezeMoneyResponse res = moneyService.restoreMoneyInGame(
                    this.sessionId, this.nickName, GameUtils.gameName, String.valueOf(roomId), this.moneyTypeName);
            LoggerUtils.info("game_money", "restoreMoney: isSucess?", res.isSuccess(), "sessionId:", res.getSessionId(), roomId);
            return res.isSuccess();
        }
    }

    public long chargeMoneyInGame(UserScore score, int roomId, int gameId) throws MoneyException {
        synchronized (this) {
            if (score.money < 0L) {
                return -this.subMoneyInGame(score, roomId, gameId);
            }
            return this.addMoneyInGame(score, roomId, gameId);
        }
    }

    public long chargeMoneyInTour(UserScore score, int roomId, int gameId) {
        synchronized (this) {
            if (score.money < 0L) {
                LoggerUtils.debug("tour", "chargeMoneyInTour - 1", this.nickName, "room ", roomId, "game", gameId, "currentMoney", this.currentMoney, " freeze", this.freezeMoney);
                long result = -this.subMoneyInTour(score, roomId, gameId);
                LoggerUtils.debug("tour", this.nickName, "chargeMoneyInTour - 2", "room ", roomId, "game", gameId, "currentMoney", this.currentMoney, " freeze", this.freezeMoney);
                return result;
            }
            LoggerUtils.debug("tour", this.nickName, "chargeMoneyInTour + 1", "room ", roomId, "game", gameId, "currentMoney", this.currentMoney, " freeze", this.freezeMoney);
            long result = this.addMoneyInTour(score, roomId, gameId);
            LoggerUtils.debug("tour", this.nickName, "chargeMoneyInTour + 2", "room ", roomId, "game", gameId, "currentMoney", this.currentMoney, " freeze", this.freezeMoney);
            return result;
        }
    }

    private synchronized long subMoneyInGame(UserScore score, int roomId, int gameId) throws MoneyException {
        if (!GameUtils.enable_payment) {
            long newMoney = this.currentMoney + score.money;
            if (newMoney < 0L) {
                score.money = -this.currentMoney;
                this.currentMoney = 0L;
                this.freezeMoney = 0L;
            } else if (newMoney < this.requireMoney) {
                this.freezeMoney = newMoney;
                this.currentMoney = newMoney;
            } else {
                this.currentMoney = newMoney;
            }
            return -score.money;
        }
        long now = System.currentTimeMillis();
        MoneyResponse res = moneyService.subtractMoneyInGame(
                this.sessionId, this.nickName, GameUtils.gameName, String.valueOf(roomId),
                -score.money, this.moneyTypeName, String.valueOf(gameId));
        LoggerUtils.info("game_money", "subMoneyInGame: isSucess?", res.isSuccess(), res.getErrorCode(), "sessionId:", this.sessionId, "matchId:", gameId);
        if (!res.isSuccess()) {
            if (res.getResponseCode() == MoneyResponse.MoneyResponseCode.NOT_ENOUGH_MONEY) {
                throw new NotEnoughMoneyException();
            }
            throw new MoneyException("SubtractMoneyInGame: " + res.getResponseCode().errorCode + " - " + res.getResponseCode().description + " sessionId: " + this.sessionId + "matchId:" + gameId);
        }
        this.currentMoney = res.getCurrentMoney();
        this.freezeMoney = res.getFreezeMoney();
        return res.getSubtractMoney();
    }

    private synchronized long subMoneyInTour(UserScore score, int roomId, int gameId) {
        long newMoney = this.currentMoney + score.money;
        if (newMoney < 0L) {
            score.money = -this.currentMoney;
            this.currentMoney = 0L;
            this.freezeMoney = 0L;
        } else if (newMoney < this.requireMoney) {
            this.freezeMoney = newMoney;
            this.currentMoney = newMoney;
        } else {
            this.currentMoney = newMoney;
        }
        return -score.money;
    }

    private synchronized long addMoneyInGame(UserScore score, int roomId, int gameId) throws MoneyException {
        if (!GameUtils.enable_payment) {
            this.freezeMoney += score.money;
            if (this.freezeMoney > this.requireMoney) {
                this.freezeMoney = this.requireMoney;
            }
            this.currentMoney += score.money;
            return score.money;
        }
        MoneyResponse res = moneyService.addingMoneyInGame(
                this.sessionId, this.nickName, GameUtils.gameName, String.valueOf(roomId),
                score.money, this.moneyTypeName, this.requireMoney, String.valueOf(gameId), score.wastedMoney);
        LoggerUtils.info("game_money", "addMoneyInGame: isSucess?", res.isSuccess(), res.getErrorCode(), "sessionId:", this.sessionId, "matchId:", gameId);
        if (!res.isSuccess()) {
            if (res.getResponseCode() == MoneyResponse.MoneyResponseCode.NOT_ENOUGH_MONEY) {
                throw new NotEnoughMoneyException();
            }
            throw new MoneyException("addingMoneyInGame: " + res.getResponseCode().errorCode + " - " + res.getResponseCode().description + " sessionId: " + this.sessionId + "matchId:" + gameId);
        }
        this.currentMoney = res.getCurrentMoney();
        this.freezeMoney = res.getFreezeMoney();
        return score.money;
    }

    private long addMoneyInTour(UserScore score, int roomId, int gameId) {
        this.freezeMoney += score.money;
        if (this.freezeMoney > this.requireMoney) {
            this.freezeMoney = this.requireMoney;
        }
        this.currentMoney += score.money;
        return score.money;
    }

    public static GameMoneyInfo copyFromDB(String sessionId) {
        StringBuilder key = new StringBuilder(GameMoneyInfo.class.getSimpleName());
        key.append(sessionId);
        GameMoneyInfo info = (GameMoneyInfo) DataUtils.copyDataFromDB(key.toString(), GameMoneyInfo.class);
        return info;
    }

    public String toString() {
        return GameUtils.toJsonString(this);
    }

    public JSONObject toJSONObject() {
        JSONObject json = null;
        try {
            json = new JSONObject(this.toString());
        } catch (JSONException jSONException) {
            // empty catch block
        }
        return json;
    }
}
