/*
 * Decompiled with CFR 0.150.
 */
package game.utils;

import game.utils.GameUtils;
import java.util.Arrays;
import java.util.List;

public class GameCommonUtils {
    private static final List<String> listGameLockCreate = Arrays.asList("XocDia", "Sam", "Tlmn", "Binh");

    public static boolean canCreateRoom() {
        return !listGameLockCreate.contains(GameUtils.gameName);
    }
}

