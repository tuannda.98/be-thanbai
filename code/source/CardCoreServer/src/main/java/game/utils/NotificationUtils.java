package game.utils;

import casio.king365.GU;

public class NotificationUtils {

    public static void sendOperation(String message) {
        GU.sendOperation(GameUtils.gameName + ": " + message);
    }

}
