/*
 * Decompiled with CFR 0.150.
 */
package game.xocdia.conf;

public class XocDiaBetBotModel {
    public byte id;
    public long money;
    public double ratio;
    public long moneyWin;

    public XocDiaBetBotModel(byte id, long money, double ratio) {
        this.id = id;
        this.money = money;
        this.ratio = ratio;
        this.moneyWin = 0L;
    }
}

