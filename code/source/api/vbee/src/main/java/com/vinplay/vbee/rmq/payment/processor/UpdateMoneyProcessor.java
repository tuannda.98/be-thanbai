package com.vinplay.vbee.rmq.payment.processor;

import casio.king365.util.KingUtil;
import com.hazelcast.core.IMap;
import com.vinplay.usercore.utils.UserMakertingUtil;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.hazelcast.HazelcastUtils;
import com.vinplay.vbee.common.messages.BaseMessage;
import com.vinplay.vbee.common.messages.MoneyMessageInMinigame;
import com.vinplay.vbee.common.models.cache.UserActiveModel;
import com.vinplay.vbee.common.utils.VinPlayUtils;
import com.vinplay.vbee.dao.impl.UserDaoImpl;
import org.apache.log4j.Logger;

import java.util.Date;

public class UpdateMoneyProcessor
        implements BaseProcessor<byte[], Boolean> {
    private static final Logger logger = Logger.getLogger("vbee");
    private static final long INTERVAL_TIME_UPDATE_MONEY = 10L;

    public Boolean execute(Param<byte[]> param) {
        byte[] body = param.get();
        MoneyMessageInMinigame message = (MoneyMessageInMinigame) BaseMessage.fromBytes(body);
        try {
            boolean updateTimeOut;
            block20:
            {
                KingUtil.printLog("UpdateMoneyProcessor - MoneyMessageInMinigame message: " + message);
                String nickname = message.getNickname();
                IMap userMap = HazelcastUtils.getActiveMap();
                updateTimeOut = true;
                if (userMap.containsKey(nickname)) {
                    try {
                        userMap.lock(nickname);
                        UserActiveModel model = (UserActiveModel) userMap.get(nickname);
                        if (Long.parseLong(message.getId()) < model.getLastMessageId() || model.isBot()) {
                            return true;
                        }
                        long currentTime = System.currentTimeMillis();
                        long validTimeMs = currentTime - 600000L;
                        if (model.getLastActive() > validTimeMs) {
                            updateTimeOut = false;
                        } else {
                            if (message.getMoneyType().equals("vin")) {
                                boolean bl = updateTimeOut = !model.isBot() || VinPlayUtils.updateMoneyTimeout(model.getLastActiveVin(), 30);
                                if (updateTimeOut) {
                                    model.setLastActiveVin(new Date().getTime());
                                }
                            } else {
                                updateTimeOut = model.isBot()
                                        ? VinPlayUtils.updateMoneyTimeout(model.getLastActiveXu(), 30)
                                        : VinPlayUtils.updateMoneyTimeout(model.getLastActiveXu(), 10);
                                if (updateTimeOut) {
                                    model.setLastActiveXu(new Date().getTime());
                                }
                            }
                            model.setLastActive(new Date().getTime());
                            model.setLastMessageId(Long.parseLong(message.getId()));
                        }
                        model.setUpdateMySQL(updateTimeOut);
                        userMap.put(nickname, model);
                    } catch (Exception e) {
                        logger.error(e);
                        break block20;
                    }
                    try {
                        userMap.unlock(nickname);
                    } catch (Exception e) {
                        // empty catch block
                    }
                }
            }
            int type = 0;
            if (message.getActionName().equals("Bot")) {
                type = 3;
            } else if (message.getActionName().equals("RechargeByCard") || message.getActionName().equals("RechargeByVinCard") || message.getActionName().equals("RechargeByMegaCard") || message.getActionName().equals("RechargeByBank") || message.getActionName().equals("RechargeByIAP") || message.getActionName().equals("RechargeBySMS") || message.getActionName().equals("TransferMoney") && message.getMoneyVP() == -1) {
                type = 1;
                UserMakertingUtil.userNapVin(message.getNickname(), message.getMoneyExchange());
            } else if (message.getActionName().equals("CashoutByCard") || message.getActionName().equals("CashoutByMomo") || message.getActionName().equals("CashoutByBank")) {
                type = 4;
            } else if (message.getMoneyVP() > 0 || message.getVp() != 0) {
                type = 2;
            }
            if (!updateTimeOut) {
                logger.info("update no timeout nickname: " + message.getNickname() + " money: " + message.getMoneyExchange() + " current: " + message.getAfterMoney() + " moneyType: " + message.getMoneyType());
                return true;
            }
            KingUtil.printLog("UpdateMoneyProcessor - type: " + type);
            UserDaoImpl userDao = new UserDaoImpl();
            userDao.updateMoney(message, type);
            KingUtil.printLog("UpdateMoneyProcessor - finish");
            return true;
        } catch (Exception e2) {
            KingUtil.printException("VBee UpdateMoneyProcessor", e2);
            e2.printStackTrace();
            return false;
        }
    }
}

