/*
 * Decompiled with CFR 0.144.
 * 
 * Could not load the following classes:
 *  com.mongodb.client.MongoCollection
 *  com.mongodb.client.MongoDatabase
 *  com.vinplay.vbee.common.messages.sentsms.LogSentSmsMessage
 *  com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory
 *  org.bson.Document
 */
package com.vinplay.vbee.dao.impl;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.vinplay.vbee.common.messages.sentsms.LogSentSmsMessage;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.dao.LogSentSms;
import java.util.Date;
import org.bson.Document;

public class LogSentSmsImpl
implements LogSentSms {
    @Override
    public void saveLogSentSms(LogSentSmsMessage message) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("log_sent_sms");
        Document doc = new Document();
        doc.append("trans_id", message.getTransId());
        doc.append("sms_content", message.getSms());
        doc.append("phone_number", message.getTel());
        doc.append("partner_trans_id", message.getPartnerTransId());
        doc.append("status", message.getStatus());
        doc.append("message", message.getMessage());
        doc.append("trans_time", message.getTransTime());
        doc.append("create_time", new Date());
        col.insertOne(doc);
    }
}

