/*
 * Decompiled with CFR 0.144.
 * 
 * Could not load the following classes:
 *  com.vinplay.vbee.common.cp.BaseProcessor
 *  com.vinplay.vbee.common.cp.Param
 *  com.vinplay.vbee.common.messages.minigame.TransactionTaiXiuMessage
 *  org.apache.log4j.Logger
 */
package com.vinplay.vbee.rmq.minigame.processor;

import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.messages.minigame.TransactionTaiXiuMessage;
import com.vinplay.vbee.dao.impl.TaiXiuDaoImpl;
import org.apache.log4j.Logger;

public class SaveTransactionTaiXiuProcessor
implements BaseProcessor<byte[], Boolean> {
    private static final Logger logger = Logger.getLogger("vbee");

    public Boolean execute(Param<byte[]> param) {
        byte[] body = param.get();
        try {
            TransactionTaiXiuMessage message = (TransactionTaiXiuMessage)TransactionTaiXiuMessage.fromBytes(body);
            TaiXiuDaoImpl dao = new TaiXiuDaoImpl();
            dao.saveTransactionTaiXiu(message);
            logger.debug("Handle message : " + message.referenceId);
        }
        catch (Exception e) {
            logger.error("Handle save transaction error ", e);
        }
        return false;
    }
}

