/*
 * Decompiled with CFR 0.144.
 * 
 * Could not load the following classes:
 *  com.mongodb.client.MongoCollection
 *  com.mongodb.client.MongoDatabase
 *  com.vinplay.cardlib.models.Card
 *  com.vinplay.cardlib.models.CardGroup
 *  com.vinplay.cardlib.models.Hand
 *  com.vinplay.cardlib.models.Rank
 *  com.vinplay.cardlib.utils.CardLibUtils
 *  com.vinplay.vbee.common.messages.minigame.LogCaoThapMessage
 *  com.vinplay.vbee.common.messages.minigame.LogCaoThapWinMessage
 *  com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory
 *  com.vinplay.vbee.common.utils.VinPlayUtils
 *  org.bson.Document
 */
package com.vinplay.vbee.dao.impl;

import casio.king365.util.KingUtil;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.vinplay.cardlib.models.Card;
import com.vinplay.cardlib.models.CardGroup;
import com.vinplay.cardlib.models.Hand;
import com.vinplay.cardlib.utils.CardLibUtils;
import com.vinplay.vbee.common.messages.minigame.LogCaoThapMessage;
import com.vinplay.vbee.common.messages.minigame.LogCaoThapWinMessage;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.utils.VinPlayUtils;
import com.vinplay.vbee.dao.CaoThapDao;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Date;

public class CaoThapDaoImpl
implements CaoThapDao {
    @Override
    public void logCaoThap(LogCaoThapMessage message) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("log_cao_thap");
        Document doc = new Document();
        doc.append("trans_id", message.transId);
        doc.append("nick_name", message.nickname);
        doc.append("pot_bet", message.potBet);
        doc.append("step", message.step);
        doc.append("bet_value", message.betValue);
        doc.append("result", message.result);
        doc.append("prize", message.prize);
        doc.append("cards", message.cards);
        doc.append("current_pot", message.currentPot);
        doc.append("current_fund", message.currentFund);
        doc.append("money_type", message.moneyType);
        doc.append("time_log", VinPlayUtils.getCurrentDateTime());
        doc.append("create_time", new Date());
        col.insertOne(doc);
    }

    @Override
    public void logCaoThapWin(LogCaoThapWinMessage message) {
        String[] arrCard;
        int gt = -1;
        String handStr = "";
        int rank1 = -1;
        int rank2 = -1;
        int rank3 = -1;
        int rank4 = -1;
        int rank5 = -1;
        ArrayList<Card> cardLst = new ArrayList<>();
        String cardStr = message.cards.substring(0, message.cards.length() - 1);
        for (String cd : arrCard = cardStr.split(",")) {
            if (cd.trim().isEmpty()) continue;
            cardLst.add(new Card(Integer.parseInt(cd.trim())));
        }
        try {
            if (message.moneyType == 1 && cardLst.size() >= 5) {
                CardGroup cg = CardLibUtils.calculatePoker(cardLst);
                gt = cg.getType();
                Hand hand = cg.getHand();
                rank1 = hand.getCards().get(0).getRank().getRank();
                rank2 = hand.getCards().get(1).getRank().getRank();
                rank3 = hand.getCards().get(2).getRank().getRank();
                rank4 = hand.getCards().get(3).getRank().getRank();
                rank5 = hand.getCards().get(4).getRank().getRank();
                handStr = hand.cardsToString();
            }
        }
        catch (Exception e) {
            KingUtil.printException("CaoThapDaoImpl", e);
            e.printStackTrace();
        }
        KingUtil.printLog("CaoThapDaoImpl begin insert");
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("log_cao_thap_win");
        Document doc = new Document();
        doc.append("trans_id", message.transId);
        doc.append("nick_name", message.nickname);
        doc.append("bet_value", message.betValue);
        doc.append("result", message.result);
        doc.append("prize", message.prize);
        doc.append("cards", cardStr);
        doc.append("money_type", message.moneyType);
        doc.append("time_log", VinPlayUtils.getCurrentDateTime());
        doc.append("group_type", gt);
        doc.append("hand", handStr);
        doc.append("rank_1", rank1);
        doc.append("rank_2", rank2);
        doc.append("rank_3", rank3);
        doc.append("rank_4", rank4);
        doc.append("rank_5", rank5);
        doc.append("money_win", message.prize - message.betValue);
        doc.append("create_time", new Date());
        col.insertOne(doc);
        KingUtil.printLog("CaoThapDaoImpl finish insert");
    }
}

