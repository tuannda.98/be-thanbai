/*
 * Decompiled with CFR 0.144.
 * 
 * Could not load the following classes:
 *  com.mongodb.client.MongoCollection
 *  com.mongodb.client.MongoDatabase
 *  com.vinplay.vbee.common.messages.dvt.RechargeByBankMessage
 *  com.vinplay.vbee.common.messages.dvt.RechargeByCardMessage
 *  com.vinplay.vbee.common.messages.dvt.RefundFeeAgentMessage
 *  com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory
 *  org.bson.Document
 */
package com.vinplay.vbee.dao.impl;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.vinplay.vbee.common.messages.dvt.RechargeByBankMessage;
import com.vinplay.vbee.common.messages.dvt.RechargeByCardMessage;
import com.vinplay.vbee.common.messages.dvt.RefundFeeAgentMessage;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.dao.RechargeDao;
import java.util.Date;
import org.bson.Document;

public class RechargeDaoImpl
implements RechargeDao {
    @Override
    public void logRechargeByCard(RechargeByCardMessage message) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("dvt_recharge_by_card");
        Document doc = new Document();
        doc.append("reference_id", message.getReferenceId());
        doc.append("nick_name", message.getNickname());
        doc.append("provider", message.getProvider());
        doc.append("serial", message.getSerial());
        doc.append("pin", message.getPin());
        doc.append("amount", message.getAmount());
        doc.append("money", message.getMoney());
        doc.append("status", message.getStatus());
        doc.append("message", message.getMessage());
        doc.append("code", message.getCode());
        doc.append("time_log", message.getCreateTime());
        doc.append("update_time", message.getCreateTime());
        doc.append("create_time", new Date());
        doc.append("partner", message.getPartner());
        doc.append("platform", message.getPlatform());
        doc.append("user_mega", message.getUserNameMega());
        col.insertOne(doc);
    }

    @Override
    public void logRechargeByVinCard(RechargeByCardMessage message) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("dvt_recharge_by_vin_card");
        Document doc = new Document();
        doc.append("reference_id", message.getReferenceId());
        doc.append("nick_name", message.getNickname());
        doc.append("provider", message.getProvider());
        doc.append("serial", message.getSerial());
        doc.append("pin", message.getPin());
        doc.append("amount", message.getAmount());
        doc.append("money", message.getMoney());
        doc.append("status", message.getStatus());
        doc.append("message", message.getMessage());
        doc.append("code", message.getCode());
        doc.append("time_log", message.getCreateTime());
        doc.append("update_time", message.getCreateTime());
        doc.append("create_time", new Date());
        doc.append("platform", message.getPlatform());
        col.insertOne(doc);
    }

    @Override
    public void logRechargeByMegaCard(RechargeByCardMessage message) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("epay_recharge_by_mega_card");
        Document doc = new Document();
        doc.append("reference_id", message.getReferenceId());
        doc.append("nick_name", message.getNickname());
        doc.append("provider", message.getProvider());
        doc.append("serial", message.getSerial());
        doc.append("pin", message.getPin());
        doc.append("amount", message.getAmount());
        doc.append("money", message.getMoney());
        doc.append("status", message.getStatus());
        doc.append("message", message.getMessage());
        doc.append("code", message.getCode());
        doc.append("time_log", message.getCreateTime());
        doc.append("update_time", message.getCreateTime());
        doc.append("create_time", new Date());
        doc.append("partner", message.getPartner());
        doc.append("platform", message.getPlatform());
        col.insertOne(doc);
    }

    @Override
    public void logRechargeByBank(RechargeByBankMessage message) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("dvt_recharge_by_bank");
        Document doc = new Document();
        doc.append("nick_name", message.getNickname());
        doc.append("money", message.getMoney());
        doc.append("bank", message.getBank());
        doc.append("trans_id", message.getTransId());
        doc.append("amount", message.getAmount());
        doc.append("order_info", message.getOrderInfo());
        doc.append("ticket_no", message.getTicketNo());
        doc.append("trans_time", message.getCreateTime());
        doc.append("response_code", "");
        doc.append("description", "");
        doc.append("amount_receive", "");
        doc.append("transaction_no", "");
        doc.append("message", "");
        doc.append("update_time", "");
        doc.append("create_time", new Date());
        doc.append("platform", message.getPlatform());
        col.insertOne(doc);
    }

    @Override
    public void logRefundFeeAgent(RefundFeeAgentMessage message) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("log_refund_fee_agent");
        Document doc = new Document();
        doc.append("nick_name", message.getNickname());
        doc.append("fee_1", message.getFee1());
        doc.append("ratio_1", message.getRatio1());
        doc.append("fee_2", message.getFee2());
        doc.append("ratio_2", message.getRatio2());
        doc.append("fee_2_more", message.getFee2More());
        doc.append("ratio_2_more", message.getRatio2More());
        doc.append("fee", message.getFee());
        doc.append("code", message.getCode());
        doc.append("month", message.getMonth());
        doc.append("description", message.getDescription());
        doc.append("time_log", message.getCreateTime());
        doc.append("create_time", new Date());
        doc.append("fee_vinplay_card", message.getFeeVinplayCard());
        doc.append("fee_vin_cash", message.getFeeVinCash());
        doc.append("percent", message.getPercent());
        doc.append("end_time", message.getEndTime());
        doc.append("start_time", message.getStartTime());
        col.insertOne(doc);
    }
}

