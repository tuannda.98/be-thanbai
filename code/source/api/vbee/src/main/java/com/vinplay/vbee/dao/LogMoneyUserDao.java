/*
 * Decompiled with CFR 0.144.
 * 
 * Could not load the following classes:
 *  com.vinplay.vbee.common.messages.LogChuyenTienDaiLyMessage
 *  com.vinplay.vbee.common.messages.LogMoneyUserMessage
 *  com.vinplay.vbee.common.messages.gamebai.LogNoHuGameBaiMessage
 *  com.vinplay.vbee.common.messages.pay.ExchangeMessage
 *  com.vinplay.vbee.common.models.TopUserPlayGame
 */
package com.vinplay.vbee.dao;

import com.vinplay.vbee.common.messages.LogChuyenTienDaiLyMessage;
import com.vinplay.vbee.common.messages.LogMoneyUserMessage;
import com.vinplay.vbee.common.messages.gamebai.LogNoHuGameBaiMessage;
import com.vinplay.vbee.common.messages.pay.ExchangeMessage;
import com.vinplay.vbee.common.models.TopUserPlayGame;
import java.sql.SQLException;

public interface LogMoneyUserDao {
    boolean saveLogMoneyUser(LogMoneyUserMessage var1, long var2, boolean var4, boolean var5);

    boolean saveLogMoneyUserVinOther(LogMoneyUserMessage var1, long var2, int var4);

    void saveLogMoneySystem(String var1, long var2, long var4, String var6);

    long getLastReferenceId(String var1);

    boolean logTopUserPlayGame(TopUserPlayGame var1);

    boolean logChuyenTienDaiLy(LogChuyenTienDaiLyMessage var1);

    void logChuyenTienDaiLyMySQL(LogChuyenTienDaiLyMessage var1) throws SQLException;

    boolean logNoHuGameBai(LogNoHuGameBaiMessage var1);

    boolean checkBot(String var1) throws SQLException;

    boolean logExchangeMoney(ExchangeMessage var1);
}

