/*
 * Decompiled with CFR 0.144.
 *
 * Could not load the following classes:
 *  com.mongodb.BasicDBObject
 *  com.mongodb.client.FindIterable
 *  com.mongodb.client.MongoCollection
 *  com.mongodb.client.MongoDatabase
 *  com.mongodb.client.model.FindOneAndUpdateOptions
 *  com.vinplay.vbee.common.messages.LogChuyenTienDaiLyMessage
 *  com.vinplay.vbee.common.messages.LogMoneyUserMessage
 *  com.vinplay.vbee.common.messages.gamebai.LogNoHuGameBaiMessage
 *  com.vinplay.vbee.common.messages.pay.ExchangeMessage
 *  com.vinplay.vbee.common.models.TopUserPlayGame
 *  com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory
 *  com.vinplay.vbee.common.pools.ConnectionPool
 *  com.vinplay.vbee.common.utils.DateTimeUtils
 *  org.bson.Document
 *  org.bson.conversions.Bson
 */
package com.vinplay.vbee.dao.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.FindOneAndUpdateOptions;
import com.vinplay.vbee.common.messages.LogChuyenTienDaiLyMessage;
import com.vinplay.vbee.common.messages.LogMoneyUserMessage;
import com.vinplay.vbee.common.messages.gamebai.LogNoHuGameBaiMessage;
import com.vinplay.vbee.common.messages.pay.ExchangeMessage;
import com.vinplay.vbee.common.models.TopUserPlayGame;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.pools.ConnectionPool;
import com.vinplay.vbee.common.utils.DateTimeUtils;
import com.vinplay.vbee.dao.LogMoneyUserDao;
import org.bson.Document;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;

public class LogMoneyUserDaoImpl
        implements LogMoneyUserDao {
    @Override
    public boolean saveLogMoneyUser(LogMoneyUserMessage message, long transId, boolean isBot, boolean playGame) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = null;
        if (message.getMoneyType().equals("vin")) {
            col = db.getCollection("log_money_user_vin");
        } else if (message.getMoneyType().equals("xu")) {
            col = db.getCollection("log_money_user_xu");
        }
        Document doc = new Document();
        doc.append("trans_id", transId);
        doc.append("user_id", message.getUserId());
        doc.append("nick_name", message.getNickname());
        doc.append("service_name", message.getServiceName());
        doc.append("current_money", message.getCurrentMoney());
        doc.append("money_exchange", message.getMoneyExchange());
        doc.append("description", message.getDescription());
        doc.append("trans_time", message.getCreateTime());
        doc.append("action_name", message.getActionName());
        doc.append("fee", message.getFee());
        doc.append("is_bot", isBot);
        doc.append("play_game", playGame);
        doc.append("create_time", new Date());
        col.insertOne(doc);
        return true;
    }

    @Override
    public boolean saveLogMoneyUserVinOther(LogMoneyUserMessage message, long transId, int type) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = null;
        col = type == 3 ? db.getCollection("log_money_user_nap_vin") : db.getCollection("log_money_user_tieu_vin");
        Document doc = new Document();
        doc.append("trans_id", transId);
        doc.append("user_id", message.getUserId());
        doc.append("nick_name", message.getNickname());
        doc.append("service_name", message.getServiceName());
        doc.append("current_money", message.getCurrentMoney());
        doc.append("money_exchange", message.getMoneyExchange());
        doc.append("description", message.getDescription());
        doc.append("trans_time", message.getCreateTime());
        doc.append("action_name", message.getActionName());
        doc.append("fee", message.getFee());
        doc.append("create_time", new Date());
        col.insertOne(doc);
        return true;
    }

    @Override
    public long getLastReferenceId(String moneyType) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        HashMap conditions = new HashMap();
        BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        FindIterable iterable = null;
        if (moneyType.equals("vin")) {
            iterable = db.getCollection("log_money_user_vin").find(new Document(conditions)).sort(objsort).limit(1);
        } else if (moneyType.equals("xu")) {
            iterable = db.getCollection("log_money_user_xu").find(new Document(conditions)).sort(objsort).limit(1);
        }
        Document document = iterable != null ? (Document) iterable.first() : null;
        return document == null ? 0L : document.getLong("trans_id");
    }

    @Override
    public void saveLogMoneySystem(String name, long currentMoney, long moneyExchange, String transTime) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("log_money_system");
        Document doc = new Document();
        doc.append("name", name);
        doc.append("current_money", currentMoney);
        doc.append("money_exchange", moneyExchange);
        doc.append("trans_time", transTime);
        doc.append("create_time", new Date());
        col.insertOne(doc);
    }

    @Override
    public boolean logTopUserPlayGame(TopUserPlayGame userWin) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = null;
        col = userWin.getMoneyType().equals("vin") ? db.getCollection("top_user_play_game_vin") : db.getCollection("top_user_play_game_xu");
        BasicDBObject updateFields = new BasicDBObject();
        updateFields.append("money_win", userWin.getMoneyWin());
        BasicDBObject conditions = new BasicDBObject();
        conditions.append("nick_name", userWin.getNickname());
        conditions.append("money_type", userWin.getMoneyType());
        conditions.append("date", userWin.getDate());
        FindOneAndUpdateOptions options = new FindOneAndUpdateOptions();
        options.upsert(true);
        col.findOneAndUpdate(conditions, new Document("$inc", updateFields), options);
        return true;
    }

    @Override
    public boolean logChuyenTienDaiLy(LogChuyenTienDaiLyMessage message) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("log_chuyen_tien_dai_ly");
        Document doc = new Document();
        doc.append("nick_name_send", message.getNicknameSend());
        doc.append("nick_name_receive", message.getNicknameReceive());
        doc.append("money_send", message.getMoneySend());
        doc.append("money_receive", message.getMoneyReceive());
        doc.append("status", message.getStatus());
        doc.append("fee", message.getFee());
        doc.append("trans_time", message.getTransTime());
        doc.append("top_ds", 1);
        doc.append("process", 0);
        doc.append("des_send", message.getDesSend());
        doc.append("des_receive", message.getDesReceive());
        doc.append("process", 0);
        doc.append("create_time", new Date());
        doc.append("transaction_no", message.getTransactionId());
        doc.append("is_freeze_money", message.getIsFreezeMoney());
        doc.append("agent_level1", message.getAgentLevel1());
        doc.append("session_id_freeze_money", message.getSessionIdFreezeMoney());
        col.insertOne(doc);
        return true;
    }

    @Override
    public void logChuyenTienDaiLyMySQL(LogChuyenTienDaiLyMessage message) throws SQLException {
        String sql = " INSERT INTO vinplay.log_tranfer_agent " +
                " (  transaction_no,  agent_level1,  nick_name_send,  nick_name_receive," +
                "money_send,  money_receive,  status,  fee," +
                "top_ds,  process,  ti_gia,  is_freeze_money," +
                "des_send,  des_receive,  session_id_freeze_money,  trans_time,  update_time  )" +
                "        VALUES (?,?,?,?," +
                "?,?,?,?," +
                "?,?,?,?," +
                "?,?,?,?,?) ";
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, message.getTransactionId());
            stmt.setString(2, message.getAgentLevel1());
            stmt.setString(3, message.getNicknameSend());
            stmt.setString(4, message.getNicknameReceive());
            stmt.setLong(5, message.getMoneySend());
            stmt.setLong(6, message.getMoneyReceive());
            stmt.setInt(7, message.getStatus());
            stmt.setLong(8, message.getFee());
            stmt.setInt(9, 1);
            stmt.setInt(10, 0);
            stmt.setInt(11, 0);
            stmt.setInt(12, message.getIsFreezeMoney());
            stmt.setString(13, message.getDesSend());
            stmt.setString(14, message.getDesReceive());
            stmt.setString(15, message.getSessionIdFreezeMoney());
            stmt.setString(16, message.getTransTime());
            stmt.setString(17, DateTimeUtils.getCurrentTime("yyyy-MM-dd HH:mm:ss"));
            stmt.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public boolean logNoHuGameBai(LogNoHuGameBaiMessage message) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("log_no_hu_game_bai");
        Document doc = new Document();
        doc.append("nick_name", message.getNickname());
        doc.append("room", message.getRoom());
        doc.append("pot_value", message.getPotValue());
        doc.append("money_win", message.getMoneyWin());
        doc.append("game_name", message.getGamename());
        doc.append("description", message.getDescription());
        doc.append("trans_time", message.getCreateTime());
        doc.append("create_time", new Date());
        doc.append("tour_id", message.getTourId());
        col.insertOne(doc);
        return true;
    }

    @Override
    public boolean checkBot(String nickname) throws SQLException {
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement("SELECT is_bot FROM users WHERE nick_name=?")) {
            stm.setString(1, nickname);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next() && rs.getInt("is_bot") == 1) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean logExchangeMoney(ExchangeMessage message) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("log_exchange_money");
        Document doc = new Document();
        doc.append("nick_name", message.nickname);
        doc.append("merchant_id", message.merchantId);
        doc.append("trans_id", message.merchantTransId);
        doc.append("money", message.money);
        doc.append("money_type", message.moneyType);
        doc.append("type", message.type);
        doc.append("money_exchange", message.exchangeMoney);
        doc.append("fee", message.fee);
        doc.append("code", message.code);
        doc.append("ip", message.ip);
        doc.append("trans_time", message.getCreateTime());
        doc.append("create_time", new Date());
        col.insertOne(doc);
        return true;
    }
}
