/*
 * Decompiled with CFR 0.144.
 * 
 * Could not load the following classes:
 *  com.mongodb.client.MongoCollection
 *  com.mongodb.client.MongoDatabase
 *  com.vinplay.vbee.common.messages.BrandnameDLVRMessage
 *  com.vinplay.vbee.common.messages.BrandnameMessage
 *  com.vinplay.vbee.common.messages.OtpMessage
 *  com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory
 *  org.bson.Document
 */
package com.vinplay.vbee.dao.impl;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.vinplay.vbee.common.messages.BrandnameDLVRMessage;
import com.vinplay.vbee.common.messages.BrandnameMessage;
import com.vinplay.vbee.common.messages.OtpMessage;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.dao.OtpDao;
import java.util.Date;
import org.bson.Document;

public class OtpDaoImpl
implements OtpDao {
    @Override
    public boolean saveLogOtp(OtpMessage message) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("vmg_transaction");
        Document doc = new Document();
        doc.append("request_id", message.getRequestId());
        doc.append("mobile", message.getMobile());
        doc.append("command_code", message.getCommandCode());
        doc.append("message_MO", message.getMessageMO());
        doc.append("response_MO", message.getResponseMO());
        doc.append("message_MT", message.getMessageMT());
        doc.append("response_MT", message.getResponseMT());
        doc.append("trans_time", message.getCreateTime());
        doc.append("create_time", new Date());
        col.insertOne(doc);
        return true;
    }

    @Override
    public boolean saveLogBrandname(BrandnameMessage message) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("brandname_transaction");
        Document doc = new Document();
        doc.append("request_id", message.getRequestId());
        doc.append("brandname", message.getBrandname());
        doc.append("message", message.getMessage());
        doc.append("receives", message.getReceives());
        doc.append("code", message.getCode());
        doc.append("trans_time", message.getCreateTime());
        doc.append("status", -1);
        doc.append("count", -1);
        doc.append("status_desc", "");
        doc.append("sent_date", "");
        doc.append("update_time", "");
        doc.append("create_time", new Date());
        col.insertOne(doc);
        return true;
    }

    @Override
    public boolean saveLogBrandnameDLVR(BrandnameDLVRMessage message) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("brandname_dlvr_transaction");
        Document doc = new Document();
        doc.append("request_id", message.getRequestId());
        doc.append("status", message.getSmsStatus());
        doc.append("count", message.getCount());
        doc.append("status_desc", message.getStatusDesc());
        doc.append("sent_date", message.getSentDate());
        doc.append("trans_time", message.getCreateTime());
        doc.append("create_time", new Date());
        col.insertOne(doc);
        return true;
    }
}

