/*
 * Decompiled with CFR 0.144.
 * 
 * Could not load the following classes:
 *  com.mongodb.BasicDBObject
 *  com.mongodb.client.MongoCollection
 *  com.mongodb.client.MongoDatabase
 *  com.mongodb.client.model.FindOneAndUpdateOptions
 *  com.vinplay.vbee.common.messages.dvt.CashoutByBankMessage
 *  com.vinplay.vbee.common.messages.dvt.CashoutByCardMessage
 *  com.vinplay.vbee.common.messages.dvt.CashoutByTopUpMessage
 *  com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory
 *  com.vinplay.vbee.common.utils.VinPlayUtils
 *  org.bson.Document
 *  org.bson.conversions.Bson
 */
package com.vinplay.vbee.dao.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.FindOneAndUpdateOptions;
import com.vinplay.vbee.common.messages.dvt.CashoutByBankMessage;
import com.vinplay.vbee.common.messages.dvt.CashoutByCardMessage;
import com.vinplay.vbee.common.messages.dvt.CashoutByTopUpMessage;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.utils.VinPlayUtils;
import com.vinplay.vbee.dao.CashoutDao;
import java.text.ParseException;
import java.util.Date;
import org.bson.Document;

public class CashoutDaoImpl
implements CashoutDao {
    @Override
    public void logCashoutByCard(CashoutByCardMessage message) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("dvt_cash_out_by_card");
        Document doc = new Document();
        doc.append("reference_id", message.getReferenceId());
        doc.append("nick_name", message.getNickname());
        doc.append("provider", message.getProvider());
        doc.append("amount", message.getAmount());
        doc.append("quantity", message.getQuantity());
        doc.append("softpin", message.getSoftpin().toString());
        doc.append("status", message.getStatus());
        doc.append("message", message.getMessage());
        doc.append("sign", message.getSign());
        doc.append("code", message.getCode());
        doc.append("time_log", message.getCreateTime());
        doc.append("update_time", message.getCreateTime());
        doc.append("create_time", new Date());
        doc.append("partner", message.getPartner());
        doc.append("partner_trans_id", message.getPartnerTransId());
        doc.append("is_scanned", 1);
        col.insertOne(doc);
        if (message.getCode() == 0) {
            try {
                this.logMoneyCashoutUser(message.getNickname(), message.getAmount() * message.getQuantity(), message.getCreateTime());
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void logCashoutByTopUp(CashoutByTopUpMessage message) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("dvt_cash_out_by_topup");
        Document doc = new Document();
        doc.append("reference_id", message.getReferenceId());
        doc.append("nick_name", message.getNickname());
        doc.append("target", message.getTarget());
        doc.append("amount", message.getAmount());
        doc.append("status", message.getStatus());
        doc.append("message", message.getMessage());
        doc.append("sign", message.getSign());
        doc.append("code", message.getCode());
        doc.append("time_log", message.getCreateTime());
        doc.append("update_time", message.getCreateTime());
        doc.append("create_time", new Date());
        doc.append("partner", message.getPartner());
        doc.append("partner_trans_id", message.getPartnerTransId());
        doc.append("provider", message.getProvider());
        doc.append("type", message.getType());
        col.insertOne(doc);
        if (message.getCode() == 0) {
            try {
                this.logMoneyCashoutUser(message.getNickname(), message.getAmount(), message.getCreateTime());
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void logCashoutByBank(CashoutByBankMessage message) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("dvt_cash_out_by_bank");
        Document doc = new Document();
        doc.append("reference_id", message.getReferenceId());
        doc.append("nick_name", message.getNickname());
        doc.append("bank", message.getBank());
        doc.append("account", message.getAccount());
        doc.append("name", message.getName());
        doc.append("amount", message.getAmount());
        doc.append("status", message.getStatus());
        doc.append("message", message.getMessage());
        doc.append("sign", message.getSign());
        doc.append("code", message.getCode());
        doc.append("description", message.getDesc());
        doc.append("time_log", message.getCreateTime());
        doc.append("update_time", message.getCreateTime());
        doc.append("create_time", new Date());
        col.insertOne(doc);
    }

    private void logMoneyCashoutUser(String nickname, int money, String datetime) throws ParseException {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("log_cash_out_user_daily");
        BasicDBObject updateFields = new BasicDBObject();
        updateFields.append("money", money);
        BasicDBObject conditions = new BasicDBObject();
        conditions.append("nick_name", nickname);
        conditions.append("date", VinPlayUtils.getDateFromDateTime(datetime));
        FindOneAndUpdateOptions options = new FindOneAndUpdateOptions();
        options.upsert(true);
        col.findOneAndUpdate(conditions, new Document("$inc", updateFields), options);
    }
}

