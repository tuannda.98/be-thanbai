package com.vinplay.vbee.dao.impl;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.vinplay.vbee.common.messages.statistic.LoginPortalInfoMsg;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.utils.VinPlayUtils;
import com.vinplay.vbee.dao.StatisticDao;
import org.bson.Document;

import java.util.Date;

public class StatisticDaoImpl
        implements StatisticDao {
    @Override
    public boolean saveLoginPortalInfo(LoginPortalInfoMsg msg) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("user_login_info");
        Document doc = new Document();
        doc.append("user_id", msg.getUserId());
        doc.append("user_name", msg.getUsername());
        doc.append("nick_name", msg.getNickname());
        doc.append("ip", msg.getIp());
        doc.append("main_ip", msg.getMainIp());
        doc.append("agent", msg.getAgent());
        doc.append("type", msg.getType());
        doc.append("time_log", VinPlayUtils.getCurrentDateTime());
        doc.append("create_time", new Date());
        col.insertOne(doc);
        return true;
    }
}
