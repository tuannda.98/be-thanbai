/*
 * Decompiled with CFR 0.144.
 *
 * Could not load the following classes:
 *  com.vinplay.vbee.common.hazelcast.HazelcastLoader
 *  com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory
 *  org.apache.log4j.Logger
 *  org.apache.log4j.PropertyConfigurator
 */
package com.vinplay.vbee.main;

import com.vinplay.vbee.common.hazelcast.HazelcastLoader;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.dao.impl.LogMoneyUserDaoImpl;
import com.vinplay.vbee.dao.impl.LuckyDaoImpl;
import com.vinplay.vbee.rmq.RMQ;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.hazelcast.HazelcastAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, HazelcastAutoConfiguration.class})
@ComponentScan({"game", "bitzero", "casio", "com.vinplay", "extension"})
public class VBeeMain {
    private static final String LOG_PROPERTIES_FILE = "config/log4j.properties";
    private static final Logger logger = Logger.getLogger("vbee");
    public static long luckyReferenceId;
    public static long moneyVinReferenceId;
    public static long moneyXuReferenceId;

    private static void initializeLogger() {
        Properties logProperties = new Properties();
        try {
            File file = new File(LOG_PROPERTIES_FILE);
            logProperties.load(new FileInputStream(file));
            PropertyConfigurator.configure(logProperties);
            logger.info("Logging initialized.");
        } catch (IOException e) {
            throw new RuntimeException("Unable to load logging property config/log4j.properties");
        }
    }

    public static void main(String[] args) {
        VBeeMain.initializeLogger();
        logger.info("VBEE INIT");
        try {
            HazelcastLoader.start();
            MongoDBConnectionFactory.init();
            LuckyDaoImpl lkDao = new LuckyDaoImpl();
            luckyReferenceId = lkDao.getLastReferenceId();
            LogMoneyUserDaoImpl mnDao = new LogMoneyUserDaoImpl();
            moneyVinReferenceId = mnDao.getLastReferenceId("vin");
            moneyXuReferenceId = mnDao.getLastReferenceId("xu");
            logger.info("luckyReferenceId: " + luckyReferenceId);
            logger.info("moneyVinReferenceId: " + moneyVinReferenceId);
            logger.info("moneyXuReferenceId: " + moneyXuReferenceId);
            RMQ.start();
        } catch (IOException e) {
            e.printStackTrace();
            logger.debug(e);
        }
    }
}

