package com.vinplay.vbee.rmq;

import casio.king365.GU;
import com.rabbitmq.client.*;
import com.vinplay.vbee.common.cp.BaseController;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.rmq.RMQConnectionFactory;
import com.vinplay.vbee.logger.HandleMessageLogger;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

public class RMQConsumer {
    private static final int PREFETCH_COUNT = 20;
    private static final Logger logger = Logger.getLogger("vbee");
    private BaseController<byte[], Boolean> controller;
    private final String queueName;
    private int numConsumer = 1;

    public RMQConsumer(String queueName, int numConsumer) {
        this.queueName = queueName;
        if (numConsumer > 1) {
            this.numConsumer = numConsumer;
        }
    }

    public void start(Map<Integer, String> commandMap) {
        try {
            this.controller = new BaseController();
            for (Map.Entry<Integer, String> entry : commandMap.entrySet()) {
                logger.debug("  " + entry.getKey() + " - " + entry.getValue());
            }
            try {
                this.controller.initCommands(commandMap);
            } catch (Exception e) {
                e.printStackTrace();
                logger.error(e.getMessage());
            }
            Connection connection = RMQConnectionFactory.newConnection();
            Channel channel = connection.createChannel();
            channel.queueDeclare(this.queueName, true, false, false, null);
            this.run(channel);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
    }

    private void run(final Channel channel) throws IOException {
        channel.basicQos(20);
        DefaultConsumer consumer = new DefaultConsumer(channel) {

            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {
                long startHandleTime = System.currentTimeMillis();
                int command = Integer.valueOf(properties.getMessageId());
                try {
                    logger.debug(RMQConsumer.this.queueName + " HANDLE MESSAGE: " + command);
                    Param p = new Param();
                    p.set(body);
                    RMQConsumer.this.controller.processCommand(Integer.valueOf(command), p);
                    channel.basicAck(envelope.getDeliveryTag(), false);
                } catch (Exception e) {
                    e.printStackTrace();
                    GU.sendOperation("Veebee error at consumer, command " + command + e.getMessage());
                    logger.error(RMQConsumer.this.queueName + " HANDLE MESSAGE: " + command + " ERROR: ", e);
                }
                long endHandleTime = System.currentTimeMillis();
                long handleTime = endHandleTime - startHandleTime;
                HandleMessageLogger.log(RMQConsumer.this.queueName, command, handleTime, new Date());
            }
        };
        for (int i = 0; i < this.numConsumer; ++i) {
            channel.basicConsume(this.queueName, false, consumer);
        }
    }
}
