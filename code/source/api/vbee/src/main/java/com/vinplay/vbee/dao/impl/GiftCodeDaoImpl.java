/*
 * Decompiled with CFR 0.144.
 * 
 * Could not load the following classes:
 *  com.mongodb.BasicDBObject
 *  com.mongodb.client.MongoCollection
 *  com.mongodb.client.MongoDatabase
 *  com.mongodb.client.result.UpdateResult
 *  com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory
 *  org.bson.Document
 *  org.bson.conversions.Bson
 */
package com.vinplay.vbee.dao.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoDatabase;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.dao.GiftCodeDao;
import java.sql.SQLException;
import org.bson.Document;

public class GiftCodeDaoImpl
implements GiftCodeDao {
    @Override
    public void updateGiftCodeStore(String giftCode) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        BasicDBObject updateFields = new BasicDBObject();
        updateFields.append("count_use", 1);
        db.getCollection("gift_code_store").updateOne(new Document("giftcode", giftCode), new Document("$set", updateFields));
    }

    @Override
    public void lockGiftCode(String giftCode) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        BasicDBObject updateFields = new BasicDBObject();
        updateFields.append("block", 1);
        db.getCollection("gift_code").updateOne(new Document("giftcode", giftCode), new Document("$set", updateFields));
    }
}

