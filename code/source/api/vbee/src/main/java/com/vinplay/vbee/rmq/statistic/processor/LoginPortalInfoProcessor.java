/*
 * Decompiled with CFR 0.144.
 * 
 * Could not load the following classes:
 *  com.vinplay.vbee.common.cp.BaseProcessor
 *  com.vinplay.vbee.common.cp.Param
 *  com.vinplay.vbee.common.messages.BaseMessage
 *  com.vinplay.vbee.common.messages.statistic.LoginPortalInfoMsg
 */
package com.vinplay.vbee.rmq.statistic.processor;

import casio.king365.util.KingUtil;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.messages.BaseMessage;
import com.vinplay.vbee.common.messages.statistic.LoginPortalInfoMsg;
import com.vinplay.vbee.dao.impl.StatisticDaoImpl;

import java.net.InetAddress;

public class LoginPortalInfoProcessor
implements BaseProcessor<byte[], Boolean> {
    public Boolean execute(Param<byte[]> param) {
        LoginPortalInfoMsg msg = (LoginPortalInfoMsg)BaseMessage.fromBytes(param.get());
        // Loại bỏ IP proxy. Ex:
        String ip = msg.getIp();
        if(ip.contains(",")){
            // string ip này có chứa cả ip proxy
            String[] arrIp = ip.split(",");
            ip = arrIp[0].trim();
            try {
                InetAddress inet = InetAddress.getByName(ip);
                ip = inet.getHostAddress();
            } catch (Exception e){
                e.printStackTrace();
                KingUtil.printException("LoginPortalInfoProcessor", e);
            }
        }
        msg.setMainIp(ip);
        StatisticDaoImpl dao = new StatisticDaoImpl();
        return dao.saveLoginPortalInfo(msg);
    }
}

