package com.vinplay.vbee.dao.impl;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.vinplay.vbee.common.messages.NoHuMessage;
import com.vinplay.vbee.common.messages.PotMessage;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.pools.ConnectionPool;
import com.vinplay.vbee.dao.PotDao;
import org.bson.Document;

import java.sql.*;
import java.util.Date;

public class PotDaoImpl
        implements PotDao {
    @Override
    public boolean addMoneyPot(PotMessage message) throws SQLException {
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_minigame");
             CallableStatement call = conn.prepareCall("CALL cong_tien_hu_game_bai(?,?,?)")) {
            int param = 1;
            call.setString(param++, message.getPotName());
            call.setLong(param++, message.getValuePot());
            call.setLong(param++, message.getValuePotSystem());
            call.executeUpdate();
        }
        return true;
    }

    @Override
    public boolean nohu(NoHuMessage message) throws SQLException {
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_minigame");
             CallableStatement call = conn.prepareCall("CALL no_hu_game_bai(?,?,?,?,?,?,?)")) {
            int param = 1;
            call.setString(param++, message.getSessionId());
            call.setInt(param++, message.getUserId());
            call.setLong(param++, message.getAfterMoneyUse());
            call.setLong(param++, message.getAfterMoney());
            call.setLong(param++, message.getFreezeMoney());
            call.setLong(param++, message.getPotValue());
            call.setString(param++, message.getPotName());
            call.executeUpdate();
        }
        return true;
    }

    @Override
    public long getPotValue(String potName) throws SQLException {
        long value = 0L;
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_minigame");
             PreparedStatement stm = conn.prepareStatement("SELECT value FROM hu_game_bai WHERE pot_name=?")
        ) {
            stm.setString(1, potName);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    value = rs.getLong("value");
                }
            }
        }
        return value;
    }

    @Override
    public boolean logHuGameBai(PotMessage message) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("log_hu_game_bai");
        Document doc = new Document();
        doc.append("pot_name", message.getPotName());
        doc.append("money_exchange", message.getMoneyExchange());
        doc.append("value", message.getValuePot());
        doc.append("value_pot_system", message.getValuePotSystem());
        doc.append("time_log", message.getCreateTime());
        doc.append("create_time", new Date());
        col.insertOne(doc);
        return true;
    }
}
