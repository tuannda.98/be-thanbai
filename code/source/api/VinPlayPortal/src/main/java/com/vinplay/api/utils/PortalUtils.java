package com.vinplay.api.utils;

import casio.king365.GU;
import casio.king365.core.HCMap;
import casio.king365.util.KingUtil;
import com.hazelcast.core.IMap;
import com.vinplay.api.processors.GetAppConfigProcesscor;
import com.vinplay.dichvuthe.service.RechargeService;
import com.vinplay.dichvuthe.service.impl.RechargeServiceImpl;
import com.vinplay.usercore.service.UserService;
import com.vinplay.usercore.service.impl.GameConfigServiceImpl;
import com.vinplay.usercore.service.impl.SecurityServiceImpl;
import com.vinplay.usercore.service.impl.UserExtraServiceImpl;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.usercore.utils.GameCommon;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.response.LoginResponse;
import com.vinplay.vbee.common.utils.VinPlayUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class PortalUtils {
    private static final Logger logger = Logger.getLogger((String) "api");
    private static UserService userService = new UserServiceImpl();

    public static LoginResponse loginSuccess(UserModel userModel, String appId, String pf, String ip, String uAgent) throws SQLException {

        try {
            IMap<String, UserCacheModel> userMap = HCMap.getUsersMap();
            IMap<String, String> tokenMap = HCMap.getTokenMap();

            final UserCacheModel userCache = userService.forceGetCachedUser(userModel.getNickname());

            if (userService.checkMoneyNegative(userCache) != null) {
                GU.sendOperation("Lỗi khi đăng nhập: tài khoản dang bị âm tiền khi đăng nhập, nickname: "
                        + userCache.getNickname() + ", số tiền vin total: " + userCache.getVinTotal()
                        + ", số tiền vin: " + userCache.getVin());
                return LoginResponse.fail();
            }

            if (userMap.isLocked(userModel.getNickname())) {
                int count = 0;
                while (userMap.isLocked(userModel.getNickname()) || count++ < 10) {
                    Thread.sleep(100);
                }
                if (userMap.isLocked(userModel.getNickname())) {
                    userMap.forceUnlock(userModel.getNickname());
                    // throw  new Exception("login while user is locking...");
                    KingUtil.printLog("login while user is locking...");
                    GU.sendOperation("Lỗi khi đăng nhập: tài khoản dang bị lock cache khi đăng nhập, nickname: " + userModel.getNickname());
                }
            }

            if (pf.equals("android"))
                pf = "ad";
            postLoginProcessingThread(userModel, appId, pf);

            SecurityServiceImpl securityService = new SecurityServiceImpl();
            securityService.saveLoginInfo(
                    userCache.getId(), userCache.getUsername(), userCache.getNickname(), ip, uAgent, 1, pf);

            userService.updateUser(userCache.getNickname(), (userCacheModel -> {
                userCacheModel.setLastActive(new Date());
                userCacheModel.setIp(ip);
                if (userCacheModel.getAccessToken() == null
                        || userCacheModel.getLastActive() == null
                        || VinPlayUtils.sessionTimeout(userCacheModel.getLastActive().getTime())) {
                    userCacheModel.setAccessToken(VinPlayUtils.genAccessToken(userModel.getId()));
                }
            }));

            tokenMap.put(userCache.getAccessToken(), userModel.getNickname(), 24 * 3, TimeUnit.HOURS);

            return LoginResponse.success(VinPlayUtils.genSessionKey(userCache), userCache.getAccessToken(), userModel.getUsername(), userCache.getAvatar());
        } catch (Exception e) {
            GU.sendOperation("Lỗi khi đăng nhập nickname: " + userModel.getNickname() + " lỗi " + e);
            KingUtil.printException("PortalUtil login", e);
        }
        return LoginResponse.fail();
    }

    private static void postLoginProcessingThread(UserModel userModel, String client, String platform) {
        (new Thread() {
            public void run() {
                try {
                    KingUtil.printLog("PortalUtil loginSuccess run in thread...");
                    IMap<String, UserCacheModel> userMap = HCMap.getUsersMap();

                    // Update client/App id cho tai khoan
                    if (client != null && !client.isEmpty() && userModel.getClient() == null) {
                        KingUtil.printLog("set client for user. user client == null, client: " + client);
                        userService.updateClient(userModel, client);
                        //các data nạp / rút của user trước đó cần cập nhật client luôn
                        RechargeService rechargeService = new RechargeServiceImpl();
                        rechargeService.updateUserClient(userModel.getNickname(), userModel.getClient());
                    }

                    if (!platform.equals("") && userModel.getPlatform() == null) {
                        KingUtil.printLog("set platform for user. user platform == null, platform: " + platform);
                        userService.updatePlatform(userModel, platform);
                    }

                    UserExtraServiceImpl userExtraService = new UserExtraServiceImpl();
                    UserCacheModel userCache = userMap.get(userModel.getNickname());
                    userExtraService.cacheUserExtraInfo(userCache, platform);
                    logger.debug("User " + userCache.getNickname() + " platform: " + userCache.getPlatform() + ", client: " + userCache.getClient());
                } catch (Exception e1) {
                    KingUtil.printException("LoginProcessor", e1);
                    GU.sendOperation("Có lỗi khi đăng nhập LoginProcessor, Exception: " + KingUtil.printException(e1));
                }
            }
        }).start();
    }

    public static String getIpAddress(HttpServletRequest request) {
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }
        return ipAddress;
    }

    public static String getUserAgent(HttpServletRequest request) {
        String agent = request.getHeader("USER-AGENT");
        if (agent == null) {
            agent = "";
        }
        return agent;
    }

    public static void loadGameConfig() throws SQLException, JSONException, ParseException {
        GameConfigServiceImpl ser = new GameConfigServiceImpl();
        GetAppConfigProcesscor.configs = ser.getGameConfig();
        GameCommon.init();
    }

    public static List<String> parseStringToList(String urlHelp) {
        ArrayList<String> res = new ArrayList<String>();
        if (urlHelp != null && !urlHelp.isEmpty()) {
            if (urlHelp.contains(",")) {
                String[] arr = urlHelp.trim().split(",");
                res.addAll(Arrays.asList(arr));
            } else {
                res.add(urlHelp);
            }
        }
        return res;
    }

    public static boolean checkCaptcha(String captcha, String captchaId) {
        IMap captchaCache;
        boolean isCaptcha = false;
        if (!(captcha = captcha.trim().toLowerCase()).isEmpty()
                && (captchaCache = HazelcastClientFactory.getInstance().getMap("cacheCaptcha")).containsKey(captchaId)
                && (captchaCache.get(captchaId)).equals(captcha)) {
            captchaCache.remove(captchaId);
            isCaptcha = true;
        }
        return isCaptcha;
    }
}
