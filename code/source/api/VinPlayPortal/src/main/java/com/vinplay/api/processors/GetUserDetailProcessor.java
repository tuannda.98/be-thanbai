package com.vinplay.api.processors;


import bitzero.util.common.business.Debug;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
//import com.vinplay.api.entities.GetUserDetailResponse;
//import com.vinplay.api.entities.UserTransaction;
import com.vinplay.dal.dao.LogMoneyUserDao;
import com.vinplay.dal.dao.impl.LogMoneyUserDaoImpl;
import com.vinplay.dal.service.impl.AgentServiceImpl;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.response.AgentResponse;
import com.vinplay.vbee.common.response.LogUserMoneyResponse;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;

public class GetUserDetailProcessor implements BaseProcessor<HttpServletRequest, String> {
    private static final Logger logger = Logger.getLogger("api");

    public GetUserDetailProcessor() {
    }

    public String execute(Param<HttpServletRequest> param) {
        /*try {
            HttpServletRequest request = (HttpServletRequest)param.get();
            new UserServiceImpl();
            String nickName = request.getParameter("nn");
            GetUserDetailResponse res = new GetUserDetailResponse(500, "error");
            UserServiceImpl userService = new UserServiceImpl();
            if (nickName != null && nickName != "") {
                try {
                    UserModel userModel = userService.getUserByNickName(nickName);
                    if (userModel != null) {
                        HazelcastInstance client = HazelcastClientFactory.getInstance();
                        IMap userMap = client.getMap("users");
                        res.setNickname(nickName);
                        res.setCurrent_balance(userModel.getVin());
                        res.setRegister_time(userModel.getCreateTime().getTime());
                        if (userMap.containsKey(nickName)) {
                            UserCacheModel userCacheModel = (UserCacheModel)userMap.get(nickName);
                            if (userCacheModel != null && userCacheModel.getLastActive() != null) {
                                res.setLast_login_time(userCacheModel.getLastActive().getTime());
                            }
                        }

                        AgentServiceImpl service = new AgentServiceImpl();
                        List<AgentResponse> agents = service.listAgent();
                        ArrayList<String> agentNames = new ArrayList();
                        if (agents != null && agents.size() > 0) {
                            Iterator var13 = agents.iterator();

                            while(var13.hasNext()) {
                                AgentResponse agent = (AgentResponse)var13.next();
                                agentNames.add(agent.nickName);
                            }
                        }

                        LogMoneyUserDao logService = new LogMoneyUserDaoImpl();
                        List<LogUserMoneyResponse> resultGiftCode = logService.searchAllLogMoneyUser(nickName, "GIFTCODE", false);
                        if (resultGiftCode != null && resultGiftCode.size() > 0) {
                            long total_money_giftcode = 0L;
                            total_money_giftcode = (Long)resultGiftCode.stream().map((transx) -> {
                                return transx.moneyExchange;
                            }).reduce(total_money_giftcode, (accumulator, _item) -> {
                                return accumulator + _item;
                            });
                            res.setTotal_gift_code_money(total_money_giftcode);
                        }

                        List<LogUserMoneyResponse> resultTransfer = logService.searchAllLogMoneyUser(nickName, "TRANSFER", false);
                        boolean last_agent_transfer = true;
                        LogUserMoneyResponse last_agency_transaction;
                        long total_agency_transfer;
                        if (resultTransfer != null && resultTransfer.size() > 0) {
                            LogUserMoneyResponse last_agency_transaction = null;
                            last_agency_transaction = null;
                            long total_user_transfer = 0L;
                            total_agency_transfer = 0L;
                            Iterator var23 = resultTransfer.iterator();

                            while(true) {
                                if (!var23.hasNext()) {
                                    res.setTotal_transfer_money(0L - total_user_transfer);
                                    res.setTotal_agency_transfer_money(0L - total_agency_transfer);
                                    UserTransaction trans;
                                    String name;
                                    if (last_agency_transaction != null) {
                                        trans = new UserTransaction();
                                        trans.setMoney(0L - last_agency_transaction.moneyExchange);
                                        trans.setType("TRANSFER");
                                        trans.setTime(last_agency_transaction.createdTime);
                                        name = last_agency_transaction.description.trim();
                                        name = name.substring(11, name.indexOf(":"));
                                        trans.setName(name);
                                        res.setLast_agency_transaction(trans);
                                    }

                                    if (last_agency_transaction != null) {
                                        trans = new UserTransaction();
                                        trans.setMoney(0L - last_agency_transaction.moneyExchange);
                                        trans.setTime(last_agency_transaction.createdTime);
                                        name = last_agency_transaction.description.trim();
                                        name = name.substring(11, name.indexOf(":"));
                                        trans.setName(name);
                                        trans.setType("TRANSFER");
                                        res.setLast_transfer_transaction(trans);
                                    }
                                    break;
                                }

                                LogUserMoneyResponse trans = (LogUserMoneyResponse)var23.next();
                                boolean matchAgent = false;
                                Iterator var26 = agentNames.iterator();

                                while(var26.hasNext()) {
                                    String s = (String)var26.next();
                                    if (trans.description.contains(s)) {
                                        matchAgent = true;
                                    }
                                }

                                if (matchAgent) {
                                    total_agency_transfer += trans.moneyExchange;
                                    if (last_agency_transaction == null) {
                                        last_agency_transaction = trans;
                                    }
                                } else {
                                    total_user_transfer += trans.moneyExchange;
                                    if (last_agency_transaction == null) {
                                        last_agency_transaction = trans;
                                    }
                                }
                            }
                        }

                        List<LogUserMoneyResponse> resulReceive = logService.searchAllLogMoneyUser(nickName, "RECEIVE", false);
                        if (resulReceive != null && resulReceive.size() > 0) {
                            last_agency_transaction = null;
                            LogUserMoneyResponse last_user_receive_transaction = null;
                            long total_user_receive = 0L;
                            long total_agency_receive = 0L;
                            Iterator var48 = resulReceive.iterator();

                            while(true) {
                                if (!var48.hasNext()) {
                                    res.setTotal_receive_money(total_user_receive);
                                    res.setTotal_agency_receive_money(total_agency_receive);
                                    UserTransaction trans;
                                    String name;
                                    if (last_agency_transaction != null) {
                                        trans = new UserTransaction();
                                        trans.setMoney(last_agency_transaction.moneyExchange);
                                        trans.setTime(last_agency_transaction.createdTime);
                                        if (last_agent_transfer) {
                                            name = last_user_receive_transaction.description.trim();
                                            name = name.substring(8, name.indexOf(":"));
                                            trans.setName(name);
                                            trans.setType("RECEIVE");
                                        } else {
                                            name = last_agency_transaction.description.trim();
                                            name = name.substring(8, name.indexOf(":"));
                                            trans.setName(name);
                                            trans.setType("RECEIVE");
                                        }

                                        res.setLast_agency_transaction(trans);
                                    }

                                    if (last_user_receive_transaction != null) {
                                        trans = new UserTransaction();
                                        trans.setMoney(last_user_receive_transaction.moneyExchange);
                                        trans.setTime(last_user_receive_transaction.createdTime);
                                        name = last_user_receive_transaction.description.trim();
                                        name = name.substring(8, name.indexOf(":"));
                                        trans.setName(name);
                                        trans.setType("RECEIVE");
                                        res.setLast_receive_transaction(trans);
                                    }
                                    break;
                                }

                                LogUserMoneyResponse trans = (LogUserMoneyResponse)var48.next();
                                boolean matchAgent = false;
                                Iterator var53 = agentNames.iterator();

                                while(var53.hasNext()) {
                                    String s = (String)var53.next();
                                    if (trans.description.contains(s)) {
                                        matchAgent = true;
                                    }
                                }

                                if (matchAgent) {
                                    total_agency_receive += trans.moneyExchange;
                                    if (last_agency_transaction == null) {
                                        last_agency_transaction = trans;
                                        last_agent_transfer = false;
                                    } else if (trans.createdTime > last_agency_transaction.createdTime) {
                                        last_agency_transaction = trans;
                                        last_agent_transfer = false;
                                    }
                                } else {
                                    total_user_receive += trans.moneyExchange;
                                    if (last_user_receive_transaction == null) {
                                        last_user_receive_transaction = trans;
                                    }
                                }
                            }
                        }

                        long total_recharge_card_money = 0L;
                        List<LogUserMoneyResponse> resultCard = logService.searchAllLogMoneyUser(userModel.getNickname(), "CARD", false);
                        if (resultCard != null && resultCard.size() > 0) {
                            total_recharge_card_money = (Long)resultCard.stream().map((transx) -> {
                                return transx.moneyExchange;
                            }).reduce(total_recharge_card_money, (accumulator, _item) -> {
                                return accumulator + _item;
                            });
                        }

                        res.setTotal_card_charge_money(total_recharge_card_money);
                        res.setTotal_card_exchange_money(0L);
                        res.setTotal_play_money(0L - logService.getTotalBetWin(nickName, "BET", (String)null));
                        total_agency_transfer = total_recharge_card_money + res.getTotal_agency_receive_money() + res.getTotal_receive_money() + res.getTotal_gift_code_money();
                        res.setTotal_win_money(logService.getTotalBetWin(nickName, "WIN", (String)null));
                        long quota = 0L;
                        quota += total_recharge_card_money;
                        quota += res.getTotal_agency_receive_money();
                        quota += res.getTotal_receive_money() * 4L;
                        quota += res.getTotal_gift_code_money() * 4L;
                        quota += res.getTotal_transfer_money();
                        quota += res.getTotal_agency_transfer_money();
                        Debug.trace("Quota:" + quota);
                        long total_bet_money = 0L - logService.getTotalBetWin(nickName, "BET", (String)null);
                        long total_bet_banca_money = 0L - logService.getTotalBetWin(nickName, "BET", "HamCaMap");
                        long total_bet_slot_money = 0L - logService.getTotalBetWin(nickName, "BET", "SLOT");
                        long finalQuota = total_bet_money - total_bet_banca_money * 9L / 10L - total_bet_slot_money * 9L / 10L;
                        Debug.trace("Final quota :" + finalQuota);
                        quota = finalQuota - quota;
                        res.setQuote(quota);
                        res.setCode(200);
                        res.setMessage("success");
                    } else {
                        res.setCode(404);
                        res.setMessage("nickname not found");
                    }
                } catch (SQLException var33) {
                    res.setCode(500);
                    res.setMessage("error");
                }
            } else {
                res.setCode(400);
                res.setMessage("bad request");
            }

            return res.toJson();
        } catch (Exception var34) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            var34.printStackTrace(pw);
            String sStackTrace = sw.toString();
            return var34.getMessage() + "\n" + sStackTrace;
        }

         */
        return "";
    }
}
