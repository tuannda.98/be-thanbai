/*
 * Decompiled with CFR 0.144.
 * 
 * Could not load the following classes:
 *  com.vinplay.dal.entities.taixiu.ResultTaiXiu
 *  com.vinplay.dal.entities.taixiu.TransactionTaiXiuDetail
 *  com.vinplay.dal.service.impl.TaiXiuServiceImpl
 *  com.vinplay.vbee.common.cp.BaseProcessor
 *  com.vinplay.vbee.common.cp.Param
 *  javax.servlet.http.HttpServletRequest
 */
package com.vinplay.api.processors.minigame;

import casio.king365.util.KingUtil;
import com.vinplay.api.processors.minigame.response.ChiTietPhienTXResponse;
import com.vinplay.dal.entities.taixiu.ResultTaiXiu;
import com.vinplay.dal.entities.taixiu.TransactionTaiXiuDetail;
import com.vinplay.dal.service.OverUnderService;
import com.vinplay.dal.service.TaiXiuService;
import com.vinplay.dal.service.impl.OverUnderServiceImpl;
import com.vinplay.dal.service.impl.TaiXiuServiceImpl;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import java.io.PrintStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

public class ChiTietPhienTXProcessor
implements BaseProcessor<HttpServletRequest, String> {
    public String execute(Param<HttpServletRequest> param) {
        ChiTietPhienTXResponse response = new ChiTietPhienTXResponse(false, "1001");
        HttpServletRequest request = (HttpServletRequest)param.get();
        long referenceId = Long.parseLong(request.getParameter("rid"));
        int moneyType = Integer.parseInt(request.getParameter("mt"));
        String isou = request.getParameter("isou") != null ? request.getParameter("isou") : "false";
        Boolean isOverunder = Boolean.parseBoolean(isou);
        TaiXiuService taiXiuService = new TaiXiuServiceImpl();
        OverUnderService overUnderService = new OverUnderServiceImpl();
        List<TransactionTaiXiuDetail> transaction = new ArrayList<>();
        try {
            if(isOverunder){
                transaction = overUnderService.getChiTietPhienTX(referenceId, moneyType);
                response.setTransactions(transaction);
                ResultTaiXiu resultTX = overUnderService.getKetQuaPhien(referenceId, moneyType);
                response.setResultTX(resultTX);
            } else {
                transaction = taiXiuService.getChiTietPhienTX(referenceId, moneyType);
                response.setTransactions(transaction);
                ResultTaiXiu resultTX = taiXiuService.getKetQuaPhien(referenceId, moneyType);
                response.setResultTX(resultTX);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            KingUtil.printException("ChiTietPhienTXProcessor", e);
        }

        response.setSuccess(true);
        response.setErrorCode("0");
        return response.toJson();
    }
}

