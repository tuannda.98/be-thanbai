package com.vinplay.api.server;

import bitzero.server.Main;
import casio.king365.service.ReloadBankInfoService;
import com.vinplay.api.processors.vippoint.TopVippoint;
import com.vinplay.api.utils.PortalUtils;
import com.vinplay.dal.service.LogPortalService;
import com.vinplay.dal.service.impl.LogPortalServiceImpl;
import com.vinplay.dal.utils.PotUtils;
import com.vinplay.usercore.utils.PartnerConfig;
import com.vinplay.vbee.common.cp.BaseController;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.hazelcast.HazelcastLoader;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.rmq.RMQApi;
import com.vinplay.vbee.common.utils.UserValidaton;
import misa.TeleLauncher;
import net.bull.javamelody.Parameter;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.eclipse.jetty.annotations.AnnotationConfiguration;
import org.eclipse.jetty.server.*;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.eclipse.jetty.webapp.Configuration;
import org.eclipse.jetty.webapp.WebAppContext;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.hazelcast.HazelcastAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.servlet.DispatcherType;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, HazelcastAutoConfiguration.class})
@ComponentScan({"game", "bitzero", "casio", "com.vinplay", "extension"})
public class VinplayPortalMain {
    private static final Logger logger = Logger.getLogger((String) "api");
    private static final Logger blackListIpLogger = Logger.getLogger((String) "BlackListIpLogger");
    private static final String LOG_PROPERTIES_FILE = "config/log4j.properties";
    private static LogPortalService service = new LogPortalServiceImpl();
    private static String API_PORT = "8081";
    private static String SSL_PORT = "8443";
    private static BaseController<HttpServletRequest, String> controller;

    private static void initializeLogger() {
        Properties logProperties = new Properties();
        try {
            File file = new File(LOG_PROPERTIES_FILE);
            logProperties.load(new FileInputStream(file));
            PropertyConfigurator.configure((Properties) logProperties);
            logger.info((Object) "Logging initialized.");
        } catch (IOException e) {
            throw new RuntimeException("Unable to load logging property config/log4j.properties");
        }
    }

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
        try {
            VinplayPortalMain.initializeLogger();
            logger.debug((Object) "STARTING PORTAL API SERVER .... !!!!");
            VinplayPortalMain.loadCommands();
            RMQApi.start((String) "config/rmq.properties");
            // Config
            PartnerConfig.ReadConfig();
            HazelcastLoader.start();
            MongoDBConnectionFactory.init();
            UserValidaton.init();
            PortalUtils.loadGameConfig();
            PotUtils.init();
            TopVippoint.init();

//            //===========
//            // EXTENDED
//            logger.debug("Start TeleLauncher..");
//            TeleLauncher.main(args);
            // =========
            // Start schedule
            startScheduleTask();
            //===========

            int port = Integer.parseInt(API_PORT);
            int sslPort = Integer.parseInt(SSL_PORT);
            Server server = new Server();
            ServerConnector connector = new ServerConnector(server);
            connector.setPort(port);
            connector.setIdleTimeout(30000L);

            HttpConfiguration https = new HttpConfiguration();
            https.addCustomizer(new SecureRequestCustomizer());
            SslContextFactory sslContextFactory = new SslContextFactory();
            sslContextFactory.setKeyStorePath("config/apivinplay.jks");
            sslContextFactory.setKeyStorePassword("vinplay@123");
            sslContextFactory.setKeyManagerPassword("vinplay@123");
            ServerConnector sslConnector = new ServerConnector(server, new ConnectionFactory[]{
                    new SslConnectionFactory(sslContextFactory, "http/1.1"), new HttpConnectionFactory(https)});
            sslConnector.setPort(sslPort);

            WebAppContext webAppContext = new WebAppContext();
            webAppContext.setConfigurations(new Configuration[]{
                    new AnnotationConfiguration()});
            webAppContext.setParentLoaderPriority(true);
            webAppContext.addFilter(CorsFilter.class, "/*", EnumSet.of(DispatcherType.REQUEST));
            final net.bull.javamelody.MonitoringFilter monitoringFilter = new net.bull.javamelody.MonitoringFilter();
            monitoringFilter.setApplicationType("Standalone");
            final FilterHolder filterHolder = new FilterHolder(monitoringFilter);
            final Map<Parameter, String> parameters = new HashMap<>();
// you can add basic auth:
            parameters.put(Parameter.AUTHORIZED_USERS, "vingame:p1");
// you can change the default storage directory:
//            parameters.put(Parameter.STORAGE_DIRECTORY, "/tmp/javamelody");
// you can enable hotspots sampling with a period of 1 second:
            parameters.put(Parameter.SAMPLING_SECONDS, "1.0");

// set the path of the reports:
            parameters.put(Parameter.MONITORING_PATH, "/monitor");
// start the embedded http server with javamelody
            if (parameters != null) {
                for (final Map.Entry<Parameter, String> entry : parameters.entrySet()) {
                    final Parameter parameter = entry.getKey();
                    final String value = entry.getValue();
                    filterHolder.setInitParameter(parameter.getCode(), value);
                }
            }
                webAppContext.addFilter(filterHolder, "/*",
                        EnumSet.of(DispatcherType.INCLUDE, DispatcherType.REQUEST));
            webAppContext.addServlet(JeetyServlet.class, "/api");
//            webAppContext.addServlet(ReportServlet.class, "/monitoring");

            server.setHandler(webAppContext);
            server.setConnectors(new Connector[]{connector, sslConnector});
            server.start();
            logger.info((Object) "PORTAL API SERVER Started ...!!!");
//            startJavaMelodyEmbedded();
            server.join();

        } catch (Exception e) {
            logger.info((Object) ("PORTAL API SERVER Start error: " + e.getMessage()));
            e.printStackTrace();
        }
    }

    private static void startScheduleTask() {
        // Start Schedule check bank cashout request
        ScheduledExecutorService ses = Executors.newScheduledThreadPool(1);

        // Start schedule update bank list, bank account list, momo shippers list
        // ses.scheduleAtFixedRate(new ReloadBankInfoService(), 30, 180, TimeUnit.SECONDS);

    }

    private static void loadCommands() throws Exception {
        File file = new File("config/api_portal.xml");
        DocumentBuilderFactory dFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(file);
        doc.getDocumentElement().normalize();
        NodeList nodeList = doc.getElementsByTagName("portal");
        Element el = (Element) nodeList.item(0);
        API_PORT = el.getElementsByTagName("port").item(0).getTextContent();
        SSL_PORT = el.getElementsByTagName("ssl_port").item(0).getTextContent();
        Element cmds = (Element) el.getElementsByTagName("commands").item(0);
        NodeList cmdList = cmds.getElementsByTagName("command");
        HashMap<Integer, String> commandsMap = new HashMap<Integer, String>();
        for (int i = 0; i < cmdList.getLength(); ++i) {
            Element eCmd = (Element) cmdList.item(i);
            Integer id = Integer.parseInt(eCmd.getElementsByTagName("id").item(0).getTextContent());
            String path = eCmd.getElementsByTagName("path").item(0).getTextContent();
            logger.debug((Object) (id + " <-> " + path));
            System.out.println(id + " <-> " + path);
            commandsMap.put(id, path);
        }
        controller = new BaseController();
        controller.initCommands(commandsMap);
    }

    public static class JeetyServlet
            extends HttpServlet {
        private static final long serialVersionUID = 1L;

        protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            this.onExecute(request, response);
        }

        protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            this.onExecute(request, response);
        }

        private void onExecute(HttpServletRequest request, HttpServletResponse response) throws IOException {
            response.setContentType("text/html");
            response.setCharacterEncoding("UTF-8");
            response.setStatus(200);
            Map requestMap = request.getParameterMap();
            String remoteAddr = request.getRemoteAddr();
            logger.info((Object) ("IP:" + remoteAddr));
            if (requestMap.containsKey("c")) {
                String command = request.getParameter("c");
                if (command == null || command.equalsIgnoreCase("")) {
                    blackListIpLogger.debug((Object) remoteAddr);
                    return;
                }
                Param param = new Param();
                param.set((Object) request);
                logger.debug((Object) ("command: " + command));
                try {
                    response.getWriter().println((String) controller.processCommand(Integer.valueOf(Integer.parseInt(command)), param));
                    service.log(command);
                } catch (Exception e1) {
                    e1.printStackTrace();
                    System.out.println(e1);
                    response.getWriter().println("EXCEPTION: " + e1.getMessage());
                }
            } else {
                blackListIpLogger.debug((Object) remoteAddr);
                response.getWriter().println("NO COMMANDS PARAMETERS");
                service.log("NO_CMD");
            }
        }
    }

}
