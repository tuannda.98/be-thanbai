package com.vinplay.api.processors.minigame;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import javax.servlet.http.HttpServletRequest;

public class SetTxBankProcessor implements BaseProcessor<HttpServletRequest, String> {
    public SetTxBankProcessor() {
    }

    public String execute(Param<HttpServletRequest> param) {
        HttpServletRequest request = (HttpServletRequest)param.get();
        byte type = Byte.parseByte(request.getParameter("type"));
        long amount = Long.parseLong(request.getParameter("amount"));
        byte moneyType = 1;

        try {
            String keyHazel = type == 1 ? "txBank" : "txBank2";
            HazelcastInstance client = HazelcastClientFactory.getInstance();
            IMap bankMap = client.getMap(keyHazel);
            String key = keyHazel + ":" + moneyType;
            bankMap.put(key, amount);
            return "" + amount;
        } catch (Exception var11) {
            return var11.getMessage();
        }
    }
}
