package com.vinplay.api.entities;

public class Provider {
    public String code;
    public String name;

    public Provider(String code, String name) {
        this.code = code;
        this.name = name;
    }
}
