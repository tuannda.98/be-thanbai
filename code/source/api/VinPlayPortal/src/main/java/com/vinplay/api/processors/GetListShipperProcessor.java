package com.vinplay.api.processors;


import com.google.gson.Gson;
import com.vinplay.api.utils.PortalUtils;
import com.vinplay.usercore.utils.PartnerConfig;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
//import com.vinplay.zoan.M32MomoResponse;
//import com.vinplay.zoan.ZoanClient;
//import com.vinplay.zoan.ZoanMomoResponse;
//import com.vinplay.zoan.M32MomoResponse.M32Account;
//import com.vinplay.zoan.ZoanMomoResponse.ZoanMomoAccount;
//import com.vinplay.zoan.ZoanMomoResponse.ZoanMomoData;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

public class GetListShipperProcessor implements BaseProcessor<HttpServletRequest, String> {
    private static final Logger logger = Logger.getLogger("api");

    public GetListShipperProcessor() {
    }

    public String execute(Param<HttpServletRequest> param) {
        /*ZoanMomoResponse response = new ZoanMomoResponse(-1, "failed");

        try {
            logger.debug(PartnerConfig.CongMomo);
            if (PartnerConfig.CongMomo.equals("ZOANMOMO")) {
                response = ZoanClient.GetShippers();
            } else {
                if (PartnerConfig.CongMomo.equals("M32")) {
                    M32MomoResponse m32MomoResponse;
                    if (PortalUtils.response != null) {
                        if (System.currentTimeMillis() > PortalUtils.response.getCurrentTime() + 300000L) {
                            return this.parseM32Response(PortalUtils.response).toJson();
                        }

                        m32MomoResponse = ZoanClient.GetShippersM32();
                        PortalUtils.response = m32MomoResponse;
                        return this.parseM32Response(m32MomoResponse).toJson();
                    }

                    m32MomoResponse = ZoanClient.GetShippersM32();
                    PortalUtils.response = m32MomoResponse;
                    return this.parseM32Response(m32MomoResponse).toJson();
                }

                response = ZoanClient.GetShippers();
            }
        } catch (Exception var7) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            var7.printStackTrace(pw);
            String sStackTrace = sw.toString();
            logger.debug(var7.getMessage());
            logger.debug(sStackTrace);
        }

        return response.toJson();

         */
        return "";
    }

    /*ZoanMomoResponse parseM32Response(M32MomoResponse input) {
        ZoanMomoResponse response = new ZoanMomoResponse(200, "success");
        response.getClass();
        ZoanMomoData data = new ZoanMomoData(response);
        List<ZoanMomoAccount> accounts = new ArrayList();
        if (input != null && input.getDestinationInfo() != null) {
            try {
                String decrypted = new String(Base64.decodeBase64(input.getDestinationInfo()), "UTF-8");
                Gson gson = new Gson();
                M32Account result = (M32Account)gson.fromJson(decrypted, M32Account.class);
                if (result != null) {
                    response.getClass();
                    ZoanMomoAccount account = new ZoanMomoAccount(response);
                    account.setIsMaxquota(0);
                    account.setName(result.getName());
                    account.setPhone(result.getPhone());
                    accounts.add(account);
                    data.setAccounts(accounts);
                    data.setPartnerName("M32");
                    response.setData(data);
                    return response;
                }
            } catch (Exception var9) {
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                var9.printStackTrace(pw);
                String sStackTrace = sw.toString();
                logger.debug(var9.getMessage());
                logger.debug(sStackTrace);
                return null;
            }
        }

        return null;
    }

     */
}
