package com.vinplay.api.processors;


import com.vinplay.api.entities.RechargeBankResponse;
import com.vinplay.api.entities.RechargeMomoResponse;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;

public class GetRechargeInfoProcessor implements BaseProcessor<HttpServletRequest, String> {
    private static final Logger logger = Logger.getLogger("api");

    public GetRechargeInfoProcessor() {
    }

    public String execute(Param<HttpServletRequest> param) {
        try {
            HttpServletRequest request = (HttpServletRequest)param.get();
            String type = request.getParameter("t");
            if ("momo".equals(type.toLowerCase())) {
                RechargeMomoResponse res = new RechargeMomoResponse();
                res.setName("Nguyen Thi Linh");
                res.setPhone("0848026647");
                return res.toJson();
            } else if ("bank".equals(type.toLowerCase())) {
                RechargeBankResponse res = new RechargeBankResponse();
                res.setAccount_name("Nguyen Thi Linh");
                res.setAccount_number("109871158224");
                res.setBank_branch("");
                res.setBank_name("Vietinbank");
                res.setMethod("Internet banking");
                return res.toJson();
            } else {
                return "{\"code\":500,\"message\":\"error\"}";
            }
        } catch (Exception var6) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            var6.printStackTrace(pw);
            String sStackTrace = sw.toString();
            return var6.getMessage() + "\n" + sStackTrace;
        }
    }
}
