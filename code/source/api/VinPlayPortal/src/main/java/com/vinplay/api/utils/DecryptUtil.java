package com.vinplay.api.utils;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class DecryptUtil {
    public static String encode(String key, String data) throws Exception {
        Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
        SecretKeySpec secret_key = new SecretKeySpec(key.getBytes("UTF-8"), "HmacSHA256");
        sha256_HMAC.init(secret_key);
        return Base64.encodeBase64(sha256_HMAC.doFinal(data.getBytes("UTF-8"))).toString();
    }

    public static boolean verifyFacebookInstance(String key, String data){
        return true;
    }

    public static void main(String[] args) throws Exception {
        String key = "afbf7d515552e5ea5a9b0f2ccc16cce9";
        String data = "pCWAoxU-IKhyx29DmGKUIoTSllgEO9VRCE-9tnmlDuk.eyJhbGdvcml0aG0iOiJITUFDLVNIQTI1NiIsImlzc3VlZF9hdCI6MTY1MDkwNzg5NywicGxheWVyX2lkIjoiNTI0NjQzMjQyODczNTE5MCIsInJlcXVlc3RfcGF5bG9hZCI6bnVsbH0";
        String[] datas = data.split("\\.");
        String firstpart = datas[0];
        String signature = Base64.encodeBase64(firstpart.getBytes()).toString();
        String dataHash = DecryptUtil.encode(key,datas[1]);
        System.out.println(signature);
        System.out.println(dataHash);
        DecryptUtil.encode(key,data);
    }
}
