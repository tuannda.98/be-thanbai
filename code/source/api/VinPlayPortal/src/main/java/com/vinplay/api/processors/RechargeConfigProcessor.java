package com.vinplay.api.processors;


import com.vinplay.api.entities.Provider;
import com.vinplay.api.entities.RechargeConfigResponse;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;

public class RechargeConfigProcessor implements BaseProcessor<HttpServletRequest, String> {
    private static final Logger logger = Logger.getLogger("api");

    public RechargeConfigProcessor() {
    }

    public String execute(Param<HttpServletRequest> param) {
        try {
            HttpServletRequest request = (HttpServletRequest)param.get();
            RechargeConfigResponse response = new RechargeConfigResponse();
            response.code = 0;
            response.providers = new ArrayList();
            response.providers.add(new Provider("VTT", "Viettel"));
            response.providers.add(new Provider("VNP", "Vinaphone"));
            response.providers.add(new Provider("VMS", "Mobifone"));
            response.amounts = new int[]{10, 20, 30, 50, 100, 200, 300, 500};
            response.rate = 1.0D;
            return response.toJson();
        } catch (Exception var6) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            var6.printStackTrace(pw);
            String sStackTrace = sw.toString();
            logger.info(var6.getMessage());
            logger.info(sStackTrace);
            return "{\"code\":500,\"message\":\"error\"}";
        }
    }
}
