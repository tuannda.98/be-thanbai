package com.vinplay.api.processors.security;


import com.hazelcast.core.IMap;
import com.vinplay.usercore.service.SecurityService;
import com.vinplay.usercore.service.impl.SecurityServiceImpl;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.response.BaseResponseModel;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;

public class ChangePasswordProcessor implements BaseProcessor<HttpServletRequest, String> {
    private static final Logger logger = Logger.getLogger("api");

    public ChangePasswordProcessor() {
    }

    public String execute(Param<HttpServletRequest> param) {
        HttpServletRequest request = (HttpServletRequest)param.get();
        BaseResponseModel res = BaseResponseModel.failed(false, "1001");
        String username = request.getParameter("un");
        String token = request.getParameter("t");
        String oldPass = request.getParameter("op");
        String newPass = request.getParameter("np");
        if (username != null && oldPass != null && newPass != null) {
            try {
                UserServiceImpl userService = new UserServiceImpl();
                UserModel userModel = userService.getUserByUserName(username);
                if (userModel != null) {
                    if (userModel.getNickname() != null && !userModel.getNickname().isEmpty()) {
                        IMap userMap = HazelcastClientFactory.getInstance().getMap("users");
                        if (!userMap.containsKey(userModel.getNickname())) {
                            return res.toJson();
                        }

                        UserCacheModel userCache = (UserCacheModel)userMap.get(userModel.getNickname());
                        if (userCache != null && userCache.getAccessToken().equals(token)) {
                            SecurityService securityService = new SecurityServiceImpl();
                            byte result = securityService.changePassword(userModel.getNickname(), oldPass, newPass, false);
                            res.setErrorCode(result + "");
                            if (result == 0) {
                                res.setSuccess(true);
                            }
                        }
                    } else {
                        res.setErrorCode("2001");
                    }
                } else {
                    res.setErrorCode("1005");
                }
            } catch (Exception var14) {
                logger.debug(var14);
            }
        }

        return res.toJson();
    }
}
