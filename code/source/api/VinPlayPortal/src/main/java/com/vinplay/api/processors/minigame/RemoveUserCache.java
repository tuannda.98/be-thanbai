package com.vinplay.api.processors.minigame;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.models.UserModel;
import javax.servlet.http.HttpServletRequest;

public class RemoveUserCache implements BaseProcessor<HttpServletRequest, String> {
    public RemoveUserCache() {
    }

    public String execute(Param<HttpServletRequest> param) {
        try {
            HttpServletRequest request = (HttpServletRequest)param.get();
            String data = request.getParameter("nickname");
            String[] nicknames = data.split(",");
            HazelcastInstance client = HazelcastClientFactory.getInstance();
            IMap<String, UserModel> userMap = client.getMap("users");

            for(int k = 0; k < nicknames.length; ++k) {
                String nickname = nicknames[k];
                userMap.lock(nickname);
                UserModel removed = (UserModel)userMap.remove(nickname);
                userMap.unlock(nickname);
            }

            return "ok";
        } catch (Exception var10) {
            return var10.getMessage();
        }
    }
}
