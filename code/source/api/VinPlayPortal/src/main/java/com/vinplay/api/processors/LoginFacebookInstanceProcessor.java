package com.vinplay.api.processors;

import com.hazelcast.core.IMap;
import com.vinplay.api.utils.DecryptUtil;
import com.vinplay.api.utils.PortalUtils;
import com.vinplay.api.utils.SocialUtils;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.usercore.utils.GameCommon;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.enums.SocialEnum;
import com.vinplay.vbee.common.enums.StatusGames;
import com.vinplay.vbee.common.exceptions.ValidateException;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.models.SocialModel;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.response.LoginResponse;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;
import java.util.Objects;

@Slf4j
public class LoginFacebookInstanceProcessor implements BaseProcessor<HttpServletRequest, String> {

    public String execute(final Param<HttpServletRequest> param) throws ValidateException {
        final HttpServletRequest request = param.get();
        final String playerId = request.getParameter("pi");
        final String signature = request.getParameter("sign");
        final String social = request.getParameter("s");
        final String appId = request.getParameter("app_id") != null ? request.getParameter("app_id") : "";
        final String pf = request.getParameter("pf") != null ? request.getParameter("pf") : "";
        final SocialEnum socialEnum = SocialEnum.parseByType(social);
        if (!socialEnum.isFacebook() || Objects.isNull(playerId) || Objects.isNull(signature) || Objects.isNull(appId)) {
            throw new ValidateException("not facebook login");
        }
        LoginResponse res = new LoginResponse(false, "1001");
        try {
            final int statusGame = GameCommon.getValueInt("STATUS_GAME");
            final StatusGames statusGameEnum = StatusGames.getById(statusGame);
            if (statusGameEnum.isMaintain() || statusGameEnum.isSandbox()) {
                return LoginResponse.failed("1114").toJson();
            }
            final String cache = "cacheFacebook";
            final IMap<String, SocialModel> socialMap = HazelcastClientFactory.getInstance().getMap(cache);
            if (!DecryptUtil.verifyFacebookInstance(signature, appId)) {
                throw new ValidateException("cant verify facebook");
            }
            final UserServiceImpl userService = new UserServiceImpl();
            UserModel userModel = userService.getUserBySocialId(playerId, social);
            if (Objects.isNull(userModel) && userService.insertUserBySocial(playerId, social)) {
                userModel = userService.getUserBySocialId(playerId, social);
            }
            if (Objects.isNull(userModel) || userModel.isBanLogin()) {
                res.setErrorCode("1109");
                return res.toJson();
            }
            SocialUtils.socialSuccess(socialMap, playerId, signature);
            res = PortalUtils.loginSuccess(userModel, appId, pf, PortalUtils.getIpAddress(request), PortalUtils.getUserAgent(request));
        } catch (final Exception e) {
            log.error("[LoginFacebookInstanceProcessor] error: ", e);
        }
        return res.toJson();
    }

}

