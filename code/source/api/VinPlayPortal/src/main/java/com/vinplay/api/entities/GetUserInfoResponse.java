package com.vinplay.api.entities;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class GetUserInfoResponse {
    private int code;
    private String message;
    private String nickname;
    private String mobile;
    private String identification;
    private String email;
    private long vip_point;
    private long current_balance;

    public GetUserInfoResponse(int _code, String _message) {
        this.code = _code;
        this.message = _message;
    }

    public String toJson() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException var2) {
            return "{\"code\":500,\"message\":\"error\"}";
        }
    }

    public int getCode() {
        return this.code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNickname() {
        return this.nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getIdentification() {
        return this.identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getVip_point() {
        return this.vip_point;
    }

    public void setVip_point(long vip_point) {
        this.vip_point = vip_point;
    }

    public long getCurrent_balance() {
        return this.current_balance;
    }

    public void setCurrent_balance(long current_balance) {
        this.current_balance = current_balance;
    }
}