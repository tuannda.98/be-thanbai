package com.vinplay.api.processors;


import com.hazelcast.core.IMap;
import com.vinplay.api.entities.QuotaResponse;
import com.vinplay.usercore.entities.TransferMoneyResponse;
import com.vinplay.usercore.service.impl.OtpServiceImpl;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.usercore.utils.PartnerConfig;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.TimeZone;

public class TransferMoneyProcessor implements BaseProcessor<HttpServletRequest, String> {
    private static final Logger logger = Logger.getLogger("api");

    public TransferMoneyProcessor() {
    }

    public String execute(Param<HttpServletRequest> param) {
        String receiver;
        try {
            HttpServletRequest request = (HttpServletRequest) param.get();
            String token = request.getParameter("t");
            String userName = request.getParameter("un");
            receiver = request.getParameter("r");
            String money = request.getParameter("m");
            String desc = request.getParameter("d");
            String otp = request.getParameter("otp");
            logger.debug("TransferMoneyProcessor: " + token + ":" + userName + ":" + receiver + ":" + money + ":" + desc + ":" + otp);
            TransferMoneyResponse res = new TransferMoneyResponse((byte) -12, 0L, 0L);
            UserServiceImpl userService = new UserServiceImpl();
            if (userName != null && !"".equals(userName)) {
                try {
                    UserModel userModel = userService.getUserByUserName(userName);
                    IMap userMap = HazelcastClientFactory.getInstance().getMap("users");
                    if (!userMap.containsKey(userModel.getNickname())) {
                        return res.toJson();
                    } else {
                        UserCacheModel userCache = (UserCacheModel) userMap.get(userModel.getNickname());
                        if (userCache != null) {
                            if (!userCache.getAccessToken().equals(token)) {
                                res.setCode((byte) -112);
                                return res.toJson();
                            } else if (userService.getUserByNickName(receiver) == null) {
                                res.setCode((byte) -108);
                                return res.toJson();
                            } else if (userModel.getNickname().toLowerCase().equals(receiver.toLowerCase())) {
                                res.setCode((byte) -108);
                                return res.toJson();
                            } else {
                                String serverName;
                                long lMoney;
                                if (userModel.getDaily() == 0) {
                                    QuotaResponse checkQuota = this.CheckQuota(userModel.getNickname(), false);
                                    if ("creator01".equals(userName)) {
                                        checkQuota.setCode(0);
                                    }

                                    logger.debug("TransferMoneyProcessor chuyen tien tu user: Quota resultt:" + checkQuota);
                                    if (checkQuota.getCode() != 0) {
                                        res = new TransferMoneyResponse((byte) 22, 0L, 0L);
                                        return res.toJson();
                                    } else if (otp != null && !"".equals(otp)) {
                                        OtpServiceImpl otpSer = new OtpServiceImpl();
                                        int code = otpSer.checkOtp(otp, userModel.getNickname(), "0", (String) null);
                                        if (code != 0) {
                                            return "{\"code\":222,\"message\":\"invalid otp\"}";
                                        } else {
                                            res = userService.transferMoney(userModel.getNickname(), receiver, Long.parseLong(money), desc, false);
                                            if (res.getCode() == 0) {
                                                String sender;
                                                try {
                                                    HttpClient client = HttpClientBuilder.create().build();
                                                    String url = "";
                                                    if ("XXENG".equals(PartnerConfig.Client)) {
                                                        url = PartnerConfig.HostBot;
                                                    } else if ("R68".equals(PartnerConfig.Client)) {
                                                        url = PartnerConfig.HostBot;
                                                    } else {
                                                        url = PartnerConfig.HostBot;
                                                    }

                                                    HttpPost httpPost = new HttpPost(url + "/rpadmin/ozawasecret1243/3");
                                                    httpPost.addHeader("Content-Type", "application/json");
                                                    logger.debug("TransferMoneyProcessor chuyen tien tu user:  url:" + url);
                                                    JSONObject obj = new JSONObject();
                                                    serverName = userModel.getNickname();
                                                    sender = "xxeng";
                                                    if (userModel.getClient() != null && !userModel.getClient().equals("")) {
                                                        if (userModel.getClient().equals("M")) {
                                                            sender = "manVip";
                                                        } else if (userModel.getClient().equals("R")) {
                                                            sender = "r99";
                                                        } else if (userModel.getClient().equals("V")) {
                                                            sender = "Vip52";
                                                        }
                                                    }

                                                    obj.put("sender_nick_name", serverName);
                                                    obj.put("receiver_nick_name", receiver);
                                                    obj.put("money", Long.parseLong(money));
                                                    obj.put("description", desc);
                                                    obj.put("serverName", sender);
                                                    long date = System.currentTimeMillis();
                                                    int offset = TimeZone.getDefault().getOffset(date);
                                                    obj.put("created_time", date + (long) offset);
                                                    obj.put("action", "USER_TRANSFER_MONEY");
                                                    if (Long.parseLong(money) >= 2000000L) {
                                                        obj.put("need_check", true);
                                                        checkQuota = this.CheckQuota(userModel.getNickname(), true);
                                                        obj.put("total_angecy_receive", checkQuota.getTotal_agency_receive());
                                                        obj.put("total_agency_transfer", checkQuota.getTotal_agency_transfer());
                                                        obj.put("total_bet_money", checkQuota.getTotal_bet_money());
                                                        obj.put("total_giftcode_money", checkQuota.getTotal_giftcode_money());
                                                        obj.put("total_recharge_money", checkQuota.getTotal_recharge_card_money());
                                                        obj.put("total_user_receive", checkQuota.getTotal_user_receive());
                                                        obj.put("total_user_transfer", checkQuota.getTotal_user_transfer());
                                                        obj.put("total_win_money", checkQuota.getTotal_win_money());
                                                    }

                                                    StringEntity requestEntity = new StringEntity(obj.toString(), "UTF-8");
                                                    httpPost.setEntity(requestEntity);
                                                    HttpResponse response = client.execute(httpPost);
                                                    StringBuffer result;
                                                    try (BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()))) {
                                                        result = new StringBuffer();
                                                        String line = "";

                                                        while ((line = rd.readLine()) != null) {
                                                            result.append(line);
                                                        }
                                                    }

                                                    logger.debug("LobbyModule bot tele response: " + result);
                                                } catch (Exception var37) {
                                                    logger.debug("LobbyModule error: " + var37);
                                                }

                                                UserModel userReceive = userService.getUserByNickName(receiver);
                                                if (userReceive != null && (userReceive.getDaily() == 1 || userReceive.getDaily() == 2)) {
                                                    try (CloseableHttpClient httpClient = HttpClientBuilder.create().build();) {
                                                        String url = "";
                                                        if ("XXENG".equals(PartnerConfig.Client)) {
                                                            url = PartnerConfig.HostBot;
                                                        } else if ("R68".equals(PartnerConfig.Client)) {
                                                            url = PartnerConfig.HostBot;
                                                        } else {
                                                            url = PartnerConfig.HostBot;
                                                        }

                                                        HttpPost httpPost = new HttpPost(url + "/rpadmin/ozawasecret1243/10");
                                                        httpPost.addHeader("Content-Type", "application/json");
                                                        JSONObject objSend = new JSONObject();
                                                        sender = userModel.getNickname();
                                                        serverName = "xxeng";
                                                        if (userModel.getClient() != null && !userModel.getClient().equals("")) {
                                                            if (userModel.getClient().equals("M")) {
                                                                serverName = "manVip";
                                                            } else if (userModel.getClient().equals("R")) {
                                                                serverName = "r99";
                                                            } else if (userModel.getClient().equals("V")) {
                                                                serverName = "Vip52";
                                                            }
                                                        }

                                                        objSend.put("sender_nick_name", sender);
                                                        objSend.put("receiver_nick_name", receiver);
                                                        objSend.put("serverName", serverName);
                                                        objSend.put("receiver_mobile", userReceive.getMobile());
                                                        objSend.put("is_agent", true);
                                                        objSend.put("money", Long.parseLong(money));
                                                        objSend.put("description", desc);
                                                        lMoney = Long.parseLong(money);
                                                        long currentMoneyReceive = userReceive.getCurrentMoney("vin");
                                                        objSend.put("previous_money", currentMoneyReceive - lMoney);
                                                        objSend.put("current_money", currentMoneyReceive);
                                                        long date = System.currentTimeMillis();
                                                        int offset = TimeZone.getDefault().getOffset(date);
                                                        objSend.put("created_time", date + (long) offset);
                                                        StringEntity requestEntity = new StringEntity(objSend.toString(), "UTF-8");
                                                        httpPost.setEntity(requestEntity);
                                                        try (CloseableHttpResponse httpResponse = httpClient.execute(httpPost);
                                                             InputStreamReader inputStreamReader = new InputStreamReader(httpResponse.getEntity().getContent());
                                                             BufferedReader rd = new BufferedReader(inputStreamReader);
                                                        ) {
                                                            StringBuffer result;
                                                            result = new StringBuffer();
                                                            String line = "";

                                                            while ((line = rd.readLine()) != null) {
                                                                result.append(line);
                                                            }
                                                            logger.info("LobbyModule bot tele response: " + result);
                                                        }
                                                    } catch (Exception var36) {
                                                        logger.info("LobbyModule error: " + var36);
                                                    }
                                                }

                                                return "{\"code\":0,\"message\":\"success\"}";
                                            } else {
                                                return "{\"code\":" + res.getCode() + ",\"message\":\"failed\"}";
                                            }
                                        }
                                    } else {
                                        res = userService.transferMoney(userModel.getNickname(), receiver, Long.parseLong(money), desc, true);
                                        return "{\"code\":" + res.getCode() + ",\"message\":\"success\"}";
                                    }
                                } else if (userModel.getDaily() != 1 && userModel.getDaily() != 2) {
                                    return "{\"code\":500,\"message\":\"error\"}";
                                } else {
                                    logger.debug("TransferMoneyProcessor chuyen tien tu dai ly");
                                    if (otp != null && !"".equals(otp)) {
                                        OtpServiceImpl otpSer = new OtpServiceImpl();
                                        int code = otpSer.checkOtp(otp, userModel.getNickname(), "0", (String) null);
                                        if (code != 0) {
                                            return "{\"code\":222,\"message\":\"invalid otp\"}";
                                        } else {
                                            res = userService.transferMoney(userModel.getNickname(), receiver, Long.parseLong(money), desc, false);
                                            if (res.getCode() == 0) {
                                                String url;
                                                HttpPost httpPost;
                                                JSONObject objSend;
                                                String sender;
                                                long currentMoneyReceive;
                                                try {
                                                    HttpResponse response;
                                                    try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
                                                        url = "";
                                                        if ("XXENG".equals(PartnerConfig.Client)) {
                                                            url = PartnerConfig.HostBot;
                                                        } else if ("R68".equals(PartnerConfig.Client)) {
                                                            url = PartnerConfig.HostBot;
                                                        } else {
                                                            url = PartnerConfig.HostBot;
                                                        }

                                                        httpPost = new HttpPost(url + "/rpadmin/ozawasecret1243/3");
                                                        httpPost.addHeader("Content-Type", "application/json");
                                                        logger.debug("TransferMoneyProcessor chuyen tien tu dai ly:  url:" + url);
                                                        objSend = new JSONObject();
                                                        sender = userModel.getNickname();
                                                        serverName = "xxeng";
                                                        if (userModel.getClient() != null && !userModel.getClient().equals("")) {
                                                            if (userModel.getClient().equals("M")) {
                                                                serverName = "manVip";
                                                            } else if (userModel.getClient().equals("R")) {
                                                                serverName = "r99";
                                                            } else if (userModel.getClient().equals("V")) {
                                                                serverName = "Vip52";
                                                            }
                                                        }

                                                        objSend.put("sender_nick_name", sender);
                                                        objSend.put("receiver_nick_name", receiver);
                                                        objSend.put("money", Long.parseLong(money));
                                                        objSend.put("description", desc);
                                                        objSend.put("serverName", serverName);
                                                        currentMoneyReceive = System.currentTimeMillis();
                                                        int offset = TimeZone.getDefault().getOffset(currentMoneyReceive);
                                                        objSend.put("created_time", currentMoneyReceive + (long) offset);
                                                        objSend.put("action", "AGENT_TRANSFER_MONEY");
                                                        StringEntity requestEntity = new StringEntity(objSend.toString(), "UTF-8");
                                                        httpPost.setEntity(requestEntity);
                                                        try (CloseableHttpResponse httpResponse = httpClient.execute(httpPost);
                                                             InputStreamReader inputStreamReader = new InputStreamReader(httpResponse.getEntity().getContent());
                                                             BufferedReader rd = new BufferedReader(inputStreamReader);
                                                        ) {
                                                            StringBuffer result;
                                                            result = new StringBuffer();
                                                            String line = "";

                                                            while ((line = rd.readLine()) != null) {
                                                                result.append(line);
                                                            }

                                                            logger.debug("LobbyModule bot tele response: " + result);
                                                        }
                                                    }
                                                } catch (Exception var39) {
                                                    logger.debug("LobbyModule error: " + var39);
                                                }

                                                try (CloseableHttpClient httpClient = HttpClientBuilder.create().build();) {

                                                    url = "";
                                                    if ("XXENG".equals(PartnerConfig.Client)) {
                                                        url = PartnerConfig.HostBot;
                                                    } else if ("R68".equals(PartnerConfig.Client)) {
                                                        url = PartnerConfig.HostBot;
                                                    } else {
                                                        url = PartnerConfig.HostBot;
                                                    }

                                                    httpPost = new HttpPost(url + "/rpadmin/ozawasecret1243/11");
                                                    httpPost.addHeader("Content-Type", "application/json");
                                                    objSend = new JSONObject();
                                                    sender = userModel.getNickname();
                                                    serverName = "xxeng";
                                                    if (userModel.getClient() != null && !userModel.getClient().equals("")) {
                                                        if (userModel.getClient().equals("M")) {
                                                            serverName = "manVip";
                                                        } else if (userModel.getClient().equals("R")) {
                                                            serverName = "r99";
                                                        } else if (userModel.getClient().equals("V")) {
                                                            serverName = "Vip52";
                                                        }
                                                    }

                                                    objSend.put("sender_nick_name", sender);
                                                    objSend.put("receiver_nick_name", receiver);
                                                    objSend.put("sender_mobile", userModel.getMobile());
                                                    objSend.put("is_agent", true);
                                                    objSend.put("money", Long.parseLong(money));
                                                    objSend.put("description", desc);
                                                    objSend.put("serverName", serverName);
                                                    currentMoneyReceive = userModel.getCurrentMoney("vin");
                                                    objSend.put("previous_money", currentMoneyReceive);
                                                    objSend.put("current_money", currentMoneyReceive - Long.parseLong(money));
                                                    lMoney = System.currentTimeMillis();
                                                    int offset = TimeZone.getDefault().getOffset(lMoney);
                                                    objSend.put("created_time", lMoney + (long) offset);
                                                    StringEntity requestEntity = new StringEntity(objSend.toString(), "UTF-8");
                                                    httpPost.setEntity(requestEntity);
                                                    try (CloseableHttpResponse httpResponse = httpClient.execute(httpPost);
                                                         InputStreamReader inputStreamReader = new InputStreamReader(httpResponse.getEntity().getContent());
                                                         BufferedReader rd = new BufferedReader(inputStreamReader);
                                                    ) {
                                                        StringBuffer result;
                                                        result = new StringBuffer();
                                                        String line = "";

                                                        while ((line = rd.readLine()) != null) {
                                                            result.append(line);
                                                        }
                                                        logger.info("LobbyModule bot tele response: " + result);
                                                    }
                                                } catch (Exception var38) {
                                                    logger.info("LobbyModule error: " + var38);
                                                }

                                                return "{\"code\":0,\"message\":\"success\"}";
                                            } else {
                                                return "{\"code\":" + res.getCode() + ",\"message\":\"failed\"}";
                                            }
                                        }
                                    } else {
                                        res = userService.transferMoney(userModel.getNickname(), receiver, Long.parseLong(money), desc, true);
                                        return "{\"code\":" + res.getCode() + ",\"message\":\"success\"}";
                                    }
                                }
                            }
                        } else {
                            return res.toJson();
                        }
                    }
                } catch (Exception var40) {
                    StringWriter sw = new StringWriter();
                    PrintWriter pw = new PrintWriter(sw);
                    var40.printStackTrace(pw);
                    String sStackTrace = sw.toString();
                    logger.info("TransferMoneyProcessor 1:" + var40.getMessage());
                    logger.info(sStackTrace);
                    return "{\"code\":500,\"message\":\"error\"}";
                }
            } else {
                return "{\"code\":500,\"message\":\"error\"}";
            }
        } catch (Exception var41) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            var41.printStackTrace(pw);
            receiver = sw.toString();
            logger.info("TransferMoneyProcessor 2:" + var41.getMessage());
            logger.info(receiver);
            return "{\"code\":500,\"message\":\"error\"}";
        }
    }

    private QuotaResponse CheckQuota(String nick_name, boolean seven_days) {
        QuotaResponse response = new QuotaResponse();
/*
        try {
            UserModel userModel = GameMoneyInfo.userService.getUserByNickName(nick_name);
            if (userModel != null) {
                LogMoneyUserDao logService = new LogMoneyUserDaoImpl();
                long total_giftcode_money = 0L;
                long total_user_receive = 0L;
                long total_agency_receive = 0L;
                long total_user_transfer = 0L;
                long total_agency_transfer = 0L;
                AgentServiceImpl service = new AgentServiceImpl();
                List<AgentResponse> agents = service.listAgent();
                ArrayList<String> agentNames = new ArrayList();
                if (agents != null && agents.size() > 0) {
                    Iterator var19 = agents.iterator();

                    while(var19.hasNext()) {
                        AgentResponse agent = (AgentResponse)var19.next();
                        agentNames.add(agent.nickName);
                    }
                }

                List<LogUserMoneyResponse> resultGiftCode = logService.searchAllLogMoneyUser(nick_name, "GIFTCODE", seven_days);
                if (resultGiftCode != null && resultGiftCode.size() > 0) {
                    total_giftcode_money = (Long)resultGiftCode.stream().map((transx) -> {
                        return transx.moneyExchange;
                    }).reduce(total_giftcode_money, (accumulator, _item) -> {
                        return accumulator + _item;
                    });
                }

                List<LogUserMoneyResponse> resulReceive = logService.searchAllLogMoneyUser(nick_name, "RECEIVE", seven_days);
                if (resulReceive != null && resulReceive.size() > 0) {
                    Iterator var21 = resulReceive.iterator();

                    while(var21.hasNext()) {
                        LogUserMoneyResponse trans = (LogUserMoneyResponse)var21.next();
                        boolean matchAgent = false;
                        Iterator var24 = agentNames.iterator();

                        while(var24.hasNext()) {
                            String s = (String)var24.next();
                            if (trans.description.contains(s)) {
                                matchAgent = true;
                            }
                        }

                        if (matchAgent) {
                            total_agency_receive += trans.moneyExchange;
                        } else {
                            total_user_receive += trans.moneyExchange;
                        }
                    }
                }

                List<LogUserMoneyResponse> resulTransfer = logService.searchAllLogMoneyUser(nick_name, "TRANSFER", seven_days);
                if (resulTransfer != null && resulTransfer.size() > 0) {
                    Iterator var43 = resulTransfer.iterator();

                    while(var43.hasNext()) {
                        LogUserMoneyResponse trans = (LogUserMoneyResponse)var43.next();
                        boolean matchAgent = false;
                        Iterator var48 = agentNames.iterator();

                        while(var48.hasNext()) {
                            String s = (String)var48.next();
                            if (trans.description.contains(s)) {
                                matchAgent = true;
                            }
                        }

                        if (matchAgent) {
                            total_agency_transfer += trans.moneyExchange;
                        } else {
                            total_user_transfer += trans.moneyExchange;
                        }
                    }
                }

                long total_recharge_card_money = 0L;
                List<LogUserMoneyResponse> resultCard = logService.searchAllLogMoneyUser(nick_name, "CARD", seven_days);
                if (resultCard != null && resultCard.size() > 0) {
                    total_recharge_card_money = (Long)resultCard.stream().map((transx) -> {
                        return transx.moneyExchange;
                    }).reduce(total_recharge_card_money, (accumulator, _item) -> {
                        return accumulator + _item;
                    });
                }

                long quota = 0L;
                quota += total_recharge_card_money;
                quota += total_agency_receive;
                quota += total_user_receive * 4L;
                quota += total_giftcode_money * 4L;
                quota += total_agency_transfer;
                quota += total_user_transfer;
                logger.debug("Quota:" + quota);
                long total_bet_money = 0L - logService.getTotalBetWin(nick_name, "BET", (String)null);
                long total_win_money = logService.getTotalBetWin(nick_name, "WIN", (String)null);
                long total_bet_banca_money = 0L - logService.getTotalBetWin(nick_name, "BET", "HamCaMap");
                long total_bet_slot_money = 0L - logService.getTotalBetWin(nick_name, "BET", "SLOT");
                long finalQuota = total_bet_money - total_bet_banca_money * 9L / 10L - total_bet_slot_money * 9L / 10L;
                logger.debug("Final quota :" + finalQuota);
                long manual_quota = userModel.getManual_quota();
                finalQuota += manual_quota;
                if (finalQuota > quota) {
                    response.setCode(0);
                } else {
                    response.setCode(1);
                }

                response.setTotal_agency_receive(total_agency_receive);
                response.setTotal_agency_transfer(total_agency_transfer);
                response.setTotal_bet_money(total_bet_money);
                response.setTotal_giftcode_money(total_giftcode_money);
                response.setTotal_recharge_card_money(total_recharge_card_money);
                response.setTotal_user_receive(total_user_receive);
                response.setTotal_user_transfer(total_user_transfer);
                response.setTotal_win_money(total_win_money);
            }
        } catch (Exception var39) {
            response.setCode(-1);
            logger.debug("Check quote error : " + var39.getMessage());
        }

        return response;

 */
        return null;
    }
}
