package com.vinplay.api.processors.otp;


import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.vinplay.usercore.service.impl.OtpServiceImpl;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.usercore.utils.GameCommon;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.enums.StatusGames;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.response.Set2AFResponse;
import com.vinplay.vbee.common.statics.TimeBasedOneTimePasswordUtil;
import com.vinplay.vbee.common.utils.VinPlayUtils;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;

public class ValidateAppOTPProcessor implements BaseProcessor<HttpServletRequest, String> {
    private static final Logger logger = Logger.getLogger("api");

    public ValidateAppOTPProcessor() {
    }

    public String execute(Param<HttpServletRequest> param) {
        HttpServletRequest request = (HttpServletRequest)param.get();
        String username = request.getParameter("un");
        String otp = request.getParameter("otp");
        Set2AFResponse res = new Set2AFResponse(false, "1001");

        try {
            UserServiceImpl userService = new UserServiceImpl();
            UserModel userModel = userService.getUserByUserName(username);
            if (userModel == null) {
                res.setErrorCode("1005");
                return res.toJson();
            } else {
                OtpServiceImpl otpSer = new OtpServiceImpl();
                int code = otpSer.checkOtp(otp, userModel.getNickname(), "0", (String)null);
                if (code == 0) {
                    if (username == null) {
                        return res.toJson();
                    } else if (username.isEmpty()) {
                        return res.toJson();
                    } else {
                        int statusGame = GameCommon.getValueInt("STATUS_GAME");
                        if (statusGame == StatusGames.MAINTAIN.getId()) {
                            res.setErrorCode("1114");
                            logger.debug("Response login: " + res.toJson());
                            return res.toJson();
                        } else {
                            String nick_name = userModel.getNickname();
                            if (nick_name != null && !nick_name.trim().isEmpty()) {
                                String secret = VinPlayUtils.getUserSecretKey(userModel.getNickname());
                                if (secret != null && !secret.isEmpty()) {
                                    res.setSuccess(true);
                                    res.setNickname(nick_name);
                                    res.setErrorCode("0");
                                    res.setSecret(secret);
                                    String string = res.toJson();
                                    return string;
                                } else {
                                    secret = TimeBasedOneTimePasswordUtil.generateBase32Secret();
                                    HazelcastInstance client = HazelcastClientFactory.getInstance();
                                    IMap userMap = client.getMap("users");
                                    if (!userMap.containsKey(nick_name)) {
                                        res.setErrorCode("3002");
                                        return res.toJson();
                                    } else {
                                        String var19;
                                        try {
                                            userMap.lock(nick_name);
                                            String string;
                                            if (!VinPlayUtils.setUserSecretKey(nick_name, secret)) {
                                                res.setErrorCode("3003");
                                                string = res.toJson();
                                                return string;
                                            }

                                            res.setSuccess(true);
                                            res.setNickname(nick_name);
                                            res.setErrorCode("0");
                                            res.setSecret(secret);
                                            string = res.toJson();
                                            String var28 = string;
                                            return var28;
                                        } catch (Exception var24) {
                                            res.setErrorCode(var24.getMessage());
                                            logger.debug("activeMobile error: " + var24.getMessage());
                                            StringWriter sw = new StringWriter();
                                            PrintWriter pw = new PrintWriter(sw);
                                            var24.printStackTrace(pw);
                                            String sStackTrace = sw.toString();
                                            logger.debug(sStackTrace);
                                            var19 = res.toJson();
                                        } finally {
                                            userMap.unlock(nick_name);
                                        }

                                        return var19;
                                    }
                                }
                            } else {
                                res.setErrorCode("2001");
                                return res.toJson();
                            }
                        }
                    }
                } else {
                    logger.debug("otp code :" + code);
                    res.setErrorCode("2002");
                    return res.toJson();
                }
            }
        } catch (Exception var26) {
            logger.debug(var26);
            return res.toJson();
        }
    }
}