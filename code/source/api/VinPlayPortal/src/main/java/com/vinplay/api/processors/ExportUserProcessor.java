package com.vinplay.api.processors;

import com.vinplay.api.entities.CSVUtils;
import com.vinplay.dal.service.impl.AgentServiceImpl;
import com.vinplay.usercore.dao.UserInfoDao;
import com.vinplay.usercore.dao.impl.UserInfoDaoImpl;
import com.vinplay.usercore.entities.ExportUser;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;

public class ExportUserProcessor implements BaseProcessor<HttpServletRequest, String> {
    private static final Logger logger = Logger.getLogger("api");

    public ExportUserProcessor() {
    }

    public String execute(Param<HttpServletRequest> param) {
        new AgentServiceImpl();
        HttpServletRequest request = (HttpServletRequest)param.get();
        String startDate = request.getParameter("sd");
        String endDate = request.getParameter("ed");

        try {
            Writer writer = new StringWriter();
            CSVUtils.writeLine(writer, Arrays.asList("nick_name", "mobile", "recharge_money"));
            UserInfoDao dao = new UserInfoDaoImpl();
            List<ExportUser> users = dao.GetExportUser(startDate + " 00:00:00", endDate + " 23:59:59");
            Iterator var9 = users.iterator();

            while(var9.hasNext()) {
                ExportUser d = (ExportUser)var9.next();
                List<String> list = new ArrayList();
                list.add(d.getNick_name());
                list.add(d.getMobile());
                list.add(d.getRecharge_money() + "");
                CSVUtils.writeLine(writer, list);
            }

            return writer.toString();
        } catch (Exception var12) {
            var12.printStackTrace();
            logger.debug(var12);
            return "";
        }
    }
}