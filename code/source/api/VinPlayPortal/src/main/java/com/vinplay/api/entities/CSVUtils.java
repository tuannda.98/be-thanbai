package com.vinplay.api.entities;


import java.io.IOException;
import java.io.Writer;
import java.util.Iterator;
import java.util.List;

public class CSVUtils {
    private static final char DEFAULT_SEPARATOR = ',';

    public CSVUtils() {
    }

    public static void writeLine(Writer w, List<String> values) throws IOException {
        writeLine(w, values, ',', ' ');
    }

    public static void writeLine(Writer w, List<String> values, char separators) throws IOException {
        writeLine(w, values, separators, ' ');
    }

    private static String followCVSformat(String value) {
        String result = value;
        if (value.contains("\"")) {
            result = value.replace("\"", "\"\"");
        }

        return result;
    }

    public static void writeLine(Writer w, List<String> values, char separators, char customQuote) throws IOException {
        boolean first = true;
        if (separators == ' ') {
            separators = ',';
        }

        StringBuilder sb = new StringBuilder();

        for(Iterator var6 = values.iterator(); var6.hasNext(); first = false) {
            String value = (String)var6.next();
            if (!first) {
                sb.append(separators);
            }

            if (customQuote == ' ') {
                sb.append(followCVSformat(value));
            } else {
                sb.append(customQuote).append(followCVSformat(value)).append(customQuote);
            }
        }

        sb.append("\n");
        w.append(sb.toString());
    }
}
