package com.vinplay.api.utils;

import casio.king365.GU;
import com.google.common.base.Charsets;
import com.hazelcast.core.IMap;
import com.vinplay.vbee.common.hazelcast.HazelcastUtils;
import com.vinplay.vbee.common.models.SocialModel;
import com.vinplay.vbee.common.utils.VinPlayUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class SocialUtils {
    private static final Logger logger = Logger.getLogger((String) "api");
    private static final String API_FACEBOOK = "https://graph.facebook.com/v2.7/me?access_token=";
    private static final String API_GOOGLE = "https://www.googleapis.com/oauth2/v3/tokeninfo?access_token=";
    private static final String[] GG_IDS = new String[]{"632901182605-s59smrvsrd3resph64nmbiovaa6v08oj.apps.googleusercontent.com", "632901182605-6cf5gj22ef531btoja5j6ebuccmson1f.apps.googleusercontent.com", "632901182605-35bmu82riimsndsr9g65a9fn4osunjv1.apps.googleusercontent.com", "632901182605-t0p14rbls6h017jspnrd8fantpqb4ooi.apps.googleusercontent.com", "632901182605-oalvs5e7gk6nu9kqea90bcf5e9bhkeli.apps.googleusercontent.com", "632901182605-idkb0lrdompljkdmfmoq2p7bc56ore40.apps.googleusercontent.com", "632901182605-ah8sn546fs259v87gaiinhf6ugopbr99.apps.googleusercontent.com", "632901182605-imk18vb4q850ddqn6m43c05vm8867jcu.apps.googleusercontent.com", "632901182605-h5qhgpad213jmo4mch2vf4t90fj7tb95.apps.googleusercontent.com", "632901182605-vl30qrp1svkg4mqa1ggrqa1qg17l7som.apps.googleusercontent.com", "632901182605-86fgr3vj27tf353nm2q8n347hh0s5sg9.apps.googleusercontent.com", "632901182605-mqkpean5vceq844fipah1amd36osmbu0.apps.googleusercontent.com", "632901182605-evru2518innmn7uf9atqe01m9mt5ktt8.apps.googleusercontent.com", "632901182605-u90388ie3s2v5mqb4iiql3gkgeej46d9.apps.googleusercontent.com", "632901182605-tjlq8d2433p2d90ibko12hmhkqn2c5ng.apps.googleusercontent.com", "632901182605-9aaavf95hm6tu2lpv0htrski7jrnff5f.apps.googleusercontent.com", "632901182605-7jact08ff5tq6ms0mrk6jo2klkr5u2it.apps.googleusercontent.com"};
    public static final String CACHE_SOCIAL_ID_BY_ACCESS_TOKEN = "socialIdByAccessToken";

    public static void socialSuccess(IMap<String, SocialModel> socialMap, String socialId, String accessToken) {
        if (socialMap.containsKey(socialId)) {
            try {
                socialMap.lock(socialId);
                SocialModel model = socialMap.get(socialId);
                model.setAccessToken(accessToken);
                model.setLoginTime(new Date());
                socialMap.put(socialId, model);
            } catch (Exception e) {
                logger.debug(e);
                return;
            }
            try {
                socialMap.unlock(socialId);
            } catch (Exception e) {
            }
        } else {
            socialMap.put(socialId, new SocialModel(accessToken, socialId, new Date()), 24 * 60 * 60, TimeUnit.SECONDS);
        }

        HazelcastUtils.autoExpireLoadingCache(CACHE_SOCIAL_ID_BY_ACCESS_TOKEN, accessToken, s -> socialId, 24 * 60 * 60);
    }

    public static String getSocialId(IMap<String, SocialModel> socialMap, String accessToken, String social) {
        String socialId = HazelcastUtils.autoExpireLoadingCache(CACHE_SOCIAL_ID_BY_ACCESS_TOKEN, accessToken, s -> {
            if (socialMap != null) {
                for (Map.Entry<String, SocialModel> entry : socialMap.entrySet()) {
                    if (!(entry.getValue()).getAccessToken().equals(accessToken)
                            || VinPlayUtils.socialTimeout(entry.getValue().getLoginTime()))
                        continue;
                    return entry.getValue().getSocialId();
                }
            }
            return null;
        }, 24 * 60 * 60);

        if (socialId == null) {
            if (social.equals("fb")) {
                socialId = SocialUtils.getIdFacebook(accessToken);
            } else if (social.equals("gg")) {
                socialId = SocialUtils.getIdGoogle(accessToken);
            } else if (social.equals("apple")) {
                // Trả về string dài 40 kí tự, gồm 20 kí tự đầu và 20 kí tự cuối của accessToken
                int leng = accessToken.length();
                socialId = accessToken.substring(0, 20) + accessToken.substring(leng - 20, leng);
            } else if (social.equals("dev")) {
                // Trả về string dài 40 kí tự, gồm 20 kí tự đầu và 20 kí tự cuối của accessToken
                int leng = accessToken.length();
                socialId = accessToken.substring(0, 20) + accessToken.substring(leng - 20, leng);
            }
        }
        return socialId;
    }

    private static String getIdFacebook(String accessToken) {
        try {
            String res = SocialUtils.getData(API_FACEBOOK + accessToken);
            if (!res.isEmpty()) {
                JSONObject obj = new JSONObject(res);
                String id = obj.getString("id");
                return id;
            }
            return "";
        } catch (Exception e) {
            logger.debug(e);
            return null;
        }
    }

    private static String getIdGoogle(String accessToken) {
        try {
            String res = SocialUtils.getData(API_GOOGLE + accessToken);
            if (!res.isEmpty()) {
                JSONObject obj = new JSONObject(res);
                String id = obj.getString("sub");
                String clientId = obj.getString("azp");
                for (String str : GG_IDS) {
                    if (!str.equalsIgnoreCase(clientId)) continue;
                    return id;
                }
                return "";
            }
            return "";
        } catch (Exception e) {
            logger.debug(e);
            return null;
        }
    }

    private static String getData(String sUrl) {
        String output;
        String res = "";
        try {
            URL url = new URL(sUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            conn.setConnectTimeout(12000);
            if (conn.getResponseCode() != 200) {
                return res;
            }
            try (InputStream inputStream = conn.getInputStream();
                 InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charsets.UTF_8);
                 BufferedReader br = new BufferedReader(inputStreamReader)) {
                while ((output = br.readLine()) != null) {
                    res = res + output;
                }
            }
            conn.disconnect();
        } catch (java.net.SocketTimeoutException e) {
            GU.sendOperation("LoginProcessor, Login social exceed 12s timeout, url: " + sUrl);
        } catch (java.io.IOException e) {
            GU.sendOperation("LoginProcessor, Login social exceed 12s timeout, url: " + sUrl);
        }
        return res;
    }
}
