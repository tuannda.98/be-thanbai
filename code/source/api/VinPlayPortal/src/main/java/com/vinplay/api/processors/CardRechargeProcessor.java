package com.vinplay.api.processors;


import com.vinplay.dichvuthe.response.RechargeResponse;
import com.vinplay.dichvuthe.service.RechargeService;
import com.vinplay.dichvuthe.service.impl.RechargeServiceImpl;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.usercore.utils.PartnerConfig;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.enums.Platform;
import com.vinplay.vbee.common.enums.ProviderType;
import com.vinplay.vbee.common.models.UserModel;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;

public class CardRechargeProcessor implements BaseProcessor<HttpServletRequest, String> {
    private static final Logger logger = Logger.getLogger("api");

    public CardRechargeProcessor() {
    }

    public String execute(Param<HttpServletRequest> param) {
        String serial;
        try {
            HttpServletRequest request = (HttpServletRequest)param.get();
            String token = request.getParameter("t");
            String userName = request.getParameter("un");
            serial = request.getParameter("s");
            String pin = request.getParameter("p");
            String amount = request.getParameter("a");
            String cardType = request.getParameter("ct");
            UserServiceImpl userService = new UserServiceImpl();
            if (userName != null && !"".equals(userName)) {
                UserModel userModel = userService.getUserByUserName(userName);
                if (userModel != null) {
                    logger.debug("Recharge error: " + PartnerConfig.CongGachThe + ":" + cardType + ":" + serial + ":" + pin + ":" + amount);
                    Platform platform = Platform.find("web");
                    amount = amount.replace(".", "");
                    amount = amount.replace(",", "");
                    RechargeService rechargeService = new RechargeServiceImpl();
                    RechargeResponse res = null;
                    if (PartnerConfig.CongGachThe.trim().equals("GACHTHE")) {
//                        res = rechargeService.rechargeByGachThe(userModel.getNickname(), ProviderType.getProviderByValue(cardType.toLowerCase()), serial, pin, amount, platform.getName(), 0);
                    } else if (PartnerConfig.CongGachThe.trim().equals("NAPTIENGA")) {
//                        res = rechargeService.rechargeByNapTienGa(userModel.getNickname(), ProviderType.getProviderByValue(cardType.toLowerCase()), serial, pin, amount, platform.getName(), 0);
                    } else if (PartnerConfig.CongGachThe.trim().equals("NAPNHANH")) {
                        //res = rechargeService.rechargeByNapNhanh(userModel.getNickname(), ProviderType.getProviderByValue(cardType.toLowerCase()), serial, pin, amount, platform.getName(), 0);
                    } else if (PartnerConfig.CongGachThe.trim().equals("MUACARD")) {
//                        res = rechargeService.rechargeByMuaCard(userModel.getNickname(), ProviderType.getProviderByValue(cardType.toLowerCase()), serial, pin, amount, platform.getName(), 0);
                    } else if (PartnerConfig.CongGachThe.trim().equals("MUACARD24H")) {
//                        res = rechargeService.rechargeByMuaCard24h(userModel.getNickname(), ProviderType.getProviderByValue(cardType.toLowerCase()), serial, pin, amount, platform.getName(), 0);
                    } else if (PartnerConfig.CongGachThe.trim().equals("ECARD")) {
//                        res = rechargeService.rechargeByECard(userModel.getNickname(), ProviderType.getProviderByValue(cardType.toLowerCase()), serial, pin, amount, platform.getName(), 0);
                    } else if (PartnerConfig.CongGachThe.trim().equals("ZOANCARD")) {
                        //res = rechargeService.rechargeByZoan(userModel.getNickname(), ProviderType.getProviderByValue(cardType.toLowerCase()), serial, pin, amount, platform.getName(), 0);
                    } else if (PartnerConfig.CongGachThe.trim().equals("NAPTHEZING")) {
                        //res = rechargeService.rechargeByNapTheZing(userModel.getNickname(), ProviderType.getProviderByValue(cardType.toLowerCase()), serial, pin, amount, platform.getName(), 0);
                    } else {
                        res = new RechargeResponse(-1, 0L, 0, 0L);
                    }

                    return res.toJson();
                }
            }

            return "{\"code\":500,\"message\":\"error\"}";
        } catch (Exception var14) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            var14.printStackTrace(pw);
            serial = sw.toString();
            logger.info(var14.getMessage());
            logger.info(serial);
            return "{\"code\":500,\"message\":\"error\"}";
        }
    }
}