package com.vinplay.api.processors;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.vinplay.api.entities.QuotaResponse;
import com.vinplay.dal.dao.LogMoneyUserDao;
import com.vinplay.dal.dao.impl.LogMoneyUserDaoImpl;
import com.vinplay.dal.service.impl.AgentServiceImpl;
import com.vinplay.dichvuthe.dao.impl.CashoutDaoImpl;
import com.vinplay.dichvuthe.response.CashoutResponse;
import com.vinplay.usercore.service.UserService;
import com.vinplay.usercore.service.impl.OtpServiceImpl;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.usercore.utils.GameCommon;
import com.vinplay.usercore.utils.PartnerConfig;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.messages.LogMoneyUserMessage;
import com.vinplay.vbee.common.messages.MoneyMessageInMinigame;
import com.vinplay.vbee.common.messages.dvt.CashoutByBankMessage;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.response.AgentResponse;
import com.vinplay.vbee.common.response.LogUserMoneyResponse;
import com.vinplay.vbee.common.rmq.RMQApi;
import com.vinplay.vbee.common.utils.VinPlayUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class RequestCashoutBankProcessor implements BaseProcessor<HttpServletRequest, String> {
    private static final Logger logger = Logger.getLogger("portal");

    public RequestCashoutBankProcessor() {
    }

    public String execute(Param<HttpServletRequest> param) {
        CashoutResponse resp = new CashoutResponse(-1, 0L);
        HttpServletRequest request = (HttpServletRequest) param.get();
        String nickname = request.getParameter("nn");
        String token = request.getParameter("at");
        String name = request.getParameter("n");
        String bank = request.getParameter("b");
        String accountNumber = request.getParameter("an");
        String value = request.getParameter("mn");
        String otp = request.getParameter("otp");
        logger.debug(nickname + ":" + token + ":" + name + ":" + bank + ":" + accountNumber + ":" + value);

        try {
            UserServiceImpl userService = new UserServiceImpl();
            UserModel userModel = userService.getUserByNickName(nickname);
            if (userModel == null) {
                resp = new CashoutResponse(-2, 0L);
                return resp.toJson();
            } else {
                IMap userMap = HazelcastClientFactory.getInstance().getMap("users");
                if (!userMap.containsKey(userModel.getNickname())) {
                    return resp.toJson();
                } else {
                    UserCacheModel userCache = (UserCacheModel) userMap.get(userModel.getNickname());
                    if (userCache == null) {
                        resp = new CashoutResponse(-3, 0L);
                        return resp.toJson();
                    } else if (userCache.getAccessToken().equals(token)) {
                        CashoutDaoImpl cashoutDao = new CashoutDaoImpl();
                        if (GameCommon.getValueInt("IS_CASHOUT_BANK") == 1) {
                            logger.debug("ERROR: Bao Tri");
                            resp = new CashoutResponse(-99, 0L);
                            return resp.toJson();
                        } else if (bank != null && value != null && name != null && accountNumber != null) {
                            int moneyExchange = Integer.parseInt(value);
                            List<Integer> values = Arrays.asList(100000, 200000, 300000, 500000, 1000000, 2000000, 5000000, 10000000);
                            if (!values.contains(moneyExchange)) {
                                logger.debug("ERROR: Menh gia ko dung");
                                resp = new CashoutResponse(-11, 0L);
                                return resp.toJson();
                            } else {
                                int valueSubtract = (int) ((double) moneyExchange * GameCommon.getValueDouble("RATIO_CASHOUT_BANK"));
                                if (userCache.getCurrentMoney("vin") >= (long) valueSubtract) {
                                    HazelcastInstance client = HazelcastClientFactory.getInstance();
                                    if (client == null) {
                                        logger.debug("ERROR: Khong Ket Noi Duoc Hazel");
                                        return resp.toJson();
                                    } else {
                                        long moneyUser = userCache.getVin();
                                        long currentMoney = userCache.getVinTotal();
                                        // int LIMIT_EXCHANGE = true;
                                        OtpServiceImpl ser;
                                        if (otp == null || otp.equals("")) {
                                            ser = new OtpServiceImpl();
                                            ser.sendOtpEsms(userCache.getNickname(), userCache.getMobile());
                                            resp = new CashoutResponse(0, userCache.getVinTotal());
                                            return resp.toJson();
                                        } else {
                                            ser = new OtpServiceImpl();
                                            if (ser.checkOtp(otp, userCache.getNickname(), "0", (String) null) != 0) {
                                                resp = new CashoutResponse(11, userCache.getVinTotal());
                                                return resp.toJson();
                                            } else {
                                                String var65;
                                                try {
                                                    userMap = client.getMap("users");
                                                    userMap.lock(nickname);
                                                    userCache = (UserCacheModel) userMap.get(nickname);
                                                    moneyUser = userCache.getVin();
                                                    currentMoney = userCache.getVinTotal();
                                                    userCache.setVin(moneyUser -= (long) valueSubtract);
                                                    userCache.setVinTotal(currentMoney -= (long) valueSubtract);
                                                    int moneyCashout = userCache.getCashout();
                                                    userCache.setCashout(moneyCashout + valueSubtract);
                                                    userCache.setCashoutTime(new Date());
                                                    userMap.put(nickname, userCache);
                                                    String id = VinPlayUtils.genTransactionId(userCache.getId());
                                                    CashoutByBankMessage cashoutByBankMessage = new CashoutByBankMessage();
                                                    cashoutByBankMessage.setAccount(accountNumber);
                                                    cashoutByBankMessage.setAmount(moneyExchange);
                                                    cashoutByBankMessage.setBank(bank);
                                                    cashoutByBankMessage.setDesc(name + " rút " + moneyExchange + "về STK: " + accountNumber + ", Ngân hàng :" + bank);
                                                    cashoutByBankMessage.setName(name);
                                                    cashoutByBankMessage.setNickname(nickname);
                                                    cashoutByBankMessage.setReferenceId(id);
                                                    cashoutByBankMessage.setStatus(30);
                                                    DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                                                    Date today = Calendar.getInstance().getTime();
                                                    String createdDate = df.format(today);
                                                    cashoutByBankMessage.setCreateTime(createdDate);
                                                    cashoutDao.requestCashoutByBank(cashoutByBankMessage);
                                                    MoneyMessageInMinigame messageMoney = new MoneyMessageInMinigame(VinPlayUtils.genMessageId(), userCache.getId(), nickname, "CashOutByBank", moneyUser, currentMoney, (long) moneyExchange, "vin", 0L, 0, 0);
                                                    LogMoneyUserMessage messageLog = new LogMoneyUserMessage(userCache.getId(), nickname, "CashOutByBank", "Bank", currentMoney, (long) (-moneyExchange), "vin", "Cashout bank", 0L, false, userCache.isBot());
                                                    RMQApi.publishMessagePayment(messageMoney, 16);
                                                    RMQApi.publishMessageLogMoney(messageLog);
                                                    CashoutDaoImpl cashDao = new CashoutDaoImpl();
                                                    cashDao.updateSystemCashout((long) moneyExchange);

                                                    try (CloseableHttpClient httpClient = HttpClientBuilder.create().build();) {
                                                        String url = "";
                                                        if ("XXENG".equals(PartnerConfig.Client)) {
                                                            url = PartnerConfig.HostBot;
                                                        } else if ("R68".equals(PartnerConfig.Client)) {
                                                            url = PartnerConfig.HostBot;
                                                        } else {
                                                            url = PartnerConfig.HostBot;
                                                        }

                                                        HttpPost httpRequest = new HttpPost(url + "/rpadmin/ozawasecret1243/14");
                                                        httpRequest.addHeader("Content-Type", "application/json");
                                                        JSONObject objSend = new JSONObject();
                                                        String serverName = "xxeng";
                                                        UserModel senderModel = userService.getUserByNickName(nickname);
                                                        if (senderModel != null && senderModel.getClient() != null && !senderModel.getClient().equals("")) {
                                                            if (senderModel.getClient().equals("M")) {
                                                                serverName = "manVip";
                                                            } else if (senderModel.getClient().equals("R")) {
                                                                serverName = "r99";
                                                            } else if (senderModel.getClient().equals("V")) {
                                                                serverName = "Vip52";
                                                            }
                                                        }

                                                        objSend.put("sender_nick_name", nickname);
                                                        objSend.put("receiver_nick_name", "Admin");
                                                        objSend.put("money", valueSubtract);
                                                        objSend.put("description", nickname + " rút ngân hàng mệnh giá " + valueSubtract);
                                                        objSend.put("serverName", serverName);
                                                        objSend.put("type", "BANK");
                                                        long date = System.currentTimeMillis();
                                                        int offset = TimeZone.getDefault().getOffset(date);
                                                        objSend.put("created_time", date + (long) offset);
                                                        if (valueSubtract > 500000) {
                                                            objSend.put("need_check", true);
                                                            QuotaResponse checkQuota = this.CheckQuota(nickname, true);
                                                            objSend.put("total_angecy_receive", checkQuota.getTotal_agency_receive());
                                                            objSend.put("total_agency_transfer", checkQuota.getTotal_agency_transfer());
                                                            objSend.put("total_bet_money", checkQuota.getTotal_bet_money());
                                                            objSend.put("total_giftcode_money", checkQuota.getTotal_giftcode_money());
                                                            objSend.put("total_recharge_money", checkQuota.getTotal_recharge_card_money());
                                                            objSend.put("total_user_receive", checkQuota.getTotal_user_receive());
                                                            objSend.put("total_user_transfer", checkQuota.getTotal_user_transfer());
                                                            objSend.put("total_win_money", checkQuota.getTotal_win_money());
                                                        }

                                                        StringEntity requestEntity = new StringEntity(objSend.toString(), "UTF-8");
                                                        httpRequest.setEntity(requestEntity);
                                                        try (CloseableHttpResponse httpResponse = httpClient.execute(httpRequest);
                                                             InputStreamReader inputStreamReader = new InputStreamReader(httpResponse.getEntity().getContent());
                                                             BufferedReader rd = new BufferedReader(inputStreamReader);
                                                        ) {
                                                            StringBuffer result = new StringBuffer();
                                                            String line = "";

                                                            while ((line = rd.readLine()) != null) {
                                                                result.append(line);
                                                            }
                                                        }

                                                    } catch (Exception var55) {
                                                        var55.printStackTrace();
                                                    }

                                                    resp = new CashoutResponse(0, userCache.getVinTotal());
                                                    var65 = resp.toJson();
                                                } catch (Exception var56) {
                                                    StringWriter sw = new StringWriter();
                                                    PrintWriter pw = new PrintWriter(sw);
                                                    var56.printStackTrace(pw);
                                                    String sStackTrace = sw.toString();
                                                    logger.debug(var56.getMessage());
                                                    logger.debug(sStackTrace);
                                                    return resp.toJson();
                                                } finally {
                                                    userMap.unlock(nickname);
                                                }

                                                return var65;
                                            }
                                        }
                                    }
                                } else {
                                    logger.debug("ERROR: So du ko du");
                                    resp = new CashoutResponse(-10, 0L);
                                    return resp.toJson();
                                }
                            }
                        } else {
                            logger.debug("ERROR: Du Lieu Khong Hop Le");
                            resp = new CashoutResponse(400, 0L);
                            return resp.toJson();
                        }
                    } else {
                        resp = new CashoutResponse(-4, 0L);
                        return resp.toJson();
                    }
                }
            }
        } catch (Exception var58) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            var58.printStackTrace(pw);
            String sStackTrace = sw.toString();
            logger.debug(var58.getMessage());
            logger.debug(sStackTrace);
            return resp.toJson();
        }
    }

    private QuotaResponse CheckQuota(String nick_name, boolean seven_days) {
        QuotaResponse response = new QuotaResponse();

        try {
            UserService userService = new UserServiceImpl();
            UserModel userModel = userService.getUserByNickName(nick_name);
            if (userModel != null) {
                if (userModel.getDaily() == 1 || userModel.getDaily() == 2) {
                    response.setCode(0);
                    return response;
                }

                LogMoneyUserDao logService = new LogMoneyUserDaoImpl();
                long total_giftcode_money = 0L;
                long total_user_receive = 0L;
                long total_agency_receive = 0L;
                long total_user_transfer = 0L;
                long total_agency_transfer = 0L;
                AgentServiceImpl service = new AgentServiceImpl();
                List<AgentResponse> agents = service.listAgent();
                ArrayList<String> agentNames = new ArrayList();
                if (agents != null && agents.size() > 0) {
                    Iterator var20 = agents.iterator();

                    while (var20.hasNext()) {
                        AgentResponse agent = (AgentResponse) var20.next();
                        agentNames.add(agent.nickName);
                    }
                }

                List<LogUserMoneyResponse> resultGiftCode = logService.searchAllLogMoneyUser(nick_name, "GIFTCODE", seven_days);
                if (resultGiftCode != null && resultGiftCode.size() > 0) {
                    total_giftcode_money = (Long) resultGiftCode.stream().map((transx) -> {
                        return transx.moneyExchange;
                    }).reduce(total_giftcode_money, (accumulator, _item) -> {
                        return accumulator + _item;
                    });
                }

                List<LogUserMoneyResponse> resulReceive = logService.searchAllLogMoneyUser(nick_name, "RECEIVE", seven_days);
                if (resulReceive != null && resulReceive.size() > 0) {
                    Iterator var22 = resulReceive.iterator();

                    while (var22.hasNext()) {
                        LogUserMoneyResponse trans = (LogUserMoneyResponse) var22.next();
                        boolean matchAgent = false;
                        Iterator var25 = agentNames.iterator();

                        while (var25.hasNext()) {
                            String s = (String) var25.next();
                            if (trans.description.contains(s)) {
                                matchAgent = true;
                            }
                        }

                        if (matchAgent) {
                            total_agency_receive += trans.moneyExchange;
                        } else {
                            total_user_receive += trans.moneyExchange;
                        }
                    }
                }

                List<LogUserMoneyResponse> resulTransfer = logService.searchAllLogMoneyUser(nick_name, "TRANSFER", seven_days);
                if (resulTransfer != null && resulTransfer.size() > 0) {
                    Iterator var44 = resulTransfer.iterator();

                    while (var44.hasNext()) {
                        LogUserMoneyResponse trans = (LogUserMoneyResponse) var44.next();
                        boolean matchAgent = false;
                        Iterator var49 = agentNames.iterator();

                        while (var49.hasNext()) {
                            String s = (String) var49.next();
                            if (trans.description.contains(s)) {
                                matchAgent = true;
                            }
                        }

                        if (matchAgent) {
                            total_agency_transfer += trans.moneyExchange;
                        } else {
                            total_user_transfer += trans.moneyExchange;
                        }
                    }
                }

                long total_recharge_card_money = 0L;
                List<LogUserMoneyResponse> resultCard = logService.searchAllLogMoneyUser(nick_name, "CARD", seven_days);
                if (resultCard != null && resultCard.size() > 0) {
                    total_recharge_card_money = (Long) resultCard.stream().map((transx) -> {
                        return transx.moneyExchange;
                    }).reduce(total_recharge_card_money, (accumulator, _item) -> {
                        return accumulator + _item;
                    });
                }

                long quota = 0L;
                quota += total_recharge_card_money;
                quota += total_agency_receive;
                quota += total_user_receive * 4L;
                quota += total_giftcode_money * 4L;
                quota += total_agency_transfer;
                quota += total_user_transfer;
                long total_bet_money = 0L - logService.getTotalBetWin(nick_name, "BET", (String) null);
                long total_win_money = logService.getTotalBetWin(nick_name, "WIN", (String) null);
                long total_bet_banca_money = 0L - logService.getTotalBetWin(nick_name, "BET", "HamCaMap");
                long total_bet_slot_money = 0L - logService.getTotalBetWin(nick_name, "BET", "SLOT");
                long finalQuota = total_bet_money - total_bet_banca_money * 9L / 10L - total_bet_slot_money * 9L / 10L;
                long manual_quota = userModel.getManual_quota();
                finalQuota += manual_quota;
                if (finalQuota > quota) {
                    response.setCode(0);
                } else {
                    response.setCode(1);
                }

                response.setTotal_agency_receive(total_agency_receive);
                response.setTotal_agency_transfer(total_agency_transfer);
                response.setTotal_bet_money(total_bet_money);
                response.setTotal_giftcode_money(total_giftcode_money);
                response.setTotal_recharge_card_money(total_recharge_card_money);
                response.setTotal_user_receive(total_user_receive);
                response.setTotal_user_transfer(total_user_transfer);
                response.setTotal_win_money(total_win_money);
            }
        } catch (Exception var40) {
            response.setCode(-1);
            var40.printStackTrace();
        }

        return response;
    }
}
