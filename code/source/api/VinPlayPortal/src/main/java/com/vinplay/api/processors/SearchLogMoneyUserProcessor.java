package com.vinplay.api.processors;


import com.hazelcast.core.IMap;
import com.vinplay.api.processors.response.LogMoneyResponseNew;
import com.vinplay.dal.service.impl.LogMoneyUserServiceImpl;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.response.LogUserMoneyResponse;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;

public class SearchLogMoneyUserProcessor implements BaseProcessor<HttpServletRequest, String> {
    private static final Logger logger = Logger.getLogger("api");

    public SearchLogMoneyUserProcessor() {
    }

    public String execute(Param<HttpServletRequest> param) {
        LogMoneyResponseNew response = new LogMoneyResponseNew(false, "1001");
        HttpServletRequest request = (HttpServletRequest)param.get();
        String token = request.getParameter("t");
        String nickName = request.getParameter("nn");
        String userName = request.getParameter("un");
        String timestart = request.getParameter("ts");
        String timeend = request.getParameter("te");
        String moneyType = request.getParameter("mt");
        String actionName = request.getParameter("ag");
        String serviceName = request.getParameter("sn");

        try {
            UserServiceImpl userService = new UserServiceImpl();
            UserModel userModel;
            if (userName != null && !"".equals(userName)) {
                userModel = userService.getUserByUserName(userName);
            } else {
                if (nickName == null || "".equals(nickName)) {
                    return response.toJson();
                }

                userModel = userService.getUserByNickName(nickName);
            }

            if (userModel == null) {
                return response.toJson();
            } else {
                IMap userMap = HazelcastClientFactory.getInstance().getMap("users");
                if (!userMap.containsKey(userModel.getNickname())) {
                    return response.toJson();
                } else {
                    UserCacheModel userCache = (UserCacheModel)userMap.get(userModel.getNickname());
                    if (userCache != null) {
                        if (!userCache.getAccessToken().equals(token)) {
                            response.setErrorCode("400");
                            return response.toJson();
                        } else {
                            int page = 1;
                            int like = 0;
                            int totalrecord = 50;

                            try {
                                if (request.getParameter("p") != null) {
                                    page = Integer.parseInt(request.getParameter("p"));
                                }

                                if (request.getParameter("lk") != null) {
                                    like = Integer.parseInt(request.getParameter("lk"));
                                }

                                if (request.getParameter("tr") != null) {
                                    totalrecord = Integer.parseInt(request.getParameter("tr"));
                                }
                            } catch (Exception var23) {
                            }

                            LogMoneyUserServiceImpl service = new LogMoneyUserServiceImpl();

                            try {
                                List<LogUserMoneyResponse> trans = service.searchLogMoneyUser(userModel.getNickname(), (String)null, moneyType, serviceName, actionName, timestart, timeend, page, like, totalrecord);
                                if (trans != null && trans.size() > 0) {
                                    trans.forEach((tran) -> {
                                        try {
                                            String name;
                                            if (tran.description.toLowerCase().contains("chuyá»ƒn")) {
                                                tran.sender_nick_name = tran.nickName;
                                                name = tran.description.replace("Chuyá»ƒn tá»›i", "").trim();
                                                name = name.substring(11, name.indexOf(":"));
                                                tran.receiver_nick_name = name;
                                                tran.action = "TRANSFER";
                                            } else {
                                                tran.receiver_nick_name = tran.nickName;
                                                name = tran.description.replace("Nháº\u00adn tá»«", "").trim();
                                                name = name.substring(8, name.indexOf(":"));
                                                tran.sender_nick_name = name;
                                                tran.action = "RECEIVE";
                                            }
                                        } catch (Exception var2) {
                                        }

                                    });
                                }

                                int totalPages = service.countsearchLogMoneyUser(userModel.getNickname(), moneyType, serviceName, actionName, timestart, timeend, like);
                                response.setTotalPages(totalPages);
                                response.setTransactions(trans);
                                response.setSuccess(true);
                                response.setErrorCode("0");
                            } catch (Exception var22) {
                                var22.printStackTrace();
                                logger.debug(var22);
                            }

                            return response.toJson();
                        }
                    } else {
                        return response.toJson();
                    }
                }
            }
        } catch (Exception var24) {
            logger.debug(var24);
            return response.toJson();
        }
    }
}

