package com.vinplay.api.processors;

import casio.king365.util.KingUtil;
import com.vinplay.usercore.service.impl.MailBoxServiceImpl;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.response.ListMailBoxResponse;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class ListMailBoxNewProcessor
        implements BaseProcessor<HttpServletRequest, String> {


    public int getShouldWarningProcessingTimeInMillisecond() {
        return 1000;
    }

    public String execute(Param<HttpServletRequest> param) {
        ListMailBoxResponse response = new ListMailBoxResponse(false, "1001");
        HttpServletRequest request = (HttpServletRequest) param.get();
        String accessToken = request.getParameter("at");
        UserServiceImpl userSer = new UserServiceImpl();
        String nickName = request.getParameter("nn");
        if (userSer.checkAccesstoken(nickName, accessToken)) {
            int page = Integer.parseInt(request.getParameter("p"));
            int total = 0;
            int totalPages = 0;
            if (page < 0) {
                return response.toJson();
            }
            MailBoxServiceImpl service = new MailBoxServiceImpl();
            try {
                List trans = service.listMailBox(nickName, page);
                int mailnotread = service.countMailBoxInActive(nickName);
                if (trans.size() > 0) {
                    total = service.countMailBox(nickName);
                    totalPages = total % 5 == 0 ? total / 5 : total / 5 + 1;
                    response.setMailNotRead(mailnotread);
                    response.setTotalPages((long) totalPages);
                    response.setTransactions(trans);
                    response.setSuccess(true);
                    response.setErrorCode("0");
                } else {
                    response.setErrorCode("10001");
                }
            } catch (Exception e) {
                e.printStackTrace();
                KingUtil.printException("ListMailBoxNewProcessor Exception", e);
            }
        }
        return response.toJson();
    }
}
