package com.vinplay.api.processors;


import bitzero.util.common.business.Debug;
import com.vinplay.api.entities.QuotaResponse;
import com.vinplay.dal.dao.LogMoneyUserDao;
import com.vinplay.dal.dao.impl.LogMoneyUserDaoImpl;
import com.vinplay.dal.service.impl.AgentServiceImpl;
import com.vinplay.usercore.service.UserService;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.response.AgentResponse;
import com.vinplay.vbee.common.response.LogUserMoneyResponse;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CashoutMomoProcessor implements BaseProcessor<HttpServletRequest, String> {
    private static final Logger logger = Logger.getLogger("api");

    public CashoutMomoProcessor() {
    }

    public String execute(Param<HttpServletRequest> param) {
        /*CashoutResponse resp = new CashoutResponse(-1, 0L);
        HttpServletRequest request = (HttpServletRequest)param.get();
        String nickname = request.getParameter("nn");
        String name = request.getParameter("n");
        String token = request.getParameter("at");
        String phone = request.getParameter("p");
        String value = request.getParameter("mn");
        String otp = request.getParameter("otp");
        logger.info(nickname + ":" + name + ":" + token + ":" + value + ":" + phone);

        try {
            UserServiceImpl userService = new UserServiceImpl();
            UserModel userModel = userService.getUserByNickName(nickname);
            if (userModel != null) {
                IMap userMap = HazelcastClientFactory.getInstance().getMap("users");
                if (!userMap.containsKey(userModel.getNickname())) {
                    return resp.toJson();
                }

                UserCacheModel userCache = (UserCacheModel)userMap.get(userModel.getNickname());
                if (userCache != null && userCache.getAccessToken().equals(token)) {
                    CashoutDaoImpl cashoutDao = new CashoutDaoImpl();
                    if (GameCommon.getValueInt("IS_CASHOUT_MOMO") == 1) {
                        logger.debug("ERROR: Bao Tri");
                        resp = new CashoutResponse(-99, 0L);
                        return resp.toJson();
                    }

                    if (phone != null && value != null && name != null) {
                        int moneyExchange = Integer.parseInt(value);
                        List<Integer> values = Arrays.asList(20000, 50000, 100000, 200000, 300000, 500000, 1000000, 20000000);
                        if (!values.contains(moneyExchange)) {
                            logger.debug("ERROR: Menh gia ko dung");
                            resp = new CashoutResponse(-11, 0L);
                            return resp.toJson();
                        }

                        int valueSubtract = (int)((double)moneyExchange * GameCommon.getValueDouble("RATIO_CASHOUT_MOMO"));
                        if (userCache.getCurrentMoney("vin") < (long)valueSubtract) {
                            logger.debug("ERROR: So du ko du");
                            resp = new CashoutResponse(-10, 0L);
                            return resp.toJson();
                        }

                        try {
                            if (cashoutDao.getSystemCashout() > 6000000L) {
                                resp = new CashoutResponse(99, 0L);
                                return resp.toJson();
                            }
                        } catch (Exception var50) {
                            logger.debug("Loi Ket Noi DB");
                        }

                        HazelcastInstance client = HazelcastClientFactory.getInstance();
                        if (client == null) {
                            logger.debug("ERROR: Khong Ket Noi Duoc Hazel");
                            return resp.toJson();
                        }

                        long moneyUser = userCache.getVin();
                        long currentMoney = userCache.getVinTotal();
                        int LIMIT_EXCHANGE = 5;
                        long valueCheckCashout = userCache.getRechargeMoney() - (long)userCache.getCashout() - (long)valueSubtract;
                        if (valueCheckCashout < 0L && cashoutDao.countNumberExchangeInday(nickname) >= (long)LIMIT_EXCHANGE) {
                            logger.debug("ERROR: qua so lan doi trong ngay : " + LIMIT_EXCHANGE);
                            resp = new CashoutResponse(-12, 0L);
                            return resp.toJson();
                        }

                        OtpServiceImpl ser;
                        if (otp != null && !otp.equals("")) {
                            ser = new OtpServiceImpl();
                            if (ser.checkOtp(otp, userCache.getNickname(), "0", (String)null) != 0) {
                                resp = new CashoutResponse(11, userCache.getVinTotal());
                                return resp.toJson();
                            }

                            try {
                                String url;
                                try {
                                    userMap = client.getMap("users");
                                    userMap.lock(nickname);
                                    userCache = (UserCacheModel)userMap.get(nickname);
                                    moneyUser = userCache.getVin();
                                    currentMoney = userCache.getVinTotal();
                                    userCache.setVin(moneyUser - (long)valueSubtract);
                                    userCache.setVinTotal(currentMoney - (long)valueSubtract);
                                    int moneyCashout = userCache.getCashout();
                                    userCache.setCashout(moneyCashout + valueSubtract);
                                    userCache.setCashoutTime(new Date());
                                    userMap.put(nickname, userCache);
                                    String id = VinPlayUtils.genTransactionId(userCache.getId());
                                    cashoutDao.logCashoutByMomo(nickname, id, phone, name, moneyExchange, 30, "Chờ xác nhận", "ZoanMomo", 30, "Giao dịch chờ xác nhận");
                                    resp = new CashoutResponse(0, userCache.getVinTotal());

                                    try {
                                        HttpClient httpClient = HttpClientBuilder.create().build();
                                        url = "";
                                        if ("XXENG".equals(PartnerConfig.Client)) {
                                            url = PartnerConfig.HostBot;
                                        } else if ("R68".equals(PartnerConfig.Client)) {
                                            url = PartnerConfig.HostBot;
                                        } else {
                                            url = PartnerConfig.HostBot;
                                        }

                                        HttpPost httpRequest = new HttpPost(url + "/rpadmin/ozawasecret1243/14");
                                        httpRequest.addHeader("Content-Type", "application/json");
                                        JSONObject objSend = new JSONObject();
                                        String serverName = "xxeng";
                                        UserModel senderModel = userService.getUserByNickName(nickname);
                                        if (senderModel != null && senderModel.getClient() != null && !senderModel.getClient().equals("")) {
                                            if (senderModel.getClient().equals("M")) {
                                                serverName = "manVip";
                                            } else if (senderModel.getClient().equals("R")) {
                                                serverName = "r99";
                                            } else if (senderModel.getClient().equals("V")) {
                                                serverName = "Vip52";
                                            }
                                        }

                                        objSend.put("sender_nick_name", nickname);
                                        objSend.put("receiver_nick_name", "Admin");
                                        objSend.put("money", moneyExchange);
                                        objSend.put("description", nickname + " rút Momo mệnh giá " + moneyExchange);
                                        objSend.put("serverName", serverName);
                                        objSend.put("type", "MOMO");
                                        long date = System.currentTimeMillis();
                                        int offset = TimeZone.getDefault().getOffset(date);
                                        objSend.put("created_time", date + (long)offset);
                                        if (valueSubtract > 500000) {
                                            objSend.put("need_check", true);
                                            QuotaResponse checkQuota = this.CheckQuota(nickname, true);
                                            objSend.put("total_angecy_receive", checkQuota.getTotal_agency_receive());
                                            objSend.put("total_agency_transfer", checkQuota.getTotal_agency_transfer());
                                            objSend.put("total_bet_money", checkQuota.getTotal_bet_money());
                                            objSend.put("total_giftcode_money", checkQuota.getTotal_giftcode_money());
                                            objSend.put("total_recharge_money", checkQuota.getTotal_recharge_card_money());
                                            objSend.put("total_user_receive", checkQuota.getTotal_user_receive());
                                            objSend.put("total_user_transfer", checkQuota.getTotal_user_transfer());
                                            objSend.put("total_win_money", checkQuota.getTotal_win_money());
                                        }

                                        StringEntity requestEntity = new StringEntity(objSend.toString(), "UTF-8");
                                        httpRequest.setEntity(requestEntity);
                                        HttpResponse response = httpClient.execute(httpRequest);
                                        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                                        StringBuffer result = new StringBuffer();
                                        String line = "";

                                        while((line = rd.readLine()) != null) {
                                            result.append(line);
                                        }

                                        Debug.trace("Notify bot tele response: " + result);
                                    } catch (Exception var51) {
                                        Debug.trace("Notify error: " + var51);
                                    }

                                    return resp.toJson();
                                } catch (Exception var52) {
                                    StringWriter sw = new StringWriter();
                                    PrintWriter pw = new PrintWriter(sw);
                                    var52.printStackTrace(pw);
                                    url = sw.toString();
                                    logger.debug(var52.getMessage());
                                    logger.debug(url);
                                    return resp.toJson();
                                }
                            } finally {
                                userMap.unlock(nickname);
                            }
                        }

                        ser = new OtpServiceImpl();
                        ser.sendOtpEsms(userCache.getNickname(), userCache.getMobile());
                        resp = new CashoutResponse(0, userCache.getVinTotal());
                        return resp.toJson();
                    }

                    logger.debug("ERROR: Du Lieu Khong Hop Le");
                    resp = new CashoutResponse(400, 0L);
                    return resp.toJson();
                }
            }
        } catch (Exception var54) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            var54.printStackTrace(pw);
            String sStackTrace = sw.toString();
            logger.debug(var54.getMessage());
            logger.debug(sStackTrace);
        }

        return resp.toJson();

         */
        return "";
    }

    private QuotaResponse CheckQuota(String nick_name, boolean seven_days) {
        QuotaResponse response = new QuotaResponse();

        try {
            UserService userService = new UserServiceImpl();
            UserModel userModel = userService.getUserByNickName(nick_name);
            if (userModel != null) {
                if (userModel.getDaily() == 1 || userModel.getDaily() == 2) {
                    response.setCode(0);
                    return response;
                }

                LogMoneyUserDao logService = new LogMoneyUserDaoImpl();
                long total_giftcode_money = 0L;
                long total_user_receive = 0L;
                long total_agency_receive = 0L;
                long total_user_transfer = 0L;
                long total_agency_transfer = 0L;
                AgentServiceImpl service = new AgentServiceImpl();
                List<AgentResponse> agents = service.listAgent();
                ArrayList<String> agentNames = new ArrayList();
                if (agents != null && agents.size() > 0) {
                    Iterator var20 = agents.iterator();

                    while(var20.hasNext()) {
                        AgentResponse agent = (AgentResponse)var20.next();
                        agentNames.add(agent.nickName);
                    }
                }

                List<LogUserMoneyResponse> resultGiftCode = logService.searchAllLogMoneyUser(nick_name, "GIFTCODE", seven_days);
                if (resultGiftCode != null && resultGiftCode.size() > 0) {
                    total_giftcode_money = (Long)resultGiftCode.stream().map((transx) -> {
                        return transx.moneyExchange;
                    }).reduce(total_giftcode_money, (accumulator, _item) -> {
                        return accumulator + _item;
                    });
                }

                List<LogUserMoneyResponse> resulReceive = logService.searchAllLogMoneyUser(nick_name, "RECEIVE", seven_days);
                if (resulReceive != null && resulReceive.size() > 0) {
                    Iterator var22 = resulReceive.iterator();

                    while(var22.hasNext()) {
                        LogUserMoneyResponse trans = (LogUserMoneyResponse)var22.next();
                        boolean matchAgent = false;
                        Iterator var25 = agentNames.iterator();

                        while(var25.hasNext()) {
                            String s = (String)var25.next();
                            if (trans.description.contains(s)) {
                                matchAgent = true;
                            }
                        }

                        if (matchAgent) {
                            total_agency_receive += trans.moneyExchange;
                        } else {
                            total_user_receive += trans.moneyExchange;
                        }
                    }
                }

                List<LogUserMoneyResponse> resulTransfer = logService.searchAllLogMoneyUser(nick_name, "TRANSFER", seven_days);
                if (resulTransfer != null && resulTransfer.size() > 0) {
                    Iterator var44 = resulTransfer.iterator();

                    while(var44.hasNext()) {
                        LogUserMoneyResponse trans = (LogUserMoneyResponse)var44.next();
                        boolean matchAgent = false;
                        Iterator var49 = agentNames.iterator();

                        while(var49.hasNext()) {
                            String s = (String)var49.next();
                            if (trans.description.contains(s)) {
                                matchAgent = true;
                            }
                        }

                        if (matchAgent) {
                            total_agency_transfer += trans.moneyExchange;
                        } else {
                            total_user_transfer += trans.moneyExchange;
                        }
                    }
                }

                long total_recharge_card_money = 0L;
                List<LogUserMoneyResponse> resultCard = logService.searchAllLogMoneyUser(nick_name, "CARD", seven_days);
                if (resultCard != null && resultCard.size() > 0) {
                    total_recharge_card_money = (Long)resultCard.stream().map((transx) -> {
                        return transx.moneyExchange;
                    }).reduce(total_recharge_card_money, (accumulator, _item) -> {
                        return accumulator + _item;
                    });
                }

                long quota = 0L;
                quota += total_recharge_card_money;
                quota += total_agency_receive;
                quota += total_user_receive * 4L;
                quota += total_giftcode_money * 4L;
                quota += total_agency_transfer;
                quota += total_user_transfer;
                long total_bet_money = 0L - logService.getTotalBetWin(nick_name, "BET", (String)null);
                long total_win_money = logService.getTotalBetWin(nick_name, "WIN", (String)null);
                long total_bet_banca_money = 0L - logService.getTotalBetWin(nick_name, "BET", "HamCaMap");
                long total_bet_slot_money = 0L - logService.getTotalBetWin(nick_name, "BET", "SLOT");
                long finalQuota = total_bet_money - total_bet_banca_money * 9L / 10L - total_bet_slot_money * 9L / 10L;
                long manual_quota = userModel.getManual_quota();
                finalQuota += manual_quota;
                if (finalQuota > quota) {
                    response.setCode(0);
                } else {
                    response.setCode(1);
                }

                response.setTotal_agency_receive(total_agency_receive);
                response.setTotal_agency_transfer(total_agency_transfer);
                response.setTotal_bet_money(total_bet_money);
                response.setTotal_giftcode_money(total_giftcode_money);
                response.setTotal_recharge_card_money(total_recharge_card_money);
                response.setTotal_user_receive(total_user_receive);
                response.setTotal_user_transfer(total_user_transfer);
                response.setTotal_win_money(total_win_money);
            }
        } catch (Exception var40) {
            response.setCode(-1);
        }

        return response;
    }
}