package com.vinplay.api.processors;

import casio.king365.GU;
import casio.king365.util.KingUtil;
import com.hazelcast.core.IMap;
import com.vinplay.api.utils.PortalUtils;
import com.vinplay.api.utils.SocialUtils;
import com.vinplay.usercore.service.OtpService;
import com.vinplay.usercore.service.impl.OtpServiceImpl;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.usercore.utils.GameCommon;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.enums.StatusGames;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.models.SocialModel;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.response.LoginResponse;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class LoginProcessor
        implements BaseProcessor<HttpServletRequest, String> {
    private int warningProcessingTime = 1000;
    private int alertProcessingTime = 2000;

    public LoginProcessor() {
        warningProcessingTime = 1000;
        alertProcessingTime = 10_000;
    }

    public String execute(Param<HttpServletRequest> param) {
        HttpServletRequest request = param.get();
        String username = request.getParameter("un");
        String password = request.getParameter("pw");
        String social = request.getParameter("s");
        String accessToken = request.getParameter("at");
        String appId = request.getParameter("app_id") != null ? request.getParameter("app_id") : "";
        String pf = request.getParameter("pf") != null ? request.getParameter("pf") : "";
        request.getHeader("user-agent");
        KingUtil.printLog(("Request login: username: " + username + ", password: " + password + ", social: " + social + ", accessToken: " + accessToken + ", appId: " + appId + ", pf: " + pf));

        if ((username == null || password != null)
                && (social == null && accessToken == null)) {
            LoginResponse res = LoginResponse.fail("1001");
            GU.sendOperation(this.getClass().getName() + "Missing parameters.");
            return res.toJson();
        }

        if (pf.equals("banca_token")) {
            // Đây là khi service bắn cá gửi request login social
            // Khi đó giá trị social sẽ được gửi thông qua param username
            // Và giá trị token sẽ được gửi qua param password
            social = username;
            accessToken = password;
        }

        try {
            int statusGame = GameCommon.getValueInt((String) "STATUS_GAME");
            if (statusGame == StatusGames.MAINTAIN.getId()) {
                return LoginResponse.failed("1114").toJson();
            }
            UserServiceImpl userService = new UserServiceImpl();
            if (social != null && (social.equals("fb") || social.equals("gg") || social.equals("apple") || social.equals("dev"))) {
                String loginByThirdParty = loginByThirdParty(request, social, accessToken, statusGame, appId, pf);
                KingUtil.printLog("loginByThirdParty: " + loginByThirdParty);
                warningProcessingTime = 10000;
                return loginByThirdParty;
            }

            LoginResponse res = new LoginResponse(false, "1001");
            UserModel userModel2 = userService.getUserByUserName(username);
            if (userModel2 == null) {
                res.setErrorCode("1005");
                return res.toJson();
            }
            if (statusGame == StatusGames.SANDBOX.getId() && !userModel2.isCanLoginSandbox()) {
                res.setErrorCode("1114");
                KingUtil.printLog(("Response login: " + res.toJson()));
                return res.toJson();
            }
            if (userModel2.isBanLogin()) {
                res.setErrorCode("1109");
                return res.toJson();
            }
            if (userModel2.getPassword() == null) {
                res.setErrorCode("1007");
                return res.toJson();
            }
            if (!userModel2.getPassword().equals(password)) {
                res.setErrorCode("1007");
                return res.toJson();
            }
            if (userModel2.getNickname() != null && !userModel2.getNickname().trim().isEmpty()) {
                if (userModel2.isHasLoginSecurity()
                        && userModel2.getLoginOtp() >= 0L
                        && userModel2.getLoginOtp() <= userModel2.getVinTotal()) {
                    // send otp
                    OtpService otpService = new OtpServiceImpl();
                    int ret = otpService.sendOtpEsms(userModel2.getNickname(), "");
                    if (ret != 0) {
                        res.setErrorCode("116");
                        return res.toJson();
                    }
                    res.setErrorCode("1012");
                } else {
                    res = PortalUtils.loginSuccess(userModel2, appId, pf, PortalUtils.getIpAddress(request), PortalUtils.getUserAgent(request));
                }
            } else {
                res.setErrorCode("2001");
            }
            return res.toJson();
        } catch (Exception e1) {
            KingUtil.printException("LoginProcessor", e1);
            GU.sendOperation("Có lỗi khi đăng nhập LoginProcessor, Exception: " + KingUtil.printException(e1));
        }
        return LoginResponse.failed().toJson();
    }

    private String loginByThirdParty(HttpServletRequest request, String social, String accessToken, int statusGame, String appId, String pf) throws Exception {
        LoginResponse res = new LoginResponse(false, "1001");
        String cache = "";
        switch (social) {
            case "fb":
                cache = "cacheFacebook";
                break;
            case "apple":
                cache = "cacheApple";
                break;
            case "gg":
                cache = "cacheGoogle";
                break;
            case "dev":
                cache = "cacheDevice";
                break;
        }
        IMap<String, SocialModel> socialMap = HazelcastClientFactory.getInstance().getMap(cache);
        String socialId = SocialUtils.getSocialId(socialMap, accessToken, social);
        if (socialId == null) {
            KingUtil.printLog(("Response login: " + res.toJson()));
            return res.toJson();
        }
        if (socialId.isEmpty()) {
            res.setErrorCode("1009");
            KingUtil.printLog(("Response login: " + res.toJson()));
            return res.toJson();
        }
        UserServiceImpl userService = new UserServiceImpl();
        UserModel userModel = userService.getUserBySocialId(socialId, social);
        KingUtil.printLog("loginByThirdParty 1");
        if (userModel == null) {
            KingUtil.printLog("loginByThirdParty 2");
            // try to insert to db
            if (statusGame == StatusGames.SANDBOX.getId()) {
                res.setErrorCode("1114");
                return res.toJson();
            }
            try {
                if (userService.insertUserBySocial(socialId, social)) {
                    socialMap.put(socialId, new SocialModel(accessToken, socialId, new Date()), 24 * 60 * 60, TimeUnit.SECONDS);
                    /*String campaign = request.getParameter("utm_campaign");
                    String medium = request.getParameter("utm_medium");
                    String source = request.getParameter("utm_source");
                    if (campaign != null && medium != null && source != null) {
                        MarketingServiceImpl mktService = new MarketingServiceImpl();
                        UserMarketingMessage message = new UserMarketingMessage(userModel.getUsername(), "", 0, VinPlayUtils.getCurrentDateMarketing(), campaign, medium, source);
                        mktService.saveUserMarketing(message);
                        UserMakertingUtil.newRegisterUser((String) campaign, (String) medium, (String) source);
                    }*/
                    userModel = userService.getUserBySocialId(socialId, social);
                }
            } catch (SQLIntegrityConstraintViolationException e) {
                KingUtil.printException("LoginProcessor insertUserBySocial", e);
                return res.toJson();
            }
            KingUtil.printLog("loginByThirdParty 3");
        }
        KingUtil.printLog("loginByThirdParty 4");
        if (userModel == null) {
            KingUtil.printLog("loginByThirdParty 5");
            res.setErrorCode("2001");
            return res.toJson();
        }
        KingUtil.printLog("loginByThirdParty 6");
        if (statusGame == StatusGames.SANDBOX.getId() && !userModel.isCanLoginSandbox()) {
            res.setErrorCode("1114");
            KingUtil.printLog(("Response login: " + res.toJson()));
            return res.toJson();
        }
        if (userModel.isBanLogin()) {
            res.setErrorCode("1109");
            return res.toJson();
        }
        KingUtil.printLog("loginByThirdParty 7");
        if (social != null
                || (userModel.getNickname() != null && !userModel.getNickname().trim().isEmpty())) {
//            KingUtil.printLog("loginByThirdParty 8");
//            if (userModel.isHasLoginSecurity()
//                    && userModel.getLoginOtp() >= 0L
//                    && userModel.getLoginOtp() <= userModel.getVinTotal()) {
//                KingUtil.printLog("loginByThirdParty 8a");
//                // send otp
//                OtpService otpService = new OtpServiceImpl();
//                int ret = otpService.sendOtpEsms(userModel.getNickname(), "");
//                if (ret != 0) {
//                    res.setErrorCode("116");
//                    return res.toJson();
//                }
//                res.setErrorCode("1012");
//            } else {
            KingUtil.printLog("loginByThirdParty 8b");
            SocialUtils.socialSuccess(socialMap, socialId, accessToken);
            res = PortalUtils.loginSuccess(userModel, appId, pf, PortalUtils.getIpAddress(request), PortalUtils.getUserAgent(request));
//            }
        } else {
            KingUtil.printLog("loginByThirdParty 9");
            res.setErrorCode("2001");
            return res.toJson();
        }
        KingUtil.printLog("loginByThirdParty 10");
        return res.toJson();
    }

    public int getShouldWarningProcessingTimeInMillisecond() {
        return warningProcessingTime;
    }

    public int getShouldAlertProcessingTimeInMillisecond() {
        return alertProcessingTime;
    }

    public void resetWarningProcessingTime() {
        warningProcessingTime = 1000;
    }
}
