package com.vinplay.api.processors;


import com.vinplay.dichvuthe.dao.impl.RechargeDaoImpl;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.bson.Document;

public class CheckCardTransactionProcessor implements BaseProcessor<HttpServletRequest, String> {
    private static final Logger logger = Logger.getLogger("api");

    public CheckCardTransactionProcessor() {
    }

    public String execute(Param<HttpServletRequest> param) {
        HttpServletRequest request = (HttpServletRequest)param.get();
        String tid = request.getParameter("tid");
        RechargeDaoImpl dao = new RechargeDaoImpl();
        Document trans = dao.getRechargeByGachthe(tid);
        if (trans != null) {
            return "0".equals(trans.getInteger("code")) ? "{\"code\":0,\"message\":\"success\"}" : "{\"code\":" + trans.getInteger("code") + ",\"message\":\"failed\"}";
        } else {
            return "{\"code\":404,\"message\":\"error\"}";
        }
    }
}
