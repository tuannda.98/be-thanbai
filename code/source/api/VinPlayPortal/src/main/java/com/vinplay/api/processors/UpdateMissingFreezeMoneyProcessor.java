package com.vinplay.api.processors;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.vinplay.dal.service.impl.AgentServiceImpl;
import com.vinplay.usercore.dao.impl.MoneyInGameDaoImpl;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.models.FreezeModel;
import com.vinplay.vbee.common.response.ResultAgentRespone;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;

public class UpdateMissingFreezeMoneyProcessor implements BaseProcessor<HttpServletRequest, String> {
    private static final Logger logger = Logger.getLogger("api");

    public UpdateMissingFreezeMoneyProcessor() {
    }

    public String execute(Param<HttpServletRequest> param) {
        ResultAgentRespone response = new ResultAgentRespone(false, "1001");
        new AgentServiceImpl();
        HttpServletRequest request = (HttpServletRequest)param.get();
        String key = request.getParameter("k");

        try {
            if (key != null && "gamebaiasd123".equals(key)) {
                MoneyInGameDaoImpl dao = new MoneyInGameDaoImpl();
                List<FreezeModel> freezes = dao.getListFreezeMoneyNew();
                if (freezes != null && freezes.size() > 0) {
                    HazelcastInstance client = HazelcastClientFactory.getInstance();
                    IMap freezeMap = client.getMap("freeze");
                    Iterator var10 = freezes.iterator();

                    while(var10.hasNext()) {
                        FreezeModel model = (FreezeModel)var10.next();
                        if (!freezeMap.containsKey(model.getSessionId())) {
                            freezeMap.put(model.getSessionId(), model);
                        }
                    }
                }
            }
        } catch (Exception var12) {
            var12.printStackTrace();
            logger.debug(var12);
        }

        return response.toJson();
    }
}
