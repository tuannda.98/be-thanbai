/*
 * Decompiled with CFR 0.144.
 * 
 * Could not load the following classes:
 *  com.vinplay.usercore.service.impl.OtpServiceImpl
 *  com.vinplay.vbee.common.cp.BaseProcessor
 *  com.vinplay.vbee.common.cp.Param
 *  javax.servlet.http.HttpServletRequest
 *  org.apache.log4j.Logger
 */
package com.vinplay.api.processors.otp;

import casio.king365.core.HCMap;
import com.hazelcast.core.IMap;
import com.vinplay.usercore.dao.impl.UserDaoImpl;
import com.vinplay.usercore.service.impl.OtpServiceImpl;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import javax.servlet.http.HttpServletRequest;

import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import org.apache.log4j.Logger;

public class GetOtpProcessor
implements BaseProcessor<HttpServletRequest, String> {
    private static final Logger logger = Logger.getLogger((String)"api");

    public String execute(Param<HttpServletRequest> param) {
        HttpServletRequest request = (HttpServletRequest)param.get();
        String nickname = request.getParameter("nn");
        String type = request.getParameter("type");
        String mobile = request.getParameter("m");
        if (nickname != null) {
            OtpServiceImpl service = new OtpServiceImpl();
            try {
                IMap userMap = HCMap.getUsersMap();
                UserModel model;
                if (userMap.containsKey(nickname)) {
                    model = (UserModel)userMap.get(nickname);
                    // UserCacheModel userCacheModel = (UserCacheModel)model;
                } else {
                    UserDaoImpl userDao = new UserDaoImpl();
                    model = userDao.getUserByNickName(nickname);
                }

                if(model == null)
                    return "1";

                // return String.valueOf(service.getEsmsOTP(nickname, model.getMobile(), type));
                return String.valueOf(service.getEsmsOTP(nickname, model.getMobile(), type));
            }
            catch (Exception e) {
                logger.debug((Object)e);
            }
        }
        return "0";
    }
}

