package com.vinplay.api.processors.minigame;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import javax.servlet.http.HttpServletRequest;

public class GetTxBankProcessor implements BaseProcessor<HttpServletRequest, String> {
    public GetTxBankProcessor() {
    }

    public String execute(Param<HttpServletRequest> param) {
        HttpServletRequest request = (HttpServletRequest)param.get();
        byte type = Byte.parseByte(request.getParameter("type"));
        byte moneyType = 1;

        try {
            String keyHazel = type == 1 ? "txBank" : "txBank2";
            HazelcastInstance client = HazelcastClientFactory.getInstance();
            IMap bankMap = client.getMap(keyHazel);
            String key = keyHazel + ":" + moneyType;
            long bank = 0L;
            if (bankMap.containsKey(key)) {
                bank = (Long)bankMap.get(key);
            }

            return "" + bank;
        } catch (Exception var11) {
            return var11.getMessage();
        }
    }
}
