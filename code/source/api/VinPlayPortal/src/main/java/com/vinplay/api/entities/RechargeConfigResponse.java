package com.vinplay.api.entities;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;

public class RechargeConfigResponse {
    public int code;
    public ArrayList<Provider> providers;
    public int[] amounts;
    public double rate;

    public RechargeConfigResponse() {
    }

    public String toJson() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException var2) {
            return "{\"code\":500,\"message\":\"error\"}";
        }
    }
}
