/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.vinplay.usercore.service.impl.OtpServiceImpl
 *  com.vinplay.usercore.service.impl.UserServiceImpl
 *  com.vinplay.usercore.utils.GameCommon
 *  com.vinplay.usercore.utils.PartnerConfig
 *  com.vinplay.vbee.common.cp.BaseProcessor
 *  com.vinplay.vbee.common.cp.Param
 *  com.vinplay.vbee.common.models.UserModel
 *  com.vinplay.vbee.common.response.BaseResponseModel
 *  javax.servlet.http.HttpServletRequest
 *  org.apache.http.HttpEntity
 *  org.apache.http.HttpResponse
 *  org.apache.http.client.methods.HttpPost
 *  org.apache.http.client.methods.HttpUriRequest
 *  org.apache.http.entity.StringEntity
 *  org.apache.http.impl.client.CloseableHttpClient
 *  org.apache.http.impl.client.HttpClientBuilder
 *  org.apache.log4j.Logger
 *  org.json.JSONObject
 */
package king365.vinplay.api.utils;

import com.vinplay.usercore.service.impl.OtpServiceImpl;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.usercore.utils.GameCommon;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.response.BaseResponseModel;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class UpdateMoneyUserProcessor
implements BaseProcessor<HttpServletRequest, String> {
    private static final Logger logger = Logger.getLogger((String)"backend");

    public String execute(Param<HttpServletRequest> param) {
        BaseResponseModel response;
        block32: {
            HttpServletRequest request = (HttpServletRequest)param.get();
            response = BaseResponseModel.failed(false, "1001");
            try {
                String actionName = request.getParameter("ac");
                String nickname = request.getParameter("nn");
                long money = Long.parseLong(request.getParameter("mn"));
                String moneyType = request.getParameter("mt");
                String reason = request.getParameter("rs");
                String otp = request.getParameter("otp");
                String type = request.getParameter("type");
                String nicknameSend = request.getParameter("nns");
                logger.debug((Object)("Request UpdateMoneyUser: nickname: " + nickname + ", money: " + money + ", moneyType: " + moneyType + ", reason: " + reason + ", otp: " + otp + ", otpType: " + type));
                if (nickname == null || reason == null || reason.isEmpty() || money == 0L || moneyType == null || !moneyType.equals("vin") && !moneyType.equals("xu") || otp == null || type == null || !type.equals("1") && !type.equals("0")) break block32;
                OtpServiceImpl otpService = new OtpServiceImpl();
                int code = 3;
                logger.debug(" 1 code: "+code);
                logger.debug(" SUPER_ADMIN: "+GameCommon.getValueStr((String)"SUPER_ADMIN"));
                if (moneyType.equals("vin")) {
                    String[] arr = GameCommon.getValueStr((String)"SUPER_ADMIN").split(",");
                    int i = 0;
                    int length = arr.length;
                    for (int j = 0; j < length && (code = otpService.checkOdp(arr[j], otp)) != 0; ++j) {
                        if (i > 0 && money > 2000000L) {
                            return response.toJson();
                        }
                        ++i;
                    }
                    logger.debug("2 code: "+code);
                } else if (nicknameSend != null) {
                    String[] arr = GameCommon.getValueStr((String)"SUPER_ADMIN").split(",");
                    code = otpService.checkOdp(arr[0], otp);
                }
                logger.debug(" 3 code: "+code);
                if (code == 0) {
                    if (actionName == null) {
                        actionName = "Admin";
                    }
                    UserServiceImpl service = new UserServiceImpl();
                    response = service.updateMoneyFromAdmin(nickname, money, moneyType, actionName, "Admin", reason);
                } else if (code == 3) {
                    response.setErrorCode("1008");
                } else if (code == 4) {
                    response.setErrorCode("1021");
                }
                logger.debug((Object)("Code UpdateMoneyUser: " + code));
            }
            catch (Exception e) {
                logger.debug((Object)e);
            }
        }
        logger.debug((Object)("Response UpdateMoneyUser: " + response.toJson()));
        return response.toJson();
    }
}

