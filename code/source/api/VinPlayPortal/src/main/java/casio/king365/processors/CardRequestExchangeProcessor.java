/*
 * Decompiled with CFR 0.144.
 *
 * Could not load the following classes:
 *  com.hazelcast.core.IMap
 *  com.vinplay.usercore.service.impl.MarketingServiceImpl
 *  com.vinplay.usercore.service.impl.UserServiceImpl
 *  com.vinplay.usercore.utils.GameCommon
 *  com.vinplay.usercore.utils.UserMakertingUtil
 *  com.vinplay.vbee.common.cp.BaseProcessor
 *  com.vinplay.vbee.common.cp.Param
 *  com.vinplay.vbee.common.enums.StatusGames
 *  com.vinplay.vbee.common.hazelcast.HazelcastClientFactory
 *  com.vinplay.vbee.common.messages.UserMarketingMessage
 *  com.vinplay.vbee.common.models.SocialModel
 *  com.vinplay.vbee.common.models.UserModel
 *  com.vinplay.vbee.common.response.LoginResponse
 *  com.vinplay.vbee.common.utils.VinPlayUtils
 *  javax.servlet.http.HttpServletRequest
 *  org.apache.log4j.Logger
 */
package casio.king365.processors;

import casio.king365.Constants;
import casio.king365.GU;
import casio.king365.service.MongoDbService;
import casio.king365.service.impl.MongoDbServiceImpl;
import casio.king365.util.KingUtil;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.vinplay.dal.service.impl.CacheServiceImpl;
import com.vinplay.dichvuthe.dao.CashoutDao;
import com.vinplay.dichvuthe.dao.impl.CashoutDaoImpl;
import com.vinplay.dichvuthe.enums.WithdrawResponseCode;
import com.vinplay.usercore.service.UserService;
import com.vinplay.usercore.service.impl.GameConfigServiceImpl;
import com.vinplay.usercore.service.impl.UserExtraServiceImpl;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.messages.MoneyMessageInMinigame;
import com.vinplay.vbee.common.messages.dvt.CashoutByBankMessage;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.response.BaseResponseModel;
import com.vinplay.vbee.common.rmq.RMQApi;
import com.vinplay.vbee.common.utils.VinPlayUtils;
import org.json.JSONObject;
import vn.yotel.yoker.dao.CashoutItemDao;
import vn.yotel.yoker.dao.CashoutRequestDao;
import vn.yotel.yoker.dao.RequestCardDao;
import vn.yotel.yoker.dao.impl.CashoutItemDaoImpl;
import vn.yotel.yoker.dao.impl.CashoutRequestDaoImpl;
import vn.yotel.yoker.dao.impl.RequestCardDaoImpl;
import vn.yotel.yoker.domain.CashoutItem;
import vn.yotel.yoker.domain.CashoutRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CardRequestExchangeProcessor
        implements BaseProcessor<HttpServletRequest, String> {

    HazelcastInstance client;
    IMap<String, UserModel> userMap;
    UserService userService;
    CashoutItemDao cashoutItemDao;
    CashoutDao cashoutDao;
    CashoutRequestDao cashoutRequestDao;
    RequestCardDao requestCardDao;
    MongoDbService mongoDbService;
    GameConfigServiceImpl gameConfigServiceImpl;

    public String execute(Param<HttpServletRequest> param) {
        JSONObject returnObj = new JSONObject();
        try {
            init();

            returnObj.put("is_success", false);
            returnObj.put("desc", "Không xác định");

            CashoutItemDao cashoutItemDao = new CashoutItemDaoImpl();
            CashoutRequestDao cashoutRequestDao = new CashoutRequestDaoImpl();
            UserServiceImpl userService = new UserServiceImpl();
            CacheServiceImpl cacheService = new CacheServiceImpl();
            HttpServletRequest request = (HttpServletRequest) param.get();
            String username = request.getParameter("un") != null ? request.getParameter("un") : "";
            String nickname = request.getParameter("nn") != null ? request.getParameter("nn") : "";
            String requestIdStr = request.getParameter("request_id") != null ? request.getParameter("request_id") : "";
            String accountNo = request.getParameter("account_no") != null ? request.getParameter("account_no") : "";
            String accountName = request.getParameter("account_name") != null ? request.getParameter("account_name") : "";
            String requestAccNo = request.getParameter("request_acc_no") != null ? request.getParameter("request_acc_no") : "";
            String requestAccName = request.getParameter("request_acc_name") != null ? request.getParameter("request_acc_name") : "";
            String requestBankCode = request.getParameter("request_bank_code") != null ? request.getParameter("request_bank_code") : "";
            String CoinAddress = request.getParameter("coin_addr") != null ? request.getParameter("coin_addr") : "";
            String CoinToken = request.getParameter("coin_token") != null ? request.getParameter("coin_token") : "";
            String CoinTokenType = request.getParameter("coin_token_type") != null ? request.getParameter("coin_token_type") : "";
            KingUtil.printLog("CardRequestExchangeProcessor username: " + username + ", requestIdStr: " + requestIdStr + ", accountNo: " + accountNo
                    + ", accountName: " + accountName + ", requestAccNo: " + requestAccNo + ", requestAccName: " + requestAccName + ", requestBankCode: " + requestBankCode
                    + ", CoinAddress: " + CoinAddress + ", CoinToken: " + CoinToken + ", CoinTokenType: " + CoinTokenType);

            if (username.isEmpty() || requestIdStr.isEmpty()) {
                returnObj.put("desc", "Lỗi tham số");
                return returnObj.toString();
            }
            int requestId = -1;
            try {
                requestId = Integer.parseInt(requestIdStr);
            } catch (NumberFormatException e) {
                returnObj.put("desc", "Lỗi tham số");
                return returnObj.toString();
            }
            KingUtil.printLog("1");
            CashoutItem cashoutItem = cashoutItemDao.findById(requestId);
            if (cashoutItem.getType() == Constants.CashoutItemType.MOMO) {
                if (accountNo.equals("") || accountName.equals("")) {
                    returnObj.put("desc", "Lỗi tham số");
                    return returnObj.toString();
                }
            }
            KingUtil.printLog("2");
            if (cashoutItem.getType() == Constants.CashoutItemType.BANK) {
                if (requestAccNo.equals("") || requestAccName.equals("") || requestBankCode.equals("")) {
                    returnObj.put("desc", "Lỗi tham số");
                    return returnObj.toString();
                }
            }
            if (cashoutItem.getType() == Constants.CashoutItemType.COIN) {
                if (CoinAddress.equals("") || CoinToken.equals("") || CoinTokenType.equals("")) {
                    returnObj.put("desc", "Lỗi tham số");
                    return returnObj.toString();
                }
            }
            KingUtil.printLog("3");
            UserModel userModel = null;
            UserCacheModel user = null;
            try {
                if (!nickname.equals("")) {
                    userModel = userService.getUserByNickName(nickname);
                    user = userService.forceGetCachedUser(nickname);
                } else if (!username.equals("")) {
                    userModel = userService.getUserByUserName(username);
                    user = userService.forceGetCachedUser(userModel.getNickname());
                }
            } catch (Exception e) {
                e.printStackTrace();
                KingUtil.printException("CardRequestExchangeProcessor", e);
                returnObj.put("desc", "Lỗi.");
                GU.sendOperation("Có lỗi khi gửi yêu cầu rút, Exception: " + KingUtil.printException(e));
                return returnObj.toString();
            }
            KingUtil.printLog("4");

            if (user.isBanCashOut()) {
                KingUtil.printLogError("user is Band Cashout");
                returnObj.put("desc", "Lỗi, vui lòng liên hệ CSKH.");
                return returnObj.toString();
            }
            CashoutDaoImpl cashDao = new CashoutDaoImpl();

            // Trừ tiền
            Long vinTotal = user.getVinTotal();
            Long vin = user.getVin();
            if (Math.min(vin, vinTotal) < cashoutItem.getGold()) {
                if(Math.max(vin, vinTotal) >= cashoutItem.getGold()){
                    GU.sendOperation("Tài khoản "+user.getNickname()+" rút số tiền: "+cashoutItem.getGold()
                            +", nhưng bị từ chối do tiền ít hơn. vin: "+vin+", vin total: "+vinTotal);
                }
                KingUtil.printLog(("CardRequestExchangeProcessor, Số tiền của người dùng hiện không đủ vinTotal: " + vinTotal));
                returnObj.put("desc", "Số tiền không đủ.");
                return returnObj.toString();
            }

            // Chỉ thông báo nếu user đủ tiền rút
            if(cashoutItem.getMoney() >= 10000000){
                returnObj.put("desc", "Mệnh giá lựa chọn hiện chưa hổ trợ rút. Vui lòng lựa chọn mệnh giá khác.");
                return returnObj.toString();
            }
            KingUtil.printLog("5");
            // Trừ tiền
            BaseResponseModel subMoneyResult = userService.updateMoneyWithNotify(user.getNickname()
                    , cashoutItem.getGold().longValue() * -1, "cashout_card", "Hệ thống"
                    , "Trừ tiền rút " + cashoutItem.getName(), false);
            if (!subMoneyResult.isSuccess()) {
                returnObj.put("desc", "Lỗi tiền. Vui lòng thử lại sau.");
                KingUtil.printLog(("CardRequestExchangeProcessor, Lỗi tiền. Vui lòng thử lại sau subMoneyResult: " + subMoneyResult.toJson()));
                GU.sendOperation("Có lỗi tiền khi gửi yêu cầu rút, Exception: " + subMoneyResult.toJson());
                return returnObj.toString();
            }
            // Cập nhật lại object user sau khi đã có thay đổi value tiền
            user = userService.forceGetCachedUser(user.getNickname());
            KingUtil.printLog("6");
            CashoutRequest cashoutRequest = new CashoutRequest();
            cashoutRequest.setUserId(user.getId());
            cashoutRequest.setUserName(user.getNickname());
            cashoutRequest.setUserMobile(user.getMobile());
            cashoutRequest.setRequesTime(new Timestamp(System.currentTimeMillis()));
            cashoutRequest.setItemId(cashoutItem.getId());
            cashoutRequest.setQuantity(1);
            cashoutRequest.setClient(user.getClient());
            UserExtraServiceImpl userExtraService = new UserExtraServiceImpl();
            String platform = userExtraService.getPlatformFromNickname(user.getNickname());
            if(platform == null)
                platform = "web";
            if(platform.equals("android"))
                platform = "ad";
            KingUtil.printLog("platform: "+platform);
            cashoutRequest.setPlatform(platform);
            cashoutRequest.setStatus(Constants.CashoutRequestStatus.NEW);
            if (cashoutItem.getType() == Constants.CashoutItemType.MOMO) {
                JSONObject extraDataObj = new JSONObject();
                extraDataObj.put("account_no", accountNo);
                extraDataObj.put("account_name", accountName);
                cashoutRequest.setExtraData(extraDataObj.toString());
            }
            KingUtil.printLog("7");
            if (cashoutItem.getType() == Constants.CashoutItemType.BANK) {
                JSONObject extraDataObj = new JSONObject();
                extraDataObj.put("request_acc_no", requestAccNo);
                extraDataObj.put("request_acc_name", requestAccName);
                extraDataObj.put("request_bank_code", requestBankCode);
                cashoutRequest.setExtraData(extraDataObj.toString());
            }
            if (cashoutItem.getType() == Constants.CashoutItemType.COIN) {
                JSONObject extraDataObj = new JSONObject();
                extraDataObj.put("coin_addr", CoinAddress);
                extraDataObj.put("coin_token", CoinToken);
                extraDataObj.put("coin_token_type", CoinTokenType);
                cashoutRequest.setExtraData(extraDataObj.toString());
            }

            KingUtil.printLog("8");
            KingUtil.printLog("cashoutRequest: "+cashoutRequest+", user: "+user);
            Serializable resultSaveReqCard;
            Integer reqCardId = -1;
            resultSaveReqCard = cashoutRequestDao.save(cashoutRequest);
            try {
                reqCardId = (Integer) resultSaveReqCard;
            } catch (Exception e) {
                KingUtil.printLog("Exception: " + KingUtil.printException(e));
                KingUtil.printLog("fail cash resultSaveReqCard to Integer, try parseInt string");
                GU.sendOperation("Có lỗi khi gửi yêu cầu rút, Exception: " + KingUtil.printException(e));
                reqCardId = Integer.parseInt((String) resultSaveReqCard);
            }
            KingUtil.printLog("9");

            // Báo cáo như hệ thống cũ
            if (cashoutItem.getType() == Constants.CashoutItemType.BANK) {
                try {
                    // Trừ tiền, bắn notify, lên báo cáo
                    userMap.lock(user.getNickname());
                    UserCacheModel userCache = user;
                    long moneyUser = userCache.getVin();
                    long currentMoney = userCache.getVinTotal();
                    int moneyCashout = userCache.getCashout();
                    userCache.setCashout((int) (moneyCashout + cashoutItem.getMoney().longValue()));
                    userCache.setCashoutTime(new Date());
                    userMap.put(user.getNickname(), userCache);
                    // String id = VinPlayUtils.genTransactionId(userCache.getId());
                    String id = reqCardId + "";
                    CashoutByBankMessage cashoutByBankMessage = new CashoutByBankMessage();
                    cashoutByBankMessage.setAccount(requestAccNo);
                    cashoutByBankMessage.setAmount(cashoutItem.getMoney().intValue());
                    cashoutByBankMessage.setBank(requestBankCode);
                    cashoutByBankMessage.setDesc(user.getNickname() + " rút " + cashoutItem.getMoney().intValue() + " về STK: " + requestAccNo + ", Ngân hàng code :" + requestBankCode);
                    cashoutByBankMessage.setName(requestAccName);
                    cashoutByBankMessage.setNickname(user.getNickname());
                    cashoutByBankMessage.setReferenceId(id);
                    cashoutByBankMessage.setStatus(WithdrawResponseCode.PENDING.getCode());
                    DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                    Date today = Calendar.getInstance().getTime();
                    String createdDate = df.format(today);
                    cashoutByBankMessage.setCreateTime(createdDate);
                    KingUtil.printLog("CardRequestExchangeProcessor cashoutByBankMessage: "+cashoutByBankMessage.toString());
                    cashoutDao.requestCashoutByBank(cashoutByBankMessage);
                    MoneyMessageInMinigame messageMoney = new MoneyMessageInMinigame(VinPlayUtils.genMessageId(), userCache.getId()
                            , user.getNickname(), "CashOutByBank", moneyUser, currentMoney
                            , cashoutItem.getMoney().longValue(), "vin", 0L, 0, 0);
                    /*LogMoneyUserMessage messageLog = new LogMoneyUserMessage(userCache.getId(), user.getNickname(), "CashOutByBank"
                            , "Bank", currentMoney, cashoutItem.getMoney().longValue() * -1, "vin"
                            , "Cashout bank", 0L, false, userCache.isBot());
                    RMQApi.publishMessageLogMoney(messageLog);*/
                    RMQApi.publishMessagePayment(messageMoney, 16);
                    cashDao.updateSystemCashout(cashoutItem.getMoney().longValue());
                } catch (Exception e) {
                    e.printStackTrace();
                    KingUtil.printException("CardRequestExchangeProcessor BANK PROCESS", e);
                    GU.sendOperation("Có lỗi khi gửi yêu cầu rút, CardRequestExchangeProcessor BANK PROCESS Exception: " + KingUtil.printException(e));
                } finally {
                    userMap.unlock(user.getNickname());
                }
            }

            GU.sendCustomerService("Tài khoản: " + user.getNickname() + " gửi yêu rút "
                    + (cashoutItem.getType() == Constants.CashoutItemType.CARD ? "Thẻ" : "")
                    + (cashoutItem.getType() == Constants.CashoutItemType.MOMO ? "Momo" : "")
                    + (cashoutItem.getType() == Constants.CashoutItemType.BANK ? "Bank" : "")
                    + (cashoutItem.getType() == Constants.CashoutItemType.COIN ? "COIN" : "")
                    + ", với mệnh giá: " + cashoutItem.getMoney() + ". Xem xét phê duyệt tại: https://quanchi1.chaonammoi2021.xyz/admin/paygate/cash_out_card_approve");

            // Tự động phê duyệt
            long numberCashin = userService.getDbCashinMoneyAmount(user.getNickname());
            long numberCashout = userService.getDbCashoutMoneyAmount(user.getUsername());
            KingUtil.printLog("CardRequestExchangeProcessor nickname: "+user.getUsername()
                    +", numberCashin: "+numberCashin+", numberCashout: "+numberCashout
                    +", getRechargeMoney(): " +user.getRechargeMoney()+", getCashoutMoney(): "+user.getCashoutMoney());
            if(numberCashin > numberCashout && user.getRechargeMoney() > user.getCashoutMoney()){
                String actionNote = "Tài khoản: " + user.getNickname() +
                        " có số Vin nạp là: " + numberCashin +
                        ", có số Vin rút là: " + numberCashout +
                        " đủ điều kiện nên đã được TỰ ĐỘNG PHÊ DUYỆT RÚT cho yêu cầu rút này: "+reqCardId;
                GU.sendCustomerService(actionNote);
                // CardRequestActionProcessor.getInstance().approveCashoutRequest(reqCardId+"", "true", actionNote);
            }

            returnObj.put("is_success", true);
            returnObj.put("desc", "Thành công. Vui lòng đợi phê duyệt, account_name: " + accountName);
        } catch (Exception e) {
            e.printStackTrace();
            KingUtil.printException("CardRequestExchangeProcessor", e);
            GU.sendOperation("Có lỗi khi gửi yêu cầu rút, Exception: " + KingUtil.printException(e));
        }
        return returnObj.toString();
    }

    private boolean init() {
        if ((client = HazelcastClientFactory.getInstance()) == null) {
            KingUtil.printLogError("CardRequestActionProcessor - init(), Lỗi khởi tạo hazelcast");
        }
        userMap = client.getMap("users");
        if (cashoutItemDao == null)
            cashoutItemDao = new CashoutItemDaoImpl();
        if (cashoutRequestDao == null)
            cashoutRequestDao = new CashoutRequestDaoImpl();
        if (userService == null)
            userService = new UserServiceImpl();
        if (requestCardDao == null)
            requestCardDao = new RequestCardDaoImpl();
        if (mongoDbService == null)
            mongoDbService = new MongoDbServiceImpl();
        if (gameConfigServiceImpl == null)
            gameConfigServiceImpl = new GameConfigServiceImpl();
        if (cashoutDao == null)
            cashoutDao = new CashoutDaoImpl();
        return true;
    }
}

