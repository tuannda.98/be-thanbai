package casio.king365.processors;

import casio.king365.Constants;
import casio.king365.GU;
import casio.king365.util.KingUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vinplay.dichvuthe.dao.impl.CashoutDaoImpl;
import com.vinplay.dichvuthe.enums.WithdrawResponseCode;
import com.vinplay.usercore.service.MailBoxService;
import com.vinplay.usercore.service.UserService;
import com.vinplay.usercore.service.impl.MailBoxServiceImpl;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.messages.LogMoneyUserMessage;
import com.vinplay.vbee.common.messages.MoneyMessageInMinigame;
import com.vinplay.vbee.common.messages.dvt.CashoutByBankMessage;
import com.vinplay.vbee.common.messages.dvt.CashoutByCardMessage;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.response.BaseResponseModel;
import com.vinplay.vbee.common.rmq.RMQApi;
import com.vinplay.vbee.common.utils.VinPlayUtils;
import org.bson.Document;
import org.json.JSONException;
import org.json.JSONObject;
import vn.yotel.payment.buycard.BuyCardClient;
import vn.yotel.payment.buycard.api.RSAUtil;
import vn.yotel.payment.buycard.entities.CardRes;
import vn.yotel.yoker.dao.CashoutItemDao;
import vn.yotel.yoker.dao.CashoutRequestDao;
import vn.yotel.yoker.dao.impl.CashoutItemDaoImpl;
import vn.yotel.yoker.dao.impl.CashoutRequestDaoImpl;
import vn.yotel.yoker.domain.CashoutItem;
import vn.yotel.yoker.domain.CashoutRequest;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Processor này chỉ được dùng ở backend
 */
public class CardRequestActionProcessor
        implements BaseProcessor<HttpServletRequest, String> {

    public static CardRequestActionProcessor self;
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    UserService userService;
    CashoutItemDao cashoutItemDao;
    CashoutRequestDao cashoutRequestDao;
    MailBoxService mailBoxService;
    CashoutDaoImpl cashoutDao;
    BuyCardClient buyCardClient = BuyCardClient.getInstance();

    public String execute(Param<HttpServletRequest> param) {
        if (!init())
            return "{\"is_success\":false,\"desc\":\"Lỗi hệ thống\"}";
        HttpServletRequest request = (HttpServletRequest) param.get();
        String action = request.getParameter("action") != null ? request.getParameter("action") : "";
        String page = request.getParameter("page") != null ? request.getParameter("page") : "";
        String numPerPage = request.getParameter("num_per_page") != null ? request.getParameter("num_per_page") : "";
        if (action.isEmpty()) {
            return "{}";
        }

        String resultProcessed = "{}";
        try {
            switch (action) {
                case "list_cashout_item":
                    resultProcessed = getListCashoutItem();
                    break;
                case "list_cashout_request":
                    resultProcessed = getListCashoutRequest(Integer.parseInt(page)
                            , Integer.parseInt(numPerPage));
                    break;
                case "approve_cashout_request":
                    String cashoutRequestIdStr = request.getParameter("cashout_request_id") != null ? request.getParameter("cashout_request_id") : "";
                    String isAcceptStr = request.getParameter("is_accept") != null ? request.getParameter("is_accept") : "";
                    String actionNote = request.getParameter("action_note") != null ? request.getParameter("action_note") : "";
                    resultProcessed = approveCashoutRequest(cashoutRequestIdStr, isAcceptStr, actionNote);
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        KingUtil.printLog("CardRequestActionProcessor - action: " + action + " respond " + resultProcessed);
        return resultProcessed;
    }

    @Override
    public int getShouldWarningProcessingTimeInMillisecond() {
        return 10000;
    }

    @Override
    public int getShouldAlertProcessingTimeInMillisecond() {
        return 10000;
    }

    public boolean init() {
        if (cashoutItemDao == null)
            cashoutItemDao = new CashoutItemDaoImpl();
        if (cashoutRequestDao == null)
            cashoutRequestDao = new CashoutRequestDaoImpl();
        if (userService == null)
            userService = new UserServiceImpl();
        if (mailBoxService == null)
            mailBoxService = new MailBoxServiceImpl();
        if (cashoutDao == null)
            cashoutDao = new CashoutDaoImpl();
        return true;
    }

    public static CardRequestActionProcessor getInstance() {
        if (self == null) {
            self = new CardRequestActionProcessor();
            self.init();
        }
        return self;
    }

    public String approveCashoutRequest(String cashoutRequestIdStr, String isAcceptStr, String actionNote) throws JSONException {
        KingUtil.printLog("CardRequestActionProcessor - approveCashoutRequest()" +
                ", cashoutRequestIdStr: " + cashoutRequestIdStr + ", isAcceptStr: " + isAcceptStr + ", actionNote: " + actionNote);
        JSONObject returnObj = new JSONObject();
        returnObj.put("is_success", false);
        returnObj.put("desc", "Lỗi không xác định");
        if (cashoutRequestIdStr.isEmpty() || isAcceptStr.isEmpty()) {
            KingUtil.printLog("CardRequestActionProcessor - approveCashoutRequest(), Sai tham số");
            returnObj.put("desc", "Sai tham số");
            return returnObj.toString();
        }

        if (cashoutRequestDao == null)
            KingUtil.printLog("cashoutRequestDao == null");
        else KingUtil.printLog("cashoutRequestDao !== null");
        KingUtil.printLog("parrse cashoutRequestIdStr: " + Integer.parseInt(cashoutRequestIdStr));
        CashoutRequest cashoutRequest = cashoutRequestDao.findById(Integer.parseInt(cashoutRequestIdStr));
        if (cashoutRequest == null) {
            KingUtil.printLog("CardRequestActionProcessor - approveCashoutRequest(), Không tìm thấy dữ liệu cashoutRequest");
            returnObj.put("desc", "Không tìm thấy dữ liệu.");
            return returnObj.toString();
        }

        if (cashoutRequest.getStatus() != Constants.CashoutRequestStatus.NEW) {
            KingUtil.printLog("extraDataObj: - approveCashoutRequest(), cashoutRequest.getStatus() != Constants.CashoutRequestStatus.NEW");
            returnObj.put("desc", "Yêu cầu rút này đã được xử lí. Không thể sửa lại.");
            return returnObj.toString();
        }

        CashoutItem cashoutItem = cashoutItemDao.findById(cashoutRequest.getItemId());
        if (cashoutItem == null) {
            KingUtil.printLog("CardRequestActionProcessor - approveCashoutRequest(), Không tìm thấy dữ liệu item");
            returnObj.put("desc", "Không tìm thấy dữ liệu item.");
            return returnObj.toString();
        }
        // Hiện tại cashoutRequest username đã được chuyển thành lưu nickname
        UserCacheModel user = userService.getCacheUserIgnoreCase(cashoutRequest.getUserName());
        if (user == null) {
            KingUtil.printLog("CardRequestActionProcessor - approveCashoutRequest(), Không tìm thấy dữ liệu người dùng user");
            returnObj.put("desc", "Không tìm thấy thông tin người dùng.");
            return returnObj.toString();
        }

        cashoutRequest.setProcessTime(new Timestamp(System.currentTimeMillis()));
        cashoutRequest.setActionNote(actionNote);
        Boolean isAccept = Boolean.parseBoolean(isAcceptStr);
        if (!isAccept) {
            cashoutRequest.setStatus(Constants.CashoutRequestStatus.REJECT);
            cashoutRequestDao.update(cashoutRequest);
            returnObj.put("is_success", true);
            returnObj.put("desc", "Từ chối thành công.");
            KingUtil.printLog("CardRequestActionProcessor - approveCashoutRequest(), Từ chối thành công");
            GU.sendCustomerService("Phê duyệt rút: "
                    + "TỪ CHỐI"
                    + " với id yêu cầu: " + cashoutRequestIdStr + ", của tài khoản: " + user.getNickname() + ", rút "
                    + (cashoutItem.getType() == Constants.CashoutItemType.CARD ? "Thẻ" : "")
                    + (cashoutItem.getType() == Constants.CashoutItemType.MOMO ? "Momo" : "")
                    + (cashoutItem.getType() == Constants.CashoutItemType.BANK ? "Bank" : "")
                    + ", mệnh giá: " + cashoutItem.getMoney());
            return returnObj.toString();
        }

        try {
            if (cashoutItem.getType() == Constants.CashoutItemType.CARD) {
                approveCashoutCard(cashoutRequest, cashoutItem, user);
            } else if (cashoutItem.getType() == Constants.CashoutItemType.MOMO) {
                approveCashoutMomo(cashoutRequest, cashoutItem, user);
            } else if (cashoutItem.getType() == Constants.CashoutItemType.BANK) {
                approveCashoutBank(cashoutRequest, cashoutItem, user);
            }

            GU.sendCustomerService("Phê duyệt rút: "
                    + (isAccept ? "ĐỒNG Ý" : "TỪ CHỐI")
                    + " với id yêu cầu: " + cashoutRequestIdStr + ", của tài khoản: " + user.getNickname() + ", rút "
                    + (cashoutItem.getType() == Constants.CashoutItemType.CARD ? "Thẻ" : "")
                    + (cashoutItem.getType() == Constants.CashoutItemType.MOMO ? "Momo" : "")
                    + (cashoutItem.getType() == Constants.CashoutItemType.BANK ? "Bank" : "")
                    + ", mệnh giá: " + cashoutItem.getMoney());

            returnObj.put("is_success", true);
            returnObj.put("desc", "Thành công.");
            return returnObj.toString();
        } catch (Exception e) {
            GU.sendCustomerService("[CẢNH BÁO]Phê duyệt rút GẶP LỖI: "
                    + " với id yêu cầu: " + cashoutRequestIdStr + ", của tài khoản: " + user.getNickname() + ", rút "
                    + (cashoutItem.getType() == Constants.CashoutItemType.CARD ? "Thẻ" : "")
                    + (cashoutItem.getType() == Constants.CashoutItemType.MOMO ? "Momo" : "")
                    + (cashoutItem.getType() == Constants.CashoutItemType.BANK ? "Bank" : "")
                    + ", mệnh giá: " + cashoutItem.getMoney());
            KingUtil.printException("CardRequestActionProcessor", e);
            GU.sendOperation("Có lỗi khi phê duyệt rút, Exception: " + KingUtil.printException(e));
            cashoutRequest.setStatus(Constants.CashoutRequestStatus.ERROR);
            cashoutRequestDao.update(cashoutRequest);
            returnObj.put("desc", e.getMessage());
            return returnObj.toString();
        }
    }

    private boolean approveCashoutCard(CashoutRequest cashoutRequest, CashoutItem cashoutItem, UserCacheModel user) throws Exception {
        // Lấy thẻ
        String requestId = "requestId" + System.currentTimeMillis();
        CardRes cardRes = null;
        Exception exception = null;
        try {
            cardRes = buyCardClient.buyCard(cashoutItem.getProvider(), cashoutItem.getMoney().intValue(), 1, requestId);
        } catch (Exception e) {
            exception = e;
            e.printStackTrace();
            KingUtil.printException("approveCashoutCard", e);
        }
        KingUtil.printLog("CardRequestActionProcessor - approveCashoutRequest(), cardRes: " + gson.toJson(cardRes));
        if (exception != null || cardRes == null || cardRes.code != 0) {
            // Lấy thẻ không thành công, hoàn lại tiền
            BaseResponseModel subMoneyResult2 = userService.updateMoneyWithNotify(user.getNickname(), cashoutItem.getGold().longValue(), "cashout_card", "Hệ thống", "Hoàn tiền khi không rút thành công " + cashoutItem.getName(), false);
            if (!subMoneyResult2.isSuccess()) {
                KingUtil.printLog(("CardRequestActionProcessor - approveCashoutRequest(), Lỗi hoàn tiền. Cần kiểm tra lại subMoneyResult2: " + subMoneyResult2.toJson()));
            }
            // Log lỗi rút
            CashoutByCardMessage message = new CashoutByCardMessage(user.getNickname(), requestId, cashoutItem.getProvider(), cashoutItem.getMoney().intValue()
                    , 1, -1, "Loi ket noi cong the cardRes: " + (cardRes == null ? "null" : cardRes.toString())
                    , "", "", 1, "izpay", "");
            RMQApi.publishMessage("queue_dvt", message, 303);
            cashoutRequest.setNote(gson.toJson(cardRes));
            cashoutRequest.setStatus(Constants.CashoutRequestStatus.ERROR);
            cashoutRequestDao.update(cashoutRequest);
            throw new ApiException("Rút thẻ gặp lỗi " + exception != null ? exception.getMessage() : String.valueOf(cardRes), exception);
        }

        // Lấy thẻ thành công
        cashoutRequest.setStatus(Constants.CashoutRequestStatus.COMPLETE_PAY);
        StringBuilder sb = new StringBuilder();
        sb.append("Đổi thẻ thành công. ");
        vn.yotel.payment.buycard.entities.Card firstCard = null;
        for (vn.yotel.payment.buycard.entities.Card cardResp : cardRes.data.cards) {
            if (firstCard == null) {
                firstCard = cardResp;
            }
            sb.append("\nNhà mạng: ");
            sb.append(cashoutItem.getProvider());
            sb.append("\nSố thẻ (pin): ");
            sb.append(cardResp.pin);
            sb.append("\nSố serial: ");
            sb.append(cardResp.serial);
            sb.append("\nGiá trị: ");
            sb.append(cardResp.amount);
        }
        KingUtil.printLog("sb: " + sb.toString());
        MailBoxServiceImpl service = new MailBoxServiceImpl();
        service.sendMailCardMobile(user.getNickname(), cashoutItem.getProvider(), firstCard.serial, firstCard.pin, "Mua thẻ thành công", sb.toString());
        // Tăng cashout của user
        increaseUserCashout(user.getNickname(), cashoutItem.getGold().intValue(), "CashoutByCard");
        cashoutRequest.setNote(gson.toJson(cardRes));
        cashoutRequestDao.update(cashoutRequest);

        if (exception != null)
            throw exception;
        // No error
        return true;
    }

    private boolean approveCashoutMomo(CashoutRequest cashoutRequest, CashoutItem cashoutItem, UserCacheModel user) throws Exception {
        // Gửi lệnh rút MOMO
        JSONObject extraDataObj = new JSONObject(cashoutRequest.getExtraData());
        KingUtil.printLog("extraDataObj: " + extraDataObj.toString());
        String accountNo = extraDataObj.getString("account_no");
        String accountName = extraDataObj.getString("account_name");
        BuyCardClient.MomoRes exchangeRes = buyCardClient
                .buyMomo(cashoutItem.getMoney().longValue()
                        , cashoutRequest.getId() + ""
                        , accountNo, accountName
                        , "Rut tien thanh cong Ma GD " + cashoutRequest.getId());
        KingUtil.printLog("CardRequestActionProcessor - approveCashoutRequest(), exchangeRes: " + gson.toJson(exchangeRes));
        if (exchangeRes != null && (exchangeRes.code == 0 || exchangeRes.code == 22)) {
            cashoutRequest.setStatus(Constants.CashoutRequestStatus.COMPLETE_PAY);
            mailBoxService.sendMailPopupMessage(
                    user.getNickname(), "Kết quả rút Momo", "Admin đã phê duyệt rút Momo. Vui lòng đợi giao dịch hoàn tất trong khoảng 24h");
            // Tăng cashout của user
            increaseUserCashout(user.getNickname(), cashoutItem.getGold().intValue(), "CashoutByMomo");
        } else {
            cashoutRequest.setStatus(Constants.CashoutRequestStatus.ERROR);
            BaseResponseModel subMoneyResult2 = userService.updateMoneyWithNotify(user.getNickname(), cashoutItem.getGold().longValue(), "cashout_card", "Hệ thống", "Hoàn tiền khi không rút thành công " + cashoutItem.getName(), false);
            if (!subMoneyResult2.isSuccess()) {
                KingUtil.printLog(("CardRequestActionProcessor - approveCashoutRequest(), Lỗi hoàn tiền. Cần kiểm tra lại subMoneyResult2: " + subMoneyResult2.toJson()));
                throw new Exception("Rút thẻ gặp lỗi " + String.valueOf(exchangeRes));
            }
            GU.sendOperation("Có lỗi cổng thẻ khi rút momo, CardRequestActionProcessor MOMO PROCESS exchangeRes: " + (exchangeRes == null ? "null" : exchangeRes.toString()));
        }
        cashoutRequest.setNote(gson.toJson(exchangeRes));
        cashoutRequestDao.update(cashoutRequest);
        // No error
        return true;
    }

    private boolean approveCashoutBank(CashoutRequest cashoutRequest, CashoutItem cashoutItem, UserCacheModel user) throws Exception {
        String cashoutRequestIdStr = cashoutRequest.getId() + "";
        // Gửi lệnh rút BANK
        JSONObject extraDataObj = new JSONObject(cashoutRequest.getExtraData());
        KingUtil.printLog("extraDataObj: " + extraDataObj.toString());
        String requestAccNo = extraDataObj.getString("request_acc_no");
        String requestAccName = extraDataObj.getString("request_acc_name");
        String requestBankCode = extraDataObj.getString("request_bank_code");
        String partnerReference = "Hu_" + System.currentTimeMillis();
        String telco = "BANK";
        Integer denomination = cashoutItem.getMoney().intValue();
        int quantity = 1;
        String requestDesc = "Rut tien thanh cong Ma GD " + cashoutRequest.getId();
        String signature = RSAUtil.sign(Constants.BuyCardClient.privateKey
                , "partner_id=" + Constants.BuyCardClient.partnerId + "&partner_reference=" + partnerReference
                        + "&quantity=" + quantity + "&telco=" + telco + "&denomination=" + denomination);
        JSONObject requestData = new JSONObject();
        requestData.put("partner_id", Constants.BuyCardClient.partnerId);
        requestData.put("partner_reference", partnerReference);
        requestData.put("telco", telco);
        requestData.put("denomination", denomination);
        requestData.put("quantity", quantity);
        requestData.put("request_acc_no", requestAccNo);
        requestData.put("request_acc_name", requestAccName);
        requestData.put("request_desc", requestDesc);
        requestData.put("request_bank_code", requestBankCode);
        requestData.put("signature", signature);
        String jsonRequestData = requestData.toString();
        KingUtil.printLog("jsonRequestData: " + jsonRequestData);
        Exception exception = null;
        String postResult = "";
        try {
            postResult = KingUtil.post(Constants.BuyCardClient.bankUrl + "/withdraw", jsonRequestData
                    , Constants.BuyCardClient.xVersion, Constants.BuyCardClient.partnerId);
        } catch (Exception e) {
            exception = e;
            KingUtil.printException("CardRequestActionProcessor", e);
            postResult = "";
        }
        if (postResult.equals("")) {
            KingUtil.printLog("postResult == null");
            cashoutRequest.setStatus(Constants.CashoutRequestStatus.PENDING);
            cashoutRequest.setNote("null");
            cashoutRequestDao.update(cashoutRequest);
            if (exception != null)
                throw exception;
            return true;
        }
        KingUtil.printLog("postResult != null");
        cashoutRequest.setNote(postResult);
        KingUtil.printLog("CardRequestActionProcessor - approveCashoutRequest(), postResult: " + postResult);
        JSONObject postResultObj = new JSONObject(postResult);
        Document document = cashoutDao.GetBankRequestById(cashoutRequestIdStr);
        List<Integer> listErrorCode = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 20, 21);
        if (postResultObj != null && postResultObj.getInt("code") == 0) {
            // Thành công
            cashoutRequest.setStatus(Constants.CashoutRequestStatus.COMPLETE_PAY);
            cashoutRequestDao.update(cashoutRequest);
            mailBoxService.sendMailPopupMessage(
                    user.getNickname(), "Kết quả rút Bank", "Admin đã phê duyệt rút Bank. Vui lòng đợi giao dịch hoàn tất trong khoảng 24h-48h");
            // Tăng cashout của user
            increaseUserCashout(user.getNickname(), cashoutItem.getGold().intValue(), "CashoutByBank");
            try {
                int valueSubtract = cashoutItem.getGold().intValue();
                MoneyMessageInMinigame messageMoney = new MoneyMessageInMinigame(VinPlayUtils.genMessageId(), user.getId(), user.getNickname(), "CashOutByBank", user.getVin(), user.getVinTotal(), -valueSubtract, "vin", 0L, 0, 0);
                RMQApi.publishMessagePayment(messageMoney, 16);
                RMQApi.publishMessagePayment(messageMoney, 16);
//                        LogMoneyUserMessage messageLog = new LogMoneyUserMessage(user.getId(), user.getNickname(), "CashOutByBank", "M32", user.getVinTotal(), -valueSubtract, "vin"
//                                , "Rút tiền ngân hàng, STK : " + requestAccNo + ", Mã ngân hàng:" + requestBankCode + " mệnh giá " + cashoutItem.getMoney(), 0L, false, user.isBot());

//                        RMQApi.publishMessageLogMoney(messageLog);
                cashoutDao.updateSystemCashout(cashoutItem.getMoney().longValue());
                cashoutDao.UpdateBankRequest(cashoutRequestIdStr, WithdrawResponseCode.SUCCESS.getCode());
                CashoutByBankMessage cashout = new CashoutByBankMessage();
                cashout.setReferenceId(document.getString("reference_id"));
                cashout.setNickname(document.getString("nick_name"));
                cashout.setBank(document.getString("bank"));
                cashout.setAccount(document.getString("account"));
                cashout.setName(document.getString("name"));
                cashout.setAmount(document.getInteger("amount"));
                cashout.setStatus(document.getInteger("status"));
                cashout.setMessage(document.getString("message"));
                cashout.setDesc(document.getString("description"));
                cashout.setCreateTime(document.getString("update_time"));
                cashout.setMessage(postResult);
                cashoutDao.logCashoutByBank(cashout);
            } catch (Exception e) {
                KingUtil.printException("CardRequestActionProcessor BANK SUCCESS PROCESS", e);
            }
        } else if (postResultObj != null && listErrorCode.contains(postResultObj.getInt("code"))) {
            // Thất bại
            cashoutRequest.setStatus(Constants.CashoutRequestStatus.ERROR);
            cashoutRequestDao.update(cashoutRequest);
            BaseResponseModel subMoneyResult2 = userService.updateMoneyWithNotify(user.getNickname(), cashoutItem.getGold().longValue(), "cashout_card", "Hệ thống", "Hoàn tiền khi không rút thành công " + cashoutItem.getName(), true);
            GU.sendOperation("Có lỗi cổng thẻ khi rút bank, CardRequestActionProcessor BANK PROCESS postResultObj: " + (postResultObj == null ? "null" : postResultObj.toString()));
            if (!subMoneyResult2.isSuccess()) {
                KingUtil.printLog(("CardRequestActionProcessor - approveCashoutRequest(), Lỗi hoàn tiền. Cần kiểm tra lại subMoneyResult2: " + subMoneyResult2.toJson()));
                throw new Exception("Lỗi hoàn tiền. Cần kiểm tra lại.");
            }
            try {
                int valueSubtract = cashoutItem.getGold().intValue();
                MailBoxServiceImpl service = new MailBoxServiceImpl();
                service.sendMailBoxFromByNickNameAdmin(user.getNickname(), "Hoàn tiền giao dịch rút ngân hàng thất bại", "Hoàn tiền khi không rút thành công " + cashoutItem.getName());
                MoneyMessageInMinigame messageMoney = new MoneyMessageInMinigame(VinPlayUtils.genMessageId(), user.getId(), user.getNickname(), "CashOutByBank", user.getVin(), user.getVinTotal(), valueSubtract, "vin", 0L, 0, 0);
                LogMoneyUserMessage messageLog = new LogMoneyUserMessage(user.getId(), user.getNickname(), "CashOutByBank", "M32", user.getVin(), valueSubtract, "vin", "Hoàn tiền giao dịch ngân hàng", 0L, false, user.isBot());
                RMQApi.publishMessagePayment(messageMoney, 16);
                RMQApi.publishMessageLogMoney(messageLog);
                cashoutDao.UpdateBankRequest(cashoutRequestIdStr, 99);
            } catch (Exception e) {
                KingUtil.printException("CardRequestActionProcessor BANK SUCCESS PROCESS", e);
                throw e;
            }
        } else {    // code 22 and other
            cashoutRequest.setStatus(Constants.CashoutRequestStatus.PENDING);
            cashoutRequest.setNote("null");
            cashoutRequestDao.update(cashoutRequest);
        }
        // No error
        return true;
    }

    private void increaseUserCashout(String nickName, int cashoutNumber, String actionName) {
        UserCacheModel user = userService.forceGetCachedUser(nickName);
        KingUtil.printLog("increaseUserCashout BEFORE update user " + user.getNickname() + ", cashout: " + user.getCashout() + ", cashoutMoney: " + user.getCashoutMoney());
        userService.updateUser(nickName, (u) -> {
            u.setCashout(u.getCashout() + cashoutNumber);
            u.setCashoutTime(new Date());
            u.setCashoutMoney(u.getCashoutMoney() + cashoutNumber);
        });
        user = userService.forceGetCachedUser(nickName);
        KingUtil.printLog("increaseUserCashout AFTER update user " + user.getNickname() + ", cashout: " + user.getCashout() + ", cashoutMoney: " + user.getCashoutMoney());
        try {
            MoneyMessageInMinigame messageMoney = new MoneyMessageInMinigame(
                    VinPlayUtils.genMessageId(),
                    user.getId(), user.getNickname()
                    , actionName
                    , user.getVin(), user.getVinTotal(), cashoutNumber
                    , "vin", 0L, 0, 0);
            RMQApi.publishMessageInMiniGame(messageMoney);

            /*LogMoneyUserMessage messageLog = new LogMoneyUserMessage(
                    user.getId(), user.getNickname(), actionName, "Rút tiền",
                    user.getVinTotal(), cashoutNumber, "vin", description, 0L, false, user.isBot());
            RMQApi.publishMessageLogMoney(messageLog);*/
        } catch (Exception e) {
            e.printStackTrace();
            GU.sendOperation("RechargeService.recharge: Notify MoneyMessageInMinigame exception " + e.getMessage());
        }
    }

    private String getListCashoutItem() {
        KingUtil.printLog("CardRequestActionProcessor - getListCashoutItem()");
        List<CashoutItem> list = cashoutItemDao.findAll();
        if (list == null) {
            return "[]";
        }
        List<CashoutItem> activeItems = list.stream().filter(cashoutItem -> cashoutItem.getStatus() == 1).collect(Collectors.toList());
        return gson.toJson(activeItems);
    }

    private String getListCashoutRequest(int page, int numPerPage) {
        KingUtil.printLog("CardRequestActionProcessor - getListCashoutRequest()");
        List<CashoutRequest> list = cashoutRequestDao.findAllByPage(page, numPerPage, "id");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        for (CashoutRequest item : list) {
            item.setRequesTimeStr(dateFormat.format(item.getRequesTime()));
            UserCacheModel user = userService.getCacheUserIgnoreCase(item.getUserName());
            item.setCashoutMoney(user.getCashoutMoney());
            item.setRechargeMoney(user.getRechargeMoney());
        }
        if (list != null) {
            return gson.toJson(list);
        } else
            return "[]";
    }

    private static class ApiException extends Exception {
        public ApiException(String message, Throwable cause) {
            super(message, cause);
        }
    }
}
