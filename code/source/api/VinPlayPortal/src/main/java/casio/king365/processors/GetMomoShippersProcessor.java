/*
 * Decompiled with CFR 0.144.
 *
 * Could not load the following classes:
 *  com.hazelcast.core.IMap
 *  com.vinplay.usercore.service.impl.MarketingServiceImpl
 *  com.vinplay.usercore.service.impl.UserServiceImpl
 *  com.vinplay.usercore.utils.GameCommon
 *  com.vinplay.usercore.utils.UserMakertingUtil
 *  com.vinplay.vbee.common.cp.BaseProcessor
 *  com.vinplay.vbee.common.cp.Param
 *  com.vinplay.vbee.common.enums.StatusGames
 *  com.vinplay.vbee.common.hazelcast.HazelcastClientFactory
 *  com.vinplay.vbee.common.messages.UserMarketingMessage
 *  com.vinplay.vbee.common.models.SocialModel
 *  com.vinplay.vbee.common.models.UserModel
 *  com.vinplay.vbee.common.response.LoginResponse
 *  com.vinplay.vbee.common.utils.VinPlayUtils
 *  javax.servlet.http.HttpServletRequest
 *  org.apache.log4j.Logger
 */
package casio.king365.processors;

import casio.king365.Constants;
import casio.king365.GU;
import casio.king365.core.HCMap;
import casio.king365.util.KingUtil;
import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;
import com.hazelcast.core.IMap;
import com.vinplay.dal.service.impl.CacheServiceImpl;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.models.UserModel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.TimeUnit;

public class GetMomoShippersProcessor
        implements BaseProcessor<HttpServletRequest, String> {

    UserServiceImpl userService;
    CacheServiceImpl cacheService;

    int warningProcessingTime = 1000;
    
    static final String CACHE_KEY = "shipper_momo";

    public String execute(Param<HttpServletRequest> param) throws JSONException {
        init();

        JSONObject res = new JSONObject();
        try {
            res.put("is_success", false);
            res.put("desc", "Không xác định.");

            HttpServletRequest request = (HttpServletRequest) param.get();
            String username = request.getParameter("un") != null ? request.getParameter("un") : "";
            String at = request.getParameter("at") != null ? request.getParameter("at") : "";
            KingUtil.printLog("GetMomoShippersProcessor - username: "+ username+", at: "+at);
            if(username.equals("") && at.equals("")){
                res.put("desc", "Thiếu tham số.");
                return res.toString();
            }
            UserModel userModel;
            if(username.equals("")){
                IMap<String, String> tokenMap = HCMap.getTokenMap();
                String nickname = tokenMap.get(at);
                userModel = userService.getUserByNickName(nickname);
            } else
                userModel = userService.getUserByUserName(username);
            if (userModel == null) {
                KingUtil.printLog(("GetMomoShippersProcessor - userModel == null"));
                res.put("desc", "Không tìm thấy người dùng.");
                return res.toString();
            }

            // Kiểm tra cache
            if(!cacheService.checkKeyExist(CACHE_KEY)){
                // Nếu chưa có thì add cache
                getRemoteShipperInfoAndAddToCache();
            }

            res.put("is_success", true);
            res.put("desc", "Thành công.");
            res.put("accounts", new JSONArray(cacheService.getValueStr(CACHE_KEY)));
            res.put("send_content", userModel.getNickname());
        }  catch (UnExpectedException e) {
            e.printStackTrace();
            GU.sendOperation(e.getMessage());
            KingUtil.printException("GetMomoShippersProcessor.UnExpectedException", e);
            res.put("desc", "Hệ thống xảy ra lỗi. Quý khách vui lòng liên hệ CSKH để biết thêm thông tin chi tiết.");
            return res.toString();
        } catch (Exception e) {
            e.printStackTrace();
            KingUtil.printException("GetMomoShippersProcessor", e);
            return "{\"is_success\":false,\"desc\":\"Lỗi hệ thống! Vui lòng liên hệ Admin.\"}";
        }

        return res.toString();
    }

    private void getRemoteShipperInfoAndAddToCache() throws IOException, JSONException, UnExpectedException {
        KingUtil.printLog("GetMomoShippersProcessor getRemoteShipperInfoAndAddToCache()");
        Long requestTime = System.currentTimeMillis();
        String requestId = "requestId" + requestTime;
        String signature = KingUtil.MD5(requestId + "|" + requestTime + "|" + Constants.MomoInfo.partnerName + "|" + Constants.MomoInfo.partnerKey);

        String urlString = Constants.MomoInfo.apiGetShippersUrl +"?username=" + Constants.MomoInfo.partnerName
                + "&request_id=" + requestId + "&request_time=" + requestTime + "&signature=" + signature;
        KingUtil.printLog("GetMomoShippersProcessor urlString: " + urlString);

        URL url = new URL(urlString);
        URLConnection conn = url.openConnection();
        conn.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0");
        String getResultStr;
        try (InputStream inputStream = conn.getInputStream();
             InputStreamReader inputStreamReader = new InputStreamReader(
                     inputStream, Charsets.UTF_8)) {
            getResultStr = CharStreams.toString(inputStreamReader);
        }
//              String getResultStr = "{\"code\":200,\"message\":\"Success\",\"data\":{\"partnerName\":\"hanoi\",\"accounts\":[{\"phone\":\"0904588756\",\"isMaxquota\":0,\"message\":\"OK\",\"name\":\"TON NU HUONG BINH\"}]}}";
        KingUtil.printLog("getResultStr: "+getResultStr.trim());
        if(getResultStr == null || getResultStr.equals("")) {
            throw new UnExpectedException("data trả về rỗng");
        }
        JSONObject getResult = new JSONObject(getResultStr.trim());
        int code = getResult.getInt("code");
        if (code != 200) {
            throw new UnExpectedException("data trả về sai");
        }

        // Lấy data từ cache
        JSONObject getResultData = getResult.getJSONObject("data");
        JSONArray accounts = getResultData.getJSONArray("accounts");
        JSONArray accountsResponse = new JSONArray();
        for (int i = 0; i < accounts.length(); i++) {
            JSONObject acc = accounts.getJSONObject(i);
            int isMaxquota = acc.getInt("isMaxquota");
            if (isMaxquota == 0) {
                accountsResponse.put(acc);
            }
        }

        cacheService.setValue(CACHE_KEY, accountsResponse.toString(), 300, TimeUnit.SECONDS);
        // Tăng thời gian cảnh báo lên (vì processor này đã gọi đến API cổng)
        warningProcessingTime = 10000;
    }

    void init() {
        if (userService == null)
            userService = new UserServiceImpl();
        if (cacheService == null)
            cacheService = new CacheServiceImpl();
    }

    public int getShouldWarningProcessingTimeInMillisecond() {
        return warningProcessingTime;
    }

    public static void main(String[] args) {
        try {
            Long requestTime = System.currentTimeMillis();
            String requestId = "requestId" + requestTime;
            String signature = KingUtil.MD5(requestId + "|" + requestTime + "|" + Constants.MomoInfo.partnerName + "|" + Constants.MomoInfo.partnerKey);

            JSONObject apiParam = new JSONObject();
            apiParam.put("username", Constants.MomoInfo.partnerName);
            apiParam.put("request_id", requestId);
            apiParam.put("request_time", requestTime);
            apiParam.put("signature", signature);

            System.out.println("partnerKey: " + Constants.MomoInfo.partnerKey);
            System.out.println("username: " + Constants.MomoInfo.partnerName);
            // System.out.println("apiParam: "+apiParam.toString());

            // String postResult = KingUtil.post(apiUrl, apiParam.toString());
            // System.out.println("postResult: "+postResult);

            String urlString = Constants.MomoInfo.apiGetShippersUrl + "?username=" + Constants.MomoInfo.partnerName
                    + "&request_id=" + requestId + "&request_time=" + requestTime + "&signature=" + signature;
            System.out.println("urlString: " + urlString);

            URL url = new URL(urlString);
            URLConnection conn = url.openConnection();
            String getResult = null;
            try (InputStream inputStream = conn.getInputStream();
                 InputStreamReader inputStreamReader = new InputStreamReader(
                         inputStream, Charsets.UTF_8)) {
                getResult = CharStreams.toString(inputStreamReader);
            }
            System.out.println("getResult: " + getResult);
        } catch (Exception e) {
            e.printStackTrace();
            KingUtil.printException("GetMomoShippersProcessor", e);
        }

    }

    private class UnExpectedException extends Exception {
        public UnExpectedException(String message) {
            super(message);
        }
    }
}

