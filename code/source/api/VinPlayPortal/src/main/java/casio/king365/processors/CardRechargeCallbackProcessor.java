package casio.king365.processors;

import casio.king365.GU;
import casio.king365.util.KingUtil;
import com.vinplay.dichvuthe.response.RechargeResponse;
import com.vinplay.dichvuthe.service.RechargeService;
import com.vinplay.dichvuthe.service.impl.RechargeServiceImpl;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import org.json.JSONObject;
import vn.yotel.yoker.dao.CashinNotifyDao;
import vn.yotel.yoker.dao.impl.CashinNotifyDaoImpl;
import vn.yotel.yoker.domain.CashinNotify;

import javax.persistence.PersistenceException;
import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.Date;

public class CardRechargeCallbackProcessor
        implements BaseProcessor<HttpServletRequest, String> {
    public String execute(Param<HttpServletRequest> param) {
        String orderId = "";
        String uuid = "";
        String cardCode = "";
        String cardSerial = "";
        String status = "";
        String amount = "";

        JSONObject res = new JSONObject();
        try {
            res.put("errorCode", 500);
            res.put("errorMessage", "Có lỗi");
            HttpServletRequest request = (HttpServletRequest) param.get();
            String ipAddress = request.getRemoteAddr();
            orderId = request.getParameter("orderId") != null ? request.getParameter("orderId") : "";
            uuid = request.getParameter("uuid") != null ? request.getParameter("uuid") : "";
            cardCode = request.getParameter("cardCode") != null ? request.getParameter("cardCode") : "";
            cardSerial = request.getParameter("cardSerial") != null ? request.getParameter("cardSerial") : "";
            // 1 success, 2 failure
            status = request.getParameter("status") != null ? request.getParameter("status") : "";
            amount = request.getParameter("amount") != null ? request.getParameter("amount") : "";

            String paramStr = "orderId: " + orderId
                    + ", uuid: " + uuid + ", cardCode: " + cardCode + ", cardSerial: " + cardSerial + ", status: " + status + ", amount: " + amount;
            KingUtil.printLog("CardRechargeCallbackProcessor - execute(), " + paramStr);

            KingUtil.printLog("CardRechargeCallbackProcessor 2");
            RechargeService rechargeService = new RechargeServiceImpl();
            RechargeResponse result = rechargeService.receiveResultFromIzpay(orderId, uuid, cardCode, cardSerial, status, amount, ipAddress);
            KingUtil.printLog("receiveResultFromIzpay: " + result);
            res.put("errorMessage", "receiveResultFromIzpay: " + result);

            res.put("errorCode", 200);
            //res.put("errorMessage", "Thành công");
        } catch (Exception e) {
            e.printStackTrace();
            KingUtil.printException("CardRechargeCallbackProcessor", e);
            GU.sendOperation("Có lỗi khi nhận kết quả nạp thẻ , Exception: " + KingUtil.printException(e));
            GU.sendCustomerService("Có lỗi khi nạp CARD, dữ liệu từ cổng card: bankNo: orderId: " + orderId
                    + ", uuid: " + uuid + ", cardCode: " + cardCode + ", cardSerial: " + cardSerial + ", status: " + status + ", amount: " + amount);
        }
        return res.toString();
    }

    public int getShouldWarningProcessingTimeInMillisecond() {
        return 2000;
    }

    public int getShouldAlertProcessingTimeInMillisecond() {
        return 10000;
    }
}

