package casio.king365.processors;

import casio.king365.GU;
import casio.king365.util.KingUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vinplay.dichvuthe.service.RechargeService;
import com.vinplay.dichvuthe.service.impl.RechargeServiceImpl;
import com.vinplay.usercore.logger.MoneyLogger;
import com.vinplay.usercore.service.MailBoxService;
import com.vinplay.usercore.service.UserService;
import com.vinplay.usercore.service.impl.GameConfigServiceImpl;
import com.vinplay.usercore.service.impl.MailBoxServiceImpl;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.response.ResultGameConfigResponse;
import com.vinplay.vbee.common.utils.UserValidaton;
import org.json.JSONObject;
import vn.yotel.yoker.dao.CashinItemDao;
import vn.yotel.yoker.dao.CashinNotifyDao;
import vn.yotel.yoker.dao.CashoutItemDao;
import vn.yotel.yoker.dao.CashoutRequestDao;
import vn.yotel.yoker.dao.impl.CashinItemDaoImpl;
import vn.yotel.yoker.dao.impl.CashinNotifyDaoImpl;
import vn.yotel.yoker.dao.impl.CashoutItemDaoImpl;
import vn.yotel.yoker.dao.impl.CashoutRequestDaoImpl;
import vn.yotel.yoker.domain.CashinNotify;

import javax.persistence.PersistenceException;
import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.Date;
import java.util.stream.Collectors;

public class BankCallbackProcessor
        implements BaseProcessor<HttpServletRequest, String> {

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    UserService userService;
    CashoutItemDao cashoutItemDao;
    CashoutRequestDao cashoutRequestDao;
    CashinNotifyDao cashinNotifyDao;
    CashinItemDao cashinItemDao;

    public int getShouldWarningProcessingTimeInMillisecond() {
        return 5_000;
    }

    public int getShouldAlertProcessingTimeInMillisecond() {
        return 10_000;
    }

    public String execute(Param<HttpServletRequest> param) {
        String bankNo = "";
        String bankCode = "";
        String amount = "";
        String comment = "";
        String nickname = "";
        String trans_time = "";
        String trans_id = "";
        String signature = "";

        JSONObject res = new JSONObject();
        try {
            res.put("errorCode", 500);
            res.put("errorMessage", "Có lỗi");
            if (!init())
                return res.toString();

            HttpServletRequest request = param.get();
            String ipAddress = request.getRemoteAddr();
            String requestData = request.getReader().lines().collect(Collectors.joining());
            KingUtil.printLog("BankCallbackProcessor - execute(), requestData: " + requestData);
            JSONObject requestObj = new JSONObject(requestData);
            bankNo = requestObj.has("bankNo") ? requestObj.getString("bankNo") : "";
            bankCode = requestObj.has("bankCode") ? requestObj.getString("bankCode") : "";
            amount = requestObj.has("amount") ? requestObj.getString("amount") : "";
            // comment: nội dung chuyển tiền. Thường sẽ yêu cầu nội dung chuyển tiền là nickname của khách
            comment = requestObj.has("comment") ? requestObj.getString("comment") : "";
            nickname = UserValidaton.filterNicknameInvaliChar(comment);
            // String request_id = requestObj.has("request_id") ? requestObj.getString("request_id") : "";
            trans_time = requestObj.has("time") ? requestObj.getString("time") : "";
            trans_id = requestObj.has("transId") ? requestObj.getString("transId") : "";
            signature = requestObj.has("signature") ? requestObj.getString("signature") : "";

            KingUtil.printLog("BankCallbackProcessor - execute(), bankNo: " + bankNo
                    + ", bankCode: " + bankCode + ", amount: " + amount + ", comment: " + comment
                    + ", trans_time: " + trans_time + ", trans_id: " + trans_id + ", signature: " + signature);

            if (bankNo.equals("") || bankCode.equals("") || amount.equals("") || comment.equals("") ||
                    trans_time.equals("") || trans_id.equals("") || signature.equals("")) {
                res.put("errorMessage", "Sai param");
                return res.toString();
            }
            KingUtil.printLog("BankCallbackProcessor 1");
            // Save data to db
            CashinNotify cashinNotify = new CashinNotify();
            cashinNotify.setUuid(trans_id);
            cashinNotify.setGateType("bank");
            cashinNotify.setIp(ipAddress);
            cashinNotify.setReceiveTime(new Timestamp(new Date().getTime()));
            cashinNotify.setComment(comment);
            cashinNotify.setData(requestData);
            cashinNotify.setError("true");
            try {
                cashinNotifyDao.save(cashinNotify);
            } catch (PersistenceException e) {
                // Lỗi này xảy ra khi data đã có trong db
                // có nghĩa là callback với trans_id đã được xử lí rồi
                KingUtil.printLog("BankCallbackProcessor PersistenceException: " + KingUtil.printException(e));
                // Kiểm tra nếu transId này chưa thành công thì cho xử lí,
                // nếu đã thành công thì ignore callback này
                CashinNotify oldCashinNotify = cashinNotifyDao.findById(trans_id);
                if (oldCashinNotify!= null && !oldCashinNotify.getError().isEmpty() && oldCashinNotify.getError().equals("false")) {
                    // ignore callback này thôi
                    res.put("errorCode", 200);
                    res.put("errorMessage", "Giao dịch đã được xử lí trước đó.");
                    return res.toString();
                }
            }
            KingUtil.printLog("BankCallbackProcessor 2");

//            // check valid data (signature)
//            String validSignature = KingUtil.MD5(Constants.BankInInfo.partnerKey +"|"
//                + bankNo + "|" + amount + "|" + comment + "|" + trans_time + "|" + trans_id);
//            logger.debug((Object) "validSignature: "+validSignature);
//            if(!validSignature.equals(signature)){
//                res.put("errorMessage", "Sai signature");
//                return res.toString();
//            }
            // Tìm cashinItem momo, tính được số tiền cộng cho user
//            List<CashinItem> listCashinItem = cashinItemDao.findByProvider("BANK");
//            if(listCashinItem == null || listCashinItem.size() == 0){
//                logger.debug((Object) "listCashinItem == null || listCashinItem.size() == 0");
//                res.put("errorMessage", "Lỗi hệ thống, không tìm thấy cashinItem config");
//                return res.toString();
//            }
//            CashinItem cashinItem = listCashinItem.get(0);
//            if(cashinItem == null){
//                logger.debug((Object) "cashinItem == null");
//                res.put("errorMessage", "Lỗi hệ thống, không tìm thấy cashinItem config");
//                return res.toString();
//            }
//            Double addMoney = cashinItem.getGold() * Integer.parseInt(amount);
            GameConfigServiceImpl gameConfigServiceImpl = new GameConfigServiceImpl();
            ResultGameConfigResponse gateInOutConfigData = gameConfigServiceImpl.getGameConfigAdmin("king365_gate_in_out_rate", "").get(0);
            KingUtil.printLog("BankCallbackProcessor 3");
            if (gateInOutConfigData == null) {
                KingUtil.printLog("gateInOutConfigData == null");
                res.put("errorMessage", "Lỗi hệ thống, không tìm thấy config");
                return res.toString();
            }

            KingUtil.printLog("BankCallbackProcessor 4");
            // Add money to user
            UserCacheModel user = userService.getCacheUserIgnoreCase(nickname);
            if (user == null) {
                UserModel userModel = userService.getUserByNickName(nickname);
                if (userModel == null) {
                    res.put("errorMessage", "Không tìm thấy người dùng.");
                    GU.sendCustomerService("Có lỗi khi nạp BANK, Không tìm thấy người dùng. Trans_id: " + trans_id + ", comment: " + comment);
                    return res.toString();
                }
            }
            KingUtil.printLog("BankCallbackProcessor 5");

            // Call rechargeService
            RechargeService rechargeService = new RechargeServiceImpl();
            rechargeService.receiveResultFromZoanBank(trans_id, bankNo, bankCode, bankCode
                    , amount, comment, user.getNickname(), trans_time, signature);

            KingUtil.printLog("BankCallbackProcessor 6");
            String desc = "Cộng tiền nạp Bank, mã giao dịch: " + trans_id + ", số tiền: " + amount;
            // Mail inbox
            MailBoxService service = new MailBoxServiceImpl();
            service.sendMailPopupMessage(
                    user.getNickname(), "Kết quả nạp Bank", desc);
            if (!userService.isOnline(user.getNickname())) {
                userService.sendTeleMessage(user.getNickname(), desc);
            }
            KingUtil.printLog("BankCallbackProcessor 7");
            res.put("errorCode", 200);
            res.put("errorMessage", "Thành công");

            cashinNotify = cashinNotifyDao.findById(trans_id);
            cashinNotify.setError("false");
            cashinNotifyDao.update(cashinNotify);
        } catch (Exception e) {
            e.printStackTrace();
            KingUtil.printException("BankCallbackProcessor", e);
            MoneyLogger.log("", "RechargeByBank", 0L, 0L, "vin", "Nap vin qua ngan hang", "1001", e.getMessage());
            GU.sendOperation("Có lỗi khi nhận kết quả nạp bank , Exception: " + KingUtil.printException(e));
            GU.sendCustomerService("Có lỗi khi nạp BANK, dữ liệu từ cổng bank: bankNo: " + bankNo
                    + ", bankCode: " + bankCode + ", amount: " + amount + ", comment: " + comment
                    + ", trans_time: " + trans_time + ", trans_id: " + trans_id + ", signature: " + signature);
            try {
                res.put("errorMessage", "Có lỗi: " + KingUtil.printException(e));
            } catch (Exception ex) {
                KingUtil.printException("BankCallbackProcessor", e);
            }
        }
        return res.toString();
    }

    private boolean init() {
        if (cashoutItemDao == null)
            cashoutItemDao = new CashoutItemDaoImpl();
        if (cashoutRequestDao == null)
            cashoutRequestDao = new CashoutRequestDaoImpl();
        if (userService == null)
            userService = new UserServiceImpl();
        if (cashinNotifyDao == null)
            cashinNotifyDao = new CashinNotifyDaoImpl();
        if (cashinItemDao == null)
            cashinItemDao = new CashinItemDaoImpl();
        return true;
    }
}
