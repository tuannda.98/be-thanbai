package casio.king365.processors;

import casio.king365.Constants;
import casio.king365.GU;
import casio.king365.service.MongoDbService;
import casio.king365.service.impl.MongoDbServiceImpl;
import casio.king365.util.KingUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hazelcast.core.IMap;
import com.vinplay.usercore.dao.impl.LuckyDaoImpl;
import com.vinplay.usercore.service.MailBoxService;
import com.vinplay.usercore.service.UserService;
import com.vinplay.usercore.service.impl.GameConfigServiceImpl;
import com.vinplay.usercore.service.impl.MailBoxServiceImpl;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.hazelcast.HazelcastUtils;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.response.BaseResponseModel;
import com.vinplay.vbee.common.response.ResultGameConfigResponse;
import org.json.JSONObject;
import vn.yotel.payment.buycard.api.Request;
import vn.yotel.payment.buycard.entities.CardReq;
import vn.yotel.yoker.dao.CashoutItemDao;
import vn.yotel.yoker.dao.CashoutRequestDao;
import vn.yotel.yoker.dao.RequestCardDao;
import vn.yotel.yoker.dao.impl.CashoutItemDaoImpl;
import vn.yotel.yoker.dao.impl.CashoutRequestDaoImpl;
import vn.yotel.yoker.dao.impl.RequestCardDaoImpl;
import vn.yotel.yoker.domain.RequestCard;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class GetGameConfigProcessor
        implements BaseProcessor<HttpServletRequest, String> {

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    UserService userService;
    CashoutItemDao cashoutItemDao;
    CashoutRequestDao cashoutRequestDao;
    RequestCardDao requestCardDao;
    MongoDbService mongoDbService;
    GameConfigServiceImpl gameConfigServiceImpl;

    public String execute(Param<HttpServletRequest> param) {
        JSONObject res = new JSONObject();
        try {
            res.put("is_success", false);
            res.put("desc", "Có lỗi");
            if (!init())
                return res.toString();

            ResultGameConfigResponse king365Config = gameConfigServiceImpl.getGameConfigAdmin("king365_general_config", "").get(0);
            if (king365Config == null) {
                KingUtil.printLog("king365Config == null");
                res.put("errorMessage", "Lỗi hệ thống, không tìm thấy config");
                return res.toString();
            }
            JSONObject king365ConfigObj = new JSONObject(king365Config.value);
            HttpServletRequest request = (HttpServletRequest) param.get();
            String test = request.getParameter("test") != null ? request.getParameter("test") : "";
            if (test.equals("2")) {
                KingUtil.printLog("insert giftcode mail");
                MailBoxServiceImpl service = new MailBoxServiceImpl();
                service.sendmailGiftCode(request.getParameter("nn"), "436375", "1", "10000");
            }
            if (test.equals("1")) {
                KingUtil.printLog("insert card mail");
                MailBoxServiceImpl service = new MailBoxServiceImpl();
                service.sendMailCardMobile(request.getParameter("nn"), "2", "46322371717716", "462626161356367", "Mua thẻ thành công", "Chúc mừng bạn rút thẻ thành công! Số thẻ của bạn là: 462626161356367, serial: 46322371717716");
            }
            if (test.equals("4")) {
                KingUtil.printLog("sendOperation");
                GU.sendOperation("Hai test send này");
            }
            if (test.equals("5")) {
                KingUtil.printLog("get rut bank status");
                CardReq cardReq = new CardReq();
                cardReq.partner_id = Constants.BuyCardClient.partnerId;
                cardReq.telco = "BANK";
                // String partnerReference = "requestId" + System.currentTimeMillis();
                cardReq.partner_reference = "61";
                String postResult = Request.getInstance().checkBankCashout(
                        cardReq
                        , Constants.BuyCardClient.privateKey
                        , Constants.BuyCardClient.checkBankCashoutUrl);
                KingUtil.printLog("postResult: " + postResult);
            }
            if (test.equals("6")) {
                String nn = "testtest7x";
                MailBoxService service = new MailBoxServiceImpl();
                List<String> listNickname = new ArrayList<>();
                listNickname.add(nn);
                service.sendMailBoxFromByNickName(listNickname, "sendMailBoxFromByNickName tilte", "sendMailBoxFromByNickName content");
                service.sendMailBoxFromByNickNameAdmin(nn, "sendMailBoxFromByNickNameAdmin title", "sendMailBoxFromByNickNameAdmin content");
                service.sendMailGiftCode(nn, "Giftcode0987654321", "sendMailGiftCode title", "sendMailGiftCode content");
                service.sendMailCardMobile(nn, "2", "serial098765", "pin098765", "sendMailCardMobile title", "sendMailCardMobile content");
                service.sendMailPopupMessage(nn, "sendMailPopupMessage title", "sendMailPopupMessage content");
                res.put("is_success", true);
                res.put("desc", "send mail inbox tét ok");
                res.put("errorMessage", "");
                return res.toString();
            }
            if (test.equals("7")) {
                BaseResponseModel subMoneyResult = userService.updateMoneyWithNotify(request.getParameter("nn")
                        , Integer.parseInt(request.getParameter("amount")), request.getParameter("action"), "Hệ thống"
                        , request.getParameter("desc"), false);
                res.put("is_success", true);
                res.put("errorMessage", "cong tru tien " + request.getParameter("nn") + " thanh cong");
                return res.toString();
            }
            if (test.equals("8")) {
                LuckyDaoImpl dao = new LuckyDaoImpl();
                List<Integer> rotateLst = dao.getRotateCount(6820762, "10.0.0.1");
                StringBuilder sb = new StringBuilder();
                for (Integer i : rotateLst) {
                    sb.append(i);
                    sb.append(" ");
                }
                res.put("is_success", true);
                res.put("desc", "sb: " + sb.toString());
                res.put("errorMessage", "sb: " + sb.toString());
                return res.toString();
            }
            IMap<String, UserCacheModel> usersMap = HazelcastUtils.getUsersMap();
            if (test.equals("9")) {
                UserCacheModel u = (UserCacheModel) usersMap.get(request.getParameter("nn"));
//                u.setMobile(null);
//                u.setHasMobileSecurity(false);
//                usersMap.put(request.getParameter("nn"), u);
                return request.getParameter("nn") + " mobile: " + u.getMobile() + ", isMobileSecure: " + u.isHasMobileSecurity();
            }
            if (test.equals("10")) {
                UserCacheModel u = (UserCacheModel) usersMap.get(request.getParameter("nn"));
                u.setMobile(null);
                u.setHasMobileSecurity(false);
                usersMap.put(request.getParameter("nn"), u);
                return request.getParameter("nn") + " mobile: " + u.getMobile() + ", isMobileSecure: " + u.isHasMobileSecurity();
            }
            if (test.equals("11")) {
                return "user map contained " + request.getParameter("nn") + ": " + usersMap.containsKey(request.getParameter("nn"));
            }
            if (test.equals("12")) {
                usersMap.remove(request.getParameter("nn"));
                return "user map contained " + request.getParameter("nn") + ": " + usersMap.containsKey(request.getParameter("nn"));
            }
            if (test.equals("x")) {
                res.put("errorMessage", "Ok nhé test x 38");
                return res.toString();
            }

            res.put("is_success", true);
            res.put("desc", "Thành công");
            res.put("telegram", king365ConfigObj.getString("telegram"));
            res.put("teleGroup", king365ConfigObj.getString("teleGroup"));
            res.put("fanpage", king365ConfigObj.getString("fanpage"));
            res.put("cskh", king365ConfigObj.getString("cskh"));
        } catch (Exception e) {
            e.printStackTrace();
            KingUtil.printException("GetGameConfigProcessor", e);
        }
        return res.toString();
    }

    private void convertRequestCardToMongodb() {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        // Lấy danh sách request card
        List<RequestCard> listRequestCard = requestCardDao.findAll();
        SimpleDateFormat DateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for (RequestCard rc : listRequestCard) {
            try {
                Map<String, Object> mapData = new HashMap<>();
                mapData.put("reference_id", rc.getGwTransId());
                UserModel userModel = userService.getUserByUserName(rc.getUsername());
                if (userModel != null)
                    mapData.put("nick_name", userModel.getNickname());
                else mapData.put("nick_name", rc.getUsername());
                mapData.put("provider", rc.getProvider());
                mapData.put("serial", rc.getSerial());
                mapData.put("pin", rc.getPin());
                mapData.put("amount", rc.getMoney().intValue());
                mapData.put("status", rc.getProcessStatus());
                mapData.put("message", rc.getGwMessage());
                mapData.put("code", 0);
                // mapData.put("time_log", dateFormat.format(rc.getDatetime()));
                // LocalDateTime ldt = rc.getDatetime().toLocalDateTime();
                mapData.put("time_log", DateFormat.format(rc.getDatetime()));
                mapData.put("request_card_id", rc.getId());
                // KingUtil.printLog("convert request card to mongodb, request_card_id: "+rc.getId()+", time_log: "+dateFormat.format(rc.getDatetime()));
                // bank.updateTime = document.getString("update_time");
                mapData.put("partner", "zoan");
                mongoDbService.insert("dvt_recharge_by_card", mapData);
            } catch (Exception e) {
                e.printStackTrace();
                KingUtil.printException("GetGameConfigProcessor", e);
            }
        }
    }

    private boolean init() {
        if (cashoutItemDao == null)
            cashoutItemDao = new CashoutItemDaoImpl();
        if (cashoutRequestDao == null)
            cashoutRequestDao = new CashoutRequestDaoImpl();
        if (userService == null)
            userService = new UserServiceImpl();
        if (requestCardDao == null)
            requestCardDao = new RequestCardDaoImpl();
        if (mongoDbService == null)
            mongoDbService = new MongoDbServiceImpl();
        if (gameConfigServiceImpl == null)
            gameConfigServiceImpl = new GameConfigServiceImpl();
        return true;
    }
}

