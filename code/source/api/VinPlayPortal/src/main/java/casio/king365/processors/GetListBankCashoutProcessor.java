/*
 * Decompiled with CFR 0.144.
 * 
 * Could not load the following classes:
 *  com.hazelcast.core.IMap
 *  com.vinplay.usercore.service.impl.MarketingServiceImpl
 *  com.vinplay.usercore.service.impl.UserServiceImpl
 *  com.vinplay.usercore.utils.GameCommon
 *  com.vinplay.usercore.utils.UserMakertingUtil
 *  com.vinplay.vbee.common.cp.BaseProcessor
 *  com.vinplay.vbee.common.cp.Param
 *  com.vinplay.vbee.common.enums.StatusGames
 *  com.vinplay.vbee.common.hazelcast.HazelcastClientFactory
 *  com.vinplay.vbee.common.messages.UserMarketingMessage
 *  com.vinplay.vbee.common.models.SocialModel
 *  com.vinplay.vbee.common.models.UserModel
 *  com.vinplay.vbee.common.response.LoginResponse
 *  com.vinplay.vbee.common.utils.VinPlayUtils
 *  javax.servlet.http.HttpServletRequest
 *  org.apache.log4j.Logger
 */
package casio.king365.processors;

import casio.king365.Constants;
import casio.king365.GU;
import casio.king365.util.KingUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.vinplay.dal.service.impl.CacheServiceImpl;
import com.vinplay.usercore.service.UserService;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.exceptions.KeyNotFoundException;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.models.UserModel;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import vn.yotel.payment.buycard.BuyCardClient;
import vn.yotel.payment.buycard.api.Request;
import vn.yotel.payment.buycard.entities.CardReq;
import vn.yotel.yoker.dao.CashinItemDao;
import vn.yotel.yoker.dao.CashoutItemDao;
import vn.yotel.yoker.dao.CashoutRequestDao;
import vn.yotel.yoker.dao.impl.CashinItemDaoImpl;
import vn.yotel.yoker.dao.impl.CashoutItemDaoImpl;
import vn.yotel.yoker.dao.impl.CashoutRequestDaoImpl;
import vn.yotel.yoker.domain.CashinItem;
import vn.yotel.yoker.domain.CashoutItem;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class GetListBankCashoutProcessor
implements BaseProcessor<HttpServletRequest, String> {
    
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    static final String CACHE_KEY = "list_bank_out";

    HazelcastInstance client;
    IMap<String, UserModel> userMap;
    UserService userService;
    CashoutItemDao cashoutItemDao;
    CashinItemDao cashinItemDao;
    CashoutRequestDao cashoutRequestDao;
    CacheServiceImpl cacheService;

    public String execute(Param<HttpServletRequest> param) throws JSONException {
        JSONObject res = new JSONObject();
        try {
            res.put("is_success", false);
            res.put("desc", "Có lỗi.");
            if(!init())
                return res.toString();

            HttpServletRequest request = (HttpServletRequest)param.get();
            String un = request.getParameter("un") != null ? request.getParameter("un") : "";
            String demonition = request.getParameter("demonition") != null ? request.getParameter("demonition") : "";
            KingUtil.printLog( "GetListBankCashoutProcessor - un: "+un+", demonition: "+demonition);
            if(demonition.equals("")){
                res.put("desc", "Có lỗi tham số.");
                return res.toString();
            }

            // Kiểm tra cache
            if(!cacheService.checkKeyExist(CACHE_KEY)){
                // Nếu chưa có thì add cache
                getRemoteShipperInfoAndAddToCache(demonition);
            }

            res.put("is_success", true);
            res.put("data", new JSONObject(cacheService.getValueStr(CACHE_KEY)));
            res.put("desc", "Thành công.");
        } catch (UnExpectedException e) {
            e.printStackTrace();
            GU.sendOperation(e.getMessage());
            KingUtil.printException("GetListBankCashoutProcessor.UnExpectedException", e);
            res.put("desc", "Hệ thống xảy ra lỗi. Quý khách vui lòng liên hệ CSKH để biết thêm thông tin chi tiết.");
            return res.toString();
        }  catch (Exception e) {
            e.printStackTrace();
            KingUtil.printException("GetListBankCashoutProcessor", e);
            GU.sendOperation("GetListBankCashoutProcessor Exception: " + KingUtil.printException("GetListBankCashoutProcessor", e));
            return "{\"is_success\":false,\"desc\":\"Lỗi hệ thống! Vui lòng liên hệ CSKH để biết thêm thông tin chi tiết.\"}";
        }
        return res.toString();
    }

    private void getRemoteShipperInfoAndAddToCache(String demonition) throws Exception {
        KingUtil.printLog("GetListBankCashoutProcessor getRemoteShipperInfoAndAddToCache()");
        CardReq cardReq = new CardReq();
        cardReq.partner_id = Constants.BuyCardClient.partnerId;
        cardReq.denomination = Long.parseLong(demonition);
        cardReq.quantity = 1;
        cardReq.telco = "BANK";
        String partnerReference = "requestId" + System.currentTimeMillis();
        cardReq.partner_reference = partnerReference;
        cardReq.signature = "";
        String postResult = Request.getInstance().getListBankCashout(
                cardReq
                , Constants.BuyCardClient.privateKey
                , Constants.BuyCardClient.bankUrl+"/list");
        KingUtil.printLog( "GetListBankCashoutProcessor - postResult: "+postResult.trim());
        if(postResult != null && !postResult.equals("")) {
            JSONObject data = new JSONObject(postResult.trim());
            cacheService.setValue(CACHE_KEY, data.toString(), 300, TimeUnit.SECONDS);
        }
        else {
            GU.sendOperation("GetListBankCashoutProcessor lỗi listBankDataFromCache");
            throw new UnExpectedException("listBank rỗng");
        }
    }

    private boolean init() {
        if ((client = HazelcastClientFactory.getInstance()) == null) {
            KingUtil.printLog("CardRequestActionProcessor - init(), Lỗi khởi tạo hazelcast");
        }
        if(userMap == null)
            userMap = client.getMap("users");
        if(cashinItemDao == null)
            cashinItemDao = new CashinItemDaoImpl();
        if(cashoutItemDao == null)
            cashoutItemDao = new CashoutItemDaoImpl();
        if(cashoutRequestDao == null)
            cashoutRequestDao = new CashoutRequestDaoImpl();
        if(userService == null)
            userService = new UserServiceImpl();
        if(cacheService == null)
            cacheService = new CacheServiceImpl();
        return true;
    }

    private class UnExpectedException extends Exception {
        public UnExpectedException(String message) {
            super(message);
        }
    }
}

