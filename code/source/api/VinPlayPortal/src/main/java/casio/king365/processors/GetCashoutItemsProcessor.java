package casio.king365.processors;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vinplay.usercore.service.UserService;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import org.apache.log4j.Logger;
import vn.yotel.yoker.dao.CashoutItemDao;
import vn.yotel.yoker.dao.CashoutRequestDao;
import vn.yotel.yoker.dao.impl.CashoutItemDaoImpl;
import vn.yotel.yoker.dao.impl.CashoutRequestDaoImpl;
import vn.yotel.yoker.domain.CashinItem;
import vn.yotel.yoker.domain.CashoutItem;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Processor này chỉ được dùng ở backend
 */
public class GetCashoutItemsProcessor
        implements BaseProcessor<HttpServletRequest, String> {

    private static final Logger logger = Logger.getLogger((String) "api");
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    UserService userService;
    CashoutItemDao cashoutItemDao;
    CashoutRequestDao cashoutRequestDao;

    public String execute(Param<HttpServletRequest> param) {
        if (!init())
            return "{\"is_success\":false,\"desc\":\"Lỗi hệ thống\"}";
        HttpServletRequest request = (HttpServletRequest) param.get();
        return getListCashoutItem();
    }

    private boolean init() {
        cashoutItemDao = new CashoutItemDaoImpl();
        cashoutRequestDao = new CashoutRequestDaoImpl();
        userService = new UserServiceImpl();
        return true;
    }

    private String getListCashoutItem() {
        logger.debug("CardRequestActionProcessor - getListCashoutItem()");
        List<CashoutItem> list = cashoutItemDao.findAll();
        Collections.sort(list, Comparator.comparing(CashoutItem::getMoney));
        if (list == null) {
            return "[]";
        }
        List<CashoutItem> activeItems = list.stream().filter(cashoutItem -> cashoutItem.getStatus() == 1).collect(Collectors.toList());
        return gson.toJson(activeItems);
    }
}

