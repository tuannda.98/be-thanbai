package casio.king365.service;

import casio.king365.Constants;
import casio.king365.util.KingUtil;
import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;
import com.vinplay.dal.service.impl.CacheServiceImpl;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.TimeUnit;

public class ReloadBankInfoService implements Runnable {
    @Override
    public void run() {

        try {
            CacheServiceImpl cacheService = new CacheServiceImpl();

            // Load Momo shippers
            KingUtil.printLog("Load Momo shippers...");
            Long requestTime = System.currentTimeMillis();
            String requestId = "requestId" + requestTime;
            String signature = KingUtil.MD5(requestId + "|" + requestTime + "|" + Constants.MomoInfo.partnerName + "|" + Constants.MomoInfo.partnerKey);

            String urlString = Constants.MomoInfo.apiGetShippersUrl + "?username=" + Constants.MomoInfo.partnerName
                    + "&request_id=" + requestId + "&request_time=" + requestTime + "&signature=" + signature;
            KingUtil.printLog("GetShippersMomoProcessor urlString: " + urlString);

            URL url = new URL(urlString);
            URLConnection conn = url.openConnection();
            conn.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0");

            String listMomoDataFromCache;
            try (InputStream inputStream = conn.getInputStream();
                 InputStreamReader inputStreamReader = new InputStreamReader(
                         inputStream, Charsets.UTF_8)) {
                listMomoDataFromCache = CharStreams.toString(inputStreamReader).trim();
            }

            // String getResultStr = "{\"code\":200,\"message\":\"Success\",\"data\":{\"partnerName\":\"hanoi\",\"accounts\":[{\"phone\":\"0904588756\",\"isMaxquota\":0,\"message\":\"OK\",\"name\":\"TON NU HUONG BINH\"}]}}";
            KingUtil.printLog("listMomoDataFromCache: " + listMomoDataFromCache);
            cacheService.setValue("listMomoShippers", listMomoDataFromCache, 30, TimeUnit.MINUTES);

            // Lay danh sach bank accounts
            requestTime = System.currentTimeMillis();
            requestId = "requestId" + requestTime;
            signature = KingUtil.MD5(requestId + "|" + requestTime + "|" +
                    Constants.BankInInfo.partnerName + "|" + Constants.BankInInfo.partnerKey);
            String getListAccUrl = Constants.BankInInfo.getAccUrl + "?request_id=" + requestId
                    + "&username=" + Constants.BankInInfo.partnerName + "&request_time=" + requestTime
                    + "&signature=" + signature;
            String getListAccResultStr = KingUtil.get(getListAccUrl).trim();
            KingUtil.printLog(("getListAccResultStr: " + getListAccResultStr));
            cacheService.setValue("listBankAcc", getListAccResultStr, 30, TimeUnit.MINUTES);

            // Lay danh sach bank
            String getListBankUrl = Constants.BankInInfo.getBankUrl + "?request_id=" + requestId
                    + "&username=" + Constants.BankInInfo.partnerName + "&request_time=" + requestTime
                    + "&signature=" + signature;
            String getListBankResultStr = KingUtil.get(getListBankUrl).trim();
            KingUtil.printLog(("getListBankResultStr: " + getListBankResultStr));
            cacheService.setValue("listBank", getListBankResultStr, 30, TimeUnit.MINUTES);

//            cacheService.setValue("listMomoShippers","");
//            cacheService.setValue("listBankAcc","");
//            cacheService.setValue("listBank","");
        } catch (Exception e) {
            e.printStackTrace();
            KingUtil.printException("ReloadBankInfoService", e);
        }
    }
}
