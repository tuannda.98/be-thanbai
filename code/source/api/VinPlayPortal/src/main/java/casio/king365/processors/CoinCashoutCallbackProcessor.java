package casio.king365.processors;

import casio.king365.Constants;
import casio.king365.GU;
import casio.king365.util.KingUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vinplay.dichvuthe.service.RechargeService;
import com.vinplay.dichvuthe.service.impl.RechargeServiceImpl;
import com.vinplay.usercore.logger.MoneyLogger;
import com.vinplay.usercore.service.MailBoxService;
import com.vinplay.usercore.service.UserService;
import com.vinplay.usercore.service.impl.GameConfigServiceImpl;
import com.vinplay.usercore.service.impl.MailBoxServiceImpl;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.response.ResultGameConfigResponse;
import org.json.JSONObject;
import vn.yotel.yoker.dao.CashinItemDao;
import vn.yotel.yoker.dao.CashinNotifyDao;
import vn.yotel.yoker.dao.CashoutItemDao;
import vn.yotel.yoker.dao.CashoutRequestDao;
import vn.yotel.yoker.dao.impl.CashinItemDaoImpl;
import vn.yotel.yoker.dao.impl.CashinNotifyDaoImpl;
import vn.yotel.yoker.dao.impl.CashoutItemDaoImpl;
import vn.yotel.yoker.dao.impl.CashoutRequestDaoImpl;
import vn.yotel.yoker.domain.CashinNotify;
import vn.yotel.yoker.domain.CashoutRequest;

import javax.persistence.PersistenceException;
import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.Date;
import java.util.stream.Collectors;

public class CoinCashoutCallbackProcessor
        implements BaseProcessor<HttpServletRequest, String> {

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    UserService userService;
    CashoutItemDao cashoutItemDao;
    CashoutRequestDao cashoutRequestDao;
    CashinNotifyDao cashinNotifyDao;
    CashinItemDao cashinItemDao;
    MailBoxService mailBoxService;

    public int getShouldWarningProcessingTimeInMillisecond() {
        return 5_000;
    }

    public int getShouldAlertProcessingTimeInMillisecond() {
        return 10_000;
    }

    public String execute(Param<HttpServletRequest> param) {
        String code = "";       // [Mã lỗi rút tiền]
        String desc = "";
        String message = "";
        String requestIdStr = ""; //  [request đối tác gửi sang - duy nhất]
        String serverReference = "";   // [Mã GD từ cổng]
        String transTime = "";
        String targetAddress = ""; // [Địa chỉ ví nhận]
        String transferId = "";    // [Mã giao dịch]
        String transferMessage = "";   // [Miêu tả thêm]
        String signature = "";
        String requestData = "";

        JSONObject res = new JSONObject();
        try {
            res.put("errorCode", 100);
            res.put("errorMessage", "Có lỗi");
            if (!init())
                return res.toString();

            HttpServletRequest request = param.get();
            String ipAddress = request.getRemoteAddr();
            requestData = request.getReader().lines().collect(Collectors.joining());
            KingUtil.printLog("CoinCashoutCallbackProcessor - execute(), requestData: " + requestData);
            JSONObject requestObj = new JSONObject(requestData);
            code = requestObj.has("code") ? requestObj.getString("code") : "";
            desc = requestObj.has("desc") ? requestObj.getString("desc") : "";
            message = requestObj.has("message") ? requestObj.getString("message") : "";
            requestIdStr = requestObj.has("request_id") ? requestObj.getString("request_id") : "";
            serverReference = requestObj.has("server_reference") ? requestObj.getString("server_reference") : "";
            transTime = requestObj.has("trans_time") ? requestObj.getString("trans_time") : "";
            targetAddress = requestObj.has("target_address") ? requestObj.getString("target_address") : "";
            transferId = requestObj.has("transfer_id") ? requestObj.getString("transfer_id") : "";
            transferMessage = requestObj.has("transfer_message") ? requestObj.getString("transfer_message") : "";
            signature = requestObj.has("signature") ? requestObj.getString("signature") : "";

            KingUtil.printLog("CoinCashoutCallbackProcessor - execute(), code: " + code
                    + ", desc: " + desc + ", message: " + message+ ", request_id: " + requestIdStr
                    + ", server_reference: " + serverReference + ", trans_time: " + transTime
                    + ", target_address: " + targetAddress + ", transfer_id: " + transferId
                    + ", transfer_message: " + transferMessage + ", signature: " + signature);

            if (code.equals("") || signature.equals("")) {
                res.put("errorMessage", "Sai param");
                return res.toString();
            }

            CashoutRequest cashoutRequest = cashoutRequestDao.findById(Integer.parseInt(requestIdStr));
            if(code.equals("200")) {
                cashoutRequest.setStatus(Constants.CashoutRequestStatus.COMPLETE_PAY);
                mailBoxService.sendMailPopupMessage(
                        cashoutRequest.getUserName(), "Kết quả rút Coin", "Hệ thống đã trả coin thành công");
            } else {
                cashoutRequest.setStatus(Constants.CashoutRequestStatus.ERROR);
                mailBoxService.sendMailPopupMessage(
                        cashoutRequest.getUserName(), "Kết quả rút Coin", "Có lỗi khi thực hiện giao dịch chuyển coin. Vui lòng liên hệ hỗ trợ.");
                GU.sendOperation("Có lỗi với kết quả rút coin, Lỗi: " + code + " - " + desc);
                GU.sendCustomerService("Có lỗi với kết quả rút coin, Data: " + requestData);
            }
            cashoutRequestDao.update(cashoutRequest);

            res.put("errorCode", 200);
            res.put("errorMessage", "Success");

        } catch (Exception e) {
            e.printStackTrace();
            KingUtil.printException("CoinCashoutCallbackProcessor", e);
            MoneyLogger.log("", "RechargeByCoin", 0L, 0L, "vin", "Nap vin qua coin", "1001", e.getMessage());
            GU.sendOperation("Có lỗi khi nhận kết quả nạp coin , Exception: " + KingUtil.printException(e));
            GU.sendCustomerService("Có lỗi khi nạp COIN, dữ liệu từ cổng coin: " + requestData);
            try {
                res.put("errorMessage", "Có lỗi: " + KingUtil.printException(e));
            } catch (Exception ex) {
                KingUtil.printException("CoinCashoutCallbackProcessor", e);
            }
        }
        return res.toString();
    }

    private boolean init() {
        if (cashoutItemDao == null)
            cashoutItemDao = new CashoutItemDaoImpl();
        if (cashoutRequestDao == null)
            cashoutRequestDao = new CashoutRequestDaoImpl();
        if (userService == null)
            userService = new UserServiceImpl();
        if (cashinNotifyDao == null)
            cashinNotifyDao = new CashinNotifyDaoImpl();
        if (cashinItemDao == null)
            cashinItemDao = new CashinItemDaoImpl();
        if(mailBoxService == null)
            mailBoxService = new MailBoxServiceImpl();
        return true;
    }
}
