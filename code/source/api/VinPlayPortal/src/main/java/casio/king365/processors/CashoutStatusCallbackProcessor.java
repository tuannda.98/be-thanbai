package casio.king365.processors;

import casio.king365.Constants;
import casio.king365.GU;
import casio.king365.util.KingUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vinplay.dichvuthe.service.RechargeService;
import com.vinplay.dichvuthe.service.impl.RechargeServiceImpl;
import com.vinplay.usercore.logger.MoneyLogger;
import com.vinplay.usercore.service.MailBoxService;
import com.vinplay.usercore.service.UserService;
import com.vinplay.usercore.service.impl.GameConfigServiceImpl;
import com.vinplay.usercore.service.impl.MailBoxServiceImpl;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.response.ResultGameConfigResponse;
import com.vinplay.vbee.common.utils.UserValidaton;
import org.json.JSONException;
import org.json.JSONObject;
import vn.yotel.yoker.dao.CashinItemDao;
import vn.yotel.yoker.dao.CashinNotifyDao;
import vn.yotel.yoker.dao.CashoutItemDao;
import vn.yotel.yoker.dao.CashoutRequestDao;
import vn.yotel.yoker.dao.impl.CashinItemDaoImpl;
import vn.yotel.yoker.dao.impl.CashinNotifyDaoImpl;
import vn.yotel.yoker.dao.impl.CashoutItemDaoImpl;
import vn.yotel.yoker.dao.impl.CashoutRequestDaoImpl;
import vn.yotel.yoker.domain.CashinNotify;
import vn.yotel.yoker.domain.CashoutRequest;
import vn.yotel.yoker.util.Util;

import javax.persistence.PersistenceException;
import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class CashoutStatusCallbackProcessor
        implements BaseProcessor<HttpServletRequest, String> {

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    UserService userService;
    CashoutItemDao cashoutItemDao;
    CashoutRequestDao cashoutRequestDao;
    CashinNotifyDao cashinNotifyDao;
    CashinItemDao cashinItemDao;

    public int getShouldWarningProcessingTimeInMillisecond() {
        return 5_000;
    }

    public int getShouldAlertProcessingTimeInMillisecond() {
        return 10_000;
    }

    public String execute(Param<HttpServletRequest> param) {

        JSONObject res = new JSONObject();
        try {
            res.put("errorCode", 500);
            res.put("errorMessage", "Có lỗi");
            if (!init())
                return res.toString();

            HttpServletRequest request = param.get();
            String ipAddress = request.getRemoteAddr();
            String requestData = request.getReader().lines().collect(Collectors.joining());
            KingUtil.printLog("CashoutStatusCallbackProcessor - execute(), requestData: " + requestData);
            JSONObject requestObj = new JSONObject(requestData);
            KingUtil.printLog("CashoutStatusCallbackProcessor - execute() 1");
            JSONObject requestDataObj = requestObj.getJSONObject("data");
            KingUtil.printLog("CashoutStatusCallbackProcessor - execute() 2");
            String partnerReferenceStr = requestDataObj.getString("partner_reference");
            KingUtil.printLog("CashoutStatusCallbackProcessor - execute() 3");
            if(partnerReferenceStr == null || partnerReferenceStr.equals("")){
                res.put("errorMessage", "partner_reference empty");
                return res.toString();
            }
            KingUtil.printLog("CashoutStatusCallbackProcessor - execute() 4");
            String md5Secret = KingUtil.MD5(Constants.PaymentGatewayInfo.callbackKey+partnerReferenceStr);
            KingUtil.printLog("CashoutStatusCallbackProcessor - execute() 5");
            if(!requestObj.getString("signature").equals(md5Secret)){
                res.put("errorCode", 500);
                res.put("errorMessage", "Sai signature");
                return res.toString();
            }
            KingUtil.printLog("CashoutStatusCallbackProcessor - execute() 6");
            int partnerReference = Integer.parseInt(partnerReferenceStr);
            KingUtil.printLog("CashoutStatusCallbackProcessor - execute() 7");
            // Tìm cashout request trong db dựa theo id (partner reference)
            CashoutRequest cashoutRequest = cashoutRequestDao.findById(partnerReference);
            KingUtil.printLog("CashoutStatusCallbackProcessor - execute() 8");
            if(cashoutRequest == null){
                // TODO: 1 thời gian sau cần sửa đoạn này, báo lỗi nếu k tìm thấy cashoutRequest
                res.put("errorCode", 200);
                res.put("errorMessage", "Success");
                return res.toString();
            }
            KingUtil.printLog("CashoutStatusCallbackProcessor - execute() 9");
            if(cashoutRequest.getStatus() != Constants.CashoutRequestStatus.PENDING){
                res.put("errorCode", 500);
                res.put("errorMessage", "Không được cập nhật trạng thái khác nữa");
                return res.toString();
            }
            KingUtil.printLog("CashoutStatusCallbackProcessor - execute() 10");
            List<Integer> listErrorCode = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 20, 21); // 22 nghi vaans
            int code = requestObj.getInt("code");
            if (code == 0) {
                // Thành công
                cashoutRequest.setStatus(Constants.CashoutRequestStatus.COMPLETE_PAY);
            } else if (listErrorCode.contains(code)) {
                cashoutRequest.setStatus(Constants.CashoutRequestStatus.ERROR);
                if(code == 21){
                    GU.sendCustomerService("Yêu cầu rút có mã: "+cashoutRequest.getId()+" có kết quả lỗi. Cần đội " +
                            "vận hành tra cứu lại, và hoàn tiền nếu cần thiết");
                }
            }
            KingUtil.printLog("CashoutStatusCallbackProcessor - execute() 11");
            cashoutRequestDao.update(cashoutRequest);
            KingUtil.printLog("CashoutStatusCallbackProcessor - execute() 12");
            res.put("errorCode", 200);
            res.put("errorMessage", "Success");
        } catch (NumberFormatException e){
            try {
                res.put("errorMessage", "Có lỗi, không tìm thấy reference");
            } catch (JSONException jsonException) {
                KingUtil.printException("CashoutStatusCallbackProcessor", jsonException);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            KingUtil.printException("CashoutStatusCallbackProcessor", e);
            GU.sendOperation("Có lỗi khi nhận callback CashoutStatusCallbackProcessor, Exception: " + KingUtil.printException(e));
            try {
                res.put("errorMessage", "Có lỗi: " + KingUtil.printException(e));
            } catch (Exception ex) {
                KingUtil.printException("CashoutStatusCallbackProcessor", e);
            }
        }
        return res.toString();
    }

    private boolean init() {
        if (cashoutItemDao == null)
            cashoutItemDao = new CashoutItemDaoImpl();
        if (cashoutRequestDao == null)
            cashoutRequestDao = new CashoutRequestDaoImpl();
        if (userService == null)
            userService = new UserServiceImpl();
        if (cashinNotifyDao == null)
            cashinNotifyDao = new CashinNotifyDaoImpl();
        if (cashinItemDao == null)
            cashinItemDao = new CashinItemDaoImpl();
        return true;
    }
}
