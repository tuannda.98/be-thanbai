package casio.king365.processors;

import casio.king365.Constants;
import casio.king365.GU;
import casio.king365.service.MongoDbService;
import casio.king365.service.impl.MongoDbServiceImpl;
import casio.king365.util.KingUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hazelcast.core.IMap;
import com.vinplay.usercore.dao.impl.LuckyDaoImpl;
import com.vinplay.usercore.service.MailBoxService;
import com.vinplay.usercore.service.UserService;
import com.vinplay.usercore.service.impl.GameConfigServiceImpl;
import com.vinplay.usercore.service.impl.MailBoxServiceImpl;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.hazelcast.HazelcastUtils;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.response.BaseResponseModel;
import com.vinplay.vbee.common.response.ResultGameConfigResponse;
import org.json.JSONObject;
import vn.yotel.payment.buycard.api.Request;
import vn.yotel.payment.buycard.entities.CardReq;
import vn.yotel.yoker.dao.CashoutItemDao;
import vn.yotel.yoker.dao.CashoutRequestDao;
import vn.yotel.yoker.dao.RequestCardDao;
import vn.yotel.yoker.dao.impl.CashoutItemDaoImpl;
import vn.yotel.yoker.dao.impl.CashoutRequestDaoImpl;
import vn.yotel.yoker.dao.impl.RequestCardDaoImpl;
import vn.yotel.yoker.domain.RequestCard;
import vn.yotel.yoker.domain.RequestIap;
import vn.yotel.yoker.iap.client.AppleClient;
import vn.yotel.yoker.iap.client.GoogleClient;
import vn.yotel.yoker.promotion.PurchasePromotion;
import vn.yotel.yoker.service.RequestIapBo;
import vn.yotel.yoker.service.impl.RequestIapBoImpl;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 *
 */
public class IapTopupProcessor
        implements BaseProcessor<HttpServletRequest, String> {

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy/MM/dd HH:mm:ss").create();

    UserService userService;
    CashoutItemDao cashoutItemDao;
    CashoutRequestDao cashoutRequestDao;
    RequestCardDao requestCardDao;
    MongoDbService mongoDbService;
    GameConfigServiceImpl gameConfigServiceImpl;

    public String execute(Param<HttpServletRequest> param) {
        RequestIapBo requestIapBo = new RequestIapBoImpl();
        RequestIap requestIap = new RequestIap();
        JSONObject res = new JSONObject();
        try {
            res.put("is_success", false);
            res.put("desc", "Có lỗi");
            if (!init())
                return res.toString();

            HttpServletRequest request = (HttpServletRequest) param.get();
            String nickname = request.getParameter("nn") != null ? request.getParameter("nn") : "";
            String item = request.getParameter("item") != null ? request.getParameter("item") : "";
            String transId = request.getParameter("transId") != null ? request.getParameter("transId") : "";
            String transDate = request.getParameter("transDate") != null ? request.getParameter("transDate") : "";
            String provider = request.getParameter("provider") != null ? request.getParameter("provider") : "";
            String receiptData = request.getParameter("receipt-data") != null ? request.getParameter("receipt-data") : "";
            String productId = request.getParameter("productId") != null ? request.getParameter("productId") : "";
            String googlePackageName = request.getParameter("g-package-name") != null ? request.getParameter("g-package-name") : "";
            String googleToken = request.getParameter("g-token") != null ? request.getParameter("g-token") : "";
            KingUtil.printLog("IapTopupProcessor item: " + item + ", transId: " + transId + ", productId: " + productId
                    + ", transDate: " + transDate + ", provider: " + provider + ", receiptData: " + receiptData + ", nickname: " + nickname);
            if (item.isEmpty() || transId.isEmpty() || productId.isEmpty() || transDate.isEmpty() || provider.isEmpty() || receiptData.isEmpty()) {
                res.put("desc", "Lỗi tham số");
                return res.toString();
            }
            UserCacheModel user = userService.getCacheUserIgnoreCase(nickname);
            if (user == null) {
                res.put("desc", "Không tìm thấy người dùng");
                return res.toString();
            }
            // Validate
            List<String> IAP_PROVIDERS = Arrays.asList("APPLE", "GOOGLE", "WINDOWS");
            if (!IAP_PROVIDERS.contains(provider.toUpperCase())) {
                res.put("desc", "Sai provider");
                return res.toString();
            }

            if(provider.toUpperCase().equals("APPLE")) {
                if (!AppleClient.isPurchased(receiptData, transId, productId)) {
                    res.put("desc", "Lỗi check purchase");
                    return res.toString();
                }
            }
            if(provider.toUpperCase().equals("GOOGLE")) {
                GoogleClient GoogleClient = new GoogleClient();
                if (!GoogleClient.isPurchased(googlePackageName, productId, googleToken)) {
                    res.put("desc", "Lỗi check purchase");
                    return res.toString();
                }
            }

            requestIap.setTransDate(transDate);
            requestIap.setProvider(provider);
            requestIap.setTransId(transId);
            requestIap.setUserId(user.getId());
            requestIap.setUsername(user.getNickname());
            requestIap.setDatetime(new Timestamp(new Date().getTime()));
            ResultGameConfigResponse strPurchaseItemCfg = gameConfigServiceImpl.getGameConfigAdmin("iap_purchase_item", "").get(0);
            if (strPurchaseItemCfg == null) {
                KingUtil.printLog("strPurchaseItemCfg == null");
                res.put("desc", "Lỗi config");
                return res.toString();
            }
            String strPurchaseItem = strPurchaseItemCfg.value;
            /*String strPurchaseItem = "{\n" +
                    " \"atm.id1\":{\"koin\":10000,\"money\":10000},\n" +
                    " \"atm.id2\":{\"koin\":20000,\"money\":20000},\n" +
                    " \"atm.id3\":{\"koin\":30000,\"money\":30000},\n" +
                    " \"atm.id4\":{\"koin\":40000,\"money\":40000},\n" +
                    " \"atm.id5\":{\"koin\":50000,\"money\":50000},\n" +
                    " \"winbit.id1\":{\"koin\":10000,\"money\":10000},\n" +
                    " \"winbit.id2\":{\"koin\":20000,\"money\":20000},\n" +
                    " \"winbit.id3\":{\"koin\":30000,\"money\":30000},\n" +
                    " \"winbit.id4\":{\"koin\":40000,\"money\":40000},\n" +
                    " \"winbit.id5\":{\"koin\":50000,\"money\":50000},\n" +
                    " \"com.zovip.id1\":{\"koin\":10000,\"money\":10000},\n" +
                    " \"com.zovip.id2\":{\"koin\":20000,\"money\":20000},\n" +
                    " \"com.zovip.id3\":{\"koin\":30000,\"money\":30000},\n" +
                    " \"com.zovip.id4\":{\"koin\":40000,\"money\":40000},\n" +
                    " \"com.zovip.id5\":{\"koin\":50000,\"money\":50000},\n" +
                    " \"Topone_1\":{\"koin\":10000,\"money\":20000},\n" +
                    " \"Topone_2\":{\"koin\":50000,\"money\":100000},\n" +
                    " \"Topone_3\":{\"koin\":100000,\"money\":200000},\n" +
                    " \"Topone_4\":{\"koin\":250000,\"money\":500000},\n" +
                    " \"Topone_5\":{\"koin\":500000,\"money\":1000000},\n" +
                    " \"netfunny_1\":{\"koin\":10000,\"money\":20000},\n" +
                    " \"netfunny_2\":{\"koin\":50000,\"money\":100000},\n" +
                    " \"netfunny_3\":{\"koin\":100000,\"money\":200000},\n" +
                    " \"netfunny_4\":{\"koin\":250000,\"money\":500000},\n" +
                    " \"netfunny_5\":{\"koin\":500000,\"money\":1000000},\n" +
                    " \"xeng_1\":{\"koin\":10000,\"money\":20000},\n" +
                    " \"xeng_2\":{\"koin\":50000,\"money\":100000},\n" +
                    " \"xeng_3\":{\"koin\":100000,\"money\":200000},\n" +
                    " \"xeng_4\":{\"koin\":250000,\"money\":500000},\n" +
                    " \"xeng_5\":{\"koin\":500000,\"money\":1000000}\n" +
                    " }";*/
            KingUtil.printLog("productId: "+productId);
            KingUtil.printLog("strPurchaseItem: "+strPurchaseItem);
            ResultGameConfigResponse iapPromotionCfgObj = gameConfigServiceImpl.getGameConfigAdmin("iap_promotion_cfg", "").get(0);
            if (iapPromotionCfgObj == null) {
                KingUtil.printLog("iapPromotionCfgObj == null");
                res.put("desc", "Lỗi config");
                return res.toString();
            }
            String iapPromotionCfg = iapPromotionCfgObj.value;
            // String iapPromotionCfg = "{\"type\":1,\"fromDate\":\"2015/12/23 00:00:00\",\"toDate\":\"2015/12/24 01:00:00\",\"multiply\":2.0,\"first_multiply\":3.0,\"out_target\":20000.0,\"out_multiply\":2.0,\"value10\":2.0,\"value20\":2.0,\"value30\":2.0,\"value50\":2.0,\"value100\":2.0,\"value200\":2.0,\"value300\":2.0,\"value500\":2.0}";
            PurchasePromotion purchasePromotion = gson.fromJson(iapPromotionCfg, PurchasePromotion.class);
            JSONObject configObj = new JSONObject(strPurchaseItem);
            String itemStr = configObj.optString(productId);
            if ((itemStr == null) || itemStr.length() == 0) {
                requestIap.setNote("System configuration error. There is no item: " + item);
                requestIap.setProcessStatus((byte) -1);
                res.put("desc", "System configuration error. There is no item: " + item);
                return res.toString();
            }

            JSONObject jsonItem = new JSONObject(itemStr);
            Integer koin = jsonItem.getInt("koin");
            Integer moneyAmount = jsonItem.getInt("money");
            long goldChange = 0;
            try {
                double multiply = 1;
                if (purchasePromotion != null) {
                    Date now = new Date();
                    if (now.after(purchasePromotion.getFromDate()) && now.before(purchasePromotion.getToDate()))
                        multiply = purchasePromotion.getMultiply();
                }
                if (multiply < 1)
                    multiply = 1;
                KingUtil.printLog("Apply card promotion " + multiply + " times, goldChange " + goldChange);
                goldChange = (int) (koin * multiply);
            } catch (Exception e) {
                KingUtil.printException("IapTopupProcessor", e);
                GU.sendOperation("Có lỗi khi iap Topup, Exception: " + KingUtil.printException(e));
            }
            requestIap.setEqKoin(goldChange);
            requestIap.setMoney(moneyAmount);

            KingUtil.printLog("topup_iap - goldChange " + moneyAmount + "," + goldChange);
            BaseResponseModel subMoneyResult = userService.updateMoneyWithNotify(user.getNickname()
                    , goldChange, "topup_iap", "Hệ thống"
                    , "Nạp tiền Apple IAP: số tiền:" + moneyAmount + " VNĐ!", true);
            requestIap.setProcessStatus((byte) 1);
            requestIap.setNote("Giao dịch thành công");


            res.put("is_success", true);
            res.put("desc", "Thành công");
            res.put("currentMoney", userService.getCurrentMoneyUserCache(user.getNickname(), "vin"));

            KingUtil.printLog("1 requestIap: "+requestIap);
        } catch (Exception e) {
            e.printStackTrace();
            KingUtil.printException("IapTopupProcessor", e);
            GU.sendOperation("Có lỗi khi iap Topup, Exception: " + KingUtil.printException(e));
        } finally {
            try {
                if(requestIap != null) {
                    KingUtil.printLog("2 requestIap: "+requestIap);
                    KingUtil.printLog("requestIap != null");
                    requestIapBo.create(requestIap);
                } else {
                    KingUtil.printLog("requestIap == null");
                }
            } catch (Exception e) {
                e.printStackTrace();
                KingUtil.printException("IapTopupProcessor finally", e);
                GU.sendOperation("Có lỗi khi iap Topup, Exception: " + KingUtil.printException(e));
            }
        }
        return res.toString();
    }

    private boolean init() {
        if (cashoutItemDao == null)
            cashoutItemDao = new CashoutItemDaoImpl();
        if (cashoutRequestDao == null)
            cashoutRequestDao = new CashoutRequestDaoImpl();
        if (userService == null)
            userService = new UserServiceImpl();
        if (requestCardDao == null)
            requestCardDao = new RequestCardDaoImpl();
        if (mongoDbService == null)
            mongoDbService = new MongoDbServiceImpl();
        if (gameConfigServiceImpl == null)
            gameConfigServiceImpl = new GameConfigServiceImpl();
        return true;
    }
}

