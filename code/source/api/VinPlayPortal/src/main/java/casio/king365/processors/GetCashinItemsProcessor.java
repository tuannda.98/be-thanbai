package casio.king365.processors;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vinplay.usercore.service.UserService;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import org.apache.log4j.Logger;
import vn.yotel.yoker.dao.CashinItemDao;
import vn.yotel.yoker.dao.impl.CashinItemDaoImpl;
import vn.yotel.yoker.domain.CashinItem;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Processor này chỉ được dùng ở backend
 */
public class GetCashinItemsProcessor
        implements BaseProcessor<HttpServletRequest, String> {

    private static final Logger logger = Logger.getLogger((String) "api");
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    UserService userService;
    CashinItemDao cashinItemDao;

    public String execute(Param<HttpServletRequest> param) {
        if (!init())
            return "{\"is_success\":false,\"desc\":\"Lỗi hệ thống\"}";
        HttpServletRequest request = (HttpServletRequest) param.get();
        return getListCashinItem();
    }

    private boolean init() {
        if (cashinItemDao == null)
            cashinItemDao = new CashinItemDaoImpl();
        if (userService == null)
            userService = new UserServiceImpl();
        return true;
    }

    String getListCashinItem() {
        logger.debug((Object) "CardRequestActionProcessor - getListCashinItem() 123");
        List<CashinItem> list = cashinItemDao.findAll();
        Collections.sort(list, Comparator.comparing(CashinItem::getMoney));
        if (list != null) {
            return gson.toJson(list);
        } else
            return "[]";
    }
}
