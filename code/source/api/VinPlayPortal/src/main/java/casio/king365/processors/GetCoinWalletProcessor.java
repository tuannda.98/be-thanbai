/*
 * Decompiled with CFR 0.144.
 *
 * Could not load the following classes:
 *  com.hazelcast.core.IMap
 *  com.vinplay.usercore.service.impl.MarketingServiceImpl
 *  com.vinplay.usercore.service.impl.UserServiceImpl
 *  com.vinplay.usercore.utils.GameCommon
 *  com.vinplay.usercore.utils.UserMakertingUtil
 *  com.vinplay.vbee.common.cp.BaseProcessor
 *  com.vinplay.vbee.common.cp.Param
 *  com.vinplay.vbee.common.enums.StatusGames
 *  com.vinplay.vbee.common.hazelcast.HazelcastClientFactory
 *  com.vinplay.vbee.common.messages.UserMarketingMessage
 *  com.vinplay.vbee.common.models.SocialModel
 *  com.vinplay.vbee.common.models.UserModel
 *  com.vinplay.vbee.common.response.LoginResponse
 *  com.vinplay.vbee.common.utils.VinPlayUtils
 *  javax.servlet.http.HttpServletRequest
 *  org.apache.log4j.Logger
 */
package casio.king365.processors;

import casio.king365.Constants;
import casio.king365.GU;
import casio.king365.core.HCMap;
import casio.king365.util.KingUtil;
import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;
import com.hazelcast.core.IMap;
import com.vinplay.dal.service.impl.CacheServiceImpl;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.exceptions.KeyNotFoundException;
import com.vinplay.vbee.common.models.UserModel;
import org.json.JSONArray;
import org.json.JSONObject;
import vn.yotel.payment.CoinPaymentClient;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.TimeUnit;

public class GetCoinWalletProcessor
        implements BaseProcessor<HttpServletRequest, String> {

    UserServiceImpl userService;
    CacheServiceImpl cacheService;

    int warningProcessingTime = 1000;

    public String execute(Param<HttpServletRequest> param) {
        init();

        JSONObject res = new JSONObject();
        try {
            res.put("is_success", false);
            res.put("desc", "Không xác định.");

            HttpServletRequest request = (HttpServletRequest) param.get();
            String nickname = request.getParameter("nn") != null ? request.getParameter("nn") : "";
            String at = request.getParameter("at") != null ? request.getParameter("at") : "";
            KingUtil.printLog("GetCoinWalletProcessor - nickname: "+ nickname+", at: "+at);
            if(nickname.equals("") && at.equals("")){
                res.put("desc", "Thiếu tham số.");
                return res.toString();
            }
            UserModel userModel;
            if(nickname.equals("")){
                IMap<String, String> tokenMap = HCMap.getTokenMap();
                String nickname2 = tokenMap.get(at);
                userModel = userService.getUserByNickName(nickname);
            } else
                userModel = userService.getUserByNickName(nickname);
            if (userModel == null) {
                KingUtil.printLog(("GetCoinWalletProcessor - userModel == null"));
                res.put("desc", "Không tìm thấy người dùng.");
                return res.toString();
            }

            CoinPaymentClient.CoinWallet wallet = null;
            boolean isGetWalletSuccess = true;
            try {
                wallet = CoinPaymentClient.getWallet(nickname);
            } catch (Exception e){
                e.printStackTrace();
                KingUtil.printException("GetCoinWalletProcessor", e);
                isGetWalletSuccess = false;
            }
            if(!isGetWalletSuccess){
                try {
                    wallet = CoinPaymentClient.createWallet(nickname);
                } catch (Exception e){
                    e.printStackTrace();
                    KingUtil.printException("GetCoinWalletProcessor", e);
                }
            }
            KingUtil.printLog("GetCoinWalletProcessor - wallet: "+wallet);
            if(wallet == null){
                res.put("desc", "Lỗi lấy địa chỉ ví");
                return res.toString();
            }

            res.put("is_success", true);
            res.put("desc", "Thành công.");
            res.put("wallet", wallet.toJsonObject());
            res.put("send_content", userModel.getNickname());
        } catch (Exception e) {
            e.printStackTrace();
            KingUtil.printException("GetCoinWalletProcessor", e);
        }

        return res.toString();
    }

    void init() {
        if (userService == null)
            userService = new UserServiceImpl();
        if (cacheService == null)
            cacheService = new CacheServiceImpl();
    }

    public int getShouldWarningProcessingTimeInMillisecond() {
        return warningProcessingTime;
    }
}

