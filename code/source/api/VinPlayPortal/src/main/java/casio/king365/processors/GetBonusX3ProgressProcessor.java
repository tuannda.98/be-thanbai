package casio.king365.processors;

import casio.king365.entities.X3NapLanDau;
import casio.king365.service.MongoDbService;
import casio.king365.service.X3NapLanDauService;
import casio.king365.service.impl.MongoDbServiceImpl;
import casio.king365.service.impl.X3NapLanDauServiceImpl;
import casio.king365.util.KingUtil;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.response.BaseResponseModel;
import org.bson.Document;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;
import java.util.List;

public class GetBonusX3ProgressProcessor
        implements BaseProcessor<HttpServletRequest, String> {

    UserServiceImpl userService = new UserServiceImpl();
    MongoDbService mongodbService = new MongoDbServiceImpl();
    X3NapLanDauService x3NapLanDauService = new X3NapLanDauServiceImpl();

    int warningProcessingTime = 1000;

    public String execute(Param<HttpServletRequest> param) {
        JSONObject res = new JSONObject();
        try {
            res.put("is_success", false);
            res.put("desc", "Không xác định.");

            HttpServletRequest request = param.get();
            String nickName = request.getParameter("nn") != null ? request.getParameter("nn") : "";
            String channel = request.getParameter("channel") != null ? request.getParameter("channel") : "";
            String accesstoken = request.getParameter("at") != null ? request.getParameter("at") : "";
            KingUtil.printLog("GetBonusX3ProgressProcessor - nn: " + nickName + ", at: " + accesstoken + ", channel: " + channel);
            if (nickName.equals("") && accesstoken.equals("") && channel.equals("")) {
                res.put("desc", "Thiếu tham số.");
                return res.toString();
            }

            X3NapLanDau x3Obj = x3NapLanDauService.getX3NapLanDau(nickName, channel);
            if (x3Obj == null) {
                res.put("is_success", false);
                res.put("desc", "Chưa nạp lần đầu.");
                return res.toString();
            }
            if (x3Obj.getPercent() < 100) {    // Chưa hoàn thành
                res.put("is_success", false);
                res.put("desc", "Chưa hoàn thành.");
                return res.toString();
            }
            if (Instant.now().toEpochMilli() > x3Obj.getEndTime()) {   // Quá hạn
                res.put("is_success", false);
                res.put("desc", "Quá hạn.");
                return res.toString();
            }
            if (x3Obj.getReceivedBonus()) {   // Quá hạn
                res.put("is_success", false);
                res.put("desc", "Đã nhận thưởng rồi.");
                return res.toString();
            }

            // Thưởng
            BaseResponseModel moneyResult = userService.updateMoneyWithNotify(x3Obj.getNickname()
                    , x3Obj.getBonusAmount(), "X3NapLanDau", "Hệ thống"
                    , "Thưởng X3 nạp lần đầu", true);

            if (!moneyResult.isSuccess()) {
                res.put("is_success", false);
                res.put("desc", "Lỗi cộng thưởng: " + moneyResult.getErrorCode());
                return res.toString();
            }

            x3NapLanDauService.updateReceivedBonus(x3Obj.getNickname(), channel, true);

            // Khởi động Tiến trình của kênh nạp khác chưa hoàn thành
            // bằng cách đặt lại start time của tiến trình đó về thời điểm hiện tại
            List<X3NapLanDau> listX3Progress = x3NapLanDauService.getX3NapLanDau(nickName);
            for (X3NapLanDau x3 : listX3Progress) {
                if (x3.getReceivedBonus() || x3.getPercent() >= 100)
                    continue;
                if (Instant.now().toEpochMilli() > x3.getEndTime())
                    continue;
                KingUtil.printLog("set Active Time of x3Obj: "+x3+", new activeTime: "+Instant.now().toEpochMilli());
                x3NapLanDauService.updateActiveTime(x3.getNickname(), x3.getChannel(), Instant.now().toEpochMilli());
            }

            res.put("is_success", true);
            res.put("desc", "Thành công.");
        } catch (Exception e) {
            e.printStackTrace();
            KingUtil.printException("GetBonusX3ProgressProcessor", e);
        }

        return res.toString();
    }

    public int getShouldWarningProcessingTimeInMillisecond() {
        return warningProcessingTime;
    }

}
