package casio.king365.processors;

import casio.king365.GU;
import casio.king365.mqtt.MqttSender;
import casio.king365.service.MongoDbService;
import casio.king365.service.impl.MongoDbServiceImpl;
import casio.king365.util.KingUtil;
import casio.king365.util.StringUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hazelcast.core.IMap;
import com.vinplay.dichvuthe.response.RechargeResponse;
import com.vinplay.dichvuthe.service.RechargeService;
import com.vinplay.dichvuthe.service.impl.RechargeServiceImpl;
import com.vinplay.usercore.service.UserService;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.enums.ProviderType;
import com.vinplay.vbee.common.hazelcast.HazelcastUtils;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import game.modules.lobby.cmd.send.NapTheDienThoaiMsg;
import org.json.JSONObject;
import vn.yotel.yoker.dao.CashinItemDao;
import vn.yotel.yoker.dao.RequestCardDao;
import vn.yotel.yoker.dao.impl.CashinItemDaoImpl;
import vn.yotel.yoker.dao.impl.RequestCardDaoImpl;
import vn.yotel.yoker.domain.CashinItem;

import javax.servlet.http.HttpServletRequest;

public class CardRechargeProcessor
        implements BaseProcessor<HttpServletRequest, String> {

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    UserService userService;
    CashinItemDao cashinItemDao;
    RequestCardDao requestCardDao;
    MongoDbService mongoDbService;

    public String execute(Param<HttpServletRequest> param) {
        JSONObject res = new JSONObject();
        try {
            res.put("is_success", false);
            res.put("desc", "Lỗi không xác định");
            if (!init())
                return res.toString();

            HttpServletRequest request = (HttpServletRequest) param.get();
            String username = request.getParameter("un") != null ? request.getParameter("un") : "";
            String serial = request.getParameter("serial") != null ? request.getParameter("serial") : "";
            String pin = request.getParameter("pin") != null ? request.getParameter("pin") : "";
            String requestIdStr = request.getParameter("request_id") != null ? request.getParameter("request_id") : "";
            KingUtil.printLog(("CardRechargeProcessor - exe(), username: " + username
                    + ", serial: " + serial + ", pin: " + pin + ", requestIdStr: " + requestIdStr));
            if (username.equals("")
                    || serial.equals("")
                    || pin.equals("")
                    || requestIdStr.equals("")) {
                KingUtil.printLog(("Tham số rỗng"));
                res.put("desc", "Lỗi tham số");
                return res.toString();
            }

            if (StringUtil.hasSpecialCharacter(serial) || StringUtil.hasSpecialCharacter(pin)) {
                KingUtil.printLog(("Tham số rỗng"));
                res.put("desc", "Lỗi tham số");
                return res.toString();
            }

            UserModel userModel = userService.getUserByUserName(username);
            if (userModel == null) {
                KingUtil.printLog("userModel == null");
                res.put("desc", "Không tìm thấy người dùng.");
                return res.toString();
            }
            IMap<String, UserCacheModel> usersMap = HazelcastUtils.getUsersMap();
            UserCacheModel user = usersMap.get(userModel.getNickname());
            if (user == null) {
                KingUtil.printLogError("user == null");
                res.put("desc", "Không tìm thấy người dùng.");
                return res.toString();
            }

            CashinItem cashinItem = cashinItemDao.findById(Integer.parseInt(requestIdStr));
            String provider = cashinItem.getProvider();

            /*RequestCard reqCard = new RequestCard();
            reqCard.setDatetime(new Date());
            reqCard.setSerial(serial);
            reqCard.setPin(pin);
            reqCard.setProvider(provider);
            reqCard.setUserId(user.getId());
            reqCard.setUsername(user.getNickname());
            reqCard.setProcessStatus((byte) 0);
            reqCard.setMoney(cashinItem.getMoney());

            provider = provider.toUpperCase();
            String requestId = "requestIdStr" + System.currentTimeMillis();
            reqCard.setGwTransId(requestId);


            // save into mongo db too
            Map<String, Object> mapData = new HashMap<>();
            mapData.put("reference_id", requestId);
            mapData.put("nick_name", user.getNickname());
            mapData.put("provider", cashinItem.getProvider());
            mapData.put("serial", serial);
            mapData.put("pin", pin);
            mapData.put("amount", cashinItem.getMoney().intValue());
            mapData.put("status", 0);
            mapData.put("message", "");
            mapData.put("code", 0);
            mapData.put("time_log", LocalDate.now()+" "+ LocalTime.now());
            // bank.updateTime = document.getString("update_time");
            mapData.put("partner", "zoan");
            mapData.put("request_card_id", -1);
            mongoDbService.insert("dvt_recharge_by_card", mapData);
            Serializable resultSaveReqCard;
            Integer reqCardId = -1;
            try {
                ECardClient http = ECardClient.getInstance();
                String rep = http.createCard(requestId, provider, pin, serial, cashinItem.getMoney().intValue());
                logger.debug((Object) "rep: "+ rep);
                reqCard.setGwMessage(rep);
            } catch (Exception e) {
                logger.debug((Object) "Exception: "+ KingUtil.printException(e));
            } finally {
                resultSaveReqCard = requestCardDao.save(reqCard);
            }
            try {
                reqCardId = (Integer) resultSaveReqCard;
            } catch ( Exception e){
                logger.debug((Object) "Exception: "+ KingUtil.printException(e));
                logger.debug("fail cash resultSaveReqCard to Integer, try parseInt string");
                reqCardId = Integer.parseInt((String)resultSaveReqCard);
            }
            try {
                Map<String, Object> mapFindData = new HashMap<>();
                mapFindData.put("reference_id", requestId);
                Map<String, Object> mapUpdateData = new HashMap<>();
                 mapUpdateData.put("request_card_id", reqCardId);
                mongoDbService.findOneAndUpdate("dvt_recharge_by_card", mapFindData
                        , mapUpdateData);
            } catch ( Exception e){
                logger.debug((Object) "Exception: "+ KingUtil.printException(e));
            }*/

            try {
                ProviderType providerType = null;
                switch (cashinItem.getProvider()) {
                    case "VTT":
                        providerType = ProviderType.IZPAY_VIETTEL;
                        break;
                    case "VINA":
                        providerType = ProviderType.IZPAY_VINAPHONE;
                        break;
                    case "VMS":
                        providerType = ProviderType.IZPAY_MOBIFONE;
                        break;
                }
                if (providerType == null) {
                    KingUtil.printLogError("ERROR providerId == 0, cashinItem.getProvider(): " + cashinItem.getProvider());
                    res.put("desc", "Lỗi cấu hình. Vui lòng liên hệ CSKH.");
                    return res.toString();
                }
                RechargeService ser = new RechargeServiceImpl();
                RechargeResponse rechargeResponse = ser.rechargeByIzPay(user.getNickname()
                        , providerType
                        , serial
                        , pin
                        , cashinItem.getMoney().intValue()
                        , user.getPlatform(), user.getId());
                try {
                    NapTheDienThoaiMsg msg = new NapTheDienThoaiMsg();
                    if (res != null) {
                        msg.Error = (byte) rechargeResponse.getCode();
                        msg.currentMoney = rechargeResponse.getCurrentMoney();
                        msg.numFail = rechargeResponse.getFail();
                        msg.timeFail = rechargeResponse.getTime();
                    } else {
                        msg.Error = (byte) 1;
                    }
                    MqttSender.SendMsgToNickname(user.getNickname(), msg);
                } catch (Exception e) {
                    KingUtil.printException("CardRechargeProcessor", e);
                }
                res.put("data", rechargeResponse);
            } catch (Exception e) {
                KingUtil.printException("CardRechargeProcessor", e);
            }

            res.put("is_success", true);
            res.put("desc", "Hệ thống đang kiểm tra thẻ, sẽ thông báo kết quả với quý khách ngay sau khi hoàn thành.");
        } catch (Exception e) {
            e.printStackTrace();
            KingUtil.printException("CardRechargeProcessor", e);
            GU.sendOperation("Có lỗi khi nạp thẻ, , Exception: " + KingUtil.printException(e));
        }
        return res.toString();
    }

    @Override
    public int getShouldWarningProcessingTimeInMillisecond() {
        return 10000; /*2000*/
    }

    @Override
    public int getShouldAlertProcessingTimeInMillisecond() {
        return 10000; /*5000*/
    }

    private boolean init() {
        if (cashinItemDao == null)
            cashinItemDao = new CashinItemDaoImpl();
        if (userService == null)
            userService = new UserServiceImpl();
        if (requestCardDao == null)
            requestCardDao = new RequestCardDaoImpl();
        if (mongoDbService == null)
            mongoDbService = new MongoDbServiceImpl();
        return true;
    }
}
