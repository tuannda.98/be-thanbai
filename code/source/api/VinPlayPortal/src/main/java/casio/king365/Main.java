package casio.king365;

import com.vinplay.api.processors.vippoint.TopVippoint;
import com.vinplay.api.server.CorsFilter;
import com.vinplay.api.utils.PortalUtils;
import com.vinplay.dal.service.LogPortalService;
import com.vinplay.dal.service.impl.LogPortalServiceImpl;
import com.vinplay.dal.utils.PotUtils;
import com.vinplay.usercore.utils.PartnerConfig;
import com.vinplay.vbee.common.cp.BaseController;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.hazelcast.HazelcastLoader;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.rmq.RMQApi;
import com.vinplay.vbee.common.utils.UserValidaton;
import misa.TeleLauncher;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.eclipse.jetty.server.*;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.servlet.DispatcherType;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

// Main
public class Main {

    private static final Logger logger = Logger.getLogger("api");
    private static final Logger blackListIpLogger = Logger.getLogger("BlackListIpLogger");
    private static final String LOG_PROPERTIES_FILE = "config/log4j.properties";
    private static LogPortalService service = new LogPortalServiceImpl();
    private static String API_PORT = "8081";
    private static String SSL_PORT = "8443";
    private static BaseController<HttpServletRequest, String> controller;

    private static void initializeLogger() {
        Properties logProperties = new Properties();

        try {
            File file = new File("config/log4j.properties");
            logProperties.load(new FileInputStream(file));
            PropertyConfigurator.configure(logProperties);
            logger.info("Logging initialized.");
        } catch (IOException var2) {
            throw new RuntimeException("Unable to load logging property config/log4j.properties");
        }
    }

    public static void main(String[] args) {
        try {
            initializeLogger();
            logger.debug("STARTING PORTAL API SERVER .... !!!!");
            loadCommands();
            RMQApi.start("config/rmq.properties");
            PartnerConfig.ReadConfig();
            logger.debug(PartnerConfig.ESMSApiKey);
            logger.debug(PartnerConfig.ESMSSecretKey);
            HazelcastLoader.start();
            MongoDBConnectionFactory.init();
            UserValidaton.init();
            PortalUtils.loadGameConfig();
            PotUtils.init();
            TopVippoint.init();

            //===========
            // EXTENDED
            logger.debug("Start TeleLauncher..");
            TeleLauncher.main(args);
            //===========

            int port = Integer.parseInt(API_PORT);
            int sslPort = Integer.parseInt(SSL_PORT);
            Server server = new Server();
            ServerConnector connector = new ServerConnector(server);
            connector.setPort(port);
            connector.setIdleTimeout(30000L);
            HttpConfiguration https = new HttpConfiguration();
            https.addCustomizer(new SecureRequestCustomizer());
            SslContextFactory sslContextFactory = new SslContextFactory();
            sslContextFactory.setKeyStorePath("config/apivinplay.jks");
            sslContextFactory.setKeyStorePassword("vinplay@123");
            sslContextFactory.setKeyManagerPassword("vinplay@123");
            ServerConnector sslConnector = new ServerConnector(server, new ConnectionFactory[]{new SslConnectionFactory(sslContextFactory, "http/1.1"), new HttpConnectionFactory(https)});
            sslConnector.setPort(sslPort);
            ServletHandler handler = new ServletHandler();
            handler.addFilterWithMapping(CorsFilter.class, "/*", EnumSet.of(DispatcherType.REQUEST));
            handler.addServletWithMapping(Main.JeetyServlet.class, "/api");
            HandlerCollection handlerCollection = new HandlerCollection();
            handlerCollection.setHandlers(new Handler[]{handler});
            server.setHandler(handlerCollection);
            server.setConnectors(new Connector[]{connector, sslConnector});
            server.start();
            logger.info("PORTAL API SERVER Started ...!!!");
            server.join();
        } catch (Exception var10) {
            logger.info("PORTAL API SERVER Start error: " + var10.getMessage());
            var10.printStackTrace();
        }

    }

    private static void loadCommands() throws Exception {
        File file = new File("config/api_portal.xml");
        DocumentBuilderFactory dFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(file);
        doc.getDocumentElement().normalize();
        NodeList nodeList = doc.getElementsByTagName("portal");
        Element el = (Element) nodeList.item(0);
        API_PORT = el.getElementsByTagName("port").item(0).getTextContent();
        SSL_PORT = el.getElementsByTagName("ssl_port").item(0).getTextContent();
        Element cmds = (Element) el.getElementsByTagName("commands").item(0);
        NodeList cmdList = cmds.getElementsByTagName("command");
        HashMap<Integer, String> commandsMap = new HashMap();

        for (int i = 0; i < cmdList.getLength(); ++i) {
            Element eCmd = (Element) cmdList.item(i);
            Integer id = Integer.parseInt(eCmd.getElementsByTagName("id").item(0).getTextContent());
            String path = eCmd.getElementsByTagName("path").item(0).getTextContent();
            logger.debug(id + " <-> " + path);
            System.out.println(id + " <-> " + path);
            commandsMap.put(id, path);
        }

        controller = new BaseController();
        controller.initCommands(commandsMap);
    }

    public static class JeetyServlet extends HttpServlet {
        private static final long serialVersionUID = 1L;

        public JeetyServlet() {
        }

        protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            this.onExecute(request, response);
        }

        protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            this.onExecute(request, response);
        }

        private void onExecute(HttpServletRequest request, HttpServletResponse response) throws IOException {
            response.setContentType("text/html");
            response.setCharacterEncoding("UTF-8");
            response.setStatus(200);
            Map requestMap = request.getParameterMap();
            String remoteAddr = request.getRemoteAddr();
            Main.logger.info("IP:" + remoteAddr);
            if (requestMap.containsKey("c")) {
                String command = request.getParameter("c");
                if (command == null || command.equalsIgnoreCase("")) {
                    Main.blackListIpLogger.debug(remoteAddr);
                    return;
                }

                Param param = new Param();
                param.set(request);
                Main.logger.debug("command: " + command);

                try {
                    if (!"8000".equals(command)) {
                        response.getWriter().println((String) Main.controller.processCommand(Integer.parseInt(command), param));
                        Main.service.log(command);
                    } else {
                        response.setContentType("text/csv");
                        response.setHeader("Content-Disposition", "attachment; filename=\"export.csv\"");
                        try (OutputStream outputStream = response.getOutputStream()) {
                            String outputResult = (String) Main.controller.processCommand(Integer.parseInt(command), param);
                            outputStream.write(outputResult.getBytes());
                            outputStream.flush();
                        }
                    }
                } catch (Exception var10) {
                    var10.printStackTrace();
                    System.out.println(var10);
                    response.getWriter().println("EXCEPTION: " + var10.getMessage());
                }
            } else {
                Main.blackListIpLogger.debug(remoteAddr);
                response.getWriter().println("NO COMMANDS PARAMETERS");
                Main.service.log("NO_CMD");
            }

        }
    }
}
