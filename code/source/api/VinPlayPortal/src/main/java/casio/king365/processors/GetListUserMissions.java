package casio.king365.processors;

import casio.king365.entities.Mission;
import casio.king365.entities.UserMission;
import casio.king365.service.MongoDbService;
import casio.king365.service.UserMissionService;
import casio.king365.service.impl.MongoDbServiceImpl;
import casio.king365.service.impl.UserMissionServiceImpl;
import casio.king365.util.KingUtil;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GetListUserMissions
        implements BaseProcessor<HttpServletRequest, String> {

    MongoDbService mongodbService = new MongoDbServiceImpl();
    UserMissionService UserMissionService = new UserMissionServiceImpl();

    int warningProcessingTime = 1000;

    public String execute(Param<HttpServletRequest> param) {
        JSONObject res = new JSONObject();
        try {
            res.put("is_success", false);
            res.put("desc", "Không xác định.");

            HttpServletRequest request = param.get();
            String nn = request.getParameter("nn") != null ? request.getParameter("nn") : "";
            String at = request.getParameter("at") != null ? request.getParameter("at") : "";
            KingUtil.printLog("GetListUserMissions - nn: " + nn + ", at: " + at);
            if (nn.equals("") && at.equals("")) {
                res.put("desc", "Thiếu tham số.");
                return res.toString();
            }

            List<Mission> listMission = UserMissionService.getMissions();
            Map<String, Object> findMap = new HashMap<>();
            findMap.put("nickname", nn);
            List<UserMission> listUserMission = mongodbService.find("user_mission", findMap, UserMission::fromDocument);

            List<Map> returnMissionList = new ArrayList<>();
            // Trường hợp có thêm mission mới, nhưng data của Uesr Mission chưa có thì cần tạo
            // Loop các Mission
            for (Mission mission : listMission) {
                returnMissionList.add(mission.toMap());
                UserMission um = null;
                // Tìm UserMission tương ứng
                for (UserMission _um : listUserMission) {
                    if (_um.getMissionId().equals(mission.getId())) {
                        um = _um;
                        continue;
                    }
                }
                if (um == null) {
                    um = UserMissionService.createUserMissions(nn, mission.getId());
                    listUserMission.add(um);
                }
            }
            List<Map> returnUserMissionlist = new ArrayList<>();
            for (UserMission um : listUserMission) {
                returnUserMissionlist.add(um.toMap());
            }
            res.put("missions", returnMissionList);
            res.put("user_missions", returnUserMissionlist);

            // TODO: remove this
            res.put("missions", new ArrayList());
            res.put("user_missions", new ArrayList());

            res.put("is_success", true);
            res.put("desc", "Thành công.");
        } catch (Exception e) {
            e.printStackTrace();
            KingUtil.printException("GetListUserMissions", e);
        }

        return res.toString();
    }

    public int getShouldWarningProcessingTimeInMillisecond() {
        return warningProcessingTime;
    }

    public static void main(String[] args) {
        Mission mission = new Mission();
        mission.setId("id1");
        mission.setDesc("aksgjlasgjasd");
        List<Map> listMiss = new ArrayList<>();
        listMiss.add(mission.toMap());

        JSONObject json = new JSONObject();
        try {
            json.put("a", listMiss);
            System.out.println(json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}

