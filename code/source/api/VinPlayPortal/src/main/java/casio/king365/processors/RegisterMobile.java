/*
 * Decompiled with CFR 0.144.
 *
 * Could not load the following classes:
 *  com.hazelcast.core.IMap
 *  com.vinplay.usercore.service.impl.MarketingServiceImpl
 *  com.vinplay.usercore.service.impl.UserServiceImpl
 *  com.vinplay.usercore.utils.GameCommon
 *  com.vinplay.usercore.utils.UserMakertingUtil
 *  com.vinplay.vbee.common.cp.BaseProcessor
 *  com.vinplay.vbee.common.cp.Param
 *  com.vinplay.vbee.common.enums.StatusGames
 *  com.vinplay.vbee.common.hazelcast.HazelcastClientFactory
 *  com.vinplay.vbee.common.messages.UserMarketingMessage
 *  com.vinplay.vbee.common.models.SocialModel
 *  com.vinplay.vbee.common.models.UserModel
 *  com.vinplay.vbee.common.response.LoginResponse
 *  com.vinplay.vbee.common.utils.VinPlayUtils
 *  javax.servlet.http.HttpServletRequest
 *  org.apache.log4j.Logger
 */
package casio.king365.processors;

import casio.king365.util.KingUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.vinplay.usercore.dao.impl.SecurityDaoImpl;
import com.vinplay.usercore.service.SecurityService;
import com.vinplay.usercore.service.UserService;
import com.vinplay.usercore.service.impl.SecurityServiceImpl;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.models.UserModel;
import misa.api.service.OtpService;
import misa.api.service.imp.OtpServiceImp;
import misa.data.AccountTeleLink;
import org.json.JSONException;
import org.json.JSONObject;
import vn.yotel.yoker.dao.CashinItemDao;
import vn.yotel.yoker.dao.RequestCardDao;
import vn.yotel.yoker.dao.impl.CashinItemDaoImpl;
import vn.yotel.yoker.dao.impl.RequestCardDaoImpl;

import javax.servlet.http.HttpServletRequest;

public class RegisterMobile
        implements BaseProcessor<HttpServletRequest, String> {

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    HazelcastInstance client;
    IMap<String, UserModel> userMap;
    UserService userService;
    CashinItemDao cashinItemDao;
    RequestCardDao requestCardDao;
    SecurityService securityService;

    public String execute(Param<HttpServletRequest> param) throws Exception {
        JSONObject res = new JSONObject();
        res.put("is_success", false);
        res.put("desc", "Lỗi không xác định");
        if (!init())
            return res.toString();

        HttpServletRequest request = (HttpServletRequest) param.get();
        String username = request.getParameter("un") != null ? request.getParameter("un") : "";
        String mobile = request.getParameter("mobile") != null ? request.getParameter("mobile") : "";
        String otp = request.getParameter("otp") != null ? request.getParameter("otp") : "";
        String test = request.getParameter("test") != null ? request.getParameter("test") : "";
        KingUtil.printLog("RegisterMobile - exe(), username: " + username
                + ", mobile: " + mobile + ", otp: " + otp);
        if (username.equals("")
                || mobile.equals("")
                || otp.equals("")) {
            KingUtil.printLog(("Tham số rỗng"));
            res.put("desc", "Lỗi tham số");
            return res.toString();
        }

        // Dựa vào mobile được gửi lên lấy ra mã OTP và id tele đã lưu trong hazel map OTP_UPDATE_MOBILE
        IMap<String, String> imapOTP = client.getMap("OTP_UPDATE_MOBILE");
        if (!imapOTP.containsKey(mobile)) {
            KingUtil.printLog("!imapOTP.containsKey(mobile)");
            res.put("desc", "Lỗi không tìm thấy số diện thoại đăng kí, hãy đảm bảo đã " +
                    "thay 0 hoặc +84 thành 84");
            return res.toString();
        }
        System.out.println("otp cached: " + imapOTP.get(mobile));
        KingUtil.printLog("otp cached: " + imapOTP.get(mobile));
        JSONObject otpAndIdtele = new JSONObject(imapOTP.get(mobile));
        String cachedOtp = otpAndIdtele.getString("otp");
        String cachedidTele = otpAndIdtele.getString("idTele");
        if (!otp.equals(cachedOtp)) {
            KingUtil.printLog("Sai otp");
            res.put("desc", "OTP không trùng khớp số điện thoại");
            return res.toString();
        }
        // Update sdt cho user
        UserModel userModel = userService.getUserByUserName(username);
        if (userModel == null) {
            KingUtil.printLog("Không tìm thấy username");
            res.put("desc", "Lỗi");
            return res.toString();
        }
        securityService.updateMobile(userModel.getNickname(), mobile);
        SecurityDaoImpl securityDaoImpl = new SecurityDaoImpl();
        securityDaoImpl.updateUserInfo(userModel.getId(), mobile, 4);
        imapOTP.remove(mobile);

        // Liên kết id tele và tài khoản user
        OtpService otpService = new OtpServiceImp();
        AccountTeleLink accountTeleLink = new AccountTeleLink();
        accountTeleLink.setIdUserTele(cachedidTele);
        accountTeleLink.setMobile(mobile);
        accountTeleLink.setNickName(userModel.getNickname());
        otpService.insertAccountTeleLink(accountTeleLink);

        res.put("is_success", true);
        res.put("desc", "Liên kết số điện thoại thành công.");
        return res.toString();
    }

    private boolean init() {
        if ((client = HazelcastClientFactory.getInstance()) == null) {
            KingUtil.printLog("CardRequestActionProcessor - init(), Lỗi khởi tạo hazelcast");
        }
        if (userMap == null)
            userMap = client.getMap("users");
        if (cashinItemDao == null)
            cashinItemDao = new CashinItemDaoImpl();
        if (userService == null)
            userService = new UserServiceImpl();
        if (requestCardDao == null)
            requestCardDao = new RequestCardDaoImpl();
        if (securityService == null)
            securityService = new SecurityServiceImpl();
        return true;
    }
}

