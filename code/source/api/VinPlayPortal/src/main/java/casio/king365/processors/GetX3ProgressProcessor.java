package casio.king365.processors;

import casio.king365.service.X3NapLanDauService;
import casio.king365.service.impl.X3NapLanDauServiceImpl;
import casio.king365.util.KingUtil;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import org.bson.Document;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

public class GetX3ProgressProcessor
        implements BaseProcessor<HttpServletRequest, String> {

    private X3NapLanDauService x3NapLanDauService = new X3NapLanDauServiceImpl();

    int warningProcessingTime = 1000;

    public String execute(Param<HttpServletRequest> param) {
        JSONObject res = new JSONObject();
        try {
            res.put("is_success", false);
            res.put("desc", "Không xác định.");

            HttpServletRequest request = param.get();
            String nickname = request.getParameter("nn") != null ? request.getParameter("nn") : "";
            String accessToken = request.getParameter("at") != null ? request.getParameter("at") : "";
            KingUtil.printLog("GetX3ProgressProcessor - nn: " + nickname + ", at: " + accessToken);
            if (nickname.equals("") && accessToken.equals("")) {
                res.put("desc", "Thiếu tham số.");
                return res.toString();
            }

            KingUtil.printLog("begin run calculateProgress");
            // Chạy tính toán
            x3NapLanDauService.calculateProgress(nickname);
            KingUtil.printLog("get newest data");
            // Lấy data
            List<Document> result = x3NapLanDauService.getX3NapLanDauDoc(nickname);
            if (result.isEmpty()) {
                res.put("data", new ArrayList<>());
            } else {
                res.put("data", result);
            }

            res.put("is_success", true);
            res.put("desc", "Thành công.");
        } catch (Exception e) {
            e.printStackTrace();
            KingUtil.printException("GetX3ProgressProcessor", e);
        }

        return res.toString();
    }

    public int getShouldWarningProcessingTimeInMillisecond() {
        return warningProcessingTime;
    }

}
