/*
 * Decompiled with CFR 0.144.
 *
 * Could not load the following classes:
 *  com.hazelcast.core.IMap
 *  com.vinplay.usercore.service.impl.MarketingServiceImpl
 *  com.vinplay.usercore.service.impl.UserServiceImpl
 *  com.vinplay.usercore.utils.GameCommon
 *  com.vinplay.usercore.utils.UserMakertingUtil
 *  com.vinplay.vbee.common.cp.BaseProcessor
 *  com.vinplay.vbee.common.cp.Param
 *  com.vinplay.vbee.common.enums.StatusGames
 *  com.vinplay.vbee.common.hazelcast.HazelcastClientFactory
 *  com.vinplay.vbee.common.messages.UserMarketingMessage
 *  com.vinplay.vbee.common.models.SocialModel
 *  com.vinplay.vbee.common.models.UserModel
 *  com.vinplay.vbee.common.response.LoginResponse
 *  com.vinplay.vbee.common.utils.VinPlayUtils
 *  javax.servlet.http.HttpServletRequest
 *  org.apache.log4j.Logger
 */
package casio.king365.processors;

import casio.king365.GU;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;

public class AlertFromClient
        implements BaseProcessor<HttpServletRequest, String> {

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    public String execute(Param<HttpServletRequest> param) throws JSONException {
        JSONObject res = new JSONObject();
        init();
        HttpServletRequest request = param.get();
        String name = request.getParameter("name");
        String service = request.getParameter("service");
        String mess = request.getParameter("message");

        // Bắn cảnh báo sang nhóm kĩ thuật
        GU.sendClientErrorMonitor("[CLIENT WARNING], Có lỗi từ client báo lên, tên lỗi: " + name +
                ", service lỗi: " + service + ", chi tiết tin nhắn lỗi: " + mess);

        res.put("is_success", true);
        res.put("desc", "Ghi nhận lỗi thành công.");
        return res.toString();
    }

    private boolean init() {

        return true;
    }
}

