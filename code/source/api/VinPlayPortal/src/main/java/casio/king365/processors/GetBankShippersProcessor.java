/*
 * Decompiled with CFR 0.144.
 *
 * Could not load the following classes:
 *  com.hazelcast.core.IMap
 *  com.vinplay.usercore.service.impl.MarketingServiceImpl
 *  com.vinplay.usercore.service.impl.UserServiceImpl
 *  com.vinplay.usercore.utils.GameCommon
 *  com.vinplay.usercore.utils.UserMakertingUtil
 *  com.vinplay.vbee.common.cp.BaseProcessor
 *  com.vinplay.vbee.common.cp.Param
 *  com.vinplay.vbee.common.enums.StatusGames
 *  com.vinplay.vbee.common.hazelcast.HazelcastClientFactory
 *  com.vinplay.vbee.common.messages.UserMarketingMessage
 *  com.vinplay.vbee.common.models.SocialModel
 *  com.vinplay.vbee.common.models.UserModel
 *  com.vinplay.vbee.common.response.LoginResponse
 *  com.vinplay.vbee.common.utils.VinPlayUtils
 *  javax.servlet.http.HttpServletRequest
 *  org.apache.log4j.Logger
 */
package casio.king365.processors;

import casio.king365.Constants;
import casio.king365.GU;
import casio.king365.core.HCMap;
import casio.king365.util.KingUtil;
import com.hazelcast.core.IMap;
import com.vinplay.dal.service.impl.CacheServiceImpl;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.models.UserModel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class GetBankShippersProcessor
        implements BaseProcessor<HttpServletRequest, String> {

    private static final String CACHE_KEY_BANK_CODE = "list_bank_in";
    private static final String CACHE_KEY_BANK_SHIPPER = "list_bank_shipper";

    private UserServiceImpl userService;
    private CacheServiceImpl cacheService;

    int warningProcessingTime = 2000;

    public String execute(Param<HttpServletRequest> param) throws JSONException {
        init();

        final JSONObject res = new JSONObject();
        try {
            res.put("is_success", false);
            res.put("desc", "Không xác định.");


            HttpServletRequest request = (HttpServletRequest) param.get();
            String username = request.getParameter("un") != null ? request.getParameter("un") : "";
            String at = request.getParameter("at") != null ? request.getParameter("at") : "";
            KingUtil.printLog("GetBankShippersProcessor - username: " + username + ", at: " + at);
            if (username.equals("") && at.equals("")) {
                res.put("desc", "Thiếu tham số.");
                return res.toString();
            }
            UserModel userModel;
            if (username.equals("")) {
                IMap<String, String> tokenMap = HCMap.getTokenMap();
                String nickname = tokenMap.get(at);
                userModel = userService.getUserByNickName(nickname);
            } else
                userModel = userService.getUserByUserName(username);
            if (userModel == null) {
                KingUtil.printLog(("GetBankShippersProcessor - userModel == null"));
                res.put("desc", "Không tìm thấy người dùng.");
                return res.toString();
            }

            // Kiểm tra cache
            if (!cacheService.checkKeyExist(CACHE_KEY_BANK_CODE) || !cacheService.checkKeyExist(CACHE_KEY_BANK_SHIPPER)) {
                getRemoteShipperInfoAndAddToCache();
            }

            // Lấy data từ cache
            res.put("is_success", true);
            res.put("desc", "Thành công.");
            res.put("send_content", userModel.getNickname());
            res.put("list_bank", new JSONObject(cacheService.getValueStr(CACHE_KEY_BANK_CODE)));
            res.put("accounts", new JSONArray(cacheService.getValueStr(CACHE_KEY_BANK_SHIPPER)));
        } catch (UnExpectedException e) {
            e.printStackTrace();
            GU.sendOperation(e.getMessage());
            KingUtil.printException("GetBankShippersProcessor.UnExpectedException", e);
            res.put("desc", "Hệ thống xảy ra lỗi. Quý khách vui lòng liên hệ CSKH để biết thêm thông tin chi tiết.");
            return res.toString();
        } catch (Exception e) {
            e.printStackTrace();
            KingUtil.printException("GetBankShippersProcessor", e);
            return "{\"is_success\":false,\"desc\":\"Lỗi hệ thống! Vui lòng liên hệ Admin.\"}";
        }

        return res.toString();
    }

    private void getRemoteShipperInfoAndAddToCache() throws IOException, JSONException, UnExpectedException {
        KingUtil.printLog("GetBankShippersProcessor getRemoteShipperInfoAndAddToCache()");
        Long requestTime = System.currentTimeMillis();
        String requestId = "requestId" + requestTime;
        String signature = KingUtil.MD5(requestId + "|" + requestTime + "|" +
                Constants.BankInInfo.partnerName + "|" + Constants.BankInInfo.partnerKey);
        String getListBankUrl = Constants.BankInInfo.getBankUrl + "?request_id=" + requestId
                + "&username=" + Constants.BankInInfo.partnerName + "&request_time=" + requestTime
                + "&signature=" + signature;

        String result = KingUtil.get(getListBankUrl);
        KingUtil.printLog("GetBankShippersProcessor - listBank result: " + result.trim());

        if (result == null || result.isEmpty()) {
            throw new UnExpectedException("listBank rỗng");
        }

        JSONObject listNBank = new JSONObject(result.trim());
        if (listNBank.getInt("code") != 200) {
            throw new UnExpectedException("listNBank code != 200, result: " + result);
        }

        cacheService.setValue(CACHE_KEY_BANK_CODE, listNBank.toString(), 300, TimeUnit.SECONDS);

        String getListAccUrl = Constants.BankInInfo.getAccUrl + "?request_id=" + requestId
                + "&username=" + Constants.BankInInfo.partnerName + "&request_time=" + requestTime
                + "&signature=" + signature;
        result = KingUtil.get(getListAccUrl);
        KingUtil.printLog(("GetBankShippersProcessor - result: " + result.trim()));

        if (result == null || result.isEmpty()) {
            throw new UnExpectedException("listBankAcc rỗng");
        }

        JSONObject listAcc = new JSONObject(result.trim());
        if (listAcc.getInt("code") != 200) {
            throw new UnExpectedException("listAcc code != 200, result: " + result);
        }

        JSONObject getResultData = listAcc.getJSONObject("data");
        JSONArray accounts = getResultData.getJSONArray("accounts");
        JSONArray accountsResponse = new JSONArray();
        for (int i = 0; i < accounts.length(); i++) {
            JSONObject acc = accounts.getJSONObject(i);
            int isMaxquota = acc.getInt("isMaxquota");
            if (isMaxquota == 0) {
                accountsResponse.put(acc);
            }
        }
        cacheService.setValue(CACHE_KEY_BANK_SHIPPER, accountsResponse.toString(), 300, TimeUnit.SECONDS);
    }

    void init() {
        if (userService == null)
            userService = new UserServiceImpl();
        if (cacheService == null)
            cacheService = new CacheServiceImpl();
    }

    private class UnExpectedException extends Exception {
        public UnExpectedException(String message) {
            super(message);
        }
    }

    public int getShouldWarningProcessingTimeInMillisecond() {
        return warningProcessingTime;
    }
}
