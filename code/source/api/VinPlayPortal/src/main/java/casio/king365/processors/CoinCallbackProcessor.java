package casio.king365.processors;

import casio.king365.GU;
import casio.king365.util.KingUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vinplay.dichvuthe.service.RechargeService;
import com.vinplay.dichvuthe.service.impl.RechargeServiceImpl;
import com.vinplay.usercore.logger.MoneyLogger;
import com.vinplay.usercore.service.MailBoxService;
import com.vinplay.usercore.service.UserService;
import com.vinplay.usercore.service.impl.GameConfigServiceImpl;
import com.vinplay.usercore.service.impl.MailBoxServiceImpl;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.response.ResultGameConfigResponse;
import com.vinplay.vbee.common.utils.UserValidaton;
import org.json.JSONObject;
import vn.yotel.yoker.dao.CashinItemDao;
import vn.yotel.yoker.dao.CashinNotifyDao;
import vn.yotel.yoker.dao.CashoutItemDao;
import vn.yotel.yoker.dao.CashoutRequestDao;
import vn.yotel.yoker.dao.impl.CashinItemDaoImpl;
import vn.yotel.yoker.dao.impl.CashinNotifyDaoImpl;
import vn.yotel.yoker.dao.impl.CashoutItemDaoImpl;
import vn.yotel.yoker.dao.impl.CashoutRequestDaoImpl;
import vn.yotel.yoker.domain.CashinNotify;

import javax.persistence.PersistenceException;
import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.Date;
import java.util.stream.Collectors;

public class CoinCallbackProcessor
        implements BaseProcessor<HttpServletRequest, String> {

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    UserService userService;
    CashoutItemDao cashoutItemDao;
    CashoutRequestDao cashoutRequestDao;
    CashinNotifyDao cashinNotifyDao;
    CashinItemDao cashinItemDao;

    public int getShouldWarningProcessingTimeInMillisecond() {
        return 5_000;
    }

    public int getShouldAlertProcessingTimeInMillisecond() {
        return 10_000;
    }

    public String execute(Param<HttpServletRequest> param) {
        String nickname = "";
        String wallet_code = "";
        String address = "";    // [Địa chỉ ví user]
        String from_address = ""; //  [Địa chỉ ví gửi]
        String amount = "";
        String token = "";
        String token_type = "";
        String trx_id = "";
        String status = "";
        Long trans_time = 0L;
        String signature = "";
        String requestData = "";

        JSONObject res = new JSONObject();
        try {
            res.put("errorCode", 100);
            res.put("errorMessage", "Có lỗi");
            if (!init())
                return res.toString();

            HttpServletRequest request = param.get();
            String ipAddress = request.getRemoteAddr();
            requestData = request.getReader().lines().collect(Collectors.joining());
            KingUtil.printLog("CoinCallbackProcessor - execute(), requestData: " + requestData);
            JSONObject requestObj = new JSONObject(requestData);
            wallet_code = requestObj.has("wallet_code") ? requestObj.getString("wallet_code") : "";
            nickname = wallet_code;
            address = requestObj.has("address") ? requestObj.getString("address") : "";
            from_address = requestObj.has("from_address") ? requestObj.getString("from_address") : "";
            amount = requestObj.has("amount") ? requestObj.getString("amount") : "";
            token = requestObj.has("token") ? requestObj.getString("token") : "";
            token_type = requestObj.has("token_type") ? requestObj.getString("token_type") : "";
            trx_id = requestObj.has("trx_id") ? requestObj.getString("trx_id") : "";
            status = requestObj.has("status") ? requestObj.getString("status") : "";
            signature = requestObj.has("signature") ? requestObj.getString("signature") : "";
            trans_time = requestObj.has("trans_time") ? requestObj.getLong("trans_time") : 0L;

            KingUtil.printLog("CoinCallbackProcessor - execute(), wallet_code: " + wallet_code
                    + ", address: " + address + ", from_address: " + from_address+ ", amount: " + amount
                    + ", token_type: " + token_type + ", token: " + token
                    + ", trx_id: " + trx_id + ", status: " + status
                    + ", signature: " + signature);

            if (wallet_code.equals("") || signature.equals("")) {
                res.put("errorMessage", "Sai param");
                return res.toString();
            }
            KingUtil.printLog("CoinCallbackProcessor 1");
            // Save data to db
            CashinNotify cashinNotify = new CashinNotify();
            cashinNotify.setUuid(trx_id);
            cashinNotify.setGateType("COIN");
            cashinNotify.setIp(ipAddress);
            cashinNotify.setReceiveTime(new Timestamp(new Date().getTime()));
            cashinNotify.setComment(wallet_code);
            cashinNotify.setData(requestData);
            cashinNotify.setError("true");
            try {
                cashinNotifyDao.save(cashinNotify);
            } catch (PersistenceException e) {
                // Lỗi này xảy ra khi data đã có trong db
                // có nghĩa là callback với trans_id đã được xử lí rồi
                KingUtil.printLog("CoinCallbackProcessor PersistenceException: " + KingUtil.printException(e));
                // Kiểm tra nếu transId này chưa thành công thì cho xử lí,
                // nếu đã thành công thì ignore callback này
                CashinNotify oldCashinNotify = cashinNotifyDao.findById(trx_id);
                if (oldCashinNotify!= null && !oldCashinNotify.getError().isEmpty() && oldCashinNotify.getError().equals("false")) {
                    // ignore callback này thôi
                    res.put("errorCode", 100);
                    res.put("errorMessage", "Giao dịch đã được xử lí trước đó.");
                    return res.toString();
                }
            }
            KingUtil.printLog("CoinCallbackProcessor 2");

            GameConfigServiceImpl gameConfigServiceImpl = new GameConfigServiceImpl();
            ResultGameConfigResponse gateInOutConfigData = gameConfigServiceImpl.getGameConfigAdmin("king365_gate_in_out_rate", "").get(0);
            KingUtil.printLog("CoinCallbackProcessor 3");
            if (gateInOutConfigData == null) {
                KingUtil.printLog("gateInOutConfigData == null");
                res.put("errorMessage", "Lỗi hệ thống, không tìm thấy config");
                return res.toString();
            }

            KingUtil.printLog("CoinCallbackProcessor 4");
            // Add money to user
            UserCacheModel user = userService.getCacheUserIgnoreCase(nickname);
            if (user == null) {
                UserModel userModel = userService.getUserByNickName(nickname);
                if (userModel == null) {
                    res.put("errorMessage", "Không tìm thấy người dùng.");
                    GU.sendCustomerService("Có lỗi khi nạp COIN, Không tìm thấy người dùng. Trans_id: " + trx_id + ", comment: " + nickname);
                    return res.toString();
                }
            }
            KingUtil.printLog("CoinCallbackProcessor 5");

            // Call rechargeService
            RechargeService rechargeService = new RechargeServiceImpl();
            rechargeService.receiveResultFromCoinGate(nickname, amount, trx_id, address, from_address, token, token_type, signature, status, trans_time);

            KingUtil.printLog("CoinCallbackProcessor 6");
            String desc = "Cộng tiền nạp Coin, mã giao dịch: " + trx_id + ", số tiền: " + amount;
            // Mail inbox
            MailBoxService service = new MailBoxServiceImpl();
            service.sendMailPopupMessage(
                    user.getNickname(), "Kết quả nạp Coin", desc);
            if (!userService.isOnline(user.getNickname())) {
                userService.sendTeleMessage(user.getNickname(), desc);
            }
            KingUtil.printLog("CoinCallbackProcessor 7");
            res.put("errorCode", 200);
            res.put("errorMessage", "Thành công");

            cashinNotify = cashinNotifyDao.findById(trx_id);
            cashinNotify.setError("false");
            cashinNotifyDao.update(cashinNotify);
        } catch (Exception e) {
            e.printStackTrace();
            KingUtil.printException("CoinCallbackProcessor", e);
            MoneyLogger.log("", "RechargeByCoin", 0L, 0L, "vin", "Nap vin qua coin", "1001", e.getMessage());
            GU.sendOperation("Có lỗi khi nhận kết quả nạp coin , Exception: " + KingUtil.printException(e));
            GU.sendCustomerService("Có lỗi khi nạp COIN, dữ liệu từ cổng coin: " + requestData);
            try {
                res.put("errorMessage", "Có lỗi: " + KingUtil.printException(e));
            } catch (Exception ex) {
                KingUtil.printException("CoinCallbackProcessor", e);
            }
        }
        return res.toString();
    }

    private boolean init() {
        if (cashoutItemDao == null)
            cashoutItemDao = new CashoutItemDaoImpl();
        if (cashoutRequestDao == null)
            cashoutRequestDao = new CashoutRequestDaoImpl();
        if (userService == null)
            userService = new UserServiceImpl();
        if (cashinNotifyDao == null)
            cashinNotifyDao = new CashinNotifyDaoImpl();
        if (cashinItemDao == null)
            cashinItemDao = new CashinItemDaoImpl();
        return true;
    }
}
