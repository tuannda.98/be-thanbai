/*
 * Decompiled with CFR 0.144.
 *
 * Could not load the following classes:
 *  com.hazelcast.core.IMap
 *  com.vinplay.usercore.service.impl.MarketingServiceImpl
 *  com.vinplay.usercore.service.impl.UserServiceImpl
 *  com.vinplay.usercore.utils.GameCommon
 *  com.vinplay.usercore.utils.UserMakertingUtil
 *  com.vinplay.vbee.common.cp.BaseProcessor
 *  com.vinplay.vbee.common.cp.Param
 *  com.vinplay.vbee.common.enums.StatusGames
 *  com.vinplay.vbee.common.hazelcast.HazelcastClientFactory
 *  com.vinplay.vbee.common.messages.UserMarketingMessage
 *  com.vinplay.vbee.common.models.SocialModel
 *  com.vinplay.vbee.common.models.UserModel
 *  com.vinplay.vbee.common.response.LoginResponse
 *  com.vinplay.vbee.common.utils.VinPlayUtils
 *  javax.servlet.http.HttpServletRequest
 *  org.apache.log4j.Logger
 */
package casio.king365.processors;

import casio.king365.entities.Mission;
import casio.king365.entities.UserMission;
import casio.king365.service.MongoDbService;
import casio.king365.service.impl.MongoDbServiceImpl;
import casio.king365.service.impl.UserMissionServiceImpl;
import casio.king365.util.KingUtil;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ILock;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.response.BaseResponseModel;
import org.bson.Document;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GetBonusUserMissions
        implements BaseProcessor<HttpServletRequest, String> {

    HazelcastInstance hazelcastInstance = HazelcastClientFactory.getInstance();
    MongoDbService mongodbService = new MongoDbServiceImpl();
    UserServiceImpl userService = new UserServiceImpl();
    casio.king365.service.UserMissionService UserMissionService = new UserMissionServiceImpl();

    int warningProcessingTime = 1000;

    public String execute(Param<HttpServletRequest> param) {
        JSONObject res = new JSONObject();
        ILock lock = null;
        try {
            res.put("is_success", false);
            res.put("desc", "Không xác định.");

            HttpServletRequest request = param.get();
            String nn = request.getParameter("nn") != null ? request.getParameter("nn") : "";
            String at = request.getParameter("at") != null ? request.getParameter("at") : "";
            String missionId = request.getParameter("mi") != null ? request.getParameter("mi") : "";
            KingUtil.printLog("GetBonusUserMissions - nn: "+ nn+", at: "+at+", mi: "+missionId);
            if(nn.equals("") && at.equals("")){
                res.put("desc", "Thiếu tham số.");
                return res.toString();
            }
            lock = hazelcastInstance.getLock("GetBonusUserMissions_"+nn);
            try {
                // Nếu user chưa xác thực OTP thì k được phép nhận thưởng
                Map<String, Object> mapFindData = new HashMap<>();
                mapFindData.put("nickName", nn);
                mapFindData.put("bonusCode", "VERIFY_TELE");
                List<Document> docs =  mongodbService.find("user_bonus", mapFindData);
                if(docs.size() < 0){
                    // Chưa verify
                    res.put("is_success", false);
                    res.put("desc", "Chưa xác thực OTP tài khoản");
                }
            } catch (Exception e) {
                e.printStackTrace();
                KingUtil.printException("UserService checkVerifyMobileAndBonus()", e);
            }

            UserMission userMission = UserMissionService.getUserMissions(nn, missionId);
            if(userMission == null){
                res.put("is_success", false);
                res.put("desc", "Không tìm thấy thông tin nhiệm vụ");
            }
            if(userMission.getReceivedBonus()){
                res.put("is_success", false);
                res.put("desc", "Đã nhận thưởng rồi");
            }
            Mission mission = null;
            for(Mission m : UserMissionService.getMissions()){
                if(m.getId().equals(userMission.getMissionId())){
                    userMission.setMission(m);
                    break;
                }
            }
            if(userMission.getNumberDoingTarget() < userMission.getMission().getNumberTarget()){
                res.put("is_success", false);
                res.put("desc", "Chưa hoàn thành nhiệm vụ");
            }

            // Thưởng
            BaseResponseModel moneyResult = userService.updateMoneyWithNotify(userMission.getNickname()
                    , userMission.getMission().getBonus(), "Mission", "Mission"
                    , "Thưởng hoàn thành nhiệm vụ: "+userMission.getMission().getDesc(), true);

            if(!moneyResult.isSuccess()){
                res.put("is_success", false);
                res.put("desc", "Lỗi cộng thưởng: "+moneyResult.getErrorCode());
                return res.toString();
            }

            // Update db
            KingUtil.printLog("GetBonusUserMissions update receivedBonus userMission: "+userMission);
            mongodbService.update("user_mission",
                    new Document("nickname", userMission.getNickname()).append("missionId", userMission.getMissionId()), new Document("receivedBonus", true));

            res.put("is_success", true);
            res.put("desc", "Thành công.");
        } catch (Exception e) {
            e.printStackTrace();
            KingUtil.printException("GetBonusUserMissions", e);
        } finally {
            if(lock != null)
                lock.forceUnlock();
        }

        return res.toString();
    }

    public int getShouldWarningProcessingTimeInMillisecond() {
        return warningProcessingTime;
    }

}

