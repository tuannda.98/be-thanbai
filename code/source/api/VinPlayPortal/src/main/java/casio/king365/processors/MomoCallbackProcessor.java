package casio.king365.processors;

import casio.king365.GU;
import casio.king365.util.KingUtil;
import com.vinplay.dichvuthe.service.RechargeService;
import com.vinplay.dichvuthe.service.impl.RechargeServiceImpl;
import com.vinplay.vbee.common.cp.BaseProcessor;
import com.vinplay.vbee.common.cp.Param;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import java.util.stream.Collectors;

public class MomoCallbackProcessor
        implements BaseProcessor<HttpServletRequest, String> {
    public int getShouldWarningProcessingTimeInMillisecond() {
        return 5_000;
    }

    public int getShouldAlertProcessingTimeInMillisecond() {
        return 10_000;
    }

    public String execute(Param<HttpServletRequest> param) {
        String requestData = "";
        JSONObject res = new JSONObject();
        try {
            res.put("errorCode", 200);
            res.put("errorMessage", "Thành công");
            HttpServletRequest request = param.get();
            requestData = request.getReader().lines().collect(Collectors.joining());
            String ipAddress = request.getRemoteAddr();
            KingUtil.printLog("MomoCallbackProcessor - execute(), requestData: " + requestData);
            RechargeService rechargeService = new RechargeServiceImpl();
            rechargeService.receiveResultFromZoanMomo(requestData, ipAddress);
        } catch (Exception e) {
            e.printStackTrace();
            KingUtil.printException("MomoCallbackProcessor", e);
            if(!e.getMessage().equals("Không tìm thấy người dùng"))
                GU.sendOperation("Có lỗi khi nhận kết quả nạp momo , Exception: " + KingUtil.printException(e)+", requestData: "+requestData);
            GU.sendCustomerService("Có lỗi khi nạp MOMO: " + e.getMessage() + ", dữ liệu nhận từ cổng momo: " + requestData);
            try {
                res.put("errorMessage", e.getMessage());
            } catch (JSONException jsonException) {
                jsonException.printStackTrace();
            }
        }
        return res.toString();
    }
}

