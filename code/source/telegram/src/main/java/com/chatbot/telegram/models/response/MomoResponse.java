package com.chatbot.telegram.models.response;

public class MomoResponse {
  private String id;
  
  private String transId;
  
  private String nickName;
  
  private String ip;
  
  private String requestTime;
  
  private long amount;
  
  private String phone;
  
  private long type;
  
  private String authKey;
  
  private String authKeyGen;
  
  private String errorMessage;
  
  private String createTime;
  
  public String getId() {
    return this.id;
  }
  
  public void setId(String id) {
    this.id = id;
  }
  
  public String getTransId() {
    return this.transId;
  }
  
  public void setTransId(String transId) {
    this.transId = transId;
  }
  
  public String getNickName() {
    return this.nickName;
  }
  
  public void setNickName(String nickName) {
    this.nickName = nickName;
  }
  
  public String getIp() {
    return this.ip;
  }
  
  public void setIp(String ip) {
    this.ip = ip;
  }
  
  public String getRequestTime() {
    return this.requestTime;
  }
  
  public void setRequestTime(String requestTime) {
    this.requestTime = requestTime;
  }
  
  public long getAmount() {
    return this.amount;
  }
  
  public void setAmount(long amount) {
    this.amount = amount;
  }
  
  public String getPhone() {
    return this.phone;
  }
  
  public void setPhone(String phone) {
    this.phone = phone;
  }
  
  public long getType() {
    return this.type;
  }
  
  public void setType(long type) {
    this.type = type;
  }
  
  public String getAuthKey() {
    return this.authKey;
  }
  
  public void setAuthKey(String authKey) {
    this.authKey = authKey;
  }
  
  public String getAuthKeyGen() {
    return this.authKeyGen;
  }
  
  public void setAuthKeyGen(String authKeyGen) {
    this.authKeyGen = authKeyGen;
  }
  
  public String getErrorMessage() {
    return this.errorMessage;
  }
  
  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }
  
  public String getCreateTime() {
    return this.createTime;
  }
  
  public void setCreateTime(String createTime) {
    this.createTime = createTime;
  }
}
