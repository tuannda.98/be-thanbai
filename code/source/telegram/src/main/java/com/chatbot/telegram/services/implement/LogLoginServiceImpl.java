package com.chatbot.telegram.services.implement;

import com.chatbot.telegram.entites.mongo.LogLoginEntity;
import com.chatbot.telegram.repositories.mongodb.LogLoginRepository;
import com.chatbot.telegram.services.define.LogLoginService;
import java.util.concurrent.TimeUnit;
import org.springframework.stereotype.Service;

@Service
public class LogLoginServiceImpl implements LogLoginService {
  private final LogLoginRepository logLoginRepository;
  
  public LogLoginServiceImpl(LogLoginRepository logLoginRepository) {
    this.logLoginRepository = logLoginRepository;
  }
  
  public Long allowLogin(String ip) {
    LogLoginEntity entity = this.logLoginRepository.findByIp(ip).orElse(null);
    if (entity == null)
      return null; 
    if (!entity.isLoginFail() || entity.getTimes().intValue() < 5)
      return null; 
    Long currentTime = Long.valueOf(System.currentTimeMillis());
    long diffInMillies = currentTime.longValue() - entity.getUpdateAt().longValue();
    long timeWait = TimeUnit.SECONDS.convert(diffInMillies, TimeUnit.MILLISECONDS);
    if (timeWait > 300L)
      return null; 
    return Long.valueOf(timeWait);
  }
  
  public void loginSuccess(String ip) {
    Long currentTime = Long.valueOf(System.currentTimeMillis());
    LogLoginEntity entity = this.logLoginRepository.findByIp(ip).orElse(null);
    if (entity == null) {
      entity = new LogLoginEntity();
      entity.setIp(ip);
      entity.setLoginFail(false);
      entity.setCreatAt(currentTime);
      entity.setUpdateAt(currentTime);
      entity.setTimes(Integer.valueOf(1));
    } else {
      Integer times = entity.getTimes();
      if (entity.isLoginFail()) {
        times = Integer.valueOf(0);
        entity.setLoginFail(false);
      } 
      entity.setTimes(Integer.valueOf(times.intValue() + 1));
      entity.setUpdateAt(currentTime);
    } 
    this.logLoginRepository.save(entity);
  }
  
  public void loginFail(String ip) {
    Long currentTime = Long.valueOf(System.currentTimeMillis());
    LogLoginEntity entity = this.logLoginRepository.findByIp(ip).orElse(null);
    if (entity == null) {
      entity = new LogLoginEntity();
      entity.setIp(ip);
      entity.setLoginFail(true);
      entity.setCreatAt(currentTime);
      entity.setUpdateAt(currentTime);
      entity.setTimes(Integer.valueOf(1));
    } else {
      Integer times = entity.getTimes();
      if (!entity.isLoginFail()) {
        times = Integer.valueOf(0);
        entity.setLoginFail(true);
      } 
      entity.setTimes(Integer.valueOf(times.intValue() + 1));
      entity.setUpdateAt(currentTime);
    } 
    this.logLoginRepository.save(entity);
  }
}
