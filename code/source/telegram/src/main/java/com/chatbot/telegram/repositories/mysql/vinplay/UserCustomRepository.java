package com.chatbot.telegram.repositories.mysql.vinplay;

import com.chatbot.telegram.entites.mysql.vinplay.UsersEntity;

public interface UserCustomRepository {
  UsersEntity getByPhoneNumber(String paramString);
}
