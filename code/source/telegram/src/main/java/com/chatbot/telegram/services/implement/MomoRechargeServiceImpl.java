package com.chatbot.telegram.services.implement;

import com.chatbot.telegram.models.response.MomoResponse;
import com.chatbot.telegram.models.response.PageResponse;
import com.chatbot.telegram.repositories.mongodb.MomoRepository;
import com.chatbot.telegram.services.define.MomoRechargeService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
public class MomoRechargeServiceImpl implements MomoRechargeService {
  private final MomoRepository momoRepository;
  
  public MomoRechargeServiceImpl(MomoRepository momoRepository) {
    this.momoRepository = momoRepository;
  }
  
  public PageResponse<MomoResponse> searchLogMomo(String nickName, String ip, String phoneNumber, String fromDate, String toDate, int page, int size) {
    Page<MomoResponse> pageResult = this.momoRepository.searchLogMomo(nickName, ip, phoneNumber, fromDate, toDate, page, size);
    return new PageResponse(pageResult);
  }
}
