package com.chatbot.telegram.repositories.mongodb;

import com.chatbot.telegram.repositories.mongodb.BoboRepository;
import com.chatbot.telegram.repositories.mongodb.BoboRepositoryCustom;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface BoboRepository extends MongoRepository<BoboRepository, String>, BoboRepositoryCustom {}
