package com.chatbot.telegram.entites.mysql.vinplay;

import java.sql.Timestamp;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class UsersEntity {
  private long id;
  
  private String userName;
  
  private String nickName;
  
  private String password;
  
  private String email;
  
  private String googleId;
  
  private String facebookId;
  
  private String mobile;
  
  private String identification;
  
  private int avatar;
  
  private Timestamp birthday;
  
  private Boolean gender;
  
  private String address;
  
  private long vin;
  
  private long xu;
  
  private long vinTotal;
  
  private long xuTotal;
  
  private long safe;
  
  private long rechargeMoney;
  
  private int vipPoint;
  
  private int vipPointSave;
  
  private int moneyVp;
  
  private int daiLy;
  
  private int status;
  
  private Timestamp createTime;
  
  private Timestamp securityTime;
  
  private long loginOtp;
  
  private int isBot;
  
  private Timestamp updatePwTime;
  
  private String lastTransactionAgent;
  
  @Id
  @Column(name = "id")
  public long getId() {
    return this.id;
  }
  
  public void setId(long id) {
    this.id = id;
  }
  
  @Basic
  @Column(name = "user_name")
  public String getUserName() {
    return this.userName;
  }
  
  public void setUserName(String userName) {
    this.userName = userName;
  }
  
  @Basic
  @Column(name = "nick_name")
  public String getNickName() {
    return this.nickName;
  }
  
  public void setNickName(String nickName) {
    this.nickName = nickName;
  }
  
  @Basic
  @Column(name = "password")
  public String getPassword() {
    return this.password;
  }
  
  public void setPassword(String password) {
    this.password = password;
  }
  
  @Basic
  @Column(name = "email")
  public String getEmail() {
    return this.email;
  }
  
  public void setEmail(String email) {
    this.email = email;
  }
  
  @Basic
  @Column(name = "google_id")
  public String getGoogleId() {
    return this.googleId;
  }
  
  public void setGoogleId(String googleId) {
    this.googleId = googleId;
  }
  
  @Basic
  @Column(name = "facebook_id")
  public String getFacebookId() {
    return this.facebookId;
  }
  
  public void setFacebookId(String facebookId) {
    this.facebookId = facebookId;
  }
  
  @Basic
  @Column(name = "mobile")
  public String getMobile() {
    return this.mobile;
  }
  
  public void setMobile(String mobile) {
    this.mobile = mobile;
  }
  
  @Basic
  @Column(name = "identification")
  public String getIdentification() {
    return this.identification;
  }
  
  public void setIdentification(String identification) {
    this.identification = identification;
  }
  
  @Basic
  @Column(name = "avatar")
  public int getAvatar() {
    return this.avatar;
  }
  
  public void setAvatar(int avatar) {
    this.avatar = avatar;
  }
  
  @Basic
  @Column(name = "birthday")
  public Timestamp getBirthday() {
    return this.birthday;
  }
  
  public void setBirthday(Timestamp birthday) {
    this.birthday = birthday;
  }
  
  @Basic
  @Column(name = "gender")
  public Boolean getGender() {
    return this.gender;
  }
  
  public void setGender(Boolean gender) {
    this.gender = gender;
  }
  
  @Basic
  @Column(name = "address")
  public String getAddress() {
    return this.address;
  }
  
  public void setAddress(String address) {
    this.address = address;
  }
  
  @Basic
  @Column(name = "vin")
  public long getVin() {
    return this.vin;
  }
  
  public void setVin(long vin) {
    this.vin = vin;
  }
  
  @Basic
  @Column(name = "xu")
  public long getXu() {
    return this.xu;
  }
  
  public void setXu(long xu) {
    this.xu = xu;
  }
  
  @Basic
  @Column(name = "vin_total")
  public long getVinTotal() {
    return this.vinTotal;
  }
  
  public void setVinTotal(long vinTotal) {
    this.vinTotal = vinTotal;
  }
  
  @Basic
  @Column(name = "xu_total")
  public long getXuTotal() {
    return this.xuTotal;
  }
  
  public void setXuTotal(long xuTotal) {
    this.xuTotal = xuTotal;
  }
  
  @Basic
  @Column(name = "safe")
  public long getSafe() {
    return this.safe;
  }
  
  public void setSafe(long safe) {
    this.safe = safe;
  }
  
  @Basic
  @Column(name = "recharge_money")
  public long getRechargeMoney() {
    return this.rechargeMoney;
  }
  
  public void setRechargeMoney(long rechargeMoney) {
    this.rechargeMoney = rechargeMoney;
  }
  
  @Basic
  @Column(name = "vip_point")
  public int getVipPoint() {
    return this.vipPoint;
  }
  
  public void setVipPoint(int vipPoint) {
    this.vipPoint = vipPoint;
  }
  
  @Basic
  @Column(name = "vip_point_save")
  public int getVipPointSave() {
    return this.vipPointSave;
  }
  
  public void setVipPointSave(int vipPointSave) {
    this.vipPointSave = vipPointSave;
  }
  
  @Basic
  @Column(name = "money_vp")
  public int getMoneyVp() {
    return this.moneyVp;
  }
  
  public void setMoneyVp(int moneyVp) {
    this.moneyVp = moneyVp;
  }
  
  @Basic
  @Column(name = "dai_ly")
  public int getDaiLy() {
    return this.daiLy;
  }
  
  public void setDaiLy(int daiLy) {
    this.daiLy = daiLy;
  }
  
  @Basic
  @Column(name = "status")
  public int getStatus() {
    return this.status;
  }
  
  public void setStatus(int status) {
    this.status = status;
  }
  
  @Basic
  @Column(name = "create_time")
  public Timestamp getCreateTime() {
    return this.createTime;
  }
  
  public void setCreateTime(Timestamp createTime) {
    this.createTime = createTime;
  }
  
  @Basic
  @Column(name = "security_time")
  public Timestamp getSecurityTime() {
    return this.securityTime;
  }
  
  public void setSecurityTime(Timestamp securityTime) {
    this.securityTime = securityTime;
  }
  
  @Basic
  @Column(name = "login_otp")
  public long getLoginOtp() {
    return this.loginOtp;
  }
  
  public void setLoginOtp(long loginOtp) {
    this.loginOtp = loginOtp;
  }
  
  @Basic
  @Column(name = "is_bot")
  public int getIsBot() {
    return this.isBot;
  }
  
  public void setIsBot(int isBot) {
    this.isBot = isBot;
  }
  
  @Basic
  @Column(name = "update_pw_time")
  public Timestamp getUpdatePwTime() {
    return this.updatePwTime;
  }
  
  public void setUpdatePwTime(Timestamp updatePwTime) {
    this.updatePwTime = updatePwTime;
  }
  
  @Basic
  @Column(name = "last_transaction_agent")
  public String getLastTransactionAgent() {
    return this.lastTransactionAgent;
  }
  
  public void setLastTransactionAgent(String lastTransactionAgent) {
    this.lastTransactionAgent = lastTransactionAgent;
  }
  
  public boolean equals(Object o) {
    if (this == o)
      return true; 
    if (o == null || getClass() != o.getClass())
      return false; 
    com.chatbot.telegram.entites.mysql.vinplay.UsersEntity that = (com.chatbot.telegram.entites.mysql.vinplay.UsersEntity)o;
    if (this.id != that.id)
      return false; 
    if (this.avatar != that.avatar)
      return false; 
    if (this.vin != that.vin)
      return false; 
    if (this.xu != that.xu)
      return false; 
    if (this.vinTotal != that.vinTotal)
      return false; 
    if (this.xuTotal != that.xuTotal)
      return false; 
    if (this.safe != that.safe)
      return false; 
    if (this.rechargeMoney != that.rechargeMoney)
      return false; 
    if (this.vipPoint != that.vipPoint)
      return false; 
    if (this.vipPointSave != that.vipPointSave)
      return false; 
    if (this.moneyVp != that.moneyVp)
      return false; 
    if (this.daiLy != that.daiLy)
      return false; 
    if (this.status != that.status)
      return false; 
    if (this.loginOtp != that.loginOtp)
      return false; 
    if (this.isBot != that.isBot)
      return false; 
    if ((this.userName != null) ? !this.userName.equals(that.userName) : (that.userName != null))
      return false; 
    if ((this.nickName != null) ? !this.nickName.equals(that.nickName) : (that.nickName != null))
      return false; 
    if ((this.password != null) ? !this.password.equals(that.password) : (that.password != null))
      return false; 
    if ((this.email != null) ? !this.email.equals(that.email) : (that.email != null))
      return false; 
    if ((this.googleId != null) ? !this.googleId.equals(that.googleId) : (that.googleId != null))
      return false; 
    if ((this.facebookId != null) ? !this.facebookId.equals(that.facebookId) : (that.facebookId != null))
      return false; 
    if ((this.mobile != null) ? !this.mobile.equals(that.mobile) : (that.mobile != null))
      return false; 
    if ((this.identification != null) ? !this.identification.equals(that.identification) : (that.identification != null))
      return false; 
    if ((this.birthday != null) ? !this.birthday.equals(that.birthday) : (that.birthday != null))
      return false; 
    if ((this.gender != null) ? !this.gender.equals(that.gender) : (that.gender != null))
      return false; 
    if ((this.address != null) ? !this.address.equals(that.address) : (that.address != null))
      return false; 
    if ((this.createTime != null) ? !this.createTime.equals(that.createTime) : (that.createTime != null))
      return false; 
    if ((this.securityTime != null) ? !this.securityTime.equals(that.securityTime) : (that.securityTime != null))
      return false; 
    if ((this.updatePwTime != null) ? !this.updatePwTime.equals(that.updatePwTime) : (that.updatePwTime != null))
      return false; 
    if ((this.lastTransactionAgent != null) ? !this.lastTransactionAgent.equals(that.lastTransactionAgent) : (that.lastTransactionAgent != null))
      return false; 
    return true;
  }
  
  public int hashCode() {
    int result = (int)(this.id ^ this.id >>> 32L);
    result = 31 * result + ((this.userName != null) ? this.userName.hashCode() : 0);
    result = 31 * result + ((this.nickName != null) ? this.nickName.hashCode() : 0);
    result = 31 * result + ((this.password != null) ? this.password.hashCode() : 0);
    result = 31 * result + ((this.email != null) ? this.email.hashCode() : 0);
    result = 31 * result + ((this.googleId != null) ? this.googleId.hashCode() : 0);
    result = 31 * result + ((this.facebookId != null) ? this.facebookId.hashCode() : 0);
    result = 31 * result + ((this.mobile != null) ? this.mobile.hashCode() : 0);
    result = 31 * result + ((this.identification != null) ? this.identification.hashCode() : 0);
    result = 31 * result + this.avatar;
    result = 31 * result + ((this.birthday != null) ? this.birthday.hashCode() : 0);
    result = 31 * result + ((this.gender != null) ? this.gender.hashCode() : 0);
    result = 31 * result + ((this.address != null) ? this.address.hashCode() : 0);
    result = 31 * result + (int)(this.vin ^ this.vin >>> 32L);
    result = 31 * result + (int)(this.xu ^ this.xu >>> 32L);
    result = 31 * result + (int)(this.vinTotal ^ this.vinTotal >>> 32L);
    result = 31 * result + (int)(this.xuTotal ^ this.xuTotal >>> 32L);
    result = 31 * result + (int)(this.safe ^ this.safe >>> 32L);
    result = 31 * result + (int)(this.rechargeMoney ^ this.rechargeMoney >>> 32L);
    result = 31 * result + this.vipPoint;
    result = 31 * result + this.vipPointSave;
    result = 31 * result + this.moneyVp;
    result = 31 * result + this.daiLy;
    result = 31 * result + this.status;
    result = 31 * result + ((this.createTime != null) ? this.createTime.hashCode() : 0);
    result = 31 * result + ((this.securityTime != null) ? this.securityTime.hashCode() : 0);
    result = 31 * result + (int)(this.loginOtp ^ this.loginOtp >>> 32L);
    result = 31 * result + this.isBot;
    result = 31 * result + ((this.updatePwTime != null) ? this.updatePwTime.hashCode() : 0);
    result = 31 * result + ((this.lastTransactionAgent != null) ? this.lastTransactionAgent.hashCode() : 0);
    return result;
  }
}
