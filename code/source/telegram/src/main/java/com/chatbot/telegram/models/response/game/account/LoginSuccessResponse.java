package com.chatbot.telegram.models.response.game.account;

public class LoginSuccessResponse {
  private boolean success;
  
  public LoginSuccessResponse() {}
  
  public LoginSuccessResponse(boolean success) {
    this.success = success;
  }
  
  public boolean isSuccess() {
    return this.success;
  }
  
  public void setSuccess(boolean success) {
    this.success = success;
  }
}
