package com.chatbot.telegram.services.define;

import java.util.List;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

public interface OtpTelegramService {
  List<SendMessage> confirmPhoneNumbers(String paramString1, String paramString2);
  
  SendMessage getOtp(String paramString);
  
  SendMessage start(String paramString);
}
