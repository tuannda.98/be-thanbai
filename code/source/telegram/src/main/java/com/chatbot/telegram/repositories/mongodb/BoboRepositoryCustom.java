package com.chatbot.telegram.repositories.mongodb;

import com.chatbot.telegram.models.response.BoboResponse;
import org.springframework.data.domain.Page;

public interface BoboRepositoryCustom {
  Page<BoboResponse> searchBobo(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, int paramInt1, int paramInt2);
}
