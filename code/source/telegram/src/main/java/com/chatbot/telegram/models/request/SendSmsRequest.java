package com.chatbot.telegram.models.request;

public class SendSmsRequest {
  private String phone;
  
  public String getPhone() {
    return this.phone;
  }
  
  public void setPhone(String phone) {
    this.phone = phone;
  }
}
