package com.chatbot.telegram.entites.mongo;

import javax.persistence.Entity;
import javax.persistence.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Entity
@Document(collection = "dvt_recharge_by_card_lht_result")
public class LhtRechargeEntity {
  @Id
  private String id;
  
  @Field(name = "request_id")
  private String requestId;
  
  @Field(name = "nick_name")
  private String nickName;
  
  private String amount;
  
  private String code;
  
  private String ip;
  
  @Field(name = "error_message")
  private String errorMessage;
  
  private String signature;
  
  @Field(name = "signature_gen")
  private String signatureGen;
  
  @Field(name = "create_time")
  private String createTime;
  
  public LhtRechargeEntity(String id, String requestId, String nickName, String amount, String code, String ip, String errorMessage, String signature, String signatureGen, String createTime) {
    this.id = id;
    this.requestId = requestId;
    this.nickName = nickName;
    this.amount = amount;
    this.code = code;
    this.ip = ip;
    this.errorMessage = errorMessage;
    this.signature = signature;
    this.signatureGen = signatureGen;
    this.createTime = createTime;
  }
  
  public LhtRechargeEntity() {}
  
  public String getId() {
    return this.id;
  }
  
  public void setId(String id) {
    this.id = id;
  }
  
  public String getRequestId() {
    return this.requestId;
  }
  
  public void setRequestId(String requestId) {
    this.requestId = requestId;
  }
  
  public String getNickName() {
    return this.nickName;
  }
  
  public void setNickName(String nickName) {
    this.nickName = nickName;
  }
  
  public String getAmount() {
    return this.amount;
  }
  
  public void setAmount(String amount) {
    this.amount = amount;
  }
  
  public String getCode() {
    return this.code;
  }
  
  public void setCode(String code) {
    this.code = code;
  }
  
  public String getIp() {
    return this.ip;
  }
  
  public void setIp(String ip) {
    this.ip = ip;
  }
  
  public String getErrorMessage() {
    return this.errorMessage;
  }
  
  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }
  
  public String getSignature() {
    return this.signature;
  }
  
  public void setSignature(String signature) {
    this.signature = signature;
  }
  
  public String getSignatureGen() {
    return this.signatureGen;
  }
  
  public void setSignatureGen(String signatureGen) {
    this.signatureGen = signatureGen;
  }
  
  public String getCreateTime() {
    return this.createTime;
  }
  
  public void setCreateTime(String createTime) {
    this.createTime = createTime;
  }
}
