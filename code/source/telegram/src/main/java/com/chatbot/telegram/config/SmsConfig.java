package com.chatbot.telegram.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SmsConfig {
  @Value("${sms.url}")
  private String url;
  
  @Value("${sms.api-key}")
  private String apiKey;
  
  @Value("${sms.api-secret}")
  private String apiSecret;
  
  @Value("${limitTimeOtp}")
  private long limitTimeOtp;
  
  @Value("${sms.offsetPerPageSmsLog}")
  private int offsetSmsLog;
  
  public int getOffsetSmsLog() {
    return this.offsetSmsLog;
  }
  
  public void setOffsetSmsLog(int offsetSmsLog) {
    this.offsetSmsLog = offsetSmsLog;
  }
  
  public long getLimitTimeOtp() {
    return this.limitTimeOtp;
  }
  
  public void setLimitTimeOtp(long limitTimeOtp) {
    this.limitTimeOtp = limitTimeOtp;
  }
  
  public String getUrl() {
    return this.url;
  }
  
  public void setUrl(String url) {
    this.url = url;
  }
  
  public String getApiKey() {
    return this.apiKey;
  }
  
  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }
  
  public String getApiSecret() {
    return this.apiSecret;
  }
  
  public void setApiSecret(String apiSecret) {
    this.apiSecret = apiSecret;
  }
}
