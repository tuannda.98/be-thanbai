package com.chatbot.telegram.controllers;

import com.chatbot.telegram.config.SmsConfig;
import com.chatbot.telegram.models.request.SendSmsRequest;
import com.chatbot.telegram.models.request.VerifySmsRequest;
import com.chatbot.telegram.models.response.LogOtpSmsResponse;
import com.chatbot.telegram.models.response.LogSmsResponse;
import com.chatbot.telegram.models.response.ResponseModel;
import com.chatbot.telegram.services.define.SmsService;
import com.chatbot.telegram.ultilities.DateTimeUltility;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/sms"})
public class SmsController {
  private static final Logger logger = LoggerFactory.getLogger(com.chatbot.telegram.controllers.SmsController.class);
  
  private final SmsService smsService;
  
  private final SmsConfig smsConfig;
  
  public SmsController(SmsService smsService, SmsConfig smsConfig) {
    this.smsService = smsService;
    this.smsConfig = smsConfig;
  }
  
  @PostMapping({"/otp"})
  public ResponseEntity<?> sendSmsOtp(@RequestBody SendSmsRequest request) throws JsonProcessingException {
    int result = this.smsService.send(request.getPhone());
    ResponseModel response = new ResponseModel();
    response.setStatus(result);
    return ResponseEntity.ok(response);
  }
  
  @PostMapping({"/otp/verify"})
  public ResponseEntity<?> verifyOtp(@RequestBody VerifySmsRequest request) throws JsonProcessingException {
    logger.info("request param: " + (new ObjectMapper()).writeValueAsString(request));
    int result = this.smsService.verify(request.getNickName(), request.getPhone(), request.getOtp());
    logger.info("response: " + result);
    ResponseModel response = new ResponseModel();
    response.setStatus(result);
    if (response.getStatus() != 0) {
      response.setMessage("error");
    } else {
      response.setMessage("success");
    } 
    if (response.getStatus() == 0) {
      response.setResult("Bạn có thể dùng số điện thoại này");
    } else if (response.getStatus() == 6) {
      response.setResult("Nick Name không tồn tại");
    } else if (response.getStatus() == 3) {
      response.setResult("Otp sms không chính xác");
    } else if (response.getStatus() == 4) {
      response.setResult("OTP sms đã hết hạn sử dụng");
    } 
    return ResponseEntity.ok(response);
  }
  
  @GetMapping({"/log"})
  public ResponseEntity<?> getLogOtp(@RequestParam(value = "mobile", required = false) String mobile, @RequestParam(value = "from_date", required = false) String strFromDate, @RequestParam(value = "to_date", required = false) String strToDate, @RequestParam(value = "request_id", required = false) String requestId, @RequestParam(value = "page", required = false) Integer page) {
    Long fromDate = DateTimeUltility.convertStringToLongTimestamp(strFromDate);
    Long toDate = DateTimeUltility.convertStringToLongTimestamp(strToDate);
    LogSmsResponse<LogOtpSmsResponse> response = this.smsService.searchLogSms(mobile, fromDate, toDate, requestId, page.intValue(), this.smsConfig.getOffsetSmsLog());
    return ResponseEntity.ok(response);
  }
}
