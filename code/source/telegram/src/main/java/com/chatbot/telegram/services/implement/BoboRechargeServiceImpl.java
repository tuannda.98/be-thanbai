package com.chatbot.telegram.services.implement;

import com.chatbot.telegram.models.response.BoboResponse;
import com.chatbot.telegram.models.response.PageResponse;
import com.chatbot.telegram.repositories.mongodb.BoboRepository;
import com.chatbot.telegram.services.define.BoboRechargeService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
public class BoboRechargeServiceImpl implements BoboRechargeService {
  BoboRepository boboRepository;
  
  public BoboRechargeServiceImpl(BoboRepository boboRepository) {
    this.boboRepository = boboRepository;
  }
  
  public PageResponse<BoboResponse> searchBobo(String nickName, String ip, String serial, String telco, String fromDate, String toDate, int page, int size) {
    Page<BoboResponse> pageResult = this.boboRepository.searchBobo(nickName, ip, serial, telco, fromDate, toDate, page, size);
    return new PageResponse(pageResult);
  }
}
