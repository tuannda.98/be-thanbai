package com.chatbot.telegram.entites.mongo;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "telegram_otp")
public class OtpEntity {
  @Id
  private String id;
  
  private String mobile;
  
  private String otp;
  
  private String type;
  
  @Field(name = "chat_id")
  private String chatId;
  
  @Field(name = "otp_create")
  private Long otpCreate;
  
  @Field(name = "status_sms")
  private long statusSms;
  
  @Field(name = "request_id")
  private String requestId;
  
  @Field(name = "sms_id")
  private String smsId;
  
  public String getRequestId() {
    return this.requestId;
  }
  
  public void setRequestId(String requestId) {
    this.requestId = requestId;
  }
  
  public String getSmsId() {
    return this.smsId;
  }
  
  public void setSmsId(String smsId) {
    this.smsId = smsId;
  }
  
  public String getChatId() {
    return this.chatId;
  }
  
  public void setChatId(String chatId) {
    this.chatId = chatId;
  }
  
  public OtpEntity() {}
  
  public OtpEntity(String mobile) {
    this.mobile = mobile;
  }
  
  public String getId() {
    return this.id;
  }
  
  public void setId(String id) {
    this.id = id;
  }
  
  public String getMobile() {
    return this.mobile;
  }
  
  public void setMobile(String mobile) {
    this.mobile = mobile;
  }
  
  public String getOtp() {
    return this.otp;
  }
  
  public void setOtp(String otp) {
    this.otp = otp;
  }
  
  public Long getOtpCreate() {
    return this.otpCreate;
  }
  
  public void setOtpCreate(Long otpCreate) {
    this.otpCreate = otpCreate;
  }
  
  public String getType() {
    return this.type;
  }
  
  public void setType(String type) {
    this.type = type;
  }
  
  public long getStatusSms() {
    return this.statusSms;
  }
  
  public void setStatusSms(long statusSms) {
    this.statusSms = statusSms;
  }
}
