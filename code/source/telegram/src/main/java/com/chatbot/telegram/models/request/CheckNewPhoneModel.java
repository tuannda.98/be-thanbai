package com.chatbot.telegram.models.request;

public class CheckNewPhoneModel {
  String phone;
  
  String otp;
  
  String nickName;
  
  public CheckNewPhoneModel() {}
  
  public CheckNewPhoneModel(String phone, String otp, String nickName) {
    this.phone = phone;
    this.otp = otp;
    this.nickName = nickName;
  }
  
  public String getPhone() {
    return this.phone;
  }
  
  public void setPhone(String phone) {
    this.phone = phone;
  }
  
  public String getOtp() {
    return this.otp;
  }
  
  public void setOtp(String otp) {
    this.otp = otp;
  }
  
  public String getNickName() {
    return this.nickName;
  }
  
  public void setNickName(String nickName) {
    this.nickName = nickName;
  }
}
