package com.chatbot.telegram.services.define;

import com.chatbot.telegram.models.response.LogOtpSmsResponse;
import com.chatbot.telegram.models.response.LogSmsResponse;
import com.fasterxml.jackson.core.JsonProcessingException;

public interface SmsService {
  int send(String paramString) throws JsonProcessingException;
  
  int verify(String paramString1, String paramString2, String paramString3);
  
  LogSmsResponse<LogOtpSmsResponse> searchLogSms(String paramString1, Long paramLong1, Long paramLong2, String paramString2, int paramInt1, int paramInt2);
}
