package com.chatbot.telegram.repositories.mysql.vinplay;

import com.chatbot.telegram.entites.mysql.vinplay.UsersEntity;
import com.chatbot.telegram.repositories.mysql.vinplay.UserCustomRepository;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<UsersEntity, Long>, UserCustomRepository {
  Optional<UsersEntity> getByMobile(String paramString);
  
  Optional<UsersEntity> getByNickName(String paramString);
  
  @Transactional
  @Modifying
  @Query("update UsersEntity e set e.mobile = :phone, e.status = :status where e.nickName = :nickName")
  int updateMobileForPhoneOtp(@Param("phone") String paramString1, @Param("nickName") String paramString2, @Param("status") int paramInt);
}
