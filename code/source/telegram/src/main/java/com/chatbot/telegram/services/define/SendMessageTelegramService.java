package com.chatbot.telegram.services.define;

import com.chatbot.telegram.models.request.SendAmountChangeRequest;

public interface SendMessageTelegramService {
  boolean sendMessageChangeAmount(SendAmountChangeRequest paramSendAmountChangeRequest);
}
