package com.chatbot.telegram.services.implement;

import com.chatbot.telegram.bot.LuxVipOtpBot;
import com.chatbot.telegram.config.TelegramBotConfig;
import com.chatbot.telegram.entites.mongo.OtpEntity;
import com.chatbot.telegram.entites.mysql.vinplay.UsersEntity;
import com.chatbot.telegram.models.request.SendAmountChangeRequest;
import com.chatbot.telegram.repositories.mongodb.OtpRepository;
import com.chatbot.telegram.repositories.mysql.vinplay.UserRepository;
import com.chatbot.telegram.services.define.SendMessageTelegramService;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

@Service
public class SendMessageTelegramServiceImpl implements SendMessageTelegramService {
    private final TelegramBotConfig telegramBotConfig;

    private final UserRepository userRepository;

    private final OtpRepository otpRepository;

    private final LuxVipOtpBot luxVipOtpBot;

    public SendMessageTelegramServiceImpl(TelegramBotConfig telegramBotConfig, UserRepository userRepository, OtpRepository otpRepository, LuxVipOtpBot luxVipOtpBot) {
        this.telegramBotConfig = telegramBotConfig;
        this.userRepository = userRepository;
        this.otpRepository = otpRepository;
        this.luxVipOtpBot = luxVipOtpBot;
    }

    public boolean sendMessageChangeAmount(SendAmountChangeRequest sendAmountChangeRequest) {
        UsersEntity userSend = this.userRepository.getByNickName(sendAmountChangeRequest.getNicknameSend()).orElse(null);
        UsersEntity userRecive = this.userRepository.getByNickName(sendAmountChangeRequest.getNicknameReceive()).orElse(null);
        if (userSend != null && !userSend.getMobile().isEmpty()) {
            OtpEntity otpEntity = this.otpRepository.getByMobileAndType(userSend.getMobile(), "telegram").orElse(null);
            if (otpEntity != null && !otpEntity.getChatId().isEmpty()) {
                String messageforSendUser = "Giao dịch: -" + sendAmountChangeRequest.getLuxChange() + "\nSố dư cuối: " + sendAmountChangeRequest.getAmountNicknameSend() + "\nNội dung: \nChuyển tiền cho nick name: " + sendAmountChangeRequest.getNicknameReceive() + "\nTin nhắn:\n" + sendAmountChangeRequest.getDescription();
                SendMessage sendMessage = new SendMessage();
                sendMessage.setChatId(otpEntity.getChatId());
                sendMessage.setText(messageforSendUser);
                this.luxVipOtpBot.sendMessage(sendMessage);
            }
        }
        if (userRecive != null && userRecive.getMobile() != "") {
            OtpEntity otpEntity = this.otpRepository.getByMobileAndType(userRecive.getMobile(), "telegram").orElse(null);
            if (otpEntity != null && otpEntity.getChatId() != "") {
                String messageForReciveUser = "Giao dịch: +" + sendAmountChangeRequest.getLuxChange() + "\nSố dư cuối: " + sendAmountChangeRequest.getAmountNicknameReceive() + "\nNôi dung: \nNhận tiền từ nick name: " + sendAmountChangeRequest.getNicknameSend() + "\nTin nhắn:\n" + sendAmountChangeRequest.getDescription();
                SendMessage sendMessage = new SendMessage();
                sendMessage.setText(messageForReciveUser);
                sendMessage.setChatId(otpEntity.getChatId());
                this.luxVipOtpBot.sendMessage(sendMessage);
            }
        }
        return true;
    }
}
