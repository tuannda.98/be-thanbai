package com.chatbot.telegram.models.response;

public class BoboResponse {
  private String id;
  
  private String userName;
  
  private String requestId;
  
  private String nickName;
  
  private String ip;
  
  private String serial;
  
  private long status;
  
  private long amount;
  
  private String telco;
  
  private String errorMessage;
  
  private String signature;
  
  private String signatureGen;
  
  private String createTime;
  
  public String getId() {
    return this.id;
  }
  
  public void setId(String id) {
    this.id = id;
  }
  
  public String getUserName() {
    return this.userName;
  }
  
  public void setUserName(String userName) {
    this.userName = userName;
  }
  
  public String getRequestId() {
    return this.requestId;
  }
  
  public void setRequestId(String requestId) {
    this.requestId = requestId;
  }
  
  public String getNickName() {
    return this.nickName;
  }
  
  public void setNickName(String nickName) {
    this.nickName = nickName;
  }
  
  public String getIp() {
    return this.ip;
  }
  
  public void setIp(String ip) {
    this.ip = ip;
  }
  
  public String getSerial() {
    return this.serial;
  }
  
  public void setSerial(String serial) {
    this.serial = serial;
  }
  
  public long getStatus() {
    return this.status;
  }
  
  public void setStatus(long status) {
    this.status = status;
  }
  
  public long getAmount() {
    return this.amount;
  }
  
  public void setAmount(long amount) {
    this.amount = amount;
  }
  
  public String getTelco() {
    return this.telco;
  }
  
  public void setTelco(String telco) {
    this.telco = telco;
  }
  
  public String getErrorMessage() {
    return this.errorMessage;
  }
  
  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }
  
  public String getSignature() {
    return this.signature;
  }
  
  public void setSignature(String signature) {
    this.signature = signature;
  }
  
  public String getSignatureGen() {
    return this.signatureGen;
  }
  
  public void setSignatureGen(String signatureGen) {
    this.signatureGen = signatureGen;
  }
  
  public String getCreateTime() {
    return this.createTime;
  }
  
  public void setCreateTime(String createTime) {
    this.createTime = createTime;
  }
}
