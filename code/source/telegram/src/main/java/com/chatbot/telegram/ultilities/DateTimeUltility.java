package com.chatbot.telegram.ultilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.springframework.util.StringUtils;

public class DateTimeUltility {
  public static long compareTimeToSeconds(long newTime, long oldTime) {
    long diff = newTime - oldTime;
    long resultSeconds = TimeUnit.MILLISECONDS.toSeconds(diff);
    return resultSeconds;
  }
  
  public static Long convertStringToLongTimestamp(String date) {
    return convertStringToLongTimestamp(date, null);
  }
  
  public static Long convertStringToLongTimestamp(String date, String format) {
    if (StringUtils.isEmpty(date))
      return null; 
    if (StringUtils.isEmpty(format))
      format = "yyyy-MM-dd HH:mm:ss"; 
    SimpleDateFormat formatter = new SimpleDateFormat(format);
    try {
      return Long.valueOf(formatter.parse(date).getTime());
    } catch (ParseException e) {
      return null;
    } 
  }
  
  public static String convertLongTimestampToString(Long date) {
    return convertLongTimestampToString(date, null);
  }
  
  public static String convertLongTimestampToString(Long date, String format) {
    if (StringUtils.isEmpty(date))
      return null; 
    if (StringUtils.isEmpty(format))
      format = "yyyy-MM-dd HH:mm:ss"; 
    SimpleDateFormat formatter = new SimpleDateFormat(format);
    return formatter.format(new Date(date.longValue()));
  }
}
