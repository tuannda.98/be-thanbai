package com.chatbot.telegram.services.define;

import com.chatbot.telegram.models.response.BoboResponse;
import com.chatbot.telegram.models.response.PageResponse;

public interface BoboRechargeService {
  PageResponse<BoboResponse> searchBobo(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, int paramInt1, int paramInt2);
}
