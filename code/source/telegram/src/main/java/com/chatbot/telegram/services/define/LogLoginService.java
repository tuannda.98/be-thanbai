package com.chatbot.telegram.services.define;

public interface LogLoginService {
  Long allowLogin(String paramString);
  
  void loginSuccess(String paramString);
  
  void loginFail(String paramString);
}
