package com.chatbot.telegram.models.response.game.account;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LoginCountResponse {
  @JsonProperty("allow_login")
  private boolean allowLogin;
  
  @JsonProperty("time_wait")
  private Long timeWait;
  
  public LoginCountResponse() {}
  
  public LoginCountResponse(boolean allowLogin, Long timeWait) {
    this.allowLogin = allowLogin;
    this.timeWait = timeWait;
  }
  
  public boolean isAllowLogin() {
    return this.allowLogin;
  }
  
  public void setAllowLogin(boolean allowLogin) {
    this.allowLogin = allowLogin;
  }
  
  public Long getTimeWait() {
    return this.timeWait;
  }
  
  public void setTimeWait(Long timeWait) {
    this.timeWait = timeWait;
  }
}
