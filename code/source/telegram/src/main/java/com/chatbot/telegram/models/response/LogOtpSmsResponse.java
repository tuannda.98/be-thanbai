package com.chatbot.telegram.models.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LogOtpSmsResponse {
  private String mobile;
  
  @JsonProperty("messageMT")
  private String otp;
  
  private String type;
  
  @JsonProperty("transTime")
  private String create;
  
  @JsonProperty("responseMT")
  private long status;
  
  private String requestId;
  
  @JsonProperty("sms_id")
  private String smsId;
  
  public String getMobile() {
    return this.mobile;
  }
  
  public void setMobile(String mobile) {
    this.mobile = mobile;
  }
  
  public String getOtp() {
    return this.otp;
  }
  
  public void setOtp(String otp) {
    this.otp = otp;
  }
  
  public String getType() {
    return this.type;
  }
  
  public void setType(String type) {
    this.type = type;
  }
  
  public String getCreate() {
    return this.create;
  }
  
  public void setCreate(String create) {
    this.create = create;
  }
  
  public long getStatus() {
    return this.status;
  }
  
  public void setStatus(long status) {
    this.status = status;
  }
  
  public String getRequestId() {
    return this.requestId;
  }
  
  public void setRequestId(String requestId) {
    this.requestId = requestId;
  }
  
  public String getSmsId() {
    return this.smsId;
  }
  
  public void setSmsId(String smsId) {
    this.smsId = smsId;
  }
}
