package com.chatbot.telegram.entites.mongo;

import javax.persistence.Entity;
import javax.persistence.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Entity
@Document(collection = "momo_recharge_result")
public class MomoRechargeEntity {
  @Id
  private String id;
  
  @Field(name = "trans_id")
  private String transId;
  
  @Field(name = "nick_name")
  private String nickName;
  
  private String ip;
  
  @Field(name = "request_time")
  private String requestTime;
  
  private String amount;
  
  private String phone;
  
  private String type;
  
  @Field(name = "auth_key")
  private String authKey;
  
  @Field(name = "auth_key_gen")
  private String authKeyGen;
  
  @Field(name = "error_message")
  private String errorMessage;
  
  @Field(name = "create_time")
  private String createTime;
  
  public MomoRechargeEntity() {}
  
  public MomoRechargeEntity(String id, String transId, String nickName, String ip, String requestTime, String amount, String phone, String type, String authKey, String authKeyGen, String errorMessage, String createTime) {
    this.id = id;
    this.transId = transId;
    this.nickName = nickName;
    this.ip = ip;
    this.requestTime = requestTime;
    this.amount = amount;
    this.phone = phone;
    this.type = type;
    this.authKey = authKey;
    this.authKeyGen = authKeyGen;
    this.errorMessage = errorMessage;
    this.createTime = createTime;
  }
  
  public String getId() {
    return this.id;
  }
  
  public void setId(String id) {
    this.id = id;
  }
  
  public String getTransId() {
    return this.transId;
  }
  
  public void setTransId(String transId) {
    this.transId = transId;
  }
  
  public String getNickName() {
    return this.nickName;
  }
  
  public void setNickName(String nickName) {
    this.nickName = nickName;
  }
  
  public String getIp() {
    return this.ip;
  }
  
  public void setIp(String ip) {
    this.ip = ip;
  }
  
  public String getRequestTime() {
    return this.requestTime;
  }
  
  public void setRequestTime(String requestTime) {
    this.requestTime = requestTime;
  }
  
  public String getAmount() {
    return this.amount;
  }
  
  public void setAmount(String amount) {
    this.amount = amount;
  }
  
  public String getPhone() {
    return this.phone;
  }
  
  public void setPhone(String phone) {
    this.phone = phone;
  }
  
  public String getType() {
    return this.type;
  }
  
  public void setType(String type) {
    this.type = type;
  }
  
  public String getAuthKey() {
    return this.authKey;
  }
  
  public void setAuthKey(String authKey) {
    this.authKey = authKey;
  }
  
  public String getAuthKeyGen() {
    return this.authKeyGen;
  }
  
  public void setAuthKeyGen(String authKeyGen) {
    this.authKeyGen = authKeyGen;
  }
  
  public String getErrorMessage() {
    return this.errorMessage;
  }
  
  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }
  
  public String getCreateTime() {
    return this.createTime;
  }
  
  public void setCreateTime(String createTime) {
    this.createTime = createTime;
  }
}
