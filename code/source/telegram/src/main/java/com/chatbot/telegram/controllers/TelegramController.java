package com.chatbot.telegram.controllers;

import com.chatbot.telegram.config.SmsConfig;
import com.chatbot.telegram.entites.mongo.OtpEntity;
import com.chatbot.telegram.entites.mysql.vinplay.UsersEntity;
import com.chatbot.telegram.models.request.CheckNewPhoneModel;
import com.chatbot.telegram.models.request.SendAmountChangeRequest;
import com.chatbot.telegram.models.response.ResponseModel;
import com.chatbot.telegram.repositories.mongodb.OtpRepository;
import com.chatbot.telegram.repositories.mysql.vinplay.UserRepository;
import com.chatbot.telegram.services.define.OtpTelegramService;
import com.chatbot.telegram.services.define.SendMessageTelegramService;
import com.chatbot.telegram.ultilities.DateTimeUltility;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = {"/telegram"}, method = {RequestMethod.GET, RequestMethod.POST})
public class TelegramController {
  private final OtpTelegramService otpTelegramService;
  
  private final OtpRepository otpRepository;
  
  private final UserRepository userRepository;
  
  private final SendMessageTelegramService sendMessageTelegramService;
  
  private final SmsConfig smsConfig;
  
  private static final Logger logger = LoggerFactory.getLogger(com.chatbot.telegram.controllers.TelegramController.class);
  
  public TelegramController(OtpTelegramService otpTelegramService, OtpRepository otpRepository, UserRepository userRepository, SendMessageTelegramService sendMessageTelegramService, SmsConfig smsConfig) {
    this.otpTelegramService = otpTelegramService;
    this.otpRepository = otpRepository;
    this.userRepository = userRepository;
    this.sendMessageTelegramService = sendMessageTelegramService;
    this.smsConfig = smsConfig;
  }
  
  @PostMapping({"/checknewphoneotp"})
  ResponseEntity<ResponseModel> checkNewPhoneWithOtp(@RequestBody CheckNewPhoneModel checkNewPhoneModel) throws JsonProcessingException {
    logger.info("checknewphoneotp: {}", (new ObjectMapper()).writeValueAsString(checkNewPhoneModel));
    ResponseModel responseModel = new ResponseModel();
    if (checkNewPhoneModel.getOtp().length() < 5) {
      responseModel.setStatus(3);
      responseModel.setMessage("error");
      responseModel.setResult("Otp telegram không chính xác.");
      return ResponseEntity.ok(responseModel);
    } 
    UsersEntity usersEntity = this.userRepository.getByNickName(checkNewPhoneModel.getNickName()).orElse(null);
    if (usersEntity != null) {
      OtpEntity otpEntity = this.otpRepository.getByMobileAndOtpAndType(usersEntity.getMobile(), checkNewPhoneModel.getOtp(), "telegram").orElse(null);
      if (otpEntity != null) {
        if (DateTimeUltility.compareTimeToSeconds(System.currentTimeMillis(), otpEntity.getOtpCreate().longValue()) > this.smsConfig.getLimitTimeOtp()) {
          responseModel.setStatus(3);
          responseModel.setMessage("error");
          responseModel.setResult("OTP letegram đã hết hạn sử dụng!");
          return ResponseEntity.ok(responseModel);
        } 
        otpEntity.setOtp("");
        this.otpRepository.save(otpEntity);
        UsersEntity checkUsersEntity = this.userRepository.getByMobile(checkNewPhoneModel.getPhone()).orElse(null);
        if (checkUsersEntity != null) {
          responseModel.setStatus(5);
          responseModel.setMessage("error");
          responseModel.setResult("Số điện thoại đã được đăng ký bởi tài khoản khác!");
        } else if (validatePhoneNumber(checkNewPhoneModel.getPhone())) {
          responseModel.setStatus(0);
          responseModel.setMessage("success");
          responseModel.setResult("Bạn có thể dùng số điện thoại này");
        } else {
          responseModel.setStatus(4);
          responseModel.setMessage("error");
          responseModel.setResult("SĐT không đúng định dạng.");
        } 
      } else {
        responseModel.setStatus(3);
        responseModel.setMessage("error");
        responseModel.setResult("Otp telegram không chính xác.");
      } 
    } else {
      responseModel.setStatus(6);
      responseModel.setMessage("error");
      responseModel.setResult("Nick Name không tồn tại");
    } 
    logger.info("checknewphoneotp - response: {}", (new ObjectMapper()).writeValueAsString(responseModel));
    return ResponseEntity.ok(responseModel);
  }
  
  @PostMapping({"/verifyotp"})
  ResponseEntity<ResponseModel> verifyOtp(@RequestBody CheckNewPhoneModel checkNewPhoneModel) throws JsonProcessingException {
    logger.info("verify otp {}", (new ObjectMapper()).writeValueAsString(checkNewPhoneModel));
    ResponseModel responseModel = new ResponseModel();
    if (checkNewPhoneModel.getOtp().length() < 5) {
      responseModel.setStatus(3);
      responseModel.setMessage("error");
      responseModel.setResult("Otp telegram không chính xác!");
      return ResponseEntity.ok(responseModel);
    } 
    UsersEntity usersEntity = this.userRepository.getByNickName(checkNewPhoneModel.getNickName()).orElse(null);
    if (usersEntity != null) {
      OtpEntity otpEntity = this.otpRepository.getByMobileAndOtpAndType(usersEntity.getMobile(), checkNewPhoneModel.getOtp(), "telegram").orElse(null);
      if (otpEntity != null) {
        if (DateTimeUltility.compareTimeToSeconds(System.currentTimeMillis(), otpEntity.getOtpCreate().longValue()) > this.smsConfig.getLimitTimeOtp()) {
          responseModel.setStatus(4);
          responseModel.setMessage("error");
          responseModel.setResult("OTP telegram đã hết hạn sử dụng");
          return ResponseEntity.ok(responseModel);
        } 
        otpEntity.setOtp("");
        otpEntity.setOtpCreate(Long.valueOf(System.currentTimeMillis()));
        this.otpRepository.save(otpEntity);
        responseModel.setStatus(0);
        responseModel.setMessage("success");
        responseModel.setResult("Xác thực OTP thành công");
      } else {
        responseModel.setStatus(3);
        responseModel.setMessage("error");
        responseModel.setResult("Otp telegram không chính xác!");
      } 
    } else {
      responseModel.setStatus(2);
      responseModel.setMessage("error");
      responseModel.setResult("Không tồn tại nick name!");
    } 
    logger.info("response: {}", (new ObjectMapper()).writeValueAsString(responseModel));
    return ResponseEntity.ok(responseModel);
  }
  
  @PostMapping({"/amountchange"})
  ResponseEntity<?> sendSmsAmountChange(@RequestBody SendAmountChangeRequest sendAmountChangeRequest) throws JsonProcessingException {
    logger.info("sms telegram amount change {}", (new ObjectMapper()).writeValueAsString(sendAmountChangeRequest));
    boolean result = this.sendMessageTelegramService.sendMessageChangeAmount(sendAmountChangeRequest);
    return ResponseEntity.ok(Boolean.valueOf(result));
  }
  
  private boolean validatePhoneNumber(String phoneNumber) {
    Pattern p = Pattern.compile("^0\\d{9}$");
    Matcher m = p.matcher(phoneNumber);
    return (m.find() && m.group().equals(phoneNumber));
  }
}
