package com.chatbot.telegram.controllers;

import com.chatbot.telegram.models.response.MomoResponse;
import com.chatbot.telegram.models.response.PageResponse;
import com.chatbot.telegram.services.define.MomoRechargeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/momo"})
public class MomoController {
  private static final Logger logger = LoggerFactory.getLogger(com.chatbot.telegram.controllers.MomoController.class);
  
  private final MomoRechargeService momoRechargeService;
  
  public MomoController(MomoRechargeService momoRechargeService) {
    this.momoRechargeService = momoRechargeService;
  }
  
  @GetMapping({"/search"})
  public ResponseEntity<?> getTransaction(@RequestParam(name = "nick_name", required = false) String nickName, @RequestParam(name = "ip", required = false) String ip, @RequestParam(name = "phone", required = false) String phone, @RequestParam(name = "from_date", required = false) String fromDate, @RequestParam(name = "to_date", required = false) String toDate, @RequestParam(name = "page", defaultValue = "1") int page, @RequestParam(name = "size", defaultValue = "50") int size) {
    PageResponse<MomoResponse> pageResponse = this.momoRechargeService.searchLogMomo(nickName, ip, phone, fromDate, toDate, page, size);
    logger.info(pageResponse.toString());
    return ResponseEntity.ok(pageResponse);
  }
}
