package com.chatbot.telegram.models.request;

public class SendAmountChangeRequest {
  private String nicknameSend;
  
  private String nicknameReceive;
  
  private long luxChange;
  
  private String description;
  
  private long amountNicknameSend;
  
  private long amountNicknameReceive;
  
  public SendAmountChangeRequest(String nicknameSend, String nicknameReceive, long luxChange, String description, long amountNicknameSend, long amountNicknameReceive) {
    this.nicknameSend = nicknameSend;
    this.nicknameReceive = nicknameReceive;
    this.luxChange = luxChange;
    this.description = description;
    this.amountNicknameSend = amountNicknameSend;
    this.amountNicknameReceive = amountNicknameReceive;
  }
  
  public SendAmountChangeRequest() {}
  
  public String getNicknameSend() {
    return this.nicknameSend;
  }
  
  public void setNicknameSend(String nicknameSend) {
    this.nicknameSend = nicknameSend;
  }
  
  public String getNicknameReceive() {
    return this.nicknameReceive;
  }
  
  public void setNicknameReceive(String nicknameReceive) {
    this.nicknameReceive = nicknameReceive;
  }
  
  public long getLuxChange() {
    return this.luxChange;
  }
  
  public void setLuxChange(long luxChange) {
    this.luxChange = luxChange;
  }
  
  public String getDescription() {
    return this.description;
  }
  
  public void setDescription(String description) {
    this.description = description;
  }
  
  public long getAmountNicknameSend() {
    return this.amountNicknameSend;
  }
  
  public void setAmountNicknameSend(long amountNicknameSend) {
    this.amountNicknameSend = amountNicknameSend;
  }
  
  public long getAmountNicknameReceive() {
    return this.amountNicknameReceive;
  }
  
  public void setAmountNicknameReceive(long amountNicknameReceive) {
    this.amountNicknameReceive = amountNicknameReceive;
  }
}
