package com.chatbot.telegram.models.request;

public class VerifySmsRequest {
  private String otp;
  
  private String phone;
  
  private String nickName;
  
  public String getOtp() {
    return this.otp;
  }
  
  public void setOtp(String otp) {
    this.otp = otp;
  }
  
  public String getPhone() {
    return this.phone;
  }
  
  public void setPhone(String phone) {
    this.phone = phone;
  }
  
  public String getNickName() {
    return this.nickName;
  }
  
  public void setNickName(String nickName) {
    this.nickName = nickName;
  }
}
