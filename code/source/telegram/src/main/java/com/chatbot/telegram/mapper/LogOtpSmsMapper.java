package com.chatbot.telegram.mapper;

import com.chatbot.telegram.entites.mongo.OtpEntity;
import com.chatbot.telegram.models.response.LogOtpSmsResponse;
import com.chatbot.telegram.ultilities.DateTimeUltility;
import com.github.rozidan.springboot.modelmapper.TypeMapConfigurer;
import org.modelmapper.Converter;
import org.modelmapper.TypeMap;
import org.modelmapper.builder.ConfigurableMapExpression;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;

@Component
public class LogOtpSmsMapper extends TypeMapConfigurer<OtpEntity, LogOtpSmsResponse> {
  public String getTypeMapName() {
    return getClass().getName();
  }
  
  public void configure(TypeMap<OtpEntity, LogOtpSmsResponse> typeMap) {
    typeMap.addMapping(OtpEntity::getStatusSms, LogOtpSmsResponse::setStatus);
    Converter<Long, String> toString = ctx -> DateTimeUltility.convertLongTimestampToString((Long)ctx.getSource(), "dd-MM-yyyy HH:mm:ss");
    typeMap.addMappings(mapping -> mapping.using(toString).map(OtpEntity::getOtpCreate, LogOtpSmsResponse::setCreate));
  }
}
