package com.chatbot.telegram.config;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "adminEntityManagerFactory", basePackages = {"com.chatbot.telegram.repositories.mysql.vinadmin"})
public class VinadminMysqlConfig {
  @Bean(name = {"barDataSource"})
  @ConfigurationProperties(prefix = "vinadmin.datasource")
  public DataSource dataSource() {
    return DataSourceBuilder.create().build();
  }
  
  @Bean(name = {"adminEntityManagerFactory"})
  public LocalContainerEntityManagerFactoryBean barEntityManagerFactory(EntityManagerFactoryBuilder builder, @Qualifier("barDataSource") DataSource dataSource) {
      return builder
      .dataSource(dataSource)
      .packages(new String[] { "com.chatbot.telegram.entites.mysql.vinadmin" }).persistenceUnit("vinadmin")
      .build();
  }
  
  @Bean(name = {"adminTransactionManager"})
  public PlatformTransactionManager barTransactionManager(@Qualifier("adminEntityManagerFactory") EntityManagerFactory barEntityManagerFactory) {
    return (PlatformTransactionManager)new JpaTransactionManager(barEntityManagerFactory);
  }
}
