package com.chatbot.telegram.services.implement;

import com.chatbot.telegram.config.SmsConfig;
import com.chatbot.telegram.entites.mongo.OtpEntity;
import com.chatbot.telegram.entites.mysql.vinplay.UsersEntity;
import com.chatbot.telegram.mapper.LogOtpSmsMapper;
import com.chatbot.telegram.models.response.EsmsSendResponse;
import com.chatbot.telegram.models.response.LogOtpSmsResponse;
import com.chatbot.telegram.models.response.LogSmsResponse;
import com.chatbot.telegram.repositories.mongodb.OtpRepository;
import com.chatbot.telegram.repositories.mysql.vinplay.UserRepository;
import com.chatbot.telegram.services.define.SmsService;
import com.chatbot.telegram.ultilities.DateTimeUltility;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang3.RandomStringUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class SmsServiceImpl implements SmsService {
  private static final Logger logger = LoggerFactory.getLogger(com.chatbot.telegram.services.implement.SmsServiceImpl.class);
  
  private final ModelMapper modelMapper;
  
  private final SmsConfig smsConfig;
  
  private final RestTemplate restTemplate;
  
  private final OtpRepository otpRepository;
  
  private final UserRepository userRepository;
  
  public SmsServiceImpl(ModelMapper modelMapper, SmsConfig smsConfig, RestTemplate restTemplate, OtpRepository otpRepository, UserRepository userRepository) {
    this.modelMapper = modelMapper;
    this.smsConfig = smsConfig;
    this.restTemplate = restTemplate;
    this.otpRepository = otpRepository;
    this.userRepository = userRepository;
  }
  
  public int send(String phone) throws JsonProcessingException {
    String newOtp = RandomStringUtils.randomNumeric(5);
    String contentSms = "Ma xac minh " + newOtp;
    OtpEntity otpEntity = new OtpEntity();
    otpEntity.setType("sms");
    otpEntity.setMobile(phone);
    otpEntity.setOtp(newOtp);
    otpEntity.setOtpCreate(Long.valueOf(System.currentTimeMillis()));
    otpEntity.setRequestId(System.currentTimeMillis() + "");
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    HttpEntity<?> entity = new HttpEntity((MultiValueMap)headers);
    UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(this.smsConfig.getUrl()).queryParam("Phone", new Object[] { phone }).queryParam("Content", new Object[] { contentSms }).queryParam("ApiKey", new Object[] { this.smsConfig.getApiKey() }).queryParam("SecretKey", new Object[] { this.smsConfig.getApiSecret() }).queryParam("SmsType", new Object[] { Integer.valueOf(2) }).queryParam("Brandname", new Object[] { "Verify" });
    this.otpRepository.save(otpEntity);
    ResponseEntity responseEntity = this.restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity, EsmsSendResponse.class, new Object[0]);
    EsmsSendResponse smsResponse = (EsmsSendResponse)responseEntity.getBody();
    logger.info("sms response" + (new ObjectMapper()).writeValueAsString(smsResponse));
    if (!smsResponse.getCodeResult().equals("100")) {
      otpEntity.setStatusSms(0L);
      otpEntity.setSmsId(smsResponse.getSmsId());
      this.otpRepository.save(otpEntity);
      return 1;
    } 
    otpEntity.setStatusSms(1L);
    this.otpRepository.save(otpEntity);
    return 0;
  }
  
  public int verify(String nickName, String phone, String otp) {
    OtpEntity entity = this.otpRepository.getByMobileAndOtpAndTypeAndStatusSms(phone, otp, "sms", 1L).orElse(null);
    if (entity == null)
      return 3; 
    UsersEntity usersEntity = this.userRepository.getByNickName(nickName).orElse(null);
    if (usersEntity == null)
      return 6; 
    if (DateTimeUltility.compareTimeToSeconds(System.currentTimeMillis(), entity.getOtpCreate().longValue()) > this.smsConfig.getLimitTimeOtp())
      return 4; 
    this.userRepository.updateMobileForPhoneOtp(phone, nickName, usersEntity.getStatus() | 0x10);
    entity.setStatusSms(2L);
    entity.setOtpCreate(Long.valueOf(System.currentTimeMillis()));
    this.otpRepository.save(entity);
    return 0;
  }
  
  public LogSmsResponse<LogOtpSmsResponse> searchLogSms(String mobile, Long fromDate, Long toDate, String requestId, int page, int size) {
    if (page <= 0)
      page = 1; 
    Page<OtpEntity> list = this.otpRepository.searchLogSms(mobile, fromDate, toDate, requestId, page, size);
    List<LogOtpSmsResponse> listResponse = (List<LogOtpSmsResponse>)list.stream().map(item -> (LogOtpSmsResponse)this.modelMapper.map(item, LogOtpSmsResponse.class, LogOtpSmsMapper.class.getName())).collect(Collectors.toList());
    PageImpl pageImpl = new PageImpl(listResponse, list.getPageable(), list.getTotalElements());
    long totalSendSuccess = this.otpRepository.countSmsSuccess(mobile, fromDate, toDate, requestId);
    return new LogSmsResponse((Page)pageImpl, totalSendSuccess);
  }
}
