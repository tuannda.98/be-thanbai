package com.chatbot.telegram.repositories.mongodb;

import com.chatbot.telegram.models.response.MomoResponse;
import org.springframework.data.domain.Page;

public interface MomoRepositoryCustom {
  Page<MomoResponse> searchLogMomo(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, int paramInt1, int paramInt2);
}
