package com.chatbot.telegram.models.request.game.account;

public class LoginSuccessRequest {
  private String ip;
  
  private String headers;
  
  public String getIp() {
    return this.ip;
  }
  
  public void setIp(String ip) {
    this.ip = ip;
  }
  
  public String getHeaders() {
    return this.headers;
  }
  
  public void setHeaders(String headers) {
    this.headers = headers;
  }
}
