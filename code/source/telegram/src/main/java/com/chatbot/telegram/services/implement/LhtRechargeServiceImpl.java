package com.chatbot.telegram.services.implement;

import com.chatbot.telegram.models.response.LhtResponse;
import com.chatbot.telegram.models.response.PageResponse;
import com.chatbot.telegram.repositories.mongodb.LhtRepository;
import com.chatbot.telegram.services.define.LhtRechargeService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
public class LhtRechargeServiceImpl implements LhtRechargeService {
  private final LhtRepository lhtRepository;
  
  public LhtRechargeServiceImpl(LhtRepository lhtRepository) {
    this.lhtRepository = lhtRepository;
  }
  
  public PageResponse<LhtResponse> searchLht(String nickName, String code, String ip, String fromDate, String toDate, int page, int size) {
    Page<LhtResponse> pageResult = this.lhtRepository.searchLhtLog(nickName, code, ip, fromDate, toDate, page, size);
    PageResponse<LhtResponse> pageResponse = new PageResponse(pageResult);
    return pageResponse;
  }
}
