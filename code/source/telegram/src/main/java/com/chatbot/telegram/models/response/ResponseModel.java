package com.chatbot.telegram.models.response;

public class ResponseModel {
  int status;
  
  String message;
  
  Object result;
  
  public int getStatus() {
    return this.status;
  }
  
  public void setStatus(int status) {
    this.status = status;
  }
  
  public String getMessage() {
    return this.message;
  }
  
  public void setMessage(String message) {
    this.message = message;
  }
  
  public Object getResult() {
    return this.result;
  }
  
  public void setResult(Object result) {
    this.result = result;
  }
  
  public ResponseModel(int status, String message, Object result) {
    this.status = status;
    this.message = message;
    this.result = result;
  }
  
  public ResponseModel() {}
}
