package com.chatbot.telegram.filter;

import org.springframework.web.filter.CommonsRequestLoggingFilter;

public class CustomeRequestLoggingFilter extends CommonsRequestLoggingFilter {
  public CustomeRequestLoggingFilter() {
    setIncludeQueryString(true);
    setIncludePayload(true);
    setMaxPayloadLength(10000);
  }
}
