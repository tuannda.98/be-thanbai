package com.chatbot.telegram.repositories.mongodb;

import com.chatbot.telegram.entites.mongo.MomoRechargeEntity;
import com.chatbot.telegram.mapper.MomoMapper;
import com.chatbot.telegram.models.response.MomoResponse;
import com.chatbot.telegram.repositories.mongodb.MomoRepositoryCustom;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.CriteriaDefinition;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.util.StringUtils;

public class MomoRepositoryCustomImpl implements MomoRepositoryCustom {
  private final MongoTemplate mongoTemplate;
  
  private static final Logger logger = LoggerFactory.getLogger(MomoRepositoryCustom.class);
  
  private final ModelMapper modelMapper;
  
  public MomoRepositoryCustomImpl(MongoTemplate mongoTemplate, ModelMapper modelMapper) {
    this.mongoTemplate = mongoTemplate;
    this.modelMapper = modelMapper;
  }
  
  public Page<MomoResponse> searchLogMomo(String nickName, String ip, String phoneNumber, String fromDate, String toDate, int page, int size) {
    Query query = new Query();
    if (!StringUtils.isEmpty(nickName))
      query.addCriteria((CriteriaDefinition)Criteria.where("nick_name").is(nickName)); 
    if (!StringUtils.isEmpty(ip))
      query.addCriteria((CriteriaDefinition)Criteria.where("ip").is(ip)); 
    if (!StringUtils.isEmpty(phoneNumber))
      query.addCriteria((CriteriaDefinition)Criteria.where("phone").is(phoneNumber)); 
    Criteria whereCriteria = null;
    if (!StringUtils.isEmpty(fromDate))
      if (whereCriteria == null) {
        whereCriteria = Criteria.where("create_time").gte(fromDate);
      } else {
        whereCriteria.gte(fromDate);
      }  
    if (!StringUtils.isEmpty(toDate))
      if (whereCriteria == null) {
        whereCriteria = Criteria.where("create_time").lte(toDate);
      } else {
        whereCriteria.lte(toDate);
      }  
    if (whereCriteria != null)
      query.addCriteria((CriteriaDefinition)whereCriteria); 
    logger.info(query.toString());
    long count = this.mongoTemplate.count(query, MomoRechargeEntity.class);
    if (count == 0L)
      return (Page<MomoResponse>)new PageImpl(new ArrayList()); 
    PageRequest pageRequest = PageRequest.of(page - 1, size);
    query.with(Sort.by(Sort.Direction.DESC, new String[] { "create_time" }));
    query.with((Pageable)pageRequest);
    List<MomoRechargeEntity> list = this.mongoTemplate.find(query, MomoRechargeEntity.class);
    List<MomoResponse> listResponse = (List<MomoResponse>)list.stream().map(item -> (MomoResponse)this.modelMapper.map(item, MomoResponse.class, MomoMapper.class.getName())).collect(Collectors.toList());
    return (Page<MomoResponse>)new PageImpl(listResponse, (Pageable)pageRequest, count);
  }
}
