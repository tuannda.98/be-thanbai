package com.chatbot.telegram.controllers;

import com.chatbot.telegram.models.response.LhtResponse;
import com.chatbot.telegram.models.response.PageResponse;
import com.chatbot.telegram.services.define.LhtRechargeService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/lht"})
public class LhtRechargeController {
  private final LhtRechargeService lhtRechargeService;
  
  public LhtRechargeController(LhtRechargeService lhtRechargeService) {
    this.lhtRechargeService = lhtRechargeService;
  }
  
  @GetMapping({"/search"})
  ResponseEntity<?> searchLogLht(@RequestParam(name = "nick_name", required = false) String nickName, @RequestParam(name = "code", required = false) String code, @RequestParam(name = "ip", required = false) String ip, @RequestParam(name = "from_date", required = false) String fromDate, @RequestParam(name = "to_date", required = false) String toDate, @RequestParam(name = "page", defaultValue = "1") int page, @RequestParam(name = "size", defaultValue = "50") int size) {
    PageResponse<LhtResponse> pageResponse = this.lhtRechargeService.searchLht(nickName, code, ip, fromDate, toDate, page, size);
    return ResponseEntity.ok(pageResponse);
  }
}
