package com.chatbot.telegram.bot;

import com.chatbot.telegram.config.TelegramBotConfig;
import com.chatbot.telegram.services.define.OtpTelegramService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

@Component
public class LuxVipOtpBot extends TelegramLongPollingBot {
  private final TelegramBotConfig telegramBotConfig;

  @Autowired
  private OtpTelegramService otpTelegramService;

  public LuxVipOtpBot(TelegramBotConfig telegramBotConfig) {
    this.telegramBotConfig = telegramBotConfig;
  }

  public String getBotUsername() {
    return this.telegramBotConfig.getOptUsername();
  }

  public String getBotToken() {
    return this.telegramBotConfig.getOtpToken();
  }

  public void onUpdateReceived(Update update) {
    List<SendMessage> listSendMessage = new ArrayList<>();
    if (update.hasMessage()) {
      Message message = update.getMessage();
      if (message.hasText()) {
        String text = message.getText();
        if (text.equals("/start")) {
          SendMessage sendMessage = this.otpTelegramService.start(message.getChatId().toString());
          listSendMessage.add(sendMessage);
        } else if (text.equals("Lấy OTP")) {
          SendMessage sendMessage = this.otpTelegramService.getOtp(message.getChatId().toString());
          listSendMessage.add(sendMessage);
        }
      } else if (update.getMessage().hasContact() && !StringUtils.isEmpty(update.getMessage().getContact().getPhoneNumber())) {
        listSendMessage = this.otpTelegramService.confirmPhoneNumbers(update.getMessage().getContact().getPhoneNumber(), message.getChatId().toString());
      }
      try {
        for (SendMessage sendMessage : listSendMessage) {
          System.out.println(sendMessage);
          execute((BotApiMethod)sendMessage);
        }
      } catch (TelegramApiException e) {
        e.printStackTrace();
      }
    }
  }

  public void sendMessage(SendMessage message) {
    try {
      execute((BotApiMethod)message);
    } catch (TelegramApiException e) {
      e.printStackTrace();
    }
  }
}
