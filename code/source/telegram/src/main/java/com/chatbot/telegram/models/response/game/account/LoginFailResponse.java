package com.chatbot.telegram.models.response.game.account;

public class LoginFailResponse {
  private boolean success;
  
  public LoginFailResponse() {}
  
  public LoginFailResponse(boolean success) {
    this.success = success;
  }
  
  public boolean isSuccess() {
    return this.success;
  }
  
  public void setSuccess(boolean success) {
    this.success = success;
  }
}
