package com.chatbot.telegram.repositories.mysql.vinadmin;

import com.chatbot.telegram.entites.mysql.vinadmin.UserAdmin;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserAdminRepository extends JpaRepository<UserAdmin, Long> {}
