package com.chatbot.telegram.repositories.mysql.vinplay;

import com.chatbot.telegram.entites.mysql.vinplay.UsersEntity;
import com.chatbot.telegram.repositories.mysql.vinplay.UserCustomRepository;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;

@Repository
public class UserCustomRepositoryImpl implements UserCustomRepository {
  @PersistenceContext
  EntityManager entityManager;
  
  public UsersEntity getByPhoneNumber(String phoneNumber) {
    String sql = "select * from users where mobile = :mobile";
    Query query = this.entityManager.createNativeQuery(sql, UsersEntity.class);
    query.setParameter("mobile", phoneNumber);
    return (UsersEntity)query.getSingleResult();
  }
}
