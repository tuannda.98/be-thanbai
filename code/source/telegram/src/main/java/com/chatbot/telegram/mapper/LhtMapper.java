package com.chatbot.telegram.mapper;

import com.chatbot.telegram.entites.mongo.LhtRechargeEntity;
import com.chatbot.telegram.models.response.LhtResponse;
import com.github.rozidan.springboot.modelmapper.TypeMapConfigurer;
import org.modelmapper.Converter;
import org.modelmapper.TypeMap;
import org.modelmapper.builder.ConfigurableMapExpression;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class LhtMapper extends TypeMapConfigurer<LhtRechargeEntity, LhtResponse> {
  public String getTypeMapName() {
    return getClass().getName();
  }
  
  public void configure(TypeMap<LhtRechargeEntity, LhtResponse> typeMap) {
    Converter<String, Long> converStringToLong = ctx -> {
        String input = (String)ctx.getSource();
        return StringUtils.isEmpty(input) ? Long.valueOf(0L) : Long.valueOf(Long.parseLong(input));
      };
    typeMap.addMappings(mapping -> mapping.using(converStringToLong).map(LhtRechargeEntity::getAmount, LhtResponse::setAmount));
  }
}
