package com.chatbot.telegram.initializer;

import com.chatbot.telegram.filter.CustomeRequestLoggingFilter;
import java.util.EventListener;
import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

public class CustomWebAppInitializer implements WebApplicationInitializer {
  public void onStartup(ServletContext container) {
    AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
    context.setConfigLocation("com.chatbot.telegram");
    container.addListener((EventListener)new ContextLoaderListener((WebApplicationContext)context));
    ServletRegistration.Dynamic dispatcher = container.addServlet("dispatcher", (Servlet)new DispatcherServlet((WebApplicationContext)context));
    dispatcher.setLoadOnStartup(1);
    dispatcher.addMapping(new String[] { "/" });
    container.addFilter("customRequestLoggingFilter", CustomeRequestLoggingFilter.class)
      .addMappingForServletNames(null, false, new String[] { "dispatcher" });
  }
}
