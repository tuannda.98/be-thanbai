package com.chatbot.telegram.models.response;

import com.chatbot.telegram.models.response.PageResponse;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import org.springframework.data.domain.Page;

public class LogSmsResponse<T> extends PageResponse<T> {
  @JsonProperty("numSendSuccess")
  private long totalSendSuccess;
  
  public LogSmsResponse(int totalSendSuccess) {
    this.totalSendSuccess = totalSendSuccess;
  }
  
  public LogSmsResponse(Page<T> page, long totalSendSuccess) {
    super(page);
    this.totalSendSuccess = totalSendSuccess;
  }
  
  public LogSmsResponse(List<T> result, int totalPage, long totalElement, int page, int size, int totalSendSuccess) {
    super(result, totalPage, totalElement, page, size);
    this.totalSendSuccess = totalSendSuccess;
  }
  
  public long getTotalSendSuccess() {
    return this.totalSendSuccess;
  }
  
  public void setTotalSendSuccess(long totalSendSuccess) {
    this.totalSendSuccess = totalSendSuccess;
  }
}
