package com.chatbot.telegram.services.define;

import com.chatbot.telegram.models.response.LhtResponse;
import com.chatbot.telegram.models.response.PageResponse;

public interface LhtRechargeService {
  PageResponse<LhtResponse> searchLht(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, int paramInt1, int paramInt2);
}
