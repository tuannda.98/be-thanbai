package com.chatbot.telegram.models.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EsmsSendResponse {
  @JsonProperty("CodeResult")
  private String codeResult;
  
  @JsonProperty("CountRegenerate")
  private Integer countRegenerate;
  
  @JsonProperty("SMSID")
  private String smsId;
  
  @JsonProperty("ErrorMessage")
  private String errorMessage;
  
  public String getCodeResult() {
    return this.codeResult;
  }
  
  public void setCodeResult(String codeResult) {
    this.codeResult = codeResult;
  }
  
  public Integer getCountRegenerate() {
    return this.countRegenerate;
  }
  
  public void setCountRegenerate(Integer countRegenerate) {
    this.countRegenerate = countRegenerate;
  }
  
  public String getSmsId() {
    return this.smsId;
  }
  
  public void setSmsId(String smsId) {
    this.smsId = smsId;
  }
  
  public String getErrorMessage() {
    return this.errorMessage;
  }
  
  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }
}
