package com.chatbot.telegram.controllers.games;

import com.chatbot.telegram.models.request.game.account.LoginFailRequest;
import com.chatbot.telegram.models.request.game.account.LoginSuccessRequest;
import com.chatbot.telegram.models.response.game.account.LoginCountResponse;
import com.chatbot.telegram.models.response.game.account.LoginFailResponse;
import com.chatbot.telegram.models.response.game.account.LoginSuccessResponse;
import com.chatbot.telegram.services.define.LogLoginService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/game/account"})
public class AccountController {
  private final LogLoginService logLoginService;
  
  public AccountController(LogLoginService logLoginService) {
    this.logLoginService = logLoginService;
  }
  
  @GetMapping({"/login-count"})
  public ResponseEntity<?> countLoginTime(@RequestParam(name = "ip") String ip) {
    LoginCountResponse response;
    Long timeWait = this.logLoginService.allowLogin(ip);
    if (timeWait != null && timeWait.longValue() > 0L) {
      response = new LoginCountResponse(false, timeWait);
    } else {
      response = new LoginCountResponse(true, null);
    } 
    return ResponseEntity.ok(response);
  }
  
  @PostMapping({"/login-success"})
  public ResponseEntity<?> loginSuccess(@RequestBody LoginSuccessRequest request) {
    this.logLoginService.loginSuccess(request.getIp());
    return ResponseEntity.ok(new LoginSuccessResponse(true));
  }
  
  @PostMapping({"/login-fail"})
  public ResponseEntity<?> loginFail(@RequestBody LoginFailRequest request) {
    this.logLoginService.loginFail(request.getIp());
    return ResponseEntity.ok(new LoginFailResponse(true));
  }
}
