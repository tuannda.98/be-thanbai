package com.chatbot.telegram.mapper;

import com.chatbot.telegram.entites.mongo.MomoRechargeEntity;
import com.chatbot.telegram.models.response.MomoResponse;
import com.github.rozidan.springboot.modelmapper.TypeMapConfigurer;
import org.modelmapper.Converter;
import org.modelmapper.TypeMap;
import org.modelmapper.builder.ConfigurableMapExpression;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class MomoMapper extends TypeMapConfigurer<MomoRechargeEntity, MomoResponse> {
  public String getTypeMapName() {
    return getClass().getName();
  }
  
  public void configure(TypeMap<MomoRechargeEntity, MomoResponse> typeMap) {
    Converter<String, Long> converStringToLong = ctx -> {
        String input = (String)ctx.getSource();
        return StringUtils.isEmpty(input) ? Long.valueOf(0L) : Long.valueOf(Long.parseLong(input));
      };
    typeMap.addMappings(mapping -> mapping.using(converStringToLong).map(MomoRechargeEntity::getAmount, MomoResponse::setAmount));
    typeMap.addMappings(mapping -> mapping.using(converStringToLong).map(MomoRechargeEntity::getType, MomoResponse::setType));
  }
}
