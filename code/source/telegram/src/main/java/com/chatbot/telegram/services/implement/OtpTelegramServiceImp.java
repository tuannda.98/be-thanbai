package com.chatbot.telegram.services.implement;

import com.chatbot.telegram.entites.mongo.OtpEntity;
import com.chatbot.telegram.entites.mysql.vinplay.UsersEntity;
import com.chatbot.telegram.repositories.mongodb.OtpRepository;
import com.chatbot.telegram.repositories.mysql.vinplay.UserRepository;
import com.chatbot.telegram.services.define.OtpTelegramService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

@Service
public class OtpTelegramServiceImp implements OtpTelegramService {
  private final OtpRepository otpRepository;
  
  private final UserRepository userRepository;
  
  public OtpTelegramServiceImp(OtpRepository otpRepository, UserRepository userRepository) {
    this.otpRepository = otpRepository;
    this.userRepository = userRepository;
  }
  
  public List<SendMessage> confirmPhoneNumbers(String phoneNumber, String chatId) {
    String phone = "0" + phoneNumber.replace("+", "").substring(2);
    UsersEntity usersEntity = this.userRepository.getByMobile(phone).orElse(null);
    if (usersEntity != null) {
      OtpEntity checkPhoneExitEntity = this.otpRepository.getByMobileAndType(phone, "telegram").orElse(null);
      if (checkPhoneExitEntity != null) {
        checkPhoneExitEntity.setChatId(chatId);
        checkPhoneExitEntity.setOtpCreate(Long.valueOf(System.currentTimeMillis()));
        this.otpRepository.save(checkPhoneExitEntity);
      } else {
        OtpEntity otpEntity = new OtpEntity();
        otpEntity.setChatId(chatId);
        otpEntity.setMobile(phone);
        otpEntity.setType("telegram");
        otpEntity.setOtpCreate(Long.valueOf(System.currentTimeMillis()));
        this.otpRepository.insert(otpEntity);
      } 
      SendMessage sendMessage3 = createStringMessage("Đã nhận SĐT " + phoneNumber, chatId);
      SendMessage sendMessage4 = createMessageGetOTP(usersEntity.getUserName(), chatId);
      List<SendMessage> list = new ArrayList<>();
      list.add(sendMessage3);
      list.add(sendMessage4);
      return list;
    } 
    List<SendMessage> sendListMessages = new ArrayList<>();
    SendMessage sendMessage1 = createMessageConfirmPhoneNumberError(chatId);
    SendMessage sendMessage2 = createStringMessage("Đã nhận SĐT " + phoneNumber, chatId);
    sendListMessages.add(sendMessage1);
    sendListMessages.add(sendMessage2);
    return sendListMessages;
  }
  
  public SendMessage getOtp(String chatId) {
    OtpEntity resultOtpEntity = this.otpRepository.getByChatId(chatId).orElse(null);
    if (resultOtpEntity != null) {
      String newOtp = RandomStringUtils.randomNumeric(5);
      resultOtpEntity.setType("telegram");
      resultOtpEntity.setOtp(newOtp);
      resultOtpEntity.setOtpCreate(Long.valueOf(System.currentTimeMillis()));
      OtpEntity resultOtp = (OtpEntity)this.otpRepository.save(resultOtpEntity);
      SendMessage sendMessage = createStringMessage(resultOtp.getOtp(), chatId);
      return sendMessage;
    } 
    return null;
  }
  
  public SendMessage start(String chatId) {
    return createMessageSendPhoneNumber(chatId);
  }
  
  private SendMessage createMessageSendPhoneNumber(String chatId) {
    String msg = "Chúng tôi cần xác nhận số điện thoại mà liên kết với tài khoản telegram này và số điện thoại bạn điền trong phần bảo mật tài khoản của fun365.club là một \n\nVui lòng gửi SĐT cho chúng tôi bằng cách bấm vào nút \"<b>Gửi số điện thoại của tôi.</b>\" 👇\n\n Chú ý: Chúng tôi chỉ nhận số điện thoại khi ấn vào nút, không nhận số khi nhập từ bàn phím.";
    SendMessage message = new SendMessage();
    message.setText(msg);
    message.setChatId(chatId);
    message.setParseMode("HTML");
    ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
    replyKeyboardMarkup.setResizeKeyboard(Boolean.valueOf(true));
    replyKeyboardMarkup.setOneTimeKeyboard(Boolean.valueOf(false));
    KeyboardButton keyboardButton = new KeyboardButton();
    keyboardButton.setText("Gửi số điện thoại của tôi");
    keyboardButton.setRequestContact(Boolean.valueOf(true));
    KeyboardRow keyboardRow = new KeyboardRow();
    keyboardRow.add(keyboardButton);
    replyKeyboardMarkup.setKeyboard(Collections.singletonList(keyboardRow));
    message.setReplyMarkup((ReplyKeyboard)replyKeyboardMarkup);
    return message;
  }
  
  private SendMessage createMessageGetOTP(String userName, String chatId) {
    SendMessage message = new SendMessage();
    String msg = "Tài khoản " + userName + " của bạn đã được xác thực và bạn có thể nhận OTP từ telegram. \nTừ giờ bạn có thể lấy OTP miễn phí bằng cách ấn nút Lấy OTP";
    message.setText(msg);
    message.setChatId(chatId);
    ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
    replyKeyboardMarkup.setResizeKeyboard(Boolean.valueOf(true));
    replyKeyboardMarkup.setOneTimeKeyboard(Boolean.valueOf(false));
    KeyboardButton keyboardButton = new KeyboardButton();
    keyboardButton.setText("Lấy OTP");
    KeyboardRow keyboardRow = new KeyboardRow();
    keyboardRow.add(keyboardButton);
    replyKeyboardMarkup.setKeyboard(Collections.singletonList(keyboardRow));
    message.setReplyMarkup((ReplyKeyboard)replyKeyboardMarkup);
    return message;
  }
  
  private SendMessage createMessageConfirmPhoneNumberError(String chatId) {
    String msg = "Bạn vui lòng vào game, nhập SĐT của bạn vào phần bảo mật rồi thực hiện lại";
    SendMessage message = new SendMessage();
    message.setText(msg);
    message.setChatId(chatId);
    ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
    replyKeyboardMarkup.setResizeKeyboard(Boolean.valueOf(true));
    replyKeyboardMarkup.setOneTimeKeyboard(Boolean.valueOf(false));
    KeyboardButton keyboardButton = new KeyboardButton();
    keyboardButton.setText("Gửi số điện thoại của tôi");
    keyboardButton.setRequestContact(Boolean.valueOf(true));
    KeyboardRow keyboardRow = new KeyboardRow();
    keyboardRow.add(keyboardButton);
    replyKeyboardMarkup.setKeyboard(Collections.singletonList(keyboardRow));
    message.setReplyMarkup((ReplyKeyboard)replyKeyboardMarkup);
    return message;
  }
  
  private SendMessage createStringMessage(String msg, String chatId) {
    SendMessage message = new SendMessage();
    message.setText(msg);
    message.setChatId(chatId);
    return message;
  }
}
