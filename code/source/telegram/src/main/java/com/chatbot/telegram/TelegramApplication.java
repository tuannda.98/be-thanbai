package com.chatbot.telegram;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.telegram.telegrambots.ApiContextInitializer;

@SpringBootApplication
public class TelegramApplication {
  public static void main(String[] args) {
    ApiContextInitializer.init();
    SpringApplication.run(com.chatbot.telegram.TelegramApplication.class, args);
//    SpringApplication app = new SpringApplication(MySpringConfiguration.class);
//    app.setBannerMode(Banner.Mode.OFF);
//    app.run(args);
  }
}
