package com.chatbot.telegram.repositories.mongodb;

import com.chatbot.telegram.entites.mongo.OtpEntity;
import com.chatbot.telegram.repositories.mongodb.OtpRepositoryCustom;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.CriteriaDefinition;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.util.StringUtils;

public class OtpRepositoryCustomImpl implements OtpRepositoryCustom {
  private static final Logger logger = LoggerFactory.getLogger(com.chatbot.telegram.repositories.mongodb.OtpRepositoryCustomImpl.class);
  
  private final MongoTemplate mongoTemplate;
  
  public OtpRepositoryCustomImpl(MongoTemplate mongoTemplate) {
    this.mongoTemplate = mongoTemplate;
  }
  
  public Page<OtpEntity> searchLogSms(String mobile, Long fromDate, Long toDate, String requestId, int page, int size) {
    Query query = createSearchLogSms(mobile, fromDate, toDate, requestId);
    long count = this.mongoTemplate.count(query, OtpEntity.class);
    if (count <= 0L)
      return (Page<OtpEntity>)new PageImpl(new ArrayList()); 
    query.with(Sort.by(Sort.Direction.DESC, new String[] { "otpCreate" }));
    PageRequest pageRequest = PageRequest.of(page - 1, size);
    query.with((Pageable)pageRequest);
    logger.debug("searchLogSms: {}", query.toString());
    List<OtpEntity> list = this.mongoTemplate.find(query, OtpEntity.class);
    return (Page<OtpEntity>)new PageImpl(list, (Pageable)pageRequest, count);
  }
  
  public long countSmsSuccess(String mobile, Long fromDate, Long toDate, String requestId) {
    Query query = createSearchLogSms(mobile, fromDate, toDate, requestId);
    query.addCriteria((CriteriaDefinition)Criteria.where("statusSms").gte(Integer.valueOf(1)));
    return this.mongoTemplate.count(query, OtpEntity.class);
  }
  
  private Query createSearchLogSms(String mobile, Long fromDate, Long toDate, String requestId) {
    Query query = new Query();
    if (!StringUtils.isEmpty(mobile))
      query.addCriteria((CriteriaDefinition)Criteria.where("mobile").is(mobile)); 
    if (!StringUtils.isEmpty(requestId))
      query.addCriteria((CriteriaDefinition)Criteria.where("requestId").is(requestId)); 
    Criteria whereCreate = null;
    if (fromDate != null)
      if (whereCreate == null) {
        whereCreate = Criteria.where("otpCreate").gte(fromDate);
      } else {
        whereCreate = whereCreate.gte(fromDate);
      }  
    if (toDate != null)
      if (whereCreate == null) {
        whereCreate = Criteria.where("otpCreate").lte(toDate);
      } else {
        whereCreate = whereCreate.lte(toDate);
      }  
    if (whereCreate != null)
      query.addCriteria((CriteriaDefinition)whereCreate); 
    query.addCriteria((CriteriaDefinition)Criteria.where("type").is("sms"));
    return query;
  }
}
