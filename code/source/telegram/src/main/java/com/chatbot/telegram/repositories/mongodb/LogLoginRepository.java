package com.chatbot.telegram.repositories.mongodb;

import com.chatbot.telegram.entites.mongo.LogLoginEntity;
import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface LogLoginRepository extends MongoRepository<LogLoginEntity, String> {
  Optional<LogLoginEntity> findByIp(String paramString);
}
