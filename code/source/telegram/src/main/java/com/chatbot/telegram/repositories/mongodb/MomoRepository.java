package com.chatbot.telegram.repositories.mongodb;

import com.chatbot.telegram.entites.mongo.MomoRechargeEntity;
import com.chatbot.telegram.repositories.mongodb.MomoRepositoryCustom;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MomoRepository extends MongoRepository<MomoRechargeEntity, String>, MomoRepositoryCustom {}
