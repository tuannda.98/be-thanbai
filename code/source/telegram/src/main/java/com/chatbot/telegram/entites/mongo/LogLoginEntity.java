package com.chatbot.telegram.entites.mongo;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "log_login")
public class LogLoginEntity {
  @Id
  private String id;
  
  private String ip;
  
  private boolean loginFail;
  
  private Integer times;
  
  @Field(name = "create_at")
  private Long creatAt;
  
  @Field(name = "update_at")
  private Long updateAt;
  
  public String getId() {
    return this.id;
  }
  
  public void setId(String id) {
    this.id = id;
  }
  
  public String getIp() {
    return this.ip;
  }
  
  public void setIp(String ip) {
    this.ip = ip;
  }
  
  public boolean isLoginFail() {
    return this.loginFail;
  }
  
  public void setLoginFail(boolean loginFail) {
    this.loginFail = loginFail;
  }
  
  public Integer getTimes() {
    return this.times;
  }
  
  public void setTimes(Integer times) {
    this.times = times;
  }
  
  public Long getCreatAt() {
    return this.creatAt;
  }
  
  public void setCreatAt(Long creatAt) {
    this.creatAt = creatAt;
  }
  
  public Long getUpdateAt() {
    return this.updateAt;
  }
  
  public void setUpdateAt(Long updateAt) {
    this.updateAt = updateAt;
  }
}
