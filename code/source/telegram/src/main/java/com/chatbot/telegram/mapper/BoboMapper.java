package com.chatbot.telegram.mapper;

import com.chatbot.telegram.entites.mongo.BoboRechargeEntity;
import com.chatbot.telegram.models.response.BoboResponse;
import com.github.rozidan.springboot.modelmapper.TypeMapConfigurer;
import org.modelmapper.Converter;
import org.modelmapper.TypeMap;
import org.modelmapper.builder.ConfigurableMapExpression;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class BoboMapper extends TypeMapConfigurer<BoboRechargeEntity, BoboResponse> {
  public String getTypeMapName() {
    return getClass().getName();
  }
  
  public void configure(TypeMap<BoboRechargeEntity, BoboResponse> typeMap) {
    Converter<String, Long> converStringToLong = ctx -> {
        String input = (String)ctx.getSource();
        return StringUtils.isEmpty(input) ? Long.valueOf(0L) : Long.valueOf(Long.parseLong(input));
      };
    typeMap.addMappings(mapping -> mapping.using(converStringToLong).map(BoboRechargeEntity::getAmount, BoboResponse::setAmount));
    typeMap.addMappings(mapping -> mapping.using(converStringToLong).map(BoboRechargeEntity::getStatus, BoboResponse::setStatus));
  }
}
