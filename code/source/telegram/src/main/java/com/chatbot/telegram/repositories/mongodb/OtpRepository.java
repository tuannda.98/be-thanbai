package com.chatbot.telegram.repositories.mongodb;

import com.chatbot.telegram.entites.mongo.OtpEntity;
import com.chatbot.telegram.repositories.mongodb.OtpRepositoryCustom;
import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface OtpRepository extends MongoRepository<OtpEntity, String>, OtpRepositoryCustom {
  Optional<OtpEntity> getByChatId(String paramString);
  
  Optional<OtpEntity> getByMobileAndOtpAndType(String paramString1, String paramString2, String paramString3);
  
  Optional<OtpEntity> getByMobileAndOtpAndTypeAndStatusSms(String paramString1, String paramString2, String paramString3, long paramLong);
  
  Optional<OtpEntity> getByMobileAndType(String paramString1, String paramString2);
}
