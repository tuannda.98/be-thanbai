package com.chatbot.telegram.entites.mysql.vinadmin;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class UserAdmin {
  @Id
  @Column(name = "ID")
  private Long id;
  
  @Column(name = "UserName")
  private String userName;
  
  public Long getId() {
    return this.id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public String getUserName() {
    return this.userName;
  }
  
  public void setUserName(String userName) {
    this.userName = userName;
  }
}
