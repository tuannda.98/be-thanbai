package com.chatbot.telegram.models.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import org.springframework.data.domain.Page;

public class PageResponse<T> {
  private int totalPage;
  
  private long totalElement;
  
  private int page;
  
  private int size;
  
  @JsonProperty("records")
  private List<T> result;
  
  public PageResponse() {}
  
  public PageResponse(Page<T> page) {
    this.result = page.getContent();
    this.totalElement = page.getTotalElements();
    this.totalPage = page.getTotalPages();
    this.page = page.getNumber() + 1;
    this.size = page.getSize();
  }
  
  public PageResponse(List<T> result, int totalPage, long totalElement, int page, int size) {
    this.totalPage = totalPage;
    this.totalElement = totalElement;
    this.page = page;
    this.size = size;
    this.result = result;
  }
  
  public int getTotalPage() {
    return this.totalPage;
  }
  
  public void setTotalPage(int totalPage) {
    this.totalPage = totalPage;
  }
  
  public long getTotalElement() {
    return this.totalElement;
  }
  
  public void setTotalElement(long totalElement) {
    this.totalElement = totalElement;
  }
  
  public int getPage() {
    return this.page;
  }
  
  public void setPage(int page) {
    this.page = page;
  }
  
  public int getSize() {
    return this.size;
  }
  
  public void setSize(int size) {
    this.size = size;
  }
  
  public List<T> getResult() {
    return this.result;
  }
  
  public void setResult(List<T> result) {
    this.result = result;
  }
}
