package com.chatbot.telegram.repositories.mongodb;

import com.chatbot.telegram.entites.mongo.LhtRechargeEntity;
import com.chatbot.telegram.mapper.LhtMapper;
import com.chatbot.telegram.models.response.LhtResponse;
import com.chatbot.telegram.repositories.mongodb.LhtRepositoryCustom;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.CriteriaDefinition;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.util.StringUtils;

public class LhtRepositoryCustomImpl implements LhtRepositoryCustom {
  private final MongoTemplate mongoTemplate;
  
  public LhtRepositoryCustomImpl(MongoTemplate mongoTemplate, ModelMapper modelMapper) {
    this.mongoTemplate = mongoTemplate;
    this.modelMapper = modelMapper;
  }
  
  private static final Logger logger = LoggerFactory.getLogger(com.chatbot.telegram.repositories.mongodb.LhtRepositoryCustomImpl.class);
  
  private final ModelMapper modelMapper;
  
  public Page<LhtResponse> searchLhtLog(String nickName, String code, String ip, String fromDate, String toDate, int page, int size) {
    Query query = new Query();
    if (!StringUtils.isEmpty(nickName))
      query.addCriteria((CriteriaDefinition)Criteria.where("nick_name").is(nickName)); 
    if (!StringUtils.isEmpty(ip))
      query.addCriteria((CriteriaDefinition)Criteria.where("ip").is(ip)); 
    if (!StringUtils.isEmpty(code))
      query.addCriteria((CriteriaDefinition)Criteria.where("code").is(code)); 
    Criteria whereCriteria = null;
    if (!StringUtils.isEmpty(fromDate))
      if (whereCriteria == null) {
        whereCriteria = Criteria.where("create_time").gte(fromDate);
      } else {
        whereCriteria.gte(fromDate);
      }  
    if (!StringUtils.isEmpty(toDate))
      if (whereCriteria == null) {
        whereCriteria = Criteria.where("create_time").lte(toDate);
      } else {
        whereCriteria.lte(toDate);
      }  
    if (whereCriteria != null)
      query.addCriteria((CriteriaDefinition)whereCriteria); 
    logger.info(query.toString());
    long count = this.mongoTemplate.count(query, LhtRechargeEntity.class);
    if (count == 0L)
      return (Page<LhtResponse>)new PageImpl(new ArrayList()); 
    query.with(Sort.by(Sort.Direction.DESC, new String[] { "create_time" }));
    PageRequest pageRequest = PageRequest.of(page - 1, size);
    query.with((Pageable)pageRequest);
    List<LhtRechargeEntity> list = this.mongoTemplate.find(query, LhtRechargeEntity.class);
    List<LhtResponse> listResponse = (List<LhtResponse>)list.stream().map(item -> (LhtResponse)this.modelMapper.map(item, LhtResponse.class, LhtMapper.class.getName())).collect(Collectors.toList());
    return (Page<LhtResponse>)new PageImpl(listResponse, (Pageable)pageRequest, count);
  }
}
