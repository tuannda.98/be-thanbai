package com.chatbot.telegram.controllers;

import com.chatbot.telegram.models.response.BoboResponse;
import com.chatbot.telegram.models.response.PageResponse;
import com.chatbot.telegram.services.define.BoboRechargeService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/bobo"})
public class BoboRechargeController {
  private final BoboRechargeService boboRechargeService;
  
  public BoboRechargeController(BoboRechargeService boboRechargeService) {
    this.boboRechargeService = boboRechargeService;
  }
  
  @GetMapping({"/search"})
  ResponseEntity<?> searchBobo(@RequestParam(name = "nick_name", required = false) String nickName, @RequestParam(name = "ip", required = false) String ip, @RequestParam(name = "serial", required = false) String serial, @RequestParam(name = "telco", required = false) String telco, @RequestParam(name = "from_date", required = false) String fromDate, @RequestParam(name = "to_date", required = false) String toDate, @RequestParam(name = "page", defaultValue = "1") int page, @RequestParam(name = "size", defaultValue = "50") int size) {
    PageResponse<BoboResponse> pageResult = this.boboRechargeService.searchBobo(nickName, ip, serial, telco, fromDate, toDate, page, size);
    return ResponseEntity.ok(pageResult);
  }
}
