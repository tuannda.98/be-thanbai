package com.chatbot.telegram.entites.mongo;

import javax.persistence.Entity;
import javax.persistence.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Entity
@Document(collection = "dvt_recharge_by_card_bobo_result")
public class BoboRechargeEntity {
  @Id
  private String id;
  
  @Field(name = "username")
  private String userName;
  
  private String password;
  
  @Field(name = "request_id")
  private String requestId;
  
  @Field(name = "nick_name")
  private String nickName;
  
  private String ip;
  
  private String serial;
  
  private String status;
  
  private String amount;
  
  private String telco;
  
  @Field(name = "error_message")
  private String errorMessage;
  
  private String signature;
  
  @Field(name = "signature_gen")
  private String signatureGen;
  
  @Field(name = "create_time")
  private String createTime;
  
  public BoboRechargeEntity(String id, String userName, String password, String requestId, String nickName, String ip, String serial, String status, String amount, String telco, String errorMessage, String signature, String signatureGen, String createTime) {
    this.id = id;
    this.userName = userName;
    this.password = password;
    this.requestId = requestId;
    this.nickName = nickName;
    this.ip = ip;
    this.serial = serial;
    this.status = status;
    this.amount = amount;
    this.telco = telco;
    this.errorMessage = errorMessage;
    this.signature = signature;
    this.signatureGen = signatureGen;
    this.createTime = createTime;
  }
  
  public BoboRechargeEntity() {}
  
  public String getId() {
    return this.id;
  }
  
  public void setId(String id) {
    this.id = id;
  }
  
  public String getUserName() {
    return this.userName;
  }
  
  public void setUserName(String userName) {
    this.userName = userName;
  }
  
  public String getPassword() {
    return this.password;
  }
  
  public void setPassword(String password) {
    this.password = password;
  }
  
  public String getRequestId() {
    return this.requestId;
  }
  
  public void setRequestId(String requestId) {
    this.requestId = requestId;
  }
  
  public String getNickName() {
    return this.nickName;
  }
  
  public void setNickName(String nickName) {
    this.nickName = nickName;
  }
  
  public String getIp() {
    return this.ip;
  }
  
  public void setIp(String ip) {
    this.ip = ip;
  }
  
  public String getSerial() {
    return this.serial;
  }
  
  public void setSerial(String serial) {
    this.serial = serial;
  }
  
  public String getStatus() {
    return this.status;
  }
  
  public void setStatus(String status) {
    this.status = status;
  }
  
  public String getAmount() {
    return this.amount;
  }
  
  public void setAmount(String amount) {
    this.amount = amount;
  }
  
  public String getTelco() {
    return this.telco;
  }
  
  public void setTelco(String telco) {
    this.telco = telco;
  }
  
  public String getErrorMessage() {
    return this.errorMessage;
  }
  
  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }
  
  public String getSignature() {
    return this.signature;
  }
  
  public void setSignature(String signature) {
    this.signature = signature;
  }
  
  public String getSignatureGen() {
    return this.signatureGen;
  }
  
  public void setSignatureGen(String signatureGen) {
    this.signatureGen = signatureGen;
  }
  
  public String getCreateTime() {
    return this.createTime;
  }
  
  public void setCreateTime(String createTime) {
    this.createTime = createTime;
  }
}
