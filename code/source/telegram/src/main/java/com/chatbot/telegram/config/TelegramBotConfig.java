package com.chatbot.telegram.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TelegramBotConfig {
  @Value("${telegram.bot.otp.token}")
  private String otpToken;
  
  @Value("${telegram.bot.otp.username}")
  private String optUsername;
  
  public String getOtpToken() {
    return this.otpToken;
  }
  
  public void setOtpToken(String otpToken) {
    this.otpToken = otpToken;
  }
  
  public String getOptUsername() {
    return this.optUsername;
  }
  
  public void setOptUsername(String optUsername) {
    this.optUsername = optUsername;
  }
}
