package com.chatbot.telegram.repositories.mongodb;

import com.chatbot.telegram.entites.mongo.BoboRechargeEntity;
import com.chatbot.telegram.mapper.BoboMapper;
import com.chatbot.telegram.models.response.BoboResponse;
import com.chatbot.telegram.repositories.mongodb.BoboRepositoryCustom;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.CriteriaDefinition;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.util.StringUtils;

public class BoboRepositoryCustomImpl implements BoboRepositoryCustom {
  private static final Logger logger = LoggerFactory.getLogger(com.chatbot.telegram.repositories.mongodb.BoboRepositoryCustomImpl.class);
  
  private final MongoTemplate mongoTemplate;
  
  private final ModelMapper modelMapper;
  
  public BoboRepositoryCustomImpl(MongoTemplate mongoTemplate, ModelMapper modelMapper) {
    this.mongoTemplate = mongoTemplate;
    this.modelMapper = modelMapper;
  }
  
  public Page<BoboResponse> searchBobo(String nickName, String ip, String serial, String telco, String fromDate, String toDate, int page, int size) {
    Query query = new Query();
    if (!StringUtils.isEmpty(nickName))
      query.addCriteria((CriteriaDefinition)Criteria.where("nick_name").is(nickName)); 
    if (!StringUtils.isEmpty(ip))
      query.addCriteria((CriteriaDefinition)Criteria.where("ip").is(ip)); 
    if (!StringUtils.isEmpty(serial))
      query.addCriteria((CriteriaDefinition)Criteria.where("serial").is(serial)); 
    if (!StringUtils.isEmpty(telco))
      query.addCriteria((CriteriaDefinition)Criteria.where("telco").is(telco)); 
    Criteria whereCriteria = null;
    if (!StringUtils.isEmpty(fromDate))
      if (whereCriteria == null) {
        whereCriteria = Criteria.where("create_time").gte(fromDate);
      } else {
        whereCriteria.gte(fromDate);
      }  
    if (!StringUtils.isEmpty(toDate))
      if (whereCriteria == null) {
        whereCriteria = Criteria.where("create_time").lte(toDate);
      } else {
        whereCriteria.lte(toDate);
      }  
    if (whereCriteria != null)
      query.addCriteria((CriteriaDefinition)whereCriteria); 
    logger.info(query.toString());
    long count = this.mongoTemplate.count(query, BoboRechargeEntity.class);
    if (count == 0L)
      return (Page<BoboResponse>)new PageImpl(new ArrayList()); 
    query.with(Sort.by(Sort.Direction.DESC, new String[] { "create_time" }));
    PageRequest pageRequest = PageRequest.of(page - 1, size);
    query.with((Pageable)pageRequest);
    List<BoboRechargeEntity> list = this.mongoTemplate.find(query, BoboRechargeEntity.class);
    List<BoboResponse> listResponse = (List<BoboResponse>)list.stream().map(item -> (BoboResponse)this.modelMapper.map(item, BoboResponse.class, BoboMapper.class.getName())).collect(Collectors.toList());
    return (Page<BoboResponse>)new PageImpl(listResponse, (Pageable)pageRequest, count);
  }
}
