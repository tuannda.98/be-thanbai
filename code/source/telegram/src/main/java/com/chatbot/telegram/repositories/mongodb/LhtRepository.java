package com.chatbot.telegram.repositories.mongodb;

import com.chatbot.telegram.entites.mongo.LhtRechargeEntity;
import com.chatbot.telegram.repositories.mongodb.LhtRepositoryCustom;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface LhtRepository extends MongoRepository<LhtRechargeEntity, String>, LhtRepositoryCustom {}
