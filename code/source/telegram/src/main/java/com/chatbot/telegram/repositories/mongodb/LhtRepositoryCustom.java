package com.chatbot.telegram.repositories.mongodb;

import com.chatbot.telegram.models.response.LhtResponse;
import org.springframework.data.domain.Page;

public interface LhtRepositoryCustom {
  Page<LhtResponse> searchLhtLog(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, int paramInt1, int paramInt2);
}
