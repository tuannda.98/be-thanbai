package com.chatbot.telegram.repositories.mongodb;

import com.chatbot.telegram.entites.mongo.OtpEntity;
import org.springframework.data.domain.Page;

public interface OtpRepositoryCustom {
  Page<OtpEntity> searchLogSms(String paramString1, Long paramLong1, Long paramLong2, String paramString2, int paramInt1, int paramInt2);
  
  long countSmsSuccess(String paramString1, Long paramLong1, Long paramLong2, String paramString2);
}
