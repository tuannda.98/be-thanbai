// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao;

import com.vinplay.dal.entities.gamebai.TopGameBaiModel;
import com.vinplay.vbee.common.messages.gamebai.LogNoHuGameBaiMessage;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.response.LogMoneyUserResponse;
import com.vinplay.vbee.common.response.LogUserMoneyResponse;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface LogMoneyUserDao {

    List<LogUserMoneyResponse> getUserHistory(String nickName, String timeStart, String timeEnd);

    List<LogUserMoneyResponse> searchLogMoneyUser(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5, final String p6, final int p7, final int p8, final int p9);

    List<LogUserMoneyResponse> searchLogMoneyUserDes(final String nickName, final String userName, final String moneyType, final String serviceName, final String actionName, final String timeStart, final String timeEnd, final int page, final int like1, final int totalRecord, String descColumn);

    List<LogUserMoneyResponse> searchAllLogMoneyUser(final String p0, final String p1, final boolean p2);

    long getTotalBetWin(final String p0, final String p1, final String p2);

    LogUserMoneyResponse searchLastLogMoneyUser(final String p0, final String p1, final ArrayList<String> p2);

    int countsearchLogMoneyUser(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5, final int p6);

    int countsearchLogMoneyUser2(final String nickName, final String userName, final String moneyType, final String serviceName, final String actionName, final String timeStart, final String timeEnd, final int like1);

    List<LogMoneyUserResponse> getHistoryTransactionLogMoney(final String p0, final int p1, final int p2);

    List<LogMoneyUserResponse> getTransactionList(final String p0, final int p1, final int p2, final int p3);

    int countHistoryTransactionLogMoney(final String p0, final int p1);

    Map<String, TopGameBaiModel> getTopGameBai(final String p0);

    List<LogNoHuGameBaiMessage> getNoHuGameBaiHistory(final int p0, final String p1);

    int countNoHuGameBaiHistory();

    UserModel getUserByNickName(final String p0) throws SQLException;

    List<LogUserMoneyResponse> searchLogMoneyTranferUser(final String p0, final String p1, final String p2, final String p3, final int p4);

    boolean UpdateProcessLogChuyenTienDaiLy(final String p0, final String p1, final String p2, final String p3);

    boolean UpdateProcessLogChuyenTienDaiLyMySQL(final String p0, final String p1, final String p2, final String p3) throws SQLException;
}
