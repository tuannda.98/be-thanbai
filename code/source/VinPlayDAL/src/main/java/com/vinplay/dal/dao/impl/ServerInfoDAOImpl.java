// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import com.vinplay.dal.dao.ServerInfoDAO;
import com.vinplay.vbee.common.models.LogCCUModel;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import org.bson.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ServerInfoDAOImpl implements ServerInfoDAO {
    @Override
    public List<LogCCUModel> getLogCCU(final String startTime, final String endTime) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        FindIterable iterable = null;
        final BasicDBObject obj = new BasicDBObject();
        obj.put("$gte", startTime);
        obj.put("$lte", endTime);
        conditions.put("time_log", obj);
        final BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", 1);
        iterable = db.getCollection("log_ccu").find(new Document(conditions)).sort(objsort);
        final ArrayList<LogCCUModel> results = new ArrayList<>();
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final LogCCUModel entry = new LogCCUModel();
                entry.ccu = document.getInteger("ccu", 0);
                entry.web = document.getInteger("web", 0);
                entry.ad = document.getInteger("ad", 0);
                entry.ios = document.getInteger("ios", 0);
                entry.wp = document.getInteger("wp", 0);
                entry.fb = document.getInteger("fb", 0);
                entry.dt = document.getInteger("dt", 0);
                entry.ot = document.getInteger("ot", 0);
                entry.ts = document.getString("time_log");
                results.add(entry);
            }
        });
        return results;
    }
}
