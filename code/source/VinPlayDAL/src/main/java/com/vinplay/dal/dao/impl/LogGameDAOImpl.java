// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import com.vinplay.dal.dao.LogGameDAO;
import com.vinplay.vbee.common.messages.LogGameMessage;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import org.bson.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LogGameDAOImpl implements LogGameDAO {
    @Override
    public List<LogGameMessage> searchLogGameByNickName(final String sessionId, final String nickName, final String gameName, final String timeStart, final String timeEnd, final String moneyType, final int page) {
        final ArrayList<LogGameMessage> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        final BasicDBObject objsort = new BasicDBObject();
        final BasicDBObject obj = new BasicDBObject();
        final int num_start = (page - 1) * 50;
        final int num_end = 50;
        FindIterable iterable = null;
        objsort.put("_id", -1);
        if (nickName != null && !nickName.equals("")) {
            conditions.put("nick_name", nickName);
        }
        if (gameName != null && !gameName.equals("")) {
            conditions.put("game_name", gameName);
        }
        if (sessionId != null && !sessionId.equals("")) {
            conditions.put("session_id", sessionId);
        }
        if (moneyType != null && !moneyType.equals("")) {
            conditions.put("money_type", moneyType);
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("time_log", obj);
        }
        iterable = db.getCollection("log_game").find(new Document(conditions)).sort(objsort).skip(num_start).limit(50);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final LogGameMessage loggame = new LogGameMessage();
                loggame.sessionId = document.getString("session_id");
                loggame.gameName = document.getString("game_name");
                loggame.timeLog = document.getString("time_log");
                loggame.nickName = document.getString("nick_name");
                loggame.moneyType = document.getString("money_type");
                results.add(loggame);
            }
        });
        return results;
    }

    @Override
    public int countSearchLogGameByNickName(final String sessionId, final String nickName, final String gameName, final String timeStart, final String timeEnd, final String moneyType) {
        return 500;
    }

    @Override
    public LogGameMessage getLogGameDetailBySessionID(final String sessionId, final String gameName, final String timelog) {
        final LogGameMessage loggame = new LogGameMessage();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        conditions.put("session_id", sessionId);
        conditions.put("game_name", gameName);
        conditions.put("time_log", timelog);
        final FindIterable iterable = db.getCollection("log_game_detail").find(new Document(conditions));
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                loggame.sessionId = document.getString("session_id");
                loggame.gameName = document.getString("game_name");
                loggame.timeLog = document.getString("time_log");
                loggame.logDetail = document.getString("log_detail");
            }
        });
        return loggame;
    }
}
