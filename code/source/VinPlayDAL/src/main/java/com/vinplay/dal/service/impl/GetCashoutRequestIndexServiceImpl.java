package com.vinplay.dal.service.impl;

import com.vinplay.dal.dao.GetCashoutRequestIndexDAO;
import com.vinplay.dal.dao.impl.GetCashoutRequestIndexDAOImpl;
import com.vinplay.dal.service.GetCashoutRequestIndexService;
import org.json.JSONObject;

import java.sql.SQLException;

public class GetCashoutRequestIndexServiceImpl implements GetCashoutRequestIndexService {
    GetCashoutRequestIndexDAO dao = new GetCashoutRequestIndexDAOImpl();
    @Override
    public JSONObject getByDateWithClientPlatform(String timeStart, String timeEnd) throws SQLException {
        return dao.getByDateWithClientPlatform(timeStart, timeEnd);
    }
}
