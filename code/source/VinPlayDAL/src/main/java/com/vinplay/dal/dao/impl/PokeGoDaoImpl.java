// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import com.vinplay.dal.dao.PokeGoDAO;
import com.vinplay.vbee.common.models.minigame.pokego.LSGDPokeGo;
import com.vinplay.vbee.common.models.minigame.pokego.TopPokeGo;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class PokeGoDaoImpl implements PokeGoDAO {
    @Override
    public List<TopPokeGo> getTopPokeGo(final int moneyType, final int pageNumber) {
        final int pageSize = 10;
        final int skipNumber = (pageNumber - 1) * 10;
        final ArrayList<TopPokeGo> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        FindIterable iterable = null;
        final Document conditions = new Document();
        conditions.put("$or", Arrays.asList(new Document("result", 3), new Document("result", 4)));
        conditions.put("money_type", moneyType);
        final BasicDBObject sortCondtions = new BasicDBObject();
        sortCondtions.put("_id", -1);
        iterable = db.getCollection("log_poke_go").find(conditions).sort(sortCondtions).skip(skipNumber).limit(10);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final TopPokeGo entry = new TopPokeGo();
                entry.un = document.getString("user_name");
                entry.bv = document.getLong("bet_value");
                entry.pz = document.getLong("prize");
                entry.ts = document.getString("time_log");
                results.add(entry);
            }
        });
        return results;
    }

    @Override
    public int countLSGD(final String username, final int moneyType) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final Document conditions = new Document();
        conditions.put("user_name", username);
        conditions.put("money_type", moneyType);
        final long totalRows = db.getCollection("log_poke_go").count(conditions);
        return (int) totalRows;
    }

    @Override
    public List<LSGDPokeGo> getLSGD(final String username, final int moneyType, final int pageNumber) {
        final int pageSize = 10;
        final int skipNumber = (pageNumber - 1) * 10;
        final ArrayList<LSGDPokeGo> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        FindIterable iterable = null;
        final Document conditions = new Document();
        conditions.put("user_name", username);
        conditions.put("money_type", moneyType);
        iterable = db.getCollection("log_poke_go").find(conditions).skip(skipNumber).limit(10).sort(new Document("_id",-1));
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final LSGDPokeGo entry = new LSGDPokeGo();
                entry.rf = document.getLong("reference_id");
                entry.un = document.getString("user_name");
                entry.bv = document.getLong("bet_value");
                entry.pz = document.getLong("prize");
                entry.lb = document.getString("lines_betting");
                entry.lw = document.getString("lines_win");
                entry.ps = document.getString("prizes_on_line");
                entry.ts = document.getString("time_log");
                results.add(entry);
            }
        });
        return results;
    }

    @Override
    public long getLastRefenceId() {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap conditions = new HashMap();
        final BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        final FindIterable iterable = db.getCollection("log_poke_go").find(new Document(conditions)).sort(objsort).limit(1);
        final Document document = (iterable != null) ? ((Document) iterable.first()) : null;
        return (document == null) ? 0L : document.getLong("reference_id");
    }

    @Override
    public List<TopPokeGo> getTop(final int moneyType, final int number) {
        final ArrayList<TopPokeGo> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        FindIterable iterable = null;
        final Document conditions = new Document();
        conditions.put("$or", Arrays.asList(new Document("result", 3), new Document("result", 4)));
        conditions.put("money_type", moneyType);
        final BasicDBObject sortCondtions = new BasicDBObject();
        sortCondtions.put("_id", -1);
        iterable = db.getCollection("log_poke_go").find(conditions).sort(sortCondtions).limit(number);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final TopPokeGo entry = new TopPokeGo();
                entry.un = document.getString("user_name");
                entry.bv = document.getLong("bet_value");
                entry.pz = document.getLong("prize");
                entry.ts = document.getString("time_log");
                results.add(entry);
            }
        });
        return results;
    }
}
