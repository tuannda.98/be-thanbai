// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service;

import com.vinplay.vbee.common.response.MoneyTotalRechargeByCardReponse;
import com.vinplay.vbee.common.response.RechargeBySmsResponse;

import java.util.List;

public interface RechargeBySmsPlus9029Service {
    List<MoneyTotalRechargeByCardReponse> moneyTotalRechargeBySmsPlus9029(final String p0, final String p1, final String p2);

    List<RechargeBySmsResponse> exportDataRechargeBySmsPlus(final String p0, final String p1, final String p2, final String p3);
}
