// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import com.vinplay.dal.dao.ListMoneyTranferDAO;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.response.TranferMoneyResponse;
import com.vinplay.vbee.common.utils.VinPlayUtils;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class ListMoneyTranferDAOImpl implements ListMoneyTranferDAO {
    @Override
    public int countTotalRecord(final String nickName, final int isFreezeMoney, final int page, final int timeSearch) {
        int results = 0;
        if (nickName != null && !nickName.isEmpty()) {
            results = ("all".equals(nickName) ? this.countAllMoneyTranfer(isFreezeMoney, page, timeSearch) : this.countMoneyTranferByNickName(nickName, isFreezeMoney, page, timeSearch));
        }
        return results;
    }

    private int countAllMoneyTranfer(final int isFreezeMoney, final int page, final int timeSearch) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final BasicDBObject objsort = new BasicDBObject();
        final Document conditions = new Document();
        objsort.put("_id", -1);
        final BasicDBObject obj = new BasicDBObject();
        obj.put("$exists", true);
        obj.put("$ne", "");
        conditions.put("agent_level1", obj);
        if (isFreezeMoney != -1) {
            conditions.put("is_freeze_money", isFreezeMoney);
        }
        conditions.put("trans_time", new BasicDBObject("$gte", VinPlayUtils.parseDateTimeToString(VinPlayUtils.subtractDay(new Date(), timeSearch))));
        return (int) db.getCollection("log_chuyen_tien_dai_ly").count(new Document(conditions));
    }

    private int countMoneyTranferByNickName(final String nickName, final int isFreezeMoney, final int page, final int timeSearch) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final BasicDBObject objsort = new BasicDBObject();
        final Document conditions = new Document();
        objsort.put("_id", -1);
        conditions.put("agent_level1", nickName);
        if (isFreezeMoney != -1) {
            conditions.put("is_freeze_money", isFreezeMoney);
        }
        conditions.put("trans_time", new BasicDBObject("$gte", VinPlayUtils.parseDateTimeToString(VinPlayUtils.subtractDay(new Date(), timeSearch))));
        return (int) db.getCollection("log_chuyen_tien_dai_ly").count(new Document(conditions));
    }

    @Override
    public List<TranferMoneyResponse> listMoneyTranfer(final String nickName, final int isFreezeMoney, final int page, final int timeSearch) {
        List<TranferMoneyResponse> results = new ArrayList<>();
        if (nickName != null && !nickName.isEmpty()) {
            results = ("all".equals(nickName) ? this.listAllMoneyTranfer(isFreezeMoney, page, timeSearch) : this.listMoneyTranferByNickName(nickName, isFreezeMoney, page, timeSearch));
        }
        return results;
    }

    private List<TranferMoneyResponse> listAllMoneyTranfer(final int isFreezeMoney, final int page, final int timeSearch) {
        final ArrayList<TranferMoneyResponse> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        FindIterable iterable = null;
        final BasicDBObject objsort = new BasicDBObject();
        final Document conditions = new Document();
        objsort.put("_id", -1);
        final int num_start = (page - 1) * 50;
        final int num_end = 50;
        final BasicDBObject obj = new BasicDBObject();
        obj.put("$exists", true);
        obj.put("$ne", "");
        conditions.put("agent_level1", obj);
        if (isFreezeMoney != -1) {
            conditions.put("is_freeze_money", isFreezeMoney);
        }
        conditions.put("trans_time", new BasicDBObject("$gte", VinPlayUtils.parseDateTimeToString(VinPlayUtils.subtractDay(new Date(), timeSearch))));
        iterable = db.getCollection("log_chuyen_tien_dai_ly").find(new Document(conditions)).sort(objsort).skip(num_start).limit(50);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final TranferMoneyResponse message = new TranferMoneyResponse(document.getString("nick_name_send"), document.getString("nick_name_receive"), document.getLong("money_send"), document.getLong("money_receive"), document.getLong("fee"), document.getInteger("status"), document.getString("trans_time"), document.getString("des_send"), document.getString("des_receive"), document.getString("transaction_no"), document.getInteger("is_freeze_money"), document.getString("agent_level1"));
                results.add(message);
            }
        });
        return results;
    }

    private List<TranferMoneyResponse> listMoneyTranferByNickName(final String nickName, final int isFreezeMoney, final int page, final int timeSearch) {
        final ArrayList<TranferMoneyResponse> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        FindIterable iterable = null;
        final BasicDBObject objsort = new BasicDBObject();
        final Document conditions = new Document();
        objsort.put("_id", -1);
        final int num_start = (page - 1) * 50;
        final int num_end = 50;
        conditions.put("agent_level1", nickName);
        if (isFreezeMoney != -1) {
            conditions.put("is_freeze_money", isFreezeMoney);
        }
        conditions.put("trans_time", new BasicDBObject("$gte", VinPlayUtils.parseDateTimeToString(VinPlayUtils.subtractDay(new Date(), timeSearch))));
        iterable = db.getCollection("log_chuyen_tien_dai_ly").find(new Document(conditions)).sort(objsort).skip(num_start).limit(50);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final TranferMoneyResponse message = new TranferMoneyResponse(document.getString("nick_name_send"), document.getString("nick_name_receive"), document.getLong("money_send"), document.getLong("money_receive"), document.getLong("fee"), document.getInteger("status"), document.getString("trans_time"), document.getString("des_send"), document.getString("des_receive"), document.getString("transaction_no"), document.getInteger("is_freeze_money"), document.getString("agent_level1"));
                results.add(message);
            }
        });
        return results;
    }

    @Override
    public TranferMoneyResponse getMoneyTranferByTransNo(final String transNo) {
        final ArrayList results = new ArrayList();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        FindIterable iterable = null;
        final Document conditions = new Document();
        if (transNo != null && !transNo.isEmpty()) {
            conditions.put("transaction_no", transNo);
        }
        iterable = db.getCollection("log_chuyen_tien_dai_ly").find(new Document(conditions));
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final TranferMoneyResponse message = new TranferMoneyResponse(document.getString("nick_name_send"), document.getString("nick_name_receive"), document.getLong("money_send"), document.getLong("money_receive"), document.getLong("fee"), document.getInteger("status"), document.getString("trans_time"), document.getString("des_send"), document.getString("des_receive"), document.getString("transaction_no"), document.getInteger("is_freeze_money"), document.getString("agent_level1"));
                results.add(message);
            }
        });
        if (results.size() > 0) {
            return (TranferMoneyResponse) results.get(0);
        }
        return null;
    }
}
