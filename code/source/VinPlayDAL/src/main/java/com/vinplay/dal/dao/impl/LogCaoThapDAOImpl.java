// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import com.vinplay.dal.dao.LogCaoThapDAO;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.response.LogCaoThapResponse;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LogCaoThapDAOImpl implements LogCaoThapDAO {
    @Override
    public List<LogCaoThapResponse> listCaoThap(final String nickName, final String transId, final String bet_value, final String timeStart, final String timeEnd, final String moneyType, final int page) {
        final ArrayList<LogCaoThapResponse> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        FindIterable iterable = null;
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        final int num_start = (page - 1) * 50;
        final int num_end = 50;
        objsort.put("_id", -1);
        if (nickName != null && !nickName.equals("")) {
            final String pattern = ".*" + nickName + ".*";
            conditions.put("nick_name", new BasicDBObject().append("$regex", pattern).append("$options", "i"));
        }
        if (transId != null && !transId.equals("")) {
            conditions.put("trans_id", Long.parseLong(transId));
        }
        if (bet_value != null && !bet_value.equals("")) {
            conditions.put("bet_value", Long.parseLong(bet_value));
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("time_log", obj);
        }
        if (moneyType != null && !moneyType.equals("")) {
            conditions.put("money_type", Integer.parseInt(moneyType));
        }
        iterable = db.getCollection("log_cao_thap").find(new Document(conditions)).sort(objsort).skip(num_start).limit(50);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final LogCaoThapResponse caothap = new LogCaoThapResponse();
                caothap.tran_id = document.getLong("trans_id");
                caothap.nickName = document.getString("nick_name");
                caothap.pot_bet = document.getInteger("pot_bet");
                caothap.step = document.getInteger("step");
                caothap.bet_value = document.getLong("bet_value");
                caothap.result = document.getInteger("result");
                caothap.prize = document.getLong("prize");
                caothap.cards = document.getString("cards");
                caothap.current_pot = document.getLong("current_pot");
                caothap.current_fund = document.getLong("current_fund");
                caothap.money_type = document.getInteger("money_type");
                caothap.time_log = document.getString("time_log");
                results.add(caothap);
            }
        });
        return results;
    }

    @Override
    public int countCaoThap(final String nickName, final String transId, final String bet_value, final String timeStart, final String timeEnd, final String moneyType) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final BasicDBObject obj = new BasicDBObject();
        final HashMap<String, Object> conditions = new HashMap<>();
        if (nickName != null && !nickName.equals("")) {
            final String pattern = ".*" + nickName + ".*";
            conditions.put("nick_name", new BasicDBObject().append("$regex", pattern).append("$options", "i"));
        }
        if (transId != null && !transId.equals("")) {
            conditions.put("trans_id", Long.parseLong(transId));
        }
        if (bet_value != null && !bet_value.equals("")) {
            conditions.put("bet_value", Long.parseLong(bet_value));
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            obj.put("$gte", timeStart + " 00:00:00");
            obj.put("$lte", timeEnd + " 23:59:59");
            conditions.put("time_log", obj);
        }
        if (moneyType != null && !moneyType.equals("")) {
            conditions.put("money_type", Integer.parseInt(moneyType));
        }
        return (int) db.getCollection("log_cao_thap").count(new Document(conditions));
    }
}
