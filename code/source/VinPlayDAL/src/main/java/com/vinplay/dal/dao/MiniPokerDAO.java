// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao;

import com.vinplay.dal.entities.minipoker.LSGDMiniPoker;
import com.vinplay.dal.entities.minipoker.VinhDanhMiniPoker;

import java.util.List;

public interface MiniPokerDAO {
    int countLichSuGiaoDich(final String p0, final int p1);

    List<LSGDMiniPoker> getLichSuGiaoDich(final String p0, final int p1, final int p2);

    List<VinhDanhMiniPoker> getBangVinhDanh(final int p0, final int p1);
}
