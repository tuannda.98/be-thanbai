// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.vinplay.dal.service.BroadcastMessageService;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.models.BroadcastMsgEntry;

import java.util.ArrayList;
import java.util.List;

public class BroadcastMessageServiceImpl implements BroadcastMessageService {
    private static int MAX_SIZE;
    public static int MIN_MONEY;
    private static final String KEY_BROADCAST = "keyBroadcast";

    static {
        BroadcastMessageServiceImpl.MAX_SIZE = 20;
        BroadcastMessageServiceImpl.MIN_MONEY = 10000;
    }

    public BroadcastMessageServiceImpl() {
        super();
    }

    @Override
    public void putMessage(final int gameId, final String nickname, final long money) {
        if (money >= BroadcastMessageServiceImpl.MIN_MONEY) {
            final BroadcastMsgEntry newEntry = new BroadcastMsgEntry(gameId, nickname, money);
            final HazelcastInstance client = HazelcastClientFactory.getInstance();
            final IMap cachedBroadcasts = client.getMap("cacheBroadcast");
            if (cachedBroadcasts != null && cachedBroadcasts.containsKey(KEY_BROADCAST)) {
                cachedBroadcasts.lock(KEY_BROADCAST);
                try {
                    final List<BroadcastMsgEntry> entries = (List) cachedBroadcasts.get(KEY_BROADCAST);
                    if (entries.size() < BroadcastMessageServiceImpl.MAX_SIZE) {
                        this.add(entries, newEntry);
                    } else {
                        final BroadcastMsgEntry minEntry;
                        if (entries.size() == BroadcastMessageServiceImpl.MAX_SIZE && (minEntry = entries.get(entries.size() - 1)).getM() < money) {
                            entries.remove(entries.size() - 1);
                            this.add(entries, newEntry);
                        }
                    }
                    cachedBroadcasts.put(KEY_BROADCAST, entries);
                } catch (Exception ex) {
                    ex.printStackTrace();
                } finally {
                    cachedBroadcasts.unlock(KEY_BROADCAST);
                }
            } else {
                final ArrayList<BroadcastMsgEntry> entries2 = new ArrayList<>();
                entries2.add(newEntry);
                cachedBroadcasts.put(KEY_BROADCAST, new ArrayList(entries2));
            }
        }
    }

    private void add(final List<BroadcastMsgEntry> entries, final BroadcastMsgEntry newEntry) {
        int index = -1;
        for (int i = 0; i < entries.size(); ++i) {
            final BroadcastMsgEntry entry = entries.get(i);
            if (entry.getM() < newEntry.getM()) {
                index = i;
                break;
            }
        }
        if (index > -1) {
            entries.add(index, newEntry);
        } else {
            entries.add(newEntry);
        }
    }

    @Override
    public String toJson() {
        try {
            final HazelcastInstance client = HazelcastClientFactory.getInstance();
            final IMap cachedBroadcasts = client.getMap("cacheBroadcast");
            final List entries = (List) cachedBroadcasts.get(KEY_BROADCAST);
            final BroadcastMessageServiceImpl this$0 = new BroadcastMessageServiceImpl();
            this$0.getClass();
            this$0.getClass();
            final BroadcastMsgModel model = this$0.new BroadcastMsgModel();
            model.setEntries(entries);
            final ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(model);
        } catch (JsonProcessingException e) {
            return "{\"success\":false,\"errorCode\":\"1001\"}";
        }
    }

    @Override
    public void clearMessage() {
        final HazelcastInstance client = HazelcastClientFactory.getInstance();
        final IMap cachedBroadcasts = client.getMap("cacheBroadcast");
        if (cachedBroadcasts != null && cachedBroadcasts.containsKey(KEY_BROADCAST)) {
            cachedBroadcasts.lock(KEY_BROADCAST);
            try {
                final List entries = (List) cachedBroadcasts.get(KEY_BROADCAST);
                entries.clear();
                cachedBroadcasts.put(KEY_BROADCAST, entries);
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                cachedBroadcasts.unlock(KEY_BROADCAST);
            }
        }
    }

    public class BroadcastMsgModel {
        private List<BroadcastMsgEntry> entries;

        public BroadcastMsgModel() {
            this.entries = new ArrayList<>();
        }

        public List<BroadcastMsgEntry> getEntries() {
            return this.entries;
        }

        public void setEntries(final List<BroadcastMsgEntry> entries) {
            this.entries = entries;
        }
    }
}
