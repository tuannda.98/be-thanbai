// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import com.vinplay.dal.dao.CashOutByBankDAO;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.response.CashOutByBankReponse;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CashOutByBankDAOImpl implements CashOutByBankDAO {
    private long totalMoney;

    public CashOutByBankDAOImpl() {
        this.totalMoney = 0L;
    }

    @Override
    public List<CashOutByBankReponse> searchCashOutByBank(final String nickName, final String bank, final String status, final String code, final String timeStart, final String timeEnd, final int page, final String transId) {
        final ArrayList<CashOutByBankReponse> result = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        FindIterable iterable = null;
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        final Document conditions = new Document();
        objsort.put("_id", -1);
        final int num_start = (page - 1) * 50;
        final int num_end = 50;
        if (transId != null && !transId.equals("")) {
            conditions.put("reference_id", transId);
        }
        if (nickName != null && !nickName.equals("")) {
            conditions.put("nick_name", nickName);
        }
        if (bank != null && !bank.equals("")) {
            conditions.put("bank", bank);
        }
        if (status != null && !status.equals("")) {
            conditions.put("status", Integer.parseInt(status));
        }
        if (code != null && !code.equals("")) {
            conditions.put("code", Integer.parseInt(code));
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("time_log", obj);
        }
        iterable = db.getCollection("dvt_cash_out_by_bank").find(new Document(conditions)).sort(objsort).skip(num_start).limit(50);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final CashOutByBankReponse bank = new CashOutByBankReponse();
                bank.refenrenceId = document.getString("reference_id");
                bank.nickName = document.getString("nick_name");
                bank.bank = document.getString("bank");
                bank.account = document.getString("account");
                bank.name = document.getString("name");
                bank.amount = document.getInteger("amount");
                bank.description = document.getString("description");
                bank.status = document.getInteger("status");
                bank.message = document.getString("message");
                bank.sign = document.getString("sign");
                bank.code = document.getInteger("code");
                bank.description = document.getString("description");
                bank.timeLog = document.getString("time_log");
                bank.updateTime = document.getString("update_time");
                result.add(bank);
            }
        });
        return result;
    }

    @Override
    public int countSearchCashOutByBank(final String nickName, final String bank, final String status, final String code, final String timeStart, final String timeEnd, final String transId) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        final Document conditions = new Document();
        objsort.put("_id", -1);
        if (transId != null && !transId.equals("")) {
            conditions.put("reference_id", transId);
        }
        if (nickName != null && !nickName.equals("")) {
            conditions.put("nick_name", nickName);
        }
        if (bank != null && !bank.equals("")) {
            conditions.put("bank", bank);
        }
        if (status != null && !status.equals("")) {
            conditions.put("status", Integer.parseInt(status));
        }
        if (code != null && !code.equals("")) {
            conditions.put("code", Integer.parseInt(code));
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("time_log", obj);
        }
        return (int) db.getCollection("dvt_cash_out_by_bank").count(new Document(conditions));
    }

    @Override
    public long moneyTotal(final String nickName, final String bank, final String status, final String code, final String timeStart, final String timeEnd, final String transId) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        final Document conditions = new Document();
        objsort.put("_id", -1);
        if (transId != null && !transId.equals("")) {
            conditions.put("reference_id", transId);
        }
        if (nickName != null && !nickName.equals("")) {
            conditions.put("nick_name", nickName);
        }
        if (bank != null && !bank.equals("")) {
            conditions.put("bank", bank);
        }
        if (status != null && !status.equals("")) {
            conditions.put("status", Integer.parseInt(status));
        }
        if (code != null && !code.equals("")) {
            conditions.put("code", Integer.parseInt(code));
        } else {
            final BasicDBObject query1 = new BasicDBObject("code", Integer.parseInt("34"));
            final BasicDBObject query2 = new BasicDBObject("code", Integer.parseInt("0"));
            final ArrayList<BasicDBObject> myList = new ArrayList<>();
            myList.add(query1);
            myList.add(query2);
            conditions.put("$or", myList);
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("time_log", obj);
        }
        FindIterable iterable = null;
        iterable = db.getCollection("dvt_cash_out_by_bank").find(new Document(conditions)).sort(objsort);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final CashOutByBankDAOImpl cashOutByBankDAOImpl = CashOutByBankDAOImpl.this;
                cashOutByBankDAOImpl.totalMoney += document.getInteger("amount");
            }
        });
        return this.totalMoney;
    }
}
