// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.vinplay.dal.dao.impl.CashOutByCardDAOImpl;
import com.vinplay.dal.service.CashOutByCardService;
import com.vinplay.vbee.common.response.CashOutByCardResponse;
import com.vinplay.vbee.common.response.MoneyTotalRechargeByCardReponse;

import java.util.List;

public class CashOutByCardServiceImpl implements CashOutByCardService {
    @Override
    public List<CashOutByCardResponse> searchCashOutByCard(final String nickName, final String provider, final String status, final String code, final String timeStart, final String timeEnd, final int page, final String transId, final String partner) {
        final CashOutByCardDAOImpl dao = new CashOutByCardDAOImpl();
        return dao.searchCashOutByCard(nickName, provider, status, code, timeStart, timeEnd, page, transId, partner);
    }

    @Override
    public int countSearchCashOutByCard(final String nickName, final String provider, final String status, final String code, final String timeStart, final String timeEnd, final String transId, final String partner) {
        final CashOutByCardDAOImpl dao = new CashOutByCardDAOImpl();
        return dao.countSearchCashOutByCard(nickName, provider, status, code, timeStart, timeEnd, transId, partner);
    }

    @Override
    public long moneyTotal(final String nickName, final String provider, final String status, final String code, final String timeStart, final String timeEnd, final String transId, final String partner) {
        final CashOutByCardDAOImpl dao = new CashOutByCardDAOImpl();
        return dao.moneyTotal(nickName, provider, status, code, timeStart, timeEnd, transId, partner);
    }

    @Override
    public List<MoneyTotalRechargeByCardReponse> moneyTotalCashOutByCard(final String timeStart, final String timeEnd, final String partner, final String code) {
        final CashOutByCardDAOImpl dao = new CashOutByCardDAOImpl();
        return dao.moneyTotalCashOutByCard(timeStart, timeEnd, partner, code);
    }

    @Override
    public List<CashOutByCardResponse> exportDataCashOutByCard(final String provider, final String code, final String timeStart, final String timeEnd, final String partner, final String amount) {
        final CashOutByCardDAOImpl dao = new CashOutByCardDAOImpl();
        return dao.exportDataCashOutByCard(provider, code, timeStart, timeEnd, partner, amount);
    }
}
