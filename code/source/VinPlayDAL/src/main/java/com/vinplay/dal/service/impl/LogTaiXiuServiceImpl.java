// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.vinplay.dal.dao.impl.LogTaiXiuDAOImpl;
import com.vinplay.dal.service.LogTaiXiuService;
import com.vinplay.vbee.common.response.TaiXiuDetailReponse;
import com.vinplay.vbee.common.response.TaiXiuResponse;
import com.vinplay.vbee.common.response.TaiXiuResultResponse;

import java.sql.SQLException;
import java.util.List;

public class LogTaiXiuServiceImpl implements LogTaiXiuService {
    @Override
    public List<TaiXiuResponse> listLogTaiXiu(final String referentId, final String userName, final String betSide, final String moneyType, final String timeStart, final String timeEnd, final int page) throws SQLException {
        final LogTaiXiuDAOImpl dao = new LogTaiXiuDAOImpl();
        return dao.listLogTaiXiu(referentId, userName, betSide, moneyType, timeStart, timeEnd, page);
    }

    @Override
    public int countLogTaiXiu(final String referentId, final String userName, final String betSide, final String moneyType, final String timeStart, final String timeEnd) throws SQLException {
        final LogTaiXiuDAOImpl dao = new LogTaiXiuDAOImpl();
        return dao.countLogTaiXiu(referentId, userName, betSide, moneyType, timeStart, timeEnd);
    }

    @Override
    public List<TaiXiuDetailReponse> getLogTaiXiuDetail(final String referent_id, final String betSide, final String moneyType, final String nickName, final int page) throws SQLException {
        final LogTaiXiuDAOImpl dao = new LogTaiXiuDAOImpl();
        return dao.getLogTaiXiuDetail(referent_id, betSide, moneyType, nickName, page);
    }

    @Override
    public int countLogTaiXiuDetail(final String referent_id, final String betSide, final String moneyType, final String nickName) throws SQLException {
        final LogTaiXiuDAOImpl dao = new LogTaiXiuDAOImpl();
        return dao.countLogTaiXiuDetail(referent_id, betSide, moneyType, nickName);
    }

    @Override
    public List<TaiXiuResultResponse> listLogTaiXiuResult(final String referentId, final String moneyType, final String timeStart, final String timeEnd, final int page) throws SQLException {
        final LogTaiXiuDAOImpl dao = new LogTaiXiuDAOImpl();
        return dao.listLogTaiXiuResult(referentId, moneyType, timeStart, timeEnd, page);
    }

    @Override
    public int countLogTaiXiuResult(final String referentId, final String moneyType, final String timeStart, final String timeEnd) throws SQLException {
        final LogTaiXiuDAOImpl dao = new LogTaiXiuDAOImpl();
        return dao.countLogTaiXiuResult(referentId, moneyType, timeStart, timeEnd);
    }
}
