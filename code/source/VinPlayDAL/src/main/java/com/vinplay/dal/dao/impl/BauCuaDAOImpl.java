// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import com.vinplay.dal.dao.BauCuaDAO;
import com.vinplay.vbee.common.models.minigame.TopWin;
import com.vinplay.vbee.common.models.minigame.baucua.ResultBauCua;
import com.vinplay.vbee.common.models.minigame.baucua.ToiChonCa;
import com.vinplay.vbee.common.models.minigame.baucua.TransactionBauCua;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.utils.CommonUtils;
import com.vinplay.vbee.common.utils.DateTimeUtils;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class BauCuaDAOImpl implements BauCuaDAO {
    @Override
    public List<TransactionBauCua> getLSGDBauCua(final String username, final int page, final byte moneyType) {
        final int pageSize = 10;
        final int skipNumber = (page - 1) * 10;
        final ArrayList<TransactionBauCua> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        FindIterable iterable = null;
        final Document conditions = new Document();
        conditions.put("user_name", username);
        conditions.put("money_type", moneyType);
        final BasicDBObject sortCondtions = new BasicDBObject();
        sortCondtions.put("_id", -1);
        iterable = db.getCollection("bau_cua_transaction").find(conditions).sort(sortCondtions).skip(skipNumber).limit(10);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final TransactionBauCua entry = new TransactionBauCua();
                entry.username = document.getString("user_name");
                entry.room = document.getInteger("room");
                entry.referenceId = document.getLong("reference_id");
                entry.dices = document.getString("dices");
                entry.betValues = CommonUtils.stringToLongArr(document.getString("bet_value"));
                entry.prizes = CommonUtils.stringToLongArr(document.getString("prize"));
                entry.timestamp = document.getString("time_log");
                results.add(entry);
            }
        });
        return results;
    }

    @Override
    public int countLSGDBauCua(final String username, final byte moneyType) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final Document conditions = new Document();
        conditions.put("user_name", username);
        conditions.put("money_type", moneyType);
        final long totalRows = db.getCollection("bau_cua_transaction").count(conditions);
        return (int) totalRows;
    }

    @Override
    public ResultBauCua getPhienBauCua(final long referenceId) {
        final ResultBauCua result = new ResultBauCua();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        FindIterable iterable = null;
        final Document conditions = new Document();
        conditions.put("reference_id", referenceId);
        iterable = db.getCollection("bau_cua_transaction").find(conditions).limit(1);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                result.referenceId = document.getLong("reference_id");
                result.dices[0] = document.getInteger("dice1").byteValue();
                result.dices[1] = document.getInteger("dice2").byteValue();
                result.dices[2] = document.getInteger("dice3").byteValue();
                result.xPot = document.getInteger("x_pot").byteValue();
                result.xValue = document.getInteger("x_value").byteValue();
            }
        });
        return result;
    }

    @Override
    public List<TopWin> getTopBauCua(final byte moneyType, final String startDate, final String endDate) {
        final int pageSize = 10;
        final ArrayList<TopWin> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        final BasicDBObject obj = new BasicDBObject();
        obj.put("$gte", startDate);
        obj.put("$lte", endDate);
        conditions.put("time_log", obj);
        conditions.put("money_type", moneyType);
        final AggregateIterable iterable = db.getCollection("bau_cua_transaction").aggregate(Arrays.asList(new Document("$match", conditions), new Document("$group", new Document("_id", "$user_name").append("total", new Document("$sum", "$money_exchange"))), new Document("$sort", new Document("total", -1)), new Document("$limit", 10)));
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final TopWin entry = new TopWin();
                entry.setUsername(document.getString("_id"));
                entry.setMoney(document.getLong("total"));
                if (entry.getMoney() > 0L) {
                    results.add(entry);
                }
            }
        });
        return results;
    }

    @Override
    public List<ResultBauCua> getLichSuPhien(final int size, final byte room) {
        final ArrayList<ResultBauCua> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        FindIterable iterable = null;
        final Document conditions = new Document();
        conditions.put("room", room);
        final BasicDBObject sortCondtions = new BasicDBObject();
        sortCondtions.put("_id", -1);
        iterable = db.getCollection("bau_cua_results").find(conditions).sort(sortCondtions).limit(size);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final ResultBauCua entry = new ResultBauCua();
                entry.referenceId = document.getLong("reference_id");
                entry.room = document.getInteger("room").byteValue();
                entry.dices[0] = document.getInteger("dice1").byteValue();
                entry.dices[1] = document.getInteger("dice2").byteValue();
                entry.dices[2] = document.getInteger("dice3").byteValue();
                entry.xPot = document.getInteger("x_pot").byteValue();
                entry.xValue = document.getInteger("x_value").byteValue();
                results.add(0, entry);
            }
        });
        return results;
    }

    @Override
    public List<ToiChonCa> getTopToiChonCa(final String startDate, final String endDate) {
        final ArrayList<ToiChonCa> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, BasicDBObject> conditions = new HashMap<>();
        final BasicDBObject obj = new BasicDBObject();
        obj.put("$gte", startDate);
        obj.put("$lte", endDate);
        conditions.put("time_log", obj);
        final Document sortObj = new Document();
        sortObj.put("so_ca", -1);
        sortObj.put("so_van", -1);
        sortObj.put("tong_thang", -1);
        sortObj.put("tong_dat", -1);
        sortObj.put("time_log", -1);
        final AggregateIterable iterable = db.getCollection("bau_cua_toi_chon_ca").aggregate(Arrays.asList(new Document("$match", conditions), new Document("$sort", sortObj), new Document("$limit", 10)));
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final ToiChonCa entry = new ToiChonCa();
                entry.username = document.getString("user_name");
                entry.soCa = (short) document.getInteger("so_ca", 0);
                entry.soVan = (short) document.getInteger("so_van", 0);
                entry.tongThang = document.getLong("tong_thang");
                entry.tongDat = document.getLong("tong_dat");
                entry.currentPhien = document.getLong("current_phien");
                entry.timestamp = document.getString("time_log");
                results.add(entry);
            }
        });
        return results;
    }

    @Override
    public short getHighScore(final String username) {
        short result = 0;
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        FindIterable iterable = null;
        final Document conditions = new Document();
        conditions.put("user_name", username);
        final BasicDBObject obj = new BasicDBObject();
        obj.put("$gte", DateTimeUtils.getStartTimeToDay());
        obj.put("$lte", DateTimeUtils.getEndTimeToDay());
        conditions.put("time_log", obj);
        iterable = db.getCollection("bau_cua_toi_chon_ca").find(conditions);
        final Document doc = (Document) iterable.first();
        if (doc != null) {
            result = doc.getInteger("so_ca").shortValue();
        }
        return result;
    }
}
