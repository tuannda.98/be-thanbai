// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.vinplay.dal.dao.MiniGameDAO;
import com.vinplay.dal.dao.impl.MiniGameDAOImpl;
import com.vinplay.dal.service.MiniGameService;
import com.vinplay.vbee.common.messages.minigame.UpdateFundMessage;
import com.vinplay.vbee.common.messages.minigame.UpdatePotMessage;
import com.vinplay.vbee.common.response.BonusFundResponse;
import com.vinplay.vbee.common.rmq.RMQApi;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.TimeoutException;

public class MiniGameServiceImpl implements MiniGameService {
    final MiniGameDAO dao;

    public MiniGameServiceImpl() {
        this.dao = new MiniGameDAOImpl();
    }

    @Override
    public long getPot(final String potName) throws SQLException {
        return this.dao.getPot(potName);
    }

    @Override
    public long[] getPots(final String potName) throws SQLException {
        return this.dao.getPots(potName);
    }

    @Override
    public long getReferenceId(final int gameId) throws SQLException {
        return this.dao.getReferenceId(gameId);
    }

    @Override
    public long getFund(final String fundName) throws SQLException {
        return this.dao.getFund(fundName);
    }

    @Override
    public void saveFund(final String fundName, final long value) throws IOException, TimeoutException, InterruptedException {
        final UpdateFundMessage msg = new UpdateFundMessage();
        msg.fundName = fundName;
        msg.newValue = value;
        RMQApi.publishMessage("queue_fund", msg, 110);
    }

    @Override
    public long[] getFunds(final String fundName) throws SQLException {
        return this.dao.getFunds(fundName);
    }

    @Override
    public List<BonusFundResponse> getFunds() throws SQLException {
        return this.dao.getFunds();
    }

    @Override
    public boolean saveReferenceId(final long newRefenceId, final int gameId) throws SQLException {
        return this.dao.saveReferenceId(newRefenceId, gameId);
    }

    @Override
    public void savePot(final String potName, final long value, final boolean x2) throws IOException, TimeoutException, InterruptedException {
        final UpdatePotMessage message = new UpdatePotMessage();
        message.potName = potName;
        message.newValue = value;
        RMQApi.publishMessage("queue_pot", message, 106);
        final CacheServiceImpl cacheService = new CacheServiceImpl();
        cacheService.setValue(potName, (int) value);
        cacheService.setValue(potName + "_x2", x2 ? 1 : 0);
    }
}
