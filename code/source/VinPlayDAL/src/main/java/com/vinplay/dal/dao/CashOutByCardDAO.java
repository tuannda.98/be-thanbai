// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao;

import com.vinplay.vbee.common.response.CashOutByCardResponse;
import com.vinplay.vbee.common.response.MoneyTotalRechargeByCardReponse;

import java.util.List;

public interface CashOutByCardDAO {
    List<CashOutByCardResponse> searchCashOutByCard(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5, final int p6, final String p7, final String p8);

    int countSearchCashOutByCard(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5, final String p6, final String p7);

    long moneyTotal(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5, final String p6, final String p7);

    List<MoneyTotalRechargeByCardReponse> moneyTotalCashOutByCard(final String p0, final String p1, final String p2, final String p3);

    List<CashOutByCardResponse> exportDataCashOutByCard(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5);
}
