// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao;

import com.vinplay.dal.entities.report.ReportMoneySystemModel;
import com.vinplay.dal.entities.taixiu.ResultTaiXiu;
import com.vinplay.dal.entities.taixiu.TransactionTaiXiu;
import com.vinplay.dal.entities.taixiu.TransactionTaiXiuDetail;
import com.vinplay.dal.entities.taixiu.VinhDanhRLTLModel;
import com.vinplay.vbee.common.models.cache.ThanhDuTXModel;
import com.vinplay.vbee.common.models.minigame.TopWin;
import com.vinplay.vbee.common.models.minigame.taixiu.XepHangRLTLModel;

import java.sql.SQLException;
import java.util.List;

public interface TaiXiuDAO {
    List<ResultTaiXiu> getLichSuPhien(final int p0, final int p1) throws SQLException;

    ResultTaiXiu getKetQuaPhien(final long p0, final int p1) throws SQLException;

    List<TransactionTaiXiu> getLichSuGiaoDich(final String p0, final int p1, final int p2) throws SQLException;

    int countLichSuGiaoDichTX(final String p0, final int p1) throws SQLException;

    List<TopWin> getTopTaiXiu(final int p0) throws SQLException;

    List<TransactionTaiXiuDetail> getChiTietPhien(final long p0, final int p1) throws SQLException;

    List<ThanhDuTXModel> getTopThanhDuDaily(final String p0, final String p1, final short p2) throws SQLException;

    int getMaxThanhDu(final String p0, final short p1) throws SQLException;

    int getSoLanRutLoc(final String p0) throws SQLException;

    List<XepHangRLTLModel> getXepHangTanLoc();

    List<VinhDanhRLTLModel> getVinhDanhTanLoc();

    long getTongTienTanLoc(final String p0);

    List<XepHangRLTLModel> getXepHangRutLoc();

    List<VinhDanhRLTLModel> getVinhDanhRutLoc();

    long getTongTienRutLoc(final String p0);

    ReportMoneySystemModel getReportTXToDay();

    ReportMoneySystemModel getReportTX(final String p0, final String p1);
}
