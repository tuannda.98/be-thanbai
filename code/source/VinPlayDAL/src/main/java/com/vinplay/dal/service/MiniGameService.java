// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service;

import com.vinplay.vbee.common.response.BonusFundResponse;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.TimeoutException;

public interface MiniGameService {
    long getPot(final String p0) throws SQLException;

    long[] getPots(final String p0) throws SQLException;

    long getFund(final String p0) throws SQLException;

    long[] getFunds(final String p0) throws SQLException;

    List<BonusFundResponse> getFunds() throws SQLException;

    void saveFund(final String p0, final long p1) throws IOException, TimeoutException, InterruptedException;

    void savePot(final String p0, final long p1, final boolean p2) throws IOException, TimeoutException, InterruptedException;

    long getReferenceId(final int p0) throws SQLException;

    boolean saveReferenceId(final long p0, final int p1) throws SQLException;
}
