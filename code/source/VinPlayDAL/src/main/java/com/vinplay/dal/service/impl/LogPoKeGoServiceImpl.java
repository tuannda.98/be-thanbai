// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.vinplay.dal.dao.impl.LogPokegoDAOImpl;
import com.vinplay.dal.service.LogPoKeGoService;
import com.vinplay.vbee.common.response.PokegoResponse;

import java.sql.SQLException;
import java.util.List;

public class LogPoKeGoServiceImpl implements LogPoKeGoService {
    @Override
    public List<PokegoResponse> listLogPokego(final String referentId, final String userName, final String moneyType, final String betValue, final String timeStart, final String timeEnd, final int page) {
        final LogPokegoDAOImpl dao = new LogPokegoDAOImpl();
        return dao.listLogPokego(referentId, userName, moneyType, betValue, timeStart, timeEnd, page);
    }

    @Override
    public long countLogPokego(final String referentId, final String userName, final String moneyType, final String betValue, final String timeStart, final String timeEnd) {
        final LogPokegoDAOImpl dao = new LogPokegoDAOImpl();
        return dao.countLogPokego(referentId, userName, moneyType, betValue, timeStart, timeEnd);
    }
}
