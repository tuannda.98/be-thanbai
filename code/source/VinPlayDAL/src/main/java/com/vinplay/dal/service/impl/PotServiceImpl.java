package com.vinplay.dal.service.impl;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.transaction.TransactionContext;
import com.hazelcast.transaction.TransactionOptions;
import com.vinplay.dal.service.PotService;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.messages.LogMoneyUserMessage;
import com.vinplay.vbee.common.messages.NoHuMessage;
import com.vinplay.vbee.common.messages.PotMessage;
import com.vinplay.vbee.common.messages.gamebai.LogNoHuGameBaiMessage;
import com.vinplay.vbee.common.models.FreezeModel;
import com.vinplay.vbee.common.models.PotModel;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.response.PotResponse;
import com.vinplay.vbee.common.rmq.RMQApi;
import com.vinplay.vbee.common.utils.VinPlayUtils;
import org.apache.log4j.Logger;

import java.util.Calendar;

public class PotServiceImpl implements PotService {
    private static final Logger logger;

    @Override
    public PotModel getPot(final String gameName) {
        final IMap potMap = HazelcastClientFactory.getInstance().getMap("huGameBai");
        if (potMap.containsKey(gameName)) {
            return (PotModel) potMap.get(gameName);
        }
        return null;
    }

    @Override
    public PotResponse addMoneyPot(final String potName, final long money, final boolean isInitial) {
        PotServiceImpl.logger.debug("Request addMoneyPot: " + potName);
        final PotResponse res = new PotResponse(false, "1001");
        final HazelcastInstance client = HazelcastClientFactory.getInstance();
        if (client == null) {
            PotServiceImpl.logger.debug("hazelcast not connected");
            return res;
        }
        final IMap potMap = client.getMap("huGameBai");
        if (potMap.containsKey(potName)) {
            try {
                potMap.lock(potName);
                potMap.lock("Vinplay");
                final PotModel pot = (PotModel) potMap.get(potName);
                long valuePot = pot.getValue();
                long maxPot = -1L;
                final IMap map = client.getMap("cacheConfig");
                if (map != null && map.containsKey("HU_GAME_BAI_MAX")) {
                    maxPot = Long.parseLong((String) map.get("HU_GAME_BAI_MAX"));
                }
                if (maxPot == -1L || valuePot + money <= maxPot) {
                    final Calendar today = Calendar.getInstance();
                    final int dateInYear = today.get(Calendar.DAY_OF_YEAR);
                    if (!isInitial || dateInYear != pot.getLastDay()) {
                        final PotModel potSystem = (PotModel) potMap.get("Vinplay");
                        final TransactionContext context = client.newTransactionContext(new TransactionOptions().setTransactionType(TransactionOptions.TransactionType.ONE_PHASE));
                        context.beginTransaction();
                        try {
                            long valuePotSystem = potSystem.getValue();
                            res.setValue(valuePot);
                            res.setValue(valuePotSystem);
                            valuePotSystem -= money;
                            pot.setValue(valuePot += money);
                            if (isInitial) {
                                pot.setLastDay(dateInYear);
                            }
                            potSystem.setValue(valuePotSystem);
                            final PotMessage message = new PotMessage(potName, money, valuePot, valuePotSystem);
                            RMQApi.publishMessage("queue_hu_gamebai", message, 401);
                            potMap.put(potName, pot);
                            potMap.put("Vinplay", potSystem);
                            context.commitTransaction();
                            res.setSuccess(true);
                            res.setErrorCode("0");
                            res.setValue(valuePot);
                        } catch (Exception e) {
                            PotServiceImpl.logger.debug("addMoneyPot error : " + e);
                            context.rollbackTransaction();
                        }
                    }
                } else {
                    res.setErrorCode("1050");
                }
            } catch (Exception e2) {
                PotServiceImpl.logger.debug("addMoneyPot error : " + e2);
            } finally {
                potMap.unlock(potName);
                potMap.unlock("Vinplay");
            }
        }
        PotServiceImpl.logger.debug("Response addMoneyPot: " + res.toJson());
        return res;
    }

    @Override
    public PotResponse noHu(final String sessionId, final String nickname, final String gameName, final String roomId, final long maxFreeze, final String matchId, final String potName, final double ratioPot, final int betValue, final String desc) {
        PotServiceImpl.logger.debug("Request noHu: sessionId: " + sessionId + ", nickname: " + nickname + ", gameName: " + gameName + ", roomId: " + roomId + ", maxFreeze: " + maxFreeze + ", matchId: " + matchId + ", potName: " + potName + ", ratioPot: " + ratioPot + ", betValue: " + betValue + ", desc: " + desc);
        final PotResponse res = new PotResponse(false, "1001");
        final HazelcastInstance client = HazelcastClientFactory.getInstance();
        if (client == null) {
            PotServiceImpl.logger.debug("hazelcast not connected");
            return res;
        }
        final IMap potMap = client.getMap("huGameBai");
        final IMap<String, UserModel> userMap = client.getMap("users");
        final IMap freezeMap = client.getMap("freeze");
        if (potMap.containsKey(potName) && userMap.containsKey(nickname) && freezeMap.containsKey(sessionId)) {
            try {
                potMap.lock(potName);
                userMap.lock(nickname);
                freezeMap.lock(sessionId);
                final PotModel pot = (PotModel) potMap.get(potName);
                final UserCacheModel user = (UserCacheModel) userMap.get(nickname);
                final FreezeModel freeze = (FreezeModel) freezeMap.get(sessionId);
                final TransactionContext context = client.newTransactionContext(new TransactionOptions().setTransactionType(TransactionOptions.TransactionType.ONE_PHASE));
                context.beginTransaction();
                try {
                    long moneyUser = user.getVin();
                    long currentMoney = user.getVinTotal();
                    long freezeMoney = freeze.getMoney();
                    long valuePot;
                    final long valuePotBefore = valuePot = pot.getValue();
                    final long money = Math.round(valuePot * ratioPot);
                    res.setCurrentMoneyUser(currentMoney);
                    res.setFreezeMoneyUser(freezeMoney);
                    res.setValue(valuePot);
                    res.setMoneyExchange(money);
                    long addMoneyUser = 0L;
                    long addMoneyFreeze = 0L;
                    if (freezeMoney >= maxFreeze) {
                        addMoneyUser = money;
                    } else if (freezeMoney + money > maxFreeze) {
                        addMoneyUser = freezeMoney + money - maxFreeze;
                        addMoneyFreeze = maxFreeze - freezeMoney;
                    } else {
                        addMoneyFreeze = money;
                    }
                    user.setVin(moneyUser += addMoneyUser);
                    user.setVinTotal(currentMoney += money);
                    freeze.setMoney(freezeMoney += addMoneyFreeze);
                    pot.setValue(valuePot -= money);
                    final long fMoney = (addMoneyFreeze == 0L) ? -1L : freezeMoney;
                    final NoHuMessage message = new NoHuMessage(VinPlayUtils.genMessageId(), user.getId(), nickname, gameName, moneyUser, currentMoney, money, "vin", fMoney, sessionId, 0L, 0, 0, potName, valuePot);
                    final LogMoneyUserMessage messageLog = new LogMoneyUserMessage(user.getId(), nickname, gameName, VinPlayUtils.getServiceName(gameName), currentMoney, money, "vin", "N\u1ed5 h\u0169. Ph\u00f2ng: " + roomId + ", B\u00e0n: " + matchId, 0L, false, user.isBot());
                    final LogNoHuGameBaiMessage nohuMessage = new LogNoHuGameBaiMessage(nickname, betValue, valuePotBefore, money, gameName, desc, "");
                    // TODO: NO HANDLE COMMAND TARGET FOR 19 HERE. USE DEFAULT:
                    //  queueName = "queue_payment_gamebai";
                    //                command = 42;
                    RMQApi.publishMessagePayment(message, 19);
                    RMQApi.publishMessageLogMoney(messageLog);
                    RMQApi.publishMessage("queue_hu_gamebai", nohuMessage, 402);
                    freezeMap.put(sessionId, freeze);
                    userMap.put(nickname, user);
                    potMap.put(potName, pot);
                    context.commitTransaction();
                    res.setSuccess(true);
                    res.setErrorCode("0");
                    res.setCurrentMoneyUser(currentMoney);
                    res.setFreezeMoneyUser(freezeMoney);
                    res.setValue(valuePot);
                    res.setMoneyExchange(money);
                } catch (Exception e) {
                    PotServiceImpl.logger.debug("noHu error : " + e);
                    context.rollbackTransaction();
                }
            } catch (Exception e2) {
                PotServiceImpl.logger.debug("noHu error : " + e2);
            } finally {
                potMap.unlock(potName);
                userMap.unlock(nickname);
                freezeMap.unlock(sessionId);
            }
        }
        PotServiceImpl.logger.debug("Response noHu: " + res.toJson());
        return res;
    }

    static {
        logger = Logger.getLogger("user_core");
    }
}
