// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.vinplay.dal.dao.impl.TopRechargeMoneyDAOImpl;
import com.vinplay.dal.service.TopRechargeMoneyService;
import com.vinplay.vbee.common.response.TopRechargeMoneyResponse;

import java.sql.SQLException;
import java.util.List;

public class TopRechargeMoneyServiceImpl implements TopRechargeMoneyService {
    @Override
    public List<TopRechargeMoneyResponse> getTopRechargeMoney(final int top, final String nickName, final int page, final int bot) throws SQLException {
        final TopRechargeMoneyDAOImpl dao = new TopRechargeMoneyDAOImpl();
        return dao.getTopRechargeMoney(top, nickName, page, bot);
    }
}
