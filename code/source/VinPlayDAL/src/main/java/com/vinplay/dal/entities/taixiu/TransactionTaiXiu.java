// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.entities.taixiu;

public class TransactionTaiXiu {
    public long referenceId;
    public int userId;
    public String username;
    public long betValue;
    public int betSide;
    public long totalPrize;
    public long totalRefund;
    public int moneyType;
    public String timestamp;
    public String resultPhien;

    public TransactionTaiXiu() {
        this.betValue = 0L;
        this.totalPrize = 0L;
        this.totalRefund = 0L;
    }

    public TransactionTaiXiu(final long referenceId, final int userId, final String username, final long betValue, final int betSide, final long totalPrize, final long totalRefund, final String timestamp) {
        this.betValue = 0L;
        this.totalPrize = 0L;
        this.totalRefund = 0L;
        this.referenceId = referenceId;
        this.userId = userId;
        this.username = username;
        this.betValue = betValue;
        this.betSide = betSide;
        this.totalPrize = totalPrize;
        this.totalRefund = totalRefund;
        this.timestamp = timestamp;
    }
}
