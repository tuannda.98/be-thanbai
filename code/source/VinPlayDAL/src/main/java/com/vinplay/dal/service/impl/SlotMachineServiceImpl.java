// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.vinplay.dal.dao.SlotMachineDAO;
import com.vinplay.dal.dao.impl.SlotMachineDAOImpl;
import com.vinplay.dal.service.SlotMachineService;
import com.vinplay.vbee.common.enums.Games;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.messages.minigame.LogNoHuSlotMessage;
import com.vinplay.vbee.common.messages.slot.LogSlotMachineMessage;
import com.vinplay.vbee.common.models.cache.SlotFreeDaily;
import com.vinplay.vbee.common.models.cache.TopPokeGoModel;
import com.vinplay.vbee.common.models.minigame.pokego.LSGDPokeGo;
import com.vinplay.vbee.common.models.minigame.pokego.TopPokeGo;
import com.vinplay.vbee.common.models.slot.NoHuModel;
import com.vinplay.vbee.common.models.slot.SlotFreeSpin;
import com.vinplay.vbee.common.rmq.RMQApi;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeoutException;

public class SlotMachineServiceImpl implements SlotMachineService {
    private final SlotMachineDAO dao;

    public SlotMachineServiceImpl() {
        this.dao = new SlotMachineDAOImpl();
    }

    @Override
    public void logKhoBau(final long referenceId, final String username, final long betValue, final String linesBetting, final String linesWin, final String prizesOnLine, final short result, final long totalPrizes, final String time) throws IOException, TimeoutException, InterruptedException {
        final LogSlotMachineMessage msg = this.buildLogSlotMsg(Games.KHO_BAU.getName(), referenceId, username, betValue, linesBetting, linesWin, prizesOnLine, result, totalPrizes, time);
        this.publishSlotMsg("queue_kho_bau", msg, 135);
    }

    @Override
    public void logAvengers(final long referenceId, final String username, final long betValue, final String linesBetting, final String linesWin, final String prizesOnLine, final short result, final long totalPrizes, final String time) throws IOException, TimeoutException, InterruptedException {
        final LogSlotMachineMessage msg = this.buildLogSlotMsg(Games.AVENGERS.getName(), referenceId, username, betValue, linesBetting, linesWin, prizesOnLine, result, totalPrizes, time);
        this.publishSlotMsg("queue_avengers", msg, 136);
    }

    @Override
    public void logMyNhanNgu(final long referenceId, final String username, final long betValue, final String linesBetting, final String linesWin, final String prizesOnLine, final short result, final long totalPrizes, final String time) throws IOException, TimeoutException, InterruptedException {
        final LogSlotMachineMessage msg = this.buildLogSlotMsg(Games.MY_NHAN_NGU.getName(), referenceId, username, betValue, linesBetting, linesWin, prizesOnLine, result, totalPrizes, time);
        this.publishSlotMsg("queue_my_nhan_ngu", msg, 136);
    }

    @Override
    public void logVQV(final long referenceId, final String username, final long betValue, final String linesBetting, final String linesWin, final String prizesOnLine, final short result, final long totalPrizes, final String time) throws IOException, TimeoutException, InterruptedException {
        final LogSlotMachineMessage msg = this.buildLogSlotMsg(Games.VUONG_QUOC_VIN.getName(), referenceId, username, betValue, linesBetting, linesWin, prizesOnLine, result, totalPrizes, time);
        this.publishSlotMsg("queue_vqv", msg, 139);
    }

    private LogSlotMachineMessage buildLogSlotMsg(final String gameName, final long referenceId, final String username, final long betValue, final String linesBetting, final String linesWin, final String prizesOnLine, final short result, final long totalPrizes, final String time) {
        final LogSlotMachineMessage message = new LogSlotMachineMessage();
        message.gameName = gameName;
        message.referenceId = referenceId;
        message.username = username;
        message.betValue = betValue;
        message.linesBetting = linesBetting;
        message.linesWin = linesWin;
        message.prizesOnLine = prizesOnLine;
        message.result = result;
        message.totalPrizes = totalPrizes;
        message.time = time;
        return message;
    }

    private void publishSlotMsg(final String queueName, final LogSlotMachineMessage msg, final int commnad) throws IOException, TimeoutException, InterruptedException {
        RMQApi.publishMessage(queueName, msg, commnad);
    }

    @Override
    public int countLSDG(final String gameName, final String username) {
        return this.dao.countLSGD(gameName, username);
    }

    @Override
    public List<LSGDPokeGo> getLSGD(final String gameName, final String username, final int pageNumber) {
        return this.dao.getLSGD(gameName, username, pageNumber);
    }

    @Override
    public void addTop(final String gameName, final String username, final int betValue, final long totalPrizes, final String time, final int result) {
        final HazelcastInstance client = HazelcastClientFactory.getInstance();
        final IMap topMap = client.getMap("cacheTop");
        TopPokeGoModel topPokeGo = (TopPokeGoModel) topMap.get(gameName);
        if (topPokeGo == null) {
            topPokeGo = new TopPokeGoModel();
        }
        final TopPokeGo entry = new TopPokeGo();
        entry.un = username;
        entry.bv = betValue;
        entry.pz = totalPrizes;
        entry.ts = time;
        entry.rs = result;
        topPokeGo.put(entry);
        topMap.put(gameName, topPokeGo);
    }

    @Override
    public List<TopPokeGo> getTopSlotMachine(final String gameName, final int page) {
        if (page <= 10) {
            int pageSize = 10;
            if (gameName.equalsIgnoreCase(Games.KHO_BAU.getName())) {
                pageSize = 5;
            }
            final HazelcastInstance client;
            final IMap topMap;
            TopPokeGoModel topPokeGo;
            if ((topPokeGo = (TopPokeGoModel) (topMap = (client = HazelcastClientFactory.getInstance()).getMap("cacheTop")).get(gameName)) == null) {
                topPokeGo = new TopPokeGoModel();
            }
            if (topPokeGo.getResults().size() == 0) {
                final List<TopPokeGo> results = this.dao.getTop(gameName, 100);
                topPokeGo.setResults(results);
                topMap.put(gameName, topPokeGo);
            }
            return topPokeGo.getResults(page, pageSize);
        }
        return this.dao.getTopByPage(gameName, page);
    }

    @Override
    public long getLastReferenceId(final String gameName) {
        return this.dao.getLastRefenceId(gameName);
    }

    @Override
    public SlotFreeSpin updateLuotQuaySlotFree(final String gameName, final String username) {
        final HazelcastInstance client = HazelcastClientFactory.getInstance();
        final IMap slotMap = client.getMap(this.buildKeySlot(gameName));
        int soLuotFree = 0;
        SlotFreeSpin slotModel = new SlotFreeSpin();
        if (slotMap.containsKey(username)) {
            slotModel = (SlotFreeSpin) slotMap.get(username);
            slotModel.setNum(slotModel.getNum() - 1);
            soLuotFree = slotModel.getNum();
            if (soLuotFree <= 0) {
                slotMap.remove(username);
            } else {
                slotMap.put(username, slotModel);
            }
        }
        return slotModel;
    }

    @Override
    public void logNuDiepVien(final long referenceId, final String username, final long betValue, final String linesBetting, final String linesWin, final String prizesOnLine, final short result, final long totalPrizes, final String time) throws IOException, TimeoutException, InterruptedException {
        final LogSlotMachineMessage msg = this.buildLogSlotMsg(Games.NU_DIEP_VIEN.getName(), referenceId, username, betValue, linesBetting, linesWin, prizesOnLine, result, totalPrizes, time);
        this.publishSlotMsg("queue_nu_diep_vien", msg, 138);
    }

    @Override
    public void setLuotQuayFreeSlot(final String gameName, final String nickName, final String lines, final int soLuot, final int ratio) {
        final HazelcastInstance client = HazelcastClientFactory.getInstance();
        final IMap slotMap = client.getMap(this.buildKeySlot(gameName));
        if (slotMap.containsKey(nickName)) {
            try {
                final SlotFreeSpin slotFreeModel = (SlotFreeSpin) slotMap.get(nickName);
                slotFreeModel.setNum(soLuot);
                slotFreeModel.setRatio(ratio);
                slotFreeModel.setLines(lines);
                slotMap.put(nickName, slotFreeModel);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            final SlotFreeSpin slotFreeModel = new SlotFreeSpin();
            slotFreeModel.setNum(soLuot);
            slotFreeModel.setRatio(ratio);
            slotFreeModel.setLines(lines);
            slotMap.put(nickName, slotFreeModel);
        }
    }

    @Override
    public SlotFreeSpin getLuotQuayFreeSlot(final String gameName, final String nickName) {
        final HazelcastInstance client = HazelcastClientFactory.getInstance();
        final IMap slotMap = client.getMap(this.buildKeySlot(gameName));
        SlotFreeSpin slotFreeSpin = new SlotFreeSpin();
        if (slotMap.containsKey(nickName)) {
            try {
                slotFreeSpin = (SlotFreeSpin) slotMap.get(nickName);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return slotFreeSpin;
    }

    private String buildKeySlot(final String gameName) {
        return "cache" + gameName + "FreeSpin";
    }

    @Override
    public void setItemsWild(final String gameName, final String nickName, final String itemsWild) {
        final HazelcastInstance client = HazelcastClientFactory.getInstance();
        final IMap slotMap = client.getMap(this.buildKeySlot(gameName));
        if (slotMap.containsKey(nickName)) {
            try {
                final SlotFreeSpin slotFreeSpin = (SlotFreeSpin) slotMap.get(nickName);
                slotFreeSpin.setItemsWild(itemsWild);
                slotMap.put(nickName, slotFreeSpin);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public int getPrizes(final String gameName, final String nickName) {
        final HazelcastInstance client = HazelcastClientFactory.getInstance();
        final IMap slotMap = client.getMap(this.buildKeySlot(gameName));
        int prizes = 0;
        if (slotMap.containsKey(nickName)) {
            try {
                final SlotFreeSpin slotFreeSpin = (SlotFreeSpin) slotMap.get(nickName);
                prizes = slotFreeSpin.getPrizes();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return prizes;
    }

    @Override
    public void addPrizes(final String gameName, final String nickName, final int prize) {
        final HazelcastInstance client = HazelcastClientFactory.getInstance();
        final IMap slotMap = client.getMap(this.buildKeySlot(gameName));
        if (slotMap.containsKey(nickName)) {
            try {
                final SlotFreeSpin slotFreeSpin = (SlotFreeSpin) slotMap.get(nickName);
                slotFreeSpin.addPrize(prize);
                slotMap.put(nickName, slotFreeSpin);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public SlotFreeDaily getLuotQuayFreeDaily(final String gameName, final String nickName, final int room) {
        final HazelcastInstance client = HazelcastClientFactory.getInstance();
        final String key = nickName + "-" + gameName + "-" + room;
        final IMap slotMap = client.getMap("cacheSlotFree");
        if (slotMap.containsKey(key)) {
            return (SlotFreeDaily) slotMap.get(key);
        }
        return null;
    }

    @Override
    public boolean updateLuotQuayFreeDaily(final String gameName, final String nickName, final int room) {
        final HazelcastInstance client = HazelcastClientFactory.getInstance();
        final String key = nickName + "-" + gameName + "-" + room;
        final IMap slotMap = client.getMap("cacheSlotFree");
        boolean success = false;
        if (slotMap.containsKey(key)) {
            try {
                slotMap.lock(nickName);
                final SlotFreeDaily slotModel = (SlotFreeDaily) slotMap.get(key);
                if (slotModel.getRotateFree() > 0) {
                    slotModel.setRotateFree(slotModel.getRotateFree() - 1);
                    success = true;
                    slotMap.put(key, slotModel);
                    this.dao.updateSlotFreeDaily(gameName, nickName, room, slotModel.getRotateFree());
                }
            } catch (Exception e) {
                success = false;
            } finally {
                slotMap.unlock(nickName);
            }
        }
        return success;
    }

    @Override
    public void logNoHu(final long referenceId, final String gameName, final String nickName, final int betValue, final String linesBetting, final String matrix, final String linesWin, final String prizesOnLine, final long totalPrizes, final short result, final String time) {
        final LogNoHuSlotMessage msg = new LogNoHuSlotMessage();
        msg.referenceId = referenceId;
        msg.gameName = gameName;
        msg.nickName = nickName;
        msg.betValue = betValue;
        msg.linesBetting = linesBetting;
        msg.matrix = matrix;
        msg.linesWin = linesWin;
        msg.prizesOnLine = prizesOnLine;
        msg.totalPrizes = totalPrizes;
        msg.result = result;
        msg.time = time;
        try {
            RMQApi.publishMessage("queue_log_nohu", msg, 140);
        } catch (IOException | InterruptedException | TimeoutException ex4) {
            ex4.printStackTrace();
        }
    }

    @Override
    public List<NoHuModel> getLogNoHu(final String gameName, final int page) {
        return this.dao.getListNoHu(gameName, page);
    }
}
