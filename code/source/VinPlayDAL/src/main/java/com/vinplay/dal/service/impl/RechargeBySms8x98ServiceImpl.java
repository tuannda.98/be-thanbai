// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.vinplay.dal.dao.impl.RechargeBySms8x98DAOImpl;
import com.vinplay.dal.service.RechargeBySms8x98Service;
import com.vinplay.vbee.common.response.MoneyTotalRechargeByCardReponse;
import com.vinplay.vbee.common.response.RechargeBySmsResponse;

import java.util.List;

public class RechargeBySms8x98ServiceImpl implements RechargeBySms8x98Service {
    @Override
    public List<MoneyTotalRechargeByCardReponse> moneyTotalRechargeBySms8x98(final String timeStart, final String timeEnd, final String code) {
        final RechargeBySms8x98DAOImpl dao = new RechargeBySms8x98DAOImpl();
        return dao.moneyTotalRechargeBySms8x98(timeStart, timeEnd, code);
    }

    @Override
    public List<RechargeBySmsResponse> exportDataRechargeBySms(final String timeStart, final String timeEnd, final String amount, final String code) {
        final RechargeBySms8x98DAOImpl dao = new RechargeBySms8x98DAOImpl();
        return dao.exportDataRechargeBySms(timeStart, timeEnd, amount, code);
    }
}
