// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.vinplay.dal.dao.LogMoneyUserDao;
import com.vinplay.dal.dao.impl.LogMoneyUserDaoImpl;
import com.vinplay.dal.service.LogMoneyUserService;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.messages.gamebai.LogNoHuGameBaiMessage;
import com.vinplay.vbee.common.models.cache.TransactionList;
import com.vinplay.vbee.common.response.LogMoneyUserResponse;
import com.vinplay.vbee.common.response.LogUserMoneyResponse;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class LogMoneyUserServiceImpl implements LogMoneyUserService {
    private static final Logger logger = Logger.getLogger("backend");
    public static int version = 1;

    @Override
    public List<LogUserMoneyResponse> searchLogMoneyUser(final String nickName, final String userName, final String moneyType, final String serviceName, final String actionName, final String timeStart, final String timeEnd, final int page, final int like, final int totalRecord) {
        final LogMoneyUserDaoImpl dao = new LogMoneyUserDaoImpl();
        List<LogUserMoneyResponse> result = null;
        result = dao.searchLogMoneyUser(nickName, userName, moneyType, serviceName, actionName, timeStart, timeEnd, page, like, totalRecord);
        return result;
    }

    @Override
    public List<LogUserMoneyResponse> searchLogMoneyUserDes(final String nickName, final String userName, final String moneyType, final String serviceName, final String actionName, final String timeStart, final String timeEnd, final int page, final int like, final int totalRecord, String descCol) {
        final LogMoneyUserDaoImpl dao = new LogMoneyUserDaoImpl();
        List<LogUserMoneyResponse> result = null;
        result = dao.searchLogMoneyUserDes(nickName, userName, moneyType, serviceName, actionName, timeStart, timeEnd, page, like, totalRecord, descCol);
        return result;
    }

    @Override
    public int countsearchLogMoneyUser(final String nickName, final String moneyType, final String serviceName, final String actionName, final String timeStart, final String timeEnd, final int like) {
        final LogMoneyUserDaoImpl dao = new LogMoneyUserDaoImpl();
        final int totalRecords = dao.countsearchLogMoneyUser(nickName, moneyType, serviceName, actionName, timeStart, timeEnd, like);
        return totalRecords / 100 + 1;
    }

    @Override
    public int countsearchLogMoneyUser(final String nickName, final String userName, final String moneyType, final String serviceName, final String actionName, final String timeStart, final String timeEnd, final int like, final int recordPerPage) {
        final LogMoneyUserDao dao = new LogMoneyUserDaoImpl();
        final int totalRecords = dao.countsearchLogMoneyUser2(nickName, "", moneyType, serviceName, actionName, timeStart, timeEnd, like);
        int totalPage = totalRecords / recordPerPage;
        if (totalRecords % recordPerPage != 0)
            totalPage++;
        return totalPage;
    }

    @Override
    public List<LogMoneyUserResponse> getHistoryTransactionLogMoney(final String nickName, final int moneyType, final int page) {
        final HazelcastInstance client = HazelcastClientFactory.getInstance();
        final IMap transMap = client.getMap("cacheTransaction");
        final String key = nickName + "-" + moneyType;
        List result = null;
        if (page <= 5) {
            if (transMap.containsKey(key)) {
                TransactionList tranList = new TransactionList();
                tranList = (TransactionList) transMap.get(key);
                result = tranList.get(page);
            }
            if (result == null || result.size() == 0) {
                result = this.pushHistoryTransactionDBToCache((IMap<String, TransactionList>) transMap, nickName, moneyType);
            }
            return (List<LogMoneyUserResponse>) result;
        }
        if (result == null) {
            final LogMoneyUserDaoImpl dao = new LogMoneyUserDaoImpl();
            result = dao.getHistoryTransactionLogMoney(nickName, moneyType, page);
        }
        return (List<LogMoneyUserResponse>) result;
    }

    @Override
    public int countHistoryTransactionLogMoney(final String nickname, final int moneyType) {
        final LogMoneyUserDaoImpl dao = new LogMoneyUserDaoImpl();
        final int totalRecords = dao.countHistoryTransactionLogMoney(nickname, moneyType);
        return totalRecords / 13 + 1;
    }

    @Override
    public List<LogNoHuGameBaiMessage> getNoHuGameBaiHistory(final int pageNumber, final String gameName) {
        final LogMoneyUserDaoImpl dao = new LogMoneyUserDaoImpl();
        return dao.getNoHuGameBaiHistory(pageNumber, gameName);
    }

    @Override
    public int countNoHuGameBaiHistory() {
        final LogMoneyUserDaoImpl dao = new LogMoneyUserDaoImpl();
        return dao.countNoHuGameBaiHistory();
    }

    @Override
    public List<LogUserMoneyResponse> searchLogMoneyTranferUser(final String nickName, final String timestart, final String timeEnd, final String type, final int page) {
        final LogMoneyUserDaoImpl dao = new LogMoneyUserDaoImpl();
        return dao.searchLogMoneyTranferUser(nickName, timestart, timeEnd, type, page);
    }

    @Override
    public boolean UpdateProcessLogChuyenTienDaiLy(final String nickNameSend, final String nickNameReceive, final String timeLog, final String Status) {
        try {
            final LogMoneyUserDaoImpl dao = new LogMoneyUserDaoImpl();
            dao.UpdateProcessLogChuyenTienDaiLy(nickNameSend, nickNameReceive, timeLog, Status);
            dao.UpdateProcessLogChuyenTienDaiLyMySQL(nickNameSend, nickNameReceive, timeLog, Status);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public List<LogMoneyUserResponse> pushHistoryTransactionDBToCache(final IMap<String, TransactionList> transMap, final String nickname, final int queryType) {
        List<LogMoneyUserResponse> result = new ArrayList<>();
        final String key = nickname + "-" + queryType;
        final LogMoneyUserDaoImpl dao = new LogMoneyUserDaoImpl();
        result = dao.getTransactionList(nickname, queryType, 0, 65);
        final TransactionList tranList = new TransactionList();
        tranList.setList(result);
        result = tranList.get(1);
        transMap.lock(key);
        transMap.put(key, tranList, 72L, TimeUnit.HOURS);
        transMap.unlock(key);
        return result;
    }
}
