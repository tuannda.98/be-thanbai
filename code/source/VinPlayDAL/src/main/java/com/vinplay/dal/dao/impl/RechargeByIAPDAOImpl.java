// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import com.vinplay.dal.dao.RechargeByIAPDAO;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.response.RechardByIAPResponse;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RechargeByIAPDAOImpl implements RechargeByIAPDAO {
    private long totalMoney;

    public RechargeByIAPDAOImpl() {
        this.totalMoney = 0L;
    }

    @Override
    public List<RechardByIAPResponse> ListRechargeIAP(final String nickName, final String code, final String timeStart, final String timeEnd, final String amount, final String orderId, final int page) {
        final ArrayList<RechardByIAPResponse> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        FindIterable iterable = null;
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        final int num_start = (page - 1) * 50;
        final int num_end = 50;
        final Document conditions = new Document();
        if (nickName != null && !nickName.equals("")) {
            conditions.put("nick_name", nickName);
        }
        if (code != null && !code.equals("")) {
            conditions.put("code", Integer.parseInt(code));
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("trans_time", obj);
        }
        if (amount != null && !amount.equals("")) {
            conditions.put("amount", Integer.parseInt(amount));
        }
        if (orderId != null && !orderId.equals("")) {
            conditions.put("order_id", orderId);
        }
        iterable = db.getCollection("dvt_recharge_by_iap").find(new Document(conditions)).sort(objsort).skip(num_start).limit(50);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final RechardByIAPResponse iap = new RechardByIAPResponse();
                iap.nick_name = document.getString("nick_name");
                iap.amount = document.getInteger("amount");
                iap.code = document.getInteger("code");
                iap.description = document.getString("description");
                iap.trans_time = document.getString("trans_time");
                iap.order_id = document.getString("order_id");
                iap.package_name = document.getString("package_name");
                iap.product_id = document.getString("product_id");
                iap.purchase_time = document.getLong("purchase_time");
                iap.purchase_state = document.getInteger("purchase_state");
                iap.developer_payload = document.getString("developer_payload");
                iap.token = document.getString("token");
                iap.signture = document.getString("signature");
                results.add(iap);
            }
        });
        return results;
    }

    @Override
    public long totalMoney(final String nickName, final String code, final String timeStart, final String timeEnd, final String amount, final String orderId) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        FindIterable iterable = null;
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        final Document conditions = new Document();
        if (nickName != null && !nickName.equals("")) {
            conditions.put("nick_name", nickName);
        }
        if (code != null && !code.equals("")) {
            conditions.put("code", Integer.parseInt(code));
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("trans_time", obj);
        }
        if (amount != null && !amount.equals("")) {
            conditions.put("amount", Integer.parseInt(amount));
        }
        if (orderId != null && !orderId.equals("")) {
            conditions.put("order_id", orderId);
        }
        iterable = db.getCollection("dvt_recharge_by_iap").find(new Document(conditions)).sort(objsort);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final RechargeByIAPDAOImpl rechargeByIAPDAOImpl = RechargeByIAPDAOImpl.this;
                rechargeByIAPDAOImpl.totalMoney += document.getInteger("amount");
            }
        });
        return this.totalMoney;
    }

    @Override
    public long countListRechargeIAP(final String nickName, final String code, final String timeStart, final String timeEnd, final String amount, final String orderId) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        final Document conditions = new Document();
        if (nickName != null && !nickName.equals("")) {
            conditions.put("nick_name", nickName);
        }
        if (code != null && !code.equals("")) {
            conditions.put("code", Integer.parseInt(code));
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("trans_time", obj);
        }
        if (amount != null && !amount.equals("")) {
            conditions.put("amount", Integer.parseInt(amount));
        }
        if (orderId != null && !orderId.equals("")) {
            conditions.put("order_id", orderId);
        }
        return db.getCollection("dvt_recharge_by_iap").count(new Document(conditions));
    }
}
