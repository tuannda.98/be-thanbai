// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.vinplay.dal.dao.impl.LuckyNewHistoryDAOImpl;
import com.vinplay.dal.service.LuckyNewHistoryService;
import com.vinplay.vbee.common.response.LuckyNewHistoryResponse;

import java.util.List;

public class LuckyNewHistoryServiceImpl implements LuckyNewHistoryService {
    @Override
    public List<LuckyNewHistoryResponse> searchLuckyNewHistory(final String nickName, final String timeStart, final String timeEnd, final int page) {
        final LuckyNewHistoryDAOImpl dao = new LuckyNewHistoryDAOImpl();
        return dao.searchLuckyNewHistory(nickName, timeStart, timeEnd, page);
    }
}
