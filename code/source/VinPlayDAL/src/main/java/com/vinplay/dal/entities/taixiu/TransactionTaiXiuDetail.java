package com.vinplay.dal.entities.taixiu;

import java.sql.Date;

public class TransactionTaiXiuDetail {
    public long referenceId;
    public String transactionCode;
    public int transactionId;
    public int userId;
    public String username;
    public long betValue;
    public int betSide;
    public long prize;
    public long refund;
    public int inputTime;
    public int moneyType;
    public Date timestamp;

    public TransactionTaiXiuDetail(final long referenceId, final int transactionId, final int userId, final String username, final long betValue, final int betSide, final long prize, final long refund, final int inputTime, final Date timestamp) {
        this.betValue = 0L;
        this.prize = 0L;
        this.refund = 0L;
        this.referenceId = referenceId;
        this.transactionId = transactionId;
        this.userId = userId;
        this.username = username;
        this.betValue = betValue;
        this.betSide = betSide;
        this.prize = prize;
        this.refund = refund;
        this.inputTime = inputTime;
        this.timestamp = timestamp;
    }

    public TransactionTaiXiuDetail(final long referenceId, final int userId, final String username, final long betValue, final int betSide, final int inputTime, final int moneyType) {
        this.betValue = 0L;
        this.prize = 0L;
        this.refund = 0L;
        this.referenceId = referenceId;
        this.userId = userId;
        this.username = username;
        this.betValue = betValue;
        this.betSide = betSide;
        this.prize = 0L;
        this.refund = 0L;
        this.inputTime = inputTime;
        this.moneyType = moneyType;
    }

    public TransactionTaiXiuDetail() {
        this.betValue = 0L;
        this.prize = 0L;
        this.refund = 0L;
    }

    public void genTransactionCode() {
        this.transactionCode = this.username + "_" + this.inputTime + System.currentTimeMillis();
    }
}
