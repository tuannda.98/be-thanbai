// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.vinplay.dal.entities.report.ReportMoneySystemModel;
import com.vinplay.dal.service.GameBaiService;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.models.cache.ReportModel;
import com.vinplay.vbee.common.utils.VinPlayUtils;

import java.util.Map;

public class GameBaiServiceImpl implements GameBaiService {
    @Override
    public ReportMoneySystemModel getReportGameToday(final String gameName) {
        final ReportMoneySystemModel res = new ReportMoneySystemModel();
        final String today = VinPlayUtils.getCurrentDate();
        final HazelcastInstance client = HazelcastClientFactory.getInstance();
        final IMap<String, ReportModel> reportMap = client.getMap("cacheReports");
        for (final Map.Entry<String, ReportModel> entry : reportMap.entrySet()) {
            if (entry.getKey().contains(today)) {
                final String actionname;
                if (!(actionname = entry.getKey().split(",")[1]).equals(gameName)) {
                    continue;
                }
                final ReportModel model = entry.getValue();
                if (model.isBot) {
                    continue;
                }
                final ReportMoneySystemModel reportMoneySystemModel7;
                final ReportMoneySystemModel reportMoneySystemModel = reportMoneySystemModel7 = res;
                reportMoneySystemModel7.moneyWin += model.moneyWin;
                final ReportMoneySystemModel reportMoneySystemModel8;
                final ReportMoneySystemModel reportMoneySystemModel2 = reportMoneySystemModel8 = res;
                reportMoneySystemModel8.moneyLost += model.moneyLost;
                final ReportMoneySystemModel reportMoneySystemModel9;
                final ReportMoneySystemModel reportMoneySystemModel3 = reportMoneySystemModel9 = res;
                reportMoneySystemModel9.moneyOther += model.moneyOther;
                final ReportMoneySystemModel reportMoneySystemModel10;
                final ReportMoneySystemModel reportMoneySystemModel4 = reportMoneySystemModel10 = res;
                reportMoneySystemModel10.fee += model.fee;
                final ReportMoneySystemModel reportMoneySystemModel11;
                final ReportMoneySystemModel reportMoneySystemModel5 = reportMoneySystemModel11 = res;
                reportMoneySystemModel11.revenuePlayGame += model.moneyWin + model.moneyLost;
                final ReportMoneySystemModel reportMoneySystemModel12;
                final ReportMoneySystemModel reportMoneySystemModel6 = reportMoneySystemModel12 = res;
                reportMoneySystemModel12.revenue += model.moneyWin + model.moneyLost + model.moneyOther;
            }
        }
        return res;
    }
}
