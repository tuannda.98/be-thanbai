// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao;

import com.vinplay.dal.entities.report.ReportMoneySystemModel;
import com.vinplay.dal.entities.report.ReportTotalMoneyModel;
import com.vinplay.vbee.common.models.cache.ReportModel;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface ReportDAO {
    List<String> getAllBot() throws SQLException;

    ReportTotalMoneyModel getTotalMoney(final String p0) throws SQLException;

    long getCurrentMoney(final String p0) throws SQLException;

    long getSafeMoney(final String p0) throws SQLException;

    boolean checkBot(final String p0) throws SQLException;

    Map<String, ReportMoneySystemModel> getReportMoneySystem(final String p0, final String p1, final boolean p2) throws Exception;

    Map<String, ReportMoneySystemModel> getReportMoneySystemMySQL(final String p0, final String p1, final boolean p2) throws Exception;

    HashMap<String, Long> getReportTopGame(final String p0, final String p1, final String p2, final boolean p3) throws Exception;

    Map<String, ReportMoneySystemModel> getReportMoneyUser(final String p0, final String p1, final String p2, final boolean p3) throws Exception;

    Map<String, ReportModel> getListReportModelByDay(final String p0, final boolean p1) throws Exception;

    boolean saveLogTotalMoney(final ReportTotalMoneyModel p0);

    List<ReportTotalMoneyModel> getReportTotalMoney(final int p0, final String p1, final String p2);

    ReportTotalMoneyModel getReportTotalMoneyAtTime(final String p0, final boolean p1) throws ParseException;

    boolean saveLogMoneyForReport(final String p0, final String p1, final String p2, final ReportModel p3) throws ParseException;

    boolean saveTopCaoThu(final String p0, final String p1, final long p2);

    void saveReportMoneyVin(final Map<String, ReportModel> p0);
}
