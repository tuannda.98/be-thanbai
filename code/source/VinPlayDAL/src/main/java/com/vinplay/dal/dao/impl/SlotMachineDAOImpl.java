// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao.impl;

import casio.king365.core.CommonDbPool;
import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import com.vinplay.dal.dao.SlotMachineDAO;
import com.vinplay.vbee.common.enums.Games;
import com.vinplay.vbee.common.models.minigame.pokego.LSGDPokeGo;
import com.vinplay.vbee.common.models.minigame.pokego.TopPokeGo;
import com.vinplay.vbee.common.models.slot.NoHuModel;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.pools.ConnectionPool;
import org.bson.Document;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class SlotMachineDAOImpl implements SlotMachineDAO {
    @Override
    public List<TopPokeGo> getTopByPage(final String gameName, final int pageNumber) {
        int pageSize = 10;
        if (gameName.equalsIgnoreCase(Games.KHO_BAU.getName())) {
            pageSize = 5;
        }
        final int skipNumber = (pageNumber - 1) * pageSize;
        final ArrayList<TopPokeGo> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        FindIterable iterable = null;
        final Document conditions = new Document();
        conditions.put("$or", Arrays.asList(new Document("result", 3), new Document("result", 4)));
        final BasicDBObject sortCondtions = new BasicDBObject();
        sortCondtions.put("_id", -1);
        iterable = db.getCollection("log_" + gameName).find(conditions).sort(sortCondtions).skip(skipNumber).limit(pageSize);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final TopPokeGo entry = new TopPokeGo();
                entry.un = document.getString("user_name");
                entry.bv = document.getLong("bet_value");
                entry.pz = document.getLong("prize");
                entry.ts = document.getString("time_log");
                entry.rs = document.getInteger("result");
                results.add(entry);
            }
        });
        return results;
    }

    @Override
    public int countLSGD(final String gameName, final String username) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final Document conditions = new Document();
        conditions.put("user_name", username);
        final long totalRows = db.getCollection("log_" + gameName).count(conditions);
        return (int) totalRows;
    }

    @Override
    public List<LSGDPokeGo> getLSGD(final String gameName, final String username, final int pageNumber) {
        int pageSize = 10;
        if (gameName.equalsIgnoreCase(Games.KHO_BAU.getName())) {
            pageSize = 5;
        }
        final int skipNumber = (pageNumber - 1) * pageSize;
        final ArrayList<LSGDPokeGo> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        FindIterable iterable = null;
        final Document conditions = new Document();
        conditions.put("user_name", username);
        final BasicDBObject sortCondtions = new BasicDBObject();
        sortCondtions.put("_id", -1);
        iterable = db.getCollection("log_" + gameName).find(conditions).sort(sortCondtions).skip(skipNumber).limit(pageSize);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final LSGDPokeGo entry = new LSGDPokeGo();
                entry.rf = document.getLong("reference_id");
                entry.un = document.getString("user_name");
                entry.bv = document.getLong("bet_value");
                entry.pz = document.getLong("prize");
                entry.lb = document.getString("lines_betting");
                entry.lw = document.getString("lines_win");
                entry.ps = document.getString("prizes_on_line");
                entry.ts = document.getString("time_log");
                results.add(entry);
            }
        });
        return results;
    }

    @Override
    public long getLastRefenceId(final String gameName) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap conditions = new HashMap();
        final BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        final FindIterable iterable = db.getCollection("log_" + gameName).find(new Document(conditions)).sort(objsort).limit(1);
        final Document document = (iterable != null) ? ((Document) iterable.first()) : null;
        return (document == null) ? 0L : document.getLong("reference_id");
    }

    @Override
    public List<TopPokeGo> getTop(final String gameName, final int number) {
        final ArrayList<TopPokeGo> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        FindIterable iterable = null;
        final Document conditions = new Document();
        conditions.put("$or", Arrays.asList(new Document("result", 3), new Document("result", 4)));
        final BasicDBObject sortCondtions = new BasicDBObject();
        sortCondtions.put("_id", -1);
        iterable = db.getCollection("log_" + gameName).find(conditions).sort(sortCondtions).limit(number);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final TopPokeGo entry = new TopPokeGo();
                entry.un = document.getString("user_name");
                entry.bv = document.getLong("bet_value");
                entry.pz = document.getLong("prize");
                entry.ts = document.getString("time_log");
                entry.rs = document.getInteger("result");
                results.add(entry);
            }
        });
        return results;
    }

    @Override
    public boolean updateSlotFreeDaily(final String gameName, final String nickname, final int room, final int newValue) throws SQLException {
        boolean res = false;
        final String sql = "UPDATE rotate_slot_free SET rotate_free = ? WHERE nick_name=? AND game_name = '" + gameName + "' AND room = ?";
        try (final Connection conn = CommonDbPool.getInstance().getVinplayMinigameDS().getConnection();
             final PreparedStatement stm = conn.prepareStatement(sql);) {
            stm.setInt(1, newValue);
            stm.setString(2, nickname);
            stm.setInt(3, room);
            if (stm.executeUpdate() == 1) {
                res = true;
            }
        }
        return res;
    }

    @Override
    public List<NoHuModel> getListNoHu(final String gameName, final int page) {
        final int numItems = 10;
        final int start = (page - 1) * 10;
        final ArrayList<NoHuModel> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        FindIterable iterable = null;
        final Document conditions = new Document();
        conditions.append("game_name", gameName);
        final BasicDBObject sortCondtions = new BasicDBObject();
        sortCondtions.put("_id", -1);
        iterable = db.getCollection("log_no_hu_slot").find(conditions).sort(sortCondtions).skip(start).limit(10);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final NoHuModel entry = new NoHuModel();
                entry.rf = document.getLong("reference_id");
                entry.nn = document.getString("nick_name");
                entry.bv = document.getInteger("bet_value");
                entry.pz = document.getLong("prize");
                entry.mx = document.getString("matrix");
                entry.lw = document.getString("lines_win");
                entry.pl = document.getString("prizes_on_line");
                entry.ts = document.getString("time_log");
                entry.rs = document.getInteger("result");
                results.add(entry);
            }
        });
        return results;
    }
}
