// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.vinplay.dal.dao.MiniGameDAO;
import com.vinplay.dal.dao.MiniPokerDAO;
import com.vinplay.dal.dao.impl.MiniGameDAOImpl;
import com.vinplay.dal.dao.impl.MiniPokerDAOImpl;
import com.vinplay.dal.entities.minipoker.LSGDMiniPoker;
import com.vinplay.dal.entities.minipoker.VinhDanhMiniPoker;
import com.vinplay.dal.service.MiniPokerService;
import com.vinplay.vbee.common.enums.Games;
import com.vinplay.vbee.common.messages.minigame.LogMiniPokerMessage;
import com.vinplay.vbee.common.messages.minigame.UpdateFundMessage;
import com.vinplay.vbee.common.messages.minigame.UpdatePotMessage;
import com.vinplay.vbee.common.rmq.RMQApi;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.TimeoutException;

public class MiniPokerServiceImpl implements MiniPokerService {
    final MiniPokerDAO dao;
    final MiniGameDAO miniGameDAO;

    public MiniPokerServiceImpl() {
        this.dao = new MiniPokerDAOImpl();
        this.miniGameDAO = new MiniGameDAOImpl();
    }

    @Override
    public long[] getPotMiniPoker() throws SQLException {
        return this.miniGameDAO.getPots(Games.MINI_POKER.getName());
    }

    @Override
    public long[] getFundMiniPoker() throws SQLException {
        return this.miniGameDAO.getFunds(Games.MINI_POKER.getName());
    }

    @Override
    public void updatePotMiniPoker(final String potName, final long newValue) throws IOException, TimeoutException, InterruptedException {
        final UpdatePotMessage message = new UpdatePotMessage();
        message.potName = potName;
        message.newValue = newValue;
        RMQApi.publishMessage("queue_pot", message, 106);
    }

    @Override
    public void updateFundMiniPoker(final String fundName, final long newValue) throws IOException, TimeoutException, InterruptedException {
        final UpdateFundMessage message = new UpdateFundMessage();
        message.fundName = fundName;
        message.newValue = newValue;
        RMQApi.publishMessage("queue_fund", message, 110);
    }

    @Override
    public void logMiniPoker(final String username, final long betValue, final short result, final long prize, final String cards, final long currentPot, final long currentFund, final int moneyType) throws IOException, TimeoutException, InterruptedException {
        final LogMiniPokerMessage message = new LogMiniPokerMessage();
        message.username = username;
        message.betValue = betValue;
        message.result = result;
        message.prize = prize;
        message.cards = cards;
        message.currentPot = currentPot;
        message.currentFund = currentFund;
        message.moneyType = moneyType;
        RMQApi.publishMessage("queue_minipoker", message, 111);
    }

    @Override
    public List<LSGDMiniPoker> getLichSuGiaoDich(final String username, final int pageNumber, final int moneyType) {
        return this.dao.getLichSuGiaoDich(username, pageNumber, moneyType);
    }

    @Override
    public List<VinhDanhMiniPoker> getBangVinhDanh(final int moneyType, final int page) {
        return this.dao.getBangVinhDanh(moneyType, page);
    }

    @Override
    public int countLichSuGiaoDich(final String username, final int moneyType) {
        return this.dao.countLichSuGiaoDich(username, moneyType);
    }
}
