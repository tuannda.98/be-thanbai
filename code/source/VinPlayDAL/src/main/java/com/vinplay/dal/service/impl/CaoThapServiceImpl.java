// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.vinplay.dal.dao.CaoThapDAO;
import com.vinplay.dal.dao.impl.CaoThapDAOImpl;
import com.vinplay.dal.entities.caothap.LSGDCaoThap;
import com.vinplay.dal.entities.caothap.TopCaoThap;
import com.vinplay.dal.entities.caothap.VinhDanhCaoThap;
import com.vinplay.dal.service.CaoThapService;
import com.vinplay.vbee.common.messages.minigame.LogCaoThapMessage;
import com.vinplay.vbee.common.messages.minigame.LogCaoThapWinMessage;
import com.vinplay.vbee.common.messages.minigame.UpdateFundMessage;
import com.vinplay.vbee.common.messages.minigame.UpdatePotMessage;
import com.vinplay.vbee.common.rmq.RMQApi;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.TimeoutException;

public class CaoThapServiceImpl implements CaoThapService {
    final CaoThapDAO dao;

    public CaoThapServiceImpl() {
        this.dao = new CaoThapDAOImpl();
    }

    @Override
    public long[] getPotCaoThap() throws SQLException {
        return this.dao.getPotCaoThap("cao_thap");
    }

    @Override
    public long[] getFundCaoThap() throws SQLException {
        return this.dao.getFundCaoThap("cao_thap");
    }

    @Override
    public void updatePotCaoThap(final String potName, final long newValue) throws IOException, TimeoutException, InterruptedException {
        final UpdatePotMessage message = new UpdatePotMessage();
        message.potName = potName;
        message.newValue = newValue;
        RMQApi.publishMessage("queue_pot", message, 106);
    }

    @Override
    public void updateFundCaoThap(final String fundName, final long newValue) throws IOException, TimeoutException, InterruptedException {
        final UpdateFundMessage message = new UpdateFundMessage();
        message.fundName = fundName;
        message.newValue = newValue;
        RMQApi.publishMessage("queue_fund", message, 110);
    }

    @Override
    public void logCaoThap(final long transId, final String nickname, final long betValue, final short result, final long prize, final String cards, final long currentPot, final long currentFund, final int moneyType, final short potBet, final int step) throws IOException, TimeoutException, InterruptedException {
        final LogCaoThapMessage message = new LogCaoThapMessage();
        message.transId = transId;
        message.nickname = nickname;
        message.betValue = betValue;
        message.result = result;
        message.prize = prize;
        message.cards = cards;
        message.currentPot = currentPot;
        message.currentFund = currentFund;
        message.moneyType = moneyType;
        message.potBet = potBet;
        message.step = step;
        RMQApi.publishMessage("queue_caothap", message, 112);
    }

    @Override
    public void logCaoThapWin(final long transId, final String nickname, final long betValue, final short result, final long prize, final String cards, final int moneyType) throws IOException, TimeoutException, InterruptedException {
        final LogCaoThapWinMessage message = new LogCaoThapWinMessage();
        message.transId = transId;
        message.nickname = nickname;
        message.betValue = betValue;
        message.result = result;
        message.prize = prize;
        message.cards = cards;
        message.moneyType = moneyType;
        RMQApi.publishMessage("queue_caothap", message, 113);
    }

    @Override
    public List<LSGDCaoThap> getLichSuGiaoDich(final String nickname, final int pageNumber, final int moneyType) {
        return this.dao.getLichSuGiaoDich(nickname, pageNumber, moneyType);
    }

    @Override
    public List<VinhDanhCaoThap> getBangVinhDanh(final int pageNumber, final int moneyType) {
        return this.dao.getBangVinhDanh(pageNumber, moneyType);
    }

    @Override
    public int countLichSuGiaoDich(final String nickname, final int moneyType) {
        return this.dao.countLichSuGiaoDich(nickname, moneyType);
    }

    @Override
    public int countVinhDanh(final int moneyType) {
        return this.dao.countVinhDanh(moneyType);
    }

    @Override
    public long getLastReferenceId() {
        return this.dao.getLastReferenceId();
    }

    @Override
    public List<TopCaoThap> geTopCaoThap(final String startTime, final String endTime) {
        return this.dao.getTop(startTime, endTime);
    }

    @Override
    public void insertBotEvent(final String nickname, final long betValue, final long prize, final String cards) {
        try {
            this.logCaoThapWin(-1L, nickname, betValue, (short) 4, prize, cards, 1);
        } catch (IOException | InterruptedException | TimeoutException ex4) {
            ex4.printStackTrace();
        }
    }
}
