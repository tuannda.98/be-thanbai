// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service;

import com.vinplay.vbee.common.models.PotModel;
import com.vinplay.vbee.common.response.PotResponse;

public interface PotService {
    PotModel getPot(final String p0);

    PotResponse addMoneyPot(final String p0, final long p1, final boolean p2);

    PotResponse noHu(final String p0, final String p1, final String p2, final String p3, final long p4, final String p5, final String p6, final double p7, final int p8, final String p9);
}
