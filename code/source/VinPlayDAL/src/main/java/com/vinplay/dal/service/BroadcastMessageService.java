// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service;

public interface BroadcastMessageService {
    void putMessage(final int p0, final String p1, final long p2);

    String toJson();

    void clearMessage();
}
