// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.vinplay.dal.dao.impl.RechargeByMegaCardDAOImpl;
import com.vinplay.dal.service.RechargeByMegaCardService;
import com.vinplay.vbee.common.response.MoneyTotalRechargeByCardReponse;
import com.vinplay.vbee.common.response.megacard.MegaCardResponse;

import java.util.List;

public class RechargeByMegaCardServiceImpl implements RechargeByMegaCardService {
    @Override
    public List<MegaCardResponse> searchRechargeByMegaCard(final String nickName, final String provider, final String serial, final String pin, final String code, final String timeStart, final String timeEnd, final int page, final String transId) {
        final RechargeByMegaCardDAOImpl dao = new RechargeByMegaCardDAOImpl();
        return dao.searchRechargeByMegaCard(nickName, provider, serial, pin, code, timeStart, timeEnd, page, transId);
    }

    @Override
    public int countSearchRechargeByMegaCard(final String nickName, final String provider, final String serial, final String pin, final String code, final String timeStart, final String timeEnd, final String transId) {
        final RechargeByMegaCardDAOImpl dao = new RechargeByMegaCardDAOImpl();
        return dao.countSearchRechargeByMegaCard(nickName, provider, serial, pin, code, timeStart, timeEnd, transId);
    }

    @Override
    public long moneyTotal(final String nickName, final String provider, final String serial, final String pin, final String code, final String timeStart, final String timeEnd, final String transId) {
        final RechargeByMegaCardDAOImpl dao = new RechargeByMegaCardDAOImpl();
        return dao.moneyTotal(nickName, provider, serial, pin, code, timeStart, timeEnd, transId);
    }

    @Override
    public List<MoneyTotalRechargeByCardReponse> moneyTotalRechargeByMegaCard(final String timeStart, final String timeEnd, final String code) {
        final RechargeByMegaCardDAOImpl dao = new RechargeByMegaCardDAOImpl();
        return dao.moneyTotalRechargeByMegaCard(timeStart, timeEnd, code);
    }
}
