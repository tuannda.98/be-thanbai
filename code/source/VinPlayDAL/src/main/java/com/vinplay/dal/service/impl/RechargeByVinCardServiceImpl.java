// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.vinplay.dal.dao.impl.RechargeByVinCardDAOImpl;
import com.vinplay.dal.service.RechargeByVinCardService;
import com.vinplay.vbee.common.response.MoneyTotalRechargeByCardReponse;
import com.vinplay.vbee.common.response.VinCardResponse;

import java.util.List;

public class RechargeByVinCardServiceImpl implements RechargeByVinCardService {
    @Override
    public List<VinCardResponse> searchRechargeByVinCard(final String nickName, final String provider, final String serial, final String pin, final String code, final String timeStart, final String timeEnd, final int page, final String transId) {
        final RechargeByVinCardDAOImpl dao = new RechargeByVinCardDAOImpl();
        return dao.searchRechargeByVinCard(nickName, provider, serial, pin, code, timeStart, timeEnd, page, transId);
    }

    @Override
    public int countSearchRechargeByVinCard(final String nickName, final String provider, final String serial, final String pin, final String code, final String timeStart, final String timeEnd, final String transId) {
        final RechargeByVinCardDAOImpl dao = new RechargeByVinCardDAOImpl();
        return dao.countSearchRechargeByVinCard(nickName, provider, serial, pin, code, timeStart, timeEnd, transId);
    }

    @Override
    public long moneyTotal(final String nickName, final String provider, final String serial, final String pin, final String code, final String timeStart, final String timeEnd, final String transId) {
        final RechargeByVinCardDAOImpl dao = new RechargeByVinCardDAOImpl();
        return dao.moneyTotal(nickName, provider, serial, pin, code, timeStart, timeEnd, transId);
    }

    @Override
    public List<MoneyTotalRechargeByCardReponse> moneyTotalRechargeByVinplayCard(final String timeStart, final String timeEnd, final String code) {
        final RechargeByVinCardDAOImpl dao = new RechargeByVinCardDAOImpl();
        return dao.moneyTotalRechargeByVinplayCard(timeStart, timeEnd, code);
    }
}
