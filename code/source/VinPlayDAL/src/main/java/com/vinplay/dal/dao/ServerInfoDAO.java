// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao;

import com.vinplay.vbee.common.models.LogCCUModel;

import java.util.List;

public interface ServerInfoDAO {
    List<LogCCUModel> getLogCCU(final String p0, final String p1);
}
