// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao;

import com.vinplay.vbee.common.models.minigame.TopWin;
import com.vinplay.vbee.common.models.minigame.baucua.ResultBauCua;
import com.vinplay.vbee.common.models.minigame.baucua.ToiChonCa;
import com.vinplay.vbee.common.models.minigame.baucua.TransactionBauCua;

import java.util.List;

public interface BauCuaDAO {
    List<TransactionBauCua> getLSGDBauCua(final String p0, final int p1, final byte p2);

    int countLSGDBauCua(final String p0, final byte p1);

    ResultBauCua getPhienBauCua(final long p0);

    List<TopWin> getTopBauCua(final byte p0, final String p1, final String p2);

    List<ResultBauCua> getLichSuPhien(final int p0, final byte p1);

    List<ToiChonCa> getTopToiChonCa(final String p0, final String p1);

    short getHighScore(final String p0);
}
