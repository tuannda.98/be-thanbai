// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import casio.king365.util.KingUtil;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.vinplay.dal.dao.BauCuaDAO;
import com.vinplay.dal.dao.impl.BauCuaDAOImpl;
import com.vinplay.dal.service.BauCuaService;
import com.vinplay.vbee.common.enums.Games;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.messages.minigame.baucua.ResultBauCuaMsg;
import com.vinplay.vbee.common.messages.minigame.baucua.ToiChonCaMsg;
import com.vinplay.vbee.common.messages.minigame.baucua.TransactionBauCuaDetailMsg;
import com.vinplay.vbee.common.messages.minigame.baucua.TransactionBauCuaMsg;
import com.vinplay.vbee.common.models.cache.ToiChonCaModel;
import com.vinplay.vbee.common.models.cache.TopWinCache;
import com.vinplay.vbee.common.models.minigame.TopWin;
import com.vinplay.vbee.common.models.minigame.baucua.ResultBauCua;
import com.vinplay.vbee.common.models.minigame.baucua.ToiChonCa;
import com.vinplay.vbee.common.models.minigame.baucua.TransactionBauCua;
import com.vinplay.vbee.common.models.minigame.baucua.TransactionBauCuaDetail;
import com.vinplay.vbee.common.rmq.RMQApi;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeoutException;

public class BauCuaServiceImpl implements BauCuaService {
    private final BauCuaDAO dao;

    public BauCuaServiceImpl() {
        this.dao = new BauCuaDAOImpl();
    }

    @Override
    public void saveTransactionBauCuaDetail(final TransactionBauCuaDetail tranDetail) throws IOException, TimeoutException, InterruptedException {
        final TransactionBauCuaDetailMsg msg = new TransactionBauCuaDetailMsg();
        msg.username = tranDetail.username;
        msg.referenceId = tranDetail.referenceId;
        msg.room = tranDetail.room;
        msg.transactionCode = tranDetail.transactionCode;
        msg.betValues = tranDetail.betValues;
        msg.moneyType = tranDetail.moneyType;
        RMQApi.publishMessage("queue_baucua", msg, 130);
    }

    @Override
    public void saveTransactionBauCua(final TransactionBauCua transaction) throws IOException, TimeoutException, InterruptedException {
        final TransactionBauCuaMsg msg = new TransactionBauCuaMsg();
        msg.username = transaction.username;
        msg.referenceId = transaction.referenceId;
        msg.room = transaction.room;
        msg.dices = transaction.dices;
        msg.transactionCode = transaction.transactionCode;
        msg.betValues = transaction.betValues;
        msg.prizes = transaction.prizes;
        msg.totalExchange = transaction.totalExchange;
        msg.moneyType = transaction.moneyType;
        RMQApi.publishMessage("queue_baucua", msg, 131);
    }

    @Override
    public void saveResultBauCua(final ResultBauCua resultBC) throws IOException, TimeoutException, InterruptedException {
        final ResultBauCuaMsg msg = new ResultBauCuaMsg();
        msg.referenceId = resultBC.referenceId;
        msg.room = resultBC.room;
        msg.minBetValue = resultBC.minBetValue;
        msg.dices = resultBC.dices;
        msg.xPot = resultBC.xPot;
        msg.xValue = resultBC.xValue;
        msg.totalBetValues = resultBC.totalBetValues;
        msg.totalPrizes = resultBC.totalPrizes;
        RMQApi.publishMessage("queue_baucua", msg, 132);
    }

    @Override
    public List<TransactionBauCua> getLSGDBauCua(final String username, final int page, final byte moneyType) {
        return this.dao.getLSGDBauCua(username, page, moneyType);
    }

    @Override
    public ResultBauCua getPhienBauCua(final long referenceId) {
        return this.dao.getPhienBauCua(referenceId);
    }

    @Override
    public List<TopWin> getTopBauCua(final byte moneyType, final String startDate, final String endDate) {
        final HazelcastInstance client = HazelcastClientFactory.getInstance();
        final IMap topMap = client.getMap("cacheTop");
        final TopWinCache topBCCache;
        if (topMap.containsKey(Games.BAU_CUA.getName() + "_" + moneyType) && (topBCCache = (TopWinCache) topMap.get(Games.BAU_CUA.getName() + "_" + moneyType)) != null) {
            return topBCCache.getResult();
        }
        return this.dao.getTopBauCua(moneyType, startDate, endDate);
    }

    @Override
    public void updateAllTop() {
        KingUtil.printLog("updateAllTop() 1");
        final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        final String currentDate = df.format(new Date());
        final String startDate = currentDate + " 00:00:00";
        final String endDate = currentDate + " 23:59:59";
        final List<TopWin> topWinVin = this.dao.getTopBauCua((byte) 1, startDate, endDate);
        final List<TopWin> topWinXu = this.dao.getTopBauCua((byte) 0, startDate, endDate);
        final HazelcastInstance client = HazelcastClientFactory.getInstance();
        final IMap topMap = client.getMap("cacheTop");
        KingUtil.printLog("updateAllTop() 2");
        TopWinCache cacheVin = (TopWinCache) topMap.get(Games.BAU_CUA.getName() + "_1");
        if (cacheVin == null) {
            cacheVin = new TopWinCache();
        }
        KingUtil.printLog("updateAllTop() 3");
        cacheVin.setResult(topWinVin);
        topMap.put(Games.BAU_CUA.getName() + "_1", cacheVin);
        TopWinCache cacheXu = (TopWinCache) topMap.get(Games.BAU_CUA.getName() + "_0");
        KingUtil.printLog("updateAllTop() 4");
        if (cacheXu == null) {
            cacheXu = new TopWinCache();
        }
        KingUtil.printLog("updateAllTop() 5");
        cacheXu.setResult(topWinXu);
        topMap.put(Games.BAU_CUA.getName() + "_0", cacheXu);
        KingUtil.printLog("updateAllTop() 6");
    }

    @Override
    public int countLSGDBauCua(final String username, final byte moneyType) {
        return this.dao.countLSGDBauCua(username, moneyType) / 10 + 1;
    }

    @Override
    public void calculteToiChonCa(final byte[] dices, final List<TransactionBauCua> transactions) throws IOException, TimeoutException, InterruptedException {
        boolean existCa = false;
        final int CA = 3;
        for (byte dice : dices) {
            if (dice == 3) {
                existCa = true;
            }
        }
        final HazelcastInstance client = HazelcastClientFactory.getInstance();
        final IMap map = client.getMap("cacheToiChonCa");
        for (final TransactionBauCua tran : transactions) {
            if (map.containsKey(tran.username)) {
                try {
                    map.lock(tran.username);
                    final ToiChonCaModel model2 = (ToiChonCaModel) map.get(tran.username);
                    if (!model2.playOnToday()) {
                        model2.soCaHighScore = 0;
                        model2.clear();
                    }
                    if (tran.betValues[3] >= 2000L && existCa) {
                        ++model2.soCa;
                        model2.addNewPhien(tran.referenceId);
                        final ToiChonCaModel toiChonCaModel4;
                        final ToiChonCaModel toiChonCaModel2 = toiChonCaModel4 = model2;
                        toiChonCaModel4.tongDat += tran.betValues[3];
                        final ToiChonCaModel toiChonCaModel5;
                        final ToiChonCaModel toiChonCaModel3 = toiChonCaModel5 = model2;
                        toiChonCaModel5.tongThang += tran.prizes[3];
                        if (!model2.valid && tran.betValues[3] >= 5000L) {
                            model2.valid = true;
                        }
                        if (model2.valid && model2.soCa > model2.soCaHighScore) {
                            this.newHighScoreToiChonCa(model2);
                        }
                    } else {
                        model2.clear();
                    }
                    map.put(tran.username, model2);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    map.unlock(tran.username);
                }
            }
            if (tran.betValues[3] >= 2000L) {
                if (!existCa) {
                    continue;
                }
                final ToiChonCaModel model2 = new ToiChonCaModel(tran.username, tran.referenceId, tran.betValues[3], tran.prizes[3]);
                model2.soCaHighScore = this.dao.getHighScore(tran.username);
                if (tran.betValues[3] >= 5000L) {
                    model2.valid = true;
                    if (model2.soCa > model2.soCaHighScore) {
                        this.newHighScoreToiChonCa(model2);
                    }
                }
                map.put(tran.username, model2);
            }
        }
    }

    private void newHighScoreToiChonCa(final ToiChonCaModel model) throws IOException, TimeoutException, InterruptedException {
        model.soCaHighScore = model.soCa;
        final ToiChonCaMsg msg = new ToiChonCaMsg();
        msg.username = model.username;
        msg.soCa = model.soCa;
        msg.soVan = model.soVan;
        msg.tongDat = model.tongDat;
        msg.tongThang = model.tongThang;
        msg.currentPhien = model.currentPhien;
        msg.listPhien = model.getListPhien();
        RMQApi.publishMessage("queue_baucua", msg, 133);
    }

    @Override
    public List<ResultBauCua> getLichSuPhien(final int size, final byte room) {
        return this.dao.getLichSuPhien(size, room);
    }

    @Override
    public List<ToiChonCa> getTopToiChonCa(final String startTime, final String endTime) {
        return this.dao.getTopToiChonCa(startTime, endTime);
    }
}
