// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.entities.agent;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TopVippointResponse {
    private String nick_name;
    private String mobile;
    private int vip_point;
    private long money;
    private int dai_ly;

    public String getNick_name() {
        return this.nick_name;
    }

    public void setNick_name(final String nick_name) {
        this.nick_name = nick_name;
    }

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(final String mobile) {
        this.mobile = mobile;
    }

    public int getVip_point() {
        return this.vip_point;
    }

    public void setVip_point(final int vip_point) {
        this.vip_point = vip_point;
    }

    public long getMoney() {
        return this.money;
    }

    public void setMoney(final long money) {
        this.money = money;
    }

    public int getDai_ly() {
        return this.dai_ly;
    }

    public void setDai_ly(final int dai_ly) {
        this.dai_ly = dai_ly;
    }

    public String toJson() {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException mapper2) {
            return "{\"success\":false,\"errorCode\":\"1001\"}";
        }
    }
}
