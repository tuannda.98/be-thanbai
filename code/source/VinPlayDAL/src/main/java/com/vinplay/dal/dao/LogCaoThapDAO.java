// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao;

import com.vinplay.vbee.common.response.LogCaoThapResponse;

import java.util.List;

public interface LogCaoThapDAO {
    List<LogCaoThapResponse> listCaoThap(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5, final int p6);

    int countCaoThap(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5);
}
