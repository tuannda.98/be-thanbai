package com.vinplay.dal.service.impl;

import casio.king365.core.HCMap;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.vinplay.dal.service.CacheService;
import com.vinplay.vbee.common.exceptions.KeyNotFoundException;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class CacheServiceImpl implements CacheService {
    @Override
    public void setValue(final String key, final String value) {
        final IMap<String, String> map = HCMap.getCachedConfig();
        map.put(key, value);
    }

    @Override
    public void setValue(final String key, final String value, int time, TimeUnit timeUnit) {
        final IMap<String, String> map = HCMap.getCachedConfig();
        map.put(key, value, time, timeUnit);
    }

    @Override
    public void setValue(final String key, final int value) {
        final HazelcastInstance instance = HazelcastClientFactory.getInstance();
        final IMap<String, String> map = HCMap.getCachedConfig();
        map.put(key, String.valueOf(value));
    }

    @Override
    public String getValueStr(final String key) throws KeyNotFoundException {
        final HazelcastInstance instance = HazelcastClientFactory.getInstance();
        final IMap<String, String> map = HCMap.getCachedConfig();
        if (map.containsKey(key)) {
            return map.get(key);
        }
        throw new KeyNotFoundException();
    }

    @Override
    public int getValueInt(final String key) throws KeyNotFoundException, NumberFormatException {
        final HazelcastInstance instance = HazelcastClientFactory.getInstance();
        final IMap<String, String> map = HCMap.getCachedConfig();
        if (map.containsKey(key)) {
            return Integer.parseInt(map.get(key));
        }
        throw new KeyNotFoundException();
    }

    @Override
    public boolean removeKey(final String key) throws KeyNotFoundException {
        final HazelcastInstance instance = HazelcastClientFactory.getInstance();
        final IMap<String, String> map = HCMap.getCachedConfig();
        if (map.containsKey(key)) {
            map.remove(key);
            return true;
        }
        throw new KeyNotFoundException();
    }

    @Override
    public boolean checkKeyExist(String key) {
        final HazelcastInstance instance = HazelcastClientFactory.getInstance();
        final IMap<String, String> map = HCMap.getCachedConfig();
        if (map.containsKey(key))
            return true;
        return false;
    }

    @Override
    public void setObject(final String key, final Object obj) {
        final HazelcastInstance instance = HazelcastClientFactory.getInstance();
        final IMap map = instance.getMap("cacheGameBai");
        map.put(key, obj);
    }

    @Override
    public Object getObject(final String key) throws KeyNotFoundException {
        final HazelcastInstance instance = HazelcastClientFactory.getInstance();
        final IMap map = instance.getMap("cacheGameBai");
        if (map.containsKey(key)) {
            return map.get(key);
        }
        throw new KeyNotFoundException();
    }

    @Override
    public Object removeObject(final String key) throws KeyNotFoundException {
        final HazelcastInstance instance = HazelcastClientFactory.getInstance();
        final IMap map = instance.getMap("cacheGameBai");
        if (map.containsKey(key)) {
            return map.remove(key);
        }
        throw new KeyNotFoundException();
    }

    @Override
    public Map<String, Object> getBulk(final Set<String> keys) {
        final HazelcastInstance instance = HazelcastClientFactory.getInstance();
        final IMap map = instance.getMap("cacheGameBai");
        return (Map<String, Object>) map.getAll(keys);
    }

    @Override
    public void setObject(final String key, final int expireTime, final Object obj) {
        final HazelcastInstance instance = HazelcastClientFactory.getInstance();
        final IMap map = instance.getMap("cacheGameBai");
        map.put(key, obj, expireTime, TimeUnit.SECONDS);
    }
}
