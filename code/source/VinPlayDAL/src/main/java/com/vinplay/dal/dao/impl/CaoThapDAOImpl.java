// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import com.vinplay.dal.dao.CaoThapDAO;
import com.vinplay.dal.entities.caothap.LSGDCaoThap;
import com.vinplay.dal.entities.caothap.TopCaoThap;
import com.vinplay.dal.entities.caothap.VinhDanhCaoThap;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.pools.ConnectionPool;
import org.bson.Document;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CaoThapDAOImpl implements CaoThapDAO {
    @Override
    public long[] getPotCaoThap(final String potName) throws SQLException {
        final ArrayList<Long> result = new ArrayList<>();
        final String sql = "SELECT value FROM minigame_pots WHERE minigame_pots.pot_name like '" + potName + "%'";
        try (final Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_minigame");
             final PreparedStatement stmt = conn.prepareStatement(sql);
             final ResultSet rs = stmt.executeQuery();) {
            while (rs.next()) {
                result.add(rs.getLong("value"));
            }
        }
        final long[] arr = new long[result.size()];
        for (int i = 0; i < result.size(); ++i) {
            arr[i] = result.get(i);
        }
        return arr;
    }

    @Override
    public long[] getFundCaoThap(final String fundName) throws SQLException {
        final ArrayList<Long> result = new ArrayList<>();
        final String sql = "SELECT value FROM minigame_funds WHERE minigame_funds.fund_name like '" + fundName + "%'";
        try (final Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_minigame");
             final PreparedStatement stmt = conn.prepareStatement(sql);
             final ResultSet rs = stmt.executeQuery();) {
            while (rs.next()) {
                result.add(rs.getLong("value"));
            }
        }
        final long[] arr = new long[result.size()];
        for (int i = 0; i < result.size(); ++i) {
            arr[i] = result.get(i);
        }
        return arr;
    }

    @Override
    public int countLichSuGiaoDich(final String nickname, final int moneyType) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final Document conditions = new Document();
        conditions.put("nick_name", nickname);
        conditions.put("money_type", moneyType);
        final long totalRows = db.getCollection("log_cao_thap").count(conditions);
        return (int) totalRows;
    }

    @Override
    public List<LSGDCaoThap> getLichSuGiaoDich(final String nickname, final int pageNumber, final int moneyType) {
        final int pageSize = 10;
        final int skipNumber = (pageNumber - 1) * 10;
        final ArrayList<LSGDCaoThap> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        FindIterable iterable = null;
        final Document conditions = new Document();
        conditions.put("nick_name", nickname);
        conditions.put("money_type", moneyType);
        final BasicDBObject sortCondtions = new BasicDBObject();
        sortCondtions.put("trans_id", -1);
        sortCondtions.put("_id", -1);
        iterable = db.getCollection("log_cao_thap").find(conditions).sort(sortCondtions).skip(skipNumber).limit(10);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final LSGDCaoThap entry = new LSGDCaoThap();
                entry.transId = document.getLong("trans_id");
                entry.potBet = document.getInteger("pot_bet");
                entry.step = document.getInteger("step");
                entry.betValue = document.getLong("bet_value");
                entry.prize = document.getLong("prize");
                entry.cards = document.getString("cards");
                entry.timestamp = document.getString("time_log");
                results.add(entry);
            }
        });
        return results;
    }

    @Override
    public List<VinhDanhCaoThap> getBangVinhDanh(final int pageNumber, final int moneyType) {
        final int pageSize = 10;
        final int skipNumber = (pageNumber - 1) * 10;
        final ArrayList<VinhDanhCaoThap> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        FindIterable iterable = null;
        final Document conditions = new Document();
        conditions.put("money_type", moneyType);
        conditions.put("money_win", new BasicDBObject("$gt", 0));
        final BasicDBObject query1 = new BasicDBObject("prize", new BasicDBObject("$gte", 100000));
        final BasicDBObject query2 = new BasicDBObject("result", 7);
        final ArrayList<BasicDBObject> myList = new ArrayList<>();
        myList.add(query1);
        myList.add(query2);
        conditions.put("$or", myList);
        final BasicDBObject sortCondtions = new BasicDBObject();
        sortCondtions.put("_id", -1);
        iterable = db.getCollection("log_cao_thap_win").find(conditions).sort(sortCondtions).skip(skipNumber).limit(10);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final VinhDanhCaoThap entry = new VinhDanhCaoThap();
                entry.nickname = document.getString("nick_name");
                entry.betValue = document.getLong("bet_value");
                entry.prize = document.getLong("prize");
                entry.result = document.getInteger("result");
                entry.timestamp = document.getString("time_log");
                results.add(entry);
            }
        });
        return results;
    }

    @Override
    public int countVinhDanh(final int moneyType) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final Document conditions = new Document();
        conditions.put("money_type", moneyType);
        conditions.put("money_win", new BasicDBObject("$gt", 0));
        final long totalRows = db.getCollection("log_cao_thap_win").count(conditions);
        return (int) totalRows;
    }

    @Override
    public long getLastReferenceId() {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap conditions = new HashMap();
        final BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        final FindIterable iterable = db.getCollection("log_cao_thap").find(new Document(conditions)).sort(objsort).limit(1);
        final Document document = (iterable != null) ? ((Document) iterable.first()) : null;
        return (document == null) ? 0L : document.getLong("trans_id");
    }

    @Override
    public List<TopCaoThap> getTop(final String startTime, final String endTime) {
        final ArrayList<TopCaoThap> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        FindIterable iterable = null;
        final Document conditions = new Document();
        conditions.put("money_type", 1);
        conditions.put("group_type", new BasicDBObject("$gte", 0));
        final BasicDBObject obj = new BasicDBObject();
        obj.put("$gte", startTime);
        obj.put("$lte", endTime);
        conditions.put("time_log", obj);
        final BasicDBObject sortCondtions = new BasicDBObject();
        sortCondtions.put("group_type", -1);
        sortCondtions.put("rank_1", -1);
        sortCondtions.put("rank_2", -1);
        sortCondtions.put("rank_3", -1);
        sortCondtions.put("rank_4", -1);
        sortCondtions.put("rank_5", -1);
        sortCondtions.put("prize", -1);
        sortCondtions.put("_id", -1);
        iterable = db.getCollection("log_cao_thap_win").find(conditions).sort(sortCondtions).limit(10);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final TopCaoThap entry = new TopCaoThap();
                entry.nickname = document.getString("nick_name");
                entry.hand = document.getString("hand");
                entry.money = document.getLong("prize");
                entry.timestamp = document.getString("time_log");
                results.add(entry);
            }
        });
        return results;
    }
}
