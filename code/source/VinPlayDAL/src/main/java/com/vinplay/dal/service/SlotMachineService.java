// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service;

import com.vinplay.vbee.common.models.cache.SlotFreeDaily;
import com.vinplay.vbee.common.models.minigame.pokego.LSGDPokeGo;
import com.vinplay.vbee.common.models.minigame.pokego.TopPokeGo;
import com.vinplay.vbee.common.models.slot.NoHuModel;
import com.vinplay.vbee.common.models.slot.SlotFreeSpin;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeoutException;

public interface SlotMachineService {
    void logKhoBau(final long p0, final String p1, final long p2, final String p3, final String p4, final String p5, final short p6, final long p7, final String p8) throws IOException, TimeoutException, InterruptedException;

    void logAvengers(final long p0, final String p1, final long p2, final String p3, final String p4, final String p5, final short p6, final long p7, final String p8) throws IOException, TimeoutException, InterruptedException;

    void logMyNhanNgu(final long p0, final String p1, final long p2, final String p3, final String p4, final String p5, final short p6, final long p7, final String p8) throws IOException, TimeoutException, InterruptedException;

    void logNuDiepVien(final long p0, final String p1, final long p2, final String p3, final String p4, final String p5, final short p6, final long p7, final String p8) throws IOException, TimeoutException, InterruptedException;

    void logVQV(final long p0, final String p1, final long p2, final String p3, final String p4, final String p5, final short p6, final long p7, final String p8) throws IOException, TimeoutException, InterruptedException;

    int countLSDG(final String p0, final String p1);

    List<LSGDPokeGo> getLSGD(final String p0, final String p1, final int p2);

    void addTop(final String p0, final String p1, final int p2, final long p3, final String p4, final int p5);

    List<TopPokeGo> getTopSlotMachine(final String p0, final int p1);

    long getLastReferenceId(final String p0);

    SlotFreeSpin updateLuotQuaySlotFree(final String p0, final String p1);

    void setLuotQuayFreeSlot(final String p0, final String p1, final String p2, final int p3, final int p4);

    SlotFreeSpin getLuotQuayFreeSlot(final String p0, final String p1);

    void setItemsWild(final String p0, final String p1, final String p2);

    int getPrizes(final String p0, final String p1);

    void addPrizes(final String p0, final String p1, final int p2);

    SlotFreeDaily getLuotQuayFreeDaily(final String p0, final String p1, final int p2);

    boolean updateLuotQuayFreeDaily(final String p0, final String p1, final int p2);

    void logNoHu(final long p0, final String p1, final String p2, final int p3, final String p4, final String p5, final String p6, final String p7, final long p8, final short p9, final String p10);

    List<NoHuModel> getLogNoHu(final String p0, final int p1);
}
