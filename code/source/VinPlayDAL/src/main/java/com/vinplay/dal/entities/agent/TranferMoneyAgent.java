// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.entities.agent;

public class TranferMoneyAgent {
    private String nicknameSend;
    private String nicknameReceive;
    private long moneySend;
    private long moneyReceive;
    private int status;

    public TranferMoneyAgent() {
    }

    public TranferMoneyAgent(final String nicknameSend, final String nicknameReceive, final long moneySend, final long moneyReceive, final int status) {
        this.nicknameSend = nicknameSend;
        this.nicknameReceive = nicknameReceive;
        this.moneySend = moneySend;
        this.moneyReceive = moneyReceive;
        this.status = status;
    }

    public String getNicknameSend() {
        return this.nicknameSend;
    }

    public void setNicknameSend(final String nicknameSend) {
        this.nicknameSend = nicknameSend;
    }

    public String getNicknameReceive() {
        return this.nicknameReceive;
    }

    public void setNicknameReceive(final String nicknameReceive) {
        this.nicknameReceive = nicknameReceive;
    }

    public long getMoneySend() {
        return this.moneySend;
    }

    public void setMoneySend(final long moneySend) {
        this.moneySend = moneySend;
    }

    public long getMoneyReceive() {
        return this.moneyReceive;
    }

    public void setMoneyReceive(final long moneyReceive) {
        this.moneyReceive = moneyReceive;
    }

    public int getStatus() {
        return this.status;
    }

    public void setStatus(final int status) {
        this.status = status;
    }
}
