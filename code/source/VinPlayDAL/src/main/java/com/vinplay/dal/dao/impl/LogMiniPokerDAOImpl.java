// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import com.vinplay.dal.dao.LogMiniPokerDAO;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.response.MiniPokerResponse;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LogMiniPokerDAOImpl implements LogMiniPokerDAO {
    @Override
    public List<MiniPokerResponse> listMiniPoker(final String nickName, final String bet_value, final String timeStart, final String timeEnd, final String moneyType, final int page) {
        final ArrayList<MiniPokerResponse> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        FindIterable iterable = null;
        final BasicDBObject obj = new BasicDBObject();
        final int num_start = (page - 1) * 50;
        final int num_end = 50;
        if (nickName != null && !nickName.equals("")) {
            final String pattern = ".*" + nickName + ".*";
            conditions.put("user_name", new BasicDBObject().append("$regex", pattern).append("$options", "i"));
        }
        if (bet_value != null && !bet_value.equals("")) {
            conditions.put("bet_value", Long.parseLong(bet_value));
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("time_log", obj);
        }
        if (moneyType != null && !moneyType.equals("")) {
            conditions.put("money_type", Integer.parseInt(moneyType));
        }
        iterable = db.getCollection("log_mini_poker").find(new Document(conditions)).skip(num_start).limit(50);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final MiniPokerResponse minipoker = new MiniPokerResponse();
                minipoker.user_name = document.getString("user_name");
                minipoker.bet_value = document.getLong("bet_value");
                minipoker.result = document.getInteger("result");
                minipoker.prize = document.getLong("prize");
                minipoker.cards = document.getString("cards");
                minipoker.current_pot = document.getLong("current_pot");
                minipoker.current_fund = document.getLong("current_fund");
                minipoker.money_type = document.getInteger("money_type");
                minipoker.time_log = document.getString("time_log");
                results.add(minipoker);
            }
        });
        return results;
    }

    @Override
    public int countMiniPoker(final String nickName, final String bet_value, final String timeStart, final String timeEnd, final String moneyType) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final BasicDBObject obj = new BasicDBObject();
        final HashMap<String, Object> conditions = new HashMap<>();
        if (nickName != null && !nickName.equals("")) {
            final String pattern = ".*" + nickName + ".*";
            conditions.put("user_name", new BasicDBObject().append("$regex", pattern).append("$options", "i"));
        }
        if (bet_value != null && !bet_value.equals("")) {
            conditions.put("bet_value", Long.parseLong(bet_value));
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            obj.put("$gte", timeStart + " 00:00:00");
            obj.put("$lte", timeEnd + " 23:59:59");
            conditions.put("time_log", obj);
        }
        if (moneyType != null && !moneyType.equals("")) {
            conditions.put("money_type", Integer.parseInt(moneyType));
        }
        return (int) db.getCollection("log_mini_poker").count(new Document(conditions));
    }
}
