// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.entities.report;

public class ReportTXModel {
    public long moneyWin;
    public long moneyLost;
    public long moneyRefund;
    public long moneyOther;
    public long fee;
    public long revenuePlayGame;
    public long revenue;
}
