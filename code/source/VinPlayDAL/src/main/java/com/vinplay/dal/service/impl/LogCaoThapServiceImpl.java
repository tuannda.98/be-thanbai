// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.vinplay.dal.dao.impl.LogCaoThapDAOImpl;
import com.vinplay.dal.service.LogCaoThapService;
import com.vinplay.vbee.common.response.LogCaoThapResponse;

import java.util.List;

public class LogCaoThapServiceImpl implements LogCaoThapService {
    @Override
    public List<LogCaoThapResponse> listCaoThap(final String nickName, final String transId, final String bet_value, final String timeStart, final String timeEnd, final String moneyType, final int page) {
        final LogCaoThapDAOImpl dao = new LogCaoThapDAOImpl();
        return dao.listCaoThap(nickName, transId, bet_value, timeStart, timeEnd, moneyType, page);
    }

    @Override
    public int countCaoThap(final String nickName, final String transId, final String bet_value, final String timeStart, final String timeEnd, final String moneyType) {
        final LogCaoThapDAOImpl dao = new LogCaoThapDAOImpl();
        return dao.countCaoThap(nickName, transId, bet_value, timeStart, timeEnd, moneyType);
    }
}
