// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao;

import com.vinplay.vbee.common.models.PotModel;

import java.sql.SQLException;
import java.util.List;

public interface PotDao {
    List<PotModel> getAll() throws SQLException;
}
