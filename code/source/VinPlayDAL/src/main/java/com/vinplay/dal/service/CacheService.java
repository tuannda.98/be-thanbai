// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service;

import com.vinplay.vbee.common.exceptions.KeyNotFoundException;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public interface CacheService {
    void setValue(final String p0, final String p1);

    void setValue(final String p0, final String p1, int time, TimeUnit timeUnit);

    void setValue(final String p0, final int p1);

    String getValueStr(final String p0) throws KeyNotFoundException;

    int getValueInt(final String p0) throws KeyNotFoundException, NumberFormatException;

    boolean removeKey(final String p0) throws KeyNotFoundException;

    boolean checkKeyExist(String key);

    void setObject(final String p0, final Object p1);

    void setObject(final String p0, final int p1, final Object p2);

    Object getObject(final String p0) throws KeyNotFoundException;

    Object removeObject(final String p0) throws KeyNotFoundException;

    Map<String, Object> getBulk(final Set<String> p0);
}
