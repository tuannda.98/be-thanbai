package com.vinplay.dal.dao.impl;

import casio.king365.util.KingUtil;
import com.vinplay.dal.dao.GetCashoutRequestIndexDAO;
import com.vinplay.vbee.common.pools.ConnectionPool;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class GetCashoutRequestIndexDAOImpl implements GetCashoutRequestIndexDAO {
    @Override
    public JSONObject getByDateWithClientPlatform(String timeStart, String timeEnd) throws SQLException {
        JSONObject res = new JSONObject();
        String sql = "SELECT sum(ci.money) as total, cr.client, cr.platform\n" +
                " FROM `cashout_request` cr \n" +
                " LEFT JOIN `cashout_items` ci ON cr.item_id = ci.id\n" +
                " WHERE cr.status = 2 AND reques_time BETWEEN ? AND ?\n" +
                " GROUP BY cr.client, cr.platform" +
                " ORDER BY cr.client,cr.platform DESC";
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stmt = conn.prepareStatement(sql);) {
            stmt.setString(1, timeStart);
            stmt.setString(2, timeEnd);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    String client = rs.getString("client");
                    if(client == null)
                        client = "null";
                    String platform = rs.getString("platform");
                    if(platform == null)
                        platform = "null";
                    int number = rs.getInt("total");
                    KingUtil.printLog("getByDateWithClientPlatform() client: "+client+", number: "+number);
                    res.put(client + "-" + platform, number);
                }
            } catch (JSONException e) {
                KingUtil.printException("GetCashoutRequestIndexDAOImpl getByDateWithClientPlatform()", e);
            }
        }
        return res;
    }
}
