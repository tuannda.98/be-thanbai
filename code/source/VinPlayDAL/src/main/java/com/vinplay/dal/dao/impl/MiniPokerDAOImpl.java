// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import com.vinplay.dal.dao.MiniPokerDAO;
import com.vinplay.dal.entities.minipoker.LSGDMiniPoker;
import com.vinplay.dal.entities.minipoker.VinhDanhMiniPoker;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MiniPokerDAOImpl implements MiniPokerDAO {
    @Override
    public List<LSGDMiniPoker> getLichSuGiaoDich(final String username, final int pageNumber, final int moneyType) {
        final int pageSize = 10;
        final int skipNumber = (pageNumber - 1) * 10;
        final ArrayList<LSGDMiniPoker> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        FindIterable iterable = null;
        final Document conditions = new Document();
        conditions.put("user_name", username);
        conditions.put("money_type", moneyType);
        iterable = db.getCollection("log_mini_poker").find(conditions).skip(skipNumber).limit(10).sort(new Document("_id",-1));
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final LSGDMiniPoker entry = new LSGDMiniPoker();
                entry.username = document.getString("user_name");
                entry.betValue = document.getLong("bet_value");
                entry.prize = document.getLong("prize");
                entry.cards = document.getString("cards");
                entry.timestamp = document.getString("time_log");
                results.add(entry);
            }
        });
        return results;
    }

    @Override
    public List<VinhDanhMiniPoker> getBangVinhDanh(final int moneyType, final int page) {
        final int pageSize = 10;
        final int skipNumber = (page - 1) * 10;
        final ArrayList<VinhDanhMiniPoker> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        FindIterable iterable = null;
        final Document conditions = new Document();
        conditions.put("$or", Arrays.asList(new Document("result", 1), new Document("result", 2), new Document("result", 12)));
        conditions.put("money_type", moneyType);
        final BasicDBObject sortCondtions = new BasicDBObject();
        sortCondtions.put("_id", -1);
        iterable = db.getCollection("log_mini_poker").find(conditions).sort(sortCondtions).skip(skipNumber).limit(10);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final VinhDanhMiniPoker entry = new VinhDanhMiniPoker();
                entry.username = document.getString("user_name");
                entry.betValue = document.getLong("bet_value");
                entry.prize = document.getLong("prize");
                entry.result = document.getInteger("result");
                entry.timestamp = document.getString("time_log");
                results.add(entry);
            }
        });
        return results;
    }

    @Override
    public int countLichSuGiaoDich(final String username, final int moneyType) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final Document conditions = new Document();
        conditions.put("user_name", username);
        conditions.put("money_type", moneyType);
        final long totalRows = db.getCollection("log_mini_poker").count(conditions);
        return (int) totalRows;
    }
}
