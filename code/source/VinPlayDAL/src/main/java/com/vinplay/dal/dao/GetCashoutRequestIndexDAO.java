package com.vinplay.dal.dao;

import org.json.JSONObject;

import java.sql.SQLException;

public interface GetCashoutRequestIndexDAO {
    JSONObject getByDateWithClientPlatform(final String timeStart, final String timeEnd) throws SQLException;
}
