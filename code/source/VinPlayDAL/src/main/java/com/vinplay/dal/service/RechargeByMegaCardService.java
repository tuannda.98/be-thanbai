// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service;

import com.vinplay.vbee.common.response.MoneyTotalRechargeByCardReponse;
import com.vinplay.vbee.common.response.megacard.MegaCardResponse;

import java.util.List;

public interface RechargeByMegaCardService {
    List<MegaCardResponse> searchRechargeByMegaCard(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5, final String p6, final int p7, final String p8);

    int countSearchRechargeByMegaCard(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5, final String p6, final String p7);

    long moneyTotal(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5, final String p6, final String p7);

    List<MoneyTotalRechargeByCardReponse> moneyTotalRechargeByMegaCard(final String p0, final String p1, final String p2);
}
