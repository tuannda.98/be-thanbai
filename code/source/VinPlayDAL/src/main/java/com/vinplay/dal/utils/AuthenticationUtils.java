// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.utils;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.utils.VinPlayUtils;

public class AuthenticationUtils {
    public static boolean decodeBaseAuthen(final String dataEncryt) {
        boolean response = false;
        final String[] dataSplit = VinPlayUtils.decodeBase64(dataEncryt).split("\\|");
        final String accessToken = dataSplit[0];
        final String nickName = dataSplit[1];
        final String key = dataSplit[2];
        final HazelcastInstance client = HazelcastClientFactory.getInstance();
        final IMap<String, UserModel> userMap = client.getMap("users");
        final UserCacheModel user;
        if (userMap.containsKey(nickName) && (user = (UserCacheModel) userMap.get(nickName)).getAccessToken().equals(accessToken) && "fU3z7wP0IeFOPntKXcRifUDTGbV8AXyI".equals(key)) {
            response = true;
        }
        return response;
    }
}
