// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.entities.report;

public class ReportTotalMoneyModel {
    public long moneyBot;
    public long moneyUser;
    public long moneyAgent1;
    public long moneyAgent2;
    public long moneySuperAgent;
    public long total;
    public String timeLog;

    public ReportTotalMoneyModel(final long moneyBot, final long moneyUser, final long moneyAgent1, final long moneyAgent2, final long moneySuperAgent, final long total, final String timeLog) {
        this.moneyBot = moneyBot;
        this.moneyUser = moneyUser;
        this.moneyAgent1 = moneyAgent1;
        this.moneyAgent2 = moneyAgent2;
        this.moneySuperAgent = moneySuperAgent;
        this.total = total;
        this.timeLog = timeLog;
    }

    public ReportTotalMoneyModel() {
    }
}
