// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.entities.report;

public class ReportMoneySystemModel {
    public long moneyWin;
    public long moneyLost;
    public long moneyOther;
    public long fee;
    public long revenuePlayGame;
    public long revenue;
    public String dateReset;

    public ReportMoneySystemModel() {
        this.moneyWin = 0L;
        this.moneyLost = 0L;
        this.moneyOther = 0L;
        this.fee = 0L;
        this.revenuePlayGame = 0L;
        this.revenue = 0L;
    }
}
