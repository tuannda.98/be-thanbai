/*
 * Decompiled with CFR 0.144.
 *
 * Could not load the following classes:
 *  com.vinplay.vbee.common.response.BonusFundResponse
 */
package com.vinplay.dal.dao;

import com.vinplay.dal.entities.gamebai.FundPotCache;
import com.vinplay.vbee.common.response.BonusFundResponse;

import java.sql.SQLException;
import java.util.List;

public interface MiniGameDAO {
    long getReferenceId(int var1) throws SQLException;

    long getPot(String var1) throws SQLException;

    long[] getPots(String var1) throws SQLException;

    long getFund(String var1) throws SQLException;

    long[] getFunds(String var1) throws SQLException;

    List<BonusFundResponse> getFunds() throws SQLException;

    boolean saveReferenceId(long var1, int var3) throws SQLException;

    List<FundPotCache> GetFunds() throws SQLException;

    List<FundPotCache> GetPots() throws SQLException;

    boolean UpdatePot(String key, long value) throws SQLException;

    boolean UpdateFund(String key, long value) throws SQLException;
}