package com.vinplay.dal.dao.impl;

import casio.king365.entities.X3NapLanDau;
import casio.king365.service.MongoDbService;
import casio.king365.service.impl.MongoDbServiceImpl;
import casio.king365.util.KingUtil;
import com.vinplay.dal.dao.X3NapLanDauDAO;
import org.bson.Document;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class X3NapLanDauDAOImpl implements X3NapLanDauDAO {

    private MongoDbService mongodbService = new MongoDbServiceImpl();

    @Override
    public X3NapLanDau getX3NapLanDau(String nickname, String channel) {
        Map<String, Object> findMap = new HashMap<>();
        findMap.put("nickname", nickname);
        findMap.put("channel", channel);
        List<Document> result = mongodbService.find("x3naplandau", findMap);
        if (result.size() == 0)
            return null;
        else
            return X3NapLanDau.fromDocument(result.get(0));
    }

    @Override
    public List<Document> getX3NapLanDau(String nickname) {
        Map<String, Object> findMap = new HashMap<>();
        findMap.put("nickname", nickname);

        return mongodbService.find("x3naplandau", findMap);
    }

    @Override
    public void createX3NapLanDau(X3NapLanDau obj) {
        KingUtil.printLog("create X3NapLanDau for user: " + obj);
        mongodbService.insert("x3naplandau", obj.toMap());
    }

    @Override
    public void updatePercent(String nickname, String channel, Double percent) {
        mongodbService.update("x3naplandau",
                new Document("nickname", nickname).append("channel", channel), new Document("percent", percent));
    }

    @Override
    public void updateReceivedBonus(String nickName, String channel, boolean newReceBonus) {
        mongodbService.update("x3naplandau",
                new Document("nickname", nickName).append("channel", channel), new Document("receivedBonus", newReceBonus));
    }

    @Override
    public void updateActiveTime(String nickName, String channel, long newActiveTime) {
        mongodbService.update("x3naplandau",
                new Document("nickname", nickName).append("channel", channel), new Document("startActiveTime", newActiveTime));
    }
}
