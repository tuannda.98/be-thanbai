// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.vinplay.dal.dao.impl.LogSlotDAOImpl;
import com.vinplay.dal.service.LogSlotService;
import com.vinplay.vbee.common.response.SlotResponse;

import java.sql.SQLException;
import java.util.List;

public class LogSlotServiceImpl implements LogSlotService {
    @Override
    public List<SlotResponse> listLogSlot(final String referentId, final String userName, final String betValue, final String timeStart, final String timeEnd, final int page, final String gameName) {
        final LogSlotDAOImpl dao = new LogSlotDAOImpl();
        return dao.listLogSlot(referentId, userName, betValue, timeStart, timeEnd, page, gameName);
    }

    @Override
    public long countLogKhoBau(final String referentId, final String userName, final String betValue, final String timeStart, final String timeEnd) {
        final LogSlotDAOImpl dao = new LogSlotDAOImpl();
        return dao.countLogKhoBau(referentId, userName, betValue, timeStart, timeEnd);
    }
}
