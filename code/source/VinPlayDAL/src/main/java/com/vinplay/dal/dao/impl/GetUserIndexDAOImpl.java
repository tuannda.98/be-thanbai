package com.vinplay.dal.dao.impl;

import casio.king365.util.KingUtil;
import com.vinplay.dal.dao.GetUserIndexDAO;
import com.vinplay.vbee.common.pools.ConnectionPool;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class GetUserIndexDAOImpl implements GetUserIndexDAO {
    @Override
    public int getRegister(final String timeStart, final String timeEnd) throws SQLException {
        int res = 0;
        String sql = " SELECT COUNT(*) AS total  FROM vinplay.users  WHERE `create_time` >= ?    AND `create_time` <= ? ";
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stmt = conn.prepareStatement(sql);) {
            stmt.setString(1, timeStart);
            stmt.setString(2, timeEnd);
            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    res = rs.getInt("total");
                }
            }
        }
        return res;
    }

    @Override
    public int getRecharge(final String timeStart, final String timeEnd) throws SQLException {
        int res = 0;
        String sql = " SELECT COUNT(*) AS total  FROM vinplay.users  WHERE `create_time` >= ?    AND `create_time` <= ?    AND recharge_money > 0 ";
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stmt = conn.prepareStatement(sql);) {
            stmt.setString(1, timeStart);
            stmt.setString(2, timeEnd);
            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    res = rs.getInt("total");
                }
            }
        }
        return res;
    }

    @Override
    public int getSecMobile(final String timeStart, final String timeEnd) throws SQLException {
        int res = 0;
        String sql = " SELECT COUNT(*) AS total  FROM vinplay.users  WHERE `create_time` >= ?    AND `create_time` <= ?    AND (`status` & 16) ";
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stmt = conn.prepareStatement(sql);) {
            stmt.setString(1, timeStart);
            stmt.setString(2, timeEnd);
            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    res = rs.getInt("total");
                }
            }
        }
        return res;
    }

    @Override
    public int getBoth(final String timeStart, final String timeEnd) throws SQLException {
        int res = 0;
        String sql = " SELECT COUNT(*) AS total  FROM vinplay.users  WHERE `create_time` >= ?    AND `create_time` <= ?    AND recharge_money > 0    AND (`status` & 16) ";
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stmt = conn.prepareStatement(sql);) {
            stmt.setString(1, timeStart);
            stmt.setString(2, timeEnd);
            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    res = rs.getInt("total");
                }
            }
        }
        return res;
    }

    @Override
    public JSONObject getRegisterWithPlatform(String timeStart, String timeEnd) throws SQLException {
        JSONObject res = new JSONObject();
        String sql = " SELECT platform, COUNT(*) AS total FROM vinplay.users " +
                "WHERE `create_time` >= ? AND `create_time` <= ? GROUP BY platform";
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stmt = conn.prepareStatement(sql);) {
            stmt.setString(1, timeStart);
            stmt.setString(2, timeEnd);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    String platform = rs.getString("platform");
                    if(platform == null)
                        platform = "null";
                    int number = rs.getInt("total");
                    KingUtil.printLog("getRegisterWithPlatform() platform: "+platform+", number: "+number);
                    res.put(platform, number);
                }
            } catch (JSONException e) {
                KingUtil.printException("GetUserIndexDAOImpl getRegisterWithPlatform()", e);
            }
        }
        return res;
    }

    @Override
    public JSONObject getRechargeWithPlatform(String timeStart, String timeEnd) throws SQLException {
        JSONObject res = new JSONObject();
        String sql = " SELECT platform, COUNT(*) AS total FROM vinplay.users " +
                "WHERE `create_time` >= ? AND `create_time` <= ? AND recharge_money > 0 GROUP BY platform";
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stmt = conn.prepareStatement(sql);) {
            stmt.setString(1, timeStart);
            stmt.setString(2, timeEnd);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    String platform = rs.getString("platform");
                    if(platform == null)
                        platform = "null";
                    int number = rs.getInt("total");
                    res.put(platform, number);
                }
            } catch (JSONException e) {
                KingUtil.printException("GetUserIndexDAOImpl getRechargeWithPlatform()", e);
            }
        }
        return res;
    }

    @Override
    public JSONObject getSecMobileWithPlatform(String timeStart, String timeEnd) throws SQLException {
        JSONObject res = new JSONObject();
        String sql = " SELECT platform, COUNT(*) AS total FROM vinplay.users " +
                "WHERE `create_time` >= ? AND `create_time` <= ? AND (`status` & 16) GROUP BY platform";
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stmt = conn.prepareStatement(sql);) {
            stmt.setString(1, timeStart);
            stmt.setString(2, timeEnd);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    String platform = rs.getString("platform");
                    if(platform == null)
                        platform = "null";
                    int number = rs.getInt("total");
                    res.put(platform, number);
                }
            } catch (JSONException e) {
                KingUtil.printException("getSecMobileWithPlatform getRegisterWithPlatform()", e);
            }
        }
        return res;
    }

    @Override
    public JSONObject getBothWithPlatform(String timeStart, String timeEnd) throws SQLException {
        JSONObject res = new JSONObject();
        String sql = " SELECT platform, COUNT(*) AS total FROM vinplay.users " +
                "WHERE `create_time` >= ? AND `create_time` <= ? AND recharge_money > 0 AND (`status` & 16) " +
                "GROUP BY platform";
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stmt = conn.prepareStatement(sql);) {
            stmt.setString(1, timeStart);
            stmt.setString(2, timeEnd);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    String platform = rs.getString("platform");
                    if(platform == null)
                        platform = "null";
                    int number = rs.getInt("total");
                    res.put(platform, number);
                }
            } catch (JSONException e) {
                KingUtil.printException("GetUserIndexDAOImpl getRegisterWithPlatform()", e);
            }
        }
        return res;
    }

    @Override
    public JSONObject getRegisterWithClient(final String timeStart, final String timeEnd) throws SQLException {
        JSONObject res = new JSONObject();
        String sql = " SELECT client, platform, COUNT(*) AS total FROM vinplay.users " +
                "WHERE `create_time` >= ? AND `create_time` <= ? GROUP BY client, platform";
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stmt = conn.prepareStatement(sql);) {
            stmt.setString(1, timeStart);
            stmt.setString(2, timeEnd);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    String client = rs.getString("client");
                    String platform = rs.getString("platform");
                    int number = rs.getInt("total");
                    KingUtil.printLog("getRegisterWithClient() client: "+platform+", number: "+number);
                    res.put(client + "-" + platform, number);
                }
            } catch (JSONException e) {
                KingUtil.printException("GetUserIndexDAOImpl getRegisterWithClient()", e);
            }
        }
        return res;
    }
}
