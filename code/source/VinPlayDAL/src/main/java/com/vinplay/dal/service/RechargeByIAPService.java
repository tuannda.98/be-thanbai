// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service;

import com.vinplay.vbee.common.response.RechardByIAPResponse;

import java.util.List;

public interface RechargeByIAPService {
    List<RechardByIAPResponse> ListRechargeIAP(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5, final int p6);

    long totalMoney(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5);

    long countListRechargeIAP(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5);
}
