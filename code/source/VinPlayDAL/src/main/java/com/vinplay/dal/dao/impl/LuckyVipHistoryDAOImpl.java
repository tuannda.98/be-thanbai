// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import com.vinplay.dal.dao.LuckyVipHistoryDAO;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.response.LuckyVipHistoryResponse;
import org.bson.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LuckyVipHistoryDAOImpl implements LuckyVipHistoryDAO {
    @Override
    public List<LuckyVipHistoryResponse> searchLuckyVipHistory(final String nickName, final String timeStart, final String timeEnd, final int page) {
        final ArrayList<LuckyVipHistoryResponse> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        FindIterable iterable = null;
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        final int numStart = (page - 1) * 50;
        final int numEnd = 50;
        objsort.put("_id", -1);
        if (nickName != null && !nickName.equals("")) {
            conditions.put("nick_name", nickName);
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("time_log", obj);
        }
        iterable = db.getCollection("lucky_vip_history").find(new Document(conditions)).sort(objsort).skip(numStart).limit(50);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final LuckyVipHistoryResponse lucky = new LuckyVipHistoryResponse();
                lucky.trans_id = document.getLong("trans_id");
                lucky.nick_name = document.getString("nick_name");
                lucky.month = document.getString("month");
                lucky.result_vin = document.getInteger("result_vin");
                lucky.result_mutil = document.getInteger("result_multi");
                lucky.timelog = document.getString("time_log");
                results.add(lucky);
            }
        });
        return results;
    }
}
