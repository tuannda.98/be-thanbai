// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.vinplay.dal.service.LogPortalService;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.utils.DateTimeUtils;
import org.apache.log4j.Logger;

import java.util.concurrent.TimeUnit;

public class LogPortalServiceImpl implements LogPortalService {
    private static final long CACHE_LOG_PORTAL_TTL = 60L;
    private static Logger logger;
    private static String FORMAT;

    @Override
    public void log(final String command) {
        final HazelcastInstance instance = HazelcastClientFactory.getInstance();
        final IMap map;
        if (instance != null && (map = instance.getMap("cacheLogPortal")) != null) {
            if (!map.containsKey(command)) {
                map.put(command, 1L, 60L, TimeUnit.MINUTES);
            } else if (map != null) {
                long count = (long) map.get(command);
                map.put(command, ++count, 60L, TimeUnit.MINUTES);
            }
        }
    }

    @Override
    public void saveLog() {
        final String time = DateTimeUtils.getCurrentTime();
        final HazelcastInstance instance = HazelcastClientFactory.getInstance();
        final IMap map = instance.getMap("cacheLogPortal");
        for (final Object c : map.keySet()) {
            final long count = (long) map.get(c);
            LogPortalServiceImpl.logger.debug(String.format(LogPortalServiceImpl.FORMAT, time, c, count));
            map.remove(c);
        }
    }

    static {
        LogPortalServiceImpl.logger = Logger.getLogger("count_request_portal_logger");
        LogPortalServiceImpl.FORMAT = ",%20s,\t%s,\t%6d";
    }
}
