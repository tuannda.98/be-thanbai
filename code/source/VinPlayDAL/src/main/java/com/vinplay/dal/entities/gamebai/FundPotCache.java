/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.dal.entities.gamebai;

public class FundPotCache {
    public String key;
    public long value;
    public int type;

    public FundPotCache() {
    }

    public FundPotCache(String _key, long _value, int _type) {
        this.key = _key;
        this.value = _value;
        this.type = _type;
    }
}

