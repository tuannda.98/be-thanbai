// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service;

import com.vinplay.vbee.common.response.UserNewMobileResponse;

import java.util.List;

public interface UserNewMobileService {
    List<UserNewMobileResponse> searchUserNewMobile(final String p0, final String p1, final String p2, final int p3);

    long countSearchUserNewMobile(final String p0, final String p1, final String p2);
}
