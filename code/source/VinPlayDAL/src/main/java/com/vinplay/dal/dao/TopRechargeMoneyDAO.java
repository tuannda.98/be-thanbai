// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao;

import com.vinplay.vbee.common.response.TopRechargeMoneyResponse;

import java.sql.SQLException;
import java.util.List;

public interface TopRechargeMoneyDAO {
    List<TopRechargeMoneyResponse> getTopRechargeMoney(final int p0, final String p1, final int p2, final int p3) throws SQLException;
}
