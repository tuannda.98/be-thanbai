// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import com.vinplay.dal.dao.UserNewMobileDAO;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.response.UserNewMobileResponse;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

public class UserNewMobileDAOImpl implements UserNewMobileDAO {
    @Override
    public List<UserNewMobileResponse> searchUserNewMobile(final String nickName, final String mobile, final String mobileold, final int page) {
        final ArrayList<UserNewMobileResponse> result = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        FindIterable iterable = null;
        final BasicDBObject objsort = new BasicDBObject();
        final Document conditions = new Document();
        objsort.put("_id", -1);
        final int num_start = (page - 1) * 50;
        final int num_end = 50;
        if (nickName != null && !nickName.equals("")) {
            conditions.put("nick_name", nickName);
        }
        if (mobile != null && !mobile.equals("")) {
            conditions.put("mobile", mobile);
        }
        if (mobileold != null && !mobileold.equals("")) {
            conditions.put("mobile_old", mobileold);
        }
        iterable = db.getCollection("user_new_mobile").find(new Document(conditions)).sort(objsort).skip(num_start).limit(50);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final UserNewMobileResponse user = new UserNewMobileResponse();
                user.nickName = document.getString("nick_name");
                user.mobile = document.getString("mobile");
                user.mobileold = document.getString("mobile_old");
                user.updateTime = document.getString("update_time");
                result.add(user);
            }
        });
        return result;
    }

    @Override
    public long countSearchUserNewMobile(final String nickName, final String mobile, final String mobileold) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        long totalrecord = 0L;
        final Document conditions = new Document();
        if (nickName != null && !nickName.equals("")) {
            conditions.put("nick_name", nickName);
        }
        if (mobile != null && !mobile.equals("")) {
            conditions.put("mobile", mobile);
        }
        if (mobileold != null && !mobileold.equals("")) {
            conditions.put("mobile_old", mobileold);
        }
        totalrecord = db.getCollection("user_new_mobile").count(new Document(conditions));
        return totalrecord;
    }
}
