package com.vinplay.dal.dao.impl;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import com.vinplay.dal.dao.OverUnderDAO;
import com.vinplay.dal.entities.report.ReportMoneySystemModel;
import com.vinplay.dal.entities.taixiu.ResultTaiXiu;
import com.vinplay.dal.entities.taixiu.TransactionTaiXiu;
import com.vinplay.dal.entities.taixiu.TransactionTaiXiuDetail;
import com.vinplay.dal.entities.taixiu.VinhDanhRLTLModel;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.models.cache.ReportModel;
import com.vinplay.vbee.common.models.cache.ThanhDuTXModel;
import com.vinplay.vbee.common.models.minigame.TopWin;
import com.vinplay.vbee.common.models.minigame.taixiu.XepHangRLTLModel;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.pools.ConnectionPool;
import com.vinplay.vbee.common.utils.CommonUtils;
import com.vinplay.vbee.common.utils.VinPlayUtils;
import org.bson.Document;

import java.sql.*;
import java.util.*;

public class OverUnderDAOImpl implements OverUnderDAO {
    @Override
    public List<ResultTaiXiu> getLichSuPhien(final int number, final int moneyType) throws SQLException {
        final ArrayList<ResultTaiXiu> results = new ArrayList<>();
        final String sql = "SELECT * FROM result_over_under WHERE money_type=" + moneyType + " ORDER BY `timestamp` DESC LIMIT 0," + number;
        try (final Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_minigame");
             final PreparedStatement stmt = conn.prepareStatement(sql);
             final ResultSet rs = stmt.executeQuery()
        ) {
            while (rs.next()) {
                final ResultTaiXiu entry = new ResultTaiXiu();
                entry.referenceId = rs.getLong("reference_id");
                entry.result = rs.getInt("result");
                entry.dice1 = rs.getInt("dice1");
                entry.dice2 = rs.getInt("dice2");
                entry.dice3 = rs.getInt("dice3");
                entry.totalTai = rs.getLong("total_tai");
                entry.totalXiu = rs.getLong("total_xiu");
                entry.numBetTai = rs.getInt("num_bet_tai");
                entry.numBetXiu = rs.getInt("num_bet_xiu");
                entry.totalPrize = rs.getLong("total_prize");
                entry.totalRefundTai = rs.getLong("total_refund_tai");
                entry.totalRefundXiu = rs.getLong("total_refund_xiu");
                entry.totalRevenue = rs.getLong("total_revenue");
                entry.moneyType = rs.getInt("money_type");
                final Timestamp timestamp = rs.getTimestamp("timestamp");
                entry.timestamp = CommonUtils.convertTimestampToString(timestamp);
                results.add(0, entry);
            }
        }
        return results;
    }

    @Override
    public List<TransactionTaiXiu> getLichSuGiaoDich(final String nickname, final int number, final int moneyType) throws SQLException {
        final ArrayList<TransactionTaiXiu> results = new ArrayList<>();
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_minigame");
             CallableStatement call = conn.prepareCall("CALL ou_get_lich_su_giao_dich(?,?,?)")) {

            int param = 1;
            call.setString(param++, nickname);
            call.setInt(param++, number);
            call.setByte(param++, (byte) moneyType);
            try (ResultSet rs = call.executeQuery()) {
                while (rs.next()) {
                    final TransactionTaiXiu entry = new TransactionTaiXiu();
                    entry.referenceId = rs.getLong("reference_id");
                    entry.userId = rs.getInt("user_id");
                    entry.username = rs.getString("user_name");
                    entry.betValue = rs.getLong("bet_value");
                    entry.betSide = rs.getInt("bet_side");
                    entry.totalPrize = rs.getLong("total_prize");
                    entry.totalRefund = rs.getLong("total_refund");
                    final Timestamp date = rs.getTimestamp("timestamp");
                    entry.timestamp = CommonUtils.convertTimestampToString(date);
                    final byte dice1 = rs.getByte("dice1");
                    final byte dice2 = rs.getByte("dice2");
                    final byte dice3 = rs.getByte("dice3");
                    final int total = dice1 + dice2 + dice3;
                    entry.resultPhien = dice1 + " - " + dice2 + " - " + dice3 + "   " + total;
                    results.add(entry);
                }
            }
        }
        return results;
    }

    @Override
    public List<TopWin> getTopTaiXiu(final int moneyType) throws SQLException {
        final ArrayList<TopWin> result = new ArrayList<>();
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_minigame");
             CallableStatement call = conn.prepareCall("CALL ou_get_top_win(?)")) {
            int param = 1;
            call.setByte(param++, (byte) moneyType);
            try (ResultSet rs = call.executeQuery()) {
                while (rs.next()) {
                    final TopWin entry = new TopWin();
                    entry.setUsername(rs.getString("user_name"));
                    entry.setMoney(rs.getLong("money"));
                    result.add(entry);
                }
            }
        }
        return result;
    }

    @Override
    public int countLichSuGiaoDichTX(final String nickname, final int moneyType) throws SQLException {
        int totalRecords = -1;
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_minigame");
             CallableStatement call = conn.prepareCall("CALL ou_count_lich_su_giao_dich(?, ?,?)")) {
            int param = 1;
            call.setString(param++, nickname);
            call.setByte(param++, (byte) moneyType);
            call.registerOutParameter(param++, 4);
            call.execute();
            totalRecords = call.getInt(3);
        }
        return totalRecords;
    }

    @Override
    public List<TransactionTaiXiuDetail> getChiTietPhien(final long referenceId, final int moneyType) throws SQLException {
        final ArrayList<TransactionTaiXiuDetail> results = new ArrayList<>();
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_minigame");
             CallableStatement call = conn.prepareCall("CALL ou_get_chi_tiet_phien(?,?)")) {
            int param = 1;
            call.setLong(param++, referenceId);
            call.setByte(param++, (byte) moneyType);
            try (ResultSet rs = call.executeQuery()) {
                while (rs.next()) {
                    final TransactionTaiXiuDetail entry = new TransactionTaiXiuDetail();
                    entry.referenceId = rs.getLong("reference_id");
                    entry.userId = rs.getInt("user_id");
                    entry.username = rs.getString("user_name");
                    entry.betValue = rs.getLong("bet_value");
                    entry.betSide = rs.getInt("bet_side");
                    entry.prize = rs.getLong("prize");
                    entry.refund = rs.getLong("refund");
                    entry.inputTime = rs.getInt("input_time");
                    entry.moneyType = rs.getByte("money_type");
                    entry.timestamp = rs.getDate("timestamp");
                    results.add(entry);
                }
            }
        }
        return results;
    }

    @Override
    public ResultTaiXiu getKetQuaPhien(final long referenceId, final int moneyType) throws SQLException {
        ResultTaiXiu entry = null;
        final String sql = "SELECT * FROM result_over_under WHERE reference_id=" + referenceId + " AND money_type=" + moneyType;
        try (final Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_minigame");
             final PreparedStatement stmt = conn.prepareStatement(sql);
             ResultSet rs = stmt.executeQuery()
        ) {
            if (rs.next()) {
                entry = new ResultTaiXiu();
                entry.referenceId = rs.getLong("reference_id");
                entry.result = rs.getInt("result");
                entry.dice1 = rs.getInt("dice1");
                entry.dice2 = rs.getInt("dice2");
                entry.dice3 = rs.getInt("dice3");
                entry.totalTai = rs.getLong("total_tai");
                entry.totalXiu = rs.getLong("total_xiu");
                entry.numBetTai = rs.getInt("num_bet_tai");
                entry.numBetXiu = rs.getInt("num_bet_xiu");
                entry.totalPrize = rs.getLong("total_prize");
                entry.totalRefundTai = rs.getLong("total_refund_tai");
                entry.totalRefundXiu = rs.getLong("total_refund_xiu");
                entry.totalRevenue = rs.getLong("total_revenue");
                entry.moneyType = rs.getInt("money_type");
                final Timestamp timestamp = rs.getTimestamp("timestamp");
                entry.timestamp = CommonUtils.convertTimestampToString(timestamp);
            }
        }
        return entry;
    }

    @Override
    public List<ThanhDuTXModel> getTopThanhDuDaily(final String startTime, final String endTime, final short type) throws SQLException {
        final ArrayList<ThanhDuTXModel> results = new ArrayList<>();
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_minigame");
             CallableStatement call = conn.prepareCall("CALL ou_get_top_thanh_du(?, ?, ?)")) {
            int param = 1;
            call.setString(param++, startTime);
            call.setString(param++, endTime);
            call.setByte(param++, (byte) type);
            try (ResultSet rs = call.executeQuery()) {
                while (rs.next()) {
                    final String username = rs.getString("user_name");
                    final ThanhDuTXModel entry = new ThanhDuTXModel(username);
                    entry.number = rs.getInt("number");
                    entry.totalValue = rs.getLong("total_betting");
                    entry.currentReferenceId = rs.getLong("last_reference");
                    entry.parseReferences(rs.getString("references"));
                    results.add(entry);
                }
            }
        }
        return results;
    }

    @Override
    public int getMaxThanhDu(final String username, final short type) throws SQLException {
        int max = 0;
        final String sql = "SELECT `number` FROM thanh_du_ou WHERE user_name='" + username + "' AND `type`=" + type + " AND DATE(`last_update`)=CURDATE()";
        try (final Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_minigame");
             final PreparedStatement stmt = conn.prepareStatement(sql);
             final ResultSet rs = stmt.executeQuery()
        ) {
            if (rs.next()) {
                max = rs.getInt("number");
            }
        }
        return max;
    }

    @Override
    public int getSoLanRutLoc(final String username) throws SQLException {
        int soLanRut = 0;
        final String sql = "SELECT so_lan_rut FROM user_rut_loc_ou WHERE user_name='" + username + "'";
        try (final Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_minigame");
             final PreparedStatement stmt = conn.prepareStatement(sql);
             final ResultSet rs = stmt.executeQuery()
        ) {
            if (rs.next()) {
                soLanRut = rs.getInt("so_lan_rut");
            }
        }
        return soLanRut;
    }

    public List<VinhDanhRLTLModel> getVinhDanhTLRL(final String collectionName) {
        final ArrayList<VinhDanhRLTLModel> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        FindIterable iterable = null;
        final BasicDBObject sortCondtions = new BasicDBObject();
        sortCondtions.put("_id", -1);
        iterable = db.getCollection(collectionName).find().sort(sortCondtions).limit(10);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final VinhDanhRLTLModel entry = new VinhDanhRLTLModel();
                entry.username = document.getString("user_name");
                entry.money = document.getLong("money");
                entry.time = document.getString("time_log");
                results.add(entry);
            }
        });
        return results;
    }

    private List<XepHangRLTLModel> getBangXepHangTLRL(final String collectionName) {
        final ArrayList<XepHangRLTLModel> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final AggregateIterable iterable = db.getCollection(collectionName).aggregate(Arrays.asList(new Document("$group", new Document("_id", "$user_name").append("total", new Document("$sum", "$money"))), new Document("$sort", new Document("total", -1)), new Document("$limit", 10)));
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final XepHangRLTLModel model = new XepHangRLTLModel();
                model.username = document.getString("_id");
                model.money = document.getLong("total");
                results.add(model);
            }
        });
        return results;
    }

    private long getTienTLRL(final String username, final String collectionName) {
        long tongTien = 0L;
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, String> condition = new HashMap<>();
        condition.put("user_name", username);
        final AggregateIterable iterable = db.getCollection(collectionName).aggregate(Arrays.asList(new Document("$match", condition), new Document("$group", new Document("_id", "null").append("total", new Document("$sum", "$money")))));
        final Document document = (Document) iterable.first();
        if (document != null) {
            tongTien = document.getLong("total");
        }
        return tongTien;
    }

    @Override
    public List<XepHangRLTLModel> getXepHangTanLoc() {
        return this.getBangXepHangTLRL("tan_loc_ou");
    }

    @Override
    public List<VinhDanhRLTLModel> getVinhDanhTanLoc() {
        return this.getVinhDanhTLRL("tan_loc_ou");
    }

    @Override
    public long getTongTienTanLoc(final String username) {
        return this.getTienTLRL(username, "tan_loc_ou");
    }

    @Override
    public List<XepHangRLTLModel> getXepHangRutLoc() {
        return this.getBangXepHangTLRL("rut_loc_ou");
    }

    @Override
    public List<VinhDanhRLTLModel> getVinhDanhRutLoc() {
        return this.getVinhDanhTLRL("rut_loc_ou");
    }

    @Override
    public long getTongTienRutLoc(final String username) {
        return this.getTienTLRL(username, "rut_loc_ou");
    }

    @Override
    public ReportMoneySystemModel getReportTXToDay() {
        final ReportMoneySystemModel res = new ReportMoneySystemModel();
        final String today = VinPlayUtils.getCurrentDate();
        final HazelcastInstance client = HazelcastClientFactory.getInstance();
        final IMap<String, ReportModel> reportMap = client.getMap("cacheReports");
        for (final Map.Entry<String, ReportModel> entry : reportMap.entrySet()) {
            if (entry.getKey().contains(today)) {
                if (!entry.getKey().contains("OverUnder")) {
                    continue;
                }
                final ReportModel model = entry.getValue();
                if (model.isBot) {
                    continue;
                }
                final ReportMoneySystemModel reportMoneySystemModel7;
                final ReportMoneySystemModel reportMoneySystemModel = reportMoneySystemModel7 = res;
                reportMoneySystemModel7.moneyWin += model.moneyWin;
                final ReportMoneySystemModel reportMoneySystemModel8;
                final ReportMoneySystemModel reportMoneySystemModel2 = reportMoneySystemModel8 = res;
                reportMoneySystemModel8.moneyLost += model.moneyLost;
                final ReportMoneySystemModel reportMoneySystemModel9;
                final ReportMoneySystemModel reportMoneySystemModel3 = reportMoneySystemModel9 = res;
                reportMoneySystemModel9.moneyOther += model.moneyOther;
                final ReportMoneySystemModel reportMoneySystemModel10;
                final ReportMoneySystemModel reportMoneySystemModel4 = reportMoneySystemModel10 = res;
                reportMoneySystemModel10.fee += model.fee;
                final ReportMoneySystemModel reportMoneySystemModel11;
                final ReportMoneySystemModel reportMoneySystemModel5 = reportMoneySystemModel11 = res;
                reportMoneySystemModel11.revenuePlayGame += model.moneyWin + model.moneyLost;
                final ReportMoneySystemModel reportMoneySystemModel12;
                final ReportMoneySystemModel reportMoneySystemModel6 = reportMoneySystemModel12 = res;
                reportMoneySystemModel12.revenue += model.moneyWin + model.moneyLost + model.moneyOther;
            }
        }
        return res;
    }

    @Override
    public ReportMoneySystemModel getReportTX(final String startDate, final String endDate) throws SQLException {
        final ReportMoneySystemModel res = new ReportMoneySystemModel();
        final String sql = "SELECT SUM(money_win) as total_win, SUM(money_lost) as total_lost, SUM(money_other) as total_other, SUM(fee) as total_fee FROM vinplay.report_money_daily WHERE `date` >= '" + startDate + "?' and `date` <= '" + endDate + "' and action_name = 'OverUnder'";
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_minigame");
             final PreparedStatement stmt = conn.prepareStatement(sql);
             final ResultSet rs = stmt.executeQuery()
        ) {
            while (rs.next()) {
                res.moneyWin = rs.getLong("total_win");
                res.moneyLost = rs.getLong("total_lost");
                res.moneyOther = rs.getLong("total_other");
                res.fee = rs.getLong("total_fee");
            }
            res.revenuePlayGame = res.moneyWin + res.moneyLost;
            res.revenue = res.moneyWin + res.moneyLost + res.moneyOther;
        }
        return res;
    }
}
