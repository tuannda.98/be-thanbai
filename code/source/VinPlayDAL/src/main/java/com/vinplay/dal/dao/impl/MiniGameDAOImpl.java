package com.vinplay.dal.dao.impl;

import casio.king365.core.CommonDbPool;
import com.vinplay.dal.dao.MiniGameDAO;
import com.vinplay.dal.entities.gamebai.FundPotCache;
import com.vinplay.vbee.common.response.BonusFundResponse;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MiniGameDAOImpl
        implements MiniGameDAO {
    @Override
    public long getReferenceId(int gameId) throws SQLException {
        long referenceId = -1L;
        String sql = "SELECT value FROM `references` WHERE game_id=" + gameId;
        try (Connection conn = CommonDbPool.getInstance().getVinplayMinigameDS().getConnection();
             PreparedStatement stmt = conn.prepareStatement(sql);
             ResultSet rs = stmt.executeQuery();) {
            if (rs.next()) {
                referenceId = rs.getLong("value");
            }
        }
        return referenceId;
    }

    @Override
    public long getPot(String potName) throws SQLException {
        long value = 0L;
        String sql = "SELECT value FROM minigame_pots WHERE minigame_pots.pot_name = '" + potName + "'";
        try (Connection conn = CommonDbPool.getInstance().getVinplayMinigameDS().getConnection();
             PreparedStatement stmt = conn.prepareStatement(sql);
             ResultSet rs = stmt.executeQuery();) {
            if (rs.next()) {
                value = rs.getLong("value");
            }
        }
        return value;
    }

    @Override
    public long[] getPots(String potName) throws SQLException {
        ArrayList<Long> result = new ArrayList<>();
        String sql = "SELECT value FROM minigame_pots WHERE minigame_pots.pot_name like '" + potName + "%'";
        try (Connection conn = CommonDbPool.getInstance().getVinplayMinigameDS().getConnection();
             PreparedStatement stmt = conn.prepareStatement(sql);
             ResultSet rs = stmt.executeQuery();) {
            while (rs.next()) {
                result.add(rs.getLong("value"));
            }
        }
        long[] arr = new long[result.size()];
        for (int i = 0; i < result.size(); ++i) {
            arr[i] = result.get(i);
        }
        return arr;
    }

    @Override
    public long getFund(String fundName) throws SQLException {
        long value = 0L;
        String sql = "SELECT value FROM minigame_funds WHERE minigame_funds.fund_name = '" + fundName + "'";
        try (Connection conn = CommonDbPool.getInstance().getVinplayMinigameDS().getConnection();
             PreparedStatement stmt = conn.prepareStatement(sql);
             ResultSet rs = stmt.executeQuery();) {
            if (rs.next()) {
                value = rs.getLong("value");
            }
        }
        return value;
    }

    @Override
    public long[] getFunds(String fundName) throws SQLException {
        ArrayList<Long> result = new ArrayList<>();
        String sql = "SELECT value FROM minigame_funds WHERE minigame_funds.fund_name like '" + fundName + "%'";
        try (Connection conn = CommonDbPool.getInstance().getVinplayMinigameDS().getConnection();
             PreparedStatement stmt = conn.prepareStatement(sql);
             ResultSet rs = stmt.executeQuery();) {
            while (rs.next()) {
                result.add(rs.getLong("value"));
            }
        }
        long[] arr = new long[result.size()];
        for (int i = 0; i < result.size(); ++i) {
            arr[i] = result.get(i);
        }
        return arr;
    }

    @Override
    public List<BonusFundResponse> getFunds() throws SQLException {
        ArrayList<BonusFundResponse> results = new ArrayList<>();
        try (Connection conn = CommonDbPool.getInstance().getVinplayMinigameDS().getConnection();
             PreparedStatement stmt = conn.prepareStatement("SELECT fund_name, value FROM vinplay_minigame.minigame_funds");
             ResultSet rs = stmt.executeQuery();) {
            while (rs.next()) {
                BonusFundResponse bonusFund = new BonusFundResponse();
                bonusFund.name = rs.getString("fund_name");
                bonusFund.value = rs.getLong("value");
                results.add(bonusFund);
            }
        }
        return results;
    }

    @Override
    public boolean saveReferenceId(long newReferenceId, int gameId) throws SQLException {
        String sql = "UPDATE `references` SET value=" + newReferenceId + " WHERE game_id=" + gameId;
        try (Connection conn = CommonDbPool.getInstance().getVinplayMinigameDS().getConnection();
             PreparedStatement stmt = conn.prepareStatement(sql);) {
            return stmt.execute();
        }
    }

    @Override
    public List<FundPotCache> GetFunds() throws SQLException {
        ArrayList<FundPotCache> pots = new ArrayList<>();
        String sql = "SELECT * FROM minigame_funds";
        try (Connection conn = CommonDbPool.getInstance().getVinplayMinigameDS().getConnection();
             PreparedStatement stmt = conn.prepareStatement(sql);
             ResultSet rs = stmt.executeQuery();) {
            while (rs.next()) {
                FundPotCache pot = new FundPotCache();
                pot.key = rs.getString("fund_name");
                pot.value = rs.getLong("value");
                pot.type = 2;
                pots.add(pot);
            }
        }
        return pots;
    }

    @Override
    public List<FundPotCache> GetPots() throws SQLException {
        ArrayList<FundPotCache> pots = new ArrayList<>();
        String sql = "SELECT * FROM minigame_pots";
        try (Connection conn = CommonDbPool.getInstance().getVinplayMinigameDS().getConnection();
             PreparedStatement stmt = conn.prepareStatement(sql);
             ResultSet rs = stmt.executeQuery();) {
            while (rs.next()) {
                FundPotCache pot = new FundPotCache();
                pot.key = rs.getString("pot_name");
                pot.value = rs.getLong("value");
                pot.type = 1;
                pots.add(pot);
            }
        }
        return pots;
    }

    @Override
    public boolean UpdatePot(String key, long value) throws SQLException {
        String sql = "UPDATE minigame_pots SET value=? WHERE pot_name=?";
        try (Connection conn = CommonDbPool.getInstance().getVinplayMinigameDS().getConnection();
             PreparedStatement stm = conn.prepareStatement(sql);) {
            stm.setLong(1, value);
            stm.setString(2, key);
            if (stm.executeUpdate() == 1) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean UpdateFund(String key, long value) throws SQLException {
        String sql = "UPDATE minigame_funds SET value=? WHERE fund_name=?";
        try (Connection conn = CommonDbPool.getInstance().getVinplayMinigameDS().getConnection();
             PreparedStatement stm = conn.prepareStatement(sql);) {
            stm.setLong(1, value);
            stm.setString(2, key);
            if (stm.executeUpdate() == 1) {
                return true;
            }
        }
        return false;
    }
}
