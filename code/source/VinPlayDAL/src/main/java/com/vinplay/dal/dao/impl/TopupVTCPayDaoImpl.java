// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.vinplay.dal.dao.TopupVTCPayDao;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.response.MoneyTotalFollowFaceValue;
import com.vinplay.vbee.common.response.topupVTCPay.LogTopupVTCPay;
import org.bson.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TopupVTCPayDaoImpl implements TopupVTCPayDao {
    private long money10K;
    private int money10KQuantity;
    private long money20K;
    private int money20KQuantity;
    private long money50K;
    private int money50KQuantity;
    private long money100K;
    private int money100KQuantity;
    private long money200K;
    private int money200KQuantity;
    private long money500K;
    private int money500KQuantity;

    public TopupVTCPayDaoImpl() {
        this.money10K = 0L;
        this.money10KQuantity = 0;
        this.money20K = 0L;
        this.money20KQuantity = 0;
        this.money50K = 0L;
        this.money50KQuantity = 0;
        this.money100K = 0L;
        this.money100KQuantity = 0;
        this.money200K = 0L;
        this.money200KQuantity = 0;
        this.money500K = 0L;
        this.money500KQuantity = 0;
    }

    @Override
    public List<LogTopupVTCPay> getLogTopupVtcPay(final String nickname, final String price, final String transId, final String startTime, final String endTime, final String page) {
        final ArrayList<LogTopupVTCPay> response = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final MongoCollection col = db.getCollection("log_topup_vtcpay");
        final int numStart = (Integer.parseInt(page) - 1) * 50;
        final int numEnd = 50;
        final BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        final HashMap<String, Object> conditions = new HashMap<>();
        if (nickname != null && !nickname.isEmpty()) {
            final String pattern = ".*" + nickname + ".*";
            conditions.put("nick_name", new BasicDBObject().append("$regex", pattern).append("$options", "i"));
        }
        if (price != null && !price.isEmpty()) {
            conditions.put("price", Integer.parseInt(price));
        }
        if (transId != null && !transId.isEmpty()) {
            conditions.put("partner_trans_id", transId);
        }
        if (startTime != null && !startTime.isEmpty() && endTime != null && !endTime.isEmpty()) {
            final BasicDBObject obj = new BasicDBObject();
            obj.put("$gte", startTime);
            obj.put("$lte", endTime);
            conditions.put("time_request", obj);
        }
        final FindIterable iterable = col.find(new Document(conditions)).sort(objsort).skip(numStart).limit(50);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final LogTopupVTCPay model = new LogTopupVTCPay(document.getString("partner_trans_id"), document.getString("nick_name"), document.getInteger("price"), document.getInteger("status"), document.getString("response_code"), document.getString("description"), TopupVTCPayDaoImpl.this.formatTime(document.getString("time_request")), TopupVTCPayDaoImpl.this.formatTime(document.getString("time_response")));
                response.add(model);
            }
        });
        return response;
    }

    @Override
    public List<MoneyTotalFollowFaceValue> doiSoatTopupVtcPay(final String startTime, final String endTime) {
        this.money10K = 0L;
        this.money10KQuantity = 0;
        this.money20K = 0L;
        this.money20KQuantity = 0;
        this.money50K = 0L;
        this.money50KQuantity = 0;
        this.money100K = 0L;
        this.money100KQuantity = 0;
        this.money200K = 0L;
        this.money200KQuantity = 0;
        this.money500K = 0L;
        this.money500KQuantity = 0;
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        final Document conditions = new Document();
        if (startTime != null && !startTime.isEmpty() && endTime != null && !endTime.isEmpty()) {
            final BasicDBObject obj = new BasicDBObject();
            obj.put("$gte", startTime);
            obj.put("$lte", endTime);
            conditions.put("time_request", obj);
        }
        conditions.put("status", 1);
        final FindIterable iterable = db.getCollection("log_topup_vtcpay").find(new Document(conditions)).sort(objsort);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                switch (document.getInteger("price")) {
                    case 10000: {
                        TopupVTCPayDaoImpl.this.money10KQuantity++;
                        final TopupVTCPayDaoImpl topupVTCPayDaoImpl = TopupVTCPayDaoImpl.this;
                        topupVTCPayDaoImpl.money10K += 10000L;
                        break;
                    }
                    case 20000: {
                        TopupVTCPayDaoImpl.this.money20KQuantity++;
                        final TopupVTCPayDaoImpl topupVTCPayDaoImpl = TopupVTCPayDaoImpl.this;
                        topupVTCPayDaoImpl.money20K += 20000L;
                        break;
                    }
                    case 50000: {
                        TopupVTCPayDaoImpl.this.money50KQuantity++;
                        final TopupVTCPayDaoImpl topupVTCPayDaoImpl = TopupVTCPayDaoImpl.this;
                        topupVTCPayDaoImpl.money50K += 50000L;
                        break;
                    }
                    case 100000: {
                        TopupVTCPayDaoImpl.this.money100KQuantity++;
                        final TopupVTCPayDaoImpl topupVTCPayDaoImpl = TopupVTCPayDaoImpl.this;
                        topupVTCPayDaoImpl.money100K += 100000L;
                        break;
                    }
                    case 200000: {
                        TopupVTCPayDaoImpl.this.money200KQuantity++;
                        final TopupVTCPayDaoImpl topupVTCPayDaoImpl = TopupVTCPayDaoImpl.this;
                        topupVTCPayDaoImpl.money200K += 200000L;
                        break;
                    }
                    case 500000: {
                        TopupVTCPayDaoImpl.this.money500KQuantity++;
                        final TopupVTCPayDaoImpl topupVTCPayDaoImpl = TopupVTCPayDaoImpl.this;
                        topupVTCPayDaoImpl.money500K += 500000L;
                        break;
                    }
                }
            }
        });
        final ArrayList<MoneyTotalFollowFaceValue> response = new ArrayList<>();
        final MoneyTotalFollowFaceValue money10KFollowFace = new MoneyTotalFollowFaceValue();
        money10KFollowFace.setFaceValue(10000);
        money10KFollowFace.setQuantity(this.money10KQuantity);
        money10KFollowFace.setMoneyTotal(this.money10K);
        response.add(money10KFollowFace);
        final MoneyTotalFollowFaceValue money20KFollowFace = new MoneyTotalFollowFaceValue();
        money20KFollowFace.setFaceValue(20000);
        money20KFollowFace.setQuantity(this.money20KQuantity);
        money20KFollowFace.setMoneyTotal(this.money20K);
        response.add(money20KFollowFace);
        final MoneyTotalFollowFaceValue money50KFollowFace = new MoneyTotalFollowFaceValue();
        money50KFollowFace.setFaceValue(50000);
        money50KFollowFace.setQuantity(this.money50KQuantity);
        money50KFollowFace.setMoneyTotal(this.money50K);
        response.add(money50KFollowFace);
        final MoneyTotalFollowFaceValue money100KFollowFace = new MoneyTotalFollowFaceValue();
        money100KFollowFace.setFaceValue(100000);
        money100KFollowFace.setQuantity(this.money100KQuantity);
        money100KFollowFace.setMoneyTotal(this.money100K);
        response.add(money100KFollowFace);
        final MoneyTotalFollowFaceValue money200KFollowFace = new MoneyTotalFollowFaceValue();
        money200KFollowFace.setFaceValue(200000);
        money200KFollowFace.setQuantity(this.money200KQuantity);
        money200KFollowFace.setMoneyTotal(this.money200K);
        response.add(money200KFollowFace);
        final MoneyTotalFollowFaceValue money500KFollowFace = new MoneyTotalFollowFaceValue();
        money500KFollowFace.setFaceValue(500000);
        money500KFollowFace.setQuantity(this.money500KQuantity);
        money500KFollowFace.setMoneyTotal(this.money500K);
        response.add(money500KFollowFace);
        return response;
    }

    private String formatTime(final String inputTime) {
        if (inputTime == null) {
            return inputTime;
        }
        if (inputTime.length() > 12) {
            final String year = inputTime.substring(0, 4);
            final String month = inputTime.substring(4, 6);
            final String day = inputTime.substring(6, 8);
            final String hour = inputTime.substring(8, 10);
            final String min = inputTime.substring(10, 12);
            final String sec = inputTime.substring(12);
            return year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec;
        }
        return inputTime;
    }
}
