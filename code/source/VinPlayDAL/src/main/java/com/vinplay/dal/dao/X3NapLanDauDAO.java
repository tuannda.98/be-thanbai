package com.vinplay.dal.dao;

import casio.king365.entities.X3NapLanDau;
import org.bson.Document;

import java.util.List;

public interface X3NapLanDauDAO {
    X3NapLanDau getX3NapLanDau(String nickname, String channel);
    List<Document> getX3NapLanDau(String nickname);
    void createX3NapLanDau(X3NapLanDau obj);
    void updatePercent(String nickname, String channel, Double percent);
    void updateReceivedBonus(String nickName, String channel, boolean newReceBonus);
    void updateActiveTime(String nickName, String channel, long newActiveTime);
}
