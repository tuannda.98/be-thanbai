// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao;

import com.vinplay.vbee.common.models.minigame.pokego.LSGDPokeGo;
import com.vinplay.vbee.common.models.minigame.pokego.TopPokeGo;
import com.vinplay.vbee.common.models.slot.NoHuModel;

import java.sql.SQLException;
import java.util.List;

public interface SlotMachineDAO {
    List<TopPokeGo> getTopByPage(final String p0, final int p1);

    List<TopPokeGo> getTop(final String p0, final int p1);

    int countLSGD(final String p0, final String p1);

    List<LSGDPokeGo> getLSGD(final String p0, final String p1, final int p2);

    long getLastRefenceId(final String p0);

    boolean updateSlotFreeDaily(final String p0, final String p1, final int p2, final int p3) throws SQLException;

    List<NoHuModel> getListNoHu(final String p0, final int p1);
}
