// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.vinplay.dal.dao.impl.DoiSoatNganLuongDAOImpl;
import com.vinplay.dal.service.DoiSoatNganLuongService;
import com.vinplay.vbee.common.response.NganLuongFollowFaceValue;

import java.util.List;

public class DoiSoatNganLuongServiceImpl implements DoiSoatNganLuongService {
    @Override
    public List<NganLuongFollowFaceValue> getDoiSoatData(final String timeStart, final String timeEnd) {
        final DoiSoatNganLuongDAOImpl dao = new DoiSoatNganLuongDAOImpl();
        return dao.getDoiSoatData(timeStart, timeEnd);
    }
}
