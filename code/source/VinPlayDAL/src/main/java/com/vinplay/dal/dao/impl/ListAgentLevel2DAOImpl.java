package com.vinplay.dal.dao.impl;

import com.vinplay.dal.dao.ListAgentLevel2DAO;
import com.vinplay.vbee.common.pools.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ListAgentLevel2DAOImpl implements ListAgentLevel2DAO {
    @Override
    public List<String> listAgentLevel2(final String nickName) throws SQLException {
        final ArrayList<String> results = new ArrayList<>();
        try (final Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_admin");
             final PreparedStatement stmt = conn.prepareStatement(" SELECT a2.nickname AS dlcap2 FROM vinplay_admin.useragent a1, vinplay_admin.useragent a2  WHERE a1.nickname = ?    AND a1.id = a2.parentid    AND a1.status = 'D'    AND a1.active = 1    AND a2.status = 'D'    AND a2.active = 1 ");) {
            stmt.setString(1, nickName);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    results.add(rs.getString("dlcap2"));
                }
            }
        }
        return results;
    }

    @Override
    public String getAgentLevel1ByNickName(final String nickName) throws SQLException {
        String parentId = "";
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_admin");) {
            try (PreparedStatement stmt = conn.prepareStatement(" SELECT parentid  FROM vinplay_admin.useragent  WHERE nickname = ? ");) {
                stmt.setString(1, nickName);
                try (ResultSet rs = stmt.executeQuery()) {
                    while (rs.next()) {
                        parentId = rs.getString("parentid");
                    }
                    if ("-1".equals(parentId)) {
                        return nickName;
                    }
                }
            }
            try (PreparedStatement stmt = conn.prepareStatement(" SELECT a1.nickname AS dlcap1 FROM vinplay_admin.useragent a1, vinplay_admin.useragent a2  WHERE a2.nickname = ?    AND a1.id = a2.parentid ")) {
                stmt.setString(1, nickName);
                try (ResultSet rs = stmt.executeQuery()) {
                    if (rs.next()) {
                        return rs.getString("dlcap1");
                    }
                }
            }
        }
        return "";
    }
}
