// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.vinplay.dal.dao.impl.ServerInfoDAOImpl;
import com.vinplay.dal.service.ServerInfoService;
import com.vinplay.vbee.common.messages.LogCCUMessage;
import com.vinplay.vbee.common.models.LogCCUModel;
import com.vinplay.vbee.common.rmq.RMQApi;
import com.vinplay.vbee.common.utils.DateTimeUtils;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeoutException;

public class ServerInfoServiceImpl implements ServerInfoService {
    @Override
    public void logCCU(final int ccu, final int ccuWeb, final int ccuAD, final int ccuIOS, final int ccuWP, final int ccuFB, final int ccuDT, final int ccuOT) {
        final String timestamp = DateTimeUtils.getCurrentTime("yyyy-MM-dd HH:mm:ss");
        final LogCCUMessage msg = new LogCCUMessage();
        msg.ccu = ccu;
        msg.ccuWeb = ccuWeb;
        msg.ccuAD = ccuAD;
        msg.ccuIOS = ccuIOS;
        msg.ccuWP = ccuWP;
        msg.ccuFB = ccuFB;
        msg.ccuDT = ccuDT;
        msg.ccuOT = ccuOT;
        msg.timestamp = timestamp;
        try {
            RMQApi.publishMessage("queue_server_info", msg, 1);
        } catch (IOException | InterruptedException | TimeoutException ex4) {
            ex4.printStackTrace();
        }
    }

    @Override
    public List<LogCCUModel> getLogCCU(final String startTime, final String endTime) {
        final ServerInfoDAOImpl dao = new ServerInfoDAOImpl();
        return dao.getLogCCU(startTime, endTime);
    }
}
