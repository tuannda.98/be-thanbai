// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao;

import com.vinplay.dal.entities.agent.BonusTopDSModel;
import com.vinplay.dal.entities.agent.TranferMoneyAgent;
import com.vinplay.vbee.common.messages.dvt.RefundFeeAgentMessage;
import com.vinplay.vbee.common.models.AgentModel;
import com.vinplay.vbee.common.models.cache.AgentDSModel;
import com.vinplay.vbee.common.response.AgentResponse;
import com.vinplay.vbee.common.response.LogAgentTranferMoneyResponse;
import com.vinplay.vbee.common.response.TranferAgentResponse;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface AgentDAO {
    List<AgentResponse> listAgent() throws SQLException;

    List<AgentResponse> listAgentByClient(final String p0) throws SQLException;

    AgentResponse listAgentByKey(final String p0) throws SQLException;

    List<LogAgentTranferMoneyResponse> searchAgentTranferMoney(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5, final int p6);

    List<AgentResponse> listUserAgent(final String p0) throws SQLException;

    List<AgentResponse> listUserAgentByParentID(final int p0) throws SQLException;

    TranferAgentResponse searchAgentTranfer(final String p0, final String p1, final String p2, final String p3);

    LogAgentTranferMoneyResponse searchAgentTranferMoney(final String p0);

    AgentDSModel getDS(final String p0, final String p1, final String p2, final boolean p3);

    Map<String, String> getAllNameAgent() throws SQLException;

    Map<String, ArrayList<String>> getAllAgent() throws SQLException;

    boolean checkRefundFeeAgent(final String p0, final String p1);

    List<RefundFeeAgentMessage> getLogRefundFeeAgent(final String p0, final String p1);

    long countsearchAgentTranferMoney(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5);

    long totalMoneyVinReceiveFromAgent(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5);

    long totalMoneyVinSendFromAgent(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5);

    long totalMoneyVinFeeFromAgent(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5);

    List<LogAgentTranferMoneyResponse> searchAgentTranferMoneyVinSale(final String p0, final String p1, final String p2, final String p3, final int p4, final int p5);

    long countSearchAgentTranferMoneyVinSale(final String p0, final String p1, final String p2, final String p3);

    long totalMoneyVinReceiveFromAgentByStatus(final String p0, final String p1, final String p2, final String p3);

    long totalMoneyVinSendFromAgentByStatus(final String p0, final String p1, final String p2, final String p3);

    boolean updateTopDsFromAgent(final String p0, final String p1, final String p2, final String p3);

    boolean updateTopDsFromAgentMySQL(final String p0, final String p1, final String p2, final String p3) throws SQLException;

    boolean logBonusTopDS(final BonusTopDSModel p0);

    boolean checkBonusTopDS(final String p0, final String p1);

    List<BonusTopDSModel> getLogBonusTopDS(final String p0, final String p1);

    TranferMoneyAgent getTransferMoneyAgent(final String p0, final String p1, final String p2);

    List<AgentResponse> listUserAgentAdmin(final String p0) throws SQLException;

    List<LogAgentTranferMoneyResponse> searchAgentTranferMoney(final String p0, final String p1, final String p2, final String p3, final String p4, final int p5);

    long countsearchAgentTranferMoney(final String p0, final String p1, final String p2, final String p3, final String p4);

    long totalMoneyVinReceiveFromAgent(final String p0, final String p1, final String p2, final String p3, final String p4);

    long totalMoneyVinSendFromAgent(final String p0, final String p1, final String p2, final String p3, final String p4);

    long totalMoneyVinFeeFromAgent(final String p0, final String p1, final String p2, final String p3, final String p4);

    List<AgentResponse> listUserAgentLevel2ByParentID(final int p0) throws SQLException;

    List<AgentResponse> listUserAgentLevel2ByID(final int p0) throws SQLException;

    List<AgentResponse> listUserAgentActive(final String p0) throws SQLException;

    List<AgentResponse> listUserAgentLevel1Active() throws SQLException;

    List<LogAgentTranferMoneyResponse> searchAgentTongTranferMoney(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5, final int p6);

    long countsearchAgentTongTranferMoney(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5);

    long totalMoneyVinReceiveFromAgentTong(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5);

    long totalMoneyVinSendFromAgentTong(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5);

    long totalMoneyVinFeeFromAgentTong(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5);

    List<AgentModel> getListPercentBonusVincard(final String p0) throws SQLException;

    int registerPercentBonusVincard(final String p0, final int p1) throws SQLException;
}
