// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao;

import com.vinplay.vbee.common.models.minigame.pokego.LSGDPokeGo;
import com.vinplay.vbee.common.models.minigame.pokego.TopPokeGo;

import java.util.List;

public interface PokeGoDAO {
    List<TopPokeGo> getTopPokeGo(final int p0, final int p1);

    List<TopPokeGo> getTop(final int p0, final int p1);

    int countLSGD(final String p0, final int p1);

    List<LSGDPokeGo> getLSGD(final String p0, final int p1, final int p2);

    long getLastRefenceId();
}
