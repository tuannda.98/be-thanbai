// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.vinplay.dal.dao.impl.CashOutByBankDAOImpl;
import com.vinplay.dal.service.CashOutByBankService;
import com.vinplay.vbee.common.response.CashOutByBankReponse;

import java.util.List;

public class CashOutByBankServiceImpl implements CashOutByBankService {
    @Override
    public List<CashOutByBankReponse> searchCashOutByBank(final String nickName, final String bank, final String status, final String code, final String timeStart, final String timeEnd, final int page, final String transid) {
        final CashOutByBankDAOImpl dao = new CashOutByBankDAOImpl();
        return dao.searchCashOutByBank(nickName, bank, status, code, timeStart, timeEnd, page, transid);
    }

    @Override
    public int countSearchCashOutByBank(final String nickName, final String bank, final String status, final String code, final String timeStart, final String timeEnd, final String transid) {
        final CashOutByBankDAOImpl dao = new CashOutByBankDAOImpl();
        return dao.countSearchCashOutByBank(nickName, bank, status, code, timeStart, timeEnd, transid);
    }

    @Override
    public long moneyTotal(final String nickName, final String bank, final String status, final String code, final String timeStart, final String timeEnd, final String transid) {
        final CashOutByBankDAOImpl dao = new CashOutByBankDAOImpl();
        return dao.moneyTotal(nickName, bank, status, code, timeStart, timeEnd, transid);
    }
}
