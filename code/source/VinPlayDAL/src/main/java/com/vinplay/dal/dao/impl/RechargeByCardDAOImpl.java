// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import com.vinplay.dal.dao.RechargeByCardDAO;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.response.MoneyTotalFollowFaceValue;
import com.vinplay.vbee.common.response.MoneyTotalRechargeByCardReponse;
import com.vinplay.vbee.common.response.RechargeByCardReponse;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class RechargeByCardDAOImpl implements RechargeByCardDAO {
    private long totalMoney;
    private long webMoney;
    private long androidMoney;
    private long iosMoney;
    private long winphoneMoney;
    private long facebookMoney;
    private long desktopMoney;
    private long otherMoney;
    private long viettelMoney;
    private long viettel5MMoney;
    private int viettel5MQuantity;
    private long viettel2MMoney;
    private int viettel2MQuantity;
    private long viettel1MMoney;
    private int viettel1MQuantity;
    private long viettel500KMoney;
    private int viettel500KQuantity;
    private long viettel300KMoney;
    private int viettel300KQuantity;
    private long viettel200KMoney;
    private int viettel200KQuantity;
    private long viettel100KMoney;
    private int viettel100KQuantity;
    private long viettel50KMoney;
    private int viettel50KQuantity;
    private long viettel30KMoney;
    private int viettel30KQuantity;
    private long viettel20KMoney;
    private int viettel20KQuantity;
    private long viettel10KMoney;
    private int viettel10KQuantity;
    private long vinaphoneMoney;
    private long vinaphone5MMoney;
    private int vinaphone5MQuantity;
    private long vinaphone2MMoney;
    private int vinaphone2MQuantity;
    private long vinaphone1MMoney;
    private int vinaphone1MQuantity;
    private long vinaphone500KMoney;
    private int vinaphone500KQuantity;
    private long vinaphone300KMoney;
    private int vinaphone300KQuantity;
    private long vinaphone200KMoney;
    private int vinaphone200KQuantity;
    private long vinaphone100KMoney;
    private int vinaphone100KQuantity;
    private long vinaphone50KMoney;
    private int vinaphone50KQuantity;
    private long vinaphone30KMoney;
    private int vinaphone30KQuantity;
    private long vinaphone20KMoney;
    private int vinaphone20KQuantity;
    private long vinaphone10KMoney;
    private int vinaphone10KQuantity;
    private long mobifoneMoney;
    private long mobifone5MMoney;
    private int mobifone5MQuantity;
    private long mobifone2MMoney;
    private int mobifone2MQuantity;
    private long mobifone1MMoney;
    private int mobifone1MQuantity;
    private long mobifone500KMoney;
    private int mobifone500KQuantity;
    private long mobifone300KMoney;
    private int mobifone300KQuantity;
    private long mobifone200KMoney;
    private int mobifone200KQuantity;
    private long mobifone100KMoney;
    private int mobifone100KQuantity;
    private long mobifone50KMoney;
    private int mobifone50KQuantity;
    private long mobifone30KMoney;
    private int mobifone30KQuantity;
    private long mobifone20KMoney;
    private int mobifone20KQuantity;
    private long mobifone10KMoney;
    private int mobifone10KQuantity;
    private long gateMoney;
    private long gate5MMoney;
    private int gate5MQuantity;
    private long gate2MMoney;
    private int gate2MQuantity;
    private long gate1MMoney;
    private int gate1MQuantity;
    private long gate500KMoney;
    private int gate500KQuantity;
    private long gate300KMoney;
    private int gate300KQuantity;
    private long gate200KMoney;
    private int gate200KQuantity;
    private long gate100KMoney;
    private int gate100KQuantity;
    private long gate50KMoney;
    private int gate50KQuantity;
    private long gate30KMoney;
    private int gate30KQuantity;
    private long gate20KMoney;
    private int gate20KQuantity;
    private long gate10KMoney;
    private int gate10KQuantity;
    private long megaMoney;
    private long money5M;
    private int money5MQuantity;
    private long money3M;
    private int money3MQuantity;
    private long money2M;
    private int money2MQuantity;
    private long money1M;
    private int money1MQuantity;
    private long money500K;
    private int money500KQuantity;
    private long money300K;
    private int money300KQuantity;
    private long money200K;
    private int money200KQuantity;
    private long money100K;
    private int money100KQuantity;
    private long money50K;
    private int money50KQuantity;
    private long money20K;
    private int money20KQuantity;
    private long money10K;
    private int money10KQuantity;
    private long megaMoneyVat;
    private long money5MVat;
    private int money5MQuantityVat;
    private long money3MVat;
    private int money3MQuantityVat;
    private long money2MVat;
    private int money2MQuantityVat;
    private long money1MVat;
    private int money1MQuantityVat;
    private long money500KVat;
    private int money500KQuantityVat;
    private long money300KVat;
    private int money300KQuantityVat;
    private long money200KVat;
    private int money200KQuantityVat;
    private long money100KVat;
    private int money100KQuantityVat;
    private long money50KVat;
    private int money50KQuantityVat;
    private long money20KVat;
    private int money20KQuantityVat;
    private long money10KVat;
    private int money10KQuantityVat;
    private long vcoinMoney;
    private long vcoin10MMoney;
    private int vcoin10MQuantity;
    private long vcoin5MMoney;
    private int vcoin5MQuantity;
    private long vcoin2MMoney;
    private int vcoin2MQuantity;
    private long vcoin1MMoney;
    private int vcoin1MQuantity;
    private long vcoin500KMoney;
    private int vcoin500KQuantity;
    private long vcoin300KMoney;
    private int vcoin300KQuantity;
    private long vcoin200KMoney;
    private int vcoin200KQuantity;
    private long vcoin100KMoney;
    private int vcoin100KQuantity;
    private long vcoin50KMoney;
    private int vcoin50KQuantity;
    private long vcoin30KMoney;
    private int vcoin30KQuantity;
    private long vcoin20KMoney;
    private int vcoin20KQuantity;
    private long vcoin10KMoney;
    private int vcoin10KQuantity;

    public RechargeByCardDAOImpl() {
        this.totalMoney = 0L;
        this.webMoney = 0L;
        this.androidMoney = 0L;
        this.iosMoney = 0L;
        this.winphoneMoney = 0L;
        this.facebookMoney = 0L;
        this.desktopMoney = 0L;
        this.otherMoney = 0L;
        this.viettelMoney = 0L;
        this.viettel5MMoney = 0L;
        this.viettel5MQuantity = 0;
        this.viettel2MMoney = 0L;
        this.viettel2MQuantity = 0;
        this.viettel1MMoney = 0L;
        this.viettel1MQuantity = 0;
        this.viettel500KMoney = 0L;
        this.viettel500KQuantity = 0;
        this.viettel300KMoney = 0L;
        this.viettel300KQuantity = 0;
        this.viettel200KMoney = 0L;
        this.viettel200KQuantity = 0;
        this.viettel100KMoney = 0L;
        this.viettel100KQuantity = 0;
        this.viettel50KMoney = 0L;
        this.viettel50KQuantity = 0;
        this.viettel30KMoney = 0L;
        this.viettel30KQuantity = 0;
        this.viettel20KMoney = 0L;
        this.viettel20KQuantity = 0;
        this.viettel10KMoney = 0L;
        this.viettel10KQuantity = 0;
        this.vinaphoneMoney = 0L;
        this.vinaphone5MMoney = 0L;
        this.vinaphone5MQuantity = 0;
        this.vinaphone2MMoney = 0L;
        this.vinaphone2MQuantity = 0;
        this.vinaphone1MMoney = 0L;
        this.vinaphone1MQuantity = 0;
        this.vinaphone500KMoney = 0L;
        this.vinaphone500KQuantity = 0;
        this.vinaphone300KMoney = 0L;
        this.vinaphone300KQuantity = 0;
        this.vinaphone200KMoney = 0L;
        this.vinaphone200KQuantity = 0;
        this.vinaphone100KMoney = 0L;
        this.vinaphone100KQuantity = 0;
        this.vinaphone50KMoney = 0L;
        this.vinaphone50KQuantity = 0;
        this.vinaphone30KMoney = 0L;
        this.vinaphone30KQuantity = 0;
        this.vinaphone20KMoney = 0L;
        this.vinaphone20KQuantity = 0;
        this.vinaphone10KMoney = 0L;
        this.vinaphone10KQuantity = 0;
        this.mobifoneMoney = 0L;
        this.mobifone5MMoney = 0L;
        this.mobifone5MQuantity = 0;
        this.mobifone2MMoney = 0L;
        this.mobifone2MQuantity = 0;
        this.mobifone1MMoney = 0L;
        this.mobifone1MQuantity = 0;
        this.mobifone500KMoney = 0L;
        this.mobifone500KQuantity = 0;
        this.mobifone300KMoney = 0L;
        this.mobifone300KQuantity = 0;
        this.mobifone200KMoney = 0L;
        this.mobifone200KQuantity = 0;
        this.mobifone100KMoney = 0L;
        this.mobifone100KQuantity = 0;
        this.mobifone50KMoney = 0L;
        this.mobifone50KQuantity = 0;
        this.mobifone30KMoney = 0L;
        this.mobifone30KQuantity = 0;
        this.mobifone20KMoney = 0L;
        this.mobifone20KQuantity = 0;
        this.mobifone10KMoney = 0L;
        this.mobifone10KQuantity = 0;
        this.gateMoney = 0L;
        this.gate5MMoney = 0L;
        this.gate5MQuantity = 0;
        this.gate2MMoney = 0L;
        this.gate2MQuantity = 0;
        this.gate1MMoney = 0L;
        this.gate1MQuantity = 0;
        this.gate500KMoney = 0L;
        this.gate500KQuantity = 0;
        this.gate300KMoney = 0L;
        this.gate300KQuantity = 0;
        this.gate200KMoney = 0L;
        this.gate200KQuantity = 0;
        this.gate100KMoney = 0L;
        this.gate100KQuantity = 0;
        this.gate50KMoney = 0L;
        this.gate50KQuantity = 0;
        this.gate30KMoney = 0L;
        this.gate30KQuantity = 0;
        this.gate20KMoney = 0L;
        this.gate20KQuantity = 0;
        this.gate10KMoney = 0L;
        this.gate10KQuantity = 0;
        this.megaMoney = 0L;
        this.money5M = 0L;
        this.money5MQuantity = 0;
        this.money3M = 0L;
        this.money3MQuantity = 0;
        this.money2M = 0L;
        this.money2MQuantity = 0;
        this.money1M = 0L;
        this.money1MQuantity = 0;
        this.money500K = 0L;
        this.money500KQuantity = 0;
        this.money300K = 0L;
        this.money300KQuantity = 0;
        this.money200K = 0L;
        this.money200KQuantity = 0;
        this.money100K = 0L;
        this.money100KQuantity = 0;
        this.money50K = 0L;
        this.money50KQuantity = 0;
        this.money20K = 0L;
        this.money20KQuantity = 0;
        this.money10K = 0L;
        this.money10KQuantity = 0;
        this.megaMoneyVat = 0L;
        this.money5MVat = 0L;
        this.money5MQuantityVat = 0;
        this.money3MVat = 0L;
        this.money3MQuantityVat = 0;
        this.money2MVat = 0L;
        this.money2MQuantityVat = 0;
        this.money1MVat = 0L;
        this.money1MQuantityVat = 0;
        this.money500KVat = 0L;
        this.money500KQuantityVat = 0;
        this.money300KVat = 0L;
        this.money300KQuantityVat = 0;
        this.money200KVat = 0L;
        this.money200KQuantityVat = 0;
        this.money100KVat = 0L;
        this.money100KQuantityVat = 0;
        this.money50KVat = 0L;
        this.money50KQuantityVat = 0;
        this.money20KVat = 0L;
        this.money20KQuantityVat = 0;
        this.money10KVat = 0L;
        this.money10KQuantityVat = 0;
        this.vcoinMoney = 0L;
        this.vcoin10MMoney = 0L;
        this.vcoin10MQuantity = 0;
        this.vcoin5MMoney = 0L;
        this.vcoin5MQuantity = 0;
        this.vcoin2MMoney = 0L;
        this.vcoin2MQuantity = 0;
        this.vcoin1MMoney = 0L;
        this.vcoin1MQuantity = 0;
        this.vcoin500KMoney = 0L;
        this.vcoin500KQuantity = 0;
        this.vcoin300KMoney = 0L;
        this.vcoin300KQuantity = 0;
        this.vcoin200KMoney = 0L;
        this.vcoin200KQuantity = 0;
        this.vcoin100KMoney = 0L;
        this.vcoin100KQuantity = 0;
        this.vcoin50KMoney = 0L;
        this.vcoin50KQuantity = 0;
        this.vcoin30KMoney = 0L;
        this.vcoin30KQuantity = 0;
        this.vcoin20KMoney = 0L;
        this.vcoin20KQuantity = 0;
        this.vcoin10KMoney = 0L;
        this.vcoin10KQuantity = 0;
    }

    @Override
    public List<RechargeByCardReponse> searchRechargeByCard(final String nickName, final String provider, final String serial, final String pin, final String code, final String timeStart, final String timeEnd, final int page, final String transId) {
        final ArrayList<RechargeByCardReponse> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        FindIterable iterable = null;
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        final Document conditions = new Document();
        objsort.put("_id", -1);
        final int num_start = (page - 1) * 50;
        final int num_end = 50;
        if (transId != null && !transId.equals("")) {
            conditions.put("reference_id", transId);
        }
        if (nickName != null && !nickName.equals("")) {
            conditions.put("nick_name", nickName);
        }
        if (provider != null && !provider.equals("")) {
            conditions.put("provider", provider);
        }
        if (serial != null && !serial.equals("")) {
            conditions.put("serial", serial);
        }
        if (pin != null && !pin.equals("")) {
            conditions.put("pin", pin);
        }
        if (code != null && !code.equals("")) {
            conditions.put("code", Integer.parseInt(code));
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("time_log", obj);
        }
        iterable = db.getCollection("dvt_recharge_by_card").find(new Document(conditions)).sort(objsort).skip(num_start).limit(50).maxTime(30L, TimeUnit.SECONDS);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final RechargeByCardReponse bank = new RechargeByCardReponse();
                bank.referenceId = document.getString("reference_id");
                bank.nickName = document.getString("nick_name");
                bank.provider = document.getString("provider");
                bank.serial = document.getString("serial");
                bank.pin = document.getString("pin");
                bank.amount = document.getInteger("amount");
                bank.status = document.getInteger("status");
                bank.message = document.getString("message");
                bank.code = document.getInteger("code");
                bank.timelog = document.getString("time_log");
                bank.partner = document.getString("partner");
                results.add(bank);
            }
        });
        return results;
    }

    @Override
    public List<RechargeByCardReponse> exportDataRechargeByCard(final String provider, final String timeStart, final String timeEnd, final String amount, final String code) {
        final ArrayList<RechargeByCardReponse> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        FindIterable iterable = null;
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        final Document conditions = new Document();
        objsort.put("_id", -1);
        if (provider != null && !provider.equals("")) {
            conditions.put("provider", provider);
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("time_log", obj);
        }
        if (amount != null && !amount.equals("")) {
            conditions.put("amount", Integer.parseInt(amount));
        }
        if (code != null && !code.equals("")) {
            conditions.put("code", Integer.parseInt(code));
        }
        iterable = db.getCollection("dvt_recharge_by_card").find(new Document(conditions)).sort(objsort);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final RechargeByCardReponse bank = new RechargeByCardReponse();
                bank.referenceId = document.getString("reference_id");
                bank.nickName = document.getString("nick_name");
                bank.provider = document.getString("provider");
                bank.serial = document.getString("serial");
                bank.pin = document.getString("pin");
                bank.amount = document.getInteger("amount");
                bank.status = document.getInteger("status");
                bank.message = document.getString("message");
                bank.code = document.getInteger("code");
                bank.timelog = document.getString("time_log");
                bank.updateTime = document.getString("update_time");
                bank.partner = document.getString("partner");
                results.add(bank);
            }
        });
        return results;
    }

    @Override
    public int countSearchRechargeByCard(final String nickName, final String provider, final String serial, final String pin, final String code, final String timeStart, final String timeEnd, final String transId) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        final Document conditions = new Document();
        objsort.put("_id", -1);
        if (transId != null && !transId.equals("")) {
            conditions.put("reference_id", transId);
        }
        if (nickName != null && !nickName.equals("")) {
            conditions.put("nick_name", nickName);
        }
        if (provider != null && !provider.equals("")) {
            conditions.put("provider", provider);
        }
        if (serial != null && !serial.equals("")) {
            conditions.put("serial", serial);
        }
        if (pin != null && !pin.equals("")) {
            conditions.put("pin", pin);
        }
        if (code != null && !code.equals("")) {
            conditions.put("code", Integer.parseInt(code));
        }
        if (!timeStart.isEmpty() && !timeEnd.isEmpty()) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("time_log", obj);
        }
        return (int) db.getCollection("dvt_recharge_by_card").count(new Document(conditions));
    }

    @Override
    public long moneyTotal(final String nickName, final String provider, final String serial, final String pin, final String code, final String timeStart, final String timeEnd, final String transId) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        this.totalMoney = 0L;
        final Document conditions = new Document();
        objsort.put("_id", -1);
        if (transId != null && !transId.equals("")) {
            conditions.put("reference_id", transId);
        }
        if (nickName != null && !nickName.equals("")) {
            conditions.put("nick_name", nickName);
        }
        if (provider != null && !provider.equals("")) {
            conditions.put("provider", provider);
        }
        if (serial != null && !serial.equals("")) {
            conditions.put("serial", serial);
        }
        if (pin != null && !pin.equals("")) {
            conditions.put("pin", pin);
        }
        if (code != null && !code.equals("")) {
            conditions.put("code", Integer.parseInt(code));
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("time_log", obj);
        }
        FindIterable iterable = null;
        iterable = db.getCollection("dvt_recharge_by_card").find(new Document(conditions)).sort(objsort);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final RechargeByCardDAOImpl rechargeByCardDAOImpl = RechargeByCardDAOImpl.this;
                rechargeByCardDAOImpl.totalMoney += document.getInteger("amount");
            }
        });
        return this.totalMoney;
    }

    @Override
    public List<MoneyTotalRechargeByCardReponse> doiSoatRechargeByCard(final int code, final String timeStart, final String timeEnd) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        final Document conditions = new Document();
        this.totalMoney = 0L;
        this.viettelMoney = 0L;
        this.viettel5MMoney = 0L;
        this.viettel5MQuantity = 0;
        this.viettel2MMoney = 0L;
        this.viettel2MQuantity = 0;
        this.viettel1MMoney = 0L;
        this.viettel1MQuantity = 0;
        this.viettel500KMoney = 0L;
        this.viettel500KQuantity = 0;
        this.viettel300KMoney = 0L;
        this.viettel300KQuantity = 0;
        this.viettel200KMoney = 0L;
        this.viettel200KQuantity = 0;
        this.viettel100KMoney = 0L;
        this.viettel100KQuantity = 0;
        this.viettel50KMoney = 0L;
        this.viettel50KQuantity = 0;
        this.viettel30KMoney = 0L;
        this.viettel30KQuantity = 0;
        this.viettel20KMoney = 0L;
        this.viettel20KQuantity = 0;
        this.viettel10KMoney = 0L;
        this.viettel10KQuantity = 0;
        this.vinaphoneMoney = 0L;
        this.vinaphone5MMoney = 0L;
        this.vinaphone5MQuantity = 0;
        this.vinaphone2MMoney = 0L;
        this.vinaphone2MQuantity = 0;
        this.vinaphone1MMoney = 0L;
        this.vinaphone1MQuantity = 0;
        this.vinaphone500KMoney = 0L;
        this.vinaphone500KQuantity = 0;
        this.vinaphone300KMoney = 0L;
        this.vinaphone300KQuantity = 0;
        this.vinaphone200KMoney = 0L;
        this.vinaphone200KQuantity = 0;
        this.vinaphone100KMoney = 0L;
        this.vinaphone100KQuantity = 0;
        this.vinaphone50KMoney = 0L;
        this.vinaphone50KQuantity = 0;
        this.vinaphone30KMoney = 0L;
        this.vinaphone30KQuantity = 0;
        this.vinaphone20KMoney = 0L;
        this.vinaphone20KQuantity = 0;
        this.vinaphone10KMoney = 0L;
        this.vinaphone10KQuantity = 0;
        this.mobifoneMoney = 0L;
        this.mobifone5MMoney = 0L;
        this.mobifone5MQuantity = 0;
        this.mobifone2MMoney = 0L;
        this.mobifone2MQuantity = 0;
        this.mobifone1MMoney = 0L;
        this.mobifone1MQuantity = 0;
        this.mobifone500KMoney = 0L;
        this.mobifone500KQuantity = 0;
        this.mobifone300KMoney = 0L;
        this.mobifone300KQuantity = 0;
        this.mobifone200KMoney = 0L;
        this.mobifone200KQuantity = 0;
        this.mobifone100KMoney = 0L;
        this.mobifone100KQuantity = 0;
        this.mobifone50KMoney = 0L;
        this.mobifone50KQuantity = 0;
        this.mobifone30KMoney = 0L;
        this.mobifone30KQuantity = 0;
        this.mobifone20KMoney = 0L;
        this.mobifone20KQuantity = 0;
        this.mobifone10KMoney = 0L;
        this.mobifone10KQuantity = 0;
        this.gateMoney = 0L;
        this.gate5MMoney = 0L;
        this.gate5MQuantity = 0;
        this.gate2MMoney = 0L;
        this.gate2MQuantity = 0;
        this.gate1MMoney = 0L;
        this.gate1MQuantity = 0;
        this.gate500KMoney = 0L;
        this.gate500KQuantity = 0;
        this.gate300KMoney = 0L;
        this.gate300KQuantity = 0;
        this.gate200KMoney = 0L;
        this.gate200KQuantity = 0;
        this.gate100KMoney = 0L;
        this.gate100KQuantity = 0;
        this.gate50KMoney = 0L;
        this.gate50KQuantity = 0;
        this.gate30KMoney = 0L;
        this.gate30KQuantity = 0;
        this.gate20KMoney = 0L;
        this.gate20KQuantity = 0;
        this.gate10KMoney = 0L;
        this.gate10KQuantity = 0;
        this.megaMoney = 0L;
        this.money5M = 0L;
        this.money5MQuantity = 0;
        this.money3M = 0L;
        this.money3MQuantity = 0;
        this.money2M = 0L;
        this.money2MQuantity = 0;
        this.money1M = 0L;
        this.money1MQuantity = 0;
        this.money500K = 0L;
        this.money500KQuantity = 0;
        this.money300K = 0L;
        this.money300KQuantity = 0;
        this.money200K = 0L;
        this.money200KQuantity = 0;
        this.money100K = 0L;
        this.money100KQuantity = 0;
        this.money50K = 0L;
        this.money50KQuantity = 0;
        this.money20K = 0L;
        this.money20KQuantity = 0;
        this.money10K = 0L;
        this.money10KQuantity = 0;
        this.megaMoneyVat = 0L;
        this.money5MVat = 0L;
        this.money5MQuantityVat = 0;
        this.money3MVat = 0L;
        this.money3MQuantityVat = 0;
        this.money2MVat = 0L;
        this.money2MQuantityVat = 0;
        this.money1MVat = 0L;
        this.money1MQuantityVat = 0;
        this.money500KVat = 0L;
        this.money500KQuantityVat = 0;
        this.money300KVat = 0L;
        this.money300KQuantityVat = 0;
        this.money200KVat = 0L;
        this.money200KQuantityVat = 0;
        this.money100KVat = 0L;
        this.money100KQuantityVat = 0;
        this.money50KVat = 0L;
        this.money50KQuantityVat = 0;
        this.money20KVat = 0L;
        this.money20KQuantityVat = 0;
        this.money10KVat = 0L;
        this.money10KQuantityVat = 0;
        this.vcoinMoney = 0L;
        this.vcoin10MMoney = 0L;
        this.vcoin10MQuantity = 0;
        this.vcoin5MMoney = 0L;
        this.vcoin5MQuantity = 0;
        this.vcoin2MMoney = 0L;
        this.vcoin2MQuantity = 0;
        this.vcoin1MMoney = 0L;
        this.vcoin1MQuantity = 0;
        this.vcoin500KMoney = 0L;
        this.vcoin500KQuantity = 0;
        this.vcoin300KMoney = 0L;
        this.vcoin300KQuantity = 0;
        this.vcoin200KMoney = 0L;
        this.vcoin200KQuantity = 0;
        this.vcoin100KMoney = 0L;
        this.vcoin100KQuantity = 0;
        this.vcoin50KMoney = 0L;
        this.vcoin50KQuantity = 0;
        this.vcoin30KMoney = 0L;
        this.vcoin30KQuantity = 0;
        this.vcoin20KMoney = 0L;
        this.vcoin20KQuantity = 0;
        this.vcoin10KMoney = 0L;
        this.vcoin10KQuantity = 0;
        objsort.put("_id", -1);
        conditions.put("code", code);
        if (!timeStart.isEmpty() && !timeEnd.isEmpty()) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("time_log", obj);
        }
        FindIterable iterable = null;
        iterable = db.getCollection("epay_recharge_by_mega_card").find(new Document(conditions)).sort(objsort);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                RechargeByCardDAOImpl rechargeByCardDAOImpl = RechargeByCardDAOImpl.this;
                rechargeByCardDAOImpl.totalMoney += document.getInteger("amount");
                rechargeByCardDAOImpl = RechargeByCardDAOImpl.this;
                rechargeByCardDAOImpl.megaMoney += document.getInteger("amount");
                switch (document.getInteger("amount")) {
                    case 5000000: {
                        RechargeByCardDAOImpl.this.money5MQuantity++;
                        rechargeByCardDAOImpl = RechargeByCardDAOImpl.this;
                        rechargeByCardDAOImpl.money5M += 5000000L;
                        break;
                    }
                    case 3000000: {
                        RechargeByCardDAOImpl.this.money3MQuantity++;
                        rechargeByCardDAOImpl = RechargeByCardDAOImpl.this;
                        rechargeByCardDAOImpl.money3M += 3000000L;
                        break;
                    }
                    case 2000000: {
                        RechargeByCardDAOImpl.this.money2MQuantity++;
                        rechargeByCardDAOImpl = RechargeByCardDAOImpl.this;
                        rechargeByCardDAOImpl.money2M += 2000000L;
                        break;
                    }
                    case 1000000: {
                        RechargeByCardDAOImpl.this.money1MQuantity++;
                        rechargeByCardDAOImpl = RechargeByCardDAOImpl.this;
                        rechargeByCardDAOImpl.money1M += 1000000L;
                        break;
                    }
                    case 500000: {
                        RechargeByCardDAOImpl.this.money500KQuantity++;
                        rechargeByCardDAOImpl = RechargeByCardDAOImpl.this;
                        rechargeByCardDAOImpl.money500K += 500000L;
                        break;
                    }
                    case 300000: {
                        RechargeByCardDAOImpl.this.money300KQuantity++;
                        rechargeByCardDAOImpl = RechargeByCardDAOImpl.this;
                        rechargeByCardDAOImpl.money300K += 300000L;
                        break;
                    }
                    case 200000: {
                        RechargeByCardDAOImpl.this.money200KQuantity++;
                        rechargeByCardDAOImpl = RechargeByCardDAOImpl.this;
                        rechargeByCardDAOImpl.money200K += 200000L;
                        break;
                    }
                    case 100000: {
                        RechargeByCardDAOImpl.this.money100KQuantity++;
                        rechargeByCardDAOImpl = RechargeByCardDAOImpl.this;
                        rechargeByCardDAOImpl.money100K += 100000L;
                        break;
                    }
                    case 50000: {
                        RechargeByCardDAOImpl.this.money50KQuantity++;
                        rechargeByCardDAOImpl = RechargeByCardDAOImpl.this;
                        rechargeByCardDAOImpl.money50K += 50000L;
                        break;
                    }
                    case 20000: {
                        RechargeByCardDAOImpl.this.money20KQuantity++;
                        rechargeByCardDAOImpl = RechargeByCardDAOImpl.this;
                        rechargeByCardDAOImpl.money20K += 20000L;
                        break;
                    }
                    case 10000: {
                        RechargeByCardDAOImpl.this.money10KQuantity++;
                        rechargeByCardDAOImpl = RechargeByCardDAOImpl.this;
                        rechargeByCardDAOImpl.money10K += 10000L;
                        break;
                    }
                }
            }
        });
        iterable = db.getCollection("dvt_recharge_by_card").find(new Document(conditions)).sort(objsort);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final RechargeByCardDAOImpl rechargeByCardDAOImpl = RechargeByCardDAOImpl.this;
                rechargeByCardDAOImpl.totalMoney += document.getInteger("amount");
                final String string = document.getString("provider");
                int n = -1;
                switch (string.hashCode()) {
                    case -758522174: {
                        if (string.equals("MegaCard")) {
                            n = 0;
                            break;
                        }
                        break;
                    }
                    case 2118812185: {
                        if (string.equals("Viettel")) {
                            n = 1;
                            break;
                        }
                        break;
                    }
                    case -785942168: {
                        if (string.equals("Vinaphone")) {
                            n = 2;
                            break;
                        }
                        break;
                    }
                    case -608121527: {
                        if (string.equals("Mobifone")) {
                            n = 3;
                            break;
                        }
                        break;
                    }
                    case 2212075: {
                        if (string.equals("Gate")) {
                            n = 4;
                            break;
                        }
                        break;
                    }
                    case 82482151: {
                        if (string.equals("Vcoin")) {
                            n = 5;
                            break;
                        }
                        break;
                    }
                }
                Label_3827:
                {
                    switch (n) {
                        case 0: {
                            String userNameMega = "";
                            final String s;
                            userNameMega = (s = (document.containsKey("user_mega") ? document.getString("user_mega") : "CTT_VINPLAY"));
                            switch (s) {
                                case "CTT_VINPLAY": {
                                    RechargeByCardDAOImpl rechargeByCardDAOImpl2 = RechargeByCardDAOImpl.this;
                                    rechargeByCardDAOImpl2.megaMoney += document.getInteger("amount");
                                    switch (document.getInteger("amount")) {
                                        case 5000000: {
                                            RechargeByCardDAOImpl.this.money5MQuantity++;
                                            rechargeByCardDAOImpl2 = RechargeByCardDAOImpl.this;
                                            rechargeByCardDAOImpl2.money5M += 5000000L;
                                            break Label_3827;
                                        }
                                        case 3000000: {
                                            RechargeByCardDAOImpl.this.money3MQuantity++;
                                            rechargeByCardDAOImpl2 = RechargeByCardDAOImpl.this;
                                            rechargeByCardDAOImpl2.money3M += 3000000L;
                                            break Label_3827;
                                        }
                                        case 2000000: {
                                            RechargeByCardDAOImpl.this.money2MQuantity++;
                                            rechargeByCardDAOImpl2 = RechargeByCardDAOImpl.this;
                                            rechargeByCardDAOImpl2.money2M += 2000000L;
                                            break Label_3827;
                                        }
                                        case 1000000: {
                                            RechargeByCardDAOImpl.this.money1MQuantity++;
                                            rechargeByCardDAOImpl2 = RechargeByCardDAOImpl.this;
                                            rechargeByCardDAOImpl2.money1M += 1000000L;
                                            break Label_3827;
                                        }
                                        case 500000: {
                                            RechargeByCardDAOImpl.this.money500KQuantity++;
                                            rechargeByCardDAOImpl2 = RechargeByCardDAOImpl.this;
                                            rechargeByCardDAOImpl2.money500K += 500000L;
                                            break Label_3827;
                                        }
                                        case 300000: {
                                            RechargeByCardDAOImpl.this.money300KQuantity++;
                                            rechargeByCardDAOImpl2 = RechargeByCardDAOImpl.this;
                                            rechargeByCardDAOImpl2.money300K += 300000L;
                                            break Label_3827;
                                        }
                                        case 200000: {
                                            RechargeByCardDAOImpl.this.money200KQuantity++;
                                            rechargeByCardDAOImpl2 = RechargeByCardDAOImpl.this;
                                            rechargeByCardDAOImpl2.money200K += 200000L;
                                            break Label_3827;
                                        }
                                        case 100000: {
                                            RechargeByCardDAOImpl.this.money100KQuantity++;
                                            rechargeByCardDAOImpl2 = RechargeByCardDAOImpl.this;
                                            rechargeByCardDAOImpl2.money100K += 100000L;
                                            break Label_3827;
                                        }
                                        case 50000: {
                                            RechargeByCardDAOImpl.this.money50KQuantity++;
                                            rechargeByCardDAOImpl2 = RechargeByCardDAOImpl.this;
                                            rechargeByCardDAOImpl2.money50K += 50000L;
                                            break Label_3827;
                                        }
                                        case 20000: {
                                            RechargeByCardDAOImpl.this.money20KQuantity++;
                                            rechargeByCardDAOImpl2 = RechargeByCardDAOImpl.this;
                                            rechargeByCardDAOImpl2.money20K += 20000L;
                                            break Label_3827;
                                        }
                                        case 10000: {
                                            RechargeByCardDAOImpl.this.money10KQuantity++;
                                            rechargeByCardDAOImpl2 = RechargeByCardDAOImpl.this;
                                            rechargeByCardDAOImpl2.money10K += 10000L;
                                            break Label_3827;
                                        }
                                        default: {
                                            break Label_3827;
                                        }
                                    }

                                }
                                case "VINPLAY": {
                                    RechargeByCardDAOImpl rechargeByCardDAOImpl3 = RechargeByCardDAOImpl.this;
                                    rechargeByCardDAOImpl3.megaMoneyVat += document.getInteger("amount");
                                    switch (document.getInteger("amount")) {
                                        case 5000000: {
                                            RechargeByCardDAOImpl.this.money5MQuantityVat++;
                                            rechargeByCardDAOImpl3 = RechargeByCardDAOImpl.this;
                                            rechargeByCardDAOImpl3.money5MVat += 5000000L;
                                            break Label_3827;
                                        }
                                        case 3000000: {
                                            RechargeByCardDAOImpl.this.money3MQuantityVat++;
                                            rechargeByCardDAOImpl3 = RechargeByCardDAOImpl.this;
                                            rechargeByCardDAOImpl3.money3MVat += 3000000L;
                                            break Label_3827;
                                        }
                                        case 2000000: {
                                            RechargeByCardDAOImpl.this.money2MQuantityVat++;
                                            rechargeByCardDAOImpl3 = RechargeByCardDAOImpl.this;
                                            rechargeByCardDAOImpl3.money2MVat += 2000000L;
                                            break Label_3827;
                                        }
                                        case 1000000: {
                                            RechargeByCardDAOImpl.this.money1MQuantityVat++;
                                            rechargeByCardDAOImpl3 = RechargeByCardDAOImpl.this;
                                            rechargeByCardDAOImpl3.money1MVat += 1000000L;
                                            break Label_3827;
                                        }
                                        case 500000: {
                                            RechargeByCardDAOImpl.this.money500KQuantityVat++;
                                            rechargeByCardDAOImpl3 = RechargeByCardDAOImpl.this;
                                            rechargeByCardDAOImpl3.money500KVat += 500000L;
                                            break Label_3827;
                                        }
                                        case 300000: {
                                            RechargeByCardDAOImpl.this.money300KQuantityVat++;
                                            rechargeByCardDAOImpl3 = RechargeByCardDAOImpl.this;
                                            rechargeByCardDAOImpl3.money300KVat += 300000L;
                                            break Label_3827;
                                        }
                                        case 200000: {
                                            RechargeByCardDAOImpl.this.money200KQuantityVat++;
                                            rechargeByCardDAOImpl3 = RechargeByCardDAOImpl.this;
                                            rechargeByCardDAOImpl3.money200KVat += 200000L;
                                            break Label_3827;
                                        }
                                        case 100000: {
                                            RechargeByCardDAOImpl.this.money100KQuantityVat++;
                                            rechargeByCardDAOImpl3 = RechargeByCardDAOImpl.this;
                                            rechargeByCardDAOImpl3.money100KVat += 100000L;
                                            break Label_3827;
                                        }
                                        case 50000: {
                                            RechargeByCardDAOImpl.this.money50KQuantityVat++;
                                            rechargeByCardDAOImpl3 = RechargeByCardDAOImpl.this;
                                            rechargeByCardDAOImpl3.money50KVat += 50000L;
                                            break Label_3827;
                                        }
                                        case 20000: {
                                            RechargeByCardDAOImpl.this.money20KQuantityVat++;
                                            rechargeByCardDAOImpl3 = RechargeByCardDAOImpl.this;
                                            rechargeByCardDAOImpl3.money20KVat += 20000L;
                                            break Label_3827;
                                        }
                                        case 10000: {
                                            RechargeByCardDAOImpl.this.money10KQuantityVat++;
                                            rechargeByCardDAOImpl3 = RechargeByCardDAOImpl.this;
                                            rechargeByCardDAOImpl3.money10KVat += 10000L;
                                            break Label_3827;
                                        }
                                        default: {
                                            break Label_3827;
                                        }
                                    }

                                }
                                default: {
                                    break Label_3827;
                                }
                            }

                        }
                        case 1: {
                            RechargeByCardDAOImpl userNameMega2 = RechargeByCardDAOImpl.this;
                            userNameMega2.viettelMoney += document.getInteger("amount");
                            switch (document.getInteger("amount")) {
                                case 5000000: {
                                    RechargeByCardDAOImpl.this.viettel5MQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.viettel5MMoney += 5000000L;
                                    break Label_3827;
                                }
                                case 2000000: {
                                    RechargeByCardDAOImpl.this.viettel2MQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.viettel2MMoney += 2000000L;
                                    break Label_3827;
                                }
                                case 1000000: {
                                    RechargeByCardDAOImpl.this.viettel1MQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.viettel1MMoney += 1000000L;
                                    break Label_3827;
                                }
                                case 500000: {
                                    RechargeByCardDAOImpl.this.viettel500KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.viettel500KMoney += 500000L;
                                    break Label_3827;
                                }
                                case 300000: {
                                    RechargeByCardDAOImpl.this.viettel300KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.viettel300KMoney += 300000L;
                                    break Label_3827;
                                }
                                case 200000: {
                                    RechargeByCardDAOImpl.this.viettel200KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.viettel200KMoney += 200000L;
                                    break Label_3827;
                                }
                                case 100000: {
                                    RechargeByCardDAOImpl.this.viettel100KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.viettel100KMoney += 100000L;
                                    break Label_3827;
                                }
                                case 50000: {
                                    RechargeByCardDAOImpl.this.viettel50KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.viettel50KMoney += 50000L;
                                    break Label_3827;
                                }
                                case 30000: {
                                    RechargeByCardDAOImpl.this.viettel30KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.viettel30KMoney += 30000L;
                                    break Label_3827;
                                }
                                case 20000: {
                                    RechargeByCardDAOImpl.this.viettel20KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.viettel20KMoney += 20000L;
                                    break Label_3827;
                                }
                                case 10000: {
                                    RechargeByCardDAOImpl.this.viettel10KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.viettel10KMoney += 10000L;
                                    break Label_3827;
                                }
                                default: {
                                    break Label_3827;
                                }
                            }

                        }
                        case 2: {
                            RechargeByCardDAOImpl userNameMega2 = RechargeByCardDAOImpl.this;
                            userNameMega2.vinaphoneMoney += document.getInteger("amount");
                            switch (document.getInteger("amount")) {
                                case 5000000: {
                                    RechargeByCardDAOImpl.this.vinaphone5MQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.vinaphone5MMoney += 5000000L;
                                    break Label_3827;
                                }
                                case 2000000: {
                                    RechargeByCardDAOImpl.this.vinaphone2MQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.vinaphone2MMoney += 2000000L;
                                    break Label_3827;
                                }
                                case 1000000: {
                                    RechargeByCardDAOImpl.this.vinaphone1MQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.vinaphone1MMoney += 1000000L;
                                    break Label_3827;
                                }
                                case 500000: {
                                    RechargeByCardDAOImpl.this.vinaphone500KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.vinaphone500KMoney += 500000L;
                                    break Label_3827;
                                }
                                case 300000: {
                                    RechargeByCardDAOImpl.this.vinaphone300KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.vinaphone300KMoney += 300000L;
                                    break Label_3827;
                                }
                                case 200000: {
                                    RechargeByCardDAOImpl.this.vinaphone200KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.vinaphone200KMoney += 200000L;
                                    break Label_3827;
                                }
                                case 100000: {
                                    RechargeByCardDAOImpl.this.vinaphone100KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.vinaphone100KMoney += 100000L;
                                    break Label_3827;
                                }
                                case 50000: {
                                    RechargeByCardDAOImpl.this.vinaphone50KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.vinaphone50KMoney += 50000L;
                                    break Label_3827;
                                }
                                case 30000: {
                                    RechargeByCardDAOImpl.this.vinaphone30KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.vinaphone30KMoney += 30000L;
                                    break Label_3827;
                                }
                                case 20000: {
                                    RechargeByCardDAOImpl.this.vinaphone20KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.vinaphone20KMoney += 20000L;
                                    break Label_3827;
                                }
                                case 10000: {
                                    RechargeByCardDAOImpl.this.vinaphone10KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.vinaphone10KMoney += 10000L;
                                    break Label_3827;
                                }
                                default: {
                                    break Label_3827;
                                }
                            }

                        }
                        case 3: {
                            RechargeByCardDAOImpl userNameMega2 = RechargeByCardDAOImpl.this;
                            userNameMega2.mobifoneMoney += document.getInteger("amount");
                            switch (document.getInteger("amount")) {
                                case 5000000: {
                                    RechargeByCardDAOImpl.this.mobifone5MQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.mobifone5MMoney += 5000000L;
                                    break Label_3827;
                                }
                                case 2000000: {
                                    RechargeByCardDAOImpl.this.mobifone2MQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.mobifone2MMoney += 2000000L;
                                    break Label_3827;
                                }
                                case 1000000: {
                                    RechargeByCardDAOImpl.this.mobifone1MQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.mobifone1MMoney += 1000000L;
                                    break Label_3827;
                                }
                                case 500000: {
                                    RechargeByCardDAOImpl.this.mobifone500KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.mobifone500KMoney += 500000L;
                                    break Label_3827;
                                }
                                case 300000: {
                                    RechargeByCardDAOImpl.this.mobifone300KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.mobifone300KMoney += 300000L;
                                    break Label_3827;
                                }
                                case 200000: {
                                    RechargeByCardDAOImpl.this.mobifone200KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.mobifone200KMoney += 200000L;
                                    break Label_3827;
                                }
                                case 100000: {
                                    RechargeByCardDAOImpl.this.mobifone100KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.mobifone100KMoney += 100000L;
                                    break Label_3827;
                                }
                                case 50000: {
                                    RechargeByCardDAOImpl.this.mobifone50KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.mobifone50KMoney += 50000L;
                                    break Label_3827;
                                }
                                case 30000: {
                                    RechargeByCardDAOImpl.this.mobifone30KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.mobifone30KMoney += 30000L;
                                    break Label_3827;
                                }
                                case 20000: {
                                    RechargeByCardDAOImpl.this.mobifone20KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.mobifone20KMoney += 20000L;
                                    break Label_3827;
                                }
                                case 10000: {
                                    RechargeByCardDAOImpl.this.mobifone10KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.mobifone10KMoney += 10000L;
                                    break Label_3827;
                                }
                                default: {
                                    break Label_3827;
                                }
                            }

                        }
                        case 4: {
                            RechargeByCardDAOImpl userNameMega2 = RechargeByCardDAOImpl.this;
                            userNameMega2.gateMoney += document.getInteger("amount");
                            switch (document.getInteger("amount")) {
                                case 5000000: {
                                    RechargeByCardDAOImpl.this.gate5MQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.gate5MMoney += 5000000L;
                                    break Label_3827;
                                }
                                case 2000000: {
                                    RechargeByCardDAOImpl.this.gate2MQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.gate2MMoney += 2000000L;
                                    break Label_3827;
                                }
                                case 1000000: {
                                    RechargeByCardDAOImpl.this.gate1MQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.gate1MMoney += 1000000L;
                                    break Label_3827;
                                }
                                case 500000: {
                                    RechargeByCardDAOImpl.this.gate500KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.gate500KMoney += 500000L;
                                    break Label_3827;
                                }
                                case 300000: {
                                    RechargeByCardDAOImpl.this.gate300KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.gate300KMoney += 300000L;
                                    break Label_3827;
                                }
                                case 200000: {
                                    RechargeByCardDAOImpl.this.gate200KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.gate200KMoney += 200000L;
                                    break Label_3827;
                                }
                                case 100000: {
                                    RechargeByCardDAOImpl.this.gate100KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.gate100KMoney += 100000L;
                                    break Label_3827;
                                }
                                case 50000: {
                                    RechargeByCardDAOImpl.this.gate50KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.gate50KMoney += 50000L;
                                    break Label_3827;
                                }
                                case 30000: {
                                    RechargeByCardDAOImpl.this.gate30KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.gate30KMoney += 30000L;
                                    break Label_3827;
                                }
                                case 20000: {
                                    RechargeByCardDAOImpl.this.gate20KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.gate20KMoney += 20000L;
                                    break Label_3827;
                                }
                                case 10000: {
                                    RechargeByCardDAOImpl.this.gate10KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.gate10KMoney += 10000L;
                                    break Label_3827;
                                }
                                default: {
                                    break Label_3827;
                                }
                            }

                        }
                        case 5: {
                            RechargeByCardDAOImpl userNameMega2 = RechargeByCardDAOImpl.this;
                            userNameMega2.vcoinMoney += document.getInteger("amount");
                            switch (document.getInteger("amount")) {
                                case 10000000: {
                                    RechargeByCardDAOImpl.this.vcoin10MQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.vcoin10MMoney += 10000000L;
                                    break Label_3827;
                                }
                                case 5000000: {
                                    RechargeByCardDAOImpl.this.vcoin5MQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.vcoin5MMoney += 5000000L;
                                    break Label_3827;
                                }
                                case 2000000: {
                                    RechargeByCardDAOImpl.this.vcoin2MQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.vcoin2MMoney += 2000000L;
                                    break Label_3827;
                                }
                                case 1000000: {
                                    RechargeByCardDAOImpl.this.vcoin1MQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.vcoin1MMoney += 1000000L;
                                    break Label_3827;
                                }
                                case 500000: {
                                    RechargeByCardDAOImpl.this.vcoin500KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.vcoin500KMoney += 500000L;
                                    break Label_3827;
                                }
                                case 300000: {
                                    RechargeByCardDAOImpl.this.vcoin300KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.vcoin300KMoney += 300000L;
                                    break Label_3827;
                                }
                                case 200000: {
                                    RechargeByCardDAOImpl.this.vcoin200KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.vcoin200KMoney += 200000L;
                                    break Label_3827;
                                }
                                case 100000: {
                                    RechargeByCardDAOImpl.this.vcoin100KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.vcoin100KMoney += 100000L;
                                    break Label_3827;
                                }
                                case 50000: {
                                    RechargeByCardDAOImpl.this.vcoin50KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.vcoin50KMoney += 50000L;
                                    break Label_3827;
                                }
                                case 30000: {
                                    RechargeByCardDAOImpl.this.vcoin30KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.vcoin30KMoney += 30000L;
                                    break Label_3827;
                                }
                                case 20000: {
                                    RechargeByCardDAOImpl.this.vcoin20KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.vcoin20KMoney += 20000L;
                                    break Label_3827;
                                }
                                case 10000: {
                                    RechargeByCardDAOImpl.this.vcoin10KQuantity++;
                                    userNameMega2 = RechargeByCardDAOImpl.this;
                                    userNameMega2.vcoin10KMoney += 10000L;
                                    break Label_3827;
                                }
                                default: {
                                }
                            }

                        }
                    }
                }
            }
        });
        final ArrayList<MoneyTotalRechargeByCardReponse> listReponse = new ArrayList<>();
        final MoneyTotalRechargeByCardReponse tong = new MoneyTotalRechargeByCardReponse();
        tong.setName("Tong");
        tong.setValue(this.totalMoney);
        tong.setTrans(null);
        listReponse.add(tong);
        final MoneyTotalRechargeByCardReponse viettel = new MoneyTotalRechargeByCardReponse();
        final ArrayList<MoneyTotalFollowFaceValue> listViettelMoneyFollowFace = new ArrayList<>();
        final MoneyTotalFollowFaceValue viettelMoney5MFollowFace = new MoneyTotalFollowFaceValue();
        viettelMoney5MFollowFace.setFaceValue(5000000);
        viettelMoney5MFollowFace.setQuantity(this.viettel5MQuantity);
        viettelMoney5MFollowFace.setMoneyTotal(this.viettel5MMoney);
        listViettelMoneyFollowFace.add(viettelMoney5MFollowFace);
        final MoneyTotalFollowFaceValue viettelMoney2MFollowFace = new MoneyTotalFollowFaceValue();
        viettelMoney2MFollowFace.setFaceValue(2000000);
        viettelMoney2MFollowFace.setQuantity(this.viettel2MQuantity);
        viettelMoney2MFollowFace.setMoneyTotal(this.viettel2MMoney);
        listViettelMoneyFollowFace.add(viettelMoney2MFollowFace);
        final MoneyTotalFollowFaceValue viettelMoney1MFollowFace = new MoneyTotalFollowFaceValue();
        viettelMoney1MFollowFace.setFaceValue(1000000);
        viettelMoney1MFollowFace.setQuantity(this.viettel1MQuantity);
        viettelMoney1MFollowFace.setMoneyTotal(this.viettel1MMoney);
        listViettelMoneyFollowFace.add(viettelMoney1MFollowFace);
        final MoneyTotalFollowFaceValue viettelMoney500KFollowFace = new MoneyTotalFollowFaceValue();
        viettelMoney500KFollowFace.setFaceValue(500000);
        viettelMoney500KFollowFace.setQuantity(this.viettel500KQuantity);
        viettelMoney500KFollowFace.setMoneyTotal(this.viettel500KMoney);
        listViettelMoneyFollowFace.add(viettelMoney500KFollowFace);
        final MoneyTotalFollowFaceValue viettelMoney300KFollowFace = new MoneyTotalFollowFaceValue();
        viettelMoney300KFollowFace.setFaceValue(300000);
        viettelMoney300KFollowFace.setQuantity(this.viettel300KQuantity);
        viettelMoney300KFollowFace.setMoneyTotal(this.viettel300KMoney);
        listViettelMoneyFollowFace.add(viettelMoney300KFollowFace);
        final MoneyTotalFollowFaceValue viettelMoney200KFollowFace = new MoneyTotalFollowFaceValue();
        viettelMoney200KFollowFace.setFaceValue(200000);
        viettelMoney200KFollowFace.setQuantity(this.viettel200KQuantity);
        viettelMoney200KFollowFace.setMoneyTotal(this.viettel200KMoney);
        listViettelMoneyFollowFace.add(viettelMoney200KFollowFace);
        final MoneyTotalFollowFaceValue viettelMoney100KFollowFace = new MoneyTotalFollowFaceValue();
        viettelMoney100KFollowFace.setFaceValue(100000);
        viettelMoney100KFollowFace.setQuantity(this.viettel100KQuantity);
        viettelMoney100KFollowFace.setMoneyTotal(this.viettel100KMoney);
        listViettelMoneyFollowFace.add(viettelMoney100KFollowFace);
        final MoneyTotalFollowFaceValue viettelMoney50KFollowFace = new MoneyTotalFollowFaceValue();
        viettelMoney50KFollowFace.setFaceValue(50000);
        viettelMoney50KFollowFace.setQuantity(this.viettel50KQuantity);
        viettelMoney50KFollowFace.setMoneyTotal(this.viettel50KMoney);
        listViettelMoneyFollowFace.add(viettelMoney50KFollowFace);
        final MoneyTotalFollowFaceValue viettelMoney30KFollowFace = new MoneyTotalFollowFaceValue();
        viettelMoney30KFollowFace.setFaceValue(30000);
        viettelMoney30KFollowFace.setQuantity(this.viettel30KQuantity);
        viettelMoney30KFollowFace.setMoneyTotal(this.viettel30KMoney);
        listViettelMoneyFollowFace.add(viettelMoney30KFollowFace);
        final MoneyTotalFollowFaceValue viettelMoney20KFollowFace = new MoneyTotalFollowFaceValue();
        viettelMoney20KFollowFace.setFaceValue(20000);
        viettelMoney20KFollowFace.setQuantity(this.viettel20KQuantity);
        viettelMoney20KFollowFace.setMoneyTotal(this.viettel20KMoney);
        listViettelMoneyFollowFace.add(viettelMoney20KFollowFace);
        final MoneyTotalFollowFaceValue viettelMoney10KFollowFace = new MoneyTotalFollowFaceValue();
        viettelMoney10KFollowFace.setFaceValue(10000);
        viettelMoney10KFollowFace.setQuantity(this.viettel10KQuantity);
        viettelMoney10KFollowFace.setMoneyTotal(this.viettel10KMoney);
        listViettelMoneyFollowFace.add(viettelMoney10KFollowFace);
        viettel.setName("Viettel");
        viettel.setValue(this.viettelMoney);
        viettel.setTrans(listViettelMoneyFollowFace);
        listReponse.add(viettel);
        final MoneyTotalRechargeByCardReponse vinaphone = new MoneyTotalRechargeByCardReponse();
        final ArrayList<MoneyTotalFollowFaceValue> listVinaphoneMoneyFollowFace = new ArrayList<>();
        final MoneyTotalFollowFaceValue vinaphoneMoney5MFollowFace = new MoneyTotalFollowFaceValue();
        vinaphoneMoney5MFollowFace.setFaceValue(5000000);
        vinaphoneMoney5MFollowFace.setQuantity(this.vinaphone5MQuantity);
        vinaphoneMoney5MFollowFace.setMoneyTotal(this.vinaphone5MMoney);
        listVinaphoneMoneyFollowFace.add(vinaphoneMoney5MFollowFace);
        final MoneyTotalFollowFaceValue vinaphoneMoney2MFollowFace = new MoneyTotalFollowFaceValue();
        vinaphoneMoney2MFollowFace.setFaceValue(2000000);
        vinaphoneMoney2MFollowFace.setQuantity(this.vinaphone2MQuantity);
        vinaphoneMoney2MFollowFace.setMoneyTotal(this.vinaphone2MMoney);
        listVinaphoneMoneyFollowFace.add(vinaphoneMoney2MFollowFace);
        final MoneyTotalFollowFaceValue vinaphoneMoney1MFollowFace = new MoneyTotalFollowFaceValue();
        vinaphoneMoney1MFollowFace.setFaceValue(1000000);
        vinaphoneMoney1MFollowFace.setQuantity(this.vinaphone1MQuantity);
        vinaphoneMoney1MFollowFace.setMoneyTotal(this.vinaphone1MMoney);
        listVinaphoneMoneyFollowFace.add(vinaphoneMoney1MFollowFace);
        final MoneyTotalFollowFaceValue vinaphoneMoney500KFollowFace = new MoneyTotalFollowFaceValue();
        vinaphoneMoney500KFollowFace.setFaceValue(500000);
        vinaphoneMoney500KFollowFace.setQuantity(this.vinaphone500KQuantity);
        vinaphoneMoney500KFollowFace.setMoneyTotal(this.vinaphone500KMoney);
        listVinaphoneMoneyFollowFace.add(vinaphoneMoney500KFollowFace);
        final MoneyTotalFollowFaceValue vinaphoneMoney300KFollowFace = new MoneyTotalFollowFaceValue();
        vinaphoneMoney300KFollowFace.setFaceValue(300000);
        vinaphoneMoney300KFollowFace.setQuantity(this.vinaphone300KQuantity);
        vinaphoneMoney300KFollowFace.setMoneyTotal(this.vinaphone300KMoney);
        listVinaphoneMoneyFollowFace.add(vinaphoneMoney300KFollowFace);
        final MoneyTotalFollowFaceValue vinaphoneMoney200KFollowFace = new MoneyTotalFollowFaceValue();
        vinaphoneMoney200KFollowFace.setFaceValue(200000);
        vinaphoneMoney200KFollowFace.setQuantity(this.vinaphone200KQuantity);
        vinaphoneMoney200KFollowFace.setMoneyTotal(this.vinaphone200KMoney);
        listVinaphoneMoneyFollowFace.add(vinaphoneMoney200KFollowFace);
        final MoneyTotalFollowFaceValue vinaphoneMoney100KFollowFace = new MoneyTotalFollowFaceValue();
        vinaphoneMoney100KFollowFace.setFaceValue(100000);
        vinaphoneMoney100KFollowFace.setQuantity(this.vinaphone100KQuantity);
        vinaphoneMoney100KFollowFace.setMoneyTotal(this.vinaphone100KMoney);
        listVinaphoneMoneyFollowFace.add(vinaphoneMoney100KFollowFace);
        final MoneyTotalFollowFaceValue vinaphoneMoney50KFollowFace = new MoneyTotalFollowFaceValue();
        vinaphoneMoney50KFollowFace.setFaceValue(50000);
        vinaphoneMoney50KFollowFace.setQuantity(this.vinaphone50KQuantity);
        vinaphoneMoney50KFollowFace.setMoneyTotal(this.vinaphone50KMoney);
        listVinaphoneMoneyFollowFace.add(vinaphoneMoney50KFollowFace);
        final MoneyTotalFollowFaceValue vinaphoneMoney30KFollowFace = new MoneyTotalFollowFaceValue();
        vinaphoneMoney30KFollowFace.setFaceValue(30000);
        vinaphoneMoney30KFollowFace.setQuantity(this.vinaphone30KQuantity);
        vinaphoneMoney30KFollowFace.setMoneyTotal(this.vinaphone30KMoney);
        listVinaphoneMoneyFollowFace.add(vinaphoneMoney30KFollowFace);
        final MoneyTotalFollowFaceValue vinaphoneMoney20KFollowFace = new MoneyTotalFollowFaceValue();
        vinaphoneMoney20KFollowFace.setFaceValue(20000);
        vinaphoneMoney20KFollowFace.setQuantity(this.vinaphone20KQuantity);
        vinaphoneMoney20KFollowFace.setMoneyTotal(this.vinaphone20KMoney);
        listVinaphoneMoneyFollowFace.add(vinaphoneMoney20KFollowFace);
        final MoneyTotalFollowFaceValue vinaphoneMoney10KFollowFace = new MoneyTotalFollowFaceValue();
        vinaphoneMoney10KFollowFace.setFaceValue(10000);
        vinaphoneMoney10KFollowFace.setQuantity(this.vinaphone10KQuantity);
        vinaphoneMoney10KFollowFace.setMoneyTotal(this.vinaphone10KMoney);
        listVinaphoneMoneyFollowFace.add(vinaphoneMoney10KFollowFace);
        vinaphone.setName("Vinaphone");
        vinaphone.setValue(this.vinaphoneMoney);
        vinaphone.setTrans(listVinaphoneMoneyFollowFace);
        listReponse.add(vinaphone);
        final MoneyTotalRechargeByCardReponse mobifone = new MoneyTotalRechargeByCardReponse();
        final ArrayList<MoneyTotalFollowFaceValue> listMobifoneMoneyFollowFace = new ArrayList<>();
        final MoneyTotalFollowFaceValue mobifoneMoney5MFollowFace = new MoneyTotalFollowFaceValue();
        mobifoneMoney5MFollowFace.setFaceValue(5000000);
        mobifoneMoney5MFollowFace.setQuantity(this.mobifone5MQuantity);
        mobifoneMoney5MFollowFace.setMoneyTotal(this.mobifone5MMoney);
        listMobifoneMoneyFollowFace.add(mobifoneMoney5MFollowFace);
        final MoneyTotalFollowFaceValue mobifoneMoney2MFollowFace = new MoneyTotalFollowFaceValue();
        mobifoneMoney2MFollowFace.setFaceValue(2000000);
        mobifoneMoney2MFollowFace.setQuantity(this.mobifone2MQuantity);
        mobifoneMoney2MFollowFace.setMoneyTotal(this.mobifone2MMoney);
        listMobifoneMoneyFollowFace.add(mobifoneMoney2MFollowFace);
        final MoneyTotalFollowFaceValue mobifoneMoney1MFollowFace = new MoneyTotalFollowFaceValue();
        mobifoneMoney1MFollowFace.setFaceValue(1000000);
        mobifoneMoney1MFollowFace.setQuantity(this.mobifone1MQuantity);
        mobifoneMoney1MFollowFace.setMoneyTotal(this.mobifone1MMoney);
        listMobifoneMoneyFollowFace.add(mobifoneMoney1MFollowFace);
        final MoneyTotalFollowFaceValue mobifoneMoney500KFollowFace = new MoneyTotalFollowFaceValue();
        mobifoneMoney500KFollowFace.setFaceValue(500000);
        mobifoneMoney500KFollowFace.setQuantity(this.mobifone500KQuantity);
        mobifoneMoney500KFollowFace.setMoneyTotal(this.mobifone500KMoney);
        listMobifoneMoneyFollowFace.add(mobifoneMoney500KFollowFace);
        final MoneyTotalFollowFaceValue mobifoneMoney300KFollowFace = new MoneyTotalFollowFaceValue();
        mobifoneMoney300KFollowFace.setFaceValue(300000);
        mobifoneMoney300KFollowFace.setQuantity(this.mobifone300KQuantity);
        mobifoneMoney300KFollowFace.setMoneyTotal(this.mobifone300KMoney);
        listMobifoneMoneyFollowFace.add(mobifoneMoney300KFollowFace);
        final MoneyTotalFollowFaceValue mobifoneMoney200KFollowFace = new MoneyTotalFollowFaceValue();
        mobifoneMoney200KFollowFace.setFaceValue(200000);
        mobifoneMoney200KFollowFace.setQuantity(this.mobifone200KQuantity);
        mobifoneMoney200KFollowFace.setMoneyTotal(this.mobifone200KMoney);
        listMobifoneMoneyFollowFace.add(mobifoneMoney200KFollowFace);
        final MoneyTotalFollowFaceValue mobifoneMoney100KFollowFace = new MoneyTotalFollowFaceValue();
        mobifoneMoney100KFollowFace.setFaceValue(100000);
        mobifoneMoney100KFollowFace.setQuantity(this.mobifone100KQuantity);
        mobifoneMoney100KFollowFace.setMoneyTotal(this.mobifone100KMoney);
        listMobifoneMoneyFollowFace.add(mobifoneMoney100KFollowFace);
        final MoneyTotalFollowFaceValue mobifoneMoney50KFollowFace = new MoneyTotalFollowFaceValue();
        mobifoneMoney50KFollowFace.setFaceValue(50000);
        mobifoneMoney50KFollowFace.setQuantity(this.mobifone50KQuantity);
        mobifoneMoney50KFollowFace.setMoneyTotal(this.mobifone50KMoney);
        listMobifoneMoneyFollowFace.add(mobifoneMoney50KFollowFace);
        final MoneyTotalFollowFaceValue mobifoneMoney30KFollowFace = new MoneyTotalFollowFaceValue();
        mobifoneMoney30KFollowFace.setFaceValue(30000);
        mobifoneMoney30KFollowFace.setQuantity(this.mobifone30KQuantity);
        mobifoneMoney30KFollowFace.setMoneyTotal(this.mobifone30KMoney);
        listMobifoneMoneyFollowFace.add(mobifoneMoney30KFollowFace);
        final MoneyTotalFollowFaceValue mobifoneMoney20KFollowFace = new MoneyTotalFollowFaceValue();
        mobifoneMoney20KFollowFace.setFaceValue(20000);
        mobifoneMoney20KFollowFace.setQuantity(this.mobifone20KQuantity);
        mobifoneMoney20KFollowFace.setMoneyTotal(this.mobifone20KMoney);
        listMobifoneMoneyFollowFace.add(mobifoneMoney20KFollowFace);
        final MoneyTotalFollowFaceValue mobifoneMoney10KFollowFace = new MoneyTotalFollowFaceValue();
        mobifoneMoney10KFollowFace.setFaceValue(10000);
        mobifoneMoney10KFollowFace.setQuantity(this.mobifone10KQuantity);
        mobifoneMoney10KFollowFace.setMoneyTotal(this.mobifone10KMoney);
        listMobifoneMoneyFollowFace.add(mobifoneMoney10KFollowFace);
        mobifone.setName("Mobifone");
        mobifone.setValue(this.mobifoneMoney);
        mobifone.setTrans(listMobifoneMoneyFollowFace);
        listReponse.add(mobifone);
        final MoneyTotalRechargeByCardReponse gate = new MoneyTotalRechargeByCardReponse();
        final ArrayList<MoneyTotalFollowFaceValue> listGateMoneyFollowFace = new ArrayList<>();
        final MoneyTotalFollowFaceValue gateMoney5MFollowFace = new MoneyTotalFollowFaceValue();
        gateMoney5MFollowFace.setFaceValue(5000000);
        gateMoney5MFollowFace.setQuantity(this.gate5MQuantity);
        gateMoney5MFollowFace.setMoneyTotal(this.gate5MMoney);
        listGateMoneyFollowFace.add(gateMoney5MFollowFace);
        final MoneyTotalFollowFaceValue gateMoney2MFollowFace = new MoneyTotalFollowFaceValue();
        gateMoney2MFollowFace.setFaceValue(2000000);
        gateMoney2MFollowFace.setQuantity(this.gate2MQuantity);
        gateMoney2MFollowFace.setMoneyTotal(this.gate2MMoney);
        listGateMoneyFollowFace.add(gateMoney2MFollowFace);
        final MoneyTotalFollowFaceValue gateMoney1MFollowFace = new MoneyTotalFollowFaceValue();
        gateMoney1MFollowFace.setFaceValue(1000000);
        gateMoney1MFollowFace.setQuantity(this.gate1MQuantity);
        gateMoney1MFollowFace.setMoneyTotal(this.gate1MMoney);
        listGateMoneyFollowFace.add(gateMoney1MFollowFace);
        final MoneyTotalFollowFaceValue gateMoney500KFollowFace = new MoneyTotalFollowFaceValue();
        gateMoney500KFollowFace.setFaceValue(500000);
        gateMoney500KFollowFace.setQuantity(this.gate500KQuantity);
        gateMoney500KFollowFace.setMoneyTotal(this.gate500KMoney);
        listGateMoneyFollowFace.add(gateMoney500KFollowFace);
        final MoneyTotalFollowFaceValue gateMoney300KFollowFace = new MoneyTotalFollowFaceValue();
        gateMoney300KFollowFace.setFaceValue(300000);
        gateMoney300KFollowFace.setQuantity(this.gate300KQuantity);
        gateMoney300KFollowFace.setMoneyTotal(this.gate300KMoney);
        listGateMoneyFollowFace.add(gateMoney300KFollowFace);
        final MoneyTotalFollowFaceValue gateMoney200KFollowFace = new MoneyTotalFollowFaceValue();
        gateMoney200KFollowFace.setFaceValue(200000);
        gateMoney200KFollowFace.setQuantity(this.gate200KQuantity);
        gateMoney200KFollowFace.setMoneyTotal(this.gate200KMoney);
        listGateMoneyFollowFace.add(gateMoney200KFollowFace);
        final MoneyTotalFollowFaceValue gateMoney100KFollowFace = new MoneyTotalFollowFaceValue();
        gateMoney100KFollowFace.setFaceValue(100000);
        gateMoney100KFollowFace.setQuantity(this.gate100KQuantity);
        gateMoney100KFollowFace.setMoneyTotal(this.gate100KMoney);
        listGateMoneyFollowFace.add(gateMoney100KFollowFace);
        final MoneyTotalFollowFaceValue gateMoney50KFollowFace = new MoneyTotalFollowFaceValue();
        gateMoney50KFollowFace.setFaceValue(50000);
        gateMoney50KFollowFace.setQuantity(this.gate50KQuantity);
        gateMoney50KFollowFace.setMoneyTotal(this.gate50KMoney);
        listGateMoneyFollowFace.add(gateMoney50KFollowFace);
        final MoneyTotalFollowFaceValue gateMoney30KFollowFace = new MoneyTotalFollowFaceValue();
        gateMoney30KFollowFace.setFaceValue(30000);
        gateMoney30KFollowFace.setQuantity(this.gate30KQuantity);
        gateMoney30KFollowFace.setMoneyTotal(this.gate30KMoney);
        listGateMoneyFollowFace.add(gateMoney30KFollowFace);
        final MoneyTotalFollowFaceValue gateMoney20KFollowFace = new MoneyTotalFollowFaceValue();
        gateMoney20KFollowFace.setFaceValue(20000);
        gateMoney20KFollowFace.setQuantity(this.gate20KQuantity);
        gateMoney20KFollowFace.setMoneyTotal(this.gate20KMoney);
        listGateMoneyFollowFace.add(gateMoney20KFollowFace);
        final MoneyTotalFollowFaceValue gateMoney10KFollowFace = new MoneyTotalFollowFaceValue();
        gateMoney10KFollowFace.setFaceValue(10000);
        gateMoney10KFollowFace.setQuantity(this.gate10KQuantity);
        gateMoney10KFollowFace.setMoneyTotal(this.gate10KMoney);
        listGateMoneyFollowFace.add(gateMoney10KFollowFace);
        gate.setName("Gate");
        gate.setValue(this.gateMoney);
        gate.setTrans(listGateMoneyFollowFace);
        listReponse.add(gate);
        final MoneyTotalRechargeByCardReponse megacard = new MoneyTotalRechargeByCardReponse();
        final ArrayList<MoneyTotalFollowFaceValue> moneyFollowFace = new ArrayList<>();
        final MoneyTotalFollowFaceValue money5MFollowFace = new MoneyTotalFollowFaceValue();
        money5MFollowFace.setFaceValue(5000000);
        money5MFollowFace.setQuantity(this.money5MQuantity);
        money5MFollowFace.setMoneyTotal(this.money5M);
        moneyFollowFace.add(money5MFollowFace);
        final MoneyTotalFollowFaceValue money3MFollowFace = new MoneyTotalFollowFaceValue();
        money3MFollowFace.setFaceValue(3000000);
        money3MFollowFace.setQuantity(this.money3MQuantity);
        money3MFollowFace.setMoneyTotal(this.money3M);
        moneyFollowFace.add(money3MFollowFace);
        final MoneyTotalFollowFaceValue money2MFollowFace = new MoneyTotalFollowFaceValue();
        money2MFollowFace.setFaceValue(2000000);
        money2MFollowFace.setQuantity(this.money2MQuantity);
        money2MFollowFace.setMoneyTotal(this.money2M);
        moneyFollowFace.add(money2MFollowFace);
        final MoneyTotalFollowFaceValue money1MFollowFace = new MoneyTotalFollowFaceValue();
        money1MFollowFace.setFaceValue(1000000);
        money1MFollowFace.setQuantity(this.money1MQuantity);
        money1MFollowFace.setMoneyTotal(this.money1M);
        moneyFollowFace.add(money1MFollowFace);
        final MoneyTotalFollowFaceValue money500KFollowFace = new MoneyTotalFollowFaceValue();
        money500KFollowFace.setFaceValue(500000);
        money500KFollowFace.setQuantity(this.money500KQuantity);
        money500KFollowFace.setMoneyTotal(this.money500K);
        moneyFollowFace.add(money500KFollowFace);
        final MoneyTotalFollowFaceValue money300KFollowFace = new MoneyTotalFollowFaceValue();
        money300KFollowFace.setFaceValue(300000);
        money300KFollowFace.setQuantity(this.money300KQuantity);
        money300KFollowFace.setMoneyTotal(this.money300K);
        moneyFollowFace.add(money300KFollowFace);
        final MoneyTotalFollowFaceValue money200KFollowFace = new MoneyTotalFollowFaceValue();
        money200KFollowFace.setFaceValue(200000);
        money200KFollowFace.setQuantity(this.money200KQuantity);
        money200KFollowFace.setMoneyTotal(this.money200K);
        moneyFollowFace.add(money200KFollowFace);
        final MoneyTotalFollowFaceValue money100KFollowFace = new MoneyTotalFollowFaceValue();
        money100KFollowFace.setFaceValue(100000);
        money100KFollowFace.setQuantity(this.money100KQuantity);
        money100KFollowFace.setMoneyTotal(this.money100K);
        moneyFollowFace.add(money100KFollowFace);
        final MoneyTotalFollowFaceValue money50KFollowFace = new MoneyTotalFollowFaceValue();
        money50KFollowFace.setFaceValue(50000);
        money50KFollowFace.setQuantity(this.money50KQuantity);
        money50KFollowFace.setMoneyTotal(this.money50K);
        moneyFollowFace.add(money50KFollowFace);
        final MoneyTotalFollowFaceValue money20KFollowFace = new MoneyTotalFollowFaceValue();
        money20KFollowFace.setFaceValue(20000);
        money20KFollowFace.setQuantity(this.money20KQuantity);
        money20KFollowFace.setMoneyTotal(this.money20K);
        moneyFollowFace.add(money20KFollowFace);
        final MoneyTotalFollowFaceValue money10KFollowFace = new MoneyTotalFollowFaceValue();
        money10KFollowFace.setFaceValue(10000);
        money10KFollowFace.setQuantity(this.money10KQuantity);
        money10KFollowFace.setMoneyTotal(this.money10K);
        moneyFollowFace.add(money10KFollowFace);
        megacard.setName("MegaCard");
        megacard.setValue(this.megaMoney);
        megacard.setTrans(moneyFollowFace);
        listReponse.add(megacard);
        final MoneyTotalRechargeByCardReponse megacardVat = new MoneyTotalRechargeByCardReponse();
        final ArrayList<MoneyTotalFollowFaceValue> moneyFollowFaceVat = new ArrayList<>();
        final MoneyTotalFollowFaceValue money5MFollowFaceVat = new MoneyTotalFollowFaceValue();
        money5MFollowFaceVat.setFaceValue(5000000);
        money5MFollowFaceVat.setQuantity(this.money5MQuantityVat);
        money5MFollowFaceVat.setMoneyTotal(this.money5MVat);
        moneyFollowFaceVat.add(money5MFollowFaceVat);
        final MoneyTotalFollowFaceValue money3MFollowFaceVat = new MoneyTotalFollowFaceValue();
        money3MFollowFaceVat.setFaceValue(3000000);
        money3MFollowFaceVat.setQuantity(this.money3MQuantityVat);
        money3MFollowFaceVat.setMoneyTotal(this.money3MVat);
        moneyFollowFaceVat.add(money3MFollowFaceVat);
        final MoneyTotalFollowFaceValue money2MFollowFaceVat = new MoneyTotalFollowFaceValue();
        money2MFollowFaceVat.setFaceValue(2000000);
        money2MFollowFaceVat.setQuantity(this.money2MQuantityVat);
        money2MFollowFaceVat.setMoneyTotal(this.money2MVat);
        moneyFollowFaceVat.add(money2MFollowFaceVat);
        final MoneyTotalFollowFaceValue money1MFollowFaceVat = new MoneyTotalFollowFaceValue();
        money1MFollowFaceVat.setFaceValue(1000000);
        money1MFollowFaceVat.setQuantity(this.money1MQuantityVat);
        money1MFollowFaceVat.setMoneyTotal(this.money1MVat);
        moneyFollowFaceVat.add(money1MFollowFaceVat);
        final MoneyTotalFollowFaceValue money500KFollowFaceVat = new MoneyTotalFollowFaceValue();
        money500KFollowFaceVat.setFaceValue(500000);
        money500KFollowFaceVat.setQuantity(this.money500KQuantityVat);
        money500KFollowFaceVat.setMoneyTotal(this.money500KVat);
        moneyFollowFaceVat.add(money500KFollowFaceVat);
        final MoneyTotalFollowFaceValue money300KFollowFaceVat = new MoneyTotalFollowFaceValue();
        money300KFollowFaceVat.setFaceValue(300000);
        money300KFollowFaceVat.setQuantity(this.money300KQuantityVat);
        money300KFollowFaceVat.setMoneyTotal(this.money300KVat);
        moneyFollowFaceVat.add(money300KFollowFaceVat);
        final MoneyTotalFollowFaceValue money200KFollowFaceVat = new MoneyTotalFollowFaceValue();
        money200KFollowFaceVat.setFaceValue(200000);
        money200KFollowFaceVat.setQuantity(this.money200KQuantityVat);
        money200KFollowFaceVat.setMoneyTotal(this.money200KVat);
        moneyFollowFaceVat.add(money200KFollowFaceVat);
        final MoneyTotalFollowFaceValue money100KFollowFaceVat = new MoneyTotalFollowFaceValue();
        money100KFollowFaceVat.setFaceValue(100000);
        money100KFollowFaceVat.setQuantity(this.money100KQuantityVat);
        money100KFollowFaceVat.setMoneyTotal(this.money100KVat);
        moneyFollowFaceVat.add(money100KFollowFaceVat);
        final MoneyTotalFollowFaceValue money50KFollowFaceVat = new MoneyTotalFollowFaceValue();
        money50KFollowFaceVat.setFaceValue(50000);
        money50KFollowFaceVat.setQuantity(this.money50KQuantityVat);
        money50KFollowFaceVat.setMoneyTotal(this.money50KVat);
        moneyFollowFaceVat.add(money50KFollowFaceVat);
        final MoneyTotalFollowFaceValue money20KFollowFaceVat = new MoneyTotalFollowFaceValue();
        money20KFollowFaceVat.setFaceValue(20000);
        money20KFollowFaceVat.setQuantity(this.money20KQuantityVat);
        money20KFollowFaceVat.setMoneyTotal(this.money20KVat);
        moneyFollowFaceVat.add(money20KFollowFaceVat);
        final MoneyTotalFollowFaceValue money10KFollowFaceVat = new MoneyTotalFollowFaceValue();
        money10KFollowFaceVat.setFaceValue(10000);
        money10KFollowFaceVat.setQuantity(this.money10KQuantityVat);
        money10KFollowFaceVat.setMoneyTotal(this.money10KVat);
        moneyFollowFaceVat.add(money10KFollowFaceVat);
        megacardVat.setName("MegaCard_VAT");
        megacardVat.setValue(this.megaMoneyVat);
        megacardVat.setTrans(moneyFollowFaceVat);
        listReponse.add(megacardVat);
        final MoneyTotalRechargeByCardReponse vcoin = new MoneyTotalRechargeByCardReponse();
        final ArrayList<MoneyTotalFollowFaceValue> vcoinFollowFace = new ArrayList<>();
        final MoneyTotalFollowFaceValue vcoin10MFollowFace = new MoneyTotalFollowFaceValue();
        vcoin10MFollowFace.setFaceValue(10000000);
        vcoin10MFollowFace.setQuantity(this.vcoin10MQuantity);
        vcoin10MFollowFace.setMoneyTotal(this.vcoin10MMoney);
        vcoinFollowFace.add(vcoin10MFollowFace);
        final MoneyTotalFollowFaceValue vcoin5MFollowFace = new MoneyTotalFollowFaceValue();
        vcoin5MFollowFace.setFaceValue(5000000);
        vcoin5MFollowFace.setQuantity(this.vcoin5MQuantity);
        vcoin5MFollowFace.setMoneyTotal(this.vcoin5MMoney);
        vcoinFollowFace.add(vcoin5MFollowFace);
        final MoneyTotalFollowFaceValue vcoin2MFollowFace = new MoneyTotalFollowFaceValue();
        vcoin2MFollowFace.setFaceValue(2000000);
        vcoin2MFollowFace.setQuantity(this.vcoin2MQuantity);
        vcoin2MFollowFace.setMoneyTotal(this.vcoin2MMoney);
        vcoinFollowFace.add(vcoin2MFollowFace);
        final MoneyTotalFollowFaceValue vcoin1MFollowFace = new MoneyTotalFollowFaceValue();
        vcoin1MFollowFace.setFaceValue(1000000);
        vcoin1MFollowFace.setQuantity(this.vcoin1MQuantity);
        vcoin1MFollowFace.setMoneyTotal(this.vcoin1MMoney);
        vcoinFollowFace.add(vcoin1MFollowFace);
        final MoneyTotalFollowFaceValue vcoin500KFollowFace = new MoneyTotalFollowFaceValue();
        vcoin500KFollowFace.setFaceValue(500000);
        vcoin500KFollowFace.setQuantity(this.vcoin500KQuantity);
        vcoin500KFollowFace.setMoneyTotal(this.vcoin500KMoney);
        vcoinFollowFace.add(vcoin500KFollowFace);
        final MoneyTotalFollowFaceValue vcoin300KFollowFace = new MoneyTotalFollowFaceValue();
        vcoin300KFollowFace.setFaceValue(300000);
        vcoin300KFollowFace.setQuantity(this.vcoin300KQuantity);
        vcoin300KFollowFace.setMoneyTotal(this.vcoin300KMoney);
        vcoinFollowFace.add(vcoin300KFollowFace);
        final MoneyTotalFollowFaceValue vcoin200KFollowFace = new MoneyTotalFollowFaceValue();
        vcoin200KFollowFace.setFaceValue(200000);
        vcoin200KFollowFace.setQuantity(this.vcoin200KQuantity);
        vcoin200KFollowFace.setMoneyTotal(this.vcoin200KMoney);
        vcoinFollowFace.add(vcoin200KFollowFace);
        final MoneyTotalFollowFaceValue vcoin100KFollowFace = new MoneyTotalFollowFaceValue();
        vcoin100KFollowFace.setFaceValue(100000);
        vcoin100KFollowFace.setQuantity(this.vcoin100KQuantity);
        vcoin100KFollowFace.setMoneyTotal(this.vcoin100KMoney);
        vcoinFollowFace.add(vcoin100KFollowFace);
        final MoneyTotalFollowFaceValue vcoin50KFollowFace = new MoneyTotalFollowFaceValue();
        vcoin50KFollowFace.setFaceValue(50000);
        vcoin50KFollowFace.setQuantity(this.vcoin50KQuantity);
        vcoin50KFollowFace.setMoneyTotal(this.vcoin50KMoney);
        vcoinFollowFace.add(vcoin50KFollowFace);
        final MoneyTotalFollowFaceValue vcoin30KFollowFace = new MoneyTotalFollowFaceValue();
        vcoin30KFollowFace.setFaceValue(30000);
        vcoin30KFollowFace.setQuantity(this.vcoin30KQuantity);
        vcoin30KFollowFace.setMoneyTotal(this.vcoin30KMoney);
        vcoinFollowFace.add(vcoin30KFollowFace);
        final MoneyTotalFollowFaceValue vcoin20KFollowFace = new MoneyTotalFollowFaceValue();
        vcoin20KFollowFace.setFaceValue(20000);
        vcoin20KFollowFace.setQuantity(this.vcoin20KQuantity);
        vcoin20KFollowFace.setMoneyTotal(this.vcoin20KMoney);
        vcoinFollowFace.add(vcoin20KFollowFace);
        final MoneyTotalFollowFaceValue vcoin10KFollowFace = new MoneyTotalFollowFaceValue();
        vcoin10KFollowFace.setFaceValue(10000);
        vcoin10KFollowFace.setQuantity(this.vcoin10KQuantity);
        vcoin10KFollowFace.setMoneyTotal(this.vcoin10KMoney);
        vcoinFollowFace.add(vcoin10KFollowFace);
        vcoin.setName("Vcoin");
        vcoin.setValue(this.vcoinMoney);
        vcoin.setTrans(vcoinFollowFace);
        listReponse.add(vcoin);
        return listReponse;
    }

    @Override
    public List<MoneyTotalRechargeByCardReponse> moneyTotalRechargeByCard(final String nickName, final String provider, final String serial, final String pin, final String code, final String timeStart, final String timeEnd, final String transId) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        final Document conditions = new Document();
        this.totalMoney = 0L;
        this.webMoney = 0L;
        this.androidMoney = 0L;
        this.iosMoney = 0L;
        this.winphoneMoney = 0L;
        this.facebookMoney = 0L;
        this.desktopMoney = 0L;
        this.otherMoney = 0L;
        this.viettelMoney = 0L;
        this.vinaphoneMoney = 0L;
        this.mobifoneMoney = 0L;
        this.gateMoney = 0L;
        this.megaMoney = 0L;
        this.vcoinMoney = 0L;
        objsort.put("_id", -1);
        if (transId != null && !transId.isEmpty()) {
            conditions.put("reference_id", transId);
        }
        if (!nickName.isEmpty()) {
            conditions.put("nick_name", nickName);
        }
        if (!provider.isEmpty()) {
            conditions.put("provider", provider);
        }
        if (!serial.isEmpty()) {
            conditions.put("serial", serial);
        }
        if (!pin.isEmpty()) {
            conditions.put("pin", pin);
        }
        if (!code.isEmpty()) {
            conditions.put("code", Integer.parseInt(code));
        }
        if (!timeStart.isEmpty() && !timeEnd.isEmpty()) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("time_log", obj);
        }
        FindIterable iterable = null;
        iterable = db.getCollection("epay_recharge_by_mega_card").find(new Document(conditions)).sort(objsort);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                RechargeByCardDAOImpl rechargeByCardDAOImpl = RechargeByCardDAOImpl.this;
                rechargeByCardDAOImpl.totalMoney += document.getInteger("amount");
                rechargeByCardDAOImpl = RechargeByCardDAOImpl.this;
                rechargeByCardDAOImpl.megaMoney += document.getInteger("amount");
                if (document.containsKey("platform")) {
                    final String string2 = document.getString("platform");
                    switch (string2) {
                        case "web": {
                            final RechargeByCardDAOImpl rechargeByCardDAOImpl2 = RechargeByCardDAOImpl.this;
                            rechargeByCardDAOImpl2.webMoney += document.getInteger("amount");
                            break;
                        }
                        case "ad": {
                            final RechargeByCardDAOImpl rechargeByCardDAOImpl3 = RechargeByCardDAOImpl.this;
                            rechargeByCardDAOImpl3.androidMoney += document.getInteger("amount");
                            break;
                        }
                        case "ios": {
                            final RechargeByCardDAOImpl rechargeByCardDAOImpl4 = RechargeByCardDAOImpl.this;
                            rechargeByCardDAOImpl4.iosMoney += document.getInteger("amount");
                            break;
                        }
                        case "wp": {
                            final RechargeByCardDAOImpl rechargeByCardDAOImpl5 = RechargeByCardDAOImpl.this;
                            rechargeByCardDAOImpl5.winphoneMoney += document.getInteger("amount");
                            break;
                        }
                        case "fb": {
                            final RechargeByCardDAOImpl rechargeByCardDAOImpl6 = RechargeByCardDAOImpl.this;
                            rechargeByCardDAOImpl6.facebookMoney += document.getInteger("amount");
                            break;
                        }
                        case "dt": {
                            final RechargeByCardDAOImpl rechargeByCardDAOImpl7 = RechargeByCardDAOImpl.this;
                            rechargeByCardDAOImpl7.desktopMoney += document.getInteger("amount");
                            break;
                        }
                        case "ot": {
                            final RechargeByCardDAOImpl rechargeByCardDAOImpl8 = RechargeByCardDAOImpl.this;
                            rechargeByCardDAOImpl8.otherMoney += document.getInteger("amount");
                            break;
                        }
                        default: {
                            final RechargeByCardDAOImpl rechargeByCardDAOImpl9 = RechargeByCardDAOImpl.this;
                            rechargeByCardDAOImpl9.otherMoney += document.getInteger("amount");
                            break;
                        }
                    }
                } else {
                    final RechargeByCardDAOImpl string = RechargeByCardDAOImpl.this;
                    string.otherMoney += document.getInteger("amount");
                }
            }
        });
        iterable = db.getCollection("dvt_recharge_by_card").find(new Document(conditions)).sort(objsort);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final RechargeByCardDAOImpl rechargeByCardDAOImpl2 = RechargeByCardDAOImpl.this;
                rechargeByCardDAOImpl2.totalMoney += document.getInteger("amount");
                if (document.containsKey("platform")) {
                    final String string3 = document.getString("platform");
                    switch (string3) {
                        case "web": {
                            final RechargeByCardDAOImpl rechargeByCardDAOImpl3 = RechargeByCardDAOImpl.this;
                            rechargeByCardDAOImpl3.webMoney += document.getInteger("amount");
                            break;
                        }
                        case "ad": {
                            final RechargeByCardDAOImpl rechargeByCardDAOImpl3 = RechargeByCardDAOImpl.this;
                            rechargeByCardDAOImpl3.androidMoney += document.getInteger("amount");
                            break;
                        }
                        case "ios": {
                            final RechargeByCardDAOImpl rechargeByCardDAOImpl3 = RechargeByCardDAOImpl.this;
                            rechargeByCardDAOImpl3.iosMoney += document.getInteger("amount");
                            break;
                        }
                        case "wp": {
                            final RechargeByCardDAOImpl rechargeByCardDAOImpl3 = RechargeByCardDAOImpl.this;
                            rechargeByCardDAOImpl3.winphoneMoney += document.getInteger("amount");
                            break;
                        }
                        case "fb": {
                            final RechargeByCardDAOImpl rechargeByCardDAOImpl3 = RechargeByCardDAOImpl.this;
                            rechargeByCardDAOImpl3.facebookMoney += document.getInteger("amount");
                            break;
                        }
                        case "dt": {
                            final RechargeByCardDAOImpl rechargeByCardDAOImpl3 = RechargeByCardDAOImpl.this;
                            rechargeByCardDAOImpl3.desktopMoney += document.getInteger("amount");
                            break;
                        }
                        case "ot": {
                            final RechargeByCardDAOImpl rechargeByCardDAOImpl3 = RechargeByCardDAOImpl.this;
                            rechargeByCardDAOImpl3.otherMoney += document.getInteger("amount");
                            break;
                        }
                        default: {
                            final RechargeByCardDAOImpl rechargeByCardDAOImpl3 = RechargeByCardDAOImpl.this;
                            rechargeByCardDAOImpl3.otherMoney += document.getInteger("amount");
                            break;
                        }
                    }
                } else {
                    final RechargeByCardDAOImpl string = RechargeByCardDAOImpl.this;
                    string.otherMoney += document.getInteger("amount");
                }
                final String string4;
                final String string2 = string4 = document.getString("provider");
                switch (string4) {
                    case "Viettel": {
                        final RechargeByCardDAOImpl rechargeByCardDAOImpl3 = RechargeByCardDAOImpl.this;
                        rechargeByCardDAOImpl3.viettelMoney += document.getInteger("amount");
                        break;
                    }
                    case "Vinaphone": {
                        final RechargeByCardDAOImpl rechargeByCardDAOImpl3 = RechargeByCardDAOImpl.this;
                        rechargeByCardDAOImpl3.vinaphoneMoney += document.getInteger("amount");
                        break;
                    }
                    case "Mobifone": {
                        final RechargeByCardDAOImpl rechargeByCardDAOImpl3 = RechargeByCardDAOImpl.this;
                        rechargeByCardDAOImpl3.mobifoneMoney += document.getInteger("amount");
                        break;
                    }
                    case "Gate": {
                        final RechargeByCardDAOImpl rechargeByCardDAOImpl3 = RechargeByCardDAOImpl.this;
                        rechargeByCardDAOImpl3.gateMoney += document.getInteger("amount");
                        break;
                    }
                    case "MegaCard": {
                        final RechargeByCardDAOImpl rechargeByCardDAOImpl3 = RechargeByCardDAOImpl.this;
                        rechargeByCardDAOImpl3.megaMoney += document.getInteger("amount");
                        break;
                    }
                    case "Vcoin": {
                        final RechargeByCardDAOImpl rechargeByCardDAOImpl3 = RechargeByCardDAOImpl.this;
                        rechargeByCardDAOImpl3.vcoinMoney += document.getInteger("amount");
                        break;
                    }
                }
            }
        });
        final ArrayList<MoneyTotalRechargeByCardReponse> listReponse = new ArrayList<>();
        final MoneyTotalRechargeByCardReponse tong = new MoneyTotalRechargeByCardReponse();
        tong.setName("Tong");
        tong.setValue(this.totalMoney);
        listReponse.add(tong);
        final MoneyTotalRechargeByCardReponse web = new MoneyTotalRechargeByCardReponse();
        web.setName("web");
        web.setValue(this.webMoney);
        listReponse.add(web);
        final MoneyTotalRechargeByCardReponse android = new MoneyTotalRechargeByCardReponse();
        android.setName("ad");
        android.setValue(this.androidMoney);
        listReponse.add(android);
        final MoneyTotalRechargeByCardReponse ios = new MoneyTotalRechargeByCardReponse();
        ios.setName("ios");
        ios.setValue(this.iosMoney);
        listReponse.add(ios);
        final MoneyTotalRechargeByCardReponse winphone = new MoneyTotalRechargeByCardReponse();
        winphone.setName("wp");
        winphone.setValue(this.winphoneMoney);
        listReponse.add(winphone);
        final MoneyTotalRechargeByCardReponse facebook = new MoneyTotalRechargeByCardReponse();
        facebook.setName("fb");
        facebook.setValue(this.facebookMoney);
        listReponse.add(facebook);
        final MoneyTotalRechargeByCardReponse desktop = new MoneyTotalRechargeByCardReponse();
        desktop.setName("dt");
        desktop.setValue(this.desktopMoney);
        listReponse.add(desktop);
        final MoneyTotalRechargeByCardReponse other = new MoneyTotalRechargeByCardReponse();
        other.setName("ot");
        other.setValue(this.otherMoney);
        listReponse.add(other);
        final MoneyTotalRechargeByCardReponse viettel = new MoneyTotalRechargeByCardReponse();
        viettel.setName("Viettel");
        viettel.setValue(this.viettelMoney);
        listReponse.add(viettel);
        final MoneyTotalRechargeByCardReponse vinaphone = new MoneyTotalRechargeByCardReponse();
        vinaphone.setName("Vinaphone");
        vinaphone.setValue(this.vinaphoneMoney);
        listReponse.add(vinaphone);
        final MoneyTotalRechargeByCardReponse mobifone = new MoneyTotalRechargeByCardReponse();
        mobifone.setName("Mobifone");
        mobifone.setValue(this.mobifoneMoney);
        listReponse.add(mobifone);
        final MoneyTotalRechargeByCardReponse gate = new MoneyTotalRechargeByCardReponse();
        gate.setName("Gate");
        gate.setValue(this.gateMoney);
        listReponse.add(gate);
        final MoneyTotalRechargeByCardReponse megacard = new MoneyTotalRechargeByCardReponse();
        megacard.setName("MegaCard");
        megacard.setValue(this.megaMoney);
        listReponse.add(megacard);
        final MoneyTotalRechargeByCardReponse vcoin = new MoneyTotalRechargeByCardReponse();
        vcoin.setName("Vcoin");
        vcoin.setValue(this.vcoinMoney);
        listReponse.add(vcoin);
        return listReponse;
    }
}
