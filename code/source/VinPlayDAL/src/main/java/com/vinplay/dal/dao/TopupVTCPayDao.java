// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao;

import com.vinplay.vbee.common.response.MoneyTotalFollowFaceValue;
import com.vinplay.vbee.common.response.topupVTCPay.LogTopupVTCPay;

import java.util.List;

public interface TopupVTCPayDao {
    List<LogTopupVTCPay> getLogTopupVtcPay(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5);

    List<MoneyTotalFollowFaceValue> doiSoatTopupVtcPay(final String p0, final String p1);
}
