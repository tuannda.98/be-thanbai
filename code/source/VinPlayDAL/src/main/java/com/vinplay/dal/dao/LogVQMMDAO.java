// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao;

import com.vinplay.vbee.common.response.VQMMResponse;

import java.util.List;

public interface LogVQMMDAO {
    List<VQMMResponse> searchVQMM(final String p0, final String p1, final String p2, final int p3);

    int countSearchVQMM(final String p0, final String p1, final String p2);
}
