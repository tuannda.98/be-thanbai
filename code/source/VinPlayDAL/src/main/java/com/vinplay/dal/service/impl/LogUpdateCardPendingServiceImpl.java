// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.vinplay.dal.dao.impl.LogUpdateCardPendingDAOImpl;
import com.vinplay.dal.service.LogUpdateCardPendingService;
import com.vinplay.vbee.common.response.LogUpdateCardPendingReponse;
import com.vinplay.vbee.common.response.MoneyTotalRechargeByCardReponse;

import java.util.List;

public class LogUpdateCardPendingServiceImpl implements LogUpdateCardPendingService {
    @Override
    public List<LogUpdateCardPendingReponse> searchLogUpdateCardPending(final String nickName, final String provider, final String serial, final String pin, final String code, final String timeStart, final String timeEnd, final int page, final String transId, final String actor) {
        final LogUpdateCardPendingDAOImpl dao = new LogUpdateCardPendingDAOImpl();
        return dao.searchLogUpdateCardPending(nickName, provider, serial, pin, code, timeStart, timeEnd, page, transId, actor);
    }

    @Override
    public int countTotalRecordLogUpdateCardPending(final String nickName, final String provider, final String serial, final String pin, final String code, final String timeStart, final String timeEnd, final String transId, final String actor) {
        final LogUpdateCardPendingDAOImpl dao = new LogUpdateCardPendingDAOImpl();
        return dao.countTotalRecordLogUpdateCardPending(nickName, provider, serial, pin, code, timeStart, timeEnd, transId, actor);
    }

    @Override
    public List<MoneyTotalRechargeByCardReponse> moneyTotalUpdateCardPengding(final String nickName, final String provider, final String serial, final String pin, final String code, final String timeStart, final String timeEnd, final String transId, final String actor) {
        final LogUpdateCardPendingDAOImpl dao = new LogUpdateCardPendingDAOImpl();
        return dao.moneyTotalUpdateCardPengding(nickName, provider, serial, pin, code, timeStart, timeEnd, transId, actor);
    }
}
