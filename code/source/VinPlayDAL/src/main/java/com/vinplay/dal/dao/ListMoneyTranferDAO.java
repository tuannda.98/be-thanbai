// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao;

import com.vinplay.vbee.common.response.TranferMoneyResponse;

import java.util.List;

public interface ListMoneyTranferDAO {
    List<TranferMoneyResponse> listMoneyTranfer(final String p0, final int p1, final int p2, final int p3);

    TranferMoneyResponse getMoneyTranferByTransNo(final String p0);

    int countTotalRecord(final String p0, final int p1, final int p2, final int p3);
}
