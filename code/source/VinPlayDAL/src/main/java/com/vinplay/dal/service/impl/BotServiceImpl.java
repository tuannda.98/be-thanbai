// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.vinplay.dal.dao.impl.UserDaoImpl;
import com.vinplay.dal.service.BotService;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.messages.LogMoneyUserMessage;
import com.vinplay.vbee.common.messages.MoneyMessageInMinigame;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.models.vippoint.UserVPEventModel;
import com.vinplay.vbee.common.response.MoneyResponse;
import com.vinplay.vbee.common.rmq.RMQApi;
import com.vinplay.vbee.common.utils.VinPlayUtils;
import org.apache.log4j.Logger;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Date;

public class BotServiceImpl implements BotService {
    private static final Logger logger;

    @Override
    public UserModel login(final String nickname) throws SQLException, NoSuchAlgorithmException {
        final IMap userMap = HazelcastClientFactory.getInstance().getMap("users");
        UserModel userModel = null;
        if (!userMap.containsKey(nickname)) {
            final UserDaoImpl dao = new UserDaoImpl();
            userModel = dao.getUserByNickName(nickname);
            if (userModel != null) {
                final UserVPEventModel vpModel = dao.getUserVPByNickName(userModel.getNickname());
                final UserCacheModel userCache = new UserCacheModel(userModel.getId(), userModel.getUsername(), userModel.getNickname(), userModel.getPassword(), userModel.getEmail(), userModel.getFacebookId(), userModel.getGoogleId(), userModel.getMobile(), userModel.getBirthday(), userModel.isGender(), userModel.getAddress(), userModel.getVin(), userModel.getXu(), userModel.getVinTotal(), userModel.getXuTotal(), userModel.getSafe(), userModel.getRechargeMoney(), userModel.getCashoutMoney(), userModel.getVippoint(), userModel.getDaily(), userModel.getStatus(), userModel.getAvatar(), userModel.getIdentification(), userModel.getVippointSave(), userModel.getCreateTime(), userModel.getMoneyVP(), userModel.getSecurityTime(), userModel.getLoginOtp(), userModel.isBot(), 0, new Date(), vpModel.getVpEvent(), vpModel.getVpReal(), vpModel.getVpAdd(), vpModel.getVpSub(), vpModel.getNumAdd(), vpModel.getNumSub(), vpModel.getPlace(), vpModel.getPlaceMax(), userModel.getClient(), userModel.getPlatform());
                final String accessToken = VinPlayUtils.genAccessToken(userModel.getId());
                userCache.setAccessToken(accessToken);
                userCache.setLastMessageId(0L);
                userCache.setLastActive(new Date());
                userCache.setOnline(0);
                userMap.put(userCache.getNickname(), userCache);
            }
        } else {
            userModel = (UserModel) userMap.get(nickname);
        }
        return userModel;
    }

    @Override
    public MoneyResponse addMoney(final String nickname, final long money, final String moneyType, final String description) {
        final MoneyResponse res = new MoneyResponse(false, "1001");
        if (nickname == null || money == 0L) {
            return res;
        }
        final HazelcastInstance client = HazelcastClientFactory.getInstance();
        final IMap<String, UserModel> userMap = client.getMap("users");
        if (userMap.containsKey(nickname)) {
            try {
                userMap.lock(nickname);
                final UserCacheModel user = (UserCacheModel) userMap.get(nickname);
                if (user.isBot()) {
                    long moneyUser = user.getMoney(moneyType);
                    long currentMoney = user.getCurrentMoney(moneyType);
                    if (currentMoney + money > 0L) {
                        user.setMoney(moneyType, moneyUser += money);
                        user.setCurrentMoney(moneyType, currentMoney += money);
                        final MoneyMessageInMinigame message = new MoneyMessageInMinigame(VinPlayUtils.genMessageId(), user.getId(), nickname, "Bot", moneyUser, currentMoney, money, moneyType, 0L, 0, 0);
                        final String des = "Chuy\u1ec3n kho\u1ea3n";
                        final LogMoneyUserMessage messageLog = new LogMoneyUserMessage(user.getId(), nickname, "Bot", "Chuy\u1ec3n kho\u1ea3n", currentMoney, money, moneyType, description, 0L, false, user.isBot());
                        RMQApi.publishMessageInMiniGame(message);
                        RMQApi.publishMessageLogMoney(messageLog);
                        userMap.put(nickname, user);
                        res.setSuccess(true);
                        res.setErrorCode("0");
                        res.setCurrentMoney(currentMoney);
                        res.setMoneyUse(moneyUser);
                    } else {
                        res.setErrorCode("1002");
                    }
                }
            } catch (Exception e) {
                BotServiceImpl.logger.debug(e);
            } finally {
                userMap.unlock(nickname);
            }
        }
        return res;
    }

    static {
        logger = Logger.getLogger("user_core");
    }
}
