// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.vinplay.dal.service.ChatLobbyService;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;

public class ChatLobbyServiceImpl implements ChatLobbyService {
    @Override
    public void banChatUser(final String nickname, final long time) {
        final HazelcastInstance client = HazelcastClientFactory.getInstance();
        final IMap userMap = client.getMap("cacheBanChat");
        if (time > 0L) {
            userMap.put(nickname, System.currentTimeMillis() + time);
        } else {
            userMap.put(nickname, time);
        }
    }

    @Override
    public long getBanTime(final String nickname) {
        final HazelcastInstance client = HazelcastClientFactory.getInstance();
        final IMap userMap = client.getMap("cacheBanChat");
        if (userMap.containsKey(nickname)) {
            long timeUnBan = (long) userMap.get(nickname);
            if (timeUnBan < System.currentTimeMillis()) {
                timeUnBan = 0L;
                userMap.remove(nickname);
            }
            return timeUnBan;
        }
        return 0L;
    }
}
