// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.entities.gamebai;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TopGameBaiModel {
    private String nickName;
    private String avatar;
    private int winCount;
    private int winCountToday;
    private int winCountThisWeek;
    private int winCountThisMonth;
    private int winCountThisYear;
    private int lostCount;
    private int lostCountToday;
    private int lostCountThisWeek;
    private int lostCountThisMonth;
    private int lostCountThisYear;
    private long moneyWin;
    private long moneyWinToday;
    private long moneyWinThisWeek;
    private long moneyWinThisMonth;
    private long moneyWinThisYear;
    private long moneyLost;
    private long moneyLostToday;
    private long moneyLostThisWeek;
    private long moneyLostThisMonth;
    private long moneyLostThisYear;
    private int lastDay;
    private int lastWeek;
    private int lastMonth;
    private int lastYear;

    public String toJson() {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            return "";
        }
    }

    public TopGameBaiModel() {
    }

    public TopGameBaiModel(final String nickName, final String avatar, final int winCount, final int winCountToday, final int winCountThisWeek, final int winCountThisMonth, final int winCountThisYear, final int lostCount, final int lostCountToday, final int lostCountThisWeek, final int lostCountThisMonth, final int lostCountThisYear, final long moneyWin, final long moneyWinToday, final long moneyWinThisWeek, final long moneyWinThisMonth, final long moneyWinThisYear, final long moneyLost, final long moneyLostToday, final long moneyLostThisWeek, final long moneyLostThisMonth, final long moneyLostThisYear, final int lastDay, final int lastWeek, final int lastMonth, final int lastYear) {
        this.nickName = nickName;
        this.avatar = avatar;
        this.winCount = winCount;
        this.winCountToday = winCountToday;
        this.winCountThisWeek = winCountThisWeek;
        this.winCountThisMonth = winCountThisMonth;
        this.winCountThisYear = winCountThisYear;
        this.lostCount = lostCount;
        this.lostCountToday = lostCountToday;
        this.lostCountThisWeek = lostCountThisWeek;
        this.lostCountThisMonth = lostCountThisMonth;
        this.lostCountThisYear = lostCountThisYear;
        this.moneyWin = moneyWin;
        this.moneyWinToday = moneyWinToday;
        this.moneyWinThisWeek = moneyWinThisWeek;
        this.moneyWinThisMonth = moneyWinThisMonth;
        this.moneyWinThisYear = moneyWinThisYear;
        this.moneyLost = moneyLost;
        this.moneyLostToday = moneyLostToday;
        this.moneyLostThisWeek = moneyLostThisWeek;
        this.moneyLostThisMonth = moneyLostThisMonth;
        this.moneyLostThisYear = moneyLostThisYear;
        this.lastDay = lastDay;
        this.lastWeek = lastWeek;
        this.lastMonth = lastMonth;
        this.lastYear = lastYear;
    }

    public String getNickName() {
        return this.nickName;
    }

    public void setNickName(final String nickName) {
        this.nickName = nickName;
    }

    public String getAvatar() {
        return this.avatar;
    }

    public void setAvatar(final String avatar) {
        this.avatar = avatar;
    }

    public int getWinCount() {
        return this.winCount;
    }

    public void setWinCount(final int winCount) {
        this.winCount = winCount;
    }

    public int getWinCountToday() {
        return this.winCountToday;
    }

    public void setWinCountToday(final int winCountToday) {
        this.winCountToday = winCountToday;
    }

    public int getWinCountThisWeek() {
        return this.winCountThisWeek;
    }

    public void setWinCountThisWeek(final int winCountThisWeek) {
        this.winCountThisWeek = winCountThisWeek;
    }

    public int getWinCountThisMonth() {
        return this.winCountThisMonth;
    }

    public void setWinCountThisMonth(final int winCountThisMonth) {
        this.winCountThisMonth = winCountThisMonth;
    }

    public int getWinCountThisYear() {
        return this.winCountThisYear;
    }

    public void setWinCountThisYear(final int winCountThisYear) {
        this.winCountThisYear = winCountThisYear;
    }

    public int getLostCount() {
        return this.lostCount;
    }

    public void setLostCount(final int lostCount) {
        this.lostCount = lostCount;
    }

    public int getLostCountToday() {
        return this.lostCountToday;
    }

    public void setLostCountToday(final int lostCountToday) {
        this.lostCountToday = lostCountToday;
    }

    public int getLostCountThisWeek() {
        return this.lostCountThisWeek;
    }

    public void setLostCountThisWeek(final int lostCountThisWeek) {
        this.lostCountThisWeek = lostCountThisWeek;
    }

    public int getLostCountThisMonth() {
        return this.lostCountThisMonth;
    }

    public void setLostCountThisMonth(final int lostCountThisMonth) {
        this.lostCountThisMonth = lostCountThisMonth;
    }

    public int getLostCountThisYear() {
        return this.lostCountThisYear;
    }

    public void setLostCountThisYear(final int lostCountThisYear) {
        this.lostCountThisYear = lostCountThisYear;
    }

    public long getMoneyWin() {
        return this.moneyWin;
    }

    public void setMoneyWin(final long moneyWin) {
        this.moneyWin = moneyWin;
    }

    public long getMoneyWinToday() {
        return this.moneyWinToday;
    }

    public void setMoneyWinToday(final long moneyWinToday) {
        this.moneyWinToday = moneyWinToday;
    }

    public long getMoneyWinThisWeek() {
        return this.moneyWinThisWeek;
    }

    public void setMoneyWinThisWeek(final long moneyWinThisWeek) {
        this.moneyWinThisWeek = moneyWinThisWeek;
    }

    public long getMoneyWinThisMonth() {
        return this.moneyWinThisMonth;
    }

    public void setMoneyWinThisMonth(final long moneyWinThisMonth) {
        this.moneyWinThisMonth = moneyWinThisMonth;
    }

    public long getMoneyWinThisYear() {
        return this.moneyWinThisYear;
    }

    public void setMoneyWinThisYear(final long moneyWinThisYear) {
        this.moneyWinThisYear = moneyWinThisYear;
    }

    public long getMoneyLost() {
        return this.moneyLost;
    }

    public void setMoneyLost(final long moneyLost) {
        this.moneyLost = moneyLost;
    }

    public long getMoneyLostToday() {
        return this.moneyLostToday;
    }

    public void setMoneyLostToday(final long moneyLostToday) {
        this.moneyLostToday = moneyLostToday;
    }

    public long getMoneyLostThisWeek() {
        return this.moneyLostThisWeek;
    }

    public void setMoneyLostThisWeek(final long moneyLostThisWeek) {
        this.moneyLostThisWeek = moneyLostThisWeek;
    }

    public long getMoneyLostThisMonth() {
        return this.moneyLostThisMonth;
    }

    public void setMoneyLostThisMonth(final long moneyLostThisMonth) {
        this.moneyLostThisMonth = moneyLostThisMonth;
    }

    public long getMoneyLostThisYear() {
        return this.moneyLostThisYear;
    }

    public void setMoneyLostThisYear(final long moneyLostThisYear) {
        this.moneyLostThisYear = moneyLostThisYear;
    }

    public int getLastDay() {
        return this.lastDay;
    }

    public void setLastDay(final int lastDay) {
        this.lastDay = lastDay;
    }

    public int getLastWeek() {
        return this.lastWeek;
    }

    public void setLastWeek(final int lastWeek) {
        this.lastWeek = lastWeek;
    }

    public int getLastMonth() {
        return this.lastMonth;
    }

    public void setLastMonth(final int lastMonth) {
        this.lastMonth = lastMonth;
    }

    public int getLastYear() {
        return this.lastYear;
    }

    public void setLastYear(final int lastYear) {
        this.lastYear = lastYear;
    }
}
