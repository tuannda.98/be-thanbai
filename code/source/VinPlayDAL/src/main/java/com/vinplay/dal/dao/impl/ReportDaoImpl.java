// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.FindOneAndUpdateOptions;
import com.vinplay.dal.dao.ReportDAO;
import com.vinplay.dal.entities.report.ReportMoneySystemModel;
import com.vinplay.dal.entities.report.ReportTotalMoneyModel;
import com.vinplay.vbee.common.models.cache.ReportModel;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.pools.ConnectionPool;
import com.vinplay.vbee.common.utils.VinPlayUtils;
import org.bson.Document;

import java.sql.*;
import java.text.ParseException;
import java.util.Date;
import java.util.*;

public class ReportDaoImpl implements ReportDAO {
    @Override
    public List<String> getAllBot() throws SQLException {
        final ArrayList<String> res = new ArrayList<>();
        try (final Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             final PreparedStatement stm = conn.prepareStatement("SELECT nick_name FROM users WHERE is_bot=1");
             final ResultSet rs = stm.executeQuery();) {
            while (rs.next()) {
                res.add(rs.getString("nick_name"));
            }
        }
        return res;
    }

    @Override
    public Map<String, ReportMoneySystemModel> getReportMoneySystemMySQL(final String startTime, final String endTime, final boolean isBot) throws Exception {
        final HashMap<String, ReportMoneySystemModel> results = new HashMap<>();
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             CallableStatement call = conn.prepareCall("CALL report_money_system(?,?)")) {
            int param = 1;
            call.setString(param++, VinPlayUtils.getDateTimeStr(VinPlayUtils.getDateTimeFromDate(startTime)));
            call.setString(param++, VinPlayUtils.getDateTimeStr(VinPlayUtils.getDateTimeFromDate(endTime)));
            try (ResultSet rs = call.executeQuery()) {
                while (rs.next()) {
                    final ReportMoneySystemModel model = new ReportMoneySystemModel();
                    model.moneyWin = rs.getLong("money_win");
                    model.moneyLost = rs.getLong("money_lost");
                    model.moneyOther = rs.getLong("money_other");
                    model.fee = rs.getLong("fee");
                    model.revenuePlayGame = model.moneyWin + model.moneyLost;
                    model.revenue = model.revenuePlayGame + model.moneyOther;
                    final String actionName = rs.getString("action_name");
                    results.put(actionName, model);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return results;
    }

    @Override
    public Map<String, ReportMoneySystemModel> getReportMoneySystem(final String startTime, final String endTime, final boolean isBot) throws Exception {
        final HashMap<String, ReportMoneySystemModel> results = new HashMap<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final Document conditions = new Document();
        if (!startTime.isEmpty() && !endTime.isEmpty()) {
            final BasicDBObject obj = new BasicDBObject();
            obj.put("$gte", VinPlayUtils.getDateTimeStr(VinPlayUtils.getDateTimeFromDate(startTime)));
            obj.put("$lte", VinPlayUtils.getDateTimeStr(VinPlayUtils.getDateTimeFromDate(endTime)));
            conditions.put("time_log", obj);
        }
        MongoCollection col = null;
        if (!isBot) {
            col = db.getCollection("report_money_vin");
            final AggregateIterable iterable = col.aggregate(Arrays.asList(new Document("$match", conditions), new Document("$group", new Document("_id", "$action_name").append("money_win", new Document("$sum", "$money_win")).append("money_lost", new Document("$sum", "$money_lost")).append("money_other", new Document("$sum", "$money_other")).append("fee", new Document("$sum", "$fee")))));
            iterable.forEach(new Block<Document>() {
                public void apply(final Document document) {
                    final ReportMoneySystemModel model = new ReportMoneySystemModel();
                    model.moneyWin = document.getLong("money_win");
                    model.moneyLost = document.getLong("money_lost");
                    model.moneyOther = document.getLong("money_other");
                    model.fee = document.getLong("fee");
                    model.revenuePlayGame = model.moneyWin + model.moneyLost;
                    model.revenue = model.revenuePlayGame + model.moneyOther;
                    final String actionName = document.getString("_id");
                    results.put(actionName, model);
                }
            });
            return results;
        }
        return results;
    }

    @Override
    public Map<String, ReportMoneySystemModel> getReportMoneyUser(final String startTime, final String endTime, final String nickname, final boolean isBot) throws Exception {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final Document conditions = new Document();
        if (!startTime.isEmpty() && !endTime.isEmpty()) {
            final BasicDBObject obj = new BasicDBObject();
            obj.put("$gte", VinPlayUtils.getDateTimeStr(VinPlayUtils.getDateTimeFromDate(startTime)));
            obj.put("$lte", VinPlayUtils.getDateTimeStr(VinPlayUtils.getDateTimeFromDate(endTime)));
            conditions.put("time_log", obj);
        }
        conditions.put("nick_name", nickname);
        MongoCollection col = null;
        col = (isBot ? db.getCollection("report_money_vin_bot") : db.getCollection("report_money_vin"));
        final AggregateIterable iterable = col.aggregate(Arrays.asList(new Document("$match", conditions), new Document("$group", new Document("_id", "$action_name").append("money_win", new Document("$sum", "$money_win")).append("money_lost", new Document("$sum", "$money_lost")).append("money_other", new Document("$sum", "$money_other")).append("fee", new Document("$sum", "$fee")))));
        final HashMap<String, ReportMoneySystemModel> results = new HashMap<>();
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final ReportMoneySystemModel model = new ReportMoneySystemModel();
                final String actionName = document.getString("_id");
                model.moneyWin = document.getLong("money_win");
                model.moneyLost = document.getLong("money_lost");
                model.moneyOther = document.getLong("money_other");
                model.fee = document.getLong("fee");
                model.revenuePlayGame = model.moneyWin + model.moneyLost;
                model.revenue = model.revenuePlayGame + model.moneyOther;
                results.put(actionName, model);
            }
        });
        return results;
    }

    @Override
    public ReportTotalMoneyModel getTotalMoney(final String superAgent) throws SQLException {
        long moneyBot = 0L;
        long moneyUser = 0L;
        long moneyAgent1 = 0L;
        long moneyAgent2 = 0L;
        long moneySuperAgent = 0L;
        final String sqlSuperAgent = "SELECT (SUM(vin_total) + SUM(safe)) as sum FROM users WHERE nick_name = '" + superAgent + "'";
        try (
                final Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
                final PreparedStatement stmBot = conn.prepareStatement("SELECT (SUM(vin_total) + SUM(safe)) as sum FROM users WHERE is_bot=1");
                final PreparedStatement stmUser = conn.prepareStatement("SELECT (SUM(vin_total) + SUM(safe)) as sum FROM users WHERE is_bot=0 AND dai_ly <> 1 AND dai_ly <> 2");
                final PreparedStatement stmAgent1 = conn.prepareStatement("SELECT (SUM(vin_total) + SUM(safe)) as sum FROM users WHERE is_bot=0 AND dai_ly = 1");
                final PreparedStatement stmAgent2 = conn.prepareStatement("SELECT (SUM(vin_total) + SUM(safe)) as sum FROM users WHERE is_bot=0 AND dai_ly = 2");
                final PreparedStatement stmSuperAgent = conn.prepareStatement(sqlSuperAgent);
                final ResultSet rsBot = stmBot.executeQuery();
                final ResultSet rsUser = stmUser.executeQuery();
                final ResultSet rsAgent1 = stmAgent1.executeQuery();
                final ResultSet rsAgent2 = stmAgent2.executeQuery();
                final ResultSet rsSuperAgent = stmSuperAgent.executeQuery();
        ) {
            if (rsBot.next()) {
                moneyBot = rsBot.getLong("sum");
            }
            if (rsUser.next()) {
                moneyUser = rsUser.getLong("sum");
            }
            if (rsAgent1.next()) {
                moneyAgent1 = rsAgent1.getLong("sum");
            }
            if (rsAgent2.next()) {
                moneyAgent2 = rsAgent2.getLong("sum");
            }
            if (rsSuperAgent.next()) {
                moneySuperAgent = rsSuperAgent.getLong("sum");
            }
        }
        final long total = moneyBot + moneyUser + (moneyAgent1 -= moneySuperAgent) + moneyAgent2 + moneySuperAgent;
        return new ReportTotalMoneyModel(moneyBot, moneyUser, moneyAgent1, moneyAgent2, moneySuperAgent, total, null);
    }

    @Override
    public long getCurrentMoney(final String nickname) throws SQLException {
        long res = 0L;
        try (final Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             final PreparedStatement stm = conn.prepareStatement("SELECT vin_total FROM users WHERE nick_name=?");) {
            stm.setString(1, nickname);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    res = rs.getLong("vin_total");
                }
            }
        }
        return res;
    }

    @Override
    public long getSafeMoney(final String nickname) throws SQLException {
        long res = 0L;
        try (final Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             final PreparedStatement stm = conn.prepareStatement("SELECT safe FROM users WHERE nick_name=?");) {
            stm.setString(1, nickname);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    res = rs.getLong("safe");
                }
            }
        }
        return res;
    }

    @Override
    public boolean checkBot(final String nickname) throws SQLException {
        boolean res = false;
        try (final Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             final PreparedStatement stm = conn.prepareStatement("SELECT is_bot FROM users WHERE nick_name=?");) {
            stm.setString(1, nickname);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next() && rs.getInt("is_bot") == 1) {
                    res = true;
                }
            }
        }
        return res;
    }

    @Override
    public boolean saveLogTotalMoney(final ReportTotalMoneyModel model) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final MongoCollection col = db.getCollection("report_total_money");
        final Document doc = new Document();
        doc.append("money_bot", model.moneyBot);
        doc.append("money_user", model.moneyUser);
        doc.append("money_agent_1", model.moneyAgent1);
        doc.append("money_agent_2", model.moneyAgent2);
        doc.append("money_super_agent", model.moneySuperAgent);
        doc.append("time_log", VinPlayUtils.getCurrentDateTime());
        col.insertOne(doc);
        return true;
    }

    @Override
    public List<ReportTotalMoneyModel> getReportTotalMoney(final int pageNumber, final String startTime, final String endTime) {
        final ArrayList<ReportTotalMoneyModel> res = new ArrayList<>();
        final int pageSize = 50;
        final int skipNumber = (pageNumber - 1) * 50;
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final Document conditions = new Document();
        if (!startTime.isEmpty() && !endTime.isEmpty()) {
            final BasicDBObject obj = new BasicDBObject();
            obj.put("$gte", startTime);
            obj.put("$lte", endTime);
            conditions.put("time_log", obj);
        }
        final BasicDBObject sortCondtions = new BasicDBObject();
        sortCondtions.put("_id", -1);
        final FindIterable iterable = db.getCollection("report_total_money").find(conditions).sort(sortCondtions).skip(skipNumber).limit(50);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final ReportTotalMoneyModel model = new ReportTotalMoneyModel();
                model.moneyBot = document.getLong("money_bot");
                model.moneyUser = document.getLong("money_user");
                model.moneyAgent1 = document.getLong("money_agent_1");
                model.moneyAgent2 = document.getLong("money_agent_2");
                model.moneySuperAgent = document.getLong("money_super_agent");
                model.total = model.moneyBot + model.moneyUser + model.moneyAgent1 + model.moneyAgent2 + model.moneySuperAgent;
                model.timeLog = document.getString("time_log");
                res.add(model);
            }
        });
        return res;
    }

    @Override
    public ReportTotalMoneyModel getReportTotalMoneyAtTime(final String date, final boolean bStart) throws ParseException {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final Document conditions = new Document();
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject sortCondtions = new BasicDBObject();
        final Date dateTime = VinPlayUtils.getDateTimeFromDate(date);
        if (bStart) {
            obj.put("$gte", VinPlayUtils.getDateTimeStr(dateTime));
            sortCondtions.put("_id", 1);
        } else {
            final Calendar cal = Calendar.getInstance();
            cal.setTime(dateTime);
            cal.add(Calendar.DATE, 1);
            obj.put("$lte", VinPlayUtils.getDateTimeStr(cal.getTime()));
            sortCondtions.put("_id", -1);
        }
        conditions.put("time_log", obj);
        final Document document = db.getCollection("report_total_money").find(conditions).sort(sortCondtions).first();
        final ReportTotalMoneyModel model = new ReportTotalMoneyModel();
        if (document != null) {
            model.moneyBot = document.getLong("money_bot");
            model.moneyUser = document.getLong("money_user");
            model.moneyAgent1 = document.getLong("money_agent_1");
            model.moneyAgent2 = document.getLong("money_agent_2");
            model.moneySuperAgent = document.getLong("money_super_agent");
            model.total = model.moneyBot + model.moneyUser + model.moneyAgent1 + model.moneyAgent2 + model.moneySuperAgent;
            model.timeLog = document.getString("time_log");
        }
        return model;
    }

    @Override
    public boolean saveLogMoneyForReport(final String nickname, final String actionname, final String date, final ReportModel model) throws ParseException {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = null;
        col = (model.isBot ? db.getCollection("report_money_vin_bot") : db.getCollection("report_money_vin"));
        final BasicDBObject updateFields = new BasicDBObject();
        updateFields.append("money_win", model.moneyWin);
        updateFields.append("money_lost", model.moneyLost);
        updateFields.append("money_other", model.moneyOther);
        updateFields.append("fee", model.fee);
        updateFields.append("time_log", VinPlayUtils.getDateTimeStr(VinPlayUtils.getDateTimeFromDate(date)));
        updateFields.append("create_time", VinPlayUtils.getDateTimeFromDate(date));
        final BasicDBObject conditions = new BasicDBObject();
        conditions.append("nick_name", nickname);
        conditions.append("action_name", actionname);
        conditions.append("date", date);
        final FindOneAndUpdateOptions options = new FindOneAndUpdateOptions();
        options.upsert(true);
        col.findOneAndUpdate(conditions, new Document("$set", updateFields), options);
        return true;
    }

    @Override
    public boolean saveTopCaoThu(final String nickname, final String date, final long moneyWin) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final MongoCollection col = db.getCollection("top_user_play_game_vin");
        final BasicDBObject updateFields = new BasicDBObject();
        updateFields.append("money_win", moneyWin);
        final BasicDBObject conditions = new BasicDBObject();
        conditions.append("nick_name", nickname);
        conditions.append("date", date);
        final FindOneAndUpdateOptions options = new FindOneAndUpdateOptions();
        options.upsert(true);
        col.findOneAndUpdate(conditions, new Document("$set", updateFields), options);
        return true;
    }

    @Override
    public HashMap<String, Long> getReportTopGame(final String startTime, final String endTime, final String actionName, final boolean isBot) throws Exception {
        final HashMap<String, Long> results = new HashMap<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final Document conditions = new Document();
        if (!startTime.isEmpty() && !endTime.isEmpty()) {
            final BasicDBObject obj = new BasicDBObject();
            obj.put("$gte", VinPlayUtils.getDateTimeStr(VinPlayUtils.getDateTimeFromDate(startTime)));
            obj.put("$lte", VinPlayUtils.getDateTimeStr(VinPlayUtils.getDateTimeFromDate(endTime)));
            conditions.put("time_log", obj);
        }
        conditions.put("action_name", actionName);
        MongoCollection col = null;
        if (!isBot) {
            col = db.getCollection("report_money_vin");
            final AggregateIterable iterable = col.aggregate(Arrays.asList(new Document("$match", conditions), new Document("$group", new Document("_id", "$nick_name").append("money_win", new Document("$sum", "$money_win")).append("money_lost", new Document("$sum", "$money_lost")).append("money_other", new Document("$sum", "$money_other")))));
            iterable.forEach(new Block<Document>() {
                public void apply(final Document document) {
                    final String nickName = document.getString("_id");
                    final long money = document.getLong("money_win") + document.getLong("money_lost") + document.getLong("money_other");
                    results.put(nickName, money);
                }
            });
            return results;
        }
        return results;
    }

    @Override
    public Map<String, ReportModel> getListReportModelByDay(final String date, final boolean isBot) throws Exception {
        final HashMap<String, ReportModel> results = new HashMap<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final Document conditions = new Document();
        conditions.put("time_log", VinPlayUtils.getDateTimeStr(VinPlayUtils.getDateTimeFromDate(date)));
        MongoCollection col = null;
        col = (isBot ? db.getCollection("report_money_vin_bot") : db.getCollection("report_money_vin"));
        final FindIterable iterable = col.find(conditions);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final ReportModel model = new ReportModel();
                final String nickname = document.getString("nick_name");
                final String actionname = document.getString("action_name");
                model.moneyWin = document.getLong("money_win");
                model.moneyLost = document.getLong("money_lost");
                model.moneyOther = document.getLong("money_other");
                model.fee = document.getLong("fee");
                model.isBot = isBot;
                final String key = nickname + "," + actionname + "," + date;
                results.put(key, model);
            }
        });
        return results;
    }

    @Override
    public void saveReportMoneyVin(final Map<String, ReportModel> input) {
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             final PreparedStatement stmt = conn.prepareStatement("INSERT INTO report_money_daily(action_name, money_win, money_lost, money_other, fee, date) VALUES(?, ?, ?, ?, ?, ?)");
        ) {
            final Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -1);
            final java.sql.Date yesterday = new java.sql.Date(cal.getTimeInMillis());
            for (final Map.Entry<String, ReportModel> entry : input.entrySet()) {
                if (entry.getValue().isBot) {
                    continue;
                }
                stmt.setString(1, entry.getKey());
                stmt.setLong(2, entry.getValue().moneyWin);
                stmt.setLong(3, entry.getValue().moneyLost);
                stmt.setLong(4, entry.getValue().moneyOther);
                stmt.setLong(5, entry.getValue().fee);
                stmt.setDate(6, yesterday);
                stmt.execute();
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
