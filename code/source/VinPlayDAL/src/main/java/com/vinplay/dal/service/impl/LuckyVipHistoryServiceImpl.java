// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.vinplay.dal.dao.impl.LuckyVipHistoryDAOImpl;
import com.vinplay.dal.service.LuckyVipHistoryService;
import com.vinplay.vbee.common.response.LuckyVipHistoryResponse;

import java.util.List;

public class LuckyVipHistoryServiceImpl implements LuckyVipHistoryService {
    @Override
    public List<LuckyVipHistoryResponse> searchLuckyVipHistory(final String nickName, final String timeStart, final String timeEnd, final int page) {
        final LuckyVipHistoryDAOImpl dao = new LuckyVipHistoryDAOImpl();
        return dao.searchLuckyVipHistory(nickName, timeStart, timeEnd, page);
    }
}
