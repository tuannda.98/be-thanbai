// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service;

import com.hazelcast.core.IMap;
import com.vinplay.vbee.common.messages.gamebai.LogNoHuGameBaiMessage;
import com.vinplay.vbee.common.models.cache.TransactionList;
import com.vinplay.vbee.common.response.LogMoneyUserResponse;
import com.vinplay.vbee.common.response.LogUserMoneyResponse;

import java.util.List;

public interface LogMoneyUserService {
    List<LogUserMoneyResponse> searchLogMoneyUser(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5, final String p6, final int p7, final int p8, final int p9);

    List<LogUserMoneyResponse> searchLogMoneyUserDes(final String nickName, final String userName, final String moneyType, final String serviceName, final String actionName, final String timeStart, final String timeEnd, final int page, final int like1, final int totalRecord, String descColumn);

    int countsearchLogMoneyUser(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5, final int p6);

    int countsearchLogMoneyUser(final String nickName, final String userName, final String moneyType, final String serviceName, final String actionName, final String timeStart, final String timeEnd, final int like1, final int recordPerPage);

    List<LogMoneyUserResponse> getHistoryTransactionLogMoney(final String p0, final int p1, final int p2);

    int countHistoryTransactionLogMoney(final String p0, final int p1);

    List<LogNoHuGameBaiMessage> getNoHuGameBaiHistory(final int p0, final String p1);

    int countNoHuGameBaiHistory();

    List<LogUserMoneyResponse> searchLogMoneyTranferUser(final String p0, final String p1, final String p2, final String p3, final int p4);

    boolean UpdateProcessLogChuyenTienDaiLy(final String p0, final String p1, final String p2, final String p3);

    List<LogMoneyUserResponse> pushHistoryTransactionDBToCache(final IMap<String, TransactionList> p0, final String p1, final int p2);
}
