package com.vinplay.dal.dao.impl;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.vinplay.dal.dao.AgentDAO;
import com.vinplay.dal.entities.agent.BonusTopDSModel;
import com.vinplay.dal.entities.agent.TranferMoneyAgent;
import com.vinplay.vbee.common.messages.dvt.RefundFeeAgentMessage;
import com.vinplay.vbee.common.models.AgentModel;
import com.vinplay.vbee.common.models.cache.AgentDSModel;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.pools.ConnectionPool;
import com.vinplay.vbee.common.response.AgentResponse;
import com.vinplay.vbee.common.response.LogAgentTranferMoneyResponse;
import com.vinplay.vbee.common.response.TranferAgentResponse;
import com.vinplay.vbee.common.utils.DateTimeUtils;
import org.bson.Document;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class AgentDAOImpl implements AgentDAO {
    @Override
    public List<AgentResponse> listAgent() throws SQLException {
        final ArrayList<AgentResponse> results = new ArrayList<>();
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_admin");
             final PreparedStatement stmt = conn.prepareStatement("SELECT nameagent,nickname,phone,address,parentid,`show`,`active`,`order`,`facebook` FROM useragent WHERE status='D' and parentid=-1 and `show`=1 and active=1 order by `order` asc");
             final ResultSet rs = stmt.executeQuery();) {
            while (rs.next()) {
                final AgentResponse agent = new AgentResponse();
                agent.fullName = rs.getString("nameagent");
                agent.nickName = rs.getString("nickname");
                agent.mobile = rs.getString("phone");
                agent.address = rs.getString("address");
                agent.parentid = rs.getInt("parentid");
                agent.show = rs.getInt("show");
                agent.active = rs.getInt("active");
                agent.orderNo = rs.getInt("order");
                agent.facebook = rs.getString("facebook");
                results.add(agent);
            }
        }
        return results;
    }

    @Override
    public List<AgentResponse> listAgentByClient(final String client) throws SQLException {
        final ArrayList<AgentResponse> results = new ArrayList<>();
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_admin");
             final PreparedStatement stmt = conn.prepareStatement("SELECT nameagent,nickname,phone,address,parentid,`show`,`active`,`order`,`facebook`,`site` FROM useragent WHERE status='D' and parentid=-1 and `show`=1 and active=1 and site = '" + client + "' order by `order` asc");
             final ResultSet rs = stmt.executeQuery();) {
            while (rs.next()) {
                final AgentResponse agent = new AgentResponse();
                agent.fullName = "[" + rs.getString("site") + "] " + rs.getString("nameagent");
                agent.nickName = rs.getString("nickname");
                agent.mobile = rs.getString("phone");
                agent.address = rs.getString("address");
                agent.parentid = rs.getInt("parentid");
                agent.show = rs.getInt("show");
                agent.active = rs.getInt("active");
                agent.orderNo = rs.getInt("order");
                agent.facebook = rs.getString("facebook");
                results.add(agent);
            }
        }
        return results;
    }

    @Override
    public AgentResponse listAgentByKey(final String key) throws SQLException {
        AgentResponse agent = null;
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_admin");
             final PreparedStatement stmt = conn.prepareStatement("SELECT nameagent,nickname,phone,address,parentid,`show`,`active`,`order`,`facebook`,`site` FROM useragent WHERE `key` = '" + key + "' order by `order` asc");
             final ResultSet rs = stmt.executeQuery();) {
            while (rs.next()) {
                agent = new AgentResponse();
                agent.fullName = rs.getString("nameagent");
                agent.nickName = rs.getString("nickname");
                agent.mobile = rs.getString("phone");
                agent.address = rs.getString("address");
                agent.parentid = rs.getInt("parentid");
                agent.show = rs.getInt("show");
                agent.active = rs.getInt("active");
                agent.orderNo = rs.getInt("order");
                agent.facebook = rs.getString("facebook");
            }
        }
        return agent;
    }

    @Override
    public List<LogAgentTranferMoneyResponse> searchAgentTranferMoney(final String nickNameSend, final String nickNameRecieve, final String status, final String timeStart, final String timeEnd, final String top_ds, final int page) {
        final ArrayList<LogAgentTranferMoneyResponse> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        FindIterable iterable = null;
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        final int numStart = (page - 1) * 50;
        final int numEnd = 50;
        final Object v2 = -1;
        objsort.put("_id", v2);
        if ((nickNameSend != null && !nickNameSend.equals("")) || (nickNameRecieve != null && !nickNameRecieve.equals(""))) {
            final BasicDBObject query1 = new BasicDBObject("nick_name_send", nickNameSend);
            final BasicDBObject query2 = new BasicDBObject("nick_name_receive", nickNameRecieve);
            final ArrayList<BasicDBObject> myList = new ArrayList<>();
            myList.add(query1);
            myList.add(query2);
            conditions.put("$or", myList);
        }
        if (top_ds != null && !top_ds.equals("")) {
            conditions.put("top_ds", Integer.parseInt(top_ds));
        }
        if (status != null && !status.equals("")) {
            conditions.put("status", Integer.parseInt(status));
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("trans_time", obj);
        }
        iterable = db.getCollection("log_chuyen_tien_dai_ly").find(new Document(conditions)).sort(objsort).skip(numStart).limit(50);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final LogAgentTranferMoneyResponse trans = new LogAgentTranferMoneyResponse();
                trans.nick_name_send = document.getString("nick_name_send");
                trans.nick_name_receive = document.getString("nick_name_receive");
                trans.status = document.getInteger("status");
                trans.trans_time = document.getString("trans_time");
                trans.fee = document.getLong("fee");
                trans.money_send = document.getLong("money_send");
                trans.money_receive = document.getLong("money_receive");
                trans.top_ds = document.getInteger("top_ds");
                results.add(trans);
            }
        });
        return results;
    }

    @Override
    public List<AgentResponse> listUserAgent(final String nickName) throws SQLException {
        final ArrayList<AgentResponse> results = new ArrayList<>();
        String sql = "";
        if (!nickName.isEmpty()) {
            sql = "SELECT nameagent,nickname,phone,address,id,parentid,`show`,active,percent_bonus_vincard FROM useragent WHERE status='D' and `active` = 1 and  nickname=?";
        } else {
            sql = "SELECT nameagent,nickname,phone,address,id,parentid,`show`,active,percent_bonus_vincard FROM useragent WHERE status='D' and `active` = 1";
        }
        try (final Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_admin");
             PreparedStatement stmt = conn.prepareStatement(sql);) {
            if (!nickName.isEmpty()) {
                stmt.setString(1, nickName);
            }
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    final AgentResponse agent = new AgentResponse();
                    agent.fullName = rs.getString("nameagent");
                    agent.nickName = rs.getString("nickname");
                    agent.mobile = rs.getString("phone");
                    agent.address = rs.getString("address");
                    agent.id = rs.getInt("id");
                    agent.parentid = rs.getInt("parentid");
                    agent.show = rs.getInt("show");
                    agent.active = rs.getInt("active");
                    agent.percent = rs.getInt("percent_bonus_vincard");
                    results.add(0, agent);
                }
            }
        }
        return results;
    }

    @Override
    public List<AgentResponse> listUserAgentByParentID(final int ParentID) throws SQLException {
        final ArrayList<AgentResponse> results = new ArrayList<>();
        String sql = "SELECT nameagent,nickname,phone,address,id,parentid FROM useragent WHERE status='D' and parentid=?";
        try (final Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_admin");
             final PreparedStatement stmt = conn.prepareStatement(sql);) {
            stmt.setInt(1, ParentID);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    final AgentResponse agent = new AgentResponse();
                    agent.fullName = rs.getString("nameagent");
                    agent.nickName = rs.getString("nickname");
                    agent.mobile = rs.getString("phone");
                    agent.address = rs.getString("address");
                    agent.id = rs.getInt("id");
                    agent.parentid = rs.getInt("parentid");
                    results.add(0, agent);
                }
            }
        }
        return results;
    }

    @Override
    public TranferAgentResponse searchAgentTranfer(final String nickName, final String status, final String timeStart, final String timeEnd) {
        long totalBuy1 = 0L;
        long totalSale1 = 0L;
        long totalFeeBuy1 = 0L;
        long totalFeeSale1 = 0L;
        int countBuy1 = 0;
        int countSale1 = 0;
        long totalBuy2 = 0L;
        long totalSale2 = 0L;
        long totalFeeBuy2 = 0L;
        long totalFeeSale2 = 0L;
        int countBuy2 = 0;
        int countSale2 = 0;
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final MongoCollection collection = db.getCollection("log_chuyen_tien_dai_ly");
        if (collection != null) {
            final HashMap<String, Object> statusmua1 = new HashMap<>();
            final HashMap<String, Object> statusmua2 = new HashMap<>();
            final HashMap<String, Object> statusban1 = new HashMap<>();
            final HashMap<String, Object> statusban2 = new HashMap<>();
            final HashMap<String, Object> statusfeeban1 = new HashMap<>();
            final HashMap<String, Object> statusfeeban2 = new HashMap<>();
            final HashMap<String, Object> statusfeemua1 = new HashMap<>();
            final HashMap<String, Object> statusfeemua2 = new HashMap<>();
            final BasicDBObject obj = new BasicDBObject();
            final BasicDBObject stt1 = new BasicDBObject("status", 1);
            final BasicDBObject stt2 = new BasicDBObject("status", 2);
            final BasicDBObject stt3 = new BasicDBObject("status", 3);
            final BasicDBObject stt4 = new BasicDBObject("status", 4);
            final BasicDBObject stt5 = new BasicDBObject("status", 5);
            final BasicDBObject stt6 = new BasicDBObject("status", 6);
            final BasicDBObject stt7 = new BasicDBObject("status", 7);
            final BasicDBObject stt8 = new BasicDBObject("status", 8);
            statusmua1.put("top_ds", 1);
            statusmua2.put("top_ds", 1);
            statusban1.put("top_ds", 1);
            statusban2.put("top_ds", 1);
            if (!timeStart.isEmpty() && !timeEnd.isEmpty()) {
                obj.put("$gte", timeStart);
                obj.put("$lte", timeEnd);
                statusmua1.put("trans_time", obj);
                statusban1.put("trans_time", obj);
                statusfeemua1.put("trans_time", obj);
                statusfeeban1.put("trans_time", obj);
                statusmua2.put("trans_time", obj);
                statusban2.put("trans_time", obj);
                statusfeemua2.put("trans_time", obj);
                statusfeeban2.put("trans_time", obj);
            }
            if (!status.isEmpty()) {
                statusmua1.put("status", status);
                statusban1.put("status", status);
                statusfeemua1.put("status", status);
                statusfeeban1.put("status", status);
                statusmua2.put("status", status);
                statusban2.put("status", status);
                statusfeemua2.put("status", status);
                statusfeeban2.put("status", status);
            } else {
                final ArrayList<BasicDBObject> sttdsmua1 = new ArrayList<>();
                sttdsmua1.add(stt1);
                statusmua1.put("$or", sttdsmua1);
                statusmua1.put("nick_name_receive", nickName);
                final ArrayList<BasicDBObject> sttdsmua2 = new ArrayList<>();
                sttdsmua2.add(stt2);
                statusmua2.put("$or", sttdsmua2);
                statusmua2.put("nick_name_receive", nickName);
                final ArrayList<BasicDBObject> sttdsban1 = new ArrayList<>();
                sttdsban1.add(stt3);
                statusban1.put("$or", sttdsban1);
                statusban1.put("nick_name_send", nickName);
                final ArrayList<BasicDBObject> sttdsban2 = new ArrayList<>();
                sttdsban2.add(stt6);
                statusban2.put("$or", sttdsban2);
                statusban2.put("nick_name_send", nickName);
                final ArrayList<BasicDBObject> sttfeemua1 = new ArrayList<>();
                sttfeemua1.add(stt1);
                sttfeemua1.add(stt7);
                statusfeemua1.put("$or", sttfeemua1);
                statusfeemua1.put("nick_name_receive", nickName);
                final ArrayList<BasicDBObject> sttfeemua2 = new ArrayList<>();
                sttfeemua2.add(stt2);
                statusfeemua2.put("$or", sttfeemua2);
                statusfeemua2.put("nick_name_receive", nickName);
                final ArrayList<BasicDBObject> sttfeeban1 = new ArrayList<>();
                sttfeeban1.add(stt3);
                sttfeeban1.add(stt4);
                sttfeeban1.add(stt5);
                statusfeeban1.put("$or", sttfeeban1);
                statusfeeban1.put("nick_name_send", nickName);
                final ArrayList<BasicDBObject> sttfeeban2 = new ArrayList<>();
                sttfeeban2.add(stt6);
                sttfeeban2.add(stt8);
                statusfeeban2.put("$or", sttfeeban2);
                statusfeeban2.put("nick_name_send", nickName);
            }
            final Document dsmua1 = (Document) collection.aggregate(Arrays.asList(new Document("$match", new Document(statusmua1)), new Document("$group", new Document("_id", "$nick_name_receive").append("money", new Document("$sum", "$money_send"))))).first();
            final Document dsmua2 = (Document) collection.aggregate(Arrays.asList(new Document("$match", new Document(statusmua2)), new Document("$group", new Document("_id", "$nick_name_receive").append("money", new Document("$sum", "$money_send"))))).first();
            final Document dsban1 = (Document) collection.aggregate(Arrays.asList(new Document("$match", new Document(statusban1)), new Document("$group", new Document("_id", "$nick_name_send").append("money", new Document("$sum", "$money_send"))))).first();
            final Document dsban2 = (Document) collection.aggregate(Arrays.asList(new Document("$match", new Document(statusban2)), new Document("$group", new Document("_id", "$nick_name_send").append("money", new Document("$sum", "$money_send"))))).first();
            final Document feemua1 = (Document) collection.aggregate(Arrays.asList(new Document("$match", new Document(statusfeemua1)), new Document("$group", new Document("_id", "$nick_name_receive").append("money", new Document("$sum", "$fee")).append("count", new Document("$sum", 1))))).first();
            final Document feemua2 = (Document) collection.aggregate(Arrays.asList(new Document("$match", new Document(statusfeemua2)), new Document("$group", new Document("_id", "$nick_name_receive").append("money", new Document("$sum", "$fee")).append("count", new Document("$sum", 1))))).first();
            final Document feeban1 = (Document) collection.aggregate(Arrays.asList(new Document("$match", new Document(statusfeeban1)), new Document("$group", new Document("_id", "$nick_name_send").append("money", new Document("$sum", "$fee")).append("count", new Document("$sum", 1))))).first();
            final Document feeban2 = (Document) collection.aggregate(Arrays.asList(new Document("$match", new Document(statusfeeban2)), new Document("$group", new Document("_id", "$nick_name_send").append("money", new Document("$sum", "$fee")).append("count", new Document("$sum", 1))))).first();
            if (dsmua1 != null) {
                totalBuy1 = dsmua1.getLong("money");
            }
            if (dsban1 != null) {
                totalSale1 = dsban1.getLong("money");
            }
            if (feemua1 != null) {
                totalFeeBuy1 = feemua1.getLong("money");
                countBuy1 = feemua1.getInteger("count");
            }
            if (feeban1 != null) {
                totalFeeSale1 = feeban1.getLong("money");
                countSale1 = feeban1.getInteger("count");
            }
            if (dsmua2 != null) {
                totalBuy2 = dsmua2.getLong("money");
            }
            if (dsban2 != null) {
                totalSale2 = dsban2.getLong("money");
            }
            if (feemua2 != null) {
                totalFeeBuy2 = feemua2.getLong("money");
                countBuy2 = feemua2.getInteger("count");
            }
            if (feeban2 != null) {
                totalFeeSale2 = feeban2.getLong("money");
                countSale2 = feeban2.getInteger("count");
            }
        }
        return new TranferAgentResponse(nickName, totalBuy1, totalSale1, totalFeeBuy1, totalFeeSale1, countBuy1, countSale1, totalBuy2, totalSale2, totalFeeBuy2, totalFeeSale2, countBuy2, countSale2);
    }

    @Override
    public AgentDSModel getDS(final String nickName, final String timeStart, final String timeEnd, final boolean bAgent1) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final MongoCollection collection = db.getCollection("log_chuyen_tien_dai_ly");
        final HashMap<String, Object> statusmua = new HashMap<>();
        final HashMap<String, Object> statusban = new HashMap<>();
        if (!timeStart.isEmpty() && !timeEnd.isEmpty()) {
            final BasicDBObject obj = new BasicDBObject();
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            statusmua.put("trans_time", obj);
            statusban.put("trans_time", obj);
        }
        statusmua.put("top_ds", 1);
        statusmua.put("nick_name_receive", nickName);
        statusban.put("top_ds", 1);
        statusban.put("nick_name_send", nickName);
        if (bAgent1) {
            statusmua.put("status", 1);
            statusban.put("status", 3);
        } else {
            statusmua.put("status", 2);
            statusban.put("status", 6);
        }
        long dsMua = 0L;
        long dsBan = 0L;
        int gdMua = 0;
        int gdBan = 0;
        final Document dsmua = (Document) collection.aggregate(Arrays.asList(new Document("$match", new Document(statusmua)), new Document("$group", new Document("_id", "$nick_name_receive").append("money", new Document("$sum", "$money_send")).append("count", new Document("$sum", 1))))).first();
        final Document dsban = (Document) collection.aggregate(Arrays.asList(new Document("$match", new Document(statusban)), new Document("$group", new Document("_id", "$nick_name_send").append("money", new Document("$sum", "$money_send")).append("count", new Document("$sum", 1))))).first();
        if (dsmua != null) {
            dsMua = dsmua.getLong("money");
            gdMua = dsmua.getInteger("count");
        }
        if (dsban != null) {
            dsBan = dsban.getLong("money");
            gdBan = dsban.getInteger("count");
        }
        final long ds = dsMua + dsBan;
        final int gd = gdMua + gdBan;
        return new AgentDSModel(dsMua, dsBan, ds, gdMua, gdBan, gd);
    }

    @Override
    public Map<String, ArrayList<String>> getAllAgent() throws SQLException {
        final HashMap<String, ArrayList<String>> map = new HashMap<>();
        try (final Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_admin");
             final PreparedStatement stmt = conn.prepareStatement(" SELECT id, nickname, percent_bonus_vincard  FROM useragent  WHERE status = 'D'    AND parentid = -1    AND active = 1 ");
             final ResultSet rs = stmt.executeQuery();) {
            while (rs.next()) {
                final ArrayList<String> temp = new ArrayList<>();
                final int id = rs.getInt("id");
                final String agent1 = rs.getString("nickname");
                final int percent = rs.getInt("percent_bonus_vincard");
                final StringBuilder agent2 = new StringBuilder();
                final String sql2 = " SELECT nickname FROM useragent WHERE status = 'D' AND parentid = ? ";
                try (PreparedStatement stmt2 = conn.prepareStatement(" SELECT nickname FROM useragent WHERE status = 'D' AND parentid = ? ")) {
                    stmt2.setInt(1, id);
                    try (ResultSet rs2 = stmt2.executeQuery()) {
                        while (rs2.next()) {
                            agent2.append(rs2.getString("nickname")).append(",");
                        }
                        if (agent2.length() >= 1) {
                            agent2.delete(agent2.length() - 1, agent2.length());
                        }
                    }
                }
                temp.add(0, agent2.toString());
                temp.add(1, String.valueOf(percent));
                map.put(agent1, temp);
            }
        }
        return map;
    }

    @Override
    public Map<String, String> getAllNameAgent() throws SQLException {
        final HashMap<String, String> map = new HashMap<>();
        try (final Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_admin");
             final PreparedStatement stmt = conn.prepareStatement("SELECT id,nickname FROM useragent WHERE status='D' and parentid = -1 and active = 1");
             final ResultSet rs = stmt.executeQuery();) {
            while (rs.next()) {
                final int id = rs.getInt("id");
                final String agent1 = rs.getString("nickname");
                final StringBuilder agent2 = new StringBuilder();
                final String sql2 = "SELECT nickname FROM useragent WHERE status='D' and parentid = ?";
                try (PreparedStatement stmt2 = conn.prepareStatement("SELECT nickname FROM useragent WHERE status='D' and parentid = ?")) {
                    stmt2.setInt(1, id);
                    try (ResultSet rs2 = stmt2.executeQuery()) {
                        while (rs2.next()) {
                            agent2.append(rs2.getString("nickname")).append(",");
                        }
                        if (agent2.length() >= 1) {
                            agent2.delete(agent2.length() - 1, agent2.length());
                        }
                    }
                }
                map.put(agent1, agent2.toString());
            }
        }
        return map;
    }

    @Override
    public boolean checkRefundFeeAgent(final String nickname, final String month) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        conditions.put("nick_name", nickname);
        conditions.put("month", month);
        conditions.put("code", Integer.parseInt("0"));
        final FindIterable iterable = db.getCollection("log_refund_fee_agent").find(new Document(conditions)).limit(1);
        final Document document = (iterable != null) ? ((Document) iterable.first()) : null;
        return document != null;
    }

    @Override
    public List<RefundFeeAgentMessage> getLogRefundFeeAgent(final String nickname, final String month) {
        final ArrayList<RefundFeeAgentMessage> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        if (!nickname.isEmpty()) {
            conditions.put("nick_name", nickname);
        }
        if (!month.isEmpty()) {
            conditions.put("month", month);
        }
        final FindIterable iterable = db.getCollection("log_refund_fee_agent").find(new Document(conditions));
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final RefundFeeAgentMessage trans = new RefundFeeAgentMessage(document.getString("nick_name"), document.getLong("fee_1"), document.getDouble("ratio_1"), document.getLong("fee_2"), document.getDouble("ratio_2"), (document.getLong("fee_2_more") == null) ? 0L : document.getLong("fee_2_more"), (document.getDouble("ratio_2_more") == null) ? 0.0 : document.getDouble("ratio_2_more"), document.getLong("fee"), document.getString("month"), document.getInteger("code"), document.getString("description"), (document.getLong("fee_vinplay_card") == null) ? 0L : document.getLong("fee_vinplay_card"), (document.getLong("fee_vin_cash") == null) ? 0L : document.getLong("fee_vin_cash"), (document.getInteger("percent") == null) ? 0 : document.getInteger("percent"));
                final String createTime = document.getString("time_log");
                trans.setCreateTime(createTime);
                results.add(trans);
            }
        });
        return results;
    }

    @Override
    public long countsearchAgentTranferMoney(final String nickNameSend, final String nickNameRecieve, final String status, final String timeStart, final String timeEnd, final String top_ds) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        if (nickNameSend != null && !nickNameSend.equals("")) {
            conditions.put("nick_name_send", nickNameSend);
        }
        if (nickNameRecieve != null && !nickNameRecieve.equals("")) {
            conditions.put("nick_name_receive", nickNameRecieve);
        }
        if (top_ds != null && !top_ds.equals("")) {
            conditions.put("top_ds", Integer.parseInt(top_ds));
        }
        if (status != null && !status.equals("")) {
            conditions.put("status", Integer.parseInt(status));
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("trans_time", obj);
        }
        return db.getCollection("log_chuyen_tien_dai_ly").count(new Document(conditions));
    }

    @Override
    public long totalMoneyVinReceiveFromAgent(final String nickNameSend, final String nickNameRecieve, final String status, final String timeStart, final String timeEnd, final String topDS) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final MongoCollection collection = db.getCollection("log_chuyen_tien_dai_ly");
        final HashMap<String, Object> conditions = new HashMap<>();
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        if ((nickNameSend != null && !nickNameSend.equals("")) || (nickNameRecieve != null && !nickNameRecieve.equals(""))) {
            final BasicDBObject query1 = new BasicDBObject("nick_name_send", nickNameSend);
            final BasicDBObject query2 = new BasicDBObject("nick_name_receive", nickNameRecieve);
            final ArrayList<BasicDBObject> myList = new ArrayList<>();
            myList.add(query1);
            myList.add(query2);
            conditions.put("$or", myList);
        }
        if (status != null && !status.equals("")) {
            conditions.put("status", Integer.parseInt(status));
        }
        if (topDS != null && !topDS.equals("")) {
            conditions.put("top_ds", Integer.parseInt(topDS));
        }
        if (timeStart != null && !timeStart.isEmpty() && timeEnd != null && !timeEnd.isEmpty()) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("trans_time", obj);
        }
        final AggregateIterable<Document> iterable = (AggregateIterable<Document>) collection.aggregate(Arrays.asList(new Document("$match", new Document(conditions)), new Document("$group", new Document("_id", "$nick_name_receive").append("money", new Document("$sum", "$money_receive")))));
        long totalMoney = 0L;
        for (final Document row : iterable) {
            totalMoney += row.getLong("money");
        }
        return totalMoney;
    }

    @Override
    public long totalMoneyVinSendFromAgent(final String nickNameSend, final String nickNameRecieve, final String status, final String timeStart, final String timeEnd, final String topDS) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final MongoCollection collection = db.getCollection("log_chuyen_tien_dai_ly");
        final HashMap<String, Object> conditions = new HashMap<>();
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        if ((nickNameSend != null && !nickNameSend.equals("")) || (nickNameRecieve != null && !nickNameRecieve.equals(""))) {
            final BasicDBObject query1 = new BasicDBObject("nick_name_send", nickNameSend);
            final BasicDBObject query2 = new BasicDBObject("nick_name_receive", nickNameRecieve);
            final ArrayList<BasicDBObject> myList = new ArrayList<>();
            myList.add(query1);
            myList.add(query2);
            conditions.put("$or", myList);
        }
        if (status != null && !status.equals("")) {
            conditions.put("status", Integer.parseInt(status));
        }
        if (topDS != null && !topDS.equals("")) {
            conditions.put("top_ds", Integer.parseInt(topDS));
        }
        if (timeStart != null && !timeStart.isEmpty() && timeEnd != null && !timeEnd.isEmpty()) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("trans_time", obj);
        }
        final AggregateIterable<Document> iterable = (AggregateIterable<Document>) collection.aggregate(Arrays.asList(new Document("$match", new Document(conditions)), new Document("$group", new Document("_id", "$nick_name_send").append("money", new Document("$sum", "$money_send")))));
        long totalMoney = 0L;
        for (final Document row : iterable) {
            totalMoney += row.getLong("money");
        }
        return totalMoney;
    }

    @Override
    public long totalMoneyVinFeeFromAgent(final String nickNameSend, final String nickNameRecieve, final String status, final String timeStart, final String timeEnd, final String topDS) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final MongoCollection collection = db.getCollection("log_chuyen_tien_dai_ly");
        final HashMap<String, Object> conditions = new HashMap<>();
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        if ((nickNameSend != null && !nickNameSend.equals("")) || (nickNameRecieve != null && !nickNameRecieve.equals(""))) {
            final BasicDBObject query1 = new BasicDBObject("nick_name_send", nickNameSend);
            final BasicDBObject query2 = new BasicDBObject("nick_name_receive", nickNameRecieve);
            final ArrayList<BasicDBObject> myList = new ArrayList<>();
            myList.add(query1);
            myList.add(query2);
            conditions.put("$or", myList);
        }
        if (status != null && !status.equals("")) {
            conditions.put("status", Integer.parseInt(status));
        }
        if (topDS != null && !topDS.equals("")) {
            conditions.put("top_ds", Integer.parseInt(topDS));
        }
        if (timeStart != null && !timeStart.isEmpty() && timeEnd != null && !timeEnd.isEmpty()) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("trans_time", obj);
        }
        final AggregateIterable<Document> iterable = (AggregateIterable<Document>) collection.aggregate(Arrays.asList(new Document("$match", new Document(conditions)), new Document("$group", new Document("_id", "").append("money", new Document("$sum", "$fee")))));
        long totalMoney = 0L;
        for (final Document row : iterable) {
            totalMoney += row.getLong("money");
        }
        return totalMoney;
    }

    @Override
    public List<LogAgentTranferMoneyResponse> searchAgentTranferMoneyVinSale(final String nickName, final String timeStart, final String timeEnd, final String type, final int page, final int totalRecord) {
        final ArrayList<LogAgentTranferMoneyResponse> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final BasicDBObject objNN = new BasicDBObject();
        final BasicDBObject objSTT = new BasicDBObject();
        final BasicDBList lstAll = new BasicDBList();
        final HashMap<String, Object> conditions = new HashMap<>();
        FindIterable iterable = null;
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        final int numStart = (page - 1) * totalRecord;
        objsort.put("_id", -1);
        if (type.equals("1")) {
            if (nickName != null && !nickName.equals("")) {
                final BasicDBObject nnSend = new BasicDBObject("nick_name_send", nickName);
                final BasicDBObject nnReceive = new BasicDBObject("nick_name_receive", nickName);
                final ArrayList<BasicDBObject> lstNN = new ArrayList<>();
                lstNN.add(nnSend);
                lstNN.add(nnReceive);
                objNN.put("$or", lstNN);
            }
            final BasicDBObject query1 = new BasicDBObject("status", 1);
            final BasicDBObject query2 = new BasicDBObject("status", 2);
            final ArrayList<BasicDBObject> myList = new ArrayList<>();
            myList.add(query1);
            myList.add(query2);
            objSTT.put("$or", myList);
            lstAll.add(objNN);
            lstAll.add(objSTT);
            conditions.put("$and", lstAll);
        }
        if (type.equals("2")) {
            if (nickName != null && !nickName.equals("")) {
                final BasicDBObject nnSend = new BasicDBObject("nick_name_send", nickName);
                final BasicDBObject nnReceive = new BasicDBObject("nick_name_receive", nickName);
                final ArrayList<BasicDBObject> lstNN = new ArrayList<>();
                lstNN.add(nnSend);
                lstNN.add(nnReceive);
                objNN.put("$or", lstNN);
            }
            final BasicDBObject query1 = new BasicDBObject("status", 3);
            final BasicDBObject query2 = new BasicDBObject("status", 6);
            final ArrayList<BasicDBObject> myList = new ArrayList<>();
            myList.add(query1);
            myList.add(query2);
            objSTT.put("$or", myList);
            lstAll.add(objNN);
            lstAll.add(objSTT);
            conditions.put("$and", lstAll);
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("trans_time", obj);
        }
        iterable = db.getCollection("log_chuyen_tien_dai_ly").find(new Document(conditions)).sort(objsort).skip(numStart).limit(totalRecord);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final LogAgentTranferMoneyResponse trans = new LogAgentTranferMoneyResponse();
                trans.nick_name_send = document.getString("nick_name_send");
                trans.nick_name_receive = document.getString("nick_name_receive");
                trans.status = document.getInteger("status");
                trans.trans_time = document.getString("trans_time");
                trans.fee = document.getLong("fee");
                trans.money_send = document.getLong("money_send");
                trans.money_receive = document.getLong("money_receive");
                results.add(trans);
            }
        });
        return results;
    }

    @Override
    public long countSearchAgentTranferMoneyVinSale(final String nickName, final String timeStart, final String timeEnd, final String type) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        final BasicDBObject objNN = new BasicDBObject();
        final BasicDBObject objSTT = new BasicDBObject();
        final BasicDBList lstAll = new BasicDBList();
        objsort.put("_id", -1);
        if (type.equals("1")) {
            if (!nickName.equals(null) && !nickName.equals("")) {
                final BasicDBObject nnSend = new BasicDBObject("nick_name_send", nickName);
                final BasicDBObject nnReceive = new BasicDBObject("nick_name_receive", nickName);
                final ArrayList<BasicDBObject> lstNN = new ArrayList<>();
                lstNN.add(nnSend);
                lstNN.add(nnReceive);
                objNN.put("$or", lstNN);
            }
            final BasicDBObject query1 = new BasicDBObject("status", 1);
            final BasicDBObject query2 = new BasicDBObject("status", 2);
            final ArrayList<BasicDBObject> myList = new ArrayList<>();
            myList.add(query1);
            myList.add(query2);
            objSTT.put("$or", myList);
            lstAll.add(objNN);
            lstAll.add(objSTT);
            conditions.put("$and", lstAll);
        }
        if (type.equals("2")) {
            if (!nickName.equals(null) && !nickName.equals("")) {
                final BasicDBObject nnSend = new BasicDBObject("nick_name_send", nickName);
                final BasicDBObject nnReceive = new BasicDBObject("nick_name_receive", nickName);
                final ArrayList<BasicDBObject> lstNN = new ArrayList<>();
                lstNN.add(nnSend);
                lstNN.add(nnReceive);
                objNN.put("$or", lstNN);
            }
            final BasicDBObject query1 = new BasicDBObject("status", 3);
            final BasicDBObject query2 = new BasicDBObject("status", 6);
            final ArrayList<BasicDBObject> myList = new ArrayList<>();
            myList.add(query1);
            myList.add(query2);
            objSTT.put("$or", myList);
            lstAll.add(objNN);
            lstAll.add(objSTT);
            conditions.put("$and", lstAll);
        }
        if (!timeStart.equals(null) && !timeStart.equals("") && !timeEnd.equals(null) && !timeEnd.equals("")) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("trans_time", obj);
        }
        return db.getCollection("log_chuyen_tien_dai_ly").count(new Document(conditions));
    }

    @Override
    public long totalMoneyVinReceiveFromAgentByStatus(final String nickName, final String type, final String timeStart, final String timeEnd) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final MongoCollection collection = db.getCollection("log_chuyen_tien_dai_ly");
        final HashMap<String, Object> conditions = new HashMap<>();
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        final BasicDBObject objNN = new BasicDBObject();
        final BasicDBObject objSTT = new BasicDBObject();
        final BasicDBList lstAll = new BasicDBList();
        objsort.put("_id", -1);
        if (type.equals("1")) {
            if (!nickName.equals(null) && !nickName.equals("")) {
                final BasicDBObject nnSend = new BasicDBObject("nick_name_send", nickName);
                final BasicDBObject nnReceive = new BasicDBObject("nick_name_receive", nickName);
                final ArrayList<BasicDBObject> lstNN = new ArrayList<>();
                lstNN.add(nnSend);
                lstNN.add(nnReceive);
                objNN.put("$or", lstNN);
            }
            final BasicDBObject query1 = new BasicDBObject("status", 1);
            final BasicDBObject query2 = new BasicDBObject("status", 2);
            final ArrayList<BasicDBObject> myList = new ArrayList<>();
            myList.add(query1);
            myList.add(query2);
            objSTT.put("$or", myList);
            lstAll.add(objNN);
            lstAll.add(objSTT);
            conditions.put("$and", lstAll);
        }
        if (type.equals("2")) {
            if (!nickName.equals(null) && !nickName.equals("")) {
                final BasicDBObject nnSend = new BasicDBObject("nick_name_send", nickName);
                final BasicDBObject nnReceive = new BasicDBObject("nick_name_receive", nickName);
                final ArrayList<BasicDBObject> lstNN = new ArrayList<>();
                lstNN.add(nnSend);
                lstNN.add(nnReceive);
                objNN.put("$or", lstNN);
            }
            final BasicDBObject query1 = new BasicDBObject("status", 3);
            final BasicDBObject query2 = new BasicDBObject("status", 6);
            final ArrayList<BasicDBObject> myList = new ArrayList<>();
            myList.add(query1);
            myList.add(query2);
            objSTT.put("$or", myList);
            lstAll.add(objNN);
            lstAll.add(objSTT);
            conditions.put("$and", lstAll);
        }
        if (!timeStart.equals(null) && !timeStart.equals("") && !timeEnd.equals(null) && !timeEnd.equals("")) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("trans_time", obj);
        }
        final AggregateIterable<Document> iterable = (AggregateIterable<Document>) collection.aggregate(Arrays.asList(new Document("$match", new Document(conditions)), new Document("$group", new Document("_id", "$nick_name_receive").append("money", new Document("$sum", "$money_receive")))));
        long totalMoney = 0L;
        for (final Document row : iterable) {
            totalMoney += row.getLong("money");
        }
        return totalMoney;
    }

    @Override
    public long totalMoneyVinSendFromAgentByStatus(final String nickName, final String type, final String timeStart, final String timeEnd) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final MongoCollection collection = db.getCollection("log_chuyen_tien_dai_ly");
        final HashMap<String, Object> conditions = new HashMap<>();
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        final BasicDBObject objNN = new BasicDBObject();
        final BasicDBObject objSTT = new BasicDBObject();
        final BasicDBList lstAll = new BasicDBList();
        objsort.put("_id", -1);
        if (type.equals("1")) {
            if (!nickName.equals(null) && !nickName.equals("")) {
                final BasicDBObject nnSend = new BasicDBObject("nick_name_send", nickName);
                final BasicDBObject nnReceive = new BasicDBObject("nick_name_receive", nickName);
                final ArrayList<BasicDBObject> lstNN = new ArrayList<>();
                lstNN.add(nnSend);
                lstNN.add(nnReceive);
                objNN.put("$or", lstNN);
            }
            final BasicDBObject query1 = new BasicDBObject("status", 1);
            final BasicDBObject query2 = new BasicDBObject("status", 2);
            final ArrayList<BasicDBObject> myList = new ArrayList<>();
            myList.add(query1);
            myList.add(query2);
            objSTT.put("$or", myList);
            lstAll.add(objNN);
            lstAll.add(objSTT);
            conditions.put("$and", lstAll);
        }
        if (type.equals("2")) {
            if (!nickName.equals(null) && !nickName.equals("")) {
                final BasicDBObject nnSend = new BasicDBObject("nick_name_send", nickName);
                final BasicDBObject nnReceive = new BasicDBObject("nick_name_receive", nickName);
                final ArrayList<BasicDBObject> lstNN = new ArrayList<>();
                lstNN.add(nnSend);
                lstNN.add(nnReceive);
                objNN.put("$or", lstNN);
            }
            final BasicDBObject query1 = new BasicDBObject("status", 3);
            final BasicDBObject query2 = new BasicDBObject("status", 6);
            final ArrayList<BasicDBObject> myList = new ArrayList<>();
            myList.add(query1);
            myList.add(query2);
            objSTT.put("$or", myList);
            lstAll.add(objNN);
            lstAll.add(objSTT);
            conditions.put("$and", lstAll);
        }
        if (!timeStart.equals(null) && !timeStart.equals("") && !timeEnd.equals(null) && !timeEnd.equals("")) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("trans_time", obj);
        }
        final AggregateIterable<Document> iterable = (AggregateIterable<Document>) collection.aggregate(Arrays.asList(new Document("$match", new Document(conditions)), new Document("$group", new Document("_id", "$nick_name_send").append("money", new Document("$sum", "$money_send")))));
        long totalMoney = 0L;
        for (final Document row : iterable) {
            totalMoney += row.getLong("money");
        }
        return totalMoney;
    }

    @Override
    public boolean updateTopDsFromAgent(final String nickNameSend, final String nickNameReceive, final String timeLog, final String topds) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final MongoCollection col = db.getCollection("log_chuyen_tien_dai_ly");
        final HashMap<String, Object> conditions = new HashMap<>();
        conditions.put("trans_time", timeLog);
        conditions.put("nick_name_send", nickNameSend);
        conditions.put("nick_name_receive", nickNameReceive);
        col.updateOne(new Document(conditions), new Document("$set", new Document("top_ds", Integer.parseInt(topds))));
        return true;
    }

    @Override
    public boolean updateTopDsFromAgentMySQL(final String nickNameSend, final String nickNameReceive, final String timeLog, final String topds) throws SQLException {
        try (final Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stmt = conn.prepareStatement(" UPDATE vinplay.log_tranfer_agent  SET top_ds = ?,      update_time = ?  WHERE trans_time = ?        AND nick_name_send = ?        AND nick_name_receive = ? ");) {
            stmt.setInt(1, Integer.parseInt(topds));
            stmt.setString(2, DateTimeUtils.getCurrentTime("yyyy-MM-dd HH:mm:ss"));
            stmt.setString(3, timeLog);
            stmt.setString(4, nickNameSend);
            stmt.setString(5, nickNameReceive);
            stmt.executeUpdate();
        }
        return true;
    }

    @Override
    public boolean logBonusTopDS(final BonusTopDSModel model) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final MongoCollection col = db.getCollection("log_bonus_top_ds_agent");
        final Document doc = new Document();
        doc.append("nick_name", model.getNickname());
        doc.append("ds", model.getDs());
        doc.append("top", model.getTop());
        doc.append("bonus_fix", model.getBonusFix());
        doc.append("bonus_more", model.getBonusMore());
        doc.append("ds_2", model.getDs2());
        doc.append("top_2", model.getTop2());
        doc.append("bonus_fix_2", model.getBonusFix2());
        doc.append("bonus_more_2", model.getBonusMore2());
        doc.append("bonus_total", model.getBonusTotal());
        doc.append("month", model.getMonth());
        doc.append("code", model.getCode());
        doc.append("description", model.getDescription());
        doc.append("time_log", model.getTimeLog());
        doc.append("create_time", new Date());
        doc.append("bonus_vinplay_card", model.getBonusVinplayCard());
        doc.append("bonus_vin_cash", model.getBonusVinCash());
        doc.append("percent", model.getPercent());
        col.insertOne(doc);
        return true;
    }

    @Override
    public boolean checkBonusTopDS(final String nickname, final String month) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        conditions.put("nick_name", nickname);
        conditions.put("month", month);
        conditions.put("code", Integer.parseInt("0"));
        final FindIterable iterable = db.getCollection("log_bonus_top_ds_agent").find(new Document(conditions)).limit(1);
        final Document document = (iterable != null) ? ((Document) iterable.first()) : null;
        return document != null;
    }

    @Override
    public List<BonusTopDSModel> getLogBonusTopDS(final String nickname, final String month) {
        final ArrayList<BonusTopDSModel> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        if (nickname != null && !nickname.equals("")) {
            conditions.put("nick_name", nickname);
        }
        if (month != null && !month.equals("")) {
            conditions.put("month", month);
        }
        final FindIterable iterable = db.getCollection("log_bonus_top_ds_agent").find(new Document(conditions));
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final BonusTopDSModel model = new BonusTopDSModel(document.getString("nick_name"), document.getLong("ds"), document.getInteger("top"), document.getLong("bonus_fix"), document.getLong("bonus_more"), (document.getLong("ds_2") == null) ? 0L : document.getLong("ds_2"), (document.getInteger("top_2") == null) ? 0 : document.getInteger("top_2"), (document.getLong("bonus_fix_2") == null) ? 0L : document.getLong("bonus_fix_2"), (document.getLong("bonus_more_2") == null) ? 0L : document.getLong("bonus_more_2"), document.getLong("bonus_total"), document.getString("month"), document.getInteger("code"), document.getString("description"), document.getString("time_log"), (document.getLong("bonus_vinplay_card") == null) ? 0L : document.getLong("bonus_vinplay_card"), (document.getLong("bonus_vin_cash") == null) ? 0L : document.getLong("bonus_vin_cash"), (document.getInteger("percent") == null) ? 0 : document.getInteger("percent"));
                results.add(model);
            }
        });
        return results;
    }

    @Override
    public TranferMoneyAgent getTransferMoneyAgent(final String nickNameSend, final String nickNameReceive, final String timeLog) {
        TranferMoneyAgent results = null;
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final MongoCollection col = db.getCollection("log_chuyen_tien_dai_ly");
        final HashMap<String, Object> conditions = new HashMap<>();
        conditions.put("trans_time", timeLog);
        conditions.put("nick_name_send", nickNameSend);
        conditions.put("nick_name_receive", nickNameReceive);
        final Document dc = (Document) col.find(new Document(conditions)).first();
        if (dc != null) {
            final int status = dc.getInteger("status");
            final long moneySend = dc.getLong("money_send");
            final long moneyReceive = dc.getLong("money_receive");
            results = new TranferMoneyAgent(nickNameSend, nickNameReceive, moneySend, moneyReceive, status);
        }
        return results;
    }

    @Override
    public List<AgentResponse> listUserAgentAdmin(final String nickName) throws SQLException {
        final ArrayList<AgentResponse> results = new ArrayList<>();
        String sql = "";
        if (nickName != null && !nickName.equals("")) {
            sql = "SELECT nameagent,nickname,phone,address,id,parentid,`show`,active,percent_bonus_vincard FROM useragent WHERE status='D'  and  nickname=?";
        } else {
            sql = "SELECT nameagent,nickname,phone,address,id,parentid,`show`,active,percent_bonus_vincard FROM useragent WHERE status='D'";
        }
        try (final Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_admin");
             PreparedStatement stmt = conn.prepareStatement(sql);) {
            if (nickName != null && !nickName.equals("")) {
                stmt.setString(1, nickName);
            }
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    final AgentResponse agent = new AgentResponse();
                    agent.fullName = rs.getString("nameagent");
                    agent.nickName = rs.getString("nickname");
                    agent.mobile = rs.getString("phone");
                    agent.address = rs.getString("address");
                    agent.id = rs.getInt("id");
                    agent.parentid = rs.getInt("parentid");
                    agent.show = rs.getInt("show");
                    agent.active = rs.getInt("active");
                    agent.percent = rs.getInt("percent_bonus_vincard");
                    results.add(0, agent);
                }
            }
        }
        return results;
    }

    @Override
    public List<LogAgentTranferMoneyResponse> searchAgentTranferMoney(final String nickName, final String status, final String timeStart, final String timeEnd, final String top_ds, final int page) {
        final ArrayList<LogAgentTranferMoneyResponse> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        FindIterable iterable = null;
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        final int numStart = (page - 1) * 50;
        final int numEnd = 50;
        objsort.put("_id", -1);
        if (nickName != null && !nickName.equals("")) {
            final BasicDBObject query1 = new BasicDBObject("nick_name_send", nickName);
            final BasicDBObject query2 = new BasicDBObject("nick_name_receive", nickName);
            final ArrayList<BasicDBObject> myList = new ArrayList<>();
            myList.add(query1);
            myList.add(query2);
            conditions.put("$or", myList);
        }
        if (top_ds != null && !top_ds.equals("")) {
            conditions.put("top_ds", Integer.parseInt(top_ds));
        }
        if (status != null && !status.equals("")) {
            conditions.put("status", Integer.parseInt(status));
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("trans_time", obj);
        }
        iterable = db.getCollection("log_chuyen_tien_dai_ly").find(new Document(conditions)).sort(objsort).skip(numStart).limit(50);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final LogAgentTranferMoneyResponse trans = new LogAgentTranferMoneyResponse();
                trans.nick_name_send = document.getString("nick_name_send");
                trans.nick_name_receive = document.getString("nick_name_receive");
                trans.status = document.getInteger("status");
                trans.trans_time = document.getString("trans_time");
                trans.fee = document.getLong("fee");
                trans.money_send = document.getLong("money_send");
                trans.money_receive = document.getLong("money_receive");
                trans.top_ds = document.getInteger("top_ds");
                trans.process = document.getInteger("process");
                trans.des_send = ((document.getString("des_send") != null && !document.getString("des_send").equals("")) ? document.getString("des_send") : "");
                trans.des_receive = ((document.getString("des_receive") != null && !document.getString("des_receive").equals("")) ? document.getString("des_receive") : "");
                results.add(trans);
            }
        });
        return results;
    }

    @Override
    public LogAgentTranferMoneyResponse searchAgentTranferMoney(final String tid) {
        final LogAgentTranferMoneyResponse result = new LogAgentTranferMoneyResponse();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        FindIterable iterable = null;
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        conditions.put("transaction_no", tid);
        iterable = db.getCollection("log_chuyen_tien_dai_ly").find(new Document(conditions)).sort(objsort).skip(0).limit(1);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                result.nick_name_send = document.getString("nick_name_send");
                result.nick_name_receive = document.getString("nick_name_receive");
                result.status = document.getInteger("status");
                result.trans_time = document.getString("trans_time");
                result.fee = document.getLong("fee");
                result.money_send = document.getLong("money_send");
                result.money_receive = document.getLong("money_receive");
                result.top_ds = document.getInteger("top_ds");
                result.process = document.getInteger("process");
                result.des_send = ((document.getString("des_send") != null && !document.getString("des_send").equals("")) ? document.getString("des_send") : "");
                result.des_receive = ((document.getString("des_receive") != null && !document.getString("des_receive").equals("")) ? document.getString("des_receive") : "");
            }
        });
        return result;
    }

    @Override
    public long countsearchAgentTranferMoney(final String nickName, final String status, final String timeStart, final String timeEnd, final String top_ds) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        if (nickName != null && !nickName.equals("")) {
            final BasicDBObject query1 = new BasicDBObject("nick_name_send", nickName);
            final BasicDBObject query2 = new BasicDBObject("nick_name_receive", nickName);
            final ArrayList<BasicDBObject> myList = new ArrayList<>();
            myList.add(query1);
            myList.add(query2);
            conditions.put("$or", myList);
        }
        if (top_ds != null && !top_ds.equals("")) {
            conditions.put("top_ds", Integer.parseInt(top_ds));
        }
        if (status != null && !status.equals("")) {
            conditions.put("status", Integer.parseInt(status));
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("trans_time", obj);
        }
        return db.getCollection("log_chuyen_tien_dai_ly").count(new Document(conditions));
    }

    @Override
    public long totalMoneyVinReceiveFromAgent(final String nickName, final String status, final String timeStart, final String timeEnd, final String topDS) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final MongoCollection collection = db.getCollection("log_chuyen_tien_dai_ly");
        final HashMap<String, Object> conditions = new HashMap<>();
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        if (nickName != null && !nickName.equals("")) {
            final BasicDBObject query1 = new BasicDBObject("nick_name_send", nickName);
            final BasicDBObject query2 = new BasicDBObject("nick_name_receive", nickName);
            final ArrayList<BasicDBObject> myList = new ArrayList<>();
            myList.add(query1);
            myList.add(query2);
            conditions.put("$or", myList);
        }
        if (status != null && !status.equals("")) {
            conditions.put("status", Integer.parseInt(status));
        }
        if (topDS != null && !topDS.equals("")) {
            conditions.put("top_ds", Integer.parseInt(topDS));
        }
        if (timeStart != null && !timeStart.isEmpty() && timeEnd != null && !timeEnd.isEmpty()) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("trans_time", obj);
        }
        final AggregateIterable<Document> iterable = (AggregateIterable<Document>) collection.aggregate(Arrays.asList(new Document("$match", new Document(conditions)), new Document("$group", new Document("_id", "$nick_name_receive").append("money", new Document("$sum", "$money_receive")))));
        long totalMoney = 0L;
        for (final Document row : iterable) {
            totalMoney += row.getLong("money");
        }
        return totalMoney;
    }

    @Override
    public long totalMoneyVinSendFromAgent(final String nickName, final String status, final String timeStart, final String timeEnd, final String topDS) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final MongoCollection collection = db.getCollection("log_chuyen_tien_dai_ly");
        final HashMap<String, Object> conditions = new HashMap<>();
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        if (nickName != null && !nickName.equals("")) {
            final BasicDBObject query1 = new BasicDBObject("nick_name_send", nickName);
            final BasicDBObject query2 = new BasicDBObject("nick_name_receive", nickName);
            final ArrayList<BasicDBObject> myList = new ArrayList<>();
            myList.add(query1);
            myList.add(query2);
            conditions.put("$or", myList);
        }
        if (status != null && !status.equals("")) {
            conditions.put("status", Integer.parseInt(status));
        }
        if (topDS != null && !topDS.equals("")) {
            conditions.put("top_ds", Integer.parseInt(topDS));
        }
        if (timeStart != null && !timeStart.isEmpty() && timeEnd != null && !timeEnd.isEmpty()) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("trans_time", obj);
        }
        final AggregateIterable<Document> iterable = (AggregateIterable<Document>) collection.aggregate(Arrays.asList(new Document("$match", new Document(conditions)), new Document("$group", new Document("_id", "$nick_name_send").append("money", new Document("$sum", "$money_send")))));
        long totalMoney = 0L;
        for (final Document row : iterable) {
            totalMoney += row.getLong("money");
        }
        return totalMoney;
    }

    @Override
    public long totalMoneyVinFeeFromAgent(final String nickName, final String status, final String timeStart, final String timeEnd, final String topDS) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final MongoCollection collection = db.getCollection("log_chuyen_tien_dai_ly");
        final HashMap<String, Object> conditions = new HashMap<>();
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        if (nickName != null && !nickName.equals("")) {
            final BasicDBObject query1 = new BasicDBObject("nick_name_send", nickName);
            final BasicDBObject query2 = new BasicDBObject("nick_name_receive", nickName);
            final ArrayList<BasicDBObject> myList = new ArrayList<>();
            myList.add(query1);
            myList.add(query2);
            conditions.put("$or", myList);
        }
        if (status != null && !status.equals("")) {
            conditions.put("status", Integer.parseInt(status));
        }
        if (topDS != null && !topDS.equals("")) {
            conditions.put("top_ds", Integer.parseInt(topDS));
        }
        if (timeStart != null && !timeStart.isEmpty() && timeEnd != null && !timeEnd.isEmpty()) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("trans_time", obj);
        }
        final AggregateIterable<Document> iterable = (AggregateIterable<Document>) collection.aggregate(Arrays.asList(new Document("$match", new Document(conditions)), new Document("$group", new Document("_id", "").append("money", new Document("$sum", "$fee")))));
        long totalMoney = 0L;
        for (final Document row : iterable) {
            totalMoney += row.getLong("money");
        }
        return totalMoney;
    }

    @Override
    public List<AgentResponse> listUserAgentLevel2ByParentID(final int ParentID) throws SQLException {
        final ArrayList<AgentResponse> results = new ArrayList<>();
        String sql = "SELECT nameagent,nickname,phone,address,id,parentid FROM useragent WHERE status='D' and  active=1 and parentid=?";
        try (final Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_admin");
             final PreparedStatement stmt = conn.prepareStatement(sql);) {
            stmt.setInt(1, ParentID);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    final AgentResponse agent = new AgentResponse();
                    agent.fullName = rs.getString("nameagent");
                    agent.nickName = rs.getString("nickname");
                    agent.mobile = rs.getString("phone");
                    agent.address = rs.getString("address");
                    agent.id = rs.getInt("id");
                    agent.parentid = rs.getInt("parentid");
                    results.add(0, agent);
                }
            }
        }
        return results;
    }

    @Override
    public List<AgentResponse> listUserAgentLevel2ByID(final int ID) throws SQLException {
        final ArrayList<AgentResponse> results = new ArrayList<>();
        String sql = "SELECT nameagent,nickname,phone,address,id,parentid FROM useragent WHERE status='D' and `show`=1 and id=?";
        try (final Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_admin");
             final PreparedStatement stmt = conn.prepareStatement(sql);) {
            stmt.setInt(1, ID);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    final AgentResponse agent = new AgentResponse();
                    agent.fullName = rs.getString("nameagent");
                    agent.nickName = rs.getString("nickname");
                    agent.mobile = rs.getString("phone");
                    agent.address = rs.getString("address");
                    agent.id = rs.getInt("id");
                    agent.parentid = rs.getInt("parentid");
                    results.add(0, agent);
                }
            }
        }
        return results;
    }

    @Override
    public List<AgentResponse> listUserAgentActive(final String nickName) throws SQLException {
        final ArrayList<AgentResponse> results = new ArrayList<>();
        String sql = "";
        if (nickName != null && !nickName.equals("")) {
            sql = "SELECT nameagent,nickname,phone,address,id,parentid,`show`,active FROM useragent WHERE status='D' and `show` = 1 and active=1 and parentid=-1 and  nickname=?";
        } else {
            sql = "SELECT nameagent,nickname,phone,address,id,parentid,`show`,active FROM useragent WHERE status='D' and active=1 and parentid=-1 and `show` = 1";
        }
        try (final Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_admin");
             PreparedStatement stmt = conn.prepareStatement(sql);) {
            if (nickName != null && !nickName.equals("")) {
                stmt.setString(1, nickName);
            }
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    final AgentResponse agent = new AgentResponse();
                    agent.fullName = rs.getString("nameagent");
                    agent.nickName = rs.getString("nickname");
                    agent.mobile = rs.getString("phone");
                    agent.address = rs.getString("address");
                    agent.id = rs.getInt("id");
                    agent.parentid = rs.getInt("parentid");
                    agent.show = rs.getInt("show");
                    agent.active = rs.getInt("active");
                    results.add(0, agent);
                }
            }
        }
        return results;
    }

    @Override
    public List<AgentResponse> listUserAgentLevel1Active() throws SQLException {
        final ArrayList<AgentResponse> results = new ArrayList<>();
        String sql = "SELECT nameagent,nickname,phone,address,id,parentid,`show`,active FROM useragent WHERE status='D' and active=1 and parentid=-1 and `show` = 1";
        try (final Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_admin");
             final PreparedStatement stmt = conn.prepareStatement(sql);
             final ResultSet rs = stmt.executeQuery();) {
            while (rs.next()) {
                final AgentResponse agent = new AgentResponse();
                agent.fullName = rs.getString("nameagent");
                agent.nickName = rs.getString("nickname");
                agent.mobile = rs.getString("phone");
                agent.address = rs.getString("address");
                agent.id = rs.getInt("id");
                agent.parentid = rs.getInt("parentid");
                agent.show = rs.getInt("show");
                agent.active = rs.getInt("active");
                results.add(0, agent);
            }
        }
        return results;
    }

    @Override
    public List<LogAgentTranferMoneyResponse> searchAgentTongTranferMoney(final String nickNameSend, final String nickNameRecieve, final String status, final String timeStart, final String timeEnd, final String top_ds, final int page) {
        final ArrayList<LogAgentTranferMoneyResponse> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        FindIterable iterable = null;
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        final int numStart = (page - 1) * 50;
        final int numEnd = 50;
        objsort.put("_id", -1);
        if (nickNameSend != null && !nickNameSend.equals("")) {
            conditions.put("nick_name_send", nickNameSend);
        }
        if (nickNameRecieve != null && !nickNameRecieve.equals("")) {
            conditions.put("nick_name_receive", nickNameRecieve);
        }
        if (top_ds != null && !top_ds.equals("")) {
            conditions.put("top_ds", Integer.parseInt(top_ds));
        }
        if (status != null && !status.equals("")) {
            conditions.put("status", Integer.parseInt(status));
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("trans_time", obj);
        }
        iterable = db.getCollection("log_chuyen_tien_dai_ly").find(new Document(conditions)).sort(objsort).skip(numStart).limit(50);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final LogAgentTranferMoneyResponse trans = new LogAgentTranferMoneyResponse();
                trans.nick_name_send = document.getString("nick_name_send");
                trans.nick_name_receive = document.getString("nick_name_receive");
                trans.status = document.getInteger("status");
                trans.trans_time = document.getString("trans_time");
                trans.fee = document.getLong("fee");
                trans.money_send = document.getLong("money_send");
                trans.money_receive = document.getLong("money_receive");
                trans.top_ds = document.getInteger("top_ds");
                trans.process = document.getInteger("process");
                trans.des_send = ((document.getString("des_send") != null && !document.getString("des_send").equals("")) ? document.getString("des_send") : "");
                trans.des_receive = ((document.getString("des_receive") != null && !document.getString("des_receive").equals("")) ? document.getString("des_receive") : "");
                results.add(trans);
            }
        });
        return results;
    }

    @Override
    public long countsearchAgentTongTranferMoney(final String nickNameSend, final String nickNameRecieve, final String status, final String timeStart, final String timeEnd, final String top_ds) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        if (nickNameSend != null && !nickNameSend.equals("")) {
            conditions.put("nick_name_send", nickNameSend);
        }
        if (nickNameRecieve != null && !nickNameRecieve.equals("")) {
            conditions.put("nick_name_receive", nickNameRecieve);
        }
        if (top_ds != null && !top_ds.equals("")) {
            conditions.put("top_ds", Integer.parseInt(top_ds));
        }
        if (status != null && !status.equals("")) {
            conditions.put("status", Integer.parseInt(status));
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("trans_time", obj);
        }
        return db.getCollection("log_chuyen_tien_dai_ly").count(new Document(conditions));
    }

    @Override
    public long totalMoneyVinReceiveFromAgentTong(final String nickNameSend, final String nickNameRecieve, final String status, final String timeStart, final String timeEnd, final String topDS) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final MongoCollection collection = db.getCollection("log_chuyen_tien_dai_ly");
        final HashMap<String, Object> conditions = new HashMap<>();
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        if (nickNameSend != null && !nickNameSend.equals("")) {
            conditions.put("nick_name_send", nickNameSend);
        }
        if (nickNameRecieve != null && !nickNameRecieve.equals("")) {
            conditions.put("nick_name_receive", nickNameRecieve);
        }
        if (status != null && !status.equals("")) {
            conditions.put("status", Integer.parseInt(status));
        }
        if (topDS != null && !topDS.equals("")) {
            conditions.put("top_ds", Integer.parseInt(topDS));
        }
        if (timeStart != null && !timeStart.isEmpty() && timeEnd != null && !timeEnd.isEmpty()) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("trans_time", obj);
        }
        final AggregateIterable<Document> iterable = (AggregateIterable<Document>) collection.aggregate(Arrays.asList(new Document("$match", new Document(conditions)), new Document("$group", new Document("_id", "$nick_name_receive").append("money", new Document("$sum", "$money_receive")))));
        long totalMoney = 0L;
        for (final Document row : iterable) {
            totalMoney += row.getLong("money");
        }
        return totalMoney;
    }

    @Override
    public long totalMoneyVinSendFromAgentTong(final String nickNameSend, final String nickNameRecieve, final String status, final String timeStart, final String timeEnd, final String topDS) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final MongoCollection collection = db.getCollection("log_chuyen_tien_dai_ly");
        final HashMap<String, Object> conditions = new HashMap<>();
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        if (nickNameSend != null && !nickNameSend.equals("")) {
            conditions.put("nick_name_send", nickNameSend);
        }
        if (nickNameRecieve != null && !nickNameRecieve.equals("")) {
            conditions.put("nick_name_receive", nickNameRecieve);
        }
        if (status != null && !status.equals("")) {
            conditions.put("status", Integer.parseInt(status));
        }
        if (topDS != null && !topDS.equals("")) {
            conditions.put("top_ds", Integer.parseInt(topDS));
        }
        if (timeStart != null && !timeStart.isEmpty() && timeEnd != null && !timeEnd.isEmpty()) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("trans_time", obj);
        }
        final AggregateIterable<Document> iterable = (AggregateIterable<Document>) collection.aggregate(Arrays.asList(new Document("$match", new Document(conditions)), new Document("$group", new Document("_id", "$nick_name_send").append("money", new Document("$sum", "$money_send")))));
        long totalMoney = 0L;
        for (final Document row : iterable) {
            totalMoney += row.getLong("money");
        }
        return totalMoney;
    }

    @Override
    public long totalMoneyVinFeeFromAgentTong(final String nickNameSend, final String nickNameRecieve, final String status, final String timeStart, final String timeEnd, final String topDS) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final MongoCollection collection = db.getCollection("log_chuyen_tien_dai_ly");
        final HashMap<String, Object> conditions = new HashMap<>();
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        if (nickNameSend != null && !nickNameSend.equals("")) {
            conditions.put("nick_name_send", nickNameSend);
        }
        if (nickNameRecieve != null && !nickNameRecieve.equals("")) {
            conditions.put("nick_name_receive", nickNameRecieve);
        }
        if (status != null && !status.equals("")) {
            conditions.put("status", Integer.parseInt(status));
        }
        if (topDS != null && !topDS.equals("")) {
            conditions.put("top_ds", Integer.parseInt(topDS));
        }
        if (timeStart != null && !timeStart.isEmpty() && timeEnd != null && !timeEnd.isEmpty()) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("trans_time", obj);
        }
        final AggregateIterable<Document> iterable = (AggregateIterable<Document>) collection.aggregate(Arrays.asList(new Document("$match", new Document(conditions)), new Document("$group", new Document("_id", "").append("money", new Document("$sum", "$fee")))));
        long totalMoney = 0L;
        for (final Document row : iterable) {
            totalMoney += row.getLong("money");
        }
        return totalMoney;
    }

    @Override
    public int registerPercentBonusVincard(final String nickName, final int percent) throws SQLException {
        int recordNumber = 0;
        try (final Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_admin");
             PreparedStatement stmt = conn.prepareStatement(" UPDATE vinplay_admin.useragent  SET percent_bonus_vincard = ?  WHERE nickname = ? ");) {
            stmt.setInt(1, percent);
            stmt.setString(2, nickName);
            recordNumber = stmt.executeUpdate();
        }
        return recordNumber;
    }

    @Override
    public List<AgentModel> getListPercentBonusVincard(final String nickName) throws SQLException {
        List<AgentModel> result = new ArrayList<>();
        if (nickName != null && !nickName.isEmpty()) {
            result = ("all".equals(nickName) ? this.getAllPercentBonusVincard() : this.getPercentBonusVincardByNickName(nickName));
        }
        return result;
    }

    private List<AgentModel> getAllPercentBonusVincard() throws SQLException {
        final ArrayList<AgentModel> result = new ArrayList<>();
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_admin");
             PreparedStatement stmt = conn.prepareStatement(" SELECT nickname, percent_bonus_vincard  FROM vinplay_admin.useragent  WHERE parentid = -1    AND active = 1    AND status = 'D'    AND `show` = 1  ORDER BY `order` ");
             ResultSet rs = stmt.executeQuery();) {
            while (rs.next()) {
                final AgentModel agentModel = new AgentModel(rs.getString("nickname"), rs.getInt("percent_bonus_vincard"));
                result.add(agentModel);
            }
        }
        return result;
    }

    private List<AgentModel> getPercentBonusVincardByNickName(final String nickName) throws SQLException {
        final ArrayList<AgentModel> result = new ArrayList<>();
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_admin");
             PreparedStatement stmt = conn.prepareStatement(" SELECT nickname, percent_bonus_vincard  FROM vinplay_admin.useragent  WHERE parentid = -1    AND nickname = ? ");) {
            stmt.setString(1, nickName);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    final AgentModel agentModel = new AgentModel(rs.getString("nickname"), rs.getInt("percent_bonus_vincard"));
                    result.add(agentModel);
                }
            }
        }
        return result;
    }
}
