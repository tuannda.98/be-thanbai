// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.vinplay.dal.dao.impl.LogMiniPokerDAOImpl;
import com.vinplay.dal.service.LogMiniPokerService;
import com.vinplay.vbee.common.response.MiniPokerResponse;

import java.util.List;

public class LogMiniPokerServiceImpl implements LogMiniPokerService {
    @Override
    public List<MiniPokerResponse> listMiniPoker(final String nickName, final String bet_value, final String timeStart, final String timeEnd, final String moneyType, final int page) {
        final LogMiniPokerDAOImpl dao = new LogMiniPokerDAOImpl();
        return dao.listMiniPoker(nickName, bet_value, timeStart, timeEnd, moneyType, page);
    }

    @Override
    public int countMiniPoker(final String nickName, final String bet_value, final String timeStart, final String timeEnd, final String moneyType) {
        final LogMiniPokerDAOImpl dao = new LogMiniPokerDAOImpl();
        return dao.countMiniPoker(nickName, bet_value, timeStart, timeEnd, moneyType);
    }
}
