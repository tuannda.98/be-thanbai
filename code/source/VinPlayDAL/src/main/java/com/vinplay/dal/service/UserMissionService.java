// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service;

import com.vinplay.vbee.common.response.userMission.LogReceivedRewardObj;

import java.sql.SQLException;
import java.util.List;

public interface UserMissionService {
    List<LogReceivedRewardObj> getLogReceivedReward(final String p0, final String p1, final String p2, final String p3, final String p4, final int p5);

    boolean updateUserMission(final String p0, final String p1, final String p2, final int p3) throws SQLException;
}
