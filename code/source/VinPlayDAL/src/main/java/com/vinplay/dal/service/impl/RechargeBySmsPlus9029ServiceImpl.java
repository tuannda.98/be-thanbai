// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.vinplay.dal.dao.impl.RechargeBySmsPlus9029DAOImpl;
import com.vinplay.dal.service.RechargeBySmsPlus9029Service;
import com.vinplay.vbee.common.response.MoneyTotalRechargeByCardReponse;
import com.vinplay.vbee.common.response.RechargeBySmsResponse;

import java.util.List;

public class RechargeBySmsPlus9029ServiceImpl implements RechargeBySmsPlus9029Service {
    @Override
    public List<MoneyTotalRechargeByCardReponse> moneyTotalRechargeBySmsPlus9029(final String timeStart, final String timeEnd, final String code) {
        final RechargeBySmsPlus9029DAOImpl dao = new RechargeBySmsPlus9029DAOImpl();
        return dao.moneyTotalRechargeBySmsPlus9029(timeStart, timeEnd, code);
    }

    @Override
    public List<RechargeBySmsResponse> exportDataRechargeBySmsPlus(final String timeStart, final String timeEnd, final String amount, final String code) {
        final RechargeBySmsPlus9029DAOImpl dao = new RechargeBySmsPlus9029DAOImpl();
        return dao.exportDataRechargeBySmsPlus(timeStart, timeEnd, amount, code);
    }
}
