// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao;

import com.vinplay.vbee.common.response.MiniPokerResponse;

import java.util.List;

public interface LogMiniPokerDAO {
    List<MiniPokerResponse> listMiniPoker(final String p0, final String p1, final String p2, final String p3, final String p4, final int p5);

    int countMiniPoker(final String p0, final String p1, final String p2, final String p3, final String p4);
}
