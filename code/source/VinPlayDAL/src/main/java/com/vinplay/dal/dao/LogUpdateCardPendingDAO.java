// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao;

import com.vinplay.vbee.common.response.LogUpdateCardPendingReponse;
import com.vinplay.vbee.common.response.MoneyTotalRechargeByCardReponse;

import java.util.List;

public interface LogUpdateCardPendingDAO {
    List<LogUpdateCardPendingReponse> searchLogUpdateCardPending(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5, final String p6, final int p7, final String p8, final String p9);

    int countTotalRecordLogUpdateCardPending(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5, final String p6, final String p7, final String p8);

    List<MoneyTotalRechargeByCardReponse> moneyTotalUpdateCardPengding(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5, final String p6, final String p7, final String p8);
}
