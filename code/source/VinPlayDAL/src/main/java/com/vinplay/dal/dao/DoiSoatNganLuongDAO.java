// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao;

import com.vinplay.vbee.common.response.NganLuongFollowFaceValue;

import java.util.List;

public interface DoiSoatNganLuongDAO {
    List<NganLuongFollowFaceValue> getDoiSoatData(final String p0, final String p1);
}
