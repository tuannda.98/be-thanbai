// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service;

import java.sql.SQLException;
import java.util.List;

public interface ListAgentLevel2Service {
    List<String> listAgentLevel2(final String p0) throws SQLException;

    String getAgentLevel1ByNickName(final String p0) throws SQLException;
}
