// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao.impl;

import com.vinplay.dal.dao.LogTaiXiuDAO;
import com.vinplay.vbee.common.pools.ConnectionPool;
import com.vinplay.vbee.common.response.TaiXiuDetailReponse;
import com.vinplay.vbee.common.response.TaiXiuResponse;
import com.vinplay.vbee.common.response.TaiXiuResultResponse;
import com.vinplay.vbee.common.utils.VinPlayUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LogTaiXiuDAOImpl implements LogTaiXiuDAO {
    @Override
    public List<TaiXiuResponse> listLogTaiXiu(final String referentId, final String userName, final String betSide, final String moneyType, final String timeStart, final String timeEnd, final int page) throws SQLException {
        final ArrayList<TaiXiuResponse> results = new ArrayList<>();
        final int num_start = (page - 1) * 50;
        String condition = "";
        final String limit = " ORDER BY reference_id DESC LIMIT " + num_start + ", " + 50 + "";
        if (referentId != null && !referentId.equals("")) {
            condition = condition + " AND reference_id=" + referentId;
        }
        if (userName != null && !userName.equals("")) {
            condition = condition + " AND user_name = '" + userName + "'";
        }
        if (betSide != null && !betSide.equals("")) {
            condition = condition + " AND bet_side=" + betSide;
        }
        if (moneyType != null && !moneyType.equals("")) {
            condition = condition + " AND money_type=" + moneyType;
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            condition = condition + " AND timestamp BETWEEN '" + timeStart + "' AND '" + timeEnd + "'";
        }
        final String sql = "SELECT reference_id,user_id,bet_value,user_name,bet_side,total_prize,total_refund,total_exchange,money_type,timestamp FROM transaction_tai_xiu where 1=1 " + condition + limit;
        try (final Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_minigame");
             final PreparedStatement stmt = conn.prepareStatement(sql);
             final ResultSet rs = stmt.executeQuery()
        ) {
            while (rs.next()) {
                final TaiXiuResponse entry = new TaiXiuResponse();
                entry.referenceId = rs.getLong("reference_id");
                entry.user_id = rs.getInt("user_id");
                entry.user_name = rs.getString("user_name");
                entry.bet_value = rs.getInt("bet_value");
                entry.bet_side = rs.getInt("bet_side");
                entry.total_prize = rs.getLong("total_prize");
                entry.total_refund = rs.getLong("total_refund");
                entry.total_exchange = rs.getLong("total_exchange");
                entry.money_type = rs.getInt("money_type");
                entry.time_log = VinPlayUtils.formatDate(rs.getString("timestamp"));
                results.add(entry);
            }
        }
        return results;
    }

    @Override
    public int countLogTaiXiu(final String referentId, final String userName, final String betSide, final String moneyType, final String timeStart, final String timeEnd) throws SQLException {
        int res = 0;
        final String query = "SELECT count(*) as total FROM transaction_tai_xiu where 1=1 ";
        String condition = "";
        if (referentId != null && referentId.equals("")) {
            condition = condition + " AND reference_id=" + referentId;
        }
        if (userName != null && userName.equals("")) {
            condition = condition + " AND user_name = '" + userName + "'";
        }
        if (betSide != null && betSide.equals("")) {
            condition = condition + " AND bet_side=" + betSide;
        }
        if (moneyType != null && moneyType.equals("")) {
            condition = condition + " AND money_type=" + moneyType;
        }
        if (timeStart != null && timeStart.equals("") && timeEnd != null && timeEnd.equals("")) {
            condition = condition + " AND timestamp BETWEEN '" + timeStart + "' AND '" + timeEnd + "'";
        }
        final String sql = "SELECT count(*) as total FROM transaction_tai_xiu where 1=1 " + condition;
        try (final Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_minigame");
             final PreparedStatement stmt = conn.prepareStatement(sql);
             final ResultSet rs = stmt.executeQuery()
        ) {
            if (rs.next()) {
                res = rs.getInt("total");
            }
        }
        return res;
    }

    @Override
    public List<TaiXiuDetailReponse> getLogTaiXiuDetail(final String referent_id, final String betSide, final String money_type, final String nickName, final int page) throws SQLException {
        final ArrayList<TaiXiuDetailReponse> results = new ArrayList<>();
        final int num_start = (page - 1) * 50;
        String condition = "";
        final String limit = " ORDER BY reference_id DESC LIMIT " + num_start + ", " + 50 + "";
        if (referent_id != null && !referent_id.equals("")) {
            condition = condition + " AND reference_id=" + referent_id;
        }
        if (betSide != null && !betSide.equals("")) {
            condition = condition + " AND bet_side=" + betSide;
        }
        if (nickName != null && !nickName.equals("")) {
            condition = condition + " AND user_name = '" + nickName + "'";
        }
        condition = condition + " AND money_type=" + money_type;
        final String sql = "SELECT reference_id,transaction_code,user_id,user_name,bet_value,bet_side,prize,refund,input_time,money_type,timestamp FROM transaction_detail_tai_xiu where 1=1 " + condition + limit;
        try (final Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_minigame");
             final PreparedStatement stmt = conn.prepareStatement(sql);
             final ResultSet rs = stmt.executeQuery()
        ) {
            while (rs.next()) {
                final TaiXiuDetailReponse entry = new TaiXiuDetailReponse();
                entry.reference_id = rs.getLong("reference_id");
                entry.transaction_code = rs.getString("transaction_code");
                entry.user_id = rs.getInt("user_id");
                entry.user_name = rs.getString("user_name");
                entry.bet_value = rs.getLong("bet_value");
                entry.bet_side = rs.getLong("bet_side");
                entry.prize = rs.getLong("prize");
                entry.refund = rs.getLong("refund");
                entry.input_time = rs.getInt("input_time");
                entry.money_type = rs.getInt("money_type");
                entry.time_log = rs.getString("timestamp");
                results.add(entry);
            }
        }
        return results;
    }

    @Override
    public int countLogTaiXiuDetail(final String referent_id, final String betSide, final String money_type, final String nickName) throws SQLException {
        int res = 0;
        String condition = "";
        if (referent_id != null && !referent_id.equals("")) {
            condition = condition + " AND reference_id=" + referent_id;
        }
        if (betSide != null && !betSide.equals("")) {
            condition = condition + " AND bet_side=" + betSide;
        }
        if (nickName != null && !nickName.equals("")) {
            condition = condition + " AND user_name = '" + nickName + "'";
        }
        condition = condition + " AND money_type=" + money_type;
        final String sql = "SELECT count(*) as total FROM transaction_detail_tai_xiu where 1=1 " + condition;
        try (final Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_minigame");
             final PreparedStatement stmt = conn.prepareStatement(sql);
             final ResultSet rs = stmt.executeQuery()
        ) {
            if (rs.next()) {
                res = rs.getInt("total");
            }
        }
        return res;
    }

    @Override
    public List<TaiXiuResultResponse> listLogTaiXiuResult(
            final String referentId, final String moneyType, final String timeStart, final String timeEnd, final int page) throws SQLException {
        final ArrayList<TaiXiuResultResponse> result = new ArrayList<>();
        final int num_start = (page - 1) * 50;
        final String limit = " ORDER BY reference_id DESC LIMIT " + num_start + ", " + 50 + "";

        String query = "SELECT reference_id,result,dice1,dice2,dice3,total_tai,total_xiu,num_bet_tai,num_bet_xiu,total_prize,total_refund_tai,total_refund_xiu,total_revenue,money_type,timestamp from result_tai_xiu where 1=1 ";
        String condition = "";
        if (referentId != null && !referentId.equals("")) {
            condition = condition + " AND reference_id=" + referentId;
        }
        if (moneyType != null && !moneyType.equals("")) {
            condition = condition + " AND money_type=" + moneyType;
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            condition = condition + " AND timestamp BETWEEN '" + timeStart + "' AND '" + timeEnd + "'";
        }
        String sql = query + condition + limit;
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_minigame");
             final PreparedStatement stmt = conn.prepareStatement(sql)
        ) {
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    final TaiXiuResultResponse taixiu = new TaiXiuResultResponse();
                    taixiu.reference_id = rs.getLong("reference_id");
                    taixiu.result = rs.getInt("result");
                    taixiu.dice1 = rs.getInt("dice1");
                    taixiu.dice2 = rs.getInt("dice2");
                    taixiu.dice3 = rs.getInt("dice3");
                    taixiu.total_tai = rs.getLong("total_tai");
                    taixiu.total_xiu = rs.getLong("total_xiu");
                    taixiu.num_bet_tai = rs.getInt("num_bet_tai");
                    taixiu.num_bet_xiu = rs.getInt("num_bet_xiu");
                    taixiu.total_prize = rs.getLong("total_prize");
                    taixiu.total_refund_tai = rs.getLong("total_refund_tai");
                    taixiu.total_refund_xiu = rs.getLong("total_refund_xiu");
                    taixiu.total_revenue = rs.getLong("total_revenue");
                    taixiu.money_type = rs.getInt("money_type");
                    taixiu.timestamp = rs.getString("timestamp");
                    result.add(taixiu);
                }
            }
        }
        return result;
    }

    @Override
    public int countLogTaiXiuResult(final String referentId, final String moneyType, final String timeStart, final String timeEnd) throws SQLException {
        String query = "SELECT count(*) as total from result_tai_xiu where 1=1 ";
        String condition = "";
        if (referentId != null && !referentId.equals("")) {
            condition = condition + " AND reference_id=" + referentId;
        }
        if (moneyType != null && !moneyType.equals("")) {
            condition = condition + " AND money_type=" + moneyType;
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            condition = condition + " AND timestamp BETWEEN '" + timeStart + "' AND '" + timeEnd + "'";
        }

        int res;
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_minigame");
             final PreparedStatement stmt = conn.prepareStatement(query + condition);
             final ResultSet rs = stmt.executeQuery()
        ) {
            res = 0;
            if (rs.next()) {
                res = rs.getInt("total");
            }
        }
        return res;
    }
}
