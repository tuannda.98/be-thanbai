// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.vinplay.dal.dao.impl.UserDaoImpl;
import com.vinplay.dal.service.UserMissionService;
import com.vinplay.vbee.common.response.userMission.LogReceivedRewardObj;

import java.sql.SQLException;
import java.util.List;

public class UserMissionServiceImpl implements UserMissionService {
    @Override
    public List<LogReceivedRewardObj> getLogReceivedReward(final String nickName, final String gameName, final String moneyType, final String timeStart, final String timeEnd, final int page) {
        final UserDaoImpl dao = new UserDaoImpl();
        return dao.getLogReceivedReward(nickName, gameName, moneyType, timeStart, timeEnd, page);
    }

    @Override
    public boolean updateUserMission(final String nickName, final String missionName, final String moneyType, final int matchWin) throws SQLException {
        final UserDaoImpl dao = new UserDaoImpl();
        return dao.updateUserMission(nickName, missionName, moneyType, matchWin);
    }
}
