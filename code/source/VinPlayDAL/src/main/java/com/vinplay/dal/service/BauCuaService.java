// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service;

import com.vinplay.vbee.common.models.minigame.TopWin;
import com.vinplay.vbee.common.models.minigame.baucua.ResultBauCua;
import com.vinplay.vbee.common.models.minigame.baucua.ToiChonCa;
import com.vinplay.vbee.common.models.minigame.baucua.TransactionBauCua;
import com.vinplay.vbee.common.models.minigame.baucua.TransactionBauCuaDetail;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeoutException;

public interface BauCuaService {
    void saveTransactionBauCuaDetail(final TransactionBauCuaDetail p0) throws IOException, TimeoutException, InterruptedException;

    void saveTransactionBauCua(final TransactionBauCua p0) throws IOException, TimeoutException, InterruptedException;

    void saveResultBauCua(final ResultBauCua p0) throws IOException, TimeoutException, InterruptedException;

    List<TransactionBauCua> getLSGDBauCua(final String p0, final int p1, final byte p2);

    int countLSGDBauCua(final String p0, final byte p1);

    ResultBauCua getPhienBauCua(final long p0);

    List<TopWin> getTopBauCua(final byte p0, final String p1, final String p2);

    void calculteToiChonCa(final byte[] p0, final List<TransactionBauCua> p1) throws IOException, TimeoutException, InterruptedException;

    List<ResultBauCua> getLichSuPhien(final int p0, final byte p1);

    List<ToiChonCa> getTopToiChonCa(final String p0, final String p1);

    void updateAllTop();
}
