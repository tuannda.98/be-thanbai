// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.vinplay.dal.dao.impl.LogBauCuaDAOImpl;
import com.vinplay.dal.service.LogBauCuaService;
import com.vinplay.vbee.common.response.BauCuaReponseDetail;
import com.vinplay.vbee.common.response.BauCuaResponse;
import com.vinplay.vbee.common.response.BauCuaResultResponse;

import java.util.List;

public class LogBauCuaServiceImpl implements LogBauCuaService {
    @Override
    public List<BauCuaResponse> listLogBauCua(final String referent_id, final String nickName, final String room, final String timeStart, final String timeEnd, final String moneyType, final int page) {
        final LogBauCuaDAOImpl dao = new LogBauCuaDAOImpl();
        return dao.listLogBauCua(referent_id, nickName, room, timeStart, timeEnd, moneyType, page);
    }

    @Override
    public int countLogBauCua(final String referent_id, final String nickName, final String room, final String timeStart, final String timeEnd, final String moneyType) {
        final LogBauCuaDAOImpl dao = new LogBauCuaDAOImpl();
        return dao.countLogBauCua(referent_id, nickName, room, timeStart, timeEnd, moneyType);
    }

    @Override
    public List<BauCuaReponseDetail> getLogBauCuaDetail(final String referent_id, final int page) {
        final LogBauCuaDAOImpl dao = new LogBauCuaDAOImpl();
        return dao.getLogBauCuaDetail(referent_id, page);
    }

    @Override
    public int countLogBauCuaDetail(final String referent_id) {
        final LogBauCuaDAOImpl dao = new LogBauCuaDAOImpl();
        return dao.countLogBauCuaDetail(referent_id);
    }

    @Override
    public List<BauCuaResultResponse> listLogBauCauResult(final String referent_id, final String room, final String timeStart, final String timeEnd, final int page) {
        final LogBauCuaDAOImpl dao = new LogBauCuaDAOImpl();
        return dao.listLogBauCauResult(referent_id, room, timeStart, timeEnd, page);
    }

    @Override
    public long countLogBauCauResult(final String referent_id, final String room, final String timeStart, final String timeEnd) {
        final LogBauCuaDAOImpl dao = new LogBauCuaDAOImpl();
        return dao.countLogBauCauResult(referent_id, room, timeStart, timeEnd);
    }
}
