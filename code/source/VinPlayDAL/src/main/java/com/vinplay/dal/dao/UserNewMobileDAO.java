// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao;

import com.vinplay.vbee.common.response.UserNewMobileResponse;

import java.util.List;

public interface UserNewMobileDAO {
    List<UserNewMobileResponse> searchUserNewMobile(final String p0, final String p1, final String p2, final int p3);

    long countSearchUserNewMobile(final String p0, final String p1, final String p2);
}
