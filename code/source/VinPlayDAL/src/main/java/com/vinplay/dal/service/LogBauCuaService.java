// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service;

import com.vinplay.vbee.common.response.BauCuaReponseDetail;
import com.vinplay.vbee.common.response.BauCuaResponse;
import com.vinplay.vbee.common.response.BauCuaResultResponse;

import java.util.List;

public interface LogBauCuaService {
    List<BauCuaResponse> listLogBauCua(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5, final int p6);

    int countLogBauCua(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5);

    List<BauCuaReponseDetail> getLogBauCuaDetail(final String p0, final int p1);

    int countLogBauCuaDetail(final String p0);

    List<BauCuaResultResponse> listLogBauCauResult(final String p0, final String p1, final String p2, final String p3, final int p4);

    long countLogBauCauResult(final String p0, final String p1, final String p2, final String p3);
}
