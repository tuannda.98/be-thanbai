// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao;

import com.vinplay.dal.entities.caothap.LSGDCaoThap;
import com.vinplay.dal.entities.caothap.TopCaoThap;
import com.vinplay.dal.entities.caothap.VinhDanhCaoThap;

import java.sql.SQLException;
import java.util.List;

public interface CaoThapDAO {
    long[] getPotCaoThap(final String p0) throws SQLException;

    long[] getFundCaoThap(final String p0) throws SQLException;

    int countLichSuGiaoDich(final String p0, final int p1);

    List<LSGDCaoThap> getLichSuGiaoDich(final String p0, final int p1, final int p2);

    int countVinhDanh(final int p0);

    List<VinhDanhCaoThap> getBangVinhDanh(final int p0, final int p1);

    List<TopCaoThap> getTop(final String p0, final String p1);

    long getLastReferenceId();
}
