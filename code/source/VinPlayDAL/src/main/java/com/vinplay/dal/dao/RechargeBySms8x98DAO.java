// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao;

import com.vinplay.vbee.common.response.MoneyTotalRechargeByCardReponse;
import com.vinplay.vbee.common.response.RechargeBySmsResponse;

import java.util.List;

public interface RechargeBySms8x98DAO {
    List<MoneyTotalRechargeByCardReponse> moneyTotalRechargeBySms8x98(final String p0, final String p1, final String p2);

    List<RechargeBySmsResponse> exportDataRechargeBySms(final String p0, final String p1, final String p2, final String p3);
}
