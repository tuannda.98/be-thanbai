// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service;

import com.vinplay.dal.entities.report.ReportMoneySystemModel;
import com.vinplay.dal.entities.taixiu.ResultTaiXiu;
import com.vinplay.dal.entities.taixiu.TransactionTaiXiu;
import com.vinplay.dal.entities.taixiu.TransactionTaiXiuDetail;
import com.vinplay.dal.entities.taixiu.VinhDanhRLTLModel;
import com.vinplay.vbee.common.models.cache.ThanhDuTXModel;
import com.vinplay.vbee.common.models.minigame.TopWin;
import com.vinplay.vbee.common.models.minigame.taixiu.XepHangRLTLModel;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import java.util.concurrent.TimeoutException;

public interface OverUnderService {
    boolean saveTransactionTaiXiu(final long p0, final int p1, final String p2, final int p3, final long p4, final short p5, final long p6, final long p7) throws IOException, TimeoutException, InterruptedException;

    boolean saveTransactionTaiXiu(final List<TransactionTaiXiu> p0) throws IOException, TimeoutException, InterruptedException;

    boolean saveTransactionTaiXiuDetails(final List<TransactionTaiXiuDetail> p0) throws IOException, TimeoutException, InterruptedException;

    boolean saveTransactionTaiXiuDetail(final TransactionTaiXiuDetail p0) throws IOException, TimeoutException, InterruptedException;

    boolean updateTransactionTaiXiuDetail(final TransactionTaiXiuDetail p0) throws IOException, TimeoutException, InterruptedException;

    boolean saveResultTaiXiu(final long p0, final int p1, final int p2, final int p3, final int p4, final long p5, final long p6, final int p7, final int p8, final long p9, final long p10, final long p11, final long p12, final int p13) throws Exception;

    boolean saveResultTaiXiu(final ResultTaiXiu p0) throws Exception;

    String getLichSuPhien(final int p0, final int p1) throws SQLException;

    List<ResultTaiXiu> getListLichSuPhien(final int p0, final int p1) throws SQLException;

    ResultTaiXiu getKetQuaPhien(final long p0, final int p1) throws SQLException;

    List<TopWin> getTopWin(final int p0);

    List<TransactionTaiXiu> getLichSuGiaoDich(final String p0, final int p1, final int p2) throws SQLException;

    int countLichSuGiaoDich(final String p0, final int p1) throws SQLException;

    List<TransactionTaiXiuDetail> getChiTietPhienTX(final long p0, final int p1) throws SQLException;

    void calculateThanhDu(final long p0, final List<TransactionTaiXiu> p1, final int p2) throws IOException, TimeoutException, InterruptedException;

    List<ThanhDuTXModel> getTopThanhDuDaily(final String p0, final int p1) throws SQLException;

    List<ThanhDuTXModel> getTopThanhDuMonthly(final String p0, final int p1) throws SQLException, ParseException;

    long getPotTanLoc() throws SQLException;

    void logTanLoc(final String p0, final long p1) throws IOException, TimeoutException, InterruptedException;

    void logRutLoc(final String p0, final long p1, final int p2, final long p3) throws IOException, TimeoutException, InterruptedException;

    void updatePotTanLoc(final long p0) throws IOException, TimeoutException, InterruptedException;

    int updateLuotRutLoc(final String p0, final int p1) throws IOException, TimeoutException, InterruptedException;

    int getLuotRutLoc(final String p0) throws SQLException;

    List<XepHangRLTLModel> getXepHangTanLoc();

    List<VinhDanhRLTLModel> getVinhDanhTanLoc();

    long getSoTienTanLoc(final String p0);

    List<XepHangRLTLModel> getXepHangRutLoc();

    List<VinhDanhRLTLModel> getVinhDanhRutLoc();

    long getSoTienRutLoc(final String p0);

    boolean updatePot(final long p0, final String p1);

    boolean updateFund(final long p0, final String p1);

    void updateAllTop();

    ReportMoneySystemModel getReportTXToday();

    ReportMoneySystemModel getReportTX(final int p0) throws SQLException;
}
