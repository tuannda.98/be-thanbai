// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.vinplay.dal.dao.impl.RechargeByCardDAOImpl;
import com.vinplay.dal.service.RechargeByCardService;
import com.vinplay.vbee.common.response.MoneyTotalRechargeByCardReponse;
import com.vinplay.vbee.common.response.RechargeByCardReponse;

import java.util.List;

public class RechargeByCardServiceImpl implements RechargeByCardService {
    @Override
    public List<RechargeByCardReponse> searchRechargeByCard(final String nickName, final String provider, final String serial, final String pin, final String code, final String timeStart, final String timeEnd, final int page, final String transId) {
        final RechargeByCardDAOImpl dao = new RechargeByCardDAOImpl();
        return dao.searchRechargeByCard(nickName, provider, serial, pin, code, timeStart, timeEnd, page, transId);
    }

    @Override
    public int countSearchRechargeByCard(final String nickName, final String provider, final String serial, final String pin, final String code, final String timeStart, final String timeEnd, final String transId) {
        final RechargeByCardDAOImpl dao = new RechargeByCardDAOImpl();
        return dao.countSearchRechargeByCard(nickName, provider, serial, pin, code, timeStart, timeEnd, transId);
    }

    @Override
    public long moneyTotal(final String nickName, final String provider, final String serial, final String pin, final String code, final String timeStart, final String timeEnd, final String transId) {
        final RechargeByCardDAOImpl dao = new RechargeByCardDAOImpl();
        return dao.moneyTotal(nickName, provider, serial, pin, code, timeStart, timeEnd, transId);
    }

    @Override
    public List<MoneyTotalRechargeByCardReponse> moneyTotalRechargeByCard(final String nickName, final String provider, final String serial, final String pin, final String code, final String timeStart, final String timeEnd, final String transId) {
        final RechargeByCardDAOImpl dao = new RechargeByCardDAOImpl();
        return dao.moneyTotalRechargeByCard(nickName, provider, serial, pin, code, timeStart, timeEnd, transId);
    }

    @Override
    public List<MoneyTotalRechargeByCardReponse> doiSoatRechargeByCard(final int code, final String timeStart, final String timeEnd) {
        final RechargeByCardDAOImpl dao = new RechargeByCardDAOImpl();
        return dao.doiSoatRechargeByCard(code, timeStart, timeEnd);
    }

    @Override
    public List<RechargeByCardReponse> exportDataRechargeByCard(final String provider, final String timeStart, final String timeEnd, final String amount, final String code) {
        final RechargeByCardDAOImpl dao = new RechargeByCardDAOImpl();
        return dao.exportDataRechargeByCard(provider, timeStart, timeEnd, amount, code);
    }
}
