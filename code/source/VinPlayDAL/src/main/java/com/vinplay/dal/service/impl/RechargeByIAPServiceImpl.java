// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.vinplay.dal.dao.impl.RechargeByIAPDAOImpl;
import com.vinplay.dal.service.RechargeByIAPService;
import com.vinplay.vbee.common.response.RechardByIAPResponse;

import java.util.List;

public class RechargeByIAPServiceImpl implements RechargeByIAPService {
    @Override
    public List<RechardByIAPResponse> ListRechargeIAP(final String nickName, final String code, final String timeStart, final String timeEnd, final String amount, final String orderId, final int page) {
        final RechargeByIAPDAOImpl dao = new RechargeByIAPDAOImpl();
        return dao.ListRechargeIAP(nickName, code, timeStart, timeEnd, amount, orderId, page);
    }

    @Override
    public long totalMoney(final String nickName, final String code, final String timeStart, final String timeEnd, final String amount, final String orderId) {
        final RechargeByIAPDAOImpl dao = new RechargeByIAPDAOImpl();
        return dao.totalMoney(nickName, code, timeStart, timeEnd, amount, orderId);
    }

    @Override
    public long countListRechargeIAP(final String nickName, final String code, final String timeStart, final String timeEnd, final String amount, final String orderId) {
        final RechargeByIAPDAOImpl dao = new RechargeByIAPDAOImpl();
        return dao.countListRechargeIAP(nickName, code, timeStart, timeEnd, amount, orderId);
    }
}
