/*
 * Decompiled with CFR 0.150.
 *
 * Could not load the following classes:
 *  com.mongodb.BasicDBObject
 *  com.mongodb.Block
 *  com.mongodb.client.FindIterable
 *  com.mongodb.client.MongoDatabase
 *  org.bson.Document
 *  org.bson.conversions.Bson
 */
package com.vinplay.dal.dao.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import com.vinplay.dal.dao.VipPointEventDao;
import com.vinplay.dal.entities.agent.TopVippointResponse;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.pools.ConnectionPool;
import com.vinplay.vbee.common.response.LogVipPointEventResponse;
import com.vinplay.vbee.common.response.UserVipPointEventResponse;
import org.bson.Document;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class VipPointEventDaoImpl
        implements VipPointEventDao {
    @Override
    public List<LogVipPointEventResponse> listLogVipPointEvent(String nickName, String value, String type, String timeStart, String timeEnd, int page, String bot) {
        final ArrayList<LogVipPointEventResponse> results = new ArrayList<>();
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        HashMap<String, Object> conditions = new HashMap<>();
        FindIterable iterable = null;
        BasicDBObject obj = new BasicDBObject();
        BasicDBObject objsort = new BasicDBObject();
        int numStart = (page - 1) * 50;
        int numEnd = 50;
        objsort.put("_id", -1);
        if (nickName != null && !nickName.equals("")) {
            conditions.put("nick_name", nickName);
        }
        if (value != null && !value.equals("")) {
            conditions.put("value", Integer.parseInt(value));
        }
        if (type != null && !type.equals("")) {
            conditions.put("type", Integer.parseInt(type));
        }
        if (bot != null && !bot.equals("")) {
            conditions.put("is_bot", Integer.parseInt(bot));
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("time_log", obj);
        }
        iterable = db.getCollection("log_vippoint_event").find(new Document(conditions)).sort(objsort).skip(numStart).limit(50);
        iterable.forEach(new Block<Document>() {

            public void apply(Document document) {
                LogVipPointEventResponse trans = new LogVipPointEventResponse();
                trans.nickName = document.getString("nick_name");
                trans.value = document.getInteger("value");
                trans.type = document.getInteger("type");
                trans.timeLog = document.getString("time_log");
                trans.is_bot = document.getInteger("is_bot");
                results.add(trans);
            }
        });
        return results;
    }

    @Override
    public long countLogVipPointEvent(String nickName, String value, String type, String timeStart, String timeEnd, String bot) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        HashMap<String, Object> conditions = new HashMap<>();
        BasicDBObject obj = new BasicDBObject();
        if (nickName != null && !nickName.equals("")) {
            conditions.put("nick_name", nickName);
        }
        if (value != null && !value.equals("")) {
            conditions.put("value", Integer.parseInt(value));
        }
        if (type != null && !type.equals("")) {
            conditions.put("type", Integer.parseInt(type));
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("time_log", obj);
        }
        if (bot != null && !bot.equals("")) {
            conditions.put("is_bot", Integer.parseInt(bot));
        }
        return db.getCollection("log_vippoint_event").count(new Document(conditions));
    }

    @Override
    public long totalVipPointEvent(String nickName, String value, String type, String timeStart, String timeEnd, String bot) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        HashMap<String, Object> conditions = new HashMap<>();
        FindIterable iterable = null;
        BasicDBObject obj = new BasicDBObject();
        if (nickName != null && !nickName.equals("")) {
            conditions.put("nick_name", nickName);
        }
        if (value != null && !value.equals("")) {
            conditions.put("value", Integer.parseInt(value));
        }
        if (type != null && !type.equals("")) {
            conditions.put("type", Integer.parseInt(type));
        }
        if (bot != null && !bot.equals("")) {
            conditions.put("is_bot", Integer.parseInt(bot));
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("time_log", obj);
        }
        final ArrayList<LogVipPointEventResponse> results = new ArrayList();
        iterable = db.getCollection("log_vippoint_event").find(new Document(conditions));
        iterable.forEach(new Block<Document>() {

            public void apply(Document document) {
                LogVipPointEventResponse trans = new LogVipPointEventResponse();
                trans.value = document.getInteger("value");
                results.add(trans);
            }
        });
        long record = 0L;
        for (LogVipPointEventResponse log : results) {
            record += log.value;
        }
        return record;
    }

    @Override
    public List<UserVipPointEventResponse> listuserVipPoint(String nickName, String sort, String filed, String timeStart, String timeEnd, int page, String bot) throws SQLException {
        ArrayList<UserVipPointEventResponse> results = new ArrayList<>();
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname")) {
            int numStart = (page - 1) * 50;
            int numEnd = 50;
            StringBuilder order = new StringBuilder();
            String sql = "SELECT nick_name,vp_real,vp_event,vp_add,num_add,vp_sub,num_sub,place,place_max,update_time,is_bot from users_vp_event WHERE 1=1 ";
            int index = 1;
            if (nickName != null && !nickName.equals("")) {
                sql = sql + " AND nick_name=?";
            }
            String[] keys = filed.split(",");
            for (int i = 0; i < keys.length; ++i) {
                if (keys[i].equals("1")) {
                    if (i == keys.length - 1) {
                        if (sort.equals("1")) {
                            order.append("vp_real asc");
                            continue;
                        }
                        if (!sort.equals("2")) continue;
                        order.append("vp_real desc");
                        continue;
                    }
                    if (sort.equals("1")) {
                        order.append("vp_real asc, ");
                        continue;
                    }
                    if (!sort.equals("2")) continue;
                    order.append("vp_real desc, ");
                    continue;
                }
                if (keys[i].equals("2")) {
                    if (i == keys.length - 1) {
                        if (sort.equals("1")) {
                            order.append("vp_event asc");
                            continue;
                        }
                        if (!sort.equals("2")) continue;
                        order.append("vp_event desc");
                        continue;
                    }
                    if (sort.equals("1")) {
                        order.append("vp_event asc, ");
                        continue;
                    }
                    if (!sort.equals("2")) continue;
                    order.append("vp_event desc, ");
                    continue;
                }
                if (keys[i].equals("3")) {
                    if (i == keys.length - 1) {
                        if (sort.equals("1")) {
                            order.append("vp_add asc");
                            continue;
                        }
                        if (!sort.equals("2")) continue;
                        order.append("vp_add desc");
                        continue;
                    }
                    if (sort.equals("1")) {
                        order.append("vp_add asc ,");
                        continue;
                    }
                    if (!sort.equals("2")) continue;
                    order.append("vp_add desc ,");
                    continue;
                }
                if (keys[i].equals("4")) {
                    if (i == keys.length - 1) {
                        if (sort.equals("1")) {
                            order.append("num_add asc");
                            continue;
                        }
                        if (!sort.equals("2")) continue;
                        order.append("num_add desc");
                        continue;
                    }
                    if (sort.equals("1")) {
                        order.append("num_add asc , ");
                        continue;
                    }
                    if (!sort.equals("2")) continue;
                    order.append("num_add desc , ");
                    continue;
                }
                if (keys[i].equals("5")) {
                    if (i == keys.length - 1) {
                        if (sort.equals("1")) {
                            order.append("vp_sub asc");
                            continue;
                        }
                        if (!sort.equals("2")) continue;
                        order.append("vp_sub desc");
                        continue;
                    }
                    if (sort.equals("1")) {
                        order.append("vp_sub asc , ");
                        continue;
                    }
                    if (!sort.equals("2")) continue;
                    order.append("vp_sub desc , ");
                    continue;
                }
                if (keys[i].equals("6")) {
                    if (i == keys.length - 1) {
                        if (sort.equals("1")) {
                            order.append("num_sub asc");
                            continue;
                        }
                        if (!sort.equals("2")) continue;
                        order.append("num_sub desc");
                        continue;
                    }
                    if (sort.equals("1")) {
                        order.append("num_sub asc , ");
                        continue;
                    }
                    if (!sort.equals("2")) continue;
                    order.append("num_sub desc , ");
                    continue;
                }
                if (keys[i].equals("7")) {
                    if (i == keys.length - 1) {
                        if (sort.equals("1")) {
                            order.append("place asc");
                            continue;
                        }
                        if (!sort.equals("2")) continue;
                        order.append("place desc");
                        continue;
                    }
                    if (sort.equals("1")) {
                        order.append("place asc ,");
                        continue;
                    }
                    if (!sort.equals("2")) continue;
                    order.append("place desc , ");
                    continue;
                }
                if (keys[i].equals("8")) {
                    if (i == keys.length - 1) {
                        if (sort.equals("1")) {
                            order.append("place_max asc");
                            continue;
                        }
                        if (!sort.equals("2")) continue;
                        order.append("place_max desc");
                        continue;
                    }
                    if (sort.equals("1")) {
                        order.append("place_max asc , ");
                        continue;
                    }
                    if (!sort.equals("2")) continue;
                    order.append("place_max desc , ");
                    continue;
                }
                if (!keys[i].equals("9")) continue;
                if (i == keys.length - 1) {
                    if (sort.equals("1")) {
                        order.append("is_bot asc");
                        continue;
                    }
                    if (!sort.equals("2")) continue;
                    order.append("is_bot desc");
                    continue;
                }
                if (sort.equals("1")) {
                    order.append("is_bot asc , ");
                    continue;
                }
                if (!sort.equals("2")) continue;
                order.append("is_bot desc , ");
            }
            if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
                sql = sql + " AND update_time BETWEEN ? AND ? ";
            }
            if (bot != null && !bot.equals("")) {
                sql = sql + " AND is_bot=?";
            }
            String limit = "";
            if (page > 0) {
                limit = " limit " + numStart + " , " + 50;
            }
            sql = sort != null && !sort.equals("") && filed != null && !filed.equals("") ? sql + " order by " + order + limit : sql + limit;
            try (PreparedStatement stm = conn.prepareStatement(sql)) {
                if (nickName != null && !nickName.equals("")) {
                    stm.setString(index, nickName);
                    ++index;
                }
                if (bot != null && !bot.equals("")) {
                    stm.setInt(index, Integer.parseInt(bot));
                    ++index;
                }
                if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
                    stm.setString(index, timeStart);
                    stm.setString(index + 1, timeEnd);
                    ++index;
                }
                try (ResultSet rs = stm.executeQuery()) {
                    while (rs.next()) {
                        UserVipPointEventResponse trans = new UserVipPointEventResponse();
                        trans.nickName = rs.getString("nick_name");
                        trans.vp_real = rs.getInt("vp_real");
                        trans.vp_event = rs.getInt("vp_event");
                        trans.vp_add = rs.getInt("vp_add");
                        trans.num_add = rs.getInt("num_add");
                        trans.vp_sub = rs.getInt("vp_sub");
                        trans.num_sub = rs.getInt("num_sub");
                        trans.place = rs.getInt("place");
                        trans.placemax = rs.getInt("place_max");
                        trans.updateTime = rs.getString("update_time");
                        trans.is_bot = rs.getBoolean("is_bot");
                        results.add(trans);
                    }
                }
            }
        }
        return results;
    }

    @Override
    public long countUserVipPoint(String nickName, String sort, String filed, String timeStart, String timeEnd, String bot) throws SQLException {
        long total = 0L;
        String sql = "SELECT Count(*) cnt from users_vp_event WHERE 1=1 ";
        int index = 1;
        if (nickName != null && !nickName.equals("")) {
            sql = sql + " AND nick_name=?";
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            sql = sql + " AND update_time BETWEEN ? AND ? ";
        }
        String[] keys = filed.split(",");
        StringBuilder order = new StringBuilder();
        for (int i = 0; i < keys.length; ++i) {
            if (keys[i].equals("1")) {
                if (i == keys.length - 1) {
                    if (sort.equals("1")) {
                        order.append("vp_real asc");
                        continue;
                    }
                    if (!sort.equals("2")) continue;
                    order.append("vp_real desc");
                    continue;
                }
                if (sort.equals("1")) {
                    order.append("vp_real asc, ");
                    continue;
                }
                if (!sort.equals("2")) continue;
                order.append("vp_real desc, ");
                continue;
            }
            if (keys[i].equals("2")) {
                if (i == keys.length - 1) {
                    if (sort.equals("1")) {
                        order.append("vp_event asc");
                        continue;
                    }
                    if (!sort.equals("2")) continue;
                    order.append("vp_event desc");
                    continue;
                }
                if (sort.equals("1")) {
                    order.append("vp_event asc, ");
                    continue;
                }
                if (!sort.equals("2")) continue;
                order.append("vp_event desc, ");
                continue;
            }
            if (keys[i].equals("3")) {
                if (i == keys.length - 1) {
                    if (sort.equals("1")) {
                        order.append("vp_add asc");
                        continue;
                    }
                    if (!sort.equals("2")) continue;
                    order.append("vp_add desc");
                    continue;
                }
                if (sort.equals("1")) {
                    order.append("vp_add asc ,");
                    continue;
                }
                if (!sort.equals("2")) continue;
                order.append("vp_add desc ,");
                continue;
            }
            if (keys[i].equals("4")) {
                if (i == keys.length - 1) {
                    if (sort.equals("1")) {
                        order.append("num_add asc");
                        continue;
                    }
                    if (!sort.equals("2")) continue;
                    order.append("num_add desc");
                    continue;
                }
                if (sort.equals("1")) {
                    order.append("num_add asc , ");
                    continue;
                }
                if (!sort.equals("2")) continue;
                order.append("num_add desc , ");
                continue;
            }
            if (keys[i].equals("5")) {
                if (i == keys.length - 1) {
                    if (sort.equals("1")) {
                        order.append("vp_sub asc");
                        continue;
                    }
                    if (!sort.equals("2")) continue;
                    order.append("vp_sub desc");
                    continue;
                }
                if (sort.equals("1")) {
                    order.append("vp_sub asc , ");
                    continue;
                }
                if (!sort.equals("2")) continue;
                order.append("vp_sub desc , ");
                continue;
            }
            if (keys[i].equals("6")) {
                if (i == keys.length - 1) {
                    if (sort.equals("1")) {
                        order.append("num_sub asc");
                        continue;
                    }
                    if (!sort.equals("2")) continue;
                    order.append("num_sub desc");
                    continue;
                }
                if (sort.equals("1")) {
                    order.append("num_sub asc , ");
                    continue;
                }
                if (!sort.equals("2")) continue;
                order.append("num_sub desc , ");
                continue;
            }
            if (keys[i].equals("7")) {
                if (i == keys.length - 1) {
                    if (sort.equals("1")) {
                        order.append("place asc");
                        continue;
                    }
                    if (!sort.equals("2")) continue;
                    order.append("place desc");
                    continue;
                }
                if (sort.equals("1")) {
                    order.append("place asc ,");
                    continue;
                }
                if (!sort.equals("2")) continue;
                order.append("place desc , ");
                continue;
            }
            if (keys[i].equals("8")) {
                if (i == keys.length - 1) {
                    if (sort.equals("1")) {
                        order.append("place_max asc");
                        continue;
                    }
                    if (!sort.equals("2")) continue;
                    order.append("place_max desc");
                    continue;
                }
                if (sort.equals("1")) {
                    order.append("place_max asc , ");
                    continue;
                }
                if (!sort.equals("2")) continue;
                order.append("place_max desc , ");
                continue;
            }
            if (!keys[i].equals("9")) continue;
            if (i == keys.length - 1) {
                if (sort.equals("1")) {
                    order.append("is_bot asc");
                    continue;
                }
                if (!sort.equals("2")) continue;
                order.append("is_bot desc");
                continue;
            }
            if (sort.equals("1")) {
                order.append("is_bot asc , ");
                continue;
            }
            if (!sort.equals("2")) continue;
            order.append("is_bot desc , ");
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            sql = sql + " AND update_time BETWEEN ? AND ? ";
        }
        if (bot != null && !bot.equals("")) {
            sql = sql + " AND is_bot=?";
        }
        if (sort != null && !sort.equals("") && filed != null && !filed.equals("")) {
            sql = sql + " order by " + order;
        }
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement(sql);) {
            if (nickName != null && !nickName.equals("")) {
                stm.setString(index, nickName);
                ++index;
            }
            if (bot != null && !bot.equals("")) {
                stm.setInt(index, Integer.parseInt(bot));
                ++index;
            }
            if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
                stm.setString(index, timeStart);
                stm.setString(index + 1, timeEnd);
                ++index;
            }
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    total = rs.getLong("cnt");
                }
            }
        }
        return total;
    }

    @Override
    public List<TopVippointResponse> GetTopVippointAgency(int skip, int pageSize, String client) throws SQLException {
        ArrayList<TopVippointResponse> results = new ArrayList<>();
        String sql = "SELECT nick_name,mobile,vin_total,vip_point,dai_ly from users WHERE (dai_ly = 1 and client = ?) order by vip_point desc limit " + skip + "," + pageSize;
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement(sql);) {
            stm.setString(1, client);
            try (ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    TopVippointResponse top = new TopVippointResponse();
                    top.setDai_ly(rs.getInt("dai_ly"));
                    top.setMobile(rs.getString("mobile"));
                    top.setMoney(rs.getLong("vin_total"));
                    top.setNick_name(rs.getString("nick_name"));
                    top.setVip_point(rs.getInt("vip_point"));
                    results.add(top);
                }
            }
        }
        return results;
    }

    @Override
    public List<TopVippointResponse> GetTopVippointAgencyByNN(int skip, int pageSize, List<String> nicknames, String client) throws SQLException {
        ArrayList<TopVippointResponse> results = new ArrayList<>();
        String sql = "SELECT nick_name,mobile,vin_total,vip_point,dai_ly from users WHERE (dai_ly = 2 and nick_name in ? and client = ?) order by vip_point desc limit " + skip + "," + pageSize;
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement(sql);) {
            Array array = conn.createArrayOf("VARCHAR", nicknames.toArray());
            stm.setArray(1, array);
            stm.setString(2, client);
            try (ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    TopVippointResponse top = new TopVippointResponse();
                    top.setDai_ly(rs.getInt("dai_ly"));
                    top.setMobile(rs.getString("mobile"));
                    top.setMoney(rs.getLong("vin_total"));
                    top.setNick_name(rs.getString("nick_name"));
                    top.setVip_point(rs.getInt("vip_point"));
                    results.add(top);
                }
            }
        }
        return results;
    }

    @Override
    public List<String> GetAgentByParent(int parentId) throws SQLException {
        ArrayList<String> results = new ArrayList<>();
        String sql = "SELECT nickname from useragent WHERE parent_id = ?";
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_admin");
             PreparedStatement stm = conn.prepareStatement(sql);) {
            stm.setInt(1, parentId);
            try (ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    results.add(rs.getString("nickname"));
                }
            }
        }
        return results;
    }
}
