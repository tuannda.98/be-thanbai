// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service;

import com.vinplay.vbee.common.response.PokegoResponse;

import java.sql.SQLException;
import java.util.List;

public interface LogPoKeGoService {
    List<PokegoResponse> listLogPokego(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5, final int p6);

    long countLogPokego(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5);
}
