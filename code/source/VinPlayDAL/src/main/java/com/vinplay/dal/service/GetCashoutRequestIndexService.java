package com.vinplay.dal.service;

import org.json.JSONObject;

import java.sql.SQLException;

public interface GetCashoutRequestIndexService {
    JSONObject getByDateWithClientPlatform(final String timeStart, final String timeEnd) throws SQLException;
}
