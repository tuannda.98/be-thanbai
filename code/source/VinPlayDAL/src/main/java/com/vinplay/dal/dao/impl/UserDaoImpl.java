// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import com.vinplay.dal.dao.UserDao;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.models.vippoint.UserVPEventModel;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.pools.ConnectionPool;
import com.vinplay.vbee.common.response.userMission.LogReceivedRewardObj;
import com.vinplay.vbee.common.utils.DateTimeUtils;
import com.vinplay.vbee.common.utils.UserUtil;
import org.bson.Document;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class UserDaoImpl implements UserDao {
    @Override
    public UserModel getUserByNickName(final String nickname) throws SQLException {
        UserModel user = null;
        try (final Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             final PreparedStatement stm = conn.prepareStatement("SELECT * FROM users WHERE nick_name=?");) {
            stm.setString(1, nickname);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    user = UserUtil.parseResultSetToUserModel(rs);
                }
            }
        }
        return user;
    }

    @Override
    public boolean updateRechargeMoney(final String nickname, final long money) throws SQLException {
        final boolean res = false;
        try (final Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             final PreparedStatement stm = conn.prepareStatement("UPDATE users SET recharge_money=? WHERE nick_name=?");) {
            stm.setLong(1, money);
            stm.setString(2, nickname);
            if (stm.executeUpdate() == 1) {
                return true;
            }
        }
        return false;
    }

    @Override
    public UserVPEventModel getUserVPByNickName(final String nickname) throws SQLException {
        UserVPEventModel user = new UserVPEventModel();
        try (final Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             final PreparedStatement stm = conn.prepareStatement("SELECT * FROM users_vp_event WHERE nick_name=?");) {
            stm.setString(1, nickname);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    user = UserUtil.parseResultSetToUserVPEventModel(rs);
                }
            }
        }
        return user;
    }

    @Override
    public boolean updateUserMission(final String nickName, final String missionName, final String moneyType, final int matchWin) throws SQLException {
        final boolean success = false;
        String tableName = "";
        tableName = (moneyType.equals("vin") ? "user_mission_vin" : "user_mission_xu");
        final String sql = " UPDATE " + tableName + " SET match_win = ?,      update_time = ?  WHERE nick_name = ?    AND mission_name = ? ";
        try (final Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             final PreparedStatement stm = conn.prepareStatement(sql);) {
            stm.setInt(1, matchWin);
            stm.setString(2, DateTimeUtils.getCurrentTime());
            stm.setString(3, nickName);
            stm.setString(4, missionName);
            stm.executeUpdate();
        }
        return false;
    }

    @Override
    public List<LogReceivedRewardObj> getLogReceivedReward(final String nickName, final String gameName, final String moneyType, final String timeStart, final String timeEnd, final int page) {
        final ArrayList<LogReceivedRewardObj> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        final int numStart = (page - 1) * 50;
        final int numEnd = 50;
        objsort.put("_id", -1);
        if (nickName != null && !nickName.equals("")) {
            conditions.put("nick_name", nickName);
        }
        if (gameName != null && !gameName.equals("")) {
            conditions.put("game_name", gameName);
        }
        if (moneyType != null && !moneyType.equals("")) {
            conditions.put("money_type", moneyType);
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("time_log", obj);
        }
        final FindIterable iterable = db.getCollection("log_received_reward_mission").find(new Document(conditions)).sort(objsort).skip(numStart).limit(50);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final LogReceivedRewardObj obj = new LogReceivedRewardObj(document.getInteger("user_id", 0), document.getString("nick_name"), document.getString("game_name"), document.getInteger("level_received_reward", 0), document.getLong("money_bonus"), document.getLong("money_user"), document.getString("money_type"), document.getString("time_log"));
                results.add(obj);
            }
        });
        return results;
    }
}
