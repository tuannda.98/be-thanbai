// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.vinplay.dal.dao.impl.ListMoneyTranferDAOImpl;
import com.vinplay.dal.service.ListMoneyTranferService;
import com.vinplay.vbee.common.response.TranferMoneyResponse;

import java.util.List;

public class ListMoneyTranferServiceImpl implements ListMoneyTranferService {
    @Override
    public List<TranferMoneyResponse> listMoneyTranfer(final String nickName, final int isFreezeMoney, final int page, final int timeSearch) {
        final ListMoneyTranferDAOImpl dao = new ListMoneyTranferDAOImpl();
        return dao.listMoneyTranfer(nickName, isFreezeMoney, page, timeSearch);
    }

    @Override
    public TranferMoneyResponse getMoneyTranferByTransNo(final String transNo) {
        final ListMoneyTranferDAOImpl dao = new ListMoneyTranferDAOImpl();
        return dao.getMoneyTranferByTransNo(transNo);
    }

    @Override
    public int countTotalRecord(final String nickName, final int isFreezeMoney, final int page, final int timeSearch) {
        final ListMoneyTranferDAOImpl dao = new ListMoneyTranferDAOImpl();
        return dao.countTotalRecord(nickName, isFreezeMoney, page, timeSearch);
    }
}
