// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.entities.taixiu;

import java.io.Serializable;

public class ResultTaiXiu implements Serializable {
    private static final long serialVersionUID = 1L;
    public long referenceId;
    public int result;
    public int dice1;
    public int dice2;
    public int dice3;
    public long totalTai;
    public long totalXiu;
    public int numBetTai;
    public int numBetXiu;
    public long totalPrize;
    public long totalRefundTai;
    public long totalRefundXiu;
    public long totalRevenue;
    public int moneyType;
    public String timestamp;

    public ResultTaiXiu() {
        this.totalTai = 0L;
        this.totalXiu = 0L;
        this.numBetTai = 0;
        this.numBetXiu = 0;
        this.totalPrize = 0L;
        this.totalRefundTai = 0L;
        this.totalRefundXiu = 0L;
        this.totalRevenue = 0L;
    }

    public ResultTaiXiu(final long referenceId, final int result, final int dice1, final int dice2, final int dice3, final int moneyType) {
        this.totalTai = 0L;
        this.totalXiu = 0L;
        this.numBetTai = 0;
        this.numBetXiu = 0;
        this.totalPrize = 0L;
        this.totalRefundTai = 0L;
        this.totalRefundXiu = 0L;
        this.totalRevenue = 0L;
        this.referenceId = referenceId;
        this.result = result;
        this.dice1 = dice1;
        this.dice2 = dice2;
        this.dice3 = dice3;
        this.moneyType = moneyType;
    }

    public ResultTaiXiu(final long referenceId, final int result, final int dice1, final int dice2, final int dice3, final long totalTai, final long totalXiu, final int numBetTai, final int numBetXiu, final long totalPrize, final long totalRefundTai, final long totalRefundXiu, final long totalRevenue, final int moneyType, final String timestamp) {
        this.totalTai = 0L;
        this.totalXiu = 0L;
        this.numBetTai = 0;
        this.numBetXiu = 0;
        this.totalPrize = 0L;
        this.totalRefundTai = 0L;
        this.totalRefundXiu = 0L;
        this.totalRevenue = 0L;
        this.referenceId = referenceId;
        this.result = result;
        this.dice1 = dice1;
        this.dice2 = dice2;
        this.dice3 = dice3;
        this.totalTai = totalTai;
        this.totalXiu = totalXiu;
        this.numBetTai = numBetTai;
        this.numBetXiu = numBetXiu;
        this.totalPrize = totalPrize;
        this.totalRefundTai = totalRefundTai;
        this.totalRefundXiu = totalRefundXiu;
        this.totalRevenue = totalRevenue;
        this.moneyType = moneyType;
        this.timestamp = timestamp;
    }
}
