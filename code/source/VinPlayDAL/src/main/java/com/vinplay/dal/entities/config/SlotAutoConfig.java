package com.vinplay.dal.entities.config;

public class SlotAutoConfig {

    IncreasePerSecond slot100;
    IncreasePerSecond slot1000;
    IncreasePerSecond slot10000;
    IncreasePerSecond slot50000;
    IncreasePerSecond slot100000;
    IncreasePerSecond slot500000;
    Integer minMinuteToNextBotWinJackpot;
    Integer maxMinuteToNextBotWinJackpot;

    public IncreasePerSecond getSlot100() {
        return slot100;
    }

    public void setSlot100(IncreasePerSecond slot100) {
        this.slot100 = slot100;
    }

    public IncreasePerSecond getSlot1000() {
        return slot1000;
    }

    public void setSlot1000(IncreasePerSecond slot1000) {
        this.slot1000 = slot1000;
    }

    public IncreasePerSecond getSlot10000() {
        return slot10000;
    }

    public void setSlot10000(IncreasePerSecond slot10000) {
        this.slot10000 = slot10000;
    }

    public IncreasePerSecond getSlot50000() { return slot50000; }

    public void setSlot50000(IncreasePerSecond slot50000) { this.slot50000 = slot50000; }

    public IncreasePerSecond getSlot100000() { return slot100000; }

    public void setSlot100000(IncreasePerSecond slot100000) { this.slot100000 = slot100000; }

    public IncreasePerSecond getSlot500000() { return slot500000; }

    public void setSlot500000(IncreasePerSecond slot500000) { this.slot500000 = slot500000; }

    public Integer getMinMinuteToNextBotWinJackpot() {
        return minMinuteToNextBotWinJackpot;
    }

    public void setMinMinuteToNextBotWinJackpot(Integer minMinuteToNextBotWinJackpot) {
        this.minMinuteToNextBotWinJackpot = minMinuteToNextBotWinJackpot;
    }

    public Integer getMaxMinuteToNextBotWinJackpot() {
        return maxMinuteToNextBotWinJackpot;
    }

    public void setMaxMinuteToNextBotWinJackpot(Integer maxMinuteToNextBotWinJackpot) {
        this.maxMinuteToNextBotWinJackpot = maxMinuteToNextBotWinJackpot;
    }

    public static class IncreasePerSecond {
        Integer minIncreasePerSecond;
        Integer maxIncreasePerSecond;

        public Integer getMinIncreasePerSecond() {
            return minIncreasePerSecond;
        }

        public void setMinIncreasePerSecond(Integer minIncreasePerSecond) {
            this.minIncreasePerSecond = minIncreasePerSecond;
        }

        public Integer getMaxIncreasePerSecond() {
            return maxIncreasePerSecond;
        }

        public void setMaxIncreasePerSecond(Integer maxIncreasePerSecond) {
            this.maxIncreasePerSecond = maxIncreasePerSecond;
        }
    }
}

