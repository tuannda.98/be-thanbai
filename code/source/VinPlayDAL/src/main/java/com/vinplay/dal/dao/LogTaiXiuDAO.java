// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao;

import com.vinplay.vbee.common.response.TaiXiuDetailReponse;
import com.vinplay.vbee.common.response.TaiXiuResponse;
import com.vinplay.vbee.common.response.TaiXiuResultResponse;

import java.sql.SQLException;
import java.util.List;

public interface LogTaiXiuDAO {
    List<TaiXiuResponse> listLogTaiXiu(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5, final int p6) throws SQLException;

    int countLogTaiXiu(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5) throws SQLException;

    List<TaiXiuDetailReponse> getLogTaiXiuDetail(final String p0, final String p1, final String p2, final String p3, final int p4) throws SQLException;

    int countLogTaiXiuDetail(final String p0, final String p1, final String p2, final String p3) throws SQLException;

    List<TaiXiuResultResponse> listLogTaiXiuResult(final String p0, final String p1, final String p2, final String p3, final int p4) throws SQLException;

    int countLogTaiXiuResult(final String p0, final String p1, final String p2, final String p3) throws SQLException;
}
