// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service;

import com.vinplay.dal.entities.caothap.LSGDCaoThap;
import com.vinplay.dal.entities.caothap.TopCaoThap;
import com.vinplay.dal.entities.caothap.VinhDanhCaoThap;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.TimeoutException;

public interface CaoThapService {
    long[] getPotCaoThap() throws SQLException;

    long[] getFundCaoThap() throws SQLException;

    void updatePotCaoThap(final String p0, final long p1) throws IOException, TimeoutException, InterruptedException;

    void updateFundCaoThap(final String p0, final long p1) throws IOException, TimeoutException, InterruptedException;

    void logCaoThap(final long p0, final String p1, final long p2, final short p3, final long p4, final String p5, final long p6, final long p7, final int p8, final short p9, final int p10) throws IOException, TimeoutException, InterruptedException;

    void logCaoThapWin(final long p0, final String p1, final long p2, final short p3, final long p4, final String p5, final int p6) throws IOException, TimeoutException, InterruptedException;

    int countLichSuGiaoDich(final String p0, final int p1);

    List<LSGDCaoThap> getLichSuGiaoDich(final String p0, final int p1, final int p2);

    int countVinhDanh(final int p0);

    List<VinhDanhCaoThap> getBangVinhDanh(final int p0, final int p1);

    long getLastReferenceId();

    List<TopCaoThap> geTopCaoThap(final String p0, final String p1);

    void insertBotEvent(final String p0, final long p1, final long p2, final String p3);
}
