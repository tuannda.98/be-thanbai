// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.vinplay.dal.dao.impl.CashOutByTopUpDAOImpl;
import com.vinplay.dal.service.CashOutByTopUpService;
import com.vinplay.vbee.common.response.CashOutByTopUpResponse;
import com.vinplay.vbee.common.response.MoneyTotalRechargeByCardReponse;

import java.util.List;

public class CashOutByTopUpServiceImpl implements CashOutByTopUpService {
    @Override
    public List<CashOutByTopUpResponse> searchCashOutByTopUp(final String nickName, final String target, final String status, final String code, final String timeStart, final String timeEnd, final int page, final String transId, final String partner, final String type) {
        final CashOutByTopUpDAOImpl dao = new CashOutByTopUpDAOImpl();
        return dao.searchCashOutByTopUp(nickName, target, status, code, timeStart, timeEnd, page, transId, partner, type);
    }

    @Override
    public int countSearchCashOutByTopUp(final String nickName, final String target, final String status, final String code, final String timeStart, final String timeEnd, final String transId, final String partner, final String type) {
        final CashOutByTopUpDAOImpl dao = new CashOutByTopUpDAOImpl();
        return dao.countSearchCashOutByTopUp(nickName, target, status, code, timeStart, timeEnd, transId, partner, type);
    }

    @Override
    public long moneyTotal(final String nickName, final String target, final String status, final String code, final String timeStart, final String timeEnd, final String transId, final String partner, final String type) {
        final CashOutByTopUpDAOImpl dao = new CashOutByTopUpDAOImpl();
        return dao.moneyTotal(nickName, target, status, code, timeStart, timeEnd, transId, partner, type);
    }

    @Override
    public List<MoneyTotalRechargeByCardReponse> moneyTotalCashOutByTopup(final String timeStart, final String timeEnd, final String partner, final String code, final String type) {
        final CashOutByTopUpDAOImpl dao = new CashOutByTopUpDAOImpl();
        return dao.moneyTotalCashOutByTopup(timeStart, timeEnd, partner, code, type);
    }

    @Override
    public List<CashOutByTopUpResponse> exportDataCashOutByTopup(final String provider, final String code, final String timeStart, final String timeEnd, final String partner, final String amount, final String type) {
        final CashOutByTopUpDAOImpl dao = new CashOutByTopUpDAOImpl();
        return dao.exportDataCashOutByTopup(provider, code, timeStart, timeEnd, partner, amount, type);
    }
}
