// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service;

import com.vinplay.vbee.common.models.AgentModel;
import com.vinplay.vbee.common.response.AgentResponse;
import com.vinplay.vbee.common.response.LogAgentTranferMoneyResponse;
import com.vinplay.vbee.common.response.TranferAgentResponse;

import java.sql.SQLException;
import java.util.List;

public interface AgentService {
    List<AgentResponse> listAgent() throws SQLException;

    List<AgentResponse> listAgentByClient(final String p0) throws SQLException;

    List<LogAgentTranferMoneyResponse> searchAgentTranferMoney(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5, final int p6);

    List<AgentResponse> listUserAgent(final String p0) throws SQLException;

    List<TranferAgentResponse> searchAgentTranfer(final String p0, final String p1, final String p2, final String p3, final double p4, final double p5, final double p6, final long p7) throws SQLException;

    long countsearchAgentTranferMoney(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5);

    long totalMoneyVinReceiveFromAgent(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5);

    long totalMoneyVinSendFromAgent(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5);

    long totalMoneyVinFeeFromAgent(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5);

    List<LogAgentTranferMoneyResponse> searchAgentTranferMoneyVinSale(final String p0, final String p1, final String p2, final String p3, final int p4, final int p5);

    long countSearchAgentTranferMoneyVinSale(final String p0, final String p1, final String p2, final String p3);

    long totalMoneyVinReceiveFromAgentByStatus(final String p0, final String p1, final String p2, final String p3);

    long totalMoneyVinSendFromAgentByStatus(final String p0, final String p1, final String p2, final String p3);

    boolean updateTopDsFromAgent(final String p0, final String p1, final String p2, final String p3);

    List<TranferAgentResponse> searchAgentTranferAdmin(final String p0, final String p1, final String p2, final String p3, final double p4, final double p5, final double p6, final long p7) throws SQLException;

    List<LogAgentTranferMoneyResponse> searchAgentTranferMoney(final String p0, final String p1, final String p2, final String p3, final String p4, final int p5);

    long countsearchAgentTranferMoney(final String p0, final String p1, final String p2, final String p3, final String p4);

    long totalMoneyVinReceiveFromAgent(final String p0, final String p1, final String p2, final String p3, final String p4);

    long totalMoneyVinSendFromAgent(final String p0, final String p1, final String p2, final String p3, final String p4);

    long totalMoneyVinFeeFromAgent(final String p0, final String p1, final String p2, final String p3, final String p4);

    List<AgentResponse> listUserAgentActive(final String p0) throws SQLException;

    List<AgentResponse> listUserAgentLevel1Active() throws SQLException;

    List<LogAgentTranferMoneyResponse> searchAgentTongTranferMoney(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5, final int p6);

    long countsearchAgentTongTranferMoney(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5);

    long totalMoneyVinReceiveFromAgentTong(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5);

    long totalMoneyVinSendFromAgentTong(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5);

    long totalMoneyVinFeeFromAgentTong(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5);

    List<AgentModel> getListPercentBonusVincard(final String p0) throws SQLException;

    int registerPercentBonusVincard(final String p0, final int p1) throws SQLException;
}
