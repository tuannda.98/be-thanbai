// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import com.vinplay.dal.dao.LogBauCuaDAO;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.response.BauCuaReponseDetail;
import com.vinplay.vbee.common.response.BauCuaResponse;
import com.vinplay.vbee.common.response.BauCuaResultResponse;
import org.bson.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LogBauCuaDAOImpl implements LogBauCuaDAO {
    @Override
    public List<BauCuaResponse> listLogBauCua(final String referent_id, final String nickName, final String room, final String timeStart, final String timeEnd, final String moneyType, final int page) {
        final ArrayList<BauCuaResponse> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        FindIterable iterable = null;
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        final int num_start = (page - 1) * 50;
        final int num_end = 50;
        objsort.put("_id", -1);
        if (nickName != null && !nickName.equals("")) {
            final String pattern = ".*" + nickName + ".*";
            conditions.put("user_name", new BasicDBObject().append("$regex", pattern).append("$options", "i"));
        }
        if (referent_id != null && !referent_id.equals("")) {
            conditions.put("reference_id", Long.parseLong(referent_id));
        }
        if (room != null && !room.equals("")) {
            conditions.put("room", Integer.parseInt(room));
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("time_log", obj);
        }
        if (moneyType != null && !moneyType.equals("")) {
            conditions.put("money_type", Integer.parseInt(moneyType));
        }
        iterable = db.getCollection("bau_cua_transaction").find(new Document(conditions)).sort(objsort).skip(num_start).limit(50);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final BauCuaResponse baucua = new BauCuaResponse();
                baucua.referent_id = document.getLong("reference_id");
                baucua.room = document.getInteger("room");
                baucua.user_name = document.getString("user_name");
                baucua.money_type = document.getInteger("money_type");
                baucua.dices = document.getString("dices");
                baucua.bet_value = document.getString("bet_value");
                baucua.bet_bau = document.getLong("bet_bau");
                baucua.bet_ca = document.getLong("bet_ca");
                baucua.bet_ga = document.getLong("bet_ga");
                baucua.bet_tom = document.getLong("bet_tom");
                baucua.bet_huou = document.getLong("bet_huou");
                baucua.bet_cua = document.getLong("bet_cua");
                baucua.prize = document.getString("prize");
                baucua.prize_bau = document.getLong("prize_bau");
                baucua.prize_cua = document.getLong("prize_cua");
                baucua.prize_ca = document.getLong("prize_ca");
                baucua.prize_tom = document.getLong("prize_tom");
                baucua.prize_huou = document.getLong("prize_huou");
                baucua.prize_ga = document.getLong("prize_ga");
                baucua.money_exchange = document.getLong("money_exchange");
                baucua.time_log = document.getString("time_log");
                results.add(baucua);
            }
        });
        return results;
    }

    @Override
    public int countLogBauCua(final String referent_id, final String nickName, final String room, final String timeStart, final String timeEnd, final String moneyType) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        final BasicDBObject obj = new BasicDBObject();
        int record = 0;
        if (nickName != null && !nickName.equals("")) {
            final String pattern = ".*" + nickName + ".*";
            conditions.put("user_name", new BasicDBObject().append("$regex", pattern).append("$options", "i"));
        }
        if (referent_id != null && !referent_id.equals("")) {
            conditions.put("reference_id", Long.parseLong(referent_id));
        }
        if (room != null && !room.equals("")) {
            conditions.put("room", Integer.parseInt(room));
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            obj.put("$gte", timeStart + " 00:00:00");
            obj.put("$lte", timeEnd + " 23:59:59");
            conditions.put("time_log", obj);
        }
        if (moneyType != null && !moneyType.equals("")) {
            conditions.put("money_type", Integer.parseInt(moneyType));
        }
        record = (int) db.getCollection("bau_cua_transaction").count(new Document(conditions));
        return record;
    }

    @Override
    public List<BauCuaReponseDetail> getLogBauCuaDetail(final String referent_id, final int page) {
        final int num_start = (page - 1) * 50;
        final int num_end = 50;
        final BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        final ArrayList<BauCuaReponseDetail> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        FindIterable iterable = null;
        if (referent_id != null && !referent_id.equals("")) {
            conditions.put("reference_id", Long.parseLong(referent_id));
        }
        iterable = db.getCollection("bau_cua_transaction_detail").find(new Document(conditions)).sort(objsort).skip(num_start).limit(50);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final BauCuaReponseDetail detail = new BauCuaReponseDetail();
                detail.referent_id = document.getLong("reference_id");
                detail.user_name = document.getString("user_name");
                detail.room = document.getInteger("room");
                detail.bet_value = document.getString("bet_value");
                detail.bet_bau = document.getLong("bet_bau");
                detail.bet_cua = document.getLong("bet_cua");
                detail.bet_tom = document.getLong("bet_tom");
                detail.bet_ca = document.getLong("bet_ca");
                detail.bet_ga = document.getLong("bet_ga");
                detail.bet_huou = document.getLong("bet_huou");
                detail.money_type = document.getInteger("money_type");
                detail.time_log = document.getString("time_log");
                results.add(detail);
            }
        });
        return results;
    }

    @Override
    public int countLogBauCuaDetail(final String referent_id) {
        int record = 0;
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        if (referent_id != null && !referent_id.equals("")) {
            conditions.put("reference_id", Long.parseLong(referent_id));
        }
        record = (int) db.getCollection("bau_cua_transaction_detail").count(new Document(conditions));
        return record;
    }

    @Override
    public List<BauCuaResultResponse> listLogBauCauResult(final String referent_id, final String room, final String timeStart, final String timeEnd, final int page) {
        final ArrayList<BauCuaResultResponse> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        FindIterable iterable = null;
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        final int num_start = (page - 1) * 50;
        final int num_end = 50;
        objsort.put("_id", -1);
        if (referent_id != null && !referent_id.equals("")) {
            conditions.put("reference_id", Long.parseLong(referent_id));
        }
        if (room != null && !room.equals("")) {
            conditions.put("room", Integer.parseInt(room));
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("time_log", obj);
        }
        iterable = db.getCollection("bau_cua_results").find(new Document(conditions)).sort(objsort).skip(num_start).limit(50);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final BauCuaResultResponse baucua = new BauCuaResultResponse();
                baucua.reference_id = document.getLong("reference_id");
                baucua.room = document.getInteger("room");
                baucua.dice1 = document.getInteger("dice1");
                baucua.dice2 = document.getInteger("dice2");
                baucua.dice3 = document.getInteger("dice3");
                baucua.x_pot = document.getInteger("x_pot");
                baucua.bet_value = document.getString("bet_value");
                baucua.x_value = document.getInteger("x_value");
                baucua.bet_bau = document.getLong("bet_bau");
                baucua.bet_ca = document.getLong("bet_ca");
                baucua.bet_ga = document.getLong("bet_ga");
                baucua.bet_tom = document.getLong("bet_tom");
                baucua.bet_huou = document.getLong("bet_huou");
                baucua.bet_cua = document.getLong("bet_cua");
                baucua.prize_bau = document.getLong("prize_bau");
                baucua.prize_cua = document.getLong("prize_cua");
                baucua.prize_ca = document.getLong("prize_ca");
                baucua.prize_tom = document.getLong("prize_tom");
                baucua.prize_huou = document.getLong("prize_huou");
                baucua.prize_ga = document.getLong("prize_ga");
                baucua.time_log = document.getString("time_log");
                baucua.totalBet = baucua.bet_bau + baucua.bet_ca + baucua.bet_ga + baucua.bet_tom + baucua.bet_huou + baucua.bet_cua;
                baucua.totalPrize = baucua.prize_bau + baucua.prize_cua + baucua.prize_ca + baucua.prize_tom + baucua.prize_huou + baucua.prize_ga;
                final long money = baucua.total = baucua.totalPrize - baucua.totalBet;
                results.add(baucua);
            }
        });
        return results;
    }

    @Override
    public long countLogBauCauResult(final String referent_id, final String room, final String timeStart, final String timeEnd) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        if (referent_id != null && !referent_id.equals("")) {
            conditions.put("reference_id", Long.parseLong(referent_id));
        }
        if (room != null && !room.equals("")) {
            conditions.put("room", Integer.parseInt(room));
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("time_log", obj);
        }
        return db.getCollection("bau_cua_results").count(new Document(conditions));
    }
}
