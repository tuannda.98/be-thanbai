// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.utils;

import com.hazelcast.core.IMap;
import com.vinplay.dal.dao.impl.PotDaoImpl;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.models.PotModel;

import java.sql.SQLException;
import java.util.List;

public class PotUtils {
    public static void init() throws SQLException {
        final IMap potMap = HazelcastClientFactory.getInstance().getMap("huGameBai");
        final PotDaoImpl dao = new PotDaoImpl();
        final List<PotModel> listModel = dao.getAll();
        for (final PotModel model : listModel) {
            if (potMap.containsKey(model.getPotName())) {
                continue;
            }
            potMap.put(model.getPotName(), model);
        }
    }
}
