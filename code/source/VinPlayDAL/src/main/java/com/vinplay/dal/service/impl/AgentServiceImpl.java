// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.vinplay.dal.dao.impl.AgentDAOImpl;
import com.vinplay.dal.service.AgentService;
import com.vinplay.vbee.common.models.AgentModel;
import com.vinplay.vbee.common.response.AgentResponse;
import com.vinplay.vbee.common.response.LogAgentTranferMoneyResponse;
import com.vinplay.vbee.common.response.TranferAgentResponse;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AgentServiceImpl implements AgentService {
    @Override
    public List<AgentResponse> listAgent() throws SQLException {
        final AgentDAOImpl dao = new AgentDAOImpl();
        return dao.listAgent();
    }

    @Override
    public List<AgentResponse> listAgentByClient(final String client) throws SQLException {
        final AgentDAOImpl dao = new AgentDAOImpl();
        return dao.listAgentByClient(client);
    }

    @Override
    public List<LogAgentTranferMoneyResponse> searchAgentTranferMoney(final String nickNameSend, final String nickNameRecieve, final String status, final String timeStart, final String timeEnd, final String top_ds, final int page) {
        final AgentDAOImpl dao = new AgentDAOImpl();
        return dao.searchAgentTranferMoney(nickNameSend, nickNameRecieve, status, timeStart, timeEnd, top_ds, page);
    }

    @Override
    public List<AgentResponse> listUserAgent(final String nickName) throws SQLException {
        final AgentDAOImpl dao = new AgentDAOImpl();
        return dao.listUserAgent(nickName);
    }

    @Override
    public List<TranferAgentResponse> searchAgentTranfer(final String nickName, final String status, final String timeStart, final String timeEnd, final double ratio1, final double ratio2, final double ratio2More, final long minRefundFee2More) throws SQLException {
        final AgentDAOImpl dao = new AgentDAOImpl();
        List<AgentResponse> agent = new ArrayList<>();
        final ArrayList<TranferAgentResponse> result = new ArrayList<>();
        final String[] arrNN = nickName.split(",");
        for (String s : arrNN) {
            final String nn = s.trim();
            agent = dao.listUserAgent(nn);
            for (final AgentResponse name : agent) {
                TranferAgentResponse tran = new TranferAgentResponse();
                tran = dao.searchAgentTranfer(name.nickName, status, timeStart, timeEnd);
                final long ds2 = tran.getTotalBuy2() + tran.getTotalSale2();
                long fee2 = 0L;
                fee2 = ((ds2 < minRefundFee2More) ? Math.round((tran.getTotalFeeBuy2() + tran.getTotalFeeSale2()) * ratio2) : Math.round((tran.getTotalFeeBuy2() + tran.getTotalFeeSale2()) * ratio2More));
                final long totalFee = Math.round((tran.getTotalFeeBuy1() + tran.getTotalFeeSale1()) * ratio1) + fee2;
                final long totalFeeByVinplayCardTemp = Math.round((float) (totalFee * name.percent / 100L));
                final long totalFeeByVinplayCard = roundVinCard(totalFee, totalFeeByVinplayCardTemp);
                final long totalFeeByVinCash = totalFee - totalFeeByVinplayCard;
                tran.setTotalFee(totalFee);
                tran.setTotalFeeByVinplayCard(totalFeeByVinplayCard);
                tran.setTotalFeeByVinCash(totalFeeByVinCash);
                tran.setPercent(name.percent);
                result.add(tran);
            }
        }
        return result;
    }

    private static long roundVinCard(final long moneyTotal, final long moneyVinCard) {
        if (moneyVinCard < 10500L) {
            return 0L;
        }
        final long division = moneyVinCard / 10500L;
        if ((division + 1L) * 10500L < moneyTotal) {
            return (division + 1L) * 10500L;
        }
        return division * 10500L;
    }

    @Override
    public long countsearchAgentTranferMoney(final String nickNameSend, final String nickNameRecieve, final String status, final String timeStart, final String timeEnd, final String topDS) {
        final AgentDAOImpl dao = new AgentDAOImpl();
        return dao.countsearchAgentTranferMoney(nickNameSend, nickNameRecieve, status, timeStart, timeEnd, topDS);
    }

    @Override
    public long totalMoneyVinReceiveFromAgent(final String nickName, final String status, final String timeStart, final String timeEnd, final String topDS) {
        final AgentDAOImpl dao = new AgentDAOImpl();
        return dao.totalMoneyVinReceiveFromAgent(nickName, status, timeStart, timeEnd, topDS);
    }

    @Override
    public long totalMoneyVinSendFromAgent(final String nickName, final String status, final String timeStart, final String timeEnd, final String topDS) {
        final AgentDAOImpl dao = new AgentDAOImpl();
        return dao.totalMoneyVinSendFromAgent(nickName, status, timeStart, timeEnd, topDS);
    }

    @Override
    public long totalMoneyVinFeeFromAgent(final String nickName, final String status, final String timeStart, final String timeEnd, final String topDS) {
        final AgentDAOImpl dao = new AgentDAOImpl();
        return dao.totalMoneyVinFeeFromAgent(nickName, status, timeStart, timeEnd, topDS);
    }

    @Override
    public List<LogAgentTranferMoneyResponse> searchAgentTranferMoneyVinSale(final String nickName, final String timeStart, final String timeEnd, final String type, final int page, final int totalRecord) {
        final AgentDAOImpl dao = new AgentDAOImpl();
        return dao.searchAgentTranferMoneyVinSale(nickName, timeStart, timeEnd, type, page, totalRecord);
    }

    @Override
    public long totalMoneyVinReceiveFromAgentByStatus(final String nickName, final String type, final String timeStart, final String timeEnd) {
        final AgentDAOImpl dao = new AgentDAOImpl();
        return dao.totalMoneyVinReceiveFromAgentByStatus(nickName, type, timeStart, timeEnd);
    }

    @Override
    public long totalMoneyVinSendFromAgentByStatus(final String nickName, final String type, final String timeStart, final String timeEnd) {
        final AgentDAOImpl dao = new AgentDAOImpl();
        return dao.totalMoneyVinSendFromAgentByStatus(nickName, type, timeStart, timeEnd);
    }

    @Override
    public long countSearchAgentTranferMoneyVinSale(final String nickName, final String timeStart, final String timeEnd, final String type) {
        final AgentDAOImpl dao = new AgentDAOImpl();
        return dao.countSearchAgentTranferMoneyVinSale(nickName, timeStart, timeEnd, type);
    }

    @Override
    public boolean updateTopDsFromAgent(final String nickNameSend, final String nickNameReceive, final String timeLog, final String topds) {
        try {
            final AgentDAOImpl dao = new AgentDAOImpl();
            dao.updateTopDsFromAgent(nickNameSend, nickNameReceive, timeLog, topds);
            dao.updateTopDsFromAgentMySQL(nickNameSend, nickNameReceive, timeLog, topds);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public List<TranferAgentResponse> searchAgentTranferAdmin(final String nickName, final String status, final String timeStart, final String timeEnd, final double ratio1, final double ratio2, final double ratio2More, final long minRefundFee2More) throws SQLException {
        final AgentDAOImpl dao = new AgentDAOImpl();
        List<AgentResponse> agent = new ArrayList<>();
        final ArrayList<TranferAgentResponse> result = new ArrayList<>();
        final String[] arrNN = nickName.split(",");
        for (String s : arrNN) {
            final String nn = s.trim();
            agent = dao.listUserAgentAdmin(nn);
            for (final AgentResponse name : agent) {
                TranferAgentResponse tran = new TranferAgentResponse();
                tran = dao.searchAgentTranfer(name.nickName, status, timeStart, timeEnd);
                final long ds2 = tran.getTotalBuy2() + tran.getTotalSale2();
                long fee2 = 0L;
                fee2 = ((ds2 < minRefundFee2More) ? Math.round((tran.getTotalFeeBuy2() + tran.getTotalFeeSale2()) * ratio2) : Math.round((tran.getTotalFeeBuy2() + tran.getTotalFeeSale2()) * ratio2More));
                final long totalFee = Math.round((tran.getTotalFeeBuy1() + tran.getTotalFeeSale1()) * ratio1) + fee2;
                final long totalFeeByVinplayCardTemp = Math.round((float) (totalFee * name.percent / 100L));
                final long totalFeeByVinplayCard = roundVinCard(totalFee, totalFeeByVinplayCardTemp);
                final long totalFeeByVinCash = totalFee - totalFeeByVinplayCard;
                tran.setTotalFee(totalFee);
                tran.setTotalFeeByVinplayCard(totalFeeByVinplayCard);
                tran.setTotalFeeByVinCash(totalFeeByVinCash);
                tran.setPercent(name.percent);
                result.add(tran);
            }
        }
        return result;
    }

    @Override
    public List<LogAgentTranferMoneyResponse> searchAgentTranferMoney(final String nickName, final String status, final String timeStart, final String timeEnd, final String top_ds, final int page) {
        final AgentDAOImpl dao = new AgentDAOImpl();
        return dao.searchAgentTranferMoney(nickName, status, timeStart, timeEnd, top_ds, page);
    }

    @Override
    public long countsearchAgentTranferMoney(final String nickName, final String status, final String timeStart, final String timeEnd, final String top_ds) {
        final AgentDAOImpl dao = new AgentDAOImpl();
        return dao.countsearchAgentTranferMoney(nickName, status, timeStart, timeEnd, top_ds);
    }

    @Override
    public long totalMoneyVinReceiveFromAgent(final String nickNameSend, final String nickNameRecieve, final String status, final String timeStart, final String timeEnd, final String top_ds) {
        final AgentDAOImpl dao = new AgentDAOImpl();
        return dao.totalMoneyVinReceiveFromAgent(nickNameSend, nickNameRecieve, status, timeStart, timeEnd, top_ds);
    }

    @Override
    public long totalMoneyVinSendFromAgent(final String nickNameSend, final String nickNameRecieve, final String status, final String timeStart, final String timeEnd, final String top_ds) {
        final AgentDAOImpl dao = new AgentDAOImpl();
        return dao.totalMoneyVinSendFromAgent(nickNameSend, nickNameRecieve, status, timeStart, timeEnd, top_ds);
    }

    @Override
    public long totalMoneyVinFeeFromAgent(final String nickNameSend, final String nickNameRecieve, final String status, final String timeStart, final String timeEnd, final String topDS) {
        final AgentDAOImpl dao = new AgentDAOImpl();
        return dao.totalMoneyVinFeeFromAgent(nickNameSend, nickNameRecieve, status, timeStart, timeEnd, topDS);
    }

    @Override
    public List<AgentResponse> listUserAgentActive(final String nickName) throws SQLException {
        final AgentDAOImpl dao = new AgentDAOImpl();
        return dao.listUserAgentActive(nickName);
    }

    @Override
    public List<AgentResponse> listUserAgentLevel1Active() throws SQLException {
        final AgentDAOImpl dao = new AgentDAOImpl();
        return dao.listUserAgentLevel1Active();
    }

    @Override
    public List<LogAgentTranferMoneyResponse> searchAgentTongTranferMoney(final String nickNameSend, final String nickNameRecieve, final String status, final String timeStart, final String timeEnd, final String top_ds, final int page) {
        final AgentDAOImpl dao = new AgentDAOImpl();
        return dao.searchAgentTongTranferMoney(nickNameSend, nickNameRecieve, status, timeStart, timeEnd, top_ds, page);
    }

    @Override
    public long countsearchAgentTongTranferMoney(final String nickNameSend, final String nickNameRecieve, final String status, final String timeStart, final String timeEnd, final String topDS) {
        final AgentDAOImpl dao = new AgentDAOImpl();
        return dao.countsearchAgentTongTranferMoney(nickNameSend, nickNameRecieve, status, timeStart, timeEnd, topDS);
    }

    @Override
    public long totalMoneyVinReceiveFromAgentTong(final String nickNameSend, final String nickNameRecieve, final String status, final String timeStart, final String timeEnd, final String top_ds) {
        final AgentDAOImpl dao = new AgentDAOImpl();
        return dao.totalMoneyVinReceiveFromAgentTong(nickNameSend, nickNameRecieve, status, timeStart, timeEnd, top_ds);
    }

    @Override
    public long totalMoneyVinSendFromAgentTong(final String nickNameSend, final String nickNameRecieve, final String status, final String timeStart, final String timeEnd, final String top_ds) {
        final AgentDAOImpl dao = new AgentDAOImpl();
        return dao.totalMoneyVinSendFromAgentTong(nickNameSend, nickNameRecieve, status, timeStart, timeEnd, top_ds);
    }

    @Override
    public long totalMoneyVinFeeFromAgentTong(final String nickNameSend, final String nickNameRecieve, final String status, final String timeStart, final String timeEnd, final String topDS) {
        final AgentDAOImpl dao = new AgentDAOImpl();
        return dao.totalMoneyVinFeeFromAgentTong(nickNameSend, nickNameRecieve, status, timeStart, timeEnd, topDS);
    }

    @Override
    public List<AgentModel> getListPercentBonusVincard(final String nickName) throws SQLException {
        final AgentDAOImpl dao = new AgentDAOImpl();
        return dao.getListPercentBonusVincard(nickName);
    }

    @Override
    public int registerPercentBonusVincard(final String nickName, final int percent) throws SQLException {
        final AgentDAOImpl dao = new AgentDAOImpl();
        return dao.registerPercentBonusVincard(nickName, percent);
    }
}
