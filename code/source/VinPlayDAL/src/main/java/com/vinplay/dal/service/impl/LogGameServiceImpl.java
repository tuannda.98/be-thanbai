// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.vinplay.dal.dao.impl.LogGameDAOImpl;
import com.vinplay.dal.service.LogGameService;
import com.vinplay.vbee.common.messages.LogGameMessage;
import com.vinplay.vbee.common.rmq.RMQApi;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeoutException;

public class LogGameServiceImpl implements LogGameService {
    @Override
    public boolean saveLogGameByNickName(final LogGameMessage message) {
        try {
            RMQApi.publishMessage("queue_log_gamebai", message, 501);
        } catch (IOException | InterruptedException | TimeoutException ex4) {
            ex4.printStackTrace();
        }
        return true;
    }

    @Override
    public boolean saveLogGameDetail(final LogGameMessage message) {
        try {
            RMQApi.publishMessage("queue_log_gamebai", message, 502);
        } catch (IOException | InterruptedException | TimeoutException ex4) {
            ex4.printStackTrace();
        }
        return true;
    }

    @Override
    public List<LogGameMessage> searchLogGameByNickName(final String sessionId, final String nickName, final String gameName, final String timeStart, final String timeEnd, final String moneyType, final int page) {
        final LogGameDAOImpl dao = new LogGameDAOImpl();
        List<LogGameMessage> result = null;
        try {
            result = dao.searchLogGameByNickName(sessionId, nickName, gameName, timeStart, timeEnd, moneyType, page);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public int countSearchLogGameByNickName(final String sessionId, final String nickName, final String gameName, final String timeStart, final String timeEnd, final String moneyType) {
        final LogGameDAOImpl dao = new LogGameDAOImpl();
        int record = 0;
        try {
            record = dao.countSearchLogGameByNickName(sessionId, nickName, gameName, timeStart, timeEnd, moneyType);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return record;
    }

    @Override
    public LogGameMessage getLogGameDetailBySessionID(final String sessionId, final String gameName, final String timelog) {
        final LogGameDAOImpl dao = new LogGameDAOImpl();
        LogGameMessage result = null;
        try {
            result = dao.getLogGameDetailBySessionID(sessionId, gameName, timelog);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
