// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.vinplay.dal.dao.impl.VipPointEventDaoImpl;
import com.vinplay.dal.service.VipPointEventService;
import com.vinplay.vbee.common.response.LogVipPointEventResponse;
import com.vinplay.vbee.common.response.UserVipPointEventResponse;

import java.sql.SQLException;
import java.util.List;

public class VipPointEventServiceImpl implements VipPointEventService {
    @Override
    public List<LogVipPointEventResponse> listLogVipPointEvent(final String nickName, final String value, final String type, final String timeStart, final String timeEnd, final int page, final String bot) {
        final VipPointEventDaoImpl dao = new VipPointEventDaoImpl();
        return dao.listLogVipPointEvent(nickName, value, type, timeStart, timeEnd, page, bot);
    }

    @Override
    public long countLogVipPointEvent(final String nickName, final String value, final String type, final String timeStart, final String timeEnd, final String bot) {
        final VipPointEventDaoImpl dao = new VipPointEventDaoImpl();
        return dao.countLogVipPointEvent(nickName, value, type, timeStart, timeEnd, bot);
    }

    @Override
    public long totalVipPointEvent(final String nickName, final String value, final String type, final String timeStart, final String timeEnd, final String bot) {
        final VipPointEventDaoImpl dao = new VipPointEventDaoImpl();
        return dao.totalVipPointEvent(nickName, value, type, timeStart, timeEnd, bot);
    }

    @Override
    public List<UserVipPointEventResponse> listuserVipPoint(final String nickName, final String sort, final String filed, final String timeStart, final String timeEnd, final int page, final String bot) throws SQLException {
        final VipPointEventDaoImpl dao = new VipPointEventDaoImpl();
        return dao.listuserVipPoint(nickName, sort, filed, timeStart, timeEnd, page, bot);
    }

    @Override
    public long countUserVipPoint(final String nickName, final String sort, final String filed, final String timeStart, final String timeEnd, final String bot) throws SQLException {
        final VipPointEventDaoImpl dao = new VipPointEventDaoImpl();
        return dao.countUserVipPoint(nickName, sort, filed, timeStart, timeEnd, bot);
    }
}
