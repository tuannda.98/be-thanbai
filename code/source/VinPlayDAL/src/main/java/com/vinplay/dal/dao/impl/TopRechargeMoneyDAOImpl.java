// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao.impl;

import com.vinplay.dal.dao.TopRechargeMoneyDAO;
import com.vinplay.vbee.common.pools.ConnectionPool;
import com.vinplay.vbee.common.response.TopRechargeMoneyResponse;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TopRechargeMoneyDAOImpl implements TopRechargeMoneyDAO {
    @Override
    public List<TopRechargeMoneyResponse> getTopRechargeMoney(final int top, final String nickName, final int page, final int bot) throws SQLException {
        final ArrayList<TopRechargeMoneyResponse> results = new ArrayList<>();
        final int numStart = (page - 1) * 50;
        final int numEnd = 50;
        String sql = "";
        String conditions = "";
        final String orderby = " ORDER BY `recharge_money` desc";
        final String limit = " LIMIT " + numStart + "," + 50;
        final String query = "SELECT nick_name,recharge_money FROM users where 1=1 ";
        if (top > 0) {
            sql = "SELECT nick_name,recharge_money FROM users where 1=1 " + conditions + " ORDER BY `recharge_money` desc" + limit;
        }
        if (bot == 0) {
            conditions = " AND is_bot=" + bot;
            sql = "SELECT nick_name,recharge_money FROM users where 1=1 " + conditions + " ORDER BY `recharge_money` desc" + limit;
        }
        if (bot == 1) {
            sql = "SELECT nick_name,recharge_money FROM users where 1=1 " + conditions + " ORDER BY `recharge_money` desc" + limit;
        }
        if (!nickName.equals("") && !nickName.equals(null)) {
            conditions = " AND nick_name='" + nickName + "'";
            sql = "SELECT nick_name,recharge_money FROM users where 1=1 " + conditions + " ORDER BY `recharge_money` desc" + limit;
        }
        try (final Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             final PreparedStatement stmt = conn.prepareStatement(sql);
             final ResultSet rs = stmt.executeQuery();) {
            while (rs.next()) {
                final TopRechargeMoneyResponse entry = new TopRechargeMoneyResponse();
                entry.userName = rs.getString("user_name");
                entry.money = rs.getLong("recharge_money");
                results.add(entry);
            }
        }
        return results;
    }
}
