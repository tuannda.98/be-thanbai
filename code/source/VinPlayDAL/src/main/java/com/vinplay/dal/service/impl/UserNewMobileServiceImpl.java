// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.vinplay.dal.dao.impl.UserNewMobileDAOImpl;
import com.vinplay.dal.service.UserNewMobileService;
import com.vinplay.vbee.common.response.UserNewMobileResponse;

import java.util.List;

public class UserNewMobileServiceImpl implements UserNewMobileService {
    @Override
    public List<UserNewMobileResponse> searchUserNewMobile(final String nickName, final String mobile, final String mobileold, final int page) {
        final UserNewMobileDAOImpl dao = new UserNewMobileDAOImpl();
        return dao.searchUserNewMobile(nickName, mobile, mobileold, page);
    }

    @Override
    public long countSearchUserNewMobile(final String nickName, final String mobile, final String mobileold) {
        final UserNewMobileDAOImpl dao = new UserNewMobileDAOImpl();
        return dao.countSearchUserNewMobile(nickName, mobile, mobileold);
    }
}
