// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao;

import com.vinplay.vbee.common.response.LuckyNewHistoryResponse;

import java.util.List;

public interface LuckyNewHistoryDAO {
    List<LuckyNewHistoryResponse> searchLuckyNewHistory(final String p0, final String p1, final String p2, final int p3);
}
