// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service;

import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.response.MoneyResponse;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

public interface BotService {
    UserModel login(final String p0) throws SQLException, NoSuchAlgorithmException;

    MoneyResponse addMoney(final String p0, final long p1, final String p2, final String p3);
}
