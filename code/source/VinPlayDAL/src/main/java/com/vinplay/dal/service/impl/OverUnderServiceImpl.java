// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.vinplay.dal.dao.OverUnderDAO;
import com.vinplay.dal.dao.impl.MiniGameDAOImpl;
import com.vinplay.dal.dao.impl.OverUnderDAOImpl;
import com.vinplay.dal.entities.report.ReportMoneySystemModel;
import com.vinplay.dal.entities.taixiu.ResultTaiXiu;
import com.vinplay.dal.entities.taixiu.TransactionTaiXiu;
import com.vinplay.dal.entities.taixiu.TransactionTaiXiuDetail;
import com.vinplay.dal.entities.taixiu.VinhDanhRLTLModel;
import com.vinplay.dal.service.OverUnderService;
import com.vinplay.vbee.common.enums.Games;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.messages.minigame.*;
import com.vinplay.vbee.common.models.cache.RutLocCacheModel;
import com.vinplay.vbee.common.models.cache.ThanhDuTXModel;
import com.vinplay.vbee.common.models.cache.TopRLTLModel;
import com.vinplay.vbee.common.models.cache.TopWinCache;
import com.vinplay.vbee.common.models.minigame.TopWin;
import com.vinplay.vbee.common.models.minigame.taixiu.XepHangRLTLModel;
import com.vinplay.vbee.common.rmq.RMQApi;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeoutException;

public class OverUnderServiceImpl implements OverUnderService {
    private final Logger logger;
    private final OverUnderDAO dao;

    public OverUnderServiceImpl() {
        this.logger = Logger.getLogger("rmq");
        this.dao = new OverUnderDAOImpl();
    }

    @Override
    public boolean saveTransactionTaiXiu(final long referenceId, final int userId, final String username, final int moneyType, final long betValue, final short betSide, final long prize, final long refund) throws IOException, TimeoutException, InterruptedException {
        final TransactionTaiXiuMessage msg = new TransactionTaiXiuMessage();
        msg.referenceId = referenceId;
        msg.userId = userId;
        msg.username = username;
        msg.moneyType = moneyType;
        msg.betValue = betValue;
        msg.betSide = betSide;
        msg.prize = prize;
        msg.refund = refund;
        RMQApi.publishMessage("queue_overunder", msg, 10100);
        return true;
    }

    @Override
    public boolean saveResultTaiXiu(final long referenceId, final int result, final int dice1, final int dice2, final int dice3, final long totalTai, final long totalXiu, final int numBetTai, final int numBetXiu, final long totalPrize, final long totalRefundTai, final long totalRefundXiu, final long totalRevenue, final int moneyType) throws Exception {
        final ResultTaiXiuMessage msg = new ResultTaiXiuMessage();
        msg.referenceId = referenceId;
        msg.result = result;
        msg.dice1 = dice1;
        msg.dice2 = dice2;
        msg.dice3 = dice3;
        msg.totalTai = totalTai;
        msg.totalXiu = totalXiu;
        msg.numBetTai = numBetTai;
        msg.numBetXiu = numBetXiu;
        msg.totalPrize = totalPrize;
        msg.totalRefundTai = totalRefundTai;
        msg.totalRefundXiu = totalRefundXiu;
        msg.totalRevenue = totalRevenue;
        msg.moneyType = moneyType;
        RMQApi.publishMessage("queue_overunder", msg, 10101);
        return true;
    }

    @Override
    public String getLichSuPhien(final int soPhien, final int moneyType) throws SQLException {
        final List<ResultTaiXiu> results = this.dao.getLichSuPhien(soPhien, moneyType);
        return this.buildLichSuPhien(results, soPhien);
    }

    @Override
    public List<TopWin> getTopWin(final int moneyType) {
        final HazelcastInstance client = HazelcastClientFactory.getInstance();
        final IMap topMap = client.getMap("cacheTop");
        final TopWinCache topTXCache;
        if (topMap.containsKey(Games.OVER_UNDER.getName() + "_" + moneyType) && (topTXCache = (TopWinCache) topMap.get(Games.OVER_UNDER.getName() + "_" + moneyType)) != null) {
            return topTXCache.getResult();
        }
        return new ArrayList<>();
    }

    @Override
    public void updateAllTop() {
        try {
            final List<TopWin> topWinVin = this.dao.getTopTaiXiu(1);
            this.logger.debug("TOP WIN VIN: " + topWinVin.size());
            final List<TopWin> topWinXu = this.dao.getTopTaiXiu(0);
            this.logger.debug("TOP WIN XU: " + topWinXu.size());
            final HazelcastInstance client = HazelcastClientFactory.getInstance();
            final IMap topMap = client.getMap("cacheTop");
            TopWinCache cacheVin = (TopWinCache) topMap.get(Games.OVER_UNDER.getName() + "_1");
            if (cacheVin == null) {
                cacheVin = new TopWinCache();
            }
            cacheVin.setResult(topWinVin);
            topMap.put(Games.OVER_UNDER.getName() + "_1", cacheVin);
            TopWinCache cacheXu = (TopWinCache) topMap.get(Games.OVER_UNDER.getName() + "_0");
            if (cacheXu == null) {
                cacheXu = new TopWinCache();
            }
            cacheXu.setResult(topWinXu);
            topMap.put(Games.OVER_UNDER.getName() + "_0", cacheXu);
        } catch (SQLException e) {
            this.logger.error("UPDATE ALL TOP exception: ", e);
            e.printStackTrace();
        }
    }

    @Override
    public List<TransactionTaiXiu> getLichSuGiaoDich(final String username, final int page, final int moneyType) throws SQLException {
        return this.dao.getLichSuGiaoDich(username, page, moneyType);
    }

    @Override
    public List<ResultTaiXiu> getListLichSuPhien(final int soPhien, final int moneyType) throws SQLException {
        return this.dao.getLichSuPhien(soPhien, moneyType);
    }

    @Override
    public boolean saveTransactionTaiXiuDetails(final List<TransactionTaiXiuDetail> trans) throws IOException, TimeoutException, InterruptedException {
        boolean success = true;
        for (final TransactionTaiXiuDetail tran : trans) {
            success = (success && this.saveTransactionTaiXiuDetail(tran));
        }
        return success;
    }

    @Override
    public boolean saveResultTaiXiu(final ResultTaiXiu rs) throws Exception {
        this.logger.debug("Save result tx");
        return this.saveResultTaiXiu(rs.referenceId, rs.result, rs.dice1, rs.dice2, rs.dice3, rs.totalTai, rs.totalXiu, rs.numBetTai, rs.numBetXiu, rs.totalPrize, rs.totalRefundTai, rs.totalRefundXiu, rs.totalRevenue, rs.moneyType);
    }

    @Override
    public boolean saveTransactionTaiXiu(final List<TransactionTaiXiu> trans) throws IOException, TimeoutException, InterruptedException {
        for (final TransactionTaiXiu tran : trans) {
            this.saveTransactionTaiXiu(tran.referenceId, tran.userId, tran.username, tran.moneyType, tran.betValue, (short) tran.betSide, tran.totalPrize, tran.totalRefund);
        }
        return false;
    }

    @Override
    public boolean saveTransactionTaiXiuDetail(final TransactionTaiXiuDetail tran) throws IOException, TimeoutException, InterruptedException {
        final TransactionTaiXiuDetailMessage msg = new TransactionTaiXiuDetailMessage();
        msg.referenceId = tran.referenceId;
        msg.transactionCode = tran.transactionCode;
        msg.userId = tran.userId;
        msg.username = tran.username;
        msg.betValue = tran.betValue;
        msg.betSide = tran.betSide;
        msg.prize = tran.prize;
        msg.refund = tran.refund;
        msg.inputTime = tran.inputTime;
        msg.moneyType = tran.moneyType;
        RMQApi.publishMessage("queue_overunder", msg, 10102);
        return true;
    }

    @Override
    public int countLichSuGiaoDich(final String nickname, final int moneyType) throws SQLException {
        final int totalRecords = this.dao.countLichSuGiaoDichTX(nickname, moneyType);
        return totalRecords / 10 + 1;
    }

    @Override
    public List<TransactionTaiXiuDetail> getChiTietPhienTX(final long referenceId, final int moneyType) throws SQLException {
        return this.dao.getChiTietPhien(referenceId, moneyType);
    }

    @Override
    public ResultTaiXiu getKetQuaPhien(final long referenceId, final int moneyType) throws SQLException {
        return this.dao.getKetQuaPhien(referenceId, moneyType);
    }

    @Override
    public boolean updateTransactionTaiXiuDetail(final TransactionTaiXiuDetail tran) throws IOException, TimeoutException, InterruptedException {
        final TransactionTaiXiuDetailMessage msg = new TransactionTaiXiuDetailMessage();
        msg.referenceId = tran.referenceId;
        msg.transactionCode = tran.transactionCode;
        msg.userId = tran.userId;
        msg.username = tran.username;
        msg.betValue = tran.betValue;
        msg.betSide = tran.betSide;
        msg.prize = tran.prize;
        msg.refund = tran.refund;
        msg.inputTime = tran.inputTime;
        msg.moneyType = tran.moneyType;
        RMQApi.publishMessage("queue_overunder", msg, 10103);
        return true;
    }

    @Override
    public void calculateThanhDu(final long referenceId, final List<TransactionTaiXiu> transacntions, final int result) throws IOException, TimeoutException, InterruptedException {
        final HazelcastInstance client = HazelcastClientFactory.getInstance();
        final IMap winMap = client.getMap("cacheWinThanhDuOU");
        final IMap lossMap = client.getMap("cacheLossThanhDuOU");
        for (final TransactionTaiXiu tran : transacntions) {
            if (tran.betSide == result) {
                final long moneyExchange = tran.betValue - tran.totalRefund;
                if (moneyExchange >= 2000L) {
                    this.incrementThanhDu(referenceId, (IMap<String, ThanhDuTXModel>) winMap, tran.username, moneyExchange, 1);
                }
                if (moneyExchange <= 0L) {
                    continue;
                }
                this.clearThanhDu((IMap<String, ThanhDuTXModel>) lossMap, tran.username, 0);
            } else {
                final long moneyExchange = tran.betValue - tran.totalRefund;
                if (moneyExchange >= 2000L) {
                    this.incrementThanhDu(referenceId, (IMap<String, ThanhDuTXModel>) lossMap, tran.username, moneyExchange, 0);
                }
                if (moneyExchange <= 0L) {
                    continue;
                }
                this.clearThanhDu((IMap<String, ThanhDuTXModel>) winMap, tran.username, 1);
            }
        }
    }

    private void incrementThanhDu(final long referenceId, final IMap<String, ThanhDuTXModel> map, final String username, final long moneyExchange, final int type) throws IOException, TimeoutException, InterruptedException {
        if (moneyExchange < 2000L) {
            return;
        }
        ThanhDuTXModel model = null;
        if (map.containsKey(username)) {
            try {
                map.lock(username);
                model = map.get(username);
                if (model != null) {
                    if (!model.playOnToday()) {
                        model = new ThanhDuTXModel(username);
                    } else {
                        final ThanhDuTXModel thanhDuTXModel3;
                        final ThanhDuTXModel thanhDuTXModel = thanhDuTXModel3 = model;
                        ++thanhDuTXModel3.number;
                    }
                    model.addReference(referenceId);
                    final ThanhDuTXModel thanhDuTXModel4;
                    final ThanhDuTXModel thanhDuTXModel2 = thanhDuTXModel4 = model;
                    thanhDuTXModel4.totalValue += moneyExchange;
                    if (!model.valid && moneyExchange >= 10000L) {
                        model.valid = true;
                    }
                    if (model.number > model.maxNumber && model.valid) {
                        model.maxNumber = model.number;
                        final ThanhDuMessage message = new ThanhDuMessage(model.username, model.number, model.totalValue, model.currentReferenceId, model.getReferences(), (short) type);
                        RMQApi.publishMessage("queue_overunder", message, 10104);
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                map.unlock(username);
            }
        } else {
            model = new ThanhDuTXModel(username);
            model.totalValue = moneyExchange;
            model.addReference(referenceId);
            int max = 0;
            try {
                max = this.dao.getMaxThanhDu(username, (short) type);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            model.maxNumber = max;
            if (moneyExchange >= 10000L) {
                model.valid = true;
                final ThanhDuMessage message2 = new ThanhDuMessage(model.username, model.number, model.totalValue, model.currentReferenceId, model.getReferences(), (short) type);
                RMQApi.publishMessage("queue_overunder", message2, 10104);
            }
        }
        if (model == null) {
            return;
        }
        map.put(username, model);
    }

    private void clearThanhDu(final IMap<String, ThanhDuTXModel> map, final String username, final int type) {
        final ThanhDuTXModel model;
        if (map.containsKey(username) && (model = map.get(username)) != null) {
            model.clear();
            map.put(username, model);
        }
    }

    @Override
    public List<ThanhDuTXModel> getTopThanhDuDaily(final String dateStr, final int type) throws SQLException {
        final String startTime = dateStr + " 00:00:00";
        final String endTime = dateStr + " 23:59:59";
        return this.dao.getTopThanhDuDaily(startTime, endTime, (short) type);
    }

    @Override
    public List<ThanhDuTXModel> getTopThanhDuMonthly(final String dateStr, final int type) throws SQLException, ParseException {
        final String string = dateStr + "-01";
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        final Date dt = sdf.parse(string);
        final Calendar c = Calendar.getInstance();
        c.setTime(dt);
        final String startDate = sdf.format(c.getTime());
        final String startTime = startDate + " 00:00:00";
        c.add(Calendar.MONTH, 1);
        c.add(Calendar.DATE, -1);
        final String endDate = sdf.format(c.getTime());
        final String endTime = endDate + " 23:59:59";
        return this.dao.getTopThanhDuDaily(startTime, endTime, (short) type);
    }

    @Override
    public long getPotTanLoc() throws SQLException {
        final MiniGameDAOImpl miniGameDAO = new MiniGameDAOImpl();
        return miniGameDAO.getPot("tan_loc_ou");
    }

    @Override
    public void logTanLoc(final String username, final long money) throws IOException, TimeoutException, InterruptedException {
        final LogTanLocMessage message = new LogTanLocMessage();
        message.username = username;
        message.value = money;
        RMQApi.publishMessage("queue_overunder", message, 10107);
    }

    @Override
    public void updatePotTanLoc(final long newValue) throws IOException, TimeoutException, InterruptedException {
        final UpdatePotMessage message = new UpdatePotMessage();
        message.newValue = newValue;
        message.potName = "tan_loc_ou";
        RMQApi.publishMessage("queue_pot", message, 106);
    }

    @Override
    public void logRutLoc(final String username, final long prize, final int timeRequest, final long currentFund) throws IOException, TimeoutException, InterruptedException {
        final LogRutLocMessge message = new LogRutLocMessge();
        message.username = username;
        message.prize = prize;
        message.timeRequest = timeRequest;
        message.currentFund = currentFund;
        RMQApi.publishMessage("queue_overunder", message, 10108);
    }

    @Override
    public int updateLuotRutLoc(final String username, final int soLuotThem) throws IOException, TimeoutException, InterruptedException {
        final UpdateLuotRutLocMessage message = new UpdateLuotRutLocMessage();
        message.username = username;
        message.soLuotThem = soLuotThem;
        RMQApi.publishMessage("queue_overunder", message, 10109);
        int soLuotRut = soLuotThem;
        final HazelcastInstance client = HazelcastClientFactory.getInstance();
        final IMap userMap = client.getMap("cacheRutLocOU");
        if (userMap.containsKey(username)) {
            try {
                userMap.lock(username);
                final RutLocCacheModel model = (RutLocCacheModel) userMap.get(username);
                soLuotRut = model.addSoLuotRut(soLuotThem);
                userMap.put(username, model);
            } catch (Exception e) {
                this.logger.error(e);
            } finally {
                userMap.unlock(username);
            }
        } else {
            final RutLocCacheModel model = new RutLocCacheModel(soLuotThem);
            userMap.put(username, model);
        }
        return soLuotRut;
    }

    @Override
    public int getLuotRutLoc(final String username) throws SQLException {
        int soLuot = 0;
        final HazelcastInstance client = HazelcastClientFactory.getInstance();
        final IMap userMap = client.getMap("cacheRutLocOU");
        if (userMap.containsKey(username)) {
            try {
                userMap.lock(username);
                final RutLocCacheModel model = (RutLocCacheModel) userMap.get(username);
                soLuot = model.getSoLuotRut();
            } catch (Exception e) {
                this.logger.error(e);
            } finally {
                userMap.unlock(username);
            }
        } else {
            soLuot = this.dao.getSoLanRutLoc(username);
            final RutLocCacheModel model = new RutLocCacheModel(soLuot);
            userMap.put(username, model);
        }
        return soLuot;
    }

    @Override
    public List<XepHangRLTLModel> getXepHangTanLoc() {
        final HazelcastInstance client = HazelcastClientFactory.getInstance();
        final IMap topMap = client.getMap("cacheTop");
        if (topMap != null) {
            TopRLTLModel topTanLoc = (TopRLTLModel) topMap.get("TopTanLocOU");
            if (topTanLoc == null) {
                topTanLoc = new TopRLTLModel();
            }
            if (topTanLoc.getResults().size() == 0) {
                final List<XepHangRLTLModel> results = this.dao.getXepHangTanLoc();
                topTanLoc.setResults(results);
                topMap.put("TopTanLocOU", topTanLoc);
            }
            return topTanLoc.getResults();
        }
        return this.dao.getXepHangTanLoc();
    }

    @Override
    public List<VinhDanhRLTLModel> getVinhDanhTanLoc() {
        return this.dao.getVinhDanhTanLoc();
    }

    @Override
    public long getSoTienTanLoc(final String username) {
        return this.dao.getTongTienTanLoc(username);
    }

    @Override
    public List<XepHangRLTLModel> getXepHangRutLoc() {
        final HazelcastInstance client = HazelcastClientFactory.getInstance();
        final IMap topMap = client.getMap("cacheTop");
        if (topMap != null) {
            TopRLTLModel topRutLoc = (TopRLTLModel) topMap.get("TopRutLocOU");
            if (topRutLoc == null) {
                topRutLoc = new TopRLTLModel();
            }
            if (topRutLoc.getResults().size() == 0) {
                final List<XepHangRLTLModel> results = this.dao.getXepHangRutLoc();
                topRutLoc.setResults(results);
                topMap.put("TopRutLocOU", topRutLoc);
            }
            return topRutLoc.getResults();
        }
        return this.dao.getXepHangRutLoc();
    }

    @Override
    public List<VinhDanhRLTLModel> getVinhDanhRutLoc() {
        return this.dao.getVinhDanhRutLoc();
    }

    @Override
    public long getSoTienRutLoc(final String username) {
        return this.dao.getTongTienRutLoc(username);
    }

    @Override
    public boolean updatePot(final long pot, final String potName) {
        return false;
    }

    @Override
    public boolean updateFund(final long fund, final String potName) {
        return false;
    }

    private String buildLichSuPhien(final List<ResultTaiXiu> input, final int number) {
        final int end = input.size();
        final int start = (end - number > 0) ? (end - number) : 0;
        final StringBuilder builder = new StringBuilder();
        for (int i = start; i < end; ++i) {
            final ResultTaiXiu entry = input.get(i);
            builder.append(entry.dice1);
            builder.append(",");
            builder.append(entry.dice2);
            builder.append(",");
            builder.append(entry.dice3);
            builder.append(",");
        }
        if (builder.length() > 0) {
            builder.deleteCharAt(builder.length() - 1);
        }
        return builder.toString();
    }

    @Override
    public ReportMoneySystemModel getReportTXToday() {
        return this.dao.getReportTXToDay();
    }

    @Override
    public ReportMoneySystemModel getReportTX(final int range) throws SQLException {
        final ReportMoneySystemModel todayModel = this.getReportTXToday();
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        final Calendar cal = Calendar.getInstance();
        final String endDate = sdf.format(cal.getTime());
        final int date = cal.get(Calendar.DATE);
        final int month = cal.get(Calendar.MONTH);
        final int dateBefore = date % range - 1;
        cal.add(Calendar.DATE, -dateBefore);
        final String startDate = sdf.format(cal.getTime());
        final int dateAfter = range - dateBefore;
        final Calendar calTmp = Calendar.getInstance();
        calTmp.add(Calendar.DATE, dateAfter);
        String dateReset = sdf.format(calTmp.getTime());
        final int monthTmp = calTmp.get(Calendar.MONTH);
        if (monthTmp != month) {
            final SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/01");
            dateReset = sdf2.format(calTmp.getTime());
        }
        final ReportMoneySystemModel result;
        final ReportMoneySystemModel reportMoneySystemModel6;
        final ReportMoneySystemModel reportTX = reportMoneySystemModel6 = (result = this.dao.getReportTX(startDate, endDate));
        reportMoneySystemModel6.moneyWin += todayModel.moneyWin;
        final ReportMoneySystemModel reportMoneySystemModel7;
        final ReportMoneySystemModel reportMoneySystemModel = reportMoneySystemModel7 = result;
        reportMoneySystemModel7.moneyLost += todayModel.moneyLost;
        final ReportMoneySystemModel reportMoneySystemModel8;
        final ReportMoneySystemModel reportMoneySystemModel2 = reportMoneySystemModel8 = result;
        reportMoneySystemModel8.fee += todayModel.fee;
        final ReportMoneySystemModel reportMoneySystemModel9;
        final ReportMoneySystemModel reportMoneySystemModel3 = reportMoneySystemModel9 = result;
        reportMoneySystemModel9.moneyOther += todayModel.moneyOther;
        final ReportMoneySystemModel reportMoneySystemModel10;
        final ReportMoneySystemModel reportMoneySystemModel4 = reportMoneySystemModel10 = result;
        reportMoneySystemModel10.revenuePlayGame += todayModel.revenuePlayGame;
        final ReportMoneySystemModel reportMoneySystemModel11;
        final ReportMoneySystemModel reportMoneySystemModel5 = reportMoneySystemModel11 = result;
        reportMoneySystemModel11.revenue += todayModel.revenue;
        result.dateReset = dateReset;
        return result;
    }
}
