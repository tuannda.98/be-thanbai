// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.vinplay.dal.dao.GetUserIndexDAO;
import com.vinplay.dal.dao.impl.GetUserIndexDAOImpl;
import com.vinplay.dal.service.GetUserIndexService;
import org.json.JSONObject;

import java.sql.SQLException;

public class GetUserIndexServiceImpl implements GetUserIndexService {
    @Override
    public int getRegister(final String timeStart, final String timeEnd) throws SQLException {
        final GetUserIndexDAO dao = new GetUserIndexDAOImpl();
        return dao.getRegister(timeStart, timeEnd);
    }

    @Override
    public int getRecharge(final String timeStart, final String timeEnd) throws SQLException {
        final GetUserIndexDAOImpl dao = new GetUserIndexDAOImpl();
        return dao.getRecharge(timeStart, timeEnd);
    }

    @Override
    public int getSecMobile(final String timeStart, final String timeEnd) throws SQLException {
        final GetUserIndexDAOImpl dao = new GetUserIndexDAOImpl();
        return dao.getSecMobile(timeStart, timeEnd);
    }

    @Override
    public int getBoth(final String timeStart, final String timeEnd) throws SQLException {
        final GetUserIndexDAOImpl dao = new GetUserIndexDAOImpl();
        return dao.getBoth(timeStart, timeEnd);
    }

    @Override
    public JSONObject getRegisterWithPlatform(final String timeStart, final String timeEnd) throws SQLException {
        final GetUserIndexDAO dao = new GetUserIndexDAOImpl();
        return dao.getRegisterWithPlatform(timeStart, timeEnd);
    }

    @Override
    public JSONObject getRechargeWithPlatform(final String timeStart, final String timeEnd) throws SQLException {
        final GetUserIndexDAOImpl dao = new GetUserIndexDAOImpl();
        return dao.getRechargeWithPlatform(timeStart, timeEnd);
    }

    @Override
    public JSONObject getSecMobileWithPlatform(final String timeStart, final String timeEnd) throws SQLException {
        final GetUserIndexDAOImpl dao = new GetUserIndexDAOImpl();
        return dao.getSecMobileWithPlatform(timeStart, timeEnd);
    }

    @Override
    public JSONObject getBothWithPlatform(final String timeStart, final String timeEnd) throws SQLException {
        final GetUserIndexDAOImpl dao = new GetUserIndexDAOImpl();
        return dao.getBothWithPlatform(timeStart, timeEnd);
    }

    @Override
    public JSONObject getRegisterWithClient(final String timeStart, final String timeEnd) throws SQLException {
        final GetUserIndexDAO dao = new GetUserIndexDAOImpl();
        return dao.getRegisterWithClient(timeStart, timeEnd);
    }

}
