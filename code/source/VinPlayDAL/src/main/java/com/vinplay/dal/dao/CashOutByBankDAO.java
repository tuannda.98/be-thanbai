// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao;

import com.vinplay.vbee.common.response.CashOutByBankReponse;

import java.util.List;

public interface CashOutByBankDAO {
    List<CashOutByBankReponse> searchCashOutByBank(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5, final int p6, final String p7);

    int countSearchCashOutByBank(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5, final String p6);

    long moneyTotal(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5, final String p6);
}
