// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service.impl;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.vinplay.dal.dao.PokeGoDAO;
import com.vinplay.dal.dao.impl.PokeGoDaoImpl;
import com.vinplay.dal.service.PokeGoService;
import com.vinplay.vbee.common.enums.Games;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.messages.minigame.pokego.LogPokeGoMessage;
import com.vinplay.vbee.common.models.cache.TopPokeGoModel;
import com.vinplay.vbee.common.models.minigame.pokego.LSGDPokeGo;
import com.vinplay.vbee.common.models.minigame.pokego.TopPokeGo;
import com.vinplay.vbee.common.rmq.RMQApi;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeoutException;

public class PokeGoServiceImpl implements PokeGoService {
    private final PokeGoDAO dao;

    public PokeGoServiceImpl() {
        this.dao = new PokeGoDaoImpl();
    }

    @Override
    public void logPokeGo(final long referenceId, final String username, final long betValue, final String linesBetting, final String linesWin, final String prizesOnLine, final short result, final long totalPrizes, final short moneyType, final String time) throws IOException, TimeoutException, InterruptedException {
        final LogPokeGoMessage message = new LogPokeGoMessage();
        message.referenceId = referenceId;
        message.username = username;
        message.betValue = betValue;
        message.linesBetting = linesBetting;
        message.linesWin = linesWin;
        message.prizesOnLine = prizesOnLine;
        message.result = result;
        message.totalPrizes = totalPrizes;
        message.moneyType = moneyType;
        message.time = time;
        RMQApi.publishMessage("queue_pokego", message, 134);
    }

    @Override
    public int countLSDG(final String username, final int moneyType) {
        return this.dao.countLSGD(username, moneyType);
    }

    @Override
    public List<LSGDPokeGo> getLSGD(final String username, final int pageNumber, final int moneyType) {
        return this.dao.getLSGD(username, moneyType, pageNumber);
    }

    @Override
    public void addTop(final String username, final int betValue, final long totalPrizes, final int moneyType, final String time, final int result) {
        final HazelcastInstance client = HazelcastClientFactory.getInstance();
        final IMap topMap = client.getMap("cacheTop");
        TopPokeGoModel topPokeGo = (TopPokeGoModel) topMap.get(Games.POKE_GO.getName() + "_" + moneyType);
        if (topPokeGo == null) {
            topPokeGo = new TopPokeGoModel();
        }
        final TopPokeGo entry = new TopPokeGo();
        entry.un = username;
        entry.bv = betValue;
        entry.pz = totalPrizes;
        entry.ts = time;
        entry.rs = result;
        topPokeGo.put(entry);
        topMap.put(Games.POKE_GO.getName() + "_" + moneyType, topPokeGo);
    }

    @Override
    public List<TopPokeGo> getTopPokeGo(final int moneyType, final int page) {
        if (page <= 10) {
            final HazelcastInstance client = HazelcastClientFactory.getInstance();
            final IMap topMap = client.getMap("cacheTop");
            TopPokeGoModel topPokeGo = (TopPokeGoModel) topMap.get(Games.POKE_GO.getName() + "_" + moneyType);
            if (topPokeGo == null) {
                topPokeGo = new TopPokeGoModel();
            }
            if (topPokeGo.getResults().size() == 0) {
                final List<TopPokeGo> results = this.dao.getTop(moneyType, 100);
                topPokeGo.setResults(results);
                topMap.put(Games.POKE_GO.getName() + "_" + moneyType, topPokeGo);
            }
            return topPokeGo.getResults(page, 10);
        }
        return this.dao.getTopPokeGo(moneyType, page);
    }

    @Override
    public long getLastReferenceId() {
        return this.dao.getLastRefenceId();
    }
}
