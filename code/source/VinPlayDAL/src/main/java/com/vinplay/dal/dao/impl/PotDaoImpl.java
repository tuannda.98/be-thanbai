// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao.impl;

import casio.king365.core.CommonDbPool;
import com.vinplay.dal.dao.PotDao;
import com.vinplay.vbee.common.models.PotModel;
import com.vinplay.vbee.common.pools.ConnectionPool;
import com.vinplay.vbee.common.utils.UserUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PotDaoImpl implements PotDao {
    @Override
    public List<PotModel> getAll() throws SQLException {
        final ArrayList<PotModel> listModel = new ArrayList<>();
        try (final Connection conn = CommonDbPool.getInstance().getVinplayMinigameDS().getConnection();
             final PreparedStatement stm = conn.prepareStatement("SELECT * FROM hu_game_bai");
             final ResultSet rs = stm.executeQuery();) {
            PotModel model = null;
            while (rs.next()) {
                model = UserUtil.parseResultSetToPotModel(rs);
                listModel.add(model);
            }
        }
        return listModel;
    }
}
