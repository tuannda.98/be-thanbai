// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service;

import com.vinplay.vbee.common.messages.LogGameMessage;

import java.util.List;

public interface LogGameService {
    boolean saveLogGameByNickName(final LogGameMessage p0);

    boolean saveLogGameDetail(final LogGameMessage p0);

    List<LogGameMessage> searchLogGameByNickName(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5, final int p6);

    int countSearchLogGameByNickName(final String p0, final String p1, final String p2, final String p3, final String p4, final String p5);

    LogGameMessage getLogGameDetailBySessionID(final String p0, final String p1, final String p2);
}
