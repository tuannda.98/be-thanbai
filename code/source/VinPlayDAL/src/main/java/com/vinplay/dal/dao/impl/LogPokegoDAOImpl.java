// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import com.vinplay.dal.dao.LogPoKeGoDAO;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.response.PokegoResponse;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LogPokegoDAOImpl implements LogPoKeGoDAO {
    @Override
    public List<PokegoResponse> listLogPokego(final String referentId, final String userName, final String moneyType, final String betValue, final String timeStart, final String timeEnd, final int page) {
        final ArrayList<PokegoResponse> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        FindIterable iterable = null;
        final BasicDBObject obj = new BasicDBObject();
        final int numStart = (page - 1) * 50;
        final int numEnd = 50;
        if (referentId != null && !referentId.equals("")) {
            conditions.put("reference_id", Long.parseLong(referentId));
        }
        if (userName != null && !userName.equals("")) {
            conditions.put("user_name", userName);
        }
        if (betValue != null && !betValue.equals("")) {
            conditions.put("bet_value", Long.parseLong(betValue));
        }
        if (moneyType != null && !moneyType.equals("")) {
            conditions.put("money_type", Integer.parseInt(moneyType));
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("time_log", obj);
        }
        iterable = db.getCollection("log_poke_go").find(new Document(conditions)).skip(numStart).limit(50);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final PokegoResponse pokego = new PokegoResponse();
                pokego.reference_id = document.getLong("reference_id");
                pokego.user_name = document.getString("user_name");
                pokego.bet_value = document.getLong("bet_value");
                pokego.lines_betting = document.getString("lines_betting");
                pokego.lines_win = document.getString("lines_win");
                pokego.prizes_on_line = document.getString("prizes_on_line");
                pokego.prize = document.getLong("prize");
                pokego.result = document.getInteger("result");
                pokego.money_type = document.getInteger("money_type");
                pokego.time_log = document.getString("time_log");
                results.add(pokego);
            }
        });
        return results;
    }

    @Override
    public long countLogPokego(final String referentId, final String userName, final String moneyType, final String betValue, final String timeStart, final String timeEnd) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        final BasicDBObject objsort = new BasicDBObject();
        final BasicDBObject obj = new BasicDBObject();
        objsort.put("_id", -1);
        if (referentId != null && !referentId.equals("")) {
            conditions.put("reference_id", Long.parseLong(referentId));
        }
        if (userName != null && !userName.equals("")) {
            conditions.put("user_name", userName);
        }
        if (betValue != null && !betValue.equals("")) {
            conditions.put("bet_value", Long.parseLong(betValue));
        }
        if (moneyType != null && !moneyType.equals("")) {
            conditions.put("money_type", Integer.parseInt(moneyType));
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("time_log", obj);
        }
        return db.getCollection("log_poke_go").count(new Document(conditions));
    }
}
