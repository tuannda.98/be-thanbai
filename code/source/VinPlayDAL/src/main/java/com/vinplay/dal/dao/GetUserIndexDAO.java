// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao;

import org.json.JSONObject;

import java.sql.SQLException;

public interface GetUserIndexDAO {
    int getRegister(final String p0, final String p1) throws SQLException;

    int getRecharge(final String p0, final String p1) throws SQLException;

    int getSecMobile(final String p0, final String p1) throws SQLException;

    int getBoth(final String p0, final String p1) throws SQLException;

    JSONObject getRegisterWithPlatform(final String timeStart, final String timeEnd) throws SQLException;

    JSONObject getRechargeWithPlatform(final String timeStart, final String timeEnd) throws SQLException;

    JSONObject getSecMobileWithPlatform(final String timeStart, final String timeEnd) throws SQLException;

    JSONObject getBothWithPlatform(final String timeStart, final String timeEnd) throws SQLException;

    JSONObject getRegisterWithClient(final String timeStart, final String timeEnd) throws SQLException;
}
