/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.dal.dao;

import com.vinplay.dal.entities.agent.TopVippointResponse;
import com.vinplay.vbee.common.response.LogVipPointEventResponse;
import com.vinplay.vbee.common.response.UserVipPointEventResponse;

import java.sql.SQLException;
import java.util.List;

public interface VipPointEventDao {
    List<LogVipPointEventResponse> listLogVipPointEvent(String var1, String var2, String var3, String var4, String var5, int var6, String var7);

    long countLogVipPointEvent(String var1, String var2, String var3, String var4, String var5, String var6);

    long totalVipPointEvent(String var1, String var2, String var3, String var4, String var5, String var6);

    List<UserVipPointEventResponse> listuserVipPoint(String var1, String var2, String var3, String var4, String var5, int var6, String var7) throws SQLException;

    long countUserVipPoint(String var1, String var2, String var3, String var4, String var5, String var6) throws SQLException;

    List<TopVippointResponse> GetTopVippointAgency(int var1, int var2, String var3) throws SQLException;

    List<TopVippointResponse> GetTopVippointAgencyByNN(int var1, int var2, List<String> var3, String var4) throws SQLException;

    List<String> GetAgentByParent(int var1) throws SQLException;
}

