// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service;

import com.vinplay.dal.entities.minipoker.LSGDMiniPoker;
import com.vinplay.dal.entities.minipoker.VinhDanhMiniPoker;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.TimeoutException;

public interface MiniPokerService {
    long[] getPotMiniPoker() throws SQLException;

    long[] getFundMiniPoker() throws SQLException;

    void updatePotMiniPoker(final String p0, final long p1) throws IOException, TimeoutException, InterruptedException;

    void updateFundMiniPoker(final String p0, final long p1) throws IOException, TimeoutException, InterruptedException;

    void logMiniPoker(final String p0, final long p1, final short p2, final long p3, final String p4, final long p5, final long p6, final int p7) throws IOException, TimeoutException, InterruptedException;

    int countLichSuGiaoDich(final String p0, final int p1);

    List<LSGDMiniPoker> getLichSuGiaoDich(final String p0, final int p1, final int p2);

    List<VinhDanhMiniPoker> getBangVinhDanh(final int p0, final int p1);
}
