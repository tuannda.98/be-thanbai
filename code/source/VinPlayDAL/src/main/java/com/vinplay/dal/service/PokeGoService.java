// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service;

import com.vinplay.vbee.common.models.minigame.pokego.LSGDPokeGo;
import com.vinplay.vbee.common.models.minigame.pokego.TopPokeGo;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeoutException;

public interface PokeGoService {
    void logPokeGo(final long p0, final String p1, final long p2, final String p3, final String p4, final String p5, final short p6, final long p7, final short p8, final String p9) throws IOException, TimeoutException, InterruptedException;

    int countLSDG(final String p0, final int p1);

    List<LSGDPokeGo> getLSGD(final String p0, final int p1, final int p2);

    void addTop(final String p0, final int p1, final long p2, final int p3, final String p4, final int p5);

    List<TopPokeGo> getTopPokeGo(final int p0, final int p1);

    long getLastReferenceId();
}
