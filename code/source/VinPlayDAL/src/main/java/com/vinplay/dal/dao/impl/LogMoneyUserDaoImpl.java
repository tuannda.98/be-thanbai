// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.dao.impl;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.vinplay.dal.dao.LogMoneyUserDao;
import com.vinplay.dal.entities.gamebai.TopGameBaiModel;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.messages.gamebai.LogNoHuGameBaiMessage;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.pools.ConnectionPool;
import com.vinplay.vbee.common.response.LogMoneyUserResponse;
import com.vinplay.vbee.common.response.LogUserMoneyResponse;
import com.vinplay.vbee.common.statics.Consts;
import com.vinplay.vbee.common.utils.DateTimeUtils;
import com.vinplay.vbee.common.utils.UserUtil;
import org.bson.Document;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class LogMoneyUserDaoImpl implements LogMoneyUserDao {

    @Override
    public List<LogUserMoneyResponse> getUserHistory(String nickName, String timeStart, String timeEnd){
        final ArrayList<LogUserMoneyResponse> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        final BasicDBObject obj = new BasicDBObject();
        // final int numStart = (page - 1) * totalRecord;
        conditions.put("nick_name", nickName);
        if(timeStart != null && !timeStart.equals(""))
            obj.put("$gte", timeStart);
        if(timeEnd != null && !timeEnd.equals(""))
            obj.put("$lte", timeEnd);
        conditions.put("trans_time", obj);
        Document sort = new Document();
        sort.put("trans_time", -1);
        // FindIterable iterable = db.getCollection("log_money_user_vin").find(new Document(conditions)).skip(numStart).limit(totalRecord).sort(sort);
        FindIterable iterable = db.getCollection("log_money_user_vin").find(new Document(conditions)).sort(sort);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final LogUserMoneyResponse tranlogmoney = new LogUserMoneyResponse();
                tranlogmoney.nickName = document.getString("nick_name");
                tranlogmoney.serviceName = document.getString("service_name");
                tranlogmoney.currentMoney = document.getLong("current_money");
                tranlogmoney.moneyExchange = document.getLong("money_exchange");
                tranlogmoney.description = document.getString("description");
                tranlogmoney.transactionTime = document.getString("trans_time");
                tranlogmoney.actionName = document.getString("action_name");
                tranlogmoney.fee = document.getLong("fee");
                results.add(tranlogmoney);
            }
        });
        return results;
    }

    @Override
    public List<LogUserMoneyResponse> searchLogMoneyUser(final String nickName, final String userName, final String moneyType, final String serviceName, final String actionName, final String timeStart, final String timeEnd, final int page, final int like1, final int totalRecord) {
        final ArrayList<LogUserMoneyResponse> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        FindIterable iterable = null;
        final BasicDBObject obj = new BasicDBObject();
        final int numStart = (page - 1) * totalRecord;
        if (nickName != null && !nickName.equals("")) {
            conditions.put("nick_name", nickName);
        }
        if (userName != null && !userName.equals("")) {
            conditions.put("user_name", userName);
        }
        if (actionName != null && !actionName.equals("")) {
            conditions.put("action_name", actionName);
        }
        if (serviceName != null && !serviceName.equals("")) {
            conditions.put("service_name", serviceName);
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("trans_time", obj);
        }
        if (moneyType.equals("vin")) {
            iterable = db.getCollection("log_money_user_vin").find(new Document(conditions)).skip(numStart).limit(totalRecord);
        } else if (moneyType.equals("xu")) {
            final BasicDBObject objsort = new BasicDBObject();
            objsort.put("_id", -1);
            iterable = db.getCollection("log_money_user_xu").find(new Document(conditions)).sort(objsort).skip(numStart).limit(totalRecord);
        }
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final LogUserMoneyResponse tranlogmoney = new LogUserMoneyResponse();
                tranlogmoney.nickName = document.getString("nick_name");
                tranlogmoney.serviceName = document.getString("service_name");
                tranlogmoney.currentMoney = document.getLong("current_money");
                tranlogmoney.moneyExchange = document.getLong("money_exchange");
                tranlogmoney.description = document.getString("description");
                tranlogmoney.transactionTime = document.getString("trans_time");
                tranlogmoney.actionName = document.getString("action_name");
                tranlogmoney.fee = document.getLong("fee");
                results.add(tranlogmoney);
            }
        });
        return results;
    }

    @Override
    public List<LogUserMoneyResponse> searchLogMoneyUserDes(final String nickName, final String userName, final String moneyType, final String serviceName, final String actionName, final String timeStart, final String timeEnd, final int page, final int like1, final int totalRecord, String descColumn) {
        final ArrayList<LogUserMoneyResponse> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        FindIterable iterable = null;
        final BasicDBObject obj = new BasicDBObject();
        final int numStart = (page - 1) * totalRecord;
        if (nickName != null && !nickName.equals("")) {
            conditions.put("nick_name", nickName);
        }
        if (userName != null && !userName.equals("")) {
            conditions.put("user_name", userName);
        }
        if (actionName != null && !actionName.equals("")) {
            conditions.put("action_name", actionName);
        }
        if (serviceName != null && !serviceName.equals("")) {
            conditions.put("service_name", serviceName);
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("trans_time", obj);
        }
        final BasicDBObject objsort = new BasicDBObject();
        objsort.put(descColumn, -1);
        if (moneyType.equals("vin")) {
            iterable = db.getCollection("log_money_user_vin").find(new Document(conditions)).sort(objsort).skip(numStart).limit(totalRecord);
        } else if (moneyType.equals("xu")) {
            objsort.put("_id", -1);
            iterable = db.getCollection("log_money_user_xu").find(new Document(conditions)).sort(objsort).skip(numStart).limit(totalRecord);
        }
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final LogUserMoneyResponse tranlogmoney = new LogUserMoneyResponse();
                tranlogmoney.nickName = document.getString("nick_name");
                tranlogmoney.serviceName = document.getString("service_name");
                tranlogmoney.currentMoney = document.getLong("current_money");
                tranlogmoney.moneyExchange = document.getLong("money_exchange");
                tranlogmoney.description = document.getString("description");
                tranlogmoney.transactionTime = document.getString("trans_time");
                tranlogmoney.actionName = document.getString("action_name");
                tranlogmoney.fee = document.getLong("fee");
                results.add(tranlogmoney);
            }
        });
        return results;
    }

    @Override
    public LogUserMoneyResponse searchLastLogMoneyUser(final String nick_name, final String type, final ArrayList<String> agents) {
        final ArrayList<LogUserMoneyResponse> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        FindIterable iterable = null;
        final BasicDBObject obj = new BasicDBObject();
        if (nick_name != null && !nick_name.equals("")) {
            conditions.put("nick_name", nick_name);
        }
        conditions.put("action_name", "TransferMoney");
        final BasicDBObject objsort = new BasicDBObject();
        objsort.put("create_time", -1);
        final BasicDBObject objMoney = new BasicDBObject();
        final BasicDBObject objAgent = new BasicDBObject();
        if ("TRANSFER_USER".equals(type)) {
            objMoney.put("$lt", 0);
            conditions.put("money_exchange", objMoney);
        } else if ("TRANSFER_AGENT".equals(type)) {
            objMoney.put("$lt", 0);
            conditions.put("money_exchange", objMoney);
        } else if ("RECEIVE_AGENT".equals(type)) {
            objMoney.put("$gt", 0);
            conditions.put("money_exchange", objMoney);
        } else if ("RECEIVE_USER".equals(type)) {
            objMoney.put("$gt", 0);
            conditions.put("money_exchange", objMoney);
        }
        iterable = db.getCollection("log_money_user_xu").find(new Document(conditions)).sort(objsort).skip(0).limit(100);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final LogUserMoneyResponse tranlogmoney = new LogUserMoneyResponse();
                tranlogmoney.nickName = document.getString("nick_name");
                tranlogmoney.serviceName = document.getString("service_name");
                tranlogmoney.currentMoney = document.getLong("current_money");
                tranlogmoney.moneyExchange = document.getLong("money_exchange");
                tranlogmoney.description = document.getString("description");
                tranlogmoney.transactionTime = document.getString("trans_time");
                tranlogmoney.actionName = document.getString("action_name");
                tranlogmoney.fee = document.getLong("fee");
                tranlogmoney.createdTime = document.getDate("create_time").getTime();
                results.add(tranlogmoney);
            }
        });
        if (results != null && results.size() > 0) {
            LogUserMoneyResponse resp = null;
            for (final LogUserMoneyResponse response : results) {
                if ("TRANSFER_USER".equals(type) || "RECEIVE_USER".equals(type)) {
                    boolean match = false;
                    for (final String s : agents) {
                        if (response.description.contains(s)) {
                            match = true;
                            break;
                        }
                    }
                    if (!match) {
                        resp = response;
                        break;
                    }
                } else {
                    boolean match = false;
                    for (final String s : agents) {
                        if (response.description.contains(s)) {
                            match = true;
                            break;
                        }
                    }
                    if (match) {
                        resp = response;
                        break;
                    }
                }
            }
            return resp;
        }
        return null;
    }

    @Override
    public List<LogUserMoneyResponse> searchAllLogMoneyUser(final String nick_name, final String type, final boolean seven_days) {
        final ArrayList<LogUserMoneyResponse> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        FindIterable iterable = null;
        if (nick_name != null && !nick_name.equals("")) {
            conditions.put("nick_name", nick_name);
        }
        if (seven_days) {
            final Date currentDate = new Date();
            final Calendar c = Calendar.getInstance();
            c.setTime(currentDate);
            c.add(Calendar.DATE, -7);
            final Date currentDatePlusSeven = c.getTime();
            final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            final String strDate = dateFormat.format(currentDatePlusSeven);
            final BasicDBObject obj = new BasicDBObject();
            obj.put("$gte", strDate);
            conditions.put("trans_time", obj);
        }
        final BasicDBObject objsort = new BasicDBObject();
        objsort.put("create_time", -1);
        if ("TRANSFER".equals(type)) {
            conditions.put("action_name", "TransferMoney");
            iterable = db.getCollection("log_money_user_tieu_vin").find(new Document(conditions)).sort(objsort);
        } else if ("GIFTCODE".equals(type)) {
            final String pattern = ".*GiftCode.*";
            conditions.put("action_name", new BasicDBObject().append("$regex", pattern).append("$options", "i"));
            iterable = db.getCollection("log_money_user_nap_vin").find(new Document(conditions)).sort(objsort);
        } else if ("CARD".equals(type)) {
            conditions.put("action_name", "RechargeByCard");
            iterable = db.getCollection("log_money_user_nap_vin").find(new Document(conditions)).sort(objsort);
        } else {
            conditions.put("action_name", "TransferMoney");
            iterable = db.getCollection("log_money_user_nap_vin").find(new Document(conditions)).sort(objsort);
        }
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final LogUserMoneyResponse tranlogmoney = new LogUserMoneyResponse();
                tranlogmoney.nickName = document.getString("nick_name");
                tranlogmoney.serviceName = document.getString("service_name");
                tranlogmoney.currentMoney = document.getLong("current_money");
                tranlogmoney.moneyExchange = document.getLong("money_exchange");
                tranlogmoney.description = document.getString("description");
                tranlogmoney.transactionTime = document.getString("trans_time");
                tranlogmoney.actionName = document.getString("action_name");
                tranlogmoney.fee = document.getLong("fee");
                if (document.getDate("create_time") != null) {
                    tranlogmoney.createdTime = document.getDate("create_time").getTime() + 25200000L;
                }
                results.add(tranlogmoney);
            }
        });
        return results;
    }

    @Override
    public long getTotalBetWin(final String nick_name, final String type, final String action_name) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        final List<String> slots = new ArrayList<>();
        slots.add("KhoBau");
        slots.add("NuDiepVien");
        slots.add("SieuAnhHung");
        slots.add("VuongQuocVin");
        final List<BasicDBObject> pipeline = new ArrayList<>();
        if ("BET".equals(type)) {
            if (action_name != null && !"".equals(action_name)) {
                if ("SLOT".equals(action_name)) {
                    final BasicDBObject match = new BasicDBObject("$match", new BasicDBObject("is_bot", false).append("nick_name", nick_name).append("action_name", new BasicDBObject("$in", slots)).append("play_game", true).append("money_exchange", new BasicDBObject("$lt", 0)));
                    pipeline.add(match);
                } else {
                    final BasicDBObject match = new BasicDBObject("$match", new BasicDBObject("is_bot", false).append("nick_name", nick_name).append("action_name", action_name).append("play_game", true).append("money_exchange", new BasicDBObject("$lt", 0)));
                    pipeline.add(match);
                }
            } else {
                final BasicDBObject match = new BasicDBObject("$match", new BasicDBObject("is_bot", false).append("nick_name", nick_name).append("play_game", true).append("money_exchange", new BasicDBObject("$lt", 0)));
                pipeline.add(match);
            }
        } else if (action_name != null && !"".equals(action_name)) {
            if ("SLOT".equals(action_name)) {
                final BasicDBObject match = new BasicDBObject("$match", new BasicDBObject("is_bot", false).append("nick_name", nick_name).append("action_name", new BasicDBObject("$in", slots)).append("play_game", true).append("money_exchange", new BasicDBObject("$gt", 0)));
                pipeline.add(match);
            } else {
                final BasicDBObject match = new BasicDBObject("$match", new BasicDBObject("is_bot", false).append("nick_name", nick_name).append("action_name", action_name).append("play_game", true).append("money_exchange", new BasicDBObject("$gt", 0)));
                pipeline.add(match);
            }
        } else {
            final BasicDBObject match = new BasicDBObject("$match", new BasicDBObject("is_bot", false).append("nick_name", nick_name).append("play_game", true).append("money_exchange", new BasicDBObject("$gt", 0)));
            pipeline.add(match);
        }
        final BasicDBObject group = new BasicDBObject("$group", new BasicDBObject("_id", null).append("total", new BasicDBObject("$sum", "$money_exchange")));
        pipeline.add(group);
        final AggregateIterable<Document> output = db.getCollection("log_money_user_vin").aggregate(pipeline);
        final List<Document> results = new ArrayList<>();
        output.forEach(new Block<Document>() {
            public void apply(final Document document) {
                results.add(document);
            }
        });
        if (results != null && results.size() > 0) {
            return results.get(0).getLong("total");
        }
        return 0L;
    }

    @Override
    public int countsearchLogMoneyUser2(final String nickName, final String userName, final String moneyType, final String serviceName, final String actionName, final String timeStart, final String timeEnd, final int like1) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        final BasicDBObject obj = new BasicDBObject();
        if (nickName != null && !nickName.equals("")) {
            conditions.put("nick_name", nickName);
        }
        if (userName != null && !userName.equals("")) {
            conditions.put("user_name", userName);
        }
        if (actionName != null && !actionName.equals("")) {
            conditions.put("action_name", actionName);
        }
        if (serviceName != null && !serviceName.equals("")) {
            conditions.put("service_name", serviceName);
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("trans_time", obj);
        }
        int totaRecord = 0;
        if (moneyType.equals("vin")) {
            totaRecord = (int) db.getCollection("log_money_user_vin").count(new Document(conditions));
        }
        if (moneyType.equals("xu")) {
            totaRecord = (int) db.getCollection("log_money_user_xu").count(new Document(conditions));
        }
        return totaRecord;
    }

    @Override
    public int countsearchLogMoneyUser(final String nickName, final String moneyType, final String serviceName, final String actionName, final String timeStart, final String timeEnd, final int like) {
        int countRecord = 0;
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        if (nickName != null && !nickName.equals("")) {
            if (like == 0) {
                conditions.put("nick_name", nickName);
            } else {
                conditions.put("nick_name", nickName);
            }
        }
        if (actionName != null && !actionName.equals("")) {
            conditions.put("action_name", actionName);
        }
        if (serviceName != null && !serviceName.equals("")) {
            conditions.put("service_name", serviceName);
        }
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("trans_time", obj);
        }
        if (moneyType.equals("vin")) {
            countRecord = (int) db.getCollection("log_money_user_vin").count(new Document(conditions));
        } else if (moneyType.equals("xu")) {
            countRecord = (int) db.getCollection("log_money_user_xu").count(new Document(conditions));
        }
        return countRecord;
    }

    @Override
    public List<LogMoneyUserResponse> getHistoryTransactionLogMoney(final String nickName, final int moneyType, final int page) {
        final int numStart = (page - 1) * 13;
        final int numEnd = 13;
        return this.getTransactionList(nickName, moneyType, numStart, 13);
    }

    private Map<String, Object> buildConditionNapVin(final String nickName) {
        final HashMap<String, Object> conditions = new HashMap<>();
        conditions.put("nick_name", nickName);
        return conditions;
    }

    private Map<String, Object> buildConditionTieuVin(final String nickName) {
        final HashMap<String, Object> conditions = new HashMap<>();
        conditions.put("nick_name", nickName);
        return conditions;
    }

    private Map<String, Object> buildConditionTransaction(final String nickName, final int moneyType) {
        final HashMap<String, Object> conditions = new HashMap<>();
        conditions.put("nick_name", nickName);
        final ArrayList<BasicDBObject> lstServerName = new ArrayList<>();
        switch (moneyType) {
            case 4: {
                for (final String action : Consts.NAP_XU) {
                    final BasicDBObject query = new BasicDBObject("action_name", action);
                    lstServerName.add(query);
                }
                conditions.put("$or", lstServerName);
                conditions.put("money_exchange", new BasicDBObject("$gt", 0));
                break;
            }
        }
        return conditions;
    }

    @Override
    public int countHistoryTransactionLogMoney(final String nickName, final int queryType) {
        int countRecord = 0;
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final Map<String, Object> conditions = this.buildConditionTransaction(nickName, queryType);
        final Map<String, Object> conditionNapVin = this.buildConditionNapVin(nickName);
        final Map<String, Object> conditionTieuVin = this.buildConditionTieuVin(nickName);
        switch (queryType) {
            case 1: {
                countRecord = (int) db.getCollection("log_money_user_vin").count(new Document(conditions));
                break;
            }
            case 3: {
                countRecord = (int) db.getCollection("log_money_user_nap_vin").count(new Document(conditionNapVin));
                break;
            }
            case 5: {
                countRecord = (int) db.getCollection("log_money_user_tieu_vin").count(new Document(conditionTieuVin));
                break;
            }
            default: {
                countRecord = (int) db.getCollection("log_money_user_xu").count(new Document(conditions));
                break;
            }
        }
        return countRecord;
    }

    @Override
    public List<LogMoneyUserResponse> getTransactionList(final String nickName, final int queryType, final int start, final int end) {
        final ArrayList<LogMoneyUserResponse> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final Map<String, Object> conditions = this.buildConditionTransaction(nickName, queryType);
        final Map<String, Object> conditionNapVin = this.buildConditionNapVin(nickName);
        final Map<String, Object> conditionTieuVin = this.buildConditionTieuVin(nickName);
        FindIterable iterable = null;
        final BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        switch (queryType) {
            case 1: {
                iterable = db.getCollection("log_money_user_vin").find(new Document(conditions)).sort(objsort).skip(start).limit(end);
                break;
            }
            case 3: {
                iterable = db.getCollection("log_money_user_nap_vin").find(new Document(conditionNapVin)).sort(objsort).skip(start).limit(end);
                break;
            }
            case 5: {
                iterable = db.getCollection("log_money_user_tieu_vin").find(new Document(conditionTieuVin)).sort(objsort).skip(start).limit(end);
                break;
            }
            default: {
                iterable = db.getCollection("log_money_user_xu").find(new Document(conditions)).sort(objsort).skip(start).limit(end);
                break;
            }
        }
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final LogMoneyUserResponse tranlogmoney = new LogMoneyUserResponse();
                tranlogmoney.transId = document.getLong("trans_id");
                tranlogmoney.serviceName = document.getString("service_name");
                tranlogmoney.currentMoney = document.getLong("current_money");
                tranlogmoney.moneyExchange = document.getLong("money_exchange");
                tranlogmoney.description = document.getString("description");
                tranlogmoney.transactionTime = document.getString("trans_time");
                results.add(tranlogmoney);
            }
        });
        return results;
    }

    @Override
    public Map<String, TopGameBaiModel> getTopGameBai(final String gameName) {
        final HashMap<String, TopGameBaiModel> result = new HashMap<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final MongoCollection collection = db.getCollection("log_money_user_xu");
        final Document conditionWin = new Document();
        conditionWin.put("action_name", gameName);
        conditionWin.put("money_exchange", new BasicDBObject("$gt", 0));
        final Document conditionWinWeek = new Document();
        conditionWinWeek.put("action_name", gameName);
        conditionWinWeek.put("money_exchange", new BasicDBObject("$gt", 0));
        final String startTime = "2016-10-17 00;00;00";
        final BasicDBObject obj = new BasicDBObject();
        obj.put("$gte", "2016-10-17 00;00;00");
        conditionWinWeek.put("trans_time", obj);
        final AggregateIterable iterableWin = collection.aggregate(Arrays.asList(new Document("$match", conditionWin), new Document("$group", new Document("_id", "$nick_name").append("money", new Document("$sum", "$money_exchange")).append("count", new Document("$sum", 1)))));
        final ArrayList<TopGameBaiModel> resultWin = new ArrayList<>();
        iterableWin.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final TopGameBaiModel top = new TopGameBaiModel();
                top.setNickName(document.getString("_id"));
                top.setWinCount(document.getInteger("count"));
                top.setMoneyWin(document.getLong("money"));
                resultWin.add(top);
            }
        });
        final AggregateIterable iterableWinWeek = collection.aggregate(Arrays.asList(new Document("$match", conditionWinWeek), new Document("$group", new Document("_id", "$nick_name").append("money", new Document("$sum", "$money_exchange")).append("count", new Document("$sum", 1)))));
        final ArrayList<TopGameBaiModel> resultWinWeek = new ArrayList<>();
        iterableWinWeek.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final TopGameBaiModel top = new TopGameBaiModel();
                top.setNickName(document.getString("_id"));
                top.setWinCountThisWeek(document.getInteger("count"));
                top.setMoneyWinThisWeek(document.getLong("money"));
                resultWinWeek.add(top);
            }
        });
        final Document conditionLost = new Document();
        conditionLost.put("action_name", gameName);
        conditionLost.put("money_exchange", new BasicDBObject("$lt", 0));
        final Document conditionLostWeek = new Document();
        conditionLostWeek.put("action_name", gameName);
        conditionLostWeek.put("money_exchange", new BasicDBObject("$lt", 0));
        conditionLostWeek.put("trans_time", obj);
        final AggregateIterable iterableLost = collection.aggregate(Arrays.asList(new Document("$match", conditionLost), new Document("$group", new Document("_id", "$nick_name").append("money", new Document("$sum", "$money_exchange")).append("count", new Document("$sum", 1)))));
        final ArrayList<TopGameBaiModel> resultLost = new ArrayList<>();
        iterableLost.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final TopGameBaiModel top = new TopGameBaiModel();
                top.setNickName(document.getString("_id"));
                top.setLostCount(document.getInteger("count"));
                top.setMoneyLost(document.getLong("money"));
                resultLost.add(top);
            }
        });
        final AggregateIterable iterablLostWeek = collection.aggregate(Arrays.asList(new Document("$match", conditionLostWeek), new Document("$group", new Document("_id", "$nick_name").append("money", new Document("$sum", "$money_exchange")).append("count", new Document("$sum", 1)))));
        final ArrayList<TopGameBaiModel> resultLostWeek = new ArrayList<>();
        iterablLostWeek.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final TopGameBaiModel top = new TopGameBaiModel();
                top.setNickName(document.getString("_id"));
                top.setLostCountThisWeek(document.getInteger("count"));
                top.setMoneyLostThisWeek(document.getLong("money"));
                resultLostWeek.add(top);
            }
        });
        for (final TopGameBaiModel top : resultWin) {
            top.setWinCountThisMonth(top.getWinCount());
            top.setWinCountThisYear(top.getWinCount());
            top.setMoneyWinThisMonth(top.getMoneyWin());
            top.setMoneyWinThisYear(top.getMoneyWin());
            result.put(top.getNickName(), top);
        }
        for (final TopGameBaiModel top : resultWinWeek) {
            if (!result.containsKey(top.getNickName())) {
                continue;
            }
            final TopGameBaiModel model = result.get(top.getNickName());
            model.setWinCountThisWeek(top.getWinCountThisWeek());
            model.setMoneyWinThisWeek(top.getMoneyWinThisWeek());
            result.put(top.getNickName(), model);
        }
        for (final TopGameBaiModel top : resultLost) {
            if (result.containsKey(top.getNickName())) {
                final TopGameBaiModel model = result.get(top.getNickName());
                model.setLostCount(top.getLostCount());
                model.setLostCountThisMonth(top.getLostCount());
                model.setLostCountThisYear(top.getLostCount());
                model.setMoneyLost(top.getMoneyLost());
                model.setMoneyLostThisMonth(top.getMoneyLost());
                model.setMoneyLostThisYear(top.getMoneyLost());
                result.put(top.getNickName(), model);
            } else {
                top.setLostCountThisMonth(top.getLostCount());
                top.setLostCountThisYear(top.getLostCount());
                top.setMoneyLostThisMonth(top.getMoneyLost());
                top.setMoneyLostThisYear(top.getMoneyLost());
                result.put(top.getNickName(), top);
            }
        }
        for (final TopGameBaiModel top : resultLostWeek) {
            if (!result.containsKey(top.getNickName())) {
                continue;
            }
            final TopGameBaiModel model = result.get(top.getNickName());
            model.setLostCountThisWeek(top.getLostCountThisWeek());
            model.setMoneyLostThisWeek(top.getMoneyLostThisWeek());
            result.put(top.getNickName(), model);
        }
        return result;
    }

    @Override
    public List<LogNoHuGameBaiMessage> getNoHuGameBaiHistory(final int pageNumber, final String gameName) {
        final int pageSize = 10;
        final int skipNumber = (pageNumber - 1) * 10;
        final ArrayList<LogNoHuGameBaiMessage> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        FindIterable iterable = null;
        final Document conditions = new Document();
        if (gameName != null && !gameName.isEmpty()) {
            conditions.put("game_name", gameName);
        } else {
            conditions.put("game_name", new Document("$ne", "PokerTour"));
        }
        final BasicDBObject sortCondtions = new BasicDBObject();
        sortCondtions.put("_id", -1);
        iterable = db.getCollection("log_no_hu_game_bai").find(conditions).sort(sortCondtions).skip(skipNumber).limit(10);
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final LogNoHuGameBaiMessage entry = new LogNoHuGameBaiMessage(document.getString("nick_name"), document.getInteger("room"), document.getLong("pot_value"), document.getLong("money_win"), document.getString("game_name"), document.getString("description"), document.getString("tour_id"));
                entry.setCreateTime(document.getString("trans_time"));
                results.add(entry);
            }
        });
        return results;
    }

    @Override
    public int countNoHuGameBaiHistory() {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final Document conditions = new Document();
        final long totalRows = db.getCollection("log_no_hu_game_bai").count(conditions);
        return (int) totalRows;
    }

    @Override
    public UserModel getUserByNickName(final String nickname) throws SQLException {
        UserModel user = null;
        try (final Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             final PreparedStatement stm = conn.prepareStatement("SELECT * FROM users WHERE nick_name=?");) {
            stm.setString(1, nickname);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    user = UserUtil.parseResultSetToUserModel(rs);
                }
            }
        }
        return user;
    }

    @Override
    public List<LogUserMoneyResponse> searchLogMoneyTranferUser(final String nickName, final String timeStart, final String timeEnd, final String type, final int page) {
        final ArrayList<LogUserMoneyResponse> results = new ArrayList<>();
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final HashMap<String, Object> conditions = new HashMap<>();
        FindIterable iterable = null;
        final BasicDBObject obj = new BasicDBObject();
        final BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        final int numStart = (page - 1) * 100;
        final int numEnd = 100;
        conditions.put("action_name", "TransferMoney");
        if (!nickName.isEmpty()) {
            conditions.put("nick_name", nickName);
        }
        if (!timeStart.isEmpty() && !timeEnd.isEmpty()) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("trans_time", obj);
        }
        if (type.equals("1")) {
            iterable = db.getCollection("log_money_user_tieu_vin").find(new Document(conditions)).sort(objsort).skip(numStart).limit(100);
        } else if (type.equals("2")) {
            iterable = db.getCollection("log_money_user_nap_vin").find(new Document(conditions)).sort(objsort).skip(numStart).limit(100);
        }
        iterable.forEach(new Block<Document>() {
            public void apply(final Document document) {
                final LogUserMoneyResponse tranlogmoney = new LogUserMoneyResponse();
                tranlogmoney.nickName = document.getString("nick_name");
                final HazelcastInstance client = HazelcastClientFactory.getInstance();
                final IMap<String, UserModel> userMap = client.getMap("users");
                if (userMap.containsKey(tranlogmoney.nickName)) {
                    try {
                        userMap.lock(tranlogmoney.nickName);
                        final UserCacheModel user = (UserCacheModel) userMap.get(tranlogmoney.nickName);
                        tranlogmoney.userName = user.getUsername();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    } finally {
                        userMap.unlock(tranlogmoney.nickName);
                    }
                } else {
                    final UserDaoImpl dao = new UserDaoImpl();
                    try {
                        final UserModel user2 = dao.getUserByNickName(tranlogmoney.nickName);
                        tranlogmoney.userName = user2.getUsername();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
                tranlogmoney.serviceName = document.getString("service_name");
                tranlogmoney.currentMoney = document.getLong("current_money");
                tranlogmoney.moneyExchange = document.getLong("money_exchange");
                tranlogmoney.description = document.getString("description");
                tranlogmoney.transactionTime = document.getString("trans_time");
                tranlogmoney.actionName = document.getString("action_name");
                tranlogmoney.fee = document.getLong("fee");
                results.add(tranlogmoney);
            }
        });
        return results;
    }

    @Override
    public boolean UpdateProcessLogChuyenTienDaiLy(final String nickNameSend, final String nickNameReceive, final String timeLog, final String Status) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final MongoCollection col = db.getCollection("log_chuyen_tien_dai_ly");
        final HashMap<String, Object> conditions = new HashMap<>();
        conditions.put("trans_time", timeLog);
        conditions.put("nick_name_send", nickNameSend);
        conditions.put("nick_name_receive", nickNameReceive);
        col.updateOne(new Document(conditions), new Document("$set", new Document("process", Integer.parseInt(Status))));
        return true;
    }

    @Override
    public boolean UpdateProcessLogChuyenTienDaiLyMySQL(final String nickNameSend, final String nickNameReceive, final String timeLog, final String Status) throws SQLException {
        try (final Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stmt = conn.prepareStatement(" UPDATE vinplay.log_tranfer_agent  SET process = ?,      update_time = ?  WHERE trans_time = ?        AND nick_name_send = ?        AND nick_name_receive = ? ");) {
            stmt.setInt(1, Integer.parseInt(Status));
            stmt.setString(2, DateTimeUtils.getCurrentTime("yyyy-MM-dd HH:mm:ss"));
            stmt.setString(3, timeLog);
            stmt.setString(4, nickNameSend);
            stmt.setString(5, nickNameReceive);
            stmt.executeUpdate();
        }
        return true;
    }
}
