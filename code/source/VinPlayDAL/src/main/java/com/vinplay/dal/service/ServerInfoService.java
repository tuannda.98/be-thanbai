// 
// Decompiled by Procyon v0.5.36
// 

package com.vinplay.dal.service;

import com.vinplay.vbee.common.models.LogCCUModel;

import java.util.List;

public interface ServerInfoService {
    void logCCU(final int p0, final int p1, final int p2, final int p3, final int p4, final int p5, final int p6, final int p7);

    List<LogCCUModel> getLogCCU(final String p0, final String p1);
}
