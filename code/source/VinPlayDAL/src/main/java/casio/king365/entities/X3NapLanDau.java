package casio.king365.entities;

import org.bson.Document;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class X3NapLanDau  implements Serializable {

    private static final long serialVersionUID = 1L;

    Integer userId;
    String nickname;
    String channel;     // Kênh nạp
    Long startTime;     // Ngày nạp lần đầu
    Long endTime;       // 20 ngày sau ngày nạp lần đầu
    Long chargeAmount;   // Giá trị lần nạp đầu
    Long bonusAmount;    // Giá trị thưởng lần nạp đầu (chargeAmount * 2)
    Double percent;      // % hoàn thành tiến trình, đủ 100% thì dc nhận thưởng;
    Boolean receivedBonus;   // User đã nhận thưởng chưa?
    Long startActiveTime;   // Thời gian mà tiến trình này bắt đầu được khởi động

    public X3NapLanDau() { }

    public Map<String, Object> toMap(){
        Map<String, Object> mapData = new HashMap<>();
        mapData.put("userId", getUserId());
        mapData.put("nickname", getNickname());
        mapData.put("startTime", getStartTime());
        mapData.put("endTime", getEndTime());
        mapData.put("chargeAmount", getChargeAmount());
        mapData.put("bonusAmount", getBonusAmount());
        mapData.put("percent", getPercent());
        mapData.put("channel", getChannel());
        mapData.put("receivedBonus", getReceivedBonus());
        mapData.put("startActiveTime", getStartActiveTime());
        return mapData;
    }

    public static X3NapLanDau fromDocument(Document doc){
        X3NapLanDau x3NapLanDau = new X3NapLanDau();
        x3NapLanDau.setUserId(doc.getInteger("userId"));
        x3NapLanDau.setNickname(doc.getString("nickname"));
        x3NapLanDau.setStartTime(doc.getLong("startTime"));
        try {
            x3NapLanDau.setEndTime(doc.getLong("endTime"));
        } catch (ClassCastException e){
            x3NapLanDau.setEndTime(doc.getInteger("endTime").longValue());
        }
        x3NapLanDau.setChargeAmount(doc.getLong("chargeAmount"));
        x3NapLanDau.setBonusAmount(doc.getLong("bonusAmount"));
        x3NapLanDau.setPercent(doc.getDouble("percent"));
        x3NapLanDau.setChannel(doc.getString("channel"));
        if(doc.containsKey("receivedBonus"))
            x3NapLanDau.setReceivedBonus(doc.getBoolean("receivedBonus"));
        else
            x3NapLanDau.setReceivedBonus(false);
        if(doc.containsKey("startActiveTime"))
            x3NapLanDau.setStartActiveTime(doc.getLong("startActiveTime"));
        else
            x3NapLanDau.setStartActiveTime(doc.getLong("startTime"));
        return x3NapLanDau;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Long getChargeAmount() {
        return chargeAmount;
    }

    public void setChargeAmount(Long chargeAmount) {
        this.chargeAmount = chargeAmount;
    }

    public Long getBonusAmount() {
        return bonusAmount;
    }

    public void setBonusAmount(Long bonusAmount) {
        this.bonusAmount = bonusAmount;
    }

    public Double getPercent() {
        return percent;
    }

    public void setPercent(Double percent) {
        this.percent = percent;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Boolean getReceivedBonus() {
        return receivedBonus;
    }

    public void setReceivedBonus(Boolean receivedBonus) {
        this.receivedBonus = receivedBonus;
    }

    public Long getStartActiveTime() {
        return startActiveTime;
    }

    public void setStartActiveTime(Long startActiveTime) {
        this.startActiveTime = startActiveTime;
    }

    @Override
    public String toString() {
        return "X3NapLanDau{" +
                "userId=" + userId +
                ", nickname='" + nickname + '\'' +
                ", channel='" + channel + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", chargeAmount=" + chargeAmount +
                ", bonusAmount=" + bonusAmount +
                ", percent=" + percent +
                ", receivedBonus=" + receivedBonus +
                ", startActiveTime=" + startActiveTime +
                '}';
    }
}
