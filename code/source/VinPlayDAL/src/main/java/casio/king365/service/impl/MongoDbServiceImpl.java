package casio.king365.service.impl;

import casio.king365.service.MongoDbService;
import casio.king365.util.KingUtil;
import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.Function;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.FindOneAndUpdateOptions;
import com.mongodb.client.model.UpdateOptions;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import org.apache.log4j.Logger;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MongoDbServiceImpl implements MongoDbService {
    private static final Logger logger = Logger.getLogger("api");

    @Override
    public void insert(String collectionName, Map<String, Object> map) {
        try {
            MongoDatabase db = MongoDBConnectionFactory.getDB();
            MongoCollection<Document> col = db.getCollection(collectionName);
            Document doc = new Document();
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                doc.append(entry.getKey(), entry.getValue());
            }
            col.insertOne(doc);
        } catch (Exception e) {
            logger.debug(e.getMessage());
        }
    }

    @Override
    public void insert(String collectionName, List<Document> docs) {
        try {
            MongoDatabase db = MongoDBConnectionFactory.getDB();
            MongoCollection<Document> col = db.getCollection(collectionName);
            col.insertMany(docs);
        } catch (Exception e) {
            logger.debug(e.getMessage());
        }
    }

    @Override
    public <T> List<T> find(String collectionName, Document mapFindData, Document sort, Function<Document, T> function) {
        List<T> listDoc = new ArrayList<>();
        FindIterable iterable = MongoDBConnectionFactory.getDB().getCollection(collectionName)
                .find(mapFindData).sort(sort);
        if (iterable.iterator().hasNext())
            iterable.forEach(new Block<Document>() {
                public void apply(Document document) {
                    listDoc.add(function.apply(document));
                }
            });
        return listDoc;
    }

    @Override
    public <T> List<T> find(String collectionName, Map<String, Object> mapFindData, Function<Document, T> function) {
        return find(collectionName, mapFindData, null, function);
    }

    @Override
    public <T> List<T> find(
            String collectionName, Map<String, Object> mapFindData, String sortDescColumnName, Function<Document, T> function) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection(collectionName);
        Document conditions = new Document();
        // BasicDBObject conditions = new BasicDBObject();
        for (Map.Entry<String, Object> entry : mapFindData.entrySet()) {
            conditions.append(entry.getKey(), entry.getValue());
        }
        HashMap<String, Object> sortMap = new HashMap<String, Object>();
        if (sortDescColumnName != null && !sortDescColumnName.isEmpty())
            sortMap.put(sortDescColumnName, -1);

        List<T> listDoc = new ArrayList<>();
        FindIterable iterable = null;
        if (sortDescColumnName != null)
            iterable = col.find((Bson) new Document((Map) conditions)).sort(new Document(sortMap));
        else
            iterable = col.find((Bson) new Document((Map) conditions));
        if (iterable.iterator().hasNext())
            iterable.forEach(new Block<Document>() {
                public void apply(Document document) {
                    listDoc.add(function.apply(document));
                }
            });
        return listDoc;
    }

    @Override
    public void findOneAndUpdate(String collectionName, Map<String, Object> mapFindData, Map<String, Object> mapUpdateData) {
        try {
            MongoDatabase db = MongoDBConnectionFactory.getDB();
            MongoCollection<Document> col = db.getCollection(collectionName);
            BasicDBObject updateFields = new BasicDBObject();
            for (Map.Entry<String, Object> entry : mapUpdateData.entrySet()) {
                updateFields.append(entry.getKey(), entry.getValue());
            }
            BasicDBObject conditions = new BasicDBObject();
            for (Map.Entry<String, Object> entry : mapFindData.entrySet()) {
                conditions.append(entry.getKey(), entry.getValue());
            }
            FindOneAndUpdateOptions options = new FindOneAndUpdateOptions();
            options.upsert(true);
            col.findOneAndUpdate(conditions, new Document("$set", updateFields), options);
        } catch (Exception e) {
            logger.debug(e.getMessage());
        }
    }

    @Override
    public void update(String collectionName, Document filter, Document mapUpdateData) {
        try {
            MongoDatabase db = MongoDBConnectionFactory.getDB();
            MongoCollection<Document> collection = db.getCollection(collectionName);

            UpdateOptions options = new UpdateOptions();
            Bson updateOperationDocument = new Document("$set", mapUpdateData);
            collection.updateMany(filter, updateOperationDocument, options);
        } catch (Exception e) {
            KingUtil.printException("MongodbeService", e);
        }
    }

    @Override
    public void update(String collectionName, Map<String, Object> mapFindData, Map<String, Object> mapUpdateData) {
        try {
            BasicDBObject conditions = new BasicDBObject();
            for (Map.Entry<String, Object> entry : mapFindData.entrySet()) {
                conditions.put(entry.getKey(), entry.getValue());
            }

            BasicDBObject updateFields = new BasicDBObject();
            for (Map.Entry<String, Object> entry : mapUpdateData.entrySet()) {
                updateFields.put(entry.getKey(), entry.getValue());
            }

            update(collectionName, conditions, updateFields, false);
        } catch (Exception e) {
            KingUtil.printException("MongodbeService", e);
        }
    }

    /**
     * Update a single or all documents in the collection according to the specified arguments.
     * When upsert set to true, the new document will be inserted if there are no matches to the query filter.
     *
     * @param filter   Bson filter
     * @param document Bson document
     * @param upsert   a new document should be inserted if there are no matches to the query filter
     */
    public void update(String collectionName, Bson filter, Bson document, boolean upsert) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection<Document> collection = db.getCollection(collectionName);

        UpdateOptions options = new UpdateOptions();
        if (upsert) {
            options.upsert(true);
        }
        Bson updateOperationDocument = new Document("$set", document);
        collection.updateMany(filter, updateOperationDocument, options);
    }

    @Override
    public List<Document> getListByDate(String collectionName, String dateColumnName, String timeStart, String timeEnd, Map<String, Object> otherConditions
            , int page, int recordPerPage) {
        List<Document> results = new ArrayList<Document>();
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        HashMap<String, Object> conditions = new HashMap<String, Object>();
        FindIterable iterable = null;
        BasicDBObject obj = new BasicDBObject();
        int numStart = (page - 1) * recordPerPage;
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                obj.put("$gte", simpleDateFormat.parse(timeStart));
                obj.put("$lte", simpleDateFormat.parse(timeEnd));
                conditions.put(dateColumnName, obj);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        for (Map.Entry<String, Object> entry : otherConditions.entrySet()) {
            conditions.put(entry.getKey(), entry.getValue());
        }
        HashMap<String, Object> sortMap = new HashMap<String, Object>();
        sortMap.put(dateColumnName, -1);
        iterable = db.getCollection(collectionName).find(new Document(conditions)).sort(new Document(sortMap)).skip(numStart).limit(recordPerPage);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        iterable.forEach((Block) new Block<Document>() {
            public void apply(Document document) {
                document.put("create_time_str", dateFormat.format(document.getDate(dateColumnName)));
                results.add(document);
            }
        });
        return results;
    }

    @Override
    public int countListByDate(String collectionName, String dateColumnName, String timeStart, String timeEnd, Map<String, Object> otherConditions) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        HashMap<String, Object> conditions = new HashMap<String, Object>();
        BasicDBObject obj = new BasicDBObject();
        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                obj.put("$gte", simpleDateFormat.parse(timeStart));
                obj.put("$lte", simpleDateFormat.parse(timeEnd));
                conditions.put(dateColumnName, obj);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        for (Map.Entry<String, Object> entry : otherConditions.entrySet()) {
            conditions.put(entry.getKey(), entry.getValue());
        }
        return (int) db.getCollection(collectionName).count(new Document(conditions));
    }

    @Override
    public List<Document> getListByConditions(String collectionName, Map<String, Object> conditions, int page, int recordPerPage) {
        List<Document> results = new ArrayList<Document>();
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        FindIterable iterable = null;
        BasicDBObject obj = new BasicDBObject();
        int numStart = (page - 1) * recordPerPage;
        HashMap<String, Object> sortMap = new HashMap<String, Object>();
        sortMap.put("_id", -1);
        iterable = db.getCollection(collectionName).find(new Document(conditions)).sort(new Document(sortMap)).skip(numStart).limit(recordPerPage);
        iterable.forEach((Block) new Block<Document>() {
            public void apply(Document document) {
                results.add(document);
            }
        });
        return results;
    }

    @Override
    public void del(String collectionName, Map<String, Object> mapFindData) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection(collectionName);
        Document conditions = new Document();
        for (Map.Entry<String, Object> entry : mapFindData.entrySet()) {
            conditions.append(entry.getKey(), entry.getValue());
        }
        col.deleteOne(new Document(conditions));
    }
}
