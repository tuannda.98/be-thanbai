package casio.king365.core;

import casio.king365.entities.X3NapLanDau;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.models.cache.UserExtraInfoModel;

public enum HCMap {
    USERS("users"),
    USER_EXTRA_INFO_MODEL("cache_user_extra_info"),
    TOKEN("cacheToken"),
    CONFIG("cacheConfig"),
    CONNECTED_MINIGAME("connectedMinigameBoolean"),
    X3NAPLANDAU("x3naplandau"),
    USERS_BET_TX("userbettaixiu"),
    USERS_BET_BAUCUA("userbetbaucua"),
    USERS_BET_XOCDIA("userbetxocdia"),
    USERS_RESTORE_XOCDIA("userrestorexocdia"),
    ALLGIFTCODE("allGiftcode")
    ;
    String key;

    HCMap(String key) {
        this.key = key;
    }

    public static IMap<String, UserCacheModel> getUsersMap() {
        HazelcastInstance instance = HazelcastClientFactory.getInstance();
        return instance.getMap(USERS.key);
    }

    public static IMap<String, String> getCachedConfig() {
        HazelcastInstance instance = HazelcastClientFactory.getInstance();
        return instance.getMap(CONFIG.key);
    }

    public static IMap<String, UserExtraInfoModel> getUserExtraInfoMap() {
        HazelcastInstance instance = HazelcastClientFactory.getInstance();
        return instance.getMap(USER_EXTRA_INFO_MODEL.key);
    }

    public static IMap<String, String> getTokenMap() {
        HazelcastInstance instance = HazelcastClientFactory.getInstance();
        return instance.getMap(TOKEN.key);
    }

    public static IMap<String, Boolean> getConnectedMinigameMap() {
        HazelcastInstance instance = HazelcastClientFactory.getInstance();
        return instance.getMap(CONNECTED_MINIGAME.key);
    }

    public static IMap<String, X3NapLanDau> getX3NLDMap() {
        HazelcastInstance instance = HazelcastClientFactory.getInstance();
        return instance.getMap(X3NAPLANDAU.key);
    }

    public static IMap<String, String> getAllGiftcode() {
        HazelcastInstance instance = HazelcastClientFactory.getInstance();
        return instance.getMap(ALLGIFTCODE.key);
    }

    public static IMap<String, Long> getUsersBetTaiXiu() {
        HazelcastInstance instance = HazelcastClientFactory.getInstance();
        return instance.getMap(USERS_BET_TX.key);
    }

    public static IMap<String, Long> getUsersBetBauCua() {
        HazelcastInstance instance = HazelcastClientFactory.getInstance();
        return instance.getMap(USERS_BET_BAUCUA.key);
    }

    public static IMap<String, Long> getUsersBetXocDia() {
        HazelcastInstance instance = HazelcastClientFactory.getInstance();
        return instance.getMap(USERS_BET_XOCDIA.key);
    }

    public static IMap<String, Long> getUsersRestoreXocDia() {
        HazelcastInstance instance = HazelcastClientFactory.getInstance();
        return instance.getMap(USERS_RESTORE_XOCDIA.key);
    }

}
