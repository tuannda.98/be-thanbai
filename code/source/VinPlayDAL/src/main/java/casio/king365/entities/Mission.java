package casio.king365.entities;

import org.bson.Document;

import java.util.HashMap;
import java.util.Map;

public class Mission {
    String id;              // ID của nhiệm vụ
    String desc;            // Mô tả
    String repeat_type;      // Chế độ lặp lại,none: không lặp, day: hàng ngày
    String mission_type;      // otp, recharge, game
    Integer number_target;   // số lượng mục tiêu cần hoàn thành
    String depend;          // Id nhiệm vụ cần hoàn thành rồi mới được làm nhiệm vụ này
    Integer bonus;          // Thưởng cho nhiệm vụ này

    public Mission() { }

    public Map<String, Object> toMap(){
        Map<String, Object> mapData = new HashMap<>();
        mapData.put("id", getId());
        mapData.put("desc", getDesc());
        mapData.put("repeat_type", getRepeatType());
        mapData.put("mission_type", getMissionType());
        mapData.put("number_target", getNumberTarget());
        mapData.put("depend", getDepend());
        mapData.put("bonus", getBonus());
        return mapData;
    }

    public static Mission fromDocument(Document doc){
        Mission mission = new Mission();
        mission.setId(doc.getString("id"));
        mission.setDesc(doc.getString("desc"));
        mission.setRepeatType(doc.getString("repeat_type"));
        mission.setMissionType(doc.getString("mission_type"));
        mission.setDepend(doc.getString("depend"));
        mission.setNumberTarget(doc.getInteger("number_target"));
        mission.setBonus(doc.getInteger("bonus"));
        return mission;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getRepeatType() {
        return repeat_type;
    }

    public void setRepeatType(String repeat_type) {
        this.repeat_type = repeat_type;
    }

    public Integer getNumberTarget() {
        return number_target;
    }

    public void setNumberTarget(Integer numberTarget) {
        this.number_target = numberTarget;
    }

    public String getDepend() {
        return depend;
    }

    public void setDepend(String depend) {
        this.depend = depend;
    }

    public Integer getBonus() {
        return bonus;
    }

    public void setBonus(Integer bonus) {
        this.bonus = bonus;
    }

    public String getMissionType() {
        return mission_type;
    }

    public void setMissionType(String mission_type) {
        this.mission_type = mission_type;
    }

    public String toString1() {
        return "Mission{" +
                "id='" + id + '\'' +
                ", desc='" + desc + '\'' +
                ", repeatType='" + repeat_type + '\'' +
                ", missionType='" + mission_type + '\'' +
                ", numberTarget=" + number_target +
                ", depend='" + depend + '\'' +
                ", bonus=" + bonus +
                '}';
    }
}
