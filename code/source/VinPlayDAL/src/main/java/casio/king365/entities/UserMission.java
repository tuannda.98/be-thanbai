package casio.king365.entities;

import org.bson.Document;

import java.util.HashMap;
import java.util.Map;

public class UserMission {
    String nickname;
    String missionId;
    Integer numberDoingTarget;  // Số lần hoàn thành nhiệm vụ (trên tổng số number_target của nhiệm vụ)
    Long startTime;     // Ngày bắt đầu làm nhiệm vụ
    Boolean receivedBonus;   // User đã nhận thưởng chưa?
    Mission mission;

    public UserMission() { }

    public Map<String, Object> toMap(){
        Map<String, Object> mapData = new HashMap<>();
        mapData.put("nickname", getNickname());
        mapData.put("startTime", getStartTime());
        mapData.put("missionId", getMissionId());
        mapData.put("numberDoingTarget", getNumberDoingTarget());
        mapData.put("receivedBonus", getReceivedBonus());
        return mapData;
    }

    public static UserMission fromDocument(Document doc){
        UserMission mission = new UserMission();
        mission.setNickname(doc.getString("nickname"));
        mission.setMissionId(doc.getString("missionId"));
        mission.setNumberDoingTarget(doc.getInteger("numberDoingTarget"));
        if(doc.containsKey("receivedBonus"))
            mission.setReceivedBonus(doc.getBoolean("receivedBonus"));
        else
            mission.setReceivedBonus(false);
        return mission;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Boolean getReceivedBonus() {
        return receivedBonus;
    }

    public void setReceivedBonus(Boolean receivedBonus) {
        this.receivedBonus = receivedBonus;
    }

    public String getMissionId() {
        return missionId;
    }

    public void setMissionId(String missionId) {
        this.missionId = missionId;
    }

    public Integer getNumberDoingTarget() {
        return numberDoingTarget;
    }

    public void setNumberDoingTarget(Integer numberDoingTarget) {
        this.numberDoingTarget = numberDoingTarget;
    }

    public Mission getMission() {
        return mission;
    }

    public void setMission(Mission mission) {
        this.mission = mission;
    }

    public String toString1() {
        return "UserMission{" +
                "nickname='" + nickname + '\'' +
                ", missionId='" + missionId + '\'' +
                ", numberDoingTarget=" + numberDoingTarget +
                ", startTime=" + startTime +
                ", receivedBonus=" + receivedBonus +
                '}';
    }
}
