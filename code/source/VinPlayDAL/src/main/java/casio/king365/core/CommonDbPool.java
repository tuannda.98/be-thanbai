package casio.king365.core;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

public class CommonDbPool {

    private static CommonDbPool instance = null;
    private static Map<String, DataSource> mDataSource = new ConcurrentHashMap<>();
    private static String CONFIG_FILE = "config/db_pool.properties";

    public CommonDbPool() {
    }

    public static CommonDbPool getInstance() {
        if (instance == null)
            instance = new CommonDbPool();
        return instance;
    }

    private DataSource getDataSource(String name) {
        if (mDataSource.containsKey(name))
            return mDataSource.get(name);

        Properties prop = new Properties();
        try (FileInputStream input = new FileInputStream(CONFIG_FILE)) {
            prop.load(input);

            HikariConfig config = new HikariConfig();


            config.setDriverClassName(prop.getProperty("db." + name + ".driver_class"));
            config.setJdbcUrl(prop.getProperty("db." + name + ".url"));
            config.setUsername(prop.getProperty("db." + name + ".user"));
            config.setPassword(prop.getProperty("db." + name + ".password"));
            //The initial number of connections that are created when the pool is started.
            config.setMinimumIdle(Integer.parseInt(prop.getProperty("db." + name + ".inital_pool_size")));
            //The maximum number of active connections that can be allocated from this pool at the same time
            config.setMaximumPoolSize(Integer.parseInt(prop.getProperty("db." + name + ".max_pool_size")));

            mDataSource.put(name, new HikariDataSource(config));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return mDataSource.get(name);
    }

    public DataSource getVinplayDataSource() {
        return getDataSource("vinplay");
    }

    public DataSource getVinplayMinigameDS() {
        return getDataSource("vinplayminigame");
    }
}
