package casio.king365.service;

import com.mongodb.Function;
import org.bson.Document;

import java.util.List;
import java.util.Map;

public interface MongoDbService {

    void insert(String collectionName, Map<String, Object> map);

    void insert(String collectionName, List<Document> docs);

    @Deprecated
    // Use <T> List<T> find(String collectionName, Document mapFindData, Document sort, Function<Document, T> function);
    default List<Document> find(String collectionName, Document mapFindData, Document sort) {
        return find(collectionName, mapFindData, sort, document -> document);
    }

    <T> List<T> find(String collectionName, Document mapFindData, Document sort, Function<Document, T> function);

    @Deprecated
    // Use <T> List<T> find(String collectionName, Map<String, Object> mapFindData, Function<Document, T> function);
    default List<Document> find(String collectionName, Map<String, Object> mapFindData) {
        return find(collectionName, mapFindData, document -> document);
    }

    <T> List<T> find(String collectionName, Map<String, Object> mapFindData, Function<Document, T> function);

    @Deprecated
    // Use <T> List<T> find(String collectionName, Map<String, Object> mapFindData, Function<Document, T> function);
    default List<Document> find(String collectionName, Map<String, Object> mapFindData, String sortDescColumnName) {
        return find(collectionName, mapFindData, sortDescColumnName, document -> document);
    }

    <T> List<T> find(String collectionName, Map<String, Object> mapFindData, String sortDescColumnName, Function<Document, T> function);

    void findOneAndUpdate(String collectionName, Map<String, Object> mapFindData, Map<String, Object> mapUpdateData);

    void update(String collectionName, Document mapFindData, Document mapUpdateData);

    void update(String collectionName, Map<String, Object> mapFindData, Map<String, Object> mapUpdateData);

    List<Document> getListByDate(String collectionName, String dateColumnName, String timeStart, String timeEnd
            , Map<String, Object> otherConditions, int page, int recordPerPage);

    int countListByDate(String collectionName, String dateColumnName, String timeStart, String timeEnd, Map<String, Object> otherConditions);

    List<Document> getListByConditions(String collectionName, Map<String, Object> conditions, int page, int recordPerPage);

    void del(String collectionName, Map<String, Object> mapFindData);
}
