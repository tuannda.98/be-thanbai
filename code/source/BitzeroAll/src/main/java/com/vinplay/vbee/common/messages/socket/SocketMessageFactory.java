package com.vinplay.vbee.common.messages.socket;

import com.vinplay.vbee.common.models.UserModel;
import casio.king365.dto.UpdateUserMoneyMsg;

public class SocketMessageFactory {

    public static UpdateUserMoneyMsg createUpdateUserMoneyMsg(UserModel userModel, long changeMoney, String desc, boolean showPopup) {
        UpdateUserMoneyMsg obj = new UpdateUserMoneyMsg();
        obj.userNickname = userModel.getNickname();
        obj.totalMoney = userModel.getVinTotal();
        obj.changeMoney = changeMoney;
        obj.desc = desc;
        obj.showPopup = showPopup;
        return obj;
    }
}
