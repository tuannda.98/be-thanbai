package game;

import bitzero.server.extensions.data.BaseMsg;

import java.io.Serializable;
import java.nio.ByteBuffer;

public class BaseMsgEx extends BaseMsg implements Serializable, Transportable<BaseMsgEx> {
    public BaseMsgEx(final int type) {
        super((short) type);
    }

    protected BaseMsgEx(final int type, final int error) {
        super((short) type, error);
    }

    @Override
    protected ByteBuffer makeBuffer() {
        ByteBuffer byteBuffer = null;
        try {
            byteBuffer = super.makeBuffer();
        } catch (Exception ex) {
        }
        if (byteBuffer == null) {
            byteBuffer = ByteBuffer.allocate(5012);
        }
        return byteBuffer;
    }
}
