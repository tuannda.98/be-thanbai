package game;

import java.io.*;

public interface Transportable<T extends Serializable> {
    default byte[] toBytes() {
        byte[] bytes;
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
             ObjectOutputStream oos = new ObjectOutputStream(baos)) {
            oos.writeObject(this);
            oos.flush();
            oos.reset();
            bytes = baos.toByteArray();
        } catch (IOException e) {
            bytes = new byte[]{};
        }
        return bytes;
    }

    static <T> T fromBytes(byte[] body) {
        T obj = null;
        try (ByteArrayInputStream bis = new ByteArrayInputStream(body);
             ObjectInputStream ois = new ObjectInputStream(bis)) {
            obj = (T) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return obj;
    }
}
