package casio.king365.dto;

import game.BaseMsgEx;

import java.nio.ByteBuffer;

public class UpdateUserMoneyMsg extends BaseMsgEx {
    public String userNickname;
    public long totalMoney;
    public long changeMoney;
    public String desc;
    public boolean showPopup = false;

    public UpdateUserMoneyMsg() {
        super(21000);
    }

    public static UpdateUserMoneyMsg create(
            String userNickname, long totalMoney, long changeMoney, String desc, boolean showPopup) {
        UpdateUserMoneyMsg obj = new UpdateUserMoneyMsg();
        obj.userNickname = userNickname;
        obj.totalMoney = totalMoney;
        obj.changeMoney = changeMoney;
        obj.desc = desc;
        obj.showPopup = showPopup;
        return obj;
    }

    public byte[] createData() {
        ByteBuffer bf = this.makeBuffer();
        this.putStr(bf, this.desc);
        this.putStr(bf, this.userNickname);
        bf.putLong(changeMoney);
        bf.putLong(totalMoney);
        this.putBoolean(bf, showPopup);
        return this.packBuffer(bf);
    }
}
