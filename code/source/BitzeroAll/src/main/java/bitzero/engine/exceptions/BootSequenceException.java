/*
 * Decompiled with CFR 0.150.
 */
package bitzero.engine.exceptions;

import bitzero.engine.exceptions.BitZeroEngineException;

public class BootSequenceException
extends BitZeroEngineException {
    public BootSequenceException(String message) {
        super(message);
    }
}

