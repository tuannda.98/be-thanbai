/*
 * Decompiled with CFR 0.150.
 */
package bitzero.engine.events;

import bitzero.engine.events.IEvent;

public interface IEventListener {
    public void handleEvent(IEvent var1);
}

