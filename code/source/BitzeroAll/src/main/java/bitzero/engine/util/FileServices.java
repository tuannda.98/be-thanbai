/*
 * Decompiled with CFR 0.150.
 */
package bitzero.engine.util;

import bitzero.engine.exceptions.BitZeroEngineException;
import java.io.File;

public class FileServices {
    private static final String WINDOWS_SEPARATOR = "\\";
    private static final String UNIX_SEPARATOR = "/";

    public static void recursiveMakeDir(String basePath, String dirStructure) throws BitZeroEngineException {
        dirStructure.replace(WINDOWS_SEPARATOR, UNIX_SEPARATOR);
        String finalPath = String.valueOf(basePath) + (basePath.endsWith(UNIX_SEPARATOR) ? "" : UNIX_SEPARATOR) + dirStructure;
        File folder = new File(finalPath);
        System.out.println("Creating: " + finalPath);
        if (folder.exists()) {
            return;
        }
        boolean ok = folder.mkdirs();
        if (!ok) {
            throw new BitZeroEngineException("Was not able to create the following folder(s): " + dirStructure);
        }
    }
}

