/*
 * Decompiled with CFR 0.150.
 */
package bitzero.engine.core.security;

import bitzero.engine.service.IService;

public interface ISecurityManager
extends IService {
    public boolean isEngineThread(Thread var1);
}

