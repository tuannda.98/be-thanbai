/*
 * Decompiled with CFR 0.150.
 */
package bitzero.engine.exceptions;

public class BitZeroEngineRuntimeException
extends RuntimeException {
    public BitZeroEngineRuntimeException() {
    }

    public BitZeroEngineRuntimeException(String msg) {
        super(msg);
    }
}

