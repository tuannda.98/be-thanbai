package bitzero.util.common.business;

public interface LogListener {
    void error(String log);
}
