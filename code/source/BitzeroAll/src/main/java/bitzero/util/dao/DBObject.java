/*
 * Decompiled with CFR 0.150.
 */
package bitzero.util.dao;

import bitzero.util.config.bean.ConstantMercury;
import bitzero.util.dao.IDBObject;
import bitzero.util.datacontroller.business.DataController;
import java.io.Serializable;

public class DBObject
implements IDBObject,
Serializable {
    static final long serialVersionUID = 1L;
    private static final String SHARED_KEY = "shared";
    public static final String SEPERATOR = "_";
    private int uId = -1;

    @Override
    public void save() throws Exception {
        StringBuilder builder = new StringBuilder(ConstantMercury.PREFIX_SNSGAME_GENERAL);
        builder.append(this.uId).append(SEPERATOR).append(this.getClass().getSimpleName());
        DataController.getController().set(builder.toString(), this);
    }

    public static Object load(int uId, Class c) throws Exception {
        StringBuilder builder = new StringBuilder(ConstantMercury.PREFIX_SNSGAME_GENERAL);
        builder.append(uId).append(SEPERATOR).append(c.getSimpleName());
        return DataController.getController().get(builder.toString());
    }
}

