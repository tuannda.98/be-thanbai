/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.json.JSONException
 *  org.json.JSONObject
 */
package bitzero.util.payment;

import java.lang.reflect.Field;
import org.json.JSONException;
import org.json.JSONObject;

public class PacketPaymentSend {
    public JSONObject toJSONObject() {
        Field[] fields;
        JSONObject data = new JSONObject();
        for (Field f : fields = this.getClass().getFields()) {
            try {
                if (f.getModifiers() != 1) continue;
                data.put(f.getName(), f.get(this));
            }
            catch (IllegalAccessException illegalAccessException) {
            }
            catch (JSONException jSONException) {
                // empty catch block
            }
        }
        return data;
    }

    public String toString() {
        return this.toJSONObject().toString();
    }
}

