/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.json.JSONException
 *  org.json.JSONObject
 */
package bitzero.util.payment;

import java.lang.reflect.Field;
import org.json.JSONException;
import org.json.JSONObject;

public class PacketPaymentReceive {
    public PacketPaymentReceive(JSONObject data) {
        this.parseJson(data);
    }

    public void parseJson(JSONObject data) {
        Field[] fields;
        for (Field f : fields = this.getClass().getFields()) {
            try {
                if (f.getModifiers() != 1) continue;
                f.set(this, data.get(f.getName()));
            }
            catch (IllegalAccessException illegalAccessException) {
            }
            catch (JSONException jSONException) {
                // empty catch block
            }
        }
    }
}

