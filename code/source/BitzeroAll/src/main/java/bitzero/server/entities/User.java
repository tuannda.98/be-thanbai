package bitzero.server.entities;

import bitzero.engine.sessions.ISession;
import bitzero.engine.sessions.SessionType;
import bitzero.server.BitZeroServer;
import bitzero.server.util.IDisconnectionReason;
import bitzero.util.common.business.Debug;
import bitzero.util.socialcontroller.bean.UserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;

public class User {
    public static final class PropertyKey<T> {
        public static final PropertyKey<UserInfo> USER_INFO = new PropertyKey("user_info");
        public static final PropertyKey<String> SESSION_KEY = new PropertyKey("sessionKey");

        public final String key;

        public PropertyKey(String key) {
            this.key = key;
        }
    }

    private static AtomicInteger autoID = new AtomicInteger(0);
    private int id;
    private ISession session;
    private String name;
    private short privilegeId;
    private volatile long lastLoginTime;
    private volatile long lastJoinRoomTime;
    private volatile Room joinedRoom;
    private volatile int playerIdByRoomId;
    private final ConcurrentMap<String, Object> properties;
    private final ConcurrentMap variables;
    private volatile int badWordsWarnings;
    private volatile int floodWarnings;
    private volatile boolean beingKicked;
    private volatile boolean connected;
    private boolean joining;
    private boolean isBot;
    private volatile Zone currentZone;
    private Logger logger;

    private static int getNewID() {
        return autoID.getAndIncrement();
    }

    public User(ISession session) {
        this("", session);
    }

    public User(String name, ISession session) {
        this.privilegeId = 0;
        this.lastLoginTime = 0L;
        this.badWordsWarnings = 0;
        this.floodWarnings = 0;
        this.beingKicked = false;
        this.connected = false;
        this.joining = false;
        this.isBot = false;
        this.id = getNewID();
        this.name = name;
        this.session = session;
        this.beingKicked = false;
        this.joinedRoom = null;
        this.properties = new ConcurrentHashMap<>();
        this.playerIdByRoomId = 0;
        this.variables = new ConcurrentHashMap();
        this.updateLastRequestTime();
        this.logger = LoggerFactory.getLogger(this.getClass());
    }

    public int getUniqueId() {
        return this.id;
    }

    public boolean isBot() {
        return this.isBot;
    }

    public void setIsBot(final boolean isBot) {
        this.isBot = isBot;
    }

    public int getId() {
        UserInfo info = this.getProperty(PropertyKey.USER_INFO);
        if (info != null) {
            return Integer.parseInt(info.getUserId());
        } else {
            if (!this.isBot()) Debug.warn(new Object[]{"User Info is not set ", this.getName()});
            return id;
        }
    }

    public void setId(int id) {
        this.id = id;
    }

    public short getPrivilegeId() {
        return this.privilegeId;
    }

    public void setPrivilegeId(short id) {
        this.privilegeId = id;
    }

    public boolean isMobile() {
        if (this.session == null) {
            return false;
        }
        return this.session.isMobile();
    }

    public boolean isSuperUser() {
        return false;
    }

    public boolean isConnected() {
        return this.connected;
    }

    public boolean isLocal() {
        return this.session.isLocal();
    }

    public void setConnected(boolean flag) {
        this.connected = flag;
    }

    public boolean isJoining() {
        return this.joining;
    }

    public void setJoining(boolean flag) {
        this.joining = flag;
    }

    public String getIpAddress() {
        if (this.session == null) {
            return "127.0.0.1";
        }
        return this.session.getAddress();
    }

    public void addJoinedRoom(Room room) {
        this.joinedRoom = room;
    }

    public void removeJoinedRoom(Room room) {
        this.joinedRoom = null;
        this.playerIdByRoomId = 0;
    }

    public void disconnect(IDisconnectionReason reason) {
        BitZeroServer.getInstance().getAPIManager().getBzApi().disconnectUser(this, reason);
    }

    public boolean isNpc() {
        return this.session.getType() == SessionType.VOID;
    }

    public Room getJoinedRoom() {
        return this.joinedRoom;
    }

    public Zone getZone() {
        return this.currentZone;
    }

    public void setZone(Zone currentZone) {
        this.currentZone = currentZone;
    }

    public long getLoginTime() {
        return this.lastLoginTime;
    }

    public void setLastLoginTime(long lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public long getLastJoinRoomTime() {
        return lastJoinRoomTime;
    }

    public void setLastJoinRoomTime(long lastJoinRoomTime) {
        this.lastJoinRoomTime = lastJoinRoomTime;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPlayerId() {
        return this.playerIdByRoomId;
    }

    public void setPlayerId(int id, Room room) {
        this.playerIdByRoomId = id;
        this.joinedRoom = room;
    }

    public boolean isPlayer() {
        return this.joinedRoom != null && this.playerIdByRoomId > 0;
    }

    public boolean isSpectator() {
        return this.joinedRoom != null && this.playerIdByRoomId < 0;
    }

    @Deprecated
    public Object getProperty(String key) {
        return this.properties.get(key);
    }

    @Deprecated
    public void setProperty(String key, Object val) {
        this.properties.put(key, val);
    }

    @Deprecated
    public void removeProperty(String key) {
        this.properties.remove(key);
    }

    public <T> void setProperty(PropertyKey<T> key, T val) {
        this.properties.put(key.key, val);
    }

    public <T> void removeProperty(PropertyKey<T> key) {
        this.properties.remove(key.key);
    }

    public <T> T getProperty(PropertyKey<T> key) {
        return (T) this.properties.get(key.key);
    }

    public boolean containsProperty(Object key) {
        return this.properties.containsKey(key);
    }

    public ISession getSession() {
        return this.session;
    }

    public String toString() {
        String ip = this.session == null ? "127.0.0.1" : this.session.getFullIpAddress();

        return String.format("( User Name: %s, Id: %s, Priv: %s, Sess: %s ) ", this.name, this.id, this.privilegeId, ip);
    }

    public long getLastRequestTime() {
        if (this.session == null) {
            return 0L;
        }
        return this.session.getLastLoggedInActivityTime();
    }

    public void updateLastRequestTime() {
        this.setLastRequestTime(System.currentTimeMillis());
    }

    public void setLastRequestTime(long lastRequestTime) {
        if (this.session != null) {
            this.session.setLastLoggedInActivityTime(lastRequestTime);
        }
    }

    public int getBadWordsWarnings() {
        return this.badWordsWarnings;
    }

    public void setBadWordsWarnings(int badWordsWarnings) {
        this.badWordsWarnings = badWordsWarnings;
    }

    public int getFloodWarnings() {
        return this.floodWarnings;
    }

    public void setFloodWarnings(int floodWarnings) {
        this.floodWarnings = floodWarnings;
    }

    public long getLastLoginTime() {
        return this.lastLoginTime;
    }

    public boolean isBeingKicked() {
        return this.beingKicked;
    }

    public void setBeingKicked(boolean flag) {
        this.beingKicked = flag;
    }

    public int getReconnectionSeconds() {
        if (this.session == null) {
            return 0;
        }
        return this.session.getReconnectionSeconds();
    }

    public void setReconnectionSeconds(int seconds) {
        if (this.session != null) {
            this.session.setReconnectionSeconds(seconds);
        }
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof User)) {
            return false;
        }
        User user = (User) obj;
        boolean isEqual = false;
        if (user.getUniqueId() == this.id) {
            isEqual = true;
        }

        return isEqual;
    }

    public String getDump() {
        return "User{" +
                "id=" + id +
                ", session=" + session +
                ", name='" + name + '\'' +
                ", privilegeId=" + privilegeId +
                ", lastLoginTime=" + lastLoginTime +
                ", joinedRoom=" + joinedRoom +
                ", playerIdByRoomId=" + playerIdByRoomId +
                ", properties=" + properties +
                ", variables=" + variables +
                ", badWordsWarnings=" + badWordsWarnings +
                ", floodWarnings=" + floodWarnings +
                ", beingKicked=" + beingKicked +
                ", connected=" + connected +
                ", joining=" + joining +
                ", isBot=" + isBot +
                ", currentZone=" + currentZone +
                '}';
    }

    private void populateTransientFields() {
        this.logger = LoggerFactory.getLogger(this.getClass());
    }
}
