/*
 * Decompiled with CFR 0.150.
 */
package bitzero.server.controllers;

public enum SystemRequest {
    Handshake((short)0),
    Login((short)1),
    LoginWebsocket((short)9002),
    Logout((short)2),
    GetRoomList((short)3),
    JoinRoom((short)4),
    AutoJoin((short)5),
    CreateRoom((short)6),
    GenericMessage((short)7),
    ChangeRoomName((short)8),
    ChangeRoomPassword((short)9),
    ObjectMessage((short)10),
    SetRoomVariables((short)11),
    SetUserVariables((short)12),
    CallExtension((short)13),
    LeaveRoom((short)14),
    SubscribeRoomGroup((short)15),
    UnsubscribeRoomGroup((short)16),
    SpectatorToPlayer((short)17),
    PlayerToSpectator((short)18),
    ChangeRoomCapacity((short)19),
    PublicMessage((short)20),
    PrivateMessage((short)21),
    ModeratorMessage((short)22),
    AdminMessage((short)23),
    KickUser((short)24),
    BanUser((short)25),
    ManualDisconnection((short)26),
    GoOnline((short)27),
    InviteUser((short)28),
    InvitationReply((short)29),
    CreateBZGame((short)30),
    QuickJoinGame((short)31),
    OnEnterRoom((short)32),
    OnRoomCountChange((short)33),
    OnUserLost((short)34),
    OnRoomLost((short)35),
    OnUserExitRoom((short)36),
    OnClientDisconnection((short)37),
    BanUserChat((short)38),
    PingPong((short)50),
    CheckOnline((short)51),
    SystemStats((short)1000),
    SetPoolSize((short)1001),
    SetLogLevel((short)1002),
    CrossCommand((short)200),
    ServiceNotify((short)201),
    DashBoard((short)9001),
    IpFilterCommand((short)8000),
    CrossExtCommand((short)300),
    ExecuteCommand((short)9000);

    private Object id;

    public static SystemRequest fromId(Object id) {
        SystemRequest req = null;
        for (SystemRequest item : SystemRequest.values()) {
            if (!item.getId().equals(id)) continue;
            req = item;
            break;
        }
        if (req == null) {
            req = CallExtension;
        }
        return req;
    }

    private SystemRequest(Object id) {
        this.id = id;
    }

    public Object getId() {
        return this.id;
    }
}

