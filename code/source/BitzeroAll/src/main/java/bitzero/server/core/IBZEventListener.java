/*
 * Decompiled with CFR 0.150.
 */
package bitzero.server.core;

import bitzero.server.core.IBZEvent;

public interface IBZEventListener {
    public void handleServerEvent(IBZEvent var1) throws Exception;
}

