/*
 * Decompiled with CFR 0.150.
 */
package bitzero.server.controllers.admin;

import bitzero.engine.sessions.ISession;
import bitzero.server.controllers.SystemRequest;
import bitzero.server.controllers.admin.utils.BaseAdminCommand;
import bitzero.server.extensions.data.DataCmd;

public class AdminMessage
extends BaseAdminCommand {
    public AdminMessage() {
        super(SystemRequest.AdminMessage);
    }

    @Override
    public void handleRequest(ISession sender, DataCmd cmd) {
        this.api.sendAdminMessage(null, cmd.readString(), new String[]{"Notify User "}, this.bz.getUserManager().getAllSessions());
    }
}

