/*
 * Decompiled with CFR 0.150.
 */
package bitzero.server.exceptions;

public class BZRuntimeException
extends RuntimeException {
    public BZRuntimeException() {
    }

    public BZRuntimeException(String message) {
        super(message);
    }

    public BZRuntimeException(Throwable t) {
        super(t);
    }
}

