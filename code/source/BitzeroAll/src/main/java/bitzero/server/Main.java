/*
 * Decompiled with CFR 0.150.
 */
package bitzero.server;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.hazelcast.HazelcastAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import java.util.Arrays;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, HazelcastAutoConfiguration.class})
@ComponentScan({"game", "bitzero", "casio", "com.vinplay", "extension", "monitor.controller"})
public class Main {
    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
        init(args);
    }

    public static void init(String[] args) {
        boolean clusterMode = false;
        boolean useConsole = false;
        if (args.length > 0) {
            clusterMode = args[0].equalsIgnoreCase("cluster");
            useConsole = args.length > 1 && args[1].equalsIgnoreCase("console");
        }
        BitZeroServer bzServer = BitZeroServer.getInstance();
        bzServer.setClustered(clusterMode);
        if (useConsole) {
            bzServer.startDebugConsole();
        }
        bzServer.start();
    }
}
