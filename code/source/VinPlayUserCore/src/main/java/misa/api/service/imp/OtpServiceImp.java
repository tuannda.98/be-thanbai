package misa.api.service.imp;


//import misa.core.pools.MongoPool;

import casio.king365.service.MongoDbService;
import casio.king365.service.impl.MongoDbServiceImpl;
import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import misa.api.service.OtpService;
import misa.data.AccountTeleLink;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.HashMap;
import java.util.Map;
//import org.jongo.Jongo;

public class OtpServiceImp implements OtpService {
    @Override
    public AccountTeleLink getAccountTeleLinkByIdUser(String idUser) {
//        StringBuilder sb = new StringBuilder();
//        sb.append("{$and :[");
//        sb.append("{idUserTele: #}");
//        sb.append("]}");
//        DB db =  MongoPool.getDBJongo();
//        Jongo jongo = new Jongo(db);
//        org.jongo.MongoCollection collection = jongo.getCollection(AccountTeleLink.class.getSimpleName());
//        org.jongo.MongoCursor<AccountTeleLink> cursor = collection.find(sb.toString(),idUser).limit(1).as(AccountTeleLink.class);

//        if (cursor.hasNext()){
//            return cursor.next();
//        }
//        return null;
        AccountTeleLink user = new AccountTeleLink();
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection(AccountTeleLink.class.getSimpleName());
        FindIterable iterable = null;
        Document conditions = new Document();
        conditions.put("idUserTele", idUser);
        iterable = col.find((Bson) new Document((Map) conditions));
        if (!iterable.iterator().hasNext())
            return null;
        iterable.forEach(new Block<Document>() {
            public void apply(Document document) {
                user.setIdUserTele(document.getString("idUserTele"));
                user.setMobile(document.getString("mobile"));
                user.setNickName(document.getString("nickName"));
            }
        });
        return user;
    }

    @Override
    public AccountTeleLink getAccountTeleLinkByNickname(String nickname) {
        AccountTeleLink user = new AccountTeleLink();
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection(AccountTeleLink.class.getSimpleName());
        FindIterable iterable = null;
        Document conditions = new Document();
        conditions.put("nickName", nickname);
        iterable = col.find((Bson) new Document((Map) conditions));
        if (!iterable.iterator().hasNext())
            return null;
        iterable.forEach(new Block<Document>() {
            public void apply(Document document) {
                user.setIdUserTele(document.getString("idUserTele"));
                user.setMobile(document.getString("mobile"));
                user.setNickName(document.getString("nickName"));
            }
        });
        return user;
    }

    @Override
    public void insertAccountTeleLink(AccountTeleLink data) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection(AccountTeleLink.class.getSimpleName());
        Document doc = new Document();
        doc.append("idUserTele", data.getIdUserTele());
        doc.append("mobile", data.getMobile());
        doc.append("nickName", data.getNickName());
        col.insertOne(doc);
    }

    @Override
    public void updateMobileAccountTeleLink(AccountTeleLink accountTeleLink, String mobile) {
        MongoDbService mongoDbService = new MongoDbServiceImpl();
        Map<String, Object> mapFindData = new HashMap<>();
        mapFindData.put("idUserTele", accountTeleLink.getIdUserTele());
        Map<String, Object> mapUpdateData = new HashMap<>();
        mapUpdateData.put("mobile", mobile);
        mongoDbService.findOneAndUpdate(AccountTeleLink.class.getSimpleName(),
                mapFindData, mapUpdateData);
    }
}
