package misa.api.service;

import misa.data.AccountTeleLink;

public interface OtpService {
    AccountTeleLink getAccountTeleLinkByIdUser(String idUser);
    AccountTeleLink getAccountTeleLinkByNickname(String nickname);
    void insertAccountTeleLink(AccountTeleLink data);
    void updateMobileAccountTeleLink(AccountTeleLink accountTeleLink, String mobile);
}
