package misa.api.handle;

import com.google.gson.JsonObject;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import misa.core.hazelcast.HazelcastClientFactory;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class OTPHandle {

    private static final Logger logger = Logger.getLogger((String)"api");

    public static String getOTP(String nickName){
        HazelcastInstance instance= HazelcastClientFactory.getSingleClient();
        IMap<String, String> imapOTP = instance.getMap("OTP_TELE_SYSTEM");

        Random rd = new Random();
        int otp = rd.nextInt(900000)+100000;
        String strOTP = String.valueOf(otp);
        imapOTP.put(nickName, strOTP, 3, TimeUnit.MINUTES);
        return strOTP;
    }

    public static String getOTPUpdateMobile(String mobile, String idTele){
        HazelcastInstance instance= com.vinplay.vbee.common.hazelcast.HazelcastClientFactory.getInstance();
        IMap<String, String> imapOTP = instance.getMap("OTP_UPDATE_MOBILE");

        Random rd = new Random();
        int otp = rd.nextInt(900000)+100000;
        String strOTP = String.valueOf(otp);
//        StringBuilder sb = new StringBuilder(strOTP);
//        sb.append("|").append(idTele);
//        System.out.println(sb.toString() + "-" +mobile);
        JSONObject data = new JSONObject();
        try {
            data.put("otp", strOTP);
            data.put("idTele", idTele);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        logger.debug((Object) ("OTPHandle - getOTPUpdateMobile(), data: "+data));
        imapOTP.put(mobile, data.toString(), 3000, TimeUnit.MINUTES);
        return strOTP;
    }

    public static boolean checkOTP(String nickName, String otp){
        HazelcastInstance instance= HazelcastClientFactory.getSingleClient();
        IMap<String, String> imapOTP = instance.getMap("OTP_TELE_SYSTEM");
        if (imapOTP.containsKey(nickName)){
            String mapOtp = imapOTP.get(nickName);
            if(mapOtp.equalsIgnoreCase(otp)){
                imapOTP.remove(nickName);
                return true;
            }
        }
        return false;
    }

    public static String checkOTPUpdateMobile(String mobile, String otp){
        HazelcastInstance instance= HazelcastClientFactory.getSingleClient();
        IMap<String, String> imapOTP = instance.getMap("OTP_UPDATE_MOBILE");
        System.out.println(mobile);
        if (imapOTP.containsKey(mobile)){
            String mapOtp = imapOTP.get(mobile);
            String[] ar = mapOtp.split("\\|");
            if(ar[0].equalsIgnoreCase(otp)){
                imapOTP.remove(mobile);
                return ar[1];
            }
        }
        return null;
    }

    public static JsonObject verifyOTP(String nickName, String otp){
        JsonObject jsonObject = new JsonObject();
        boolean check = checkOTP(nickName, otp);
        jsonObject.addProperty("error", check ? 0 : 1);
        return jsonObject;
    }

    public static JsonObject verifyOTPUpdateMobile(String mobile, String otp, String nickname){
        JsonObject jsonObject = new JsonObject();
        String check = checkOTPUpdateMobile(mobile, otp);
        System.out.println(check);
        if(check!=null) {
            jsonObject.addProperty("idTele", check);
        }
        jsonObject.addProperty("error", check!=null ? 0 : 1);
        return jsonObject;
    }
}
