package misa.api.config;

import casio.king365.util.KingUtil;
import misa.TeleLauncher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class TeleConfig {
    private static Logger logger = LoggerFactory.getLogger(TeleConfig.class.getName());
    private static String CONFIG_FILE = "config/tele.properties";

    public static void init() throws IOException {
        File fConfigFile = new File(CONFIG_FILE);
        if (fConfigFile.exists()) {
            Properties prop = new Properties();
            try (InputStream input = new FileInputStream(CONFIG_FILE);) {
                prop.load(input);
                TOKEN_ACCESS_HTTP_API = prop.getProperty("TOKEN_ACCESS_HTTP_API");
                TOKEN_ACCESS_HTTP_API_NOTI = prop.getProperty("TOKEN_ACCESS_HTTP_API_NOTI");
                USER_RECEIPTT_NOTI_CARD = Long.parseLong(prop.getProperty("USER_RECEIPTT_NOTI_CARD"));
                USER_RECEIPTT_NOTI_BANK = Long.parseLong(prop.getProperty("USER_RECEIPTT_NOTI_BANK"));
            } catch (Exception e) {
                e.printStackTrace();
                setDefaultValue();
            }
        } else {
            logger.info("TeleConfig set default values");
            setDefaultValue();
        }
        logger.info("TOKEN_ACCESS_HTTP_API " + TOKEN_ACCESS_HTTP_API);
    }

    private static void setDefaultValue() {
        TOKEN_ACCESS_HTTP_API = "1681580244:AAGqufS0n31ui61otqB6HOUXOi-Gb_lGAgE";//prop.getProperty("TOKEN_ACCESS_HTTP_API");
        TOKEN_ACCESS_HTTP_API_NOTI = "1681580244:AAGqufS0n31ui61otqB6HOUXOi-Gb_lGAgE";//prop.getProperty("TOKEN_ACCESS_HTTP_API_NOTI");
        USER_RECEIPTT_NOTI_CARD = -481464015;//Long.parseLong(prop.getProperty("USER_RECEIPTT_NOTI_CARD"));
        USER_RECEIPTT_NOTI_BANK = -428440732;//Long.parseLong(prop.getProperty("USER_RECEIPTT_NOTI_BANK"));
    }

    public static String TOKEN_ACCESS_HTTP_API;
    public static String TOKEN_ACCESS_HTTP_API_NOTI;
    public static long USER_RECEIPTT_NOTI_CARD;
    public static long USER_RECEIPTT_NOTI_BANK;

}
