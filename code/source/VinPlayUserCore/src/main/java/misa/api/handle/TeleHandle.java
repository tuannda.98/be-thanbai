package misa.api.handle;

import casio.king365.core.HCMap;
import casio.king365.service.MongoDbService;
import casio.king365.service.impl.MongoDbServiceImpl;
import casio.king365.util.KingUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.UpdatesListener;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.request.Keyboard;
import com.pengrad.telegrambot.model.request.KeyboardButton;
import com.pengrad.telegrambot.model.request.ReplyKeyboardMarkup;
import com.pengrad.telegrambot.model.request.ReplyKeyboardRemove;
import com.pengrad.telegrambot.request.SendMessage;
import com.pengrad.telegrambot.response.SendResponse;
import com.vdurmont.emoji.EmojiParser;
import com.vinplay.usercore.dao.impl.OtpDaoImpl;
import com.vinplay.usercore.dao.impl.SecurityDaoImpl;
import com.vinplay.usercore.dao.impl.UserDaoImpl;
import com.vinplay.usercore.service.SecurityService;
import com.vinplay.usercore.service.UserService;
import com.vinplay.usercore.service.impl.SecurityServiceImpl;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.utils.StringUtils;
import misa.api.config.TeleConfig;
import misa.api.service.OtpService;
import misa.api.service.imp.OtpServiceImp;
import misa.core.utils.HttpUtils;
import misa.core.utils.MisaUtils;
import misa.data.AccountTeleLink;
import org.apache.log4j.Logger;
import vn.yotel.yoker.dao.CashinItemDao;
import vn.yotel.yoker.dao.RequestCardDao;
import vn.yotel.yoker.dao.impl.CashinItemDaoImpl;
import vn.yotel.yoker.dao.impl.RequestCardDaoImpl;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class TeleHandle {
    public static String share_number_emoji = EmojiParser.parseToUnicode(":phone: Share your phone number");
    public static String share_number_emoji_OTP = EmojiParser.parseToUnicode(":mag_right: LẤY OTP");
    public static String share_number_emoji_OTP_REG = EmojiParser.parseToUnicode(":pencil2: ĐĂNG KÝ OTP");
    private static TelegramBot botNotifi = null;
    private static TelegramBot botOtp = null;

    public static TelegramBot botBotNoifiInstance() {
        if (botNotifi == null) {
            botNotifi = new TelegramBot(TeleConfig.TOKEN_ACCESS_HTTP_API_NOTI);
        }
        return botNotifi;
    }

    private static final Logger logger = Logger.getLogger((String) "api");
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private static HazelcastInstance client;
    private static IMap<String, UserCacheModel> userMap;
    private static UserService userService;
    private static CashinItemDao cashinItemDao;
    private static RequestCardDao requestCardDao;
    private static SecurityService securityService;
    private static OtpService otpService = new OtpServiceImp();

    public static void start() {

        TelegramBot bot = botOtp = new TelegramBot(TeleConfig.TOKEN_ACCESS_HTTP_API);
        teleUpdateMessageListener(bot);
        System.out.println("Bot started!");
    }

    public static void teleUpdateMessageListener(TelegramBot bot) {
        init();
        bot.setUpdatesListener(updates -> {
            for (Update update : updates) {
                if (update.message() == null) continue;
                try {
//                    System.out.println(update);
//                     KingUtil.printLog("teleUpdateMessageListener() update.message(): "+update.message().toString());
                    if (update.message().text() != null
                            && update.message().text().equalsIgnoreCase("/start")) {
                        AccountTeleLink accountTeleLink = otpService.getAccountTeleLinkByIdUser(String.valueOf(update.message().from().id()));
                        if (accountTeleLink != null) {
                            /*logger.debug("l Tele start - accountTeleLink: " + accountTeleLink + ", teleid: " + String.valueOf(update.message().from().id()));
                            System.out.println("s Tele start - accountTeleLink: " + accountTeleLink + ", teleid: " + String.valueOf(update.message().from().id()));
                            UserModel userModel = userService.getUserByUserName("testtest5");
                            UserCacheModel user = (UserCacheModel)userMap.get("testtest5");
                            logger.debug("l Tele start - user mobile: " +  user.getMobile());
                            System.out.println("s Tele start - user mobile: " +  user.getMobile());*/
                            StringBuilder sb = new StringBuilder();
                            sb.append("- Bấm vào đây: ĐĂNG KÝ OTP để lấy OTP, xác thực cập nhật SĐT.\n");
                            sb.append("- Bấm vào đây: LẤY OTP để lấy mã OTP xác thực các giao dịch trong game.");
                            System.out.println("*** chat id" + update.message().chat().id());
                            logger.debug("*** chat id" + update.message().chat().id());
                            sendMessageWithKeyboardOTP(bot, update.message().chat().id(), sb.toString());
                        } else {
                            StringBuilder sb1 = new StringBuilder();
                            sb1.append(" Bấm vào \"Share your phone number\" để lấy mã OTP xác nhận SDT.");
                            sendMessageWithKeyboardPhone(bot, update.message().chat().id(), sb1.toString());
                        }
                    } else if (update.message().text() != null && update.message().text().contains("/start ")) {
                        // B1 trong Xác thực OTP kiểu mới
                        // Data gửi lên sẽ có dạng /start <accesstorken>
                        KingUtil.printLog("teleUpdateMessageListener() tim thay /start with param");
                        String accesstoken = update.message().text().replace("/start ", "");
                        KingUtil.printLog("teleUpdateMessageListener() accesstoken: " + accesstoken);
                        AccountTeleLink accountTeleLink = otpService.getAccountTeleLinkByIdUser(String.valueOf(update.message().from().id()));
                        if (accountTeleLink == null) {
                            Optional<UserCacheModel> optinalUser = userMap.entrySet().stream()
                                    .filter(x -> x.getValue().getAccessToken().equals(accesstoken))
                                    .map(Map.Entry::getValue)
                                    .findFirst();
                            KingUtil.printLog("teleUpdateMessageListener() 1");
                            UserCacheModel user = optinalUser.orElse(null);
                            if (user == null) {
                                sendMessageWithKeyboardPhone(bot, update.message().chat().id(),
                                        "Có lỗi, vui lòng kiểm tra lại.");
                                KingUtil.printLog("teleUpdateMessageListener() user == null, stop");
                                continue;
                            }
                            if (user.getMobile() != null || user.isHasMobileSecurity()) {
                                sendMessageWithKeyboardPhone(bot, update.message().chat().id(),
                                        "User này đã được xác thực");
                                KingUtil.printLog("teleUpdateMessageListener() user.getMobile() != null, stop");
                                continue;
                            }
                            KingUtil.printLog("teleUpdateMessageListener() 2 user nickname: " + user.getNickname());
                            // Liên kết tài khoản tele và tài khoản game
                            // Xóa các tài khoản AccountTeleLink đã gán trước đó nhưng chưa xác thực
                            MongoDbService mongoDbService = new MongoDbServiceImpl();
                            Map<String, Object> ma = new HashMap<>();
                            ma.put("nickName", user.getNickname());
                            mongoDbService.del("AccountTeleLink", ma);
                            // Liên kết id tele và tài khoản user
                            AccountTeleLink newAccountTeleLink = new AccountTeleLink();
                            newAccountTeleLink.setIdUserTele(String.valueOf(update.message().from().id()));
                            newAccountTeleLink.setNickName(user.getNickname());
                            otpService.insertAccountTeleLink(newAccountTeleLink);
                            sendMessageWithKeyboardPhone(bot, update.message().chat().id(),
                                    "Vui lòng Bấm vào \"Share your phone number\" để xác nhận SDT.");
                            KingUtil.printLog("teleUpdateMessageListener() update AccountTeleLink");
                        } else {
                            sendMessageWithKeyboardPhone(bot, update.message().chat().id(),
                                    "Tài khoản tele này đã kết nối với tài khoản game. (" + String.valueOf(update.message().from().id()) + ")");
                        }

                    } else if (update.message().text() != null && (update.message().text().equalsIgnoreCase(share_number_emoji_OTP) || update.message().text().equalsIgnoreCase("/OTP") || update.message().text().equalsIgnoreCase("OTP"))) {
                        //GET OTP
                        AccountTeleLink accountTeleLink = otpService.getAccountTeleLinkByIdUser(String.valueOf(update.message().from().id()));
                        if (accountTeleLink != null) {
                            UserModel userModel = null;
                            if (userMap.containsKey(accountTeleLink.getNickName())) {
                                userModel = (UserModel)userMap.get(accountTeleLink.getNickName());
                            } else {
                                UserDaoImpl userDao = new UserDaoImpl();
                                userModel = userDao.getUserByNickName(accountTeleLink.getNickName());
                            }
                            // UserModel userModel = userService.getUserByNickName(accountTeleLink.getNickName());
                            try {
                                if (userModel != null) {
                                    String mobileTele = accountTeleLink.getMobile().trim();
                                    if (mobileTele.contains("+")) {
                                        mobileTele = mobileTele.substring(1);
                                    }
                                    /*if (userModel.getMobile().equalsIgnoreCase(mobileTele)) {
                                        // String otp = OTPHandle.getOTP(userModel.getNickname());

                                    } else {
                                        StringBuilder sb2 = new StringBuilder();
                                        sb2.append("Số điện thoại liên kết và số điên thoại trên cổng game không trùng khớp.");
                                        sb2.append("Có thể số điện thoại đã bị thay đổi. vui lòng cập nhật lại.");
                                        sendMessageWithKeyboardOTP(bot, update.message().chat().id(), sb2.toString());
                                    }*/
                                    String otp = StringUtils.randomStringNumber(5);
                                    OtpDaoImpl otpDao = new OtpDaoImpl();
                                    otpDao.updateOtpSMS(userModel.getMobile(), otp, "OZZ OTP");
                                    StringBuilder sb2 = new StringBuilder();
                                    sb2.append("Mã OTP của bạn là: ");
                                    sb2.append(otp).append(" .");
                                    sb2.append("Mã OTP có giới hạn trong 3 phút.");
                                    sendMessageWithKeyboardOTP(bot, update.message().chat().id(), sb2.toString());

                                } else {
                                    sendMessageWithKeyboardOTP(bot, update.message().chat().id(), "Tài khoản cổng game liên kết với tele không tồn tại.");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                sendMessageWithKeyboardOTP(bot, update.message().chat().id(), "Hệ thống xử lý lỗi. Vui lòng thử lại.");
                            }

                        } else {
                            StringBuilder sb1 = new StringBuilder();
                            sb1.append("Tài khoản tele chưa được liên kết với bất kỳ tài khoản nào trong game.");
                            sendMessageWithKeyboardOTP(bot, update.message().chat().id(), sb1.toString());
                        }
                    } else if (update.message().text() != null && (update.message().text().equalsIgnoreCase(share_number_emoji_OTP_REG) || update.message().text().equalsIgnoreCase("/OTPREG") || update.message().text().equalsIgnoreCase("OTPREG"))) {
                        StringBuilder sb1 = new StringBuilder();
                        sb1.append(" Bấm vào \"Share your phone number\" để lấy mã OTP xác nhận SDT.");
                        sendMessageWithKeyboardPhone(bot, update.message().chat().id(), sb1.toString());
                    } else {
                        if (update.message().contact() != null) {
                            //SHARE PHONE NUMBER TO REGISTER NEW OTP
                            logger.debug((Object) ("TeleHandle share phone"));
                            AccountTeleLink accountTeleLink = otpService.getAccountTeleLinkByIdUser(String.valueOf(update.message().from().id()));
                            if (accountTeleLink == null) {
                                logger.debug((Object) ("/SHAREMOBILE TeleHandle accountTeleLink == null"));
                                String mobile = update.message().contact().phoneNumber();
                                if (mobile.startsWith("+")) {
                                    mobile = mobile.substring(1);
                                }
                                logger.debug((Object) ("TeleHandle mobile: " + mobile));
                                String otp = OTPHandle.getOTPUpdateMobile(mobile, String.valueOf(update.message().from().id()));
                                logger.debug((Object) ("TeleHandle otp: " + otp));
                                if (otp == null) {
                                    sendMessageWithKeyboardOTP(bot, update.message().chat().id(), "Không tạo được otp, vui lòng liên hệ cskh.");
                                    continue;
                                }
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append("Mã OTP để xác nhận sdt của bạn là̀:");
                                sb2.append(otp).append(".");
                                sb2.append("Mã OTP có giới hạn trong 3 phút.");
                                sendMessageWithKeyboardOTP(bot, update.message().chat().id(), sb2.toString());
                            } else {
                                if (accountTeleLink.getMobile() == null
                                        || (accountTeleLink.getMobile() != null && accountTeleLink.getMobile().equals(""))) {
                                    // Bước 2 xác thực sdt Tele kiểu mới
                                    String mobile = update.message().contact().phoneNumber();
                                    otpService.updateMobileAccountTeleLink(accountTeleLink, mobile);
                                    sendMessageWithKeyboardOTP(bot, update.message().chat().id(),
                                            "Đã nhận được SDT xác thực. Hãy quay trở lại game để nhận phần thưởng nhé!");

                                    // Update mobile cho user
                                    UserCacheModel user = userMap.get(accountTeleLink.getNickName());
                                    byte reUpdateMobile = securityService.updateMobile(user.getNickname(), mobile); // update cache
                                    KingUtil.printLog("reUpdateMobile: " + reUpdateMobile);
                                    securityService.activeMobile(user.getNickname(), false);        // status cache
                                    SecurityDaoImpl securityDaoImpl = new SecurityDaoImpl();
                                    securityDaoImpl.updateUserInfo(user.getId(), mobile, 4);        // luu vao db

                                    user = userMap.get(user.getNickname());
                                    KingUtil.printLog("teleUpdateMessageListener() 2 user nickname: " + user.getNickname() + ", mobile: " + user.getMobile() + ", isMobileSecure: " + user.isHasMobileSecurity());

                                    // Tiến hành cộng 5k tiền thưởng
                                    // Gửi MailBox
                                    // Tiến hành trong UserService
                                    accountTeleLink = otpService.getAccountTeleLinkByIdUser(accountTeleLink.getIdUserTele());
                                    if (accountTeleLink.getMobile() != null && !accountTeleLink.getMobile().equals(""))
                                        userService.bonusFirstTimeVerifyMobileAndBonus(accountTeleLink.getNickName());
                                } else {
                                    logger.debug((Object) ("TeleHandle accountTeleLink: " + accountTeleLink));
                                    StringBuilder sb1 = new StringBuilder();
                                    sb1.append("Tài khoản tele đã được liên kết với một tài khoản khác trong game.");
                                    sendMessageWithKeyboardOTP(bot, update.message().chat().id(), sb1.toString());
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    KingUtil.printException("TeleHandle", e);
                }
            }
            return UpdatesListener.CONFIRMED_UPDATES_ALL;
        });
    }

    private static boolean init() {
        if ((client = HazelcastClientFactory.getInstance()) == null) {
            logger.debug((Object) "CardRequestActionProcessor - init(), Lỗi khởi tạo hazelcast");
        }
        if (userMap == null)
            userMap = HCMap.getUsersMap();
        if (cashinItemDao == null)
            cashinItemDao = new CashinItemDaoImpl();
        if (userService == null)
            userService = new UserServiceImpl();
        if (requestCardDao == null)
            requestCardDao = new RequestCardDaoImpl();
        if (securityService == null)
            securityService = new SecurityServiceImpl();
        return true;
    }

    public static void sendMessageTeleToUser(String nickname, String content) {
        KingUtil.printLog("sendMessageTeleToUser() nickname: "+nickname+", content: "+content);
        AccountTeleLink accountTeleLink = otpService.getAccountTeleLinkByNickname(nickname);
        KingUtil.printLog("sendMessageTeleToUser() accountTeleLink nickname: "+accountTeleLink.getNickName()+", teleid: "+accountTeleLink.getIdUserTele());
        SendMessage sendMessage = new SendMessage(accountTeleLink.getIdUserTele(), content);
        Keyboard replyKeyboardRemove = new ReplyKeyboardRemove();
        sendMessage.replyMarkup(replyKeyboardRemove);
        if(botOtp == null)
            botOtp = new TelegramBot(TeleConfig.TOKEN_ACCESS_HTTP_API);
        SendResponse response = botOtp.execute(sendMessage);
        KingUtil.printLog("sendMessageTeleToUser() response: "+response);
    }

    private static void sendMessageWithoutKeyboard(TelegramBot bot, long id, String content) {
        SendMessage sendMessage = new SendMessage(id, content);
        Keyboard replyKeyboardRemove = new ReplyKeyboardRemove();
        sendMessage.replyMarkup(replyKeyboardRemove);
        SendResponse response = bot.execute(sendMessage);
    }

    private static void sendMessageWithKeyboardOTP(TelegramBot bot, long id, String content) {
        SendMessage sendMessage = new SendMessage(id, content);
        Keyboard keyboard = new ReplyKeyboardMarkup(
                new KeyboardButton[]{
                        new KeyboardButton(TeleHandle.share_number_emoji_OTP),
                        new KeyboardButton(TeleHandle.share_number_emoji_OTP_REG)
                }
        );

        ((ReplyKeyboardMarkup) keyboard).resizeKeyboard(true);
        sendMessage.replyMarkup(keyboard);
        SendResponse response = bot.execute(sendMessage);
    }

    private static void sendMessageWithKeyboardPhone(TelegramBot bot, long id, String content) {
        SendMessage sendMessage = new SendMessage(id, content);
        Keyboard keyboard = new ReplyKeyboardMarkup(
                new KeyboardButton[]{
                        new KeyboardButton(TeleHandle.share_number_emoji).requestContact(true)
                }
        );

        ((ReplyKeyboardMarkup) keyboard).resizeKeyboard(true);
        sendMessage.replyMarkup(keyboard);
        SendResponse response = bot.execute(sendMessage);
    }

    public static void sendNoti2User(String nickname, String value, String provider) {
        String pv = convertProvider(provider);
        if (pv != null) {
            TelegramBot bot = botBotNoifiInstance();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Người chơi \"").append(nickname).append("\"");
            stringBuilder.append(" thực hiện đổi thẻ ").append(pv);
            stringBuilder.append(" ").append(value);
            sendMessageWithoutKeyboard(bot, TeleConfig.USER_RECEIPTT_NOTI_CARD, stringBuilder.toString());
        }
    }

    public static void sendNotiBank2User(String nickname, String status, String value, String bankname, String cardNumber, String cardAccount, String codeRefBank, String date) {
        TelegramBot bot = botBotNoifiInstance();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Người chơi \"").append(nickname).append("\"");
        stringBuilder.append(" thực hiện ").append(status.equals("0") ? "rút " : "nạp ");
        stringBuilder.append(commaSeparateNumber(value)).append(" qua ngân hàng: ").append(bankname.toUpperCase());
        stringBuilder.append(". ").append(cardAccount).append(" ***").append(cardNumber.substring(cardNumber.length() - 5)).append(",");
        if (status.equals("1")) {
            stringBuilder.append("MGD: ").append(codeRefBank).append(",");
        }
        stringBuilder.append(date);
        sendMessageWithoutKeyboard(bot, TeleConfig.USER_RECEIPTT_NOTI_BANK, stringBuilder.toString());
    }

    public static void sendNotiTranfer2User(String nickname, String money, String time, String content, String totalMoney, boolean isReceiver, long userReceipt) {
        if (botOtp != null) {
            StringBuilder stringBuilder = new StringBuilder(isReceiver ? "NHẬN: " : "CHUYỂN: ");
            stringBuilder.append(time).append(" ").append(nickname).append(isReceiver ? " +" : " -").append(commaSeparateNumber(money));
            stringBuilder.append(". Nội dung: ").append(content).append(". ");
            stringBuilder.append("Số dư hiện tại: ").append(commaSeparateNumber(totalMoney)).append(".");
            sendMessageWithKeyboardOTP(botOtp, userReceipt, stringBuilder.toString());
        }
    }

    public static String commaSeparateNumber(String value) {
        try {
            long val = Long.parseLong(value);
            DecimalFormat formatter = new DecimalFormat("###,###,###");
            return formatter.format(val).replace(",", ".");
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return "0";
        }

    }

    public static JsonObject sendMessage(String message, String chai_id) {
        try {
            StringBuilder strUrl = new StringBuilder("https://api.telegram.org/bot");
            strUrl.append(TeleConfig.TOKEN_ACCESS_HTTP_API_NOTI).append("/sendMessage");

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("chat_id", chai_id);
            jsonObject.addProperty("text", message);
            String result = HttpUtils.httpPostRequest(strUrl.toString(), jsonObject);
            return MisaUtils.toJsonObject(result);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String convertProvider(String provider) {
        if (provider != null) {
            provider = provider.trim();
            if (provider.equalsIgnoreCase("VTT") || provider.equalsIgnoreCase("VT") || provider.equalsIgnoreCase("viettel")) {
                return "VIETTEL";
            }

            if (provider.equalsIgnoreCase("VMS") || provider.equalsIgnoreCase("MOBI") || provider.equalsIgnoreCase("mobiphone") || provider.equalsIgnoreCase("mobifone")) {
                return "MOBIFONE";
            }

            if (provider.equalsIgnoreCase("VNP") || provider.equalsIgnoreCase("VINA") || provider.equalsIgnoreCase("vinaphone")) {
                return "VINAPHONE";
            }
        }
        return null;
    }
}
