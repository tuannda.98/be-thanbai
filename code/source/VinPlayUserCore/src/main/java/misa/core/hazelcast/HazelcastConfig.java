package misa.core.hazelcast;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class HazelcastConfig {
    public static String HAZELCAST_CONFIG_FILE = "config/hazelcast.properties";
    public static String ADDRESS = "";
    public static String GROUP_NAME = "";
    public static String GROUP_PASS = "";

    public static void init() throws IOException {
        String configFile = HAZELCAST_CONFIG_FILE;
        Properties prop = new Properties();
        try (InputStream in = new FileInputStream(configFile)) {
            prop.load(in);
            ADDRESS = prop.getProperty("address");
            GROUP_NAME = prop.getProperty("group_name");
            GROUP_PASS = prop.getProperty("group_pass");
        }
    }
}
