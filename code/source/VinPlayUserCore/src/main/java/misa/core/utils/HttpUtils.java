package misa.core.utils;

import com.google.common.base.Charsets;
import com.google.gson.JsonObject;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpUtils {
    public static String requestJson(String sUrl) throws IOException {
        StringBuilder res = new StringBuilder();
        URL url = new URL(sUrl);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Accept", "application/json");
        try (InputStream inputStream = conn.getInputStream();
             InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charsets.UTF_8);
             BufferedReader in = new BufferedReader(inputStreamReader)) {
            String output;
            while ((output = in.readLine()) != null) {
                res.append(output);
            }
        }
        conn.disconnect();
        return res.toString();
    }

    public static String httpGetRequest(String url) {
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            HttpGet request = new HttpGet(url);
            HttpEntity entity;
            try (CloseableHttpResponse response = httpclient.execute(request)) {
                entity = response.getEntity();
            }
            entity.getContentEncoding();
            String json = EntityUtils.toString(entity);
            EntityUtils.consume(entity);
            return json;
        } catch (
                Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String httpPostRequest(String url, JsonObject params) {
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            HttpPost request = new HttpPost(url);
            StringEntity requestEntity = new StringEntity(params.toString());
            request.addHeader("content-type", "application/x-www-form-urlencoded");
            request.setEntity(requestEntity);
            HttpEntity entity;
            try (CloseableHttpResponse response = httpclient.execute(request)) {
                entity = response.getEntity();
                String json = EntityUtils.toString(entity);
                EntityUtils.consume(entity);
                return json;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String httpPostRequest(String url, JsonObject params, String contentType) {
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            HttpPost request = new HttpPost(url);
            StringEntity requestEntity = new StringEntity(params.toString());
            request.addHeader("content-type", contentType);
            request.setEntity(requestEntity);
            HttpEntity entity;
            try (CloseableHttpResponse response = httpclient.execute(request)) {
                entity = response.getEntity();
            }
            String json = EntityUtils.toString(entity);
            EntityUtils.consume(entity);
            return json;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String sendGetRequest(String url) throws Exception {

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("GET");
        StringBuffer response;
        try (BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()))) {
            String inputLine;
            response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
        }
        return response.toString();
    }
}
