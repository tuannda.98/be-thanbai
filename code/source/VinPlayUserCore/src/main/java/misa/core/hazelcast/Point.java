package misa.core.hazelcast;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Point implements Serializable{
    private static Logger logger = LoggerFactory.getLogger(Point.class.getName());
    public static void main(String[] args){
        try {
            PropertyConfigurator.configure("pools/log4j.properties");
            logger.info("Logger is initialed");
            HazelcastClientFactory.start();
            System.out.println(System.currentTimeMillis());
            for(int i = 0; i < 10; i++){
                HazelcastInstance instance = HazelcastClientFactory.getSingleClient();
                IMap<String, Point> points = instance.getMap("points");
                points.destroy();
            }
            for(int i = 0; i < 10; i++) {
                HazelcastInstance instance = HazelcastClientFactory.getSingleClient();
                PointTask task = new PointTask(instance, i, 0);
                service.execute(task);
            }
            for(int i = 0; i < 10; i++) {
                HazelcastInstance instance = HazelcastClientFactory.getSingleClient();
                PointTask task = new PointTask(instance, i, 0);
                service.execute(task);
            }
            for(int i = 0; i < 10; i++) {
                HazelcastInstance instance = HazelcastClientFactory.getSingleClient();
                PointTask task = new PointTask(instance, i, 0);
                service.execute(task);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static ExecutorService service = Executors.newFixedThreadPool(
            10, new ThreadFactoryBuilder().setNameFormat("Point-thread-%d").build());
    public static class PointTask implements Runnable{
        HazelcastInstance instance = null;
        int x = 0;
        int y = 0;
        public PointTask(HazelcastInstance instance, int x, int y){
            this.instance = instance;
            this.x = x;
            this.y = y;
        }
        @Override
        public void run() {
            IMap<String, Point> map = instance.getMap("points");
            Point p = new Point(x, y);
            Point p1 = map.get(p.toString());
            if(p1 == null){
                p1 = p;
                map.put(p.toString(), p1);
            }else{
                p1.y += 1;
                map.set(p.toString(), p1);
            }
            System.out.println(System.currentTimeMillis());
        }
    }

    public int x = 0;
    public int y = 0;
    public Point(int x, int y){
        this.x = x;
        this.y = y;
    }
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(x).append("_").append(y);
        return sb.toString();
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }
    @Override
    public boolean equals(Object other){
        if(other instanceof Point){
            Point p = (Point)other;
            return p.x == x && p.y == y;
        }
        return false;
    }
}
