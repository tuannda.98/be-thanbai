package misa.core.utils;

import java.util.Calendar;
import java.util.Random;

public class StringUtils {
    public static Random rd = new Random();
    public static String randomStringNumber(int length) {
        return randomString('0', '9', length);
    }
    private static String randomString(char from, char to, int length){
        int range = to - from;
        char[] buf = new char[length];
        for (int i = 0; i < buf.length; i++){
            int index = rd.nextInt(range);
            buf[i] = (char) ('a' + index);
        }
        return new String(buf);
    }

    public static String randomString(int length) {
        return randomString('a', 'z', length);
    }

    public static String randomStringUpper(int length) {
        return randomString('A', 'Z', length);
    }

    public static String getAlphaNumericString(int length) {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            int index = (int)(AlphaNumericString.length() * Math.random());
            sb.append(AlphaNumericString.charAt(index));
        }

        return sb.toString();
    }

    public static String replaceSpace(String string){
        StringBuilder sb = new StringBuilder();
        for( int i =0; i < string.length(); i ++){
            if (string.charAt(i) == ' '){
                sb.append("+");
            }else {
                sb.append(string.charAt(i));
            }
        }
        return sb.toString();
    }

    public static String phoneUtils(String phone){
        StringBuilder sb = new StringBuilder();
        phone = phone.trim();
        if (phone.startsWith("+")){
            sb.append(phone.substring(1));
        } else {
            sb.append(phone);
        }
        return sb.toString();
    }
    public static boolean checkPhoneFormat(String phone){
        StringBuilder sb = new StringBuilder(phone);
        String mb = sb.toString();
        if(phone.startsWith("+")){
            mb = mb.substring(1);
        }
        if (mb.startsWith("0")){
            StringBuilder sb1 = new StringBuilder();
            sb1.append("84").append(mb.substring(1));
            mb = sb1.toString();
        }
        if (mb.startsWith("84")){
            if(mb.length()==11) return true;
        }
        return false;
    }
    public static int checkRegexPhoneUtils(String phone){
        if(phone.startsWith("0")) {
            return 1;
        }
        String REGEX = "[0-9]{10,}";

        if(!phone.matches(REGEX)) {
            return 2;
        }

        return 0;
    }

    public static String getUserFromContentSMS(String sms, String shortCode){
        int count = 0;
            for(int i = 0 ; i < sms.length(); i ++){
                if (sms.charAt(i) == ' ' && sms.charAt(i +1) != ' '){
                    count ++;
                }
                if ( count == 2){
                    return sms.substring(i +1).trim();
                }
            }
        return null;
    }


    public static String reFormatCardata(String cardData){
        int count = 0;
        for(int i = 0 ; i < cardData.length(); i ++){
            if (cardData.charAt(i) == '|'){
                count ++;
            }
            if ( count == 4){
                return cardData.substring(0, i);
            }
        }
        return cardData;
    }

    public static String convertFromUTF8(String s) {
        String out = null;
        try {
            out = new String(s.getBytes("ISO-8859-1"), "UTF-8");
        } catch (java.io.UnsupportedEncodingException e) {
            return null;
        }
        return out;
    }

    public static String convertToUTF8(String s) {
        String out = null;
        try {
            out = new String(s.getBytes("UTF-8"));
        } catch (java.io.UnsupportedEncodingException e) {
            return null;
        }
        return out;
    }

}
