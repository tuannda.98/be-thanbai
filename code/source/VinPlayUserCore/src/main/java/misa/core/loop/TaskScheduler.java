package misa.core.loop;

import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TaskScheduler
{
    private static TaskScheduler ins = null;
    public static TaskScheduler instance(){
        if(ins == null){
            ins = new TaskScheduler(1);
        }
        return ins;
    }
    private static AtomicInteger schedulerId = new AtomicInteger(0);
    private final ScheduledThreadPoolExecutor taskScheduler;
    private final String serviceName;
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private TaskScheduler(int threadPoolSize)
    {
        serviceName = (new StringBuilder("TaskScheduler-")).append(schedulerId.getAndIncrement()).toString();
        taskScheduler = new ScheduledThreadPoolExecutor(threadPoolSize);
    }

    public void init(Object o)
    {
        logger.info((new StringBuilder(String.valueOf(serviceName))).append(" started.").toString());
    }

    public void destroy(Object o)
    {
        List awaitingExecution = taskScheduler.shutdownNow();
        logger.info((new StringBuilder(String.valueOf(serviceName))).append(" stopping. Tasks awaiting execution: ").append(awaitingExecution.size()).toString());
    }

    public String getName()
    {
        return serviceName;
    }

    public void handleMessage(Object obj)
    {
    }

    public void setName(String s)
    {
    }

    public ScheduledFuture schedule(Runnable task, int delay, TimeUnit unit)
    {
        logger.debug((new StringBuilder("Task scheduled: ")).append(task).append(", ").append(delay).append(" ").append(unit).toString());
        return taskScheduler.schedule(task, delay, unit);
    }

    public ScheduledFuture scheduleAtFixedRate(Runnable task, int initialDelay, int period, TimeUnit unit)
    {
        return taskScheduler.scheduleAtFixedRate(task, initialDelay, period, unit);
    }

    public Boolean remove(Runnable task)
    {
        return taskScheduler.remove(task);
    }

    public void resizeThreadPool(int threadPoolSize)
    {
        taskScheduler.setCorePoolSize(threadPoolSize);
    }

    public int getThreadPoolSize()
    {
        return taskScheduler.getCorePoolSize();
    }

}