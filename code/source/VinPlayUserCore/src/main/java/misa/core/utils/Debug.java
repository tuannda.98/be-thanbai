package misa.core.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Debug {
    private static Logger logger = LoggerFactory.getLogger("debug");
    public static void debug(String data){
        logger.debug(data);
    }
    public static void debug(String data, Object... objects){
        StringBuilder log = new StringBuilder(data);
        for(int i = 0; i < objects.length; i++){
            log.append(" ").append(objects[i]);
        }
        logger.debug(log.toString());
    }
    public static void info(String data){
        logger.info(data);
    }
    public static void info(String data, Object... objects){
        StringBuilder log = new StringBuilder(data);
        for(int i = 0; i < objects.length; i++){
            log.append(" ").append(objects[i]);
        }
        logger.info(log.toString());
    }
    public static void error(String data){
        logger.error(data);
    }
    public static void error(String data, Object... objects){
        StringBuilder log = new StringBuilder(data);
        for(int i = 0; i < objects.length; i++){
            log.append(" ").append(objects[i]);
        }
        logger.error(log.toString());
    }
}
