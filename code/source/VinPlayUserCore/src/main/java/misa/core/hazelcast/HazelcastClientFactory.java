package misa.core.hazelcast;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.client.config.ClientNetworkConfig;
import com.hazelcast.config.GroupConfig;
import com.hazelcast.core.HazelcastInstance;
import misa.core.utils.AESTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;

public class HazelcastClientFactory{
    private static Logger logger = LoggerFactory.getLogger(HazelcastClientFactory.class.getName());

    public static void start() throws IOException {
        HazelcastConfig.init();
        singleClient = newClient();
    }
    private static HazelcastInstance singleClient = null;
    public static HazelcastInstance getSingleClient(){
        if(!singleClient.getLifecycleService().isRunning()){
            singleClient = newClient();
        }
        return singleClient;
    }

    private static HazelcastInstance newClient() {
        ClientConfig clientConfig = new ClientConfig();
        ClientNetworkConfig networkConfig = clientConfig.getNetworkConfig();
        networkConfig.addAddress(HazelcastConfig.ADDRESS);
        GroupConfig groupConfig = new GroupConfig();
        groupConfig.setName(HazelcastConfig.GROUP_NAME);
//        groupConfig.setPassword(HazelcastConfig.GROUP_PASS);
        groupConfig.setPassword(AESTool.getInstance().decrypt(HazelcastConfig.GROUP_PASS));
        clientConfig.setGroupConfig(groupConfig);
        HazelcastInstance client = HazelcastClient.newHazelcastClient(clientConfig);
        logger.info("Initial new misa.core.hazelcast client instance: ", client.getName());
        return client;
    }
}
