package misa.core.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.MessageDigest;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class MisaUtils {
    public static AtomicInteger count = new AtomicInteger(0);
    public static String MAP_DISSCONNECT = "DISCONNECT_USER";
    private static Logger logger = LoggerFactory.getLogger(MisaUtils.class.getName());

    public static String tenNhaCungCapThe(String shortName) {
        switch (shortName) {
            case "VTT":
                return "VIETTEL";
            case "VMS":
                return "MOBIFONE";
            case "VNP":
                return "VINAPHONE";
            case "MGC":
                return "Megacard";
            default:
                return shortName;
        }
    }

    public static void main(String[] args) {
        String src = "password";
        System.out.println(sha256(src));
    }

    public static String encodeBase64(String src) {
        String result = Base64.getEncoder().encodeToString(src.getBytes());
        return result;
    }

    public static final Random rd = new Random();

    public static int randomAvatar() {
        return rd.nextInt(14);
    }



    public static String toMd5(String src) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(src.getBytes());
            byte byteData[] = md.digest();

            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();
        } catch (Exception e) {
            return "";
        }
    }

    public static String convertTime2String(long time) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(calendar.get(Calendar.YEAR)).append("/");
        stringBuilder.append((calendar.get(Calendar.MONTH) + 1)<10?"0":"").append((calendar.get(Calendar.MONTH) + 1)).append("/");
        stringBuilder.append(calendar.get(Calendar.DATE)<10?"0":"").append(calendar.get(Calendar.DATE)).append(" ");
        stringBuilder.append(calendar.get(Calendar.HOUR_OF_DAY)<10?"0":"").append(calendar.get(Calendar.HOUR_OF_DAY)).append(":");
        stringBuilder.append(calendar.get(Calendar.MINUTE)<10?"0":"").append(calendar.get(Calendar.MINUTE)).append(":");
        stringBuilder.append(calendar.get(Calendar.SECOND)<10?"0":"").append(calendar.get(Calendar.SECOND));
        return stringBuilder.toString();
    }

    public static String sha256(String src) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(src.getBytes());
            byte byteData[] = md.digest();
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();
        } catch (Exception e) {
            return "";
        }
    }

    public static String sha1(String src) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            md.update(src.getBytes());
            byte byteData[] = md.digest();
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();
        } catch (Exception e) {
            return "";
        }
    }

    private static Gson gson = new Gson();
    private static ObjectMapper mapper = new ObjectMapper();

    public static String toJsonStringGson(Object obj) {
        return gson.toJson(obj);
    }

    public static JsonObject toJsonObject(String json) {
        try {
            return gson.fromJson(json, JsonObject.class);
        } catch (Exception e) {
            return null;
        }
    }


    public static JsonArray toJsonArray(String json) {
        try {
            return gson.fromJson(json, JsonArray.class);
        } catch (Exception e) {
            return null;
        }
    }

    public static long getToday() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, count.get());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }

    public static long getStartday(long time) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }

    public static long getStartHour(long time) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }

    public static long getStartDayInMonth(long time){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }
    public static long getStartTimeLoop(long time, int timeLoopMinute) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        calendar.set(Calendar.MINUTE, (int)(calendar.get(Calendar.MINUTE)/timeLoopMinute) * timeLoopMinute);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }

    public static long formatDate(int addday, long time) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        if (addday != 0) {
            calendar.add(Calendar.DATE, addday);
        }
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }

    public static JsonArray formatArray(JsonArray array, String totalName, String grouptype) {
        JsonObject object = null;
        JsonArray objectArray = null;
        long total = 0;
        JsonArray arrayOutPut = new JsonArray();
        for (int i = 0; i < array.size(); i++) {
            JsonObject element = array.get(i).getAsJsonObject();
            if (object == null) {
                object = new JsonObject();
                object.add(grouptype, element.get(grouptype));
                if (totalName.length() > 0) {
                    total += element.get(totalName).getAsLong();
                }
                objectArray = new JsonArray();
                objectArray.add(array.get(i));
                if (i == array.size() - 1) {
                    if (totalName.length() > 0) {
                        object.addProperty("total", total);
                    }
                    object.add("array", objectArray);
                    arrayOutPut.add(object);
                }
            } else {
                if (MisaUtils.formatDate(0, object.get(grouptype).getAsLong()) != MisaUtils.formatDate(0, element.get(grouptype).getAsLong())) {
                    if (totalName.length() > 0) {
                        object.addProperty("total", total);
                    }
                    object.add("array", objectArray);
                    arrayOutPut.add(object);
                    object = new JsonObject();
                    objectArray = new JsonArray();
                    total = 0;
                    if (totalName.length() > 0) {
                        total += element.get(totalName).getAsLong();
                    }
                    object.add(grouptype, element.get(grouptype));
                    objectArray.add(array.get(i));
                    if (i == array.size() - 1) {
                        if (totalName.length() > 0) {
                            object.addProperty("total", total);
                        }
                        object.add("array", objectArray);
                        arrayOutPut.add(object);
                    }
                } else {
                    objectArray.add(array.get(i));
                    if (totalName.length() > 0) {
                        total += element.get(totalName).getAsLong();
                    }
                    if (i == array.size() - 1) {
                        if (totalName.length() > 0) {
                            object.addProperty("total", total);
                        }
                        object.add("array", objectArray);
                        arrayOutPut.add(object);
                    }
                }
            }
        }

        return arrayOutPut;
    }






    public static List<Long> getDaysBetweenTwoDay(long start, long end) {
        ArrayList<Long> days = new ArrayList<>();
        Calendar startTime = Calendar.getInstance();
        startTime.setTimeInMillis(start);
        startTime.set(Calendar.HOUR_OF_DAY, 0);
        startTime.set(Calendar.MINUTE, 0);
        startTime.set(Calendar.SECOND, 0);
        Calendar endTime = Calendar.getInstance();
        endTime.setTimeInMillis(end);
        endTime.set(Calendar.HOUR_OF_DAY, 23);
        endTime.set(Calendar.MINUTE, 59);
        endTime.set(Calendar.SECOND, 59);
        long time = startTime.getTimeInMillis();
        do {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(time);
            days.add(calendar.getTimeInMillis());
            calendar.add(Calendar.DATE, 1);
            time = calendar.getTimeInMillis();
        } while (time < endTime.getTimeInMillis());
        return days;
    }

    public static Calendar calendarInstanceWithTimezone(String timezone) {
        Calendar calendar = Calendar.getInstance();
        TimeZone tz = TimeZone.getTimeZone(timezone);
        calendar.setTimeZone(tz);
        return calendar;
    }



}
