package misa.core.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class GsonUtils {
    public static Gson gson = new Gson();
    public static Gson beautyGson = new GsonBuilder().setPrettyPrinting().create();
    public static String toJsonStringGson(Object obj) {
        return gson.toJson(obj);
    }
    public static String toBeautifulJsonStringGson(Object obj) {
        return beautyGson.toJson(obj);
    }

    public static JsonArray toJsonArray(String json) {
        try {
            return gson.fromJson(json, JsonArray.class);
        } catch (Exception e) {
            return null;
        }
    }

    public static JsonObject toJsonObject(String json) {
        try {
            return gson.fromJson(json, JsonObject.class);
        } catch (Exception e) {
            return null;
        }
    }
    public static Object toJsonObject(String json, Class clazz) {
        return gson.fromJson(json, clazz);
    }
//
//    public static JsonArray toJsonArray(int[][] arr) {
//        try {
//            JsonArray array = new JsonArray();
//            for(int i = 0; i < arr.length; i++){
//                array.add(toJsonArray(arr[i]));
//            }
//            return array;
//        } catch (Exception e) {
//            return null;
//        }
//    }
//
//    public static JsonArray toJsonArray(long[][] arr) {
//        try {
//            JsonArray array = new JsonArray();
//            for(int i = 0; i < arr.length; i++){
//                array.add(toJsonArray(arr[i]));
//            }
//            return array;
//        } catch (Exception e) {
//            return null;
//        }
//    }
//
//    public static JsonArray toJsonArray(double[][] arr) {
//        try {
//            JsonArray array = new JsonArray();
//            for(int i = 0; i < arr.length; i++){
//                array.add(toJsonArray(arr[i]));
//            }
//            return array;
//        } catch (Exception e) {
//            return null;
//        }
//    }
//
//    public static JsonArray toJsonArray(int[] arr){
//        JsonArray array = new JsonArray();
//        for(int i = 0; i < arr.length; i++){
//            array.add(arr[i]);
//        }
//        return array;
//    }
//
//    public static JsonArray toJsonArray(long[] arr){
//        JsonArray array = new JsonArray();
//        for(int i = 0; i < arr.length; i++){
//            array.add(arr[i]);
//        }
//        return array;
//    }
//
//    public static JsonArray toJsonArray(double[] arr){
//        JsonArray array = new JsonArray();
//        for(int i = 0; i < arr.length; i++){
//            array.add(arr[i]);
//        }
//        return array;
//    }

    public static int[] toIntArray(JsonArray arr){
        int[] array = new int[arr.size()];
        for(int i = 0; i < arr.size(); i++){
            array[i] = arr.get(i).getAsInt();
        }
        return array;
    }
    public static double[] toDoubleArray(JsonArray arr){
        double[] array = new double[arr.size()];
        for(int i = 0; i < arr.size(); i++){
            array[i] = arr.get(i).getAsDouble();
        }
        return array;
    }

    public static int[][] toTwoDimensionIntArray(JsonArray arr){
        int[][] result = new int[arr.size()][];
        for(int i = 0; i < arr.size(); i++){
            JsonArray jsonArray = arr.get(i).getAsJsonArray();
            result[i] = toIntArray(jsonArray);
        }
        return result;
    }
}
