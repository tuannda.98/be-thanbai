package misa.utils;

public class StringUtils {
    public static String formatPhone(String phone){
        if(phone.startsWith("0")){
            return phone;
        } else if(phone.startsWith("84")){
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("0").append(phone.substring(2,phone.length()));
            return stringBuilder.toString();
        }
        return null;
    }

    public static String phoneUtils(String phone){
        StringBuilder sb = new StringBuilder(phone);
        String mb = sb.toString();
        if(phone.startsWith("+")){
            mb = mb.substring(1);
        }
        if (mb.startsWith("0")){
            StringBuilder sb1 = new StringBuilder();
            sb1.append("84").append(mb.substring(1));
            mb = sb1.toString();
        }
        if (mb.startsWith("84")){
            if(mb.length()==11) return mb;
        }
        return "";
    }
}
