package misa.utils.debug;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Debug {
    public static Logger logger = LoggerFactory.getLogger("debug");
    public static void trace(Object ... args){
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < args.length; i++){
            sb.append(args[i]).append(" ");
        }
        logger.debug(sb.toString());
    }
    public static void info(Object ... args){
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < args.length; i++){
            sb.append(args[i]).append(" ");
        }
        logger.info(sb.toString());
    }
    public static void error(Object ... args){
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < args.length; i++){
            sb.append(args[i]).append(" ");
        }
        logger.error(sb.toString());
    }
}
