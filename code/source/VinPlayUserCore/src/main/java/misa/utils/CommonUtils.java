package misa.utils;

public class CommonUtils {
    public static String convertLangcode2Numberphonecode(String langCode) {
        switch (langCode.toLowerCase()) {
            case "vi-vn" :
                return "84";
            default:
                return null;
        }
    }
}
