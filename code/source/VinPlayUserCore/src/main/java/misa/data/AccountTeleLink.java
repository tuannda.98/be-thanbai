package misa.data;

//import misa.core.rabbitmq.message.RMQLogMessage;

import org.bson.Document;

// @Data
public class AccountTeleLink {
    private String nickName;
    private String mobile;
    private String idUserTele;

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getIdUserTele() {
        return idUserTele;
    }

    public void setIdUserTele(String idUserTele) {
        this.idUserTele = idUserTele;
    }

    public static AccountTeleLink fromDocument(Document doc) {
        AccountTeleLink mission = new AccountTeleLink();
        mission.setNickName(doc.getString("nickName"));
        mission.setMobile(doc.getString("mobile"));
        mission.setIdUserTele(doc.getString("idUserTele"));
        return mission;
    }

    @Override
    public String toString() {
        return "AccountTeleLink{" +
                "nickName='" + nickName + '\'' +
                ", mobile='" + mobile + '\'' +
                ", idUserTele='" + idUserTele + '\'' +
                '}';
    }
}
