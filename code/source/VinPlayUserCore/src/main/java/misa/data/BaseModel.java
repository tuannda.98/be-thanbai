package misa.data;


//import misa.core.pools.HikariPool;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.sql.*;

public class BaseModel implements Serializable{
    public BaseModel(){

    }

    private void updateValues(ResultSet rs) throws SQLException, IllegalAccessException {
        Field[] fields = this.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            if (field.getType() == String.class) {
                field.set(this, rs.getString(field.getName()));
            } else if (field.getType() == Integer.class || field.getType() == int.class) {
                field.set(this, rs.getInt(field.getName()));
            } else if (field.getType() == Long.class || field.getType() == long.class) {
                field.set(this, rs.getLong(field.getName()));
            } else if (field.getType() == Float.class || field.getType() == float.class) {
                field.set(this, rs.getFloat(field.getName()));
            } else if (field.getType() == Double.class || field.getType() == double.class) {
                field.set(this, rs.getDouble(field.getName()));
            }
        }
    }

    public boolean getOneByField(String fieldName) throws SQLException {
        boolean result = false;
        /*Connection connection = HikariPool.getConnection();
        try {
            StringBuilder sb = new StringBuilder("SELECT * FROM ");
            sb.append(this.getClass().getSimpleName().toLowerCase()).append(" WHERE ").append(fieldName).append( " = ?").append(" LIMIT 1");
            PreparedStatement st = connection.prepareStatement(sb.toString());
            Field field = this.getClass().getDeclaredField(fieldName);
            addParams(st, 1, field);
            ResultSet rs = st.executeQuery();
            if(rs.next()){
                this.updateValues(rs);
                result = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.close();
        }*/
        return result;
    }



    private void addParams(PreparedStatement st, int index, Field field) throws IllegalAccessException, SQLException {
        field.setAccessible(true);
        if (field.getType() == String.class) {
            st.setString(index, (String) field.get(this));
        } else if (field.getType() == Integer.class || field.getType() == int.class) {
            st.setInt(index, (Integer) field.get(this));
        } else if (field.getType() == Long.class || field.getType() == long.class) {
            st.setLong(index, (Long) field.get(this));
        } else if (field.getType() == Float.class || field.getType() == float.class) {
            st.setFloat(index, (Float) field.get(this));
        } else if (field.getType() == Double.class || field.getType() == double.class) {
            st.setDouble(index, (Double) field.get(this));
        }
    }
}
