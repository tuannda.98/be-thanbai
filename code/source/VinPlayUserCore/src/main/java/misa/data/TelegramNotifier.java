package misa.data;

import com.google.gson.JsonObject;
import misa.core.loop.TaskScheduler;
import misa.core.utils.HttpUtils;
import misa.core.utils.MisaUtils;
import misa.api.config.TeleConfig;

import java.util.concurrent.TimeUnit;

public class TelegramNotifier {
    private static final String CHAT_ID_MIRA = "793384722";

    public static void loop() {
        TaskScheduler.instance().scheduleAtFixedRate(runableCheckSystemTask, 60, 60, TimeUnit.SECONDS);
    }

    private static final Runnable runableCheckSystemTask = new RunAbleCheckSystemTask();

    private static class RunAbleCheckSystemTask implements Runnable{
        @Override
        public void run(){
            checkServer();
        }
    }

    public static JsonObject sendMessage(String message, String chai_id) {
        try {
            StringBuilder strUrl = new StringBuilder("https://api.telegram.org/bot");
            strUrl.append(TeleConfig.TOKEN_ACCESS_HTTP_API_NOTI).append("/sendMessage");
            strUrl.append("?chat_id=").append(chai_id);
            strUrl.append("&text=").append(message);
            String result = HttpUtils.httpGetRequest(strUrl.toString());
            return MisaUtils.toJsonObject(result);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static JsonObject checkLoginServer(String url, String username, String pass) {
        try {
            StringBuilder strUrl = new StringBuilder(url);
            strUrl.append("?u=").append(username);
            strUrl.append("&p=").append(pass);
            String result = HttpUtils.httpGetRequest(strUrl.toString());
            return MisaUtils.toJsonObject(result);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void checkServer() {
        JsonObject jsonObject = checkLoginServer("https://public.wynn88.club/loginv2", "merlin10", "101098");

        if(jsonObject!=null) {
            if(jsonObject.get("e").getAsInt()==1) {
                System.out.println(sendMessage("Có lỗi hệ thống server Hu rồi. gọi dev kiểm tra gấp.", CHAT_ID_MIRA));
            }
        }
        JsonObject jsonObjectQueen = checkLoginServer("https://public.queengame88.com/loginv2", "meliodas10", "101098");

        if(jsonObjectQueen!=null) {
            if(jsonObjectQueen.get("e").getAsInt()==1) {
                System.out.println(sendMessage("Có lỗi hệ thống server QUEEN rồi. gọi dev kiểm tra gấp.", CHAT_ID_MIRA));
            }
        }
    }
}
