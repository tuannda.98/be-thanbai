package misa.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class VerifyInfo implements Serializable {
    public static final long serialVersionUID = 2L;
    private String serviceId;
    private String mobile;
    private String idDevice;
}
