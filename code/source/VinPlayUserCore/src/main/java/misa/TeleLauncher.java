package misa;

import misa.api.config.TeleConfig;
import misa.api.handle.TeleHandle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TeleLauncher {
    private static Logger logger = LoggerFactory.getLogger(TeleLauncher.class.getName());

    public static void main(String[] args) {
        try {

//            PropertyConfigurator.configure("config/log4j.properties");
//            logger.info("HazelcastClientFactory is initialed");
////            HazelcastClientFactory.start();
//            logger.info("HikariPool is initialed");
////            HikariPool.init();
//            logger.info("MongoPool is initialed");
////            MongoPool.init();

            logger.info("TeleConfig is initialed");
            TeleConfig.init();

            logger.info("TeleHandle is initialed");
            TeleHandle.start();

//            TelegramNotifier.loop();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
