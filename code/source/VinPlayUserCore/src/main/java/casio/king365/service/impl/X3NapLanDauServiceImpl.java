package casio.king365.service.impl;

import casio.king365.core.HCMap;
import casio.king365.entities.X3NapLanDau;
import casio.king365.service.X3NapLanDauService;
import casio.king365.util.KingUtil;
import com.google.gson.Gson;
import com.hazelcast.core.IMap;
import com.vinplay.dal.dao.LogMoneyUserDao;
import com.vinplay.dal.dao.X3NapLanDauDAO;
import com.vinplay.dal.dao.impl.LogMoneyUserDaoImpl;
import com.vinplay.dal.dao.impl.X3NapLanDauDAOImpl;
import com.vinplay.dichvuthe.dao.RechargeDao;
import com.vinplay.dichvuthe.dao.impl.RechargeDaoImpl;
import com.vinplay.usercore.service.UserService;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.models.BankTranferModel;
import com.vinplay.vbee.common.models.CardTranferModel;
import com.vinplay.vbee.common.models.MomoTranferModel;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.response.LogUserMoneyResponse;
import org.bson.Document;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class X3NapLanDauServiceImpl implements X3NapLanDauService {
    private UserService userService = new UserServiceImpl();
    private RechargeDao rechargeDao = new RechargeDaoImpl();
    private X3NapLanDauDAO x3NapLanDauDAO = new X3NapLanDauDAOImpl();
    private LogMoneyUserDao logMoneyUserDao = new LogMoneyUserDaoImpl();

    // Thời gian bắt đầu khởi động tính năng X3 nạp, các lần nạp sau thời gian này mới được tính
    private Long timeStartX3System = 1624543200000l;   // 2021-06-21 00:00:00   (local time)

    @Override
    public X3NapLanDau getX3NapLanDau(String nickname, String channel) {
        IMap<String, X3NapLanDau> map = HCMap.getX3NLDMap();
        String key = nickname + "-" + channel;
        X3NapLanDau x3Obj = null;
        try {
            map.lock(key);
            if (!map.containsKey(key)) {
                x3Obj = x3NapLanDauDAO.getX3NapLanDau(nickname, channel);
                if(x3Obj != null)
                    map.put(key, x3Obj);
            }
            x3Obj = map.get(key);
            if(x3Obj != null)
                KingUtil.printLog("getX3NapLanDau("+nickname+", "+channel+") x3Obj: "+x3Obj);
            else
                KingUtil.printLog("getX3NapLanDau("+nickname+", "+channel+") x3Obj null");
        } finally {
            map.unlock(key);
        }
        return x3Obj;
    }

    @Override
    public List<X3NapLanDau> getX3NapLanDau(String nickname) {
        List<X3NapLanDau> listX3 = new ArrayList<>();
        for(String channel : Arrays.asList("card", "momo", "bank")){
            X3NapLanDau x3Obj = getX3NapLanDau(nickname, channel);
            if(x3Obj != null)
                listX3.add(x3Obj);
        }
        // Sort
        listX3.sort(Comparator.comparing(X3NapLanDau::getStartActiveTime));
        return listX3;
    }

    @Override
    public List<Document> getX3NapLanDauDoc(String nickname) {
        return x3NapLanDauDAO.getX3NapLanDau(nickname);
    }

    @Override
    public void createX3NapLanDau(X3NapLanDau obj) {
        x3NapLanDauDAO.createX3NapLanDau(obj);
    }

    private void checkCreateX3NapLanDau(String nickname, Long chargeAmount, String channel, String timeRecharge){
        // Nếu nạp trước ngày hệ thống x3 này hoạt động thì không tính
        if(Long.parseLong(timeRecharge) < timeStartX3System){
            KingUtil.printLog("Lần nạp đầu tiên diễn ra trước khi hệ thống x3 khởi động nên ko được tính");
            return;
        }

        UserCacheModel u = userService.getCacheUserIgnoreCase(nickname);
        X3NapLanDau progressObj = new X3NapLanDau();
        progressObj.setUserId(u.getId());
        progressObj.setNickname(nickname);
        Instant instant = Instant.ofEpochMilli(Long.parseLong(timeRecharge));
        ZonedDateTime time = instant.atZone(ZoneId.of("Asia/Ho_Chi_Minh"));
        // Active time bằng thời gian ngay khi nạp (các lịch sử chơi sẽ tính từ thời điểm này)
        progressObj.setStartActiveTime(time.toInstant().toEpochMilli());
        // Start time bằng thời gian bắt đầu của ngày
        time = time.withHour(0).withMinute(0).withSecond(0).withNano(0);
        progressObj.setStartTime(time.toInstant().toEpochMilli());
        time = time.plusDays(20);
        progressObj.setEndTime(time.toInstant().toEpochMilli());
        progressObj.setChargeAmount(chargeAmount);
        progressObj.setBonusAmount(chargeAmount * 2);
        progressObj.setPercent(0d);
        progressObj.setChannel(channel);
        progressObj.setReceivedBonus(false);
        createX3NapLanDau(progressObj);
    }

    /**
     * Method này để tính toán tiến trình thực hiện quá trình x3 nạp lần đầu
     *
     * @param nickName
     */
    @Override
    public void calculateProgress(String nickName) {
        Gson gson = new Gson();
        // Lấy danh sách các quá trình X3 tài khoản của user
        List<X3NapLanDau> listX3 = getX3NapLanDau(nickName);
        KingUtil.printLog("calculateProgress() listX3: "+gson.toJson(listX3));
        // Tạo tiến trình x3 nạp khi user nạp mới
        // Danh sách các kênh mà user chưa có nạp lần đầu
        List<String> listChannelMiss = new ArrayList<>();
        listChannelMiss.add("card");
        listChannelMiss.add("momo");
        listChannelMiss.add("bank");
        for (X3NapLanDau x3Obj : listX3) {
            // Kênh nào mà user đã nạp lần đầu rồi thì bỏ khỏi list trên
            listChannelMiss.remove(x3Obj.getChannel());
        }
        KingUtil.printLog("cac kenh chua nap lan dau: "+gson.toJson(listChannelMiss));

        // Dựa theo các kênh chưa có nạp lần đầu, tìm xem user đã nạp ở kênh đó chưa?
        for (String missChannel : listChannelMiss) {
            KingUtil.printLog("missChannel: "+missChannel);
            if (missChannel.equals("card")) {
                List<CardTranferModel> listNapCard = rechargeDao.GetCardTransfer(1, 10000, nickName, -1);
                // Nếu thấy user đã nạp card
                if (listNapCard.size() > 0) {
                    // Tạo tiến trình x3 nạp card theo thẻ nạp đầu tiên của user
                    CardTranferModel napCard = listNapCard.get(listNapCard.size() - 1);     // do đang sort mới nhất đầu tiên
                    KingUtil.printLog("tao tien trinh nap card dua theo thong tin nap card: "+napCard);
                    checkCreateX3NapLanDau(nickName, napCard.addMoney, "card", napCard.timeRequest);
                }
                else KingUtil.printLog("ko tim thay nap card");
            } else if (missChannel.equals("momo")) {
                List<MomoTranferModel> listNapMomo = rechargeDao.GetMomoTransfer(1, 10000, nickName, -1);
                // Nếu thấy user đã nạp momo
                if (listNapMomo.size() > 0) {
                    // Tạo tiến trình x3 nạp momo theo momo nạp đầu tiên của user
                    MomoTranferModel napMomo = listNapMomo.get(listNapMomo.size() - 1);     // do đang sort mới nhất đầu tiên
                    KingUtil.printLog("tao tien trinh nap momo dua theo thong tin napMomo: "+napMomo);
                    checkCreateX3NapLanDau(nickName, napMomo.addMoney, "momo", napMomo.timeRequest);
                }
                else KingUtil.printLog("ko tim thay nap momo");
            } else if (missChannel.equals("bank")) {
                List<BankTranferModel> listNapBank = rechargeDao.GetBankTransfer(1, 10000, nickName, -1);
                // Nếu thấy user đã nạp bank
                if (listNapBank.size() > 0) {
                    // Tạo tiến trình x3 nạp bank theo bank nạp đầu tiên của user
                    BankTranferModel napBank = listNapBank.get(listNapBank.size() - 1);     // do đang sort mới nhất đầu tiên
                    KingUtil.printLog("tao tien trinh nap bank dua theo thong tin napBank: "+napBank);
                    checkCreateX3NapLanDau(nickName, napBank.addMoney, "bank", napBank.timeRequest);
                }
                else KingUtil.printLog("ko tim thay nap bank");
            }
        }

        listX3 = getX3NapLanDau(nickName);

        KingUtil.printLog("bat dau xu li");
        X3NapLanDau availableX3Progress = null;
        // Xử lí các tiến trình nạp lần đầu
        for (X3NapLanDau x3Obj : listX3) {
            KingUtil.printLog("x3Obj: "+x3Obj);
            // Loại bỏ các tiến trình ko đủ điều kiện để xử lí
            if (x3Obj.getReceivedBonus())   // Đã hoàn thành
                continue;
            if (Instant.now().toEpochMilli() > x3Obj.getEndTime())   // Quá hạn
                continue;
            if (x3Obj.getPercent() >= 100)    // Đã hoàn thành nhưng chưa nhận thưởng thì treo toàn bộ quá trình lại
                // khi nào nhận thưởng xong thì quá trình tiếp theo mới được khởi động tiếp
                break;

            availableX3Progress = x3Obj;

            break;
        }

        if(availableX3Progress != null){
            KingUtil.printLog("availableX3Progress != null, availableX3Progress: "+availableX3Progress);
            long totalPlayedMoney = 0;
            // Số tiền khách cần tiêu để hoàn thành 10% tiến trình
            double moneyToCompletePhase1 = (availableX3Progress.getBonusAmount() * 0.1) / 2 * 100;
            // Số tiền khách cần tiêu để hoàn thành 40% tiến trình tiếp theo
            double moneyToCompletePhase2 = (availableX3Progress.getBonusAmount() * 0.4) / 1 * 100;
            // Số tiền khách cần tiêu để hoàn thành 50% tiến trình tiếp theo
            double moneyToCompletePhase3 = (availableX3Progress.getBonusAmount() * 0.5) / 0.5 * 100;
            KingUtil.printLog("moneyToCompletePhase1: "+moneyToCompletePhase1+", moneyToCompletePhase2: "+moneyToCompletePhase2
            +", moneyToCompletePhase3: "+moneyToCompletePhase3);
            // Duyệt lịch sử tiền của user, tính toán % hoàn thành dựa trên lịch sử chơi đó
            // Thời gian bắt đầu từ khi tiến trình này active đến hiện tại
            Instant instant = Instant.ofEpochMilli(availableX3Progress.getStartActiveTime());
            ZonedDateTime zdt = instant.atZone(ZoneId.of("Asia/Ho_Chi_Minh"));
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            KingUtil.printLog("history find time: "+dtf.format(zdt));
            List<LogUserMoneyResponse> userHistory = logMoneyUserDao.getUserHistory(nickName, dtf.format(zdt), "");
            // Reset percent của tiến trình về 0 trước khi bắt đầu tính percent
            availableX3Progress.setPercent(0d);
            for (int i=userHistory.size()-1; i>=0; i--) {       // Do sort, cần duyệt từ cũ nhất lên mới nhất
                LogUserMoneyResponse log = userHistory.get(i);
                // Nếu giao dịch này k phải là các giao dịch cần xử lí thì bỏ qua
                if (!isValidGameName(log.actionName))
                    continue;
                // Phải loại bỏ các tiền hoàn của game Tài Xỉu, Xóc Đĩa
                if(log.serviceName.equals("Hoàn trả tài xỉu Đu Dây") || log.serviceName.equals("Hoàn trả Xóc Đĩa")){
                    totalPlayedMoney -= log.moneyExchange;
                    continue;
                }
                // Chỉ tính tiền cược, tiền thắng thì k tính
                if (log.moneyExchange >= 0)
                    continue;
                // KingUtil.printLog("log: "+log);
                totalPlayedMoney += (log.moneyExchange * -1); // *-1 vì moneyExchange đang là số âm
                /*
                // Cộng thêm điểm tiến trình
                long exchangeMoney = log.moneyExchange * -1;
                Double point = (double) exchangeMoney / 100 * 0.5;
                if (availableX3Progress.getPercent() < 10)
                    point = (double) exchangeMoney / 100 * 2;
                else if (availableX3Progress.getPercent() < 50)
                    point = (double) exchangeMoney / 100 * 1;
                Double percentOfPoint = point / availableX3Progress.getBonusAmount() * 100;
                availableX3Progress.setPercent(availableX3Progress.getPercent() + percentOfPoint);
                KingUtil.printLog("increasePercentProgress(), point: " + point + ", percentOfPoint: " + percentOfPoint + ", percent after: " + availableX3Progress.getPercent());
                if (availableX3Progress.getPercent() > 100)
                    availableX3Progress.setPercent(100d);

                 */
            }
            KingUtil.printLog("totalPlayedMoney: "+totalPlayedMoney);
            if(totalPlayedMoney > moneyToCompletePhase1 + moneyToCompletePhase2 + moneyToCompletePhase3){
                // Nếu số tiền đã tiêu vượt quá số tiền cần để thắng bonus thì cho hoàn thành 100%
                availableX3Progress.setPercent(100d);
            } else if(totalPlayedMoney <= moneyToCompletePhase1){
                availableX3Progress.setPercent(totalPlayedMoney/moneyToCompletePhase1*10);
                KingUtil.printLog("totalPlayedMoney <= moneyToCompletePhase1, availableX3Progress percent: "+availableX3Progress.getPercent());
            } else if(totalPlayedMoney <= moneyToCompletePhase1 + moneyToCompletePhase2){
                availableX3Progress.setPercent(10 + ((totalPlayedMoney-moneyToCompletePhase1)/moneyToCompletePhase2*40));
                KingUtil.printLog("totalPlayedMoney <= moneyToCompletePhase2, availableX3Progress percent: "+availableX3Progress.getPercent());
            } else if(totalPlayedMoney <= moneyToCompletePhase1 + moneyToCompletePhase2 + moneyToCompletePhase3){
                availableX3Progress.setPercent(10 + 40 + ((totalPlayedMoney-moneyToCompletePhase1-moneyToCompletePhase2)/moneyToCompletePhase3*50));
                KingUtil.printLog("totalPlayedMoney <= moneyToCompletePhase3, availableX3Progress percent: "+availableX3Progress.getPercent());
            }

            // Update lại vào mongo
            updatePercent(availableX3Progress.getNickname(), availableX3Progress.getChannel(), availableX3Progress.getPercent());
        } else
            KingUtil.printLog("availableX3Progress == null");
    }

    @Override
    public void updatePercent(String nickname, String channel, Double newPercent){
        IMap<String, X3NapLanDau> map = HCMap.getX3NLDMap();
        String key = nickname + "-" + channel;
        try {
            map.lock(key);
            X3NapLanDau x3Obj = map.get(key);
            if(x3Obj != null){
                x3Obj.setPercent(newPercent);
                map.put(key, x3Obj);
                x3NapLanDauDAO.updatePercent(nickname, channel, x3Obj.getPercent());
            }
        } finally {
            map.unlock(key);
        }
    }

    @Override
    public void updateReceivedBonus(String nickname, String channel, boolean newReceBonus){
        IMap<String, X3NapLanDau> map = HCMap.getX3NLDMap();
        String key = nickname + "-" + channel;
        try {
            map.lock(key);
            X3NapLanDau x3Obj = map.get(key);
            if(x3Obj != null){
                x3Obj.setReceivedBonus(newReceBonus);
                map.put(key, x3Obj);
                x3NapLanDauDAO.updateReceivedBonus(nickname, channel, x3Obj.getReceivedBonus());
            }
        } finally {
            map.unlock(key);
        }
    }

    @Override
    public void updateActiveTime(String nickname, String channel, long newActiveTime){
        IMap<String, X3NapLanDau> map = HCMap.getX3NLDMap();
        String key = nickname + "-" + channel;
        try {
            map.lock(key);
            X3NapLanDau x3Obj = map.get(key);
            if(x3Obj != null){
                x3Obj.setStartActiveTime(newActiveTime);
                map.put(key, x3Obj);
                x3NapLanDauDAO.updateActiveTime(nickname, channel, x3Obj.getStartActiveTime());
            }
        } finally {
            map.unlock(key);
        }
    }

    boolean isValidGameName(String gameName) {
        switch (gameName) {
            case "BauCua":
            case "CaoThap":
            case "MiniPoker":
            case "PokeGo":
            case "SlotExtend":
            case "TaiXiu":
            case "SieuAnhHung":
            case "KhoBau":
            case "NuDiepVien":
            case "VuongQuocVin":
            case "Sam":
            case "BaCay":
            case "BaiCao":
            case "Binh":
            case "Caro":
            case "CoTuong":
            case "CoUp":
            case "Lieng":
            case "OverUnder":
            case "PokerTour":
            case "Poker":
            case "Tlmn":
            case "XiDzach":
            case "XocDia":
                return true;
        }
        return false;
    }

    public static void main(String args[]) {
        List<Integer> l = new ArrayList<>();
        l.add(10);
        l.add(60);
        l.add(30);
        l.add(20);
        l.add(70);
        l.add(50);
        System.out.println("l: "+l);
        l.sort((o1, o2) -> o2.compareTo(o1));
        System.out.println("l sort: "+l);
    }
}
