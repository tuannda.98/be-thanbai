package casio.king365.service;

import casio.king365.entities.Mission;
import casio.king365.entities.UserMission;

import java.util.List;

public interface UserMissionService {
    List<Mission> getMissions();
    UserMission createUserMissions(String nickname, String missionId);
    UserMission getUserMissions(String nickname, String missionId);
    List<String> getListMissionIdByAction(String action);
    void processUserMission(String nickname, long moneyExchange, String action);
    void saveNumberDoingTarget(String nickname, String missionId, int missionTarget);
}
