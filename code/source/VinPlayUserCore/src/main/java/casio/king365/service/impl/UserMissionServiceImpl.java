package casio.king365.service.impl;

import casio.king365.entities.Mission;
import casio.king365.entities.UserMission;
import casio.king365.service.MongoDbService;
import casio.king365.service.UserMissionService;
import casio.king365.util.KingUtil;
import org.bson.Document;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class UserMissionServiceImpl implements UserMissionService {
    MongoDbService mongodbService = new MongoDbServiceImpl();
    List<Mission> listMission = null;

    @Override
    public List<Mission> getMissions() {
        if (listMission == null) {
            // Load dữ liệu danh sách các nhiệm vụ
            loadListMission();
        }
        return listMission;
    }

    void loadListMission() {
        listMission = mongodbService.find("mission", new HashMap<>(), Mission::fromDocument);
    }

    @Override
    public UserMission createUserMissions(String nickname, String missionId) {
        UserMission um = new UserMission();
        um.setNickname(nickname);
        um.setMissionId(missionId);
        um.setNumberDoingTarget(0);
        um.setReceivedBonus(false);
        Instant instant = Instant.now().truncatedTo(ChronoUnit.DAYS);
        um.setStartTime(instant.toEpochMilli());
        mongodbService.insert("user_mission", um.toMap());
        return um;
    }

    @Override
    public UserMission getUserMissions(String nickname, String missionId) {
        Map<String, Object> findMap = new HashMap<>();
        findMap.put("nickname", nickname);
        findMap.put("missionId", missionId);
        List<UserMission> result = mongodbService.find("user_mission", findMap, UserMission::fromDocument);
        if (result.size() > 0) {
            return result.get(0);
        }
        UserMission um = createUserMissions(nickname, missionId);
        return um;
    }

    /**
     * Dựa vào tên action để trả lại các id nhiệm vụ có liên quan đến action đó
     *
     * @param action
     * @return
     */
    @Override
    public List<String> getListMissionIdByAction(String action) {
        switch (action) {
            case "RechargeByBank":
                return Arrays.asList("napbank100k", "napbank200k", "napbank500k", "napbank1m", "napbank5m", "napbank10m");
            case "RechargeByMomo":
                return Arrays.asList("napvidientu100k", "napvidientu200k", "napvidientu500k", "napvidientu1m", "napvidientu5m", "napvidientu10m");
            case "TaiXiu":
                return Arrays.asList("thang3vantaixiu10k", "thang3vantaixiu100k", "thang3vantaixiu500k", "thang3vantaixiu1m");
            case "XocDia":
                return Arrays.asList("thang3vanxocdia100k", "thang3vanxocdia500k", "thang3vanxocdia1m");
        }
        return new ArrayList<>();
    }

    @Override
    public void processUserMission(String nickname, long moneyExchange, String action) {
        KingUtil.printLog("processUserMission() nickname: " + nickname + ", moneyExchange: " + moneyExchange + ", action: " + action);
        List<String> listMissionIdByAction = getListMissionIdByAction(action);
        KingUtil.printLog("processUserMission() listMissionIdByAction: " + String.join(", ", listMissionIdByAction));
        if (listMissionIdByAction.size() == 0)
            return;

        // Lấy ra các mission Object có liên quan đến action này
        List<Mission> listMission = new ArrayList<>();
        for (Mission mission : getMissions()) {
            if (listMissionIdByAction.contains(mission.getId()))
                listMission.add(mission);
        }

        // Lấy ra danh sách các tiến trình thực hiện các nhiệm vụ này của user
        Map<String, UserMission> mapUserMission = new HashMap<>();
        for (Mission mission : listMission) {
            UserMission um = getUserMissions(nickname, mission.getId());
            um.setMission(mission);
            mapUserMission.put(um.getMissionId(), um);
        }

        for (Map.Entry<String, UserMission> entry : mapUserMission.entrySet()) {
            UserMission um = entry.getValue();
            KingUtil.printLog("processUserMission() um: " + um);
            // Bỏ qua các nhiệm vụ đã hoàn thành
            if (um.getReceivedBonus())
                continue;
            // Bỏ qua các nhiệm vụ mà nhiệm vụ phụ thuộc của nó chưa hoàn thành
            if (!um.getMission().getDepend().equals("")) {
                UserMission dependMission = mapUserMission.get(um.getMission().getDepend());
                if (dependMission == null) {
                    dependMission = getUserMissions(nickname, um.getMission().getDepend());
                    mapUserMission.put(dependMission.getMissionId(), dependMission);
                }
                if (!dependMission.getReceivedBonus())
                    continue;
            }

            boolean completeQuestion = false;
            switch (um.getMissionId()) {
                case "napbank100k":
                    if (moneyExchange >= 122000)
                        completeQuestion = true;
                    break;
                case "napbank200k":
                case "napvidientu200k":
                    if (moneyExchange >= 244000)
                        completeQuestion = true;
                    break;
                case "napbank500k":
                case "napvidientu500k":
                    if (moneyExchange >= 610000)
                        completeQuestion = true;
                    break;
                case "napbank1m":
                case "napvidientu1m":
                    if (moneyExchange >= 1220000)
                        completeQuestion = true;
                    break;
                case "napbank5m":
                case "napvidientu5m":
                    if (moneyExchange >= 6100000)
                        completeQuestion = true;
                    break;
                case "napbank10m":
                case "napvidientu10m":
                    if (moneyExchange >= 12200000)
                        completeQuestion = true;
                    break;
                case "thang3vantaixiu10k":
                    if (moneyExchange >= 19800)
                        completeQuestion = true;
                    break;
                case "thang3vantaixiu100k":
                case "thang3vanxocdia100k":
                    if (moneyExchange >= 198000)
                        completeQuestion = true;
                    break;
                case "thang3vantaixiu500k":
                case "thang3vanxocdia500k":
                    if (moneyExchange >= 990000)
                        completeQuestion = true;
                    break;
                case "thang3vantaixiu1m":
                case "thang3vanxocdia1m":
                    if (moneyExchange >= 1980000)
                        completeQuestion = true;
                    break;
            }

            if (completeQuestion) {
                // Tăng số lần thực hiện nhiệm vụ này lên 1
                int numberDoingTarget = um.getNumberDoingTarget() + 1;
                if (numberDoingTarget > um.getMission().getNumberTarget())
                    numberDoingTarget = um.getMission().getNumberTarget();
                um.setNumberDoingTarget(numberDoingTarget);

                saveNumberDoingTarget(um.getNickname(), um.getMissionId(), um.getNumberDoingTarget());
            }
        }
    }

    @Override
    public void saveNumberDoingTarget(String nickname, String missionId, int missionTarget) {
        // Tạo nếu chưa có
        getUserMissions(nickname, missionId);

        KingUtil.printLog("saveNumberDoingTarget() nickname: " + nickname + ", missionId: " + missionId + ", missionTarget");
        mongodbService.update("user_mission",
                new Document("nickname", nickname).append("missionId", missionId), new Document("numberDoingTarget", missionTarget));
    }
}
