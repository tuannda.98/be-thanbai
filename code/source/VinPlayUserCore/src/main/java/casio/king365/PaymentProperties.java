package casio.king365;

public class PaymentProperties {
    public static final String CardinInfo_Username = "cardininfo_username";
    public static final String CardinInfo_PW = "cardininfo_pw";
    public static final String CardinInfo_API = "cardininfo_apiUrl";
    public static final String CardinInfo_CALLBACK = "cardininfo_callbackUrl";
    public static final String CardinInfo_CashOutAdminApproveUrl = "cardininfo_cashOutAdminApproveUrl";

    public static final String BuyCardClient_PRIVATE_KEY = "BuyCardClient_privateKey";
    public static final String BuyCardClient_PARTNER_ID = "BuyCardClient_partnerId";
    public static final String BuyCardClient_CARD_URL = "BuyCardClient_cardUrl";
    public static final String BuyCardClient_MOMO_URL = "BuyCardClient_momoUrl";
    public static final String BuyCardClient_BANK_URL = "BuyCardClient_bankUrl";
    public static final String BuyCardClient_CHECK_BANK_CASHOUT_URL = "BuyCardClient_checkBankCashoutUrl";
    public static final String BuyCardClient_XVERSION = "BuyCardClient_xVersion";

    public static final String PaymentGatewayInfo_CALLBACK_KEY = "PaymentGatewayInfo_callbackKey";

    public static final String MomoInfo_PARTNER_KEY = "MomoInfo_partnerKey";
    public static final String MomoInfo_PARTNER_NAME = "MomoInfo_partnerName";
    public static final String MomoInfo_API_GET_SHIPPER_URL = "MomoInfo_apiUrl";
    public static final String MomoInfo_REJECT = "MomoInfo_REJECT";
    public static final String MomoInfo_ERROR = "MomoInfo_ERROR";


    public static final String BankInInfo_partnerKey = "BankInInfo_partnerKey";
    public static final String BankInInfo_partnerName = "BankInInfo_partnerName";
    public static final String BankInInfo_getBankUrl = "BankInInfo_getBankUrl";
    public static final String BankInInfo_getAccUrl = "BankInInfo_getAccUrl";
    public static final String BankInInfo_getCheckTranstactionStatusUrl = "BankInInfo_getCheckTranstactionStatusUrl";

    public static final String CashoutRequestStatus_NEW = "CashoutRequestStatus_NEW";
    public static final String CashoutRequestStatus_ACCEPT = "CashoutRequestStatus_ACCEPT";
    public static final String CashoutRequestStatus_COMPLETE_PAY = "CashoutRequestStatus_COMPLETE_PAY";
    public static final String CashoutRequestStatus_REJECT = "CashoutRequestStatus_REJECT";
    public static final String CashoutRequestStatus_ERROR = "CashoutRequestStatus_ERROR";
    public static final String CashoutRequestStatus_PENDING = "CashoutRequestStatus_PENDING";

    public static final String CoinPaymentInfo_urlError = "CoinPaymentInfo_urlError";
    public static final String CoinPaymentInfo_urlCreateWallet = "CoinPaymentInfo_urlCreateWallet";
    public static final String CoinPaymentInfo_urlGetWallet = "CoinPaymentInfo_urlGetWallet";
    public static final String CoinPaymentInfo_urlReActiveWallet = "CoinPaymentInfo_urlReActiveWallet";
    public static final String CoinPaymentInfo_urlRecharge = "CoinPaymentInfo_urlRecharge";
    public static final String CoinPaymentInfo_urlWithdraw = "CoinPaymentInfo_urlWithdraw";
    public static final String CoinPaymentInfo_urlCheckWithdraw = "CoinPaymentInfo_urlCheckWithdraw";
    public static final String CoinPaymentInfo_partnerName = "CoinPaymentInfo_partnerName";
    public static final String CoinPaymentInfo_token = "CoinPaymentInfo_token";
    public static final String CoinPaymentInfo_secretKey = "CoinPaymentInfo_secretKey";
    public static final String CoinPaymentInfo_usdtToVndRate = "CoinPaymentInfo_usdtToVndRate";
    public static final String CoinPaymentInfo_xVersion = "CoinPaymentInfo_xVersion";
    public static final String CoinPaymentInfo_privateKey = "CoinPaymentInfo_privateKey";

    public static final String CashoutItemType_CARD = "CashoutItemType_CARD";
    public static final String CashoutItemType_ITEM = "CashoutItemType_ITEM";
    public static final String CashoutItemType_MOMO = "CSashoutItemType_MOMO";
    public static final String CashoutItemType_BANK = "CashoutItemType_BANK";
}