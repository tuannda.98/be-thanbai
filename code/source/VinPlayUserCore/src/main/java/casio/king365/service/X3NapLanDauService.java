package casio.king365.service;

import casio.king365.entities.X3NapLanDau;
import org.bson.Document;

import java.util.List;

public interface X3NapLanDauService {
    X3NapLanDau getX3NapLanDau(String nickname, String channel);
    List<X3NapLanDau> getX3NapLanDau(String nickname);
    List<Document> getX3NapLanDauDoc(String nickname);
    void createX3NapLanDau(X3NapLanDau obj);
    void calculateProgress(String nickName);
    void updatePercent(String nickName, String channel, Double newPercent);
    void updateReceivedBonus(String nickName, String channel, boolean newReceBonus);
    void updateActiveTime(String nickName, String channel, long newActiveTime);
}
