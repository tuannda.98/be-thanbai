package casio.king365;

import com.vinplay.dichvuthe.service.impl.AlertServiceImpl;
import com.vinplay.vbee.common.hazelcast.HazelcastLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class GU {
    private static final List<String> messages = new ArrayList<>();
    private static final Logger LOG = LoggerFactory.getLogger(GU.class);
    private static ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(2);

    static {
        int PERIOD = 5;
        scheduledThreadPoolExecutor.scheduleAtFixedRate(() -> {
            synchronized (messages) {
                if (!messages.isEmpty()) {
                    String message;
                    if (messages.size() == 1) {
                        message = messages.get(0);
                    } else {
                        message = "[CHÚ Ý] Có " + messages.size() + " cảnh báo trong " + PERIOD + "s: ";
                        message += messages.stream().map(m -> m.substring(0, Math.min(m.length(), 200))).collect(Collectors.joining());
                    }
                    messages.clear();
                    message = String.format("%s: %s",getHostName(), message);
                    LOG.warn(message);
                    AlertServiceImpl alertSer = new AlertServiceImpl();
                    alertSer.sendOperation(message);
                }
            }
        }, 5, PERIOD, TimeUnit.SECONDS);
    }

    public static void sendOperation(String message) {
        synchronized (messages) {
            messages.add(message);
        }
    }

    public static void sendCustomerService(String message) {
        AlertServiceImpl alertSer = new AlertServiceImpl();
        alertSer.sendCustomerService(message);
    }

    public static void sendClientErrorMonitor(String message) {
        AlertServiceImpl alertSer = new AlertServiceImpl();
        alertSer.sendClientErrorMonitor(message);
    }

    private static String getHostName() {
        try {

            // get system name
            String SystemName = InetAddress.getLocalHost().getHostName();

            // SystemName stores the name of the system
            System.out.println("System Name : " + SystemName);
            return SystemName;
        } catch (Exception E) {
            System.err.println(E.getMessage());
            return "Unknown";
        }
    }

    private static String getComputerName()
    {
        try {
            Map<String, String> env = System.getenv();
            if (env.containsKey("COMPUTERNAME"))
                return env.get("COMPUTERNAME");
            else if (env.containsKey("HOSTNAME"))
                return env.get("HOSTNAME");
            else
                return "Unknown Computer";
        } catch (Exception E) {
            System.err.println(E.getMessage());
            return "Unknown Computer";
        }
    }

    public static void main(String[] args) throws InterruptedException, IOException {
        HazelcastLoader.start();
        GU.sendOperation("Hi from GU - Test send message");
//        GU.sendCustomerService("Hi from GU.sendCustomerService");
//        for (int i = 0; i < 10; i++) {
//            GU.sendOperation("Hi from GU " + i);
//        }
//        Thread.sleep(6_000);
//        GU.sendOperation("Hi from GU " + 10);
//        Thread.sleep(6_000);
//        GU.sendOperation("Hi from GU " + 11);
    }
}
