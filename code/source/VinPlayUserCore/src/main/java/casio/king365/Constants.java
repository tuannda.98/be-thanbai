package casio.king365;

public interface Constants {
    String CONFIG_PAYMENT_GATEWAY_PROPERTIES = "config/payment_gateway.properties";

    interface CardinInfo {
        String username = Configurations.getInstance().getProperty(PaymentProperties.CardinInfo_Username);
        String pw = Configurations.getInstance().getProperty(PaymentProperties.CardinInfo_PW);
        String apiUrl = Configurations.getInstance().getProperty(PaymentProperties.CardinInfo_API);
        String callbackUrl = Configurations.getInstance().getProperty(PaymentProperties.CardinInfo_CALLBACK);
        String cashOutAdminApproveUrl = Configurations.getInstance().getProperty(PaymentProperties.CardinInfo_CashOutAdminApproveUrl);
    }

    interface BuyCardClient {
        // public key: MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCSM5l3qn4eKM01xtNkkLpfahacN6eqtWJ2FW7
        // +lY1imonYv1qOPHx0yuWD+YgSIJbYeGNUkwP8wOESye
        // ++ACCAqnLtXpL73f3EAe2Uo9VAV7XLxBSXkEARBI8QwqeR4QoMBxEbnZU0dz6and44BDCabl/crGG4FBEaxuPXbOMypQIDAQAB
        String privateKey = Configurations.getInstance().getProperty(PaymentProperties.BuyCardClient_PRIVATE_KEY);
        String partnerId = Configurations.getInstance().getProperty(PaymentProperties.BuyCardClient_PARTNER_ID);
        String cardUrl = Configurations.getInstance().getProperty(PaymentProperties.BuyCardClient_CARD_URL);
        String momoUrl = Configurations.getInstance().getProperty(PaymentProperties.BuyCardClient_MOMO_URL);
        String bankUrl = Configurations.getInstance().getProperty(PaymentProperties.BuyCardClient_BANK_URL);
        String checkBankCashoutUrl = Configurations.getInstance().getProperty(PaymentProperties.BuyCardClient_CHECK_BANK_CASHOUT_URL);
        String xVersion = Configurations.getInstance().getProperty(PaymentProperties.BuyCardClient_XVERSION);
    }

    interface PaymentGatewayInfo {
        String callbackKey = Configurations.getInstance().getProperty(PaymentProperties.PaymentGatewayInfo_CALLBACK_KEY);
    }

    interface MomoInfo {
        String partnerKey = Configurations.getInstance().getProperty(PaymentProperties.MomoInfo_PARTNER_KEY);
        String partnerName = Configurations.getInstance().getProperty(PaymentProperties.MomoInfo_PARTNER_NAME);
        String apiGetShippersUrl = Configurations.getInstance().getProperty(PaymentProperties.MomoInfo_API_GET_SHIPPER_URL);
        byte REJECT = Byte.parseByte(Configurations.getInstance().getProperty(PaymentProperties.MomoInfo_REJECT));
        byte ERROR = Byte.parseByte(Configurations.getInstance().getProperty(PaymentProperties.MomoInfo_ERROR));
    }

    interface BankInInfo {
        String partnerKey = Configurations.getInstance().getProperty(PaymentProperties.BankInInfo_partnerKey);
        String partnerName = Configurations.getInstance().getProperty(PaymentProperties.BankInInfo_partnerName);
        String getBankUrl = Configurations.getInstance().getProperty(PaymentProperties.BankInInfo_getBankUrl);
        String getAccUrl = Configurations.getInstance().getProperty(PaymentProperties.BankInInfo_getAccUrl);
        String getCheckTranstactionStatusUrl = Configurations.getInstance().getProperty(PaymentProperties.BankInInfo_getCheckTranstactionStatusUrl);
    }

    interface CashoutRequestStatus {
        byte NEW = Byte.parseByte(Configurations.getInstance().getProperty(PaymentProperties.CashoutRequestStatus_NEW));
        byte ACCEPT = Byte.parseByte(Configurations.getInstance().getProperty(PaymentProperties.CashoutRequestStatus_ACCEPT));
        byte COMPLETE_PAY = Byte.parseByte(Configurations.getInstance().getProperty(PaymentProperties.CashoutRequestStatus_COMPLETE_PAY));
        byte REJECT = Byte.parseByte(Configurations.getInstance().getProperty(PaymentProperties.CashoutRequestStatus_REJECT));
        byte ERROR = Byte.parseByte(Configurations.getInstance().getProperty(PaymentProperties.CashoutRequestStatus_ERROR));
        byte PENDING = Byte.parseByte(Configurations.getInstance().getProperty(PaymentProperties.CashoutRequestStatus_PENDING));
    }

    interface CashoutItemType {
        byte CARD = 0;
        byte COIN = 1;
        byte MOMO = 2;
        byte BANK = 3;
    }

    interface CoinPaymentInfo {
        String CoinPaymentInfo_urlError = Configurations.getInstance().getProperty(PaymentProperties.CoinPaymentInfo_urlError);
        String CoinPaymentInfo_urlCreateWallet = Configurations.getInstance().getProperty(PaymentProperties.CoinPaymentInfo_urlCreateWallet);
        String CoinPaymentInfo_urlGetWallet = Configurations.getInstance().getProperty(PaymentProperties.CoinPaymentInfo_urlGetWallet);
        String CoinPaymentInfo_urlReActiveWallet = Configurations.getInstance().getProperty(PaymentProperties.CoinPaymentInfo_urlReActiveWallet);
        String CoinPaymentInfo_urlRecharge = Configurations.getInstance().getProperty(PaymentProperties.CoinPaymentInfo_urlRecharge);
        String CoinPaymentInfo_urlWithdraw = Configurations.getInstance().getProperty(PaymentProperties.CoinPaymentInfo_urlWithdraw);
        String CoinPaymentInfo_urlCheckWithdraw = Configurations.getInstance().getProperty(PaymentProperties.CoinPaymentInfo_urlCheckWithdraw);

        String partnerName = Configurations.getInstance().getProperty(PaymentProperties.CoinPaymentInfo_partnerName);
        String token = Configurations.getInstance().getProperty(PaymentProperties.CoinPaymentInfo_token);
        String secretKey = Configurations.getInstance().getProperty(PaymentProperties.CoinPaymentInfo_secretKey);
        String usdtToVndRate = Configurations.getInstance().getProperty(PaymentProperties.CoinPaymentInfo_usdtToVndRate);
        String xVersion = Configurations.getInstance().getProperty(PaymentProperties.CoinPaymentInfo_xVersion);
        String privateKey = Configurations.getInstance().getProperty(PaymentProperties.CoinPaymentInfo_privateKey);
    }
}
