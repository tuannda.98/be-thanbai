/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.khothe;

import java.util.List;

public class RutThe99ResponseObj {
    private int Code;
    private String Message;
    private List<RutThe99Card> Cards;

    public int getCode() {
        return this.Code;
    }

    public void setCode(int code) {
        this.Code = code;
    }

    public String getMessage() {
        return this.Message;
    }

    public void setMessage(String message) {
        this.Message = message;
    }

    public List<RutThe99Card> getCards() {
        return this.Cards;
    }

    public void setCards(List<RutThe99Card> cards) {
        this.Cards = cards;
    }

    public class RutThe99Card {
        private String Network;
        private String CardSeri;
        private String CardCode;
        private int CardValue;

        public String getNetwork() {
            return this.Network;
        }

        public void setNetwork(String network) {
            this.Network = network;
        }

        public String getCardSeri() {
            return this.CardSeri;
        }

        public void setCardSeri(String cardSeri) {
            this.CardSeri = cardSeri;
        }

        public String getCardCode() {
            return this.CardCode;
        }

        public void setCardCode(String cardCode) {
            this.CardCode = cardCode;
        }

        public int getCardValue() {
            return this.CardValue;
        }

        public void setCardValue(int cardValue) {
            this.CardValue = cardValue;
        }
    }
}

