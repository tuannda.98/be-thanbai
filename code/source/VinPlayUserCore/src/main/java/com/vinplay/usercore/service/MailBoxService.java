/*
 * Decompiled with CFR 0.150.
 *
 * Could not load the following classes:
 *  com.vinplay.vbee.common.response.MailBoxResponse
 *  com.vinplay.vbee.common.response.SendMailResponse
 */
package com.vinplay.usercore.service;

import com.vinplay.vbee.common.response.MailBoxResponse;
import com.vinplay.vbee.common.response.SendMailResponse;

import java.sql.SQLException;
import java.util.List;

public interface MailBoxService {
    boolean sendMailBoxFromByNickName(List<String> var1, String var2, String var3);

    boolean sendMailBoxFromByNickNameAdmin(String var1, String var2, String var3);

    List<MailBoxResponse> listMailBox(String var1, int var2);

    int updateStatusMailBox(String var1);

    int deleteMailBox(String var1);

    int countMailBox(String var1);

    SendMailResponse sendmailGiftCode(String var1, String var2, String var3, String var4) throws SQLException;

    int countMailBoxInActive(String var1) throws SQLException;

    SendMailResponse sendMailGiftCode(String var1, String var2, String var3, String var4) throws SQLException;

    int deleteMailBoxAdmin(String var1);

    int deleteMutilMailBox(String var1, String var2);

    SendMailResponse sendMailCardMobile(
            String nickName, String provider, String serial, String pin, String title, String content) throws SQLException;

    SendMailResponse sendMailPopupMessage(
            String nickName, String title, String content) throws SQLException;
}

