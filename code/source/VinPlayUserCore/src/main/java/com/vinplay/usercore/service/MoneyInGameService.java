package com.vinplay.usercore.service;

import com.vinplay.vbee.common.enums.FreezeInGame;
import com.vinplay.vbee.common.models.FreezeModel;
import com.vinplay.vbee.common.response.FreezeMoneyResponse;
import com.vinplay.vbee.common.response.MoneyResponse;
import com.vinplay.vbee.common.statics.TransType;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

public interface MoneyInGameService {
    FreezeModel getFreeze(String sessionId) throws SQLException;

    void pushFreezeToCache(FreezeModel model);

    FreezeMoneyResponse freezeMoneyInGame(String nickname, String gameName, String roomId, long money, String moneyType);

    FreezeMoneyResponse restoreMoneyInGame(String sessionId, String nickname, String gameName, String roomId, String moneyType);

    MoneyResponse addingMoneyInGame(String sessionId, String nickname, String gameName, String roomId, long money, String moneyType, long maxFreeze, String matchId, long fee);

    MoneyResponse subtractMoneyInGame(String sessionId, String nickname, String gameName, String roomId, long money, String moneyType, String matchId);

    MoneyResponse updateMoneyInGameByFreeze(String sessionId, String nickname, String gameName, String roomId, long money, String moneyType, String matchId, long fee);

    MoneyResponse subtractMoneyInGameExactly(String sessionId, String nickname, String gameName, String roomId, long money, String moneyType, String matchId);

    List<FreezeModel> getListFreeze(String gamename, String nickname, String moneyType, String startTime, String endTime, int page, boolean getAllPages) throws ParseException;

    boolean restoreFreeze(String nickname, String gamename, String startTime, String endTime, long ignoreByLastActiveTimeDurationInSecond) throws ParseException;

    boolean restoreFreeze(String sessionId);

    void addVippoint(String nickname, long money, String moneyType);

    MoneyResponse addFreezeMoneyInGame(String sessionId, String nickname, String gameName, String roomId, String matchId, long addFreezeMoney, String moneyType, FreezeInGame type);

    MoneyResponse updateMoneyUser(String nickname, long money, String moneyType, String gameName, String serviceName, String description, long fee, Long transId, TransType type, boolean playGame);
}
