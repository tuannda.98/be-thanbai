package com.vinplay.usercore.service.impl;

import casio.king365.Constants;
import casio.king365.GU;
import casio.king365.core.HCMap;
import casio.king365.dto.UpdateUserMoneyMsg;
import casio.king365.mqtt.MqttSender;
import casio.king365.service.MongoDbService;
import casio.king365.service.impl.MongoDbServiceImpl;
import casio.king365.util.KingUtil;
import com.google.common.base.Throwables;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.transaction.TransactionContext;
import com.hazelcast.transaction.TransactionOptions;
import com.hazelcast.transaction.TransactionOptions.TransactionType;
import com.vinplay.dichvuthe.dao.impl.CashoutDaoImpl;
import com.vinplay.dichvuthe.entities.TransferMoneyBankModel;
import com.vinplay.dichvuthe.response.CashoutUserDailyResponse;
import com.vinplay.dichvuthe.service.impl.AlertServiceImpl;
import com.vinplay.dichvuthe.service.impl.TransferMoneyBankService;
import com.vinplay.payment.service.PaymentService;
import com.vinplay.payment.service.impl.PaymentServiceImpl;
import com.vinplay.usercore.dao.SecurityDao;
import com.vinplay.usercore.dao.UserDao;
import com.vinplay.usercore.dao.impl.AgentDaoImpl;
import com.vinplay.usercore.dao.impl.SecurityDaoImpl;
import com.vinplay.usercore.dao.impl.UserDaoImpl;
import com.vinplay.usercore.dao.impl.VippointDaoImpl;
import com.vinplay.usercore.entities.TransferMoneyResponse;
import com.vinplay.usercore.logger.MoneyLogger;
import com.vinplay.usercore.service.MarketingService;
import com.vinplay.usercore.service.UserService;
import com.vinplay.usercore.utils.GameCommon;
import com.vinplay.usercore.utils.VippointUtils;
import com.vinplay.vbee.common.enums.Games;
import com.vinplay.vbee.common.enums.StatusGames;
import com.vinplay.vbee.common.exceptions.KeyNotFoundException;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.hazelcast.HazelcastUtils;
import com.vinplay.vbee.common.messages.LogChuyenTienDaiLyMessage;
import com.vinplay.vbee.common.messages.LogMoneyUserMessage;
import com.vinplay.vbee.common.messages.MoneyMessageInMinigame;
import com.vinplay.vbee.common.messages.VippointMessage;
import com.vinplay.vbee.common.messages.vippoint.VippointEventMessage;
import com.vinplay.vbee.common.models.MarketingModel;
import com.vinplay.vbee.common.models.StatusUser;
import com.vinplay.vbee.common.models.TopCaoThu;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.models.cache.UserExtraInfoModel;
import com.vinplay.vbee.common.models.vippoint.UserVPEventModel;
import com.vinplay.vbee.common.response.*;
import com.vinplay.vbee.common.rmq.RMQApi;
import com.vinplay.vbee.common.statics.TransType;
import com.vinplay.vbee.common.utils.NumberUtils;
import com.vinplay.vbee.common.utils.VinPlayUtils;
import misa.api.handle.TeleHandle;
import misa.data.AccountTeleLink;
import org.apache.log4j.Logger;
import org.bson.Document;
import org.omg.CORBA.BooleanHolder;
import vn.yotel.yoker.dao.CashoutItemDao;
import vn.yotel.yoker.dao.CashoutRequestDao;
import vn.yotel.yoker.dao.impl.CashoutItemDaoImpl;
import vn.yotel.yoker.dao.impl.CashoutRequestDaoImpl;
import vn.yotel.yoker.domain.CashoutItem;
import vn.yotel.yoker.domain.CashoutRequest;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class UserServiceImpl implements UserService {
    private static final Logger logger = Logger.getLogger("user_core");
    private MongoDbService mongoDbService = new MongoDbServiceImpl();
    private UserDao userDao = new UserDaoImpl();
    private PaymentService paymentService = new PaymentServiceImpl();
    private CashoutRequestDao cashoutRequestDao = new CashoutRequestDaoImpl();
    private CashoutItemDao cashoutItemDao = new CashoutItemDaoImpl();

    public String insertUser(String username, String password) throws SQLException {
        UserDaoImpl userDao = new UserDaoImpl();
        if (userDao.checkUsername(username)) {
            return "1006";
        }
        SecurityDaoImpl securityDao = new SecurityDaoImpl();
        if (securityDao.updateUserInfo(0, username + "," + password, 9)) {
            return "0";
        }
        return "1001";
    }

    public boolean insertUserBySocial(String socialId, String social) throws SQLException {
        SecurityDao securityDao = new SecurityDaoImpl();
        switch (social) {
            case "fb":
                return securityDao.updateUserInfo(0, socialId, 10);
            case "gg":
                return securityDao.updateUserInfo(0, socialId, 11);
            case "apple":
                return securityDao.updateUserInfo(0, socialId, 14);
            case "dev":
                return securityDao.updateUserInfo(0, socialId, 15);
        }
        return false;
    }

    public String updateNickname(int userId, String nickname) throws SQLException {
        UserDaoImpl userDao = new UserDaoImpl();
        if (!userDao.checkNicknameExist(nickname) && (new SecurityDaoImpl()).updateUserInfo(userId, nickname, 6)) {
            return "0";
        }

        return "1010";
    }

    @Override
    public boolean updateClient(UserModel userModel, String appId) throws SQLException {
        updateUser(userModel.getNickname(), (userCacheModel -> {
            userCacheModel.setClient(appId);
        }));
        return userDao.updateClient(userModel.getId(), appId);
    }

    @Override
    public boolean updatePlatform(UserModel userModel, String platform) throws SQLException {
        updateUser(userModel.getNickname(), (userCacheModel -> {
            userCacheModel.setPlatform(platform);
        }));
        return userDao.updatePlatform(userModel.getId(), platform);
    }

    public boolean checkNickname(String nickname) throws SQLException {
        UserDaoImpl dao = new UserDaoImpl();
        return dao.checkNickname(nickname);
    }

    public UserModel getUserByUserName(String username) throws SQLException {
        UserDaoImpl userDao = new UserDaoImpl();
        return userDao.getUserByUserName(username);
    }

    public UserModel getUserByNickName(String nickname) throws SQLException {
        UserDaoImpl userDao = new UserDaoImpl();
        return userDao.getUserByNickName(nickname);
    }

    public UserModel getUserBySocialId(String socialId, String social) throws SQLException {
        UserDao userDao = new UserDaoImpl();
        switch (social) {
            case "fb":
                return userDao.getUserByFBId(socialId);
            case "apple":
                return userDao.getUserByAppleId(socialId);
            case "dev":
                return userDao.getUserByDeviceId(socialId);
            case "gg":
                return userDao.getUserByGGId(socialId);
        }
        return null;
    }

    public long getMoneyUserCache(String nickname, String moneyType) {
        IMap<String, UserCacheModel> usersMap = HazelcastUtils.getUsersMap();
        if (usersMap.containsKey(nickname)) {
            UserCacheModel user = usersMap.get(nickname);
            return user.getMoney(moneyType);
        }
        return 0L;
    }

    public UserCacheModel getMoneyUser(String nickname) {
        IMap<String, UserCacheModel> usersMap = HazelcastUtils.getUsersMap();
        if (usersMap.containsKey(nickname)) {
            UserCacheModel user = usersMap.get(nickname);
            return user;
        }
        return null;
    }

    public long getMoneyCashout(String nickname) throws ParseException {
        IMap<String, UserCacheModel> usersMap = HazelcastUtils.getUsersMap();
        UserCacheModel user;
        return usersMap.containsKey(nickname) && (user = usersMap.get(nickname)).getCashoutTime() != null && VinPlayUtils.compareDate(user.getCashoutTime(), new Date()) == 0 ? (long) user.getCashout() : 0L;
    }

    public long getCurrentMoneyUserCache(String nickname, String moneyType) {
        IMap<String, UserCacheModel> usersMap = HazelcastUtils.getUsersMap();
        if (usersMap.containsKey(nickname)) {
            UserCacheModel user = usersMap.get(nickname);
            return user.getCurrentMoney(moneyType);
        }
        return 0L;
    }

    public UserResponse checkSessionKey(String nickname, String accessToken, Games game) {
        logger.debug("Request checkSessionKey: nickname: " + nickname + ", accessToken: " + accessToken);
        int statusGame = 0;
        try {
            statusGame = GameCommon.getValueInt("STATUS_GAME");
        } catch (KeyNotFoundException e) {
            e.printStackTrace();
            GU.sendOperation("checkSessionKey key not found " + e.getMessage());
        }
        if (statusGame == StatusGames.MAINTAIN.getId()) {
            return UserResponse.failed("1114");
        }

        IMap<String, UserCacheModel> usersMap = HazelcastUtils.getUsersMap();
        if (!usersMap.containsKey(nickname)) {
            return UserResponse.failed();
        }

        UserCacheModel userCache = usersMap.get(nickname);
        if (statusGame == StatusGames.SANDBOX.getId() && !userCache.isCanLoginSandbox()) {
            return UserResponse.failed("1114");
        }

        if (userCache.isBanLogin()) {
            return UserResponse.failed("1109");
        }

        if (!(game == Games.MINIGAME || userCache.getDaily() == 0 && !StatusUser.checkStatus(userCache.getStatus(), game.getId()))) {
            return UserResponse.failed("1111");
        }

        if (!userCache.getAccessToken().equals(accessToken)) {
            return UserResponse.failed("1014");
        }

        if (userCache.getLastActive() != null && VinPlayUtils.sessionTimeout(userCache.getLastActive().getTime())) {
            return UserResponse.failed("1015");
        }

        UserCacheModel uc = null;
        try {
            uc = this.checkMoneyNegative(userCache);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        if (uc != null) {
            return UserResponse.failed();
        }

        updateUser(nickname, (u) -> {
            u.setLastActive(new Date());
            u.setOnline(u.getOnline() + 1);
        });
        UserModel user = new UserModel(userCache.getId(), userCache.getUsername(), userCache.getNickname(), userCache.getPassword(), userCache.getEmail(), userCache.getFacebookId(), userCache.getFacebookId(), userCache.getMobile(), userCache.getBirthday(), userCache.isGender(), userCache.getAddress(), userCache.getVin(), userCache.getXu(), userCache.getVinTotal(), userCache.getXuTotal(), userCache.getSafe(), userCache.getRechargeMoney(), userCache.getCashoutMoney(), userCache.getVippoint(), userCache.getDaily(), userCache.getStatus(), userCache.getAvatar(), userCache.getIdentification(), userCache.getVippointSave(), userCache.getCreateTime(), userCache.getMoneyVP(), userCache.getSecurityTime(), userCache.getLoginOtp(), userCache.isBot(), userCache.getClient(), userCache.getPlatform());
        return UserResponse.success(user);
    }

    public void logout(String nickname) {
        IMap<String, UserCacheModel> usersMap = HazelcastUtils.getUsersMap();
        if (!usersMap.containsKey(nickname)) {
            return;
        }
        updateUser(nickname, (u) -> {
            if (u.getOnline() > 0) {
                u.setOnline(u.getOnline() - 1);
            }
            u.setLastActive(new Date());
        });
        UserCacheModel userCache = usersMap.get(nickname);
        usersMap.put(nickname, userCache);
    }

    public MoneyResponse updateMoneyWithNotify(String nickname, long money, String actionName, String serviceName, String description, boolean showPopup) {
        MoneyResponse response = new MoneyResponse(false, "1001");
        UserCacheModel user = getCacheUserIgnoreCase(nickname);
        if (user == null) {
            response.setErrorCode("Không tìm thấy người dùng");
            return response;
        }
        if (user.getVinTotal() + money < 0L) {
            response.setErrorCode("1002");
            return response;
        }
        // Thay đổi tiền
        updateUser(nickname, (u) -> {
            u.setVin(u.getVin() + money);
            u.setVinTotal(u.getVinTotal() + money);
            try {
                MoneyMessageInMinigame messageMoney = new MoneyMessageInMinigame(
                        VinPlayUtils.genMessageId(),
                        u.getId(), u.getNickname()
                        , actionName
                        , u.getVin(), u.getVinTotal(), money
                        , "vin", 0L, 0, 0);
                RMQApi.publishMessageInMiniGame(messageMoney);

                LogMoneyUserMessage messageLog = new LogMoneyUserMessage(
                        u.getId(), u.getNickname(), actionName, serviceName,
                        u.getVinTotal(), money, "vin", description, 0L, false, u.isBot());
                RMQApi.publishMessageLogMoney(messageLog);

                response.setCurrentMoney(u.getVinTotal());
                response.setSuccess(true);
                response.setErrorCode("0");
            } catch (Exception e) {
                e.printStackTrace();
                GU.sendOperation("RechargeService.recharge: Notify MoneyMessageInMinigame exception " + e.getMessage());
            }
        });
        // Bắn notify về
        if (response.isSuccess())
            try {
                user = getCacheUserIgnoreCase(nickname);
                KingUtil.printLog("updateMoneyWithNotify() nickname: " + nickname + ", change money: " + money + ", after vin total: " + user.getVinTotal() + ", vin: " + user.getVin());
                UpdateUserMoneyMsg msgg = new UpdateUserMoneyMsg();
                msgg.userNickname = nickname;
                msgg.totalMoney = response.getCurrentMoney();
                msgg.changeMoney = 0;
                msgg.desc = description;
                msgg.showPopup = showPopup;
                MqttSender.SendMsgToNickname(nickname, msgg);
            } catch (Exception e) {
                KingUtil.printLog("updateMoneyWithNotify Exception: " + KingUtil.printException(e));
                e.printStackTrace();
            }

        return response;
    }

    public MoneyResponse updateMoney(String nickname, long money, String moneyType, String gameName, String serviceName, String description, long fee, Long transId, TransType type) {
        logger.debug("Request updateMoney:  nickname: " + nickname + ", money: " + money + ", moneyType: " + moneyType + ", gameName: " + gameName + ", serviceName: " + serviceName + ", description: " + description + ", fee: " + fee + ", transId: " + transId + ", TransType: " + type.getId());
        MoneyResponse response = new MoneyResponse(false, "1001");
        HazelcastInstance client = HazelcastClientFactory.getInstance();
        if (client == null) {
            MoneyLogger.log(nickname, gameName, money, fee, moneyType, serviceName, "1030", "can not connect hazelcast");
            response.setErrorCode("1030");
            return response;
        }

        IMap<String, UserCacheModel> usersMap = HazelcastUtils.getUsersMap();
        if (!usersMap.containsKey(nickname)) {
            response.setErrorCode("1011");
            return response;
        }

        updateUser(nickname, (user) -> {
            try {
                long moneyUser = user.getMoney(moneyType);
                long currentMoney = user.getCurrentMoney(moneyType);
                if (money == 0L) {
                    if (moneyType.equals("vin") && transId != null && type.getId() == TransType.END_TRANS.getId()) {
                        IMap vpCache = client.getMap("VPMinigame");
                        String vpCacheId = nickname + gameName + transId;
                        long moneyVP2 = 0L;
                        if (vpCache.containsKey(vpCacheId)) {
                            moneyVP2 = Math.abs((Long) vpCache.get(vpCacheId));
                            vpCache.remove(vpCacheId);
                            if (moneyVP2 > 0L) {
                                List<Integer> vpLst2 = VippointUtils.calculateVP(client, user.getNickname(), (long) user.getMoneyVP() + moneyVP2, false);
                                int vpAddEvent = vpLst2.get(0);
                                int moneyVPs2 = vpLst2.get(1);
                                int vpReal = vpLst2.get(2);
                                user.setVippoint(user.getVippoint() + vpAddEvent);
                                user.setVippointSave(user.getVippointSave() + vpAddEvent);
                                user.setMoneyVP(moneyVPs2);

                                if (vpReal > 0) {
                                    int vpEvent = user.getVpEventReal();
                                    int place = user.getVpEvent();
                                    int placeMax = VippointUtils.calculatePlace(place += vpReal);
                                    int placeMax2 = placeMax > user.getPlace() ? placeMax : user.getPlace();
                                    user.setVpEventReal(vpEvent += vpReal);
                                    user.setVpEvent(place);
                                    user.setPlace(placeMax);
                                    user.setPlaceMax(placeMax2);
                                    VippointEventMessage vpEventMessage2 = new VippointEventMessage(user.getId(), nickname, vpEvent, place, 0, 0, 0, 0, placeMax, placeMax2, 0, 0);
                                    RMQApi.publishMessage("queue_vippoint_event", vpEventMessage2, 801);
                                }

                                VippointMessage message2 = new VippointMessage(user.getId(), nickname, moneyVPs2, vpAddEvent);
                                RMQApi.publishMessagePayment(message2, 18);
                                usersMap.put(nickname, user);
                            }
                        }
                    }

                    response.setSuccess(true);
                    response.setErrorCode("0");
                } else if (moneyUser + money >= 0L) {
                    try {
                        user.setMoney(moneyType, moneyUser += money);
                        user.setCurrentMoney(moneyType, currentMoney += money);
                        response.setCurrentMoney(user.getCurrentMoney(moneyType));
                        long moneyVP = VippointUtils.calculateMoneyVP(moneyType, transId, client, nickname, gameName, money, type);
                        int vp = 0;
                        int moneyVPs = 0;
                        //int vpAddEvent = false;
                        if (moneyVP > 0L) {
                            List<Integer> vpLst = VippointUtils.calculateVP(client, user.getNickname(), (long) user.getMoneyVP() + moneyVP, false);
                            vp = vpLst.get(0);
                            moneyVPs = vpLst.get(1);
                            int vpAddEvent = vpLst.get(2);
                            user.setVippoint(user.getVippoint() + vp);
                            user.setVippointSave(user.getVippointSave() + vp);
                            user.setMoneyVP(moneyVPs);
                            if (vpAddEvent > 0) {
                                int vpReal = user.getVpEventReal();
                                int vpEvent = user.getVpEvent();
                                int place = VippointUtils.calculatePlace(vpEvent += vpAddEvent);
                                int placeMax = place > user.getPlace() ? place : user.getPlace();
                                user.setVpEventReal(vpReal += vpAddEvent);
                                user.setVpEvent(vpEvent);
                                user.setPlace(place);
                                user.setPlaceMax(placeMax);
                                VippointEventMessage vpEventMessage = new VippointEventMessage(user.getId(), nickname, vpReal, vpEvent, 0, 0, 0, 0, place, placeMax, 0, 0);
                                RMQApi.publishMessage("queue_vippoint_event", vpEventMessage, 801);
                            }
                        }

                        boolean playgame = false;
                        if (transId != null || type == TransType.VIPPOINT) {
                            playgame = true;
                        }

                        MoneyMessageInMinigame message = new MoneyMessageInMinigame(VinPlayUtils.genMessageId(), user.getId(), nickname, gameName, moneyUser, currentMoney, money, moneyType, fee, moneyVPs, vp);
                        LogMoneyUserMessage messageLog = new LogMoneyUserMessage(user.getId(), nickname, gameName, serviceName, currentMoney, money, moneyType, description, fee, playgame, user.isBot());
                        RMQApi.publishMessagePayment(message, 16);
                        RMQApi.publishMessageLogMoney(messageLog);

                        response.setSuccess(true);
                        response.setErrorCode("0");
                    } catch (Exception var37) {
                        MoneyLogger.log(nickname, gameName, money, fee, moneyType, serviceName, "1031", "error rmq: " + var37.getMessage());
                        response.setErrorCode("1031");
                        throw new RuntimeException(var37);
                    }
                } else {
                    response.setErrorCode("1002");
                    MoneyLogger.log(nickname, gameName, money, fee, moneyType, serviceName, "1002", "khong du tien");
                }

                response.setCurrentMoney(currentMoney);
            } catch (Exception var38) {
                GU.sendOperation("Trừ tiền lỗi " + Throwables.getStackTraceAsString(var38));
                logger.debug(var38);
                MoneyLogger.log(nickname, gameName, money, fee, moneyType, serviceName, "1030", "error hazelcast: " + var38.getMessage());
                response.setErrorCode("1030");
            }
        });

        logger.debug("Response updateMoney:" + response.toJson());
        return response;
    }

    public MoneyResponse updateMoneyFromAdmin(String nickname, long money, String moneyType, String actionName, String serviceName, String description) {
        logger.debug("Request updateMoneyFromAdmin:  nickname: " + nickname + ", money: " + money + ", moneyType: " + moneyType + ", actionName: " + actionName + ", serviceName: " + serviceName + ", description: " + description);
        MoneyResponse response = new MoneyResponse(false, "1001");
        if (nickname != null && !nickname.isEmpty() && money != 0L) {
            HazelcastInstance client = HazelcastClientFactory.getInstance();
            if (client == null) {
                MoneyLogger.log(nickname, actionName, money, 0L, moneyType, serviceName, "1030", "can not connect hazelcast");
                response.setErrorCode("1030");
                return response;
            }

            try {
                UserDaoImpl userDao = new UserDaoImpl();
                UserModel model = userDao.getUserByNickName(nickname);
                if (model != null) {
                    nickname = model.getNickname();
                    IMap<String, UserCacheModel> usersMap = HazelcastUtils.getUsersMap();
                    if (usersMap.containsKey(nickname)) {
                        MoneyResponse moneyRes = this.updateMoney(nickname, money, moneyType, actionName, serviceName, description, 0L, null, TransType.NO_VIPPOINT);
                        response.setCurrentMoney(moneyRes.getCurrentMoney());
                        response.setSuccess(moneyRes.isSuccess());
                        response.setErrorCode(moneyRes.getErrorCode());

                    } else {
                        try {
                            long currentMoney = model.getCurrentMoney(moneyType);
                            long moneyUser = model.getMoney(moneyType);
                            if (moneyUser + money >= 0L) {
                                int userId = model.getId();
                                if (userDao.updateMoney(userId, money, moneyType)) {
                                    currentMoney += money;
                                    response.setCurrentMoney(currentMoney);
                                    try {
                                        LogMoneyUserMessage messageLog = new LogMoneyUserMessage(userId, nickname, actionName, serviceName, currentMoney, money, moneyType, description, 0L, false, model.isBot());
                                        RMQApi.publishMessageLogMoney(messageLog);
                                    } catch (Exception var19) {
                                        MoneyLogger.log(nickname, actionName, money, 0L, moneyType, serviceName, "1031", "error rmq: " + var19.getMessage());
                                        response.setErrorCode("1031");
                                    }

                                    response.setSuccess(true);
                                    response.setErrorCode("0");
                                }
                            } else {
                                response.setErrorCode("1002");
                            }
                        } catch (Exception var20) {
                            logger.debug(var20);
                            MoneyLogger.log(nickname, actionName, money, 0L, moneyType, serviceName, "1032", "error mysql: " + var20.getMessage());
                            response.setErrorCode("1032");
                            return response;
                        }
                    }
                } else {
                    response.setErrorCode("2001");
                }
            } catch (Exception var21) {
                logger.debug(var21);
                MoneyLogger.log(nickname, actionName, money, 0L, moneyType, serviceName, "1032", "error mysql: " + var21.getMessage());
                response.setErrorCode("1032");
            }
        }

        logger.debug("Response updateMoneyFromAdmin: " + response.toJson());
        return response;
    }

    @Override
    public BaseResponseModel updateMoneyCacheToDB(String nickname) {
        IMap<String, UserCacheModel> usersMap = HazelcastUtils.getUsersMap();
        if (usersMap == null || !usersMap.containsKey(nickname)) {
            return BaseResponseModel.failed();
        }

        final BooleanHolder updated = new BooleanHolder();
        updateUser(nickname, (user -> {
            long moneyUseVin = user.getVin();
            long moneyTotalVin = user.getVinTotal();
            long moneySafe = user.getSafe();
            long moneyUseXu = user.getXu();
            long moneyTotalXu = user.getXuTotal();
            UserDaoImpl dao = new UserDaoImpl();
            try {
                if (dao.restoreMoneyByAdmin(user.getId(), moneyUseVin, moneyTotalVin, moneySafe, "vin") && dao.restoreMoneyByAdmin(user.getId(), moneyUseXu, moneyTotalXu, 0L, "xu")) {
                    updated.value = true;
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
                updated.value = false;
            }
        }));
        if (updated.value) {
            return BaseResponseModel.success();
        }

        return BaseResponseModel.failed();
    }

    public boolean checkMobile(String mobile) throws SQLException {
        UserDaoImpl dao = new UserDaoImpl();
        if (dao.checkMobile(mobile)) {
            return true;
        }
        SecurityDaoImpl sercuDao = new SecurityDaoImpl();
        return sercuDao.checkNewMobile(mobile);
    }

    public boolean checkMobileDaiLy(String mobile) throws SQLException {
        UserDaoImpl dao = new UserDaoImpl();
        return dao.checkMobileDaiLy(mobile);
    }

    public boolean checkMobileSecurity(String mobile) throws SQLException {
        UserDaoImpl dao = new UserDaoImpl();
        return dao.checkMobileSecurity(mobile);
    }

    public boolean checkEmailSecurity(String email) throws SQLException {
        UserDaoImpl dao = new UserDaoImpl();
        return dao.checkEmailSecurity(email);
    }

    public byte checkUser(String nickname) {
        //int res = true;
        int agent = -1;
        IMap<String, UserCacheModel> usersMap = HazelcastUtils.getUsersMap();
        if (usersMap.containsKey(nickname)) {
            UserCacheModel user = usersMap.get(nickname);
            agent = user.getDaily();
        } else {
            UserDaoImpl dao = new UserDaoImpl();

            try {
                agent = dao.checkAgent(nickname);
            } catch (SQLException var8) {
                var8.printStackTrace();
            }
        }

        int res = agent == -1 ? -1 : (agent == 1 ? 1 : (agent == 2 ? 2 : 0));
        return (byte) res;
    }

    public NapXuResponse napXu(String nickname, long moneyVinToXu, boolean check) {
        NapXuResponse response = new NapXuResponse();
        response.setResult((byte) 1);

        try {
            if (GameCommon.getValueInt("IS_NAP_XU") == 1) {
                return response;
            }

            HazelcastInstance client = HazelcastClientFactory.getInstance();
            if (client == null) {
                MoneyLogger.log(nickname, "NapXu", moneyVinToXu, 0L, "vin", "Nap xu", "1030", "can not connect hazelcast");
            }

            IMap<String, UserCacheModel> usersMap = HazelcastUtils.getUsersMap();
            if (usersMap.containsKey(nickname)) {
                try {
                    usersMap.lock(nickname);
                    UserCacheModel user = usersMap.get(nickname);
                    long moneyUserVin = user.getMoney("vin");
                    long currentMoneyVin = user.getCurrentMoney("vin");
                    long moneyUserXu = user.getMoney("xu");
                    long currentMoneyXu = user.getCurrentMoney("xu");
                    if (user.getMobile() != null && user.isHasMobileSecurity()) {
                        if (user.getSecurityTime() != null && VinPlayUtils.cashoutBlockTimeout(user.getSecurityTime(), GameCommon.getValueInt("CASHOUT_TIME_BLOCK"))) {
                            if (moneyVinToXu > 0L) {
                                if (moneyUserVin - moneyVinToXu >= 0L) {
                                    if (check) {
                                        response.setResult((byte) 0);
                                    } else {
                                        TransactionContext context = client.newTransactionContext((new TransactionOptions()).setTransactionType(TransactionType.ONE_PHASE));
                                        context.beginTransaction();

                                        try {
                                            user.setMoney("vin", moneyUserVin -= moneyVinToXu);
                                            user.setCurrentMoney("vin", currentMoneyVin -= moneyVinToXu);
                                            long moneyXuAdded = Math.round((double) moneyVinToXu * GameCommon.getValueDouble("RATIO_NAP_XU"));
                                            user.setMoney("xu", moneyUserXu += moneyXuAdded);
                                            user.setCurrentMoney("xu", currentMoneyXu += moneyXuAdded);
                                            MoneyMessageInMinigame message = new MoneyMessageInMinigame(VinPlayUtils.genMessageId(), user.getId(), nickname, "NapXu", moneyUserVin, currentMoneyVin, -moneyVinToXu, "vin", 0L, 0, 0);
                                            LogMoneyUserMessage messageLogVin = new LogMoneyUserMessage(user.getId(), nickname, "NapXu", "Nạp Xu", currentMoneyVin, -moneyVinToXu, "vin", "Chuyển Vin sang Xu", 0L, false, user.isBot());
                                            RMQApi.publishMessagePayment(message, 16);
                                            RMQApi.publishMessageLogMoney(messageLogVin);
                                            message = new MoneyMessageInMinigame(VinPlayUtils.genMessageId(), user.getId(), nickname, "NapXu", moneyUserXu, currentMoneyXu, moneyXuAdded, "xu", 0L, 0, 0);
                                            LogMoneyUserMessage messageLogXu = new LogMoneyUserMessage(user.getId(), nickname, "NapXu", "Nạp Xu", currentMoneyXu, moneyXuAdded, "xu", "Chuyển Vin sang Xu", 0L, false, user.isBot());
                                            RMQApi.publishMessagePayment(message, 16);
                                            RMQApi.publishMessageLogMoney(messageLogXu);
                                            usersMap.put(nickname, user);
                                            context.commitTransaction();
                                            response.setResult((byte) 0);
                                        } catch (Exception var28) {
                                            logger.debug(var28);
                                            MoneyLogger.log(nickname, "NapXu", moneyVinToXu, 0L, "vin", "Nap xu", "1031", "rmq error: " + var28.getMessage());
                                            context.rollbackTransaction();
                                        }
                                    }
                                } else {
                                    response.setResult((byte) 2);
                                }
                            }

                            response.setCurrentMoneyVin(currentMoneyVin);
                            response.setCurrentMoneyXu(currentMoneyXu);
                        } else {
                            response.setResult((byte) 10);
                        }
                    } else {
                        response.setResult((byte) 3);
                    }
                } catch (Exception var29) {
                    logger.debug(var29);
                    MoneyLogger.log(nickname, "NapXu", moneyVinToXu, 0L, "vin", "Nap xu", "1001", var29.getMessage());
                } finally {
                    usersMap.unlock(nickname);
                }
            }
        } catch (Exception var31) {
            logger.debug(var31);
        }

        return response;
    }

    public List<TopCaoThu> getTopCaoThu(String date, String moneyType, int num) {
        List<TopCaoThu> res = new ArrayList();
        if (!moneyType.equals("vin")) {
            return res;
        }
        HazelcastInstance client = HazelcastClientFactory.getInstance();
        IMap<String, List<TopCaoThu>> topMap = client.getMap("cacheTopCaoThuVin");
        if (!topMap.containsKey(date)) {
            UserDaoImpl dao = new UserDaoImpl();
            res = dao.getTopCaoThu(date, moneyType, num);
            topMap.put(date, res);
        }
        return topMap.get(date);
    }

    public byte checkMoney(String nickname, long money, byte type) {
        IMap<String, UserCacheModel> usersMap = HazelcastUtils.getUsersMap();
        if (!usersMap.containsKey(nickname)) {
            return 1;
        }
        UserCacheModel user = usersMap.get(nickname);
        if (!user.isHasMobileSecurity() || user.getSecurityTime() == null) {
            return 5;
        }
        if (!VinPlayUtils.checkSecurityTimeout(user.getSecurityTime())) {
            return 6;
        }
        if (type != 2 || user.isBanTransferMoney()) {
            return 2;
        }
        if (type != 3 && user.isBanCashOut()) {
            return 3;
        }
        if (user.getVin() >= money) {
            return 0;
        }
        if (type == 4 && user.getVin() >= money) {
            return 0;
        }

        return 1;
    }

    @Override
    public TransferMoneyResponse transferMoney(String nicknameSend, String nicknameReceive, long vin, String description, boolean check) {
        KingUtil.printLog("Request transferMoney: nicknameSend: " + nicknameSend + ", nicknameReceive: " + nicknameReceive + ", money: " + vin + ", description: " + description + ", check: " + check);
        TransferMoneyResponse res = new TransferMoneyResponse((byte) 1, 0L, 0L);
        boolean updateVP = false;
        int status = 0;
        long moneyReceive = 0L;
        try {
            if (GameCommon.getValueInt("IS_TRANSFER_MONEY") == 1) {
                logger.debug("Khoa chuyen tien");
                return res;
            }

            if (nicknameSend == null || nicknameReceive == null || description == null || nicknameSend.equals(nicknameReceive)) {
                logger.debug("Missing param");
                return res;
            }

            IMap<String, UserCacheModel> usersMap = HazelcastUtils.getUsersMap();
            UserModel userReceive = this.getUserByNickName(nicknameReceive);
            AgentDaoImpl agentDao = new AgentDaoImpl();
            AlertServiceImpl alertSer = new AlertServiceImpl();
            if (userReceive != null) {
                nicknameReceive = userReceive.getNickname();
                res.setNicknameReceive(nicknameReceive);
                int feeSMS = GameCommon.getValueInt("SMS_FEE");

                if (vin >= (long) GameCommon.getValueInt("TRANSFER_MONEY_MIN")) {
                    HazelcastInstance client = HazelcastClientFactory.getInstance();
                    if (client == null) {
                        MoneyLogger.log(nicknameSend, "TransferMoney", vin, 0L, "vin", "chuyen khoan", "1030", "can not connect hazelcast");
                        return res;
                    }

                    long feeVal;
                    long fee2;
                    long moneyUserReceive;
                    String time4;
                    String time3;
                    LogMoneyUserMessage messageLogReceiveSMSFee2;
                    if (usersMap.containsKey(nicknameSend)) {
                        try {
                            usersMap.lock(nicknameSend);
                            UserCacheModel userSend = usersMap.get(nicknameSend);
                            String superAgent = GameCommon.getValueStr("SUPER_AGENT");
                            long dl1Max = GameCommon.getValueLong("DL1_TO_SUPER_MAX");
                            long dl1Min = GameCommon.getValueLong("DL1_TO_SUPER_MIN");
                            long dl1MinX = GameCommon.getValueLong("DL1_TO_SUPER_MIN_X");
                            boolean dl1ToSuperAgent = userSend.getDaily() == 1 && nicknameReceive.equals(superAgent);
                            long moneyUser = userSend.getVin();
                            long currentMoney = userSend.getVinTotal();
                            if (!dl1ToSuperAgent || dl1ToSuperAgent && dl1Min <= vin && dl1Max >= vin) {
                                if (dl1ToSuperAgent && (!dl1ToSuperAgent || currentMoney - vin < dl1MinX)) {
                                    res.setCode((byte) 12);
                                } else {
                                    res.setMoneyUse(moneyUser);
                                    res.setCurrentMoney(currentMoney);
                                    if (!userSend.isBanTransferMoney()) {
                                        if (userSend.getMobile() != null && !userSend.getMobile().isEmpty() && userSend.isHasMobileSecurity()) {
                                            if (moneyUser >= vin) {
                                                if (check) {
                                                    res.setCode((byte) 0);
                                                } else {
                                                    TransactionContext context = client.newTransactionContext((new TransactionOptions()).setTransactionType(TransactionType.ONE_PHASE));
                                                    String desReceive2;
                                                    LogMoneyUserMessage messageLogReceive2;
                                                    if (usersMap.containsKey(nicknameReceive)) {
                                                        KingUtil.printLog("Request transferMoney 1 ");
                                                        try {
                                                            context.beginTransaction();
                                                            usersMap.lock(nicknameReceive);
                                                            UserCacheModel userCacheReceive = usersMap.get(nicknameReceive);
                                                            status = this.getStatusChuyenTienDaiLy(userSend.getDaily(), userCacheReceive.getDaily());
                                                            feeVal = Math.round((double) vin * this.getFeeTransfer(status));
                                                            moneyReceive = vin - feeVal;
                                                            res.setMoneyReceive(moneyReceive);
                                                            moneyUserReceive = userCacheReceive.getVin();
                                                            long currentMoneyReceive = userCacheReceive.getVinTotal();
                                                            KingUtil.printLog("Request transferMoney moneyUserReceive: "+moneyUserReceive+", currentMoneyReceive: "+currentMoneyReceive);
                                                            userSend.setVin(moneyUser -= vin);
                                                            userSend.setVinTotal(currentMoney -= vin);
                                                            userCacheReceive.setVin(moneyUserReceive += moneyReceive);
                                                            userCacheReceive.setVinTotal(currentMoneyReceive += moneyReceive);
                                                            MoneyMessageInMinigame messageSend = new MoneyMessageInMinigame(VinPlayUtils.genMessageId(), userSend.getId(), nicknameSend, "TransferMoney", moneyUser, currentMoney, -vin, "vin", feeVal, 0, 0);
                                                            desReceive2 = "Chuyển tới " + nicknameReceive + ": " + description;
                                                            messageLogReceive2 = new LogMoneyUserMessage(userSend.getId(), nicknameSend, "TransferMoney", "Chuyển khoản", currentMoney, -vin, "vin", desReceive2, feeVal, false, userSend.isBot());
                                                            int recharge = 0;
                                                            if (status == 3 || status == 6) {
                                                                recharge = -1;
                                                                userCacheReceive.setRechargeMoney(userCacheReceive.getRechargeMoney() + moneyReceive);
                                                            }

                                                            MoneyMessageInMinigame messageReceive = new MoneyMessageInMinigame(VinPlayUtils.genMessageId(), userCacheReceive.getId(), nicknameReceive, "TransferMoney", moneyUserReceive, currentMoneyReceive, moneyReceive, "vin", 0L, recharge, 0);
                                                            time4 = "Nhận từ " + nicknameSend + ": " + description;
                                                            LogMoneyUserMessage messageLogReceive = new LogMoneyUserMessage(userCacheReceive.getId(), nicknameReceive, "TransferMoney", "Chuyển khoản", currentMoneyReceive, moneyReceive, "vin", time4, 0L, false, userCacheReceive.isBot());
                                                            RMQApi.publishMessagePayment(messageSend, 16);
                                                            RMQApi.publishMessageLogMoney(messageLogReceive2);
                                                            RMQApi.publishMessagePayment(messageReceive, 16);
                                                            RMQApi.publishMessageLogMoney(messageLogReceive);
                                                            if (status != 0) {
                                                                LogChuyenTienDaiLyMessage messageDaily = new LogChuyenTienDaiLyMessage(nicknameSend, nicknameReceive, vin, moneyReceive, feeVal, VinPlayUtils.getCurrentDateTime(), status, desReceive2, time4, VinPlayUtils.genTransactionId(userSend.getId()), 1, this.getAgentLevel1(nicknameSend, nicknameReceive), "");
                                                                RMQApi.publishMessage("queue_log_chuyen_tien_dai_ly", messageDaily, 602);
                                                            }

                                                            usersMap.put(nicknameSend, userSend);
                                                            usersMap.put(nicknameReceive, userCacheReceive);
                                                            KingUtil.printLog("Request transferMoney vin: "+vin+", feeVal: "+feeVal+", moneyReceive: "+moneyReceive);
                                                            KingUtil.printLog("Request transferMoney userCacheReceive vin: "+userCacheReceive.getVin()+", vinTotal: "+userCacheReceive.getVinTotal());
                                                            context.commitTransaction();
                                                            res.setCode((byte) 0);
                                                            res.setMoneyUse(moneyUser);
                                                            res.setCurrentMoney(currentMoney);
                                                            res.setCurrentMoneyReceive(currentMoneyReceive);
                                                            updateVP = true;
                                                            if (dl1ToSuperAgent) {
                                                                Timer timer = new Timer();
                                                                TransferMoneyBankModel model = new TransferMoneyBankModel(nicknameSend, moneyReceive);
                                                                timer.schedule(new TransferMoneyBankService(model), 5000L);
                                                            }
                                                        } catch (Exception var98) {
                                                            logger.debug(var98);
                                                            MoneyLogger.log(nicknameSend, "TransferMoney", vin, 0L, "vin", "chuyen khoan", "1001", var98.getMessage());
                                                            context.rollbackTransaction();
                                                        } finally {
                                                            usersMap.unlock(nicknameReceive);
                                                        }
                                                    } else if (userReceive != null) {
                                                        KingUtil.printLog("Request transferMoney 2");
                                                        try {
                                                            context.beginTransaction();
                                                            long currentMoneyReceive2 = userReceive.getVinTotal();
                                                            KingUtil.printLog("Request transferMoney userReceive vin: "+userReceive.getVin()+", vinTotal: "+userReceive.getVinTotal());
                                                            status = this.getStatusChuyenTienDaiLy(userSend.getDaily(), userReceive.getDaily());
                                                            fee2 = Math.round((double) vin * this.getFeeTransfer(status));
                                                            moneyReceive = vin - fee2;
                                                            res.setMoneyReceive(moneyReceive);
                                                            currentMoneyReceive2 += moneyReceive;
                                                            userSend.setVin(moneyUser -= vin);
                                                            userSend.setVinTotal(currentMoney -= vin);
                                                            MoneyMessageInMinigame messageSend2 = new MoneyMessageInMinigame(VinPlayUtils.genMessageId(), userSend.getId(), nicknameSend, "TransferMoney", moneyUser, currentMoney, -vin, "vin", fee2, 0, 0);
                                                            String desSend2 = "Chuyển tới " + nicknameReceive + ": " + description;
                                                            LogMoneyUserMessage messageLogSend2 = new LogMoneyUserMessage(userSend.getId(), nicknameSend, "TransferMoney", "Chuyển khoản", currentMoney, -vin, "vin", desSend2, fee2, false, userSend.isBot());
                                                            RMQApi.publishMessagePayment(messageSend2, 16);
                                                            RMQApi.publishMessageLogMoney(messageLogSend2);
                                                            usersMap.put(nicknameSend, userSend);
                                                            context.commitTransaction();
                                                            res.setCode((byte) 0);
                                                            res.setMoneyUse(moneyUser);
                                                            res.setCurrentMoney(currentMoney);
                                                            updateVP = true;
                                                            UserDaoImpl dao = new UserDaoImpl();
                                                            KingUtil.printLog("Request transferMoney vin: "+vin+", feeVal: "+fee2+", moneyReceive: "+moneyReceive);
                                                            if (dao.updateMoney(userReceive.getId(), moneyReceive, "vin")) {
                                                                if (status == 3 || status == 6) {
                                                                    try {
                                                                        dao.updateRechargeMoney(userReceive.getId(), moneyReceive);
                                                                    } catch (Exception var93) {
                                                                        logger.debug(var93);
                                                                    }
                                                                }

                                                                res.setCurrentMoneyReceive(currentMoneyReceive2);
                                                                desReceive2 = "Nhận từ " + nicknameSend + ": " + description;
                                                                messageLogReceive2 = new LogMoneyUserMessage(userReceive.getId(), nicknameReceive, "TransferMoney", "Chuyển khoản", currentMoneyReceive2, moneyReceive, "vin", desReceive2, 0L, false, userReceive.isBot());
                                                                RMQApi.publishMessageLogMoney(messageLogReceive2);
                                                                if (status != 0) {
                                                                    LogChuyenTienDaiLyMessage messageDaily2 = new LogChuyenTienDaiLyMessage(nicknameSend, nicknameReceive, vin, moneyReceive, fee2, VinPlayUtils.getCurrentDateTime(), status, desSend2, desReceive2, VinPlayUtils.genTransactionId(userSend.getId()), 1, this.getAgentLevel1(nicknameSend, nicknameReceive), "");
                                                                    RMQApi.publishMessage("queue_log_chuyen_tien_dai_ly", messageDaily2, 602);
                                                                }

                                                                if (dl1ToSuperAgent) {
                                                                    Timer timer2 = new Timer();
                                                                    TransferMoneyBankModel model2 = new TransferMoneyBankModel(nicknameSend, moneyReceive);
                                                                    timer2.schedule(new TransferMoneyBankService(model2), 5000L);
                                                                }
                                                            }
                                                        } catch (Exception var97) {
                                                            logger.debug(var97);
                                                            MoneyLogger.log(nicknameSend, "TransferMoney", vin, 0L, "vin", "chuyen khoan", "1001", var97.getMessage());
                                                            context.rollbackTransaction();
                                                        }
                                                    }
                                                }
                                            } else {
                                                res.setCode((byte) 4);
                                            }
                                        } else {
                                            res.setCode((byte) 3);
                                        }
                                    } else {
                                        res.setCode((byte) 5);
                                    }
                                }
                            } else {
                                res.setCode((byte) 11);
                            }
                        } catch (Exception var101) {
                            logger.debug(var101);
                            MoneyLogger.log(nicknameSend, "TransferMoney", vin, 0L, "vin", "chuyen khoan", "1001", var101.getMessage());
                        } finally {
                            usersMap.unlock(nicknameSend);
                        }
                    } else {
                        KingUtil.printLog("Request transferMoney 3");
                        try {
                            UserDaoImpl userDao = new UserDaoImpl();
                            UserCacheModel userSend2 = userDao.getUserByNickNameCache(nicknameSend);
                            if (userSend2 != null) {
                                String superAgent2 = GameCommon.getValueStr("SUPER_AGENT");
                                long dl1Max2 = GameCommon.getValueLong("DL1_TO_SUPER_MAX");
                                long dl1Min2 = GameCommon.getValueLong("DL1_TO_SUPER_MIN");
                                long dl1MinX2 = GameCommon.getValueLong("DL1_TO_SUPER_MIN_X");
                                boolean dl1ToSuperAgent2 = userSend2.getDaily() == 1 && nicknameReceive.equals(superAgent2);
                                long moneyUser2 = userSend2.getVin();
                                long currentMoney2 = userSend2.getVinTotal();
                                if (!dl1ToSuperAgent2 || dl1ToSuperAgent2 && dl1Min2 <= vin && dl1Max2 >= vin) {
                                    if (dl1ToSuperAgent2 && (!dl1ToSuperAgent2 || currentMoney2 - vin < dl1MinX2)) {
                                        res.setCode((byte) 12);
                                    } else {
                                        res.setMoneyUse(moneyUser2);
                                        res.setCurrentMoney(currentMoney2);
                                        if (!userSend2.isBanTransferMoney()) {
                                            if (userSend2.getMobile() != null && !userSend2.getMobile().isEmpty() && userSend2.isHasMobileSecurity()) {
                                                if (moneyUser2 >= vin) {
                                                    if (check) {
                                                        res.setCode((byte) 0);
                                                    } else {
                                                        TransactionContext context2 = client.newTransactionContext((new TransactionOptions()).setTransactionType(TransactionType.ONE_PHASE));
                                                        String desReceive4;
                                                        String content4;
                                                        if (usersMap.containsKey(nicknameReceive)) {
                                                            try {
                                                                context2.beginTransaction();
                                                                usersMap.lock(nicknameReceive);
                                                                UserCacheModel userCacheReceive2 = usersMap.get(nicknameReceive);
                                                                status = this.getStatusChuyenTienDaiLy(userSend2.getDaily(), userCacheReceive2.getDaily());
                                                                fee2 = Math.round((double) vin * this.getFeeTransfer(status));
                                                                moneyReceive = vin - fee2;
                                                                res.setMoneyReceive(moneyReceive);
                                                                long moneyUserReceive2 = userCacheReceive2.getVin();
                                                                long currentMoneyReceive3 = userCacheReceive2.getVinTotal();
                                                                userSend2.setVin(moneyUser2 -= vin);
                                                                userSend2.setVinTotal(currentMoney2 -= vin);
                                                                userCacheReceive2.setVin(moneyUserReceive2 += moneyReceive);
                                                                userCacheReceive2.setVinTotal(currentMoneyReceive3 += moneyReceive);
                                                                MoneyMessageInMinigame messageSend3 = new MoneyMessageInMinigame(VinPlayUtils.genMessageId(), userSend2.getId(), nicknameSend, "TransferMoney", moneyUser2, currentMoney2, -vin, "vin", fee2, 0, 0);
                                                                desReceive4 = "Chuyển tới " + nicknameReceive + ": " + description;
                                                                messageLogReceiveSMSFee2 = new LogMoneyUserMessage(userSend2.getId(), nicknameSend, "TransferMoney", "Chuyển khoản", currentMoney2, -vin, "vin", desReceive4, fee2, false, userSend2.isBot());
                                                                int recharge2 = 0;
                                                                if (status == 3 || status == 6) {
                                                                    recharge2 = -1;
                                                                    userCacheReceive2.setRechargeMoney(userCacheReceive2.getRechargeMoney() + moneyReceive);
                                                                }

                                                                MoneyMessageInMinigame messageReceive2 = new MoneyMessageInMinigame(VinPlayUtils.genMessageId(), userCacheReceive2.getId(), nicknameReceive, "TransferMoney", moneyUserReceive2, currentMoneyReceive3, moneyReceive, "vin", 0L, recharge2, 0);
                                                                content4 = "Nhận từ " + nicknameSend + ": " + description;
                                                                LogMoneyUserMessage messageLogReceive3 = new LogMoneyUserMessage(userCacheReceive2.getId(), nicknameReceive, "TransferMoney", "Chuyển khoản", currentMoneyReceive3, moneyReceive, "vin", content4, 0L, false, userCacheReceive2.isBot());
                                                                RMQApi.publishMessagePayment(messageSend3, 16);
                                                                RMQApi.publishMessageLogMoney(messageLogReceiveSMSFee2);
                                                                RMQApi.publishMessagePayment(messageReceive2, 16);
                                                                RMQApi.publishMessageLogMoney(messageLogReceive3);
                                                                if (status != 0) {
                                                                    LogChuyenTienDaiLyMessage messageDaily3 = new LogChuyenTienDaiLyMessage(nicknameSend, nicknameReceive, vin, moneyReceive, fee2, VinPlayUtils.getCurrentDateTime(), status, desReceive4, content4, VinPlayUtils.genTransactionId(userSend2.getId()), 1, this.getAgentLevel1(nicknameSend, nicknameReceive), "");
                                                                    RMQApi.publishMessage("queue_log_chuyen_tien_dai_ly", messageDaily3, 602);
                                                                }

                                                                usersMap.put(nicknameSend, userSend2);
                                                                usersMap.put(nicknameReceive, userCacheReceive2);
                                                                context2.commitTransaction();
                                                                res.setCode((byte) 0);
                                                                res.setMoneyUse(moneyUser2);
                                                                res.setCurrentMoney(currentMoney2);
                                                                res.setCurrentMoneyReceive(currentMoneyReceive3);
                                                                updateVP = true;
                                                                if (dl1ToSuperAgent2) {
                                                                    Timer timer3 = new Timer();
                                                                    TransferMoneyBankModel model3 = new TransferMoneyBankModel(nicknameSend, moneyReceive);
                                                                    timer3.schedule(new TransferMoneyBankService(model3), 5000L);
                                                                }
                                                            } catch (Exception var95) {
                                                                logger.debug(var95);
                                                                MoneyLogger.log(nicknameSend, "TransferMoney", vin, 0L, "vin", "chuyen khoan", "1001", var95.getMessage());
                                                                context2.rollbackTransaction();
                                                            } finally {
                                                                usersMap.unlock(nicknameReceive);
                                                            }
                                                        } else if (userReceive != null) {
                                                            try {
                                                                context2.beginTransaction();
                                                                long currentMoneyReceive4 = userReceive.getVinTotal();
                                                                status = this.getStatusChuyenTienDaiLy(userSend2.getDaily(), userReceive.getDaily());
                                                                moneyUserReceive = Math.round((double) vin * this.getFeeTransfer(status));
                                                                moneyReceive = vin - moneyUserReceive;
                                                                res.setMoneyReceive(moneyReceive);
                                                                currentMoneyReceive4 += moneyReceive;
                                                                userSend2.setVin(moneyUser2 -= vin);
                                                                userSend2.setVinTotal(currentMoney2 -= vin);
                                                                MoneyMessageInMinigame messageSend4 = new MoneyMessageInMinigame(VinPlayUtils.genMessageId(), userSend2.getId(), nicknameSend, "TransferMoney", moneyUser2, currentMoney2, -vin, "vin", moneyUserReceive, 0, 0);
                                                                String desSend4 = "Chuyển tới " + nicknameReceive + ": " + description;
                                                                LogMoneyUserMessage messageLogSend4 = new LogMoneyUserMessage(userSend2.getId(), nicknameSend, "TransferMoney", "Chuyển khoản", currentMoney2, -vin, "vin", desSend4, moneyUserReceive, false, userSend2.isBot());
                                                                RMQApi.publishMessagePayment(messageSend4, 16);
                                                                RMQApi.publishMessageLogMoney(messageLogSend4);
                                                                usersMap.put(nicknameSend, userSend2);
                                                                context2.commitTransaction();
                                                                res.setCode((byte) 0);
                                                                res.setMoneyUse(moneyUser2);
                                                                res.setCurrentMoney(currentMoney2);
                                                                updateVP = true;
                                                                UserDaoImpl dao2 = new UserDaoImpl();
                                                                if (dao2.updateMoney(userReceive.getId(), moneyReceive, "vin")) {
                                                                    if (status == 3 || status == 6) {
                                                                        try {
                                                                            dao2.updateRechargeMoney(userReceive.getId(), moneyReceive);
                                                                        } catch (Exception var92) {
                                                                            logger.debug(var92);
                                                                        }
                                                                    }

                                                                    res.setCurrentMoneyReceive(currentMoneyReceive4);
                                                                    desReceive4 = "Nhận từ " + nicknameSend + ": " + description;
                                                                    messageLogReceiveSMSFee2 = new LogMoneyUserMessage(userReceive.getId(), nicknameReceive, "TransferMoney", "Chuyển khoản", currentMoneyReceive4, moneyReceive, "vin", desReceive4, 0L, false, userReceive.isBot());
                                                                    RMQApi.publishMessageLogMoney(messageLogReceiveSMSFee2);
                                                                    if (status != 0) {
                                                                        LogChuyenTienDaiLyMessage messageDaily4 = new LogChuyenTienDaiLyMessage(nicknameSend, nicknameReceive, vin, moneyReceive, moneyUserReceive, VinPlayUtils.getCurrentDateTime(), status, desSend4, desReceive4, VinPlayUtils.genTransactionId(userSend2.getId()), 1, this.getAgentLevel1(nicknameSend, nicknameReceive), "");
                                                                        RMQApi.publishMessage("queue_log_chuyen_tien_dai_ly", messageDaily4, 602);
                                                                    }

                                                                    if (dl1ToSuperAgent2) {
                                                                        Timer timer4 = new Timer();
                                                                        TransferMoneyBankModel model4 = new TransferMoneyBankModel(nicknameSend, moneyReceive);
                                                                        timer4.schedule(new TransferMoneyBankService(model4), 5000L);
                                                                    }
                                                                }
                                                            } catch (Exception var94) {
                                                                logger.debug(var94);
                                                                MoneyLogger.log(nicknameSend, "TransferMoney", vin, 0L, "vin", "chuyen khoan", "1001", var94.getMessage());
                                                                context2.rollbackTransaction();
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    res.setCode((byte) 4);
                                                }
                                            } else {
                                                res.setCode((byte) 3);
                                            }
                                        } else {
                                            res.setCode((byte) 5);
                                        }
                                    }
                                } else {
                                    res.setCode((byte) 11);
                                }
                            }
                        } catch (Exception var100) {
                            logger.debug(var100);
                            MoneyLogger.log(nicknameSend, "TransferMoney", vin, 0L, "vin", "chuyen khoan", "1001", var100.getMessage());
                        }
                    }
                } else {
                    res.setCode((byte) 2);
                }
            } else {
                res.setCode((byte) 6);
            }
        } catch (Exception var104) {
            logger.debug(var104);
            MoneyLogger.log(nicknameSend, "TransferMoney", vin, 0L, "vin", "chuyen khoan", "1001", var104.getMessage());
        }

        if (updateVP) {
            VippointServiceImpl vpSer = new VippointServiceImpl();
            vpSer.updateVippointAgent(nicknameSend, nicknameReceive, vin, moneyReceive, status);
        }

        KingUtil.printLog("Response transferMoney: " + res.getCode());
        return res;
    }

    private String getAgentLevel1(String nicknameSend, String nicknameReceive) {
        AgentDaoImpl agentDao = new AgentDaoImpl();
        String agentLevel1 = "";

        try {
            agentLevel1 = agentDao.getAgentLevel1ByNickName(nicknameSend);
            if (agentLevel1.equals("")) {
                agentLevel1 = agentDao.getAgentLevel1ByNickName(nicknameReceive);
            }
        } catch (Exception var6) {
            var6.printStackTrace();
        }

        return agentLevel1;
    }

    private int getStatusChuyenTienDaiLy(int user1, int user2) {
        int status = 0;
        if ((user1 == 0 || user1 == 100) && user2 == 1) {
            status = 1;
        } else if ((user1 == 0 || user1 == 100) && user2 == 2) {
            status = 2;
        } else if (user1 == 1 && (user2 == 0 || user2 == 100)) {
            status = 3;
        } else if (user1 == 1 && user2 == 1) {
            status = 4;
        } else if (user1 == 1 && user2 == 2) {
            status = 5;
        } else if (user1 == 2 && (user2 == 0 || user2 == 100)) {
            status = 6;
        } else if (user1 == 2 && user2 == 1) {
            status = 7;
        } else if (user1 == 2 && user2 == 2) {
            status = 8;
        }

        return status;
    }

    public double getFeeTransfer(int status) {
        double fee = 0.98D;

        try {
            switch (status) {
                case 0:
                    fee = 1.0D - GameCommon.getValueDouble("RATIO_TRANSFER");
                    break;
                case 1:
                    fee = 1.0D - GameCommon.getValueDouble("RATIO_TRANSFER_01");
                    break;
                case 2:
                    fee = 1.0D - GameCommon.getValueDouble("RATIO_TRANSFER_02");
                    break;
                case 3:
                    fee = 1.0D - GameCommon.getValueDouble("RATIO_TRANSFER_DL_1");
                    break;
                case 4:
                    fee = 1.0D - GameCommon.getValueDouble("RATIO_TRANSFER_11");
                    break;
                case 5:
                    fee = 1.0D - GameCommon.getValueDouble("RATIO_TRANSFER_12");
                    break;
                case 6:
                    fee = 1.0D - GameCommon.getValueDouble("RATIO_TRANSFER_20");
                    break;
                case 7:
                    fee = 1.0D - GameCommon.getValueDouble("RATIO_TRANSFER_21");
                    break;
                case 8:
                    fee = 1.0D - GameCommon.getValueDouble("RATIO_TRANSFER_22");
                    break;
                default:
                    fee = 1.0D - GameCommon.getValueDouble("RATIO_TRANSFER");
            }
        } catch (Exception var5) {
            logger.debug(var5);
        }

        return fee;
    }

    public byte calFeeTransfer(int user1, int user2) {
        //byte res = false;
        int status = this.getStatusChuyenTienDaiLy(user1, user2);
        double fee = this.getFeeTransfer(status);
        byte res = (byte) ((int) (fee * 100.0D));
        return res;
    }

    public UserCacheModel forceGetCachedUser(String nickName) {
        IMap<String, UserCacheModel> usersMap = HazelcastUtils.getUsersMap();
        if (usersMap.containsKey(nickName)) return usersMap.get(nickName);

        UserModel userModel = null;
        try {
            userModel = getUserByNickName(nickName);
        } catch (SQLException e) {
            e.printStackTrace();
            GU.sendOperation("Lỗi load user " + userModel.getNickname() + "\n" + KingUtil.printException(e));
            return null;
        }

        if (userModel == null) return null;

        UserCacheModel userCacheModel = null;
        try {

            userCacheModel = wrapperUser(userModel);
            if (userCacheModel == null) return null;

            usersMap.put(userModel.getNickname(), userCacheModel, 7 * 24, TimeUnit.HOURS);
            return userCacheModel;
        } catch (Exception e) {
            e.printStackTrace();
            GU.sendOperation("Lỗi wrap user " + userModel.getNickname() + "\n" + KingUtil.printException(e));
            return null;
        }
    }

    @Override
    public long getAvailableVinMoney(String nickName) {
        return forceGetCachedUser(nickName).getAvailableVinMoney();
    }

    private void synchronizeUser(UserCacheModel user) {
        IMap<String, UserCacheModel> usersMap = HazelcastUtils.getUsersMap();
        usersMap.put(user.getNickname(), user);
    }

    public UserCacheModel getCacheUserIgnoreCase(String nickname) {
        IMap<String, UserCacheModel> usersMap = HazelcastUtils.getUsersMap();
        // Lấy trong cache
        if (usersMap.containsKey(nickname))
            return usersMap.get(nickname);

        // Tìm trong cache ignore capitalize
        for (Map.Entry<String, UserCacheModel> entry : usersMap.entrySet()) {
            if (nickname.equalsIgnoreCase(entry.getValue().getNickname()))
                return entry.getValue();
        }

        // Tìm trong db
        try {
            UserModel userModel = getUserByNickName(nickname);
            if (userModel != null) {
                UserCacheModel userCacheModel = wrapperUser(userModel);
                usersMap.put(userModel.getNickname(), userCacheModel, 24, TimeUnit.HOURS);
                return userCacheModel;

            }
        } catch (Exception e) {
            KingUtil.printException("UserService", e);
        }
        return null;
    }

    public boolean insertBot(String un, String nn, String pw, long vin, long xu, int status) throws SQLException {
        UserDaoImpl dao = new UserDaoImpl();
        return dao.insertBot(un, nn, pw, vin, xu, status);
    }

    public long getTotalRechargeMoney(String username) {
        IMap<String, UserCacheModel> usersMap = HazelcastUtils.getUsersMap();
        if (usersMap.containsKey(username)) {
            UserCacheModel model = usersMap.get(username);
            return model.getRechargeMoney();
        } else {
            return 0L;
        }
    }

    public int getVipPointSave(String username) {
        UserCacheModel model = this.forceGetCachedUser(username);
        return model != null ? model.getVippointSave() : 0;
    }

    public boolean checkAccesstoken(String nickname, String accessToken) {
        boolean res = false;
        IMap<String, UserCacheModel> usersMap = HazelcastUtils.getUsersMap();
        if (nickname != null && accessToken != null && !nickname.isEmpty() && !accessToken.isEmpty()) {
            try {
                if (usersMap.containsKey(nickname) && (usersMap.get(nickname)).getAccessToken().equals(accessToken)) {
                    res = true;
                }
            } catch (Exception var7) {
                logger.debug(var7);
            }
        }

        return res;
    }

    public int checkBot(String nickname) {
        IMap<String, UserCacheModel> usersMap = HazelcastUtils.getUsersMap();
        if (usersMap.containsKey(nickname)) {
            UserCacheModel user = usersMap.get(nickname);
            if (user.isBot()) {
                return 1;
            }
        } else {
            try {
                UserDaoImpl dao = new UserDaoImpl();
                return dao.checkBotByNickname(nickname);
            } catch (Exception var6) {
                logger.debug(var6);
            }
        }

        return 0;
    }

    public UserExtraInfoModel getUserExtraInfo(String nickname) {
        IMap<String, UserExtraInfoModel> userExtraMap = HCMap.getUserExtraInfoMap();
        return userExtraMap.containsKey(nickname) ? userExtraMap.get(nickname) : null;
    }

    public UserModel getNicknameExactly(String nickname, IMap<String, UserCacheModel> userMap) throws SQLException {
        UserModel model = null;
        if (userMap.containsKey(nickname)) {
            model = new UserModel();
            model.setNickname(nickname);
            model.setBot(userMap.get(nickname).isBot());
        } else {
            model = this.getUserByNickName(nickname);
        }

        return model;
    }

    public List<UserInfoModel> checkPhoneByUser(String phone) throws SQLException {
        UserDaoImpl userDao = new UserDaoImpl();
        return userDao.checkPhoneByUser(phone);
    }

    public UserInfoModel checkPhoneExists(String phone) throws SQLException {
        UserDaoImpl userDao = new UserDaoImpl();
        return userDao.checkPhoneExists(phone);
    }

    @Override
    public UserCacheModel checkMoneyNegative(UserCacheModel user) throws SQLException {
        if (user.getVin() >= 0L && user.getVinTotal() >= 0L && user.getXu() >= 0L && user.getXuTotal() >= 0L) {
            return null;
        }

        user.setBanLogin(true);
        user.setBanCashOut(true);
        user.setBanTransferMoney(true);
        int statusNew = user.getStatus();
        statusNew = StatusUser.changeStatus(statusNew, 0, "1");
        statusNew = StatusUser.changeStatus(statusNew, 1, "1");
        statusNew = StatusUser.changeStatus(statusNew, 3, "1");
        user.setStatus(statusNew);
        SecurityDaoImpl dao = new SecurityDaoImpl();
        dao.updateUserInfo(user.getId(), String.valueOf(statusNew), 7);
        return user;
    }

    @Override
    public void bonusFirstTimeVerifyMobileAndBonus(String nickname) {
        try {
            Map<String, Object> mapFindData = new HashMap<>();
            mapFindData.put("nickName", nickname);
            mapFindData.put("bonusCode", "VERIFY_TELE");
            List<Document> docs = mongoDbService.find("user_bonus", mapFindData, document -> document);
            if (docs.size() > 0)
                return;
        } catch (Exception e) {
            e.printStackTrace();
            KingUtil.printException("UserService checkVerifyMobileAndBonus()", e);
        }
        try {

            // Tiến hành cộng 5k tiền thưởng xác thực SDT tele lần đầu
            GameConfigServiceImpl gameConfigServiceImpl = new GameConfigServiceImpl();
            ResultGameConfigResponse gateInOutConfigData = gameConfigServiceImpl.getGameConfigAdmin("bonus_tele_mobile_verify", "").get(0);
            long bonus = Long.parseLong(gateInOutConfigData.value);
            updateMoneyWithNotify(nickname, bonus, "bonus", "Hệ thống", "Thưởng " + bonus + " xác thực SDT lần đầu", true);
            // Mail inbox
            MailBoxServiceImpl service = new MailBoxServiceImpl();
            service.sendMailPopupMessage(
                    nickname, "Thưởng xác thực OTP", "Chúc mừng bạn đã nhận được: Thưởng " + bonus + " xác thực SDT lần đầu");

            // Hoàn thành nhiệm vụ otp
            // userMissionService.saveNumberDoingTarget(nickname, "otp", 1);

            Map<String, Object> mapInsertData = new HashMap<>();
            mapInsertData.put("nickName", nickname);
            mapInsertData.put("bonusCode", "VERIFY_TELE");
            mapInsertData.put("value", true);
            mapInsertData.put("time", new Date());
            mongoDbService.insert("user_bonus", mapInsertData);
        } catch (Exception e) {
            e.printStackTrace();
            KingUtil.printException("UserService checkVerifyMobileAndBonus()", e);
        }
    }

    @Override
    public void lockUser(String nickName) {
        IMap<String, UserCacheModel> usersMap = HazelcastUtils.getUsersMap();
        usersMap.lock(nickName);
    }

    @Override
    public void unlockUser(String nickName) {
        IMap<String, UserCacheModel> usersMap = HazelcastUtils.getUsersMap();
        usersMap.unlock(nickName);
    }

    @Override
    public void updateUser(String nickName, Consumer<UserCacheModel> consumer, BiConsumer<UserCacheModel, Exception>... exceptionHandlers) {
        if (!checkExistCache(nickName)) {
            forceGetCachedUser(nickName);
        }

        if (!checkExistCache(nickName)) {
            throw new RuntimeException("Khong tim thay nguoi dung " + nickName);
        }

        try {
            lockUser(nickName);
            UserCacheModel user = forceGetCachedUser(nickName);
            if (!user.isBot()) {
                KingUtil.printLog("UserServiceImpl updateUser(), BEFORE update, nickName:" + user.getNickname() + ", vin: " + user.getVin() + ", vin total: " + user.getVinTotal() + ", rechargeMoney: " + user.getRechargeMoney() + ", cashoutMoney: " + user.getCashoutMoney());
            }
            TransactionContext context = HazelcastClientFactory.getInstance().newTransactionContext((new TransactionOptions()).setTransactionType(TransactionType.ONE_PHASE));
            context.beginTransaction();
            try {
                consumer.accept(user);
                synchronizeUser(user);
                context.commitTransaction();
            } catch (RuntimeException ex) {
                context.rollbackTransaction();
                for (BiConsumer<UserCacheModel, Exception> exceptionHandler : exceptionHandlers) {
                    exceptionHandler.accept(user, ex);
                }
            }
            if (!user.isBot()) {
                KingUtil.printLog("UserServiceImpl updateUser(), AFTER update, nickName:" + user.getNickname() + ", vin: " + user.getVin() + ", vin total: " + user.getVinTotal() + ", rechargeMoney: " + user.getRechargeMoney() + ", cashoutMoney: " + user.getCashoutMoney());
            }
        } finally {
            unlockUser(nickName);
        }
    }

    private UserCacheModel wrapperUser(UserModel userModel) throws SQLException {
        if (userModel == null) return null;

        IMap<String, UserCacheModel> usersMap = HazelcastUtils.getUsersMap();
        if (usersMap.containsKey(userModel.getNickname()))
            return usersMap.get(userModel.getNickname());

        CashoutDaoImpl cashoutDao = new CashoutDaoImpl();
        CashoutUserDailyResponse cashoutUserToday = cashoutDao.getCashoutUserToday(userModel.getNickname());

        VippointDaoImpl vpDao = new VippointDaoImpl();
        UserVPEventModel vpModel = vpDao.getUserVPByNickName(userModel.getNickname());

        UserCacheModel userCache = new UserCacheModel(userModel, cashoutUserToday.getCashout(), cashoutUserToday.getCashoutTime(), vpModel);
        userCache.setLastMessageId(0L);
        userCache.setOnline(0);

        MarketingService mktService = new MarketingServiceImpl();
        MarketingModel mktModel = mktService.getMKTInfo(userModel.getUsername());
        if (mktModel != null) {
            userCache.setCampaign(mktModel.campaign);
            userCache.setMedium(mktModel.medium);
            userCache.setSource(mktModel.source);
        }

        userCache.setAccessToken(VinPlayUtils.genAccessToken(userModel.getId()));
        userCache.setClient(userModel.getClient());

        return userCache;
    }

    @Override
    public void unloadUserModelFromCache(UserModel userModel) {
        IMap<String, UserCacheModel> usersMap = HazelcastUtils.getUsersMap();
        usersMap.remove(userModel.getNickname());
    }

    @Override
    public boolean isOnline(String nickname) {
        IMap<String, Boolean> connectedMap = HCMap.getConnectedMinigameMap();
        return connectedMap.containsKey(nickname);
    }

    @Override
    public void sendTeleMessage(String nickname, String mess) {
        MongoDbService mongoDbService = new MongoDbServiceImpl();
        Map<String, Object> mapFindData = new HashMap<>();
        mapFindData.put("nickName", nickname);
        List<Document> docs = mongoDbService.find(AccountTeleLink.class.getSimpleName(), mapFindData);
        if (docs.size() < 1)
            return;
        String teleId = docs.get(0).getString("idUserTele");
        if (teleId == null || teleId.isEmpty())
            return;
        TeleHandle.sendMessage(mess, teleId);
    }

    @Override
    public long getDbCashinMoneyAmount(String nickname) {
        // Lấy danh sách nạp thành công
        List<Document> listRecharge = paymentService.getSuccessRechargeCardData(nickname);
        listRecharge.addAll(paymentService.getSuccessRechargeMomoData(nickname));
        listRecharge.addAll(paymentService.getSuccessRechargeBankData(nickname));
        long rechargeAmount = 0;
        for (Document doc : listRecharge) {
            if (!doc.containsKey("amount"))
                continue;
            long addAmount = 0;
            Object obj = doc.get("amount");
            if (obj instanceof Integer) {
                addAmount = ((Integer) obj).longValue();
            } else if (obj instanceof Long) {
                addAmount = (Long) obj;
            } else if (obj instanceof String) {
                addAmount = Long.parseLong((String) obj);
            }
            rechargeAmount += addAmount;
        }
        return rechargeAmount;
    }

    @Override
    public long getDbCashoutMoneyAmount(String nickname) {
        // Lấy danh sách
        try {
            List<CashoutItem> cashoutItems = cashoutItemDao.findAll();
            List<CashoutRequest> results = cashoutRequestDao.findAllByUserStatus(nickname, Constants.CashoutRequestStatus.COMPLETE_PAY);
            long amount = 0;
            for (CashoutRequest cr : results) {
                CashoutItem ci = null;
                for (CashoutItem _ci : cashoutItems)
                    if (_ci.getId() == cr.getItemId()) {
                        ci = _ci;
                        break;
                    }
                amount += ci != null ? ci.getMoney() : 0;
            }
            return amount;
        } catch (Exception e) {
            KingUtil.printException("UserService getDbCashoutMoneyAmount", e);
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public boolean checkExistCache(String nickname) {
        IMap<String, UserCacheModel> usersMap = HazelcastUtils.getUsersMap();
        return usersMap.containsKey(nickname) ? true : false;
    }
}
