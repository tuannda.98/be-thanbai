/*
 * Decompiled with CFR 0.150.
 *
 * Could not load the following classes:
 *  org.bson.Document
 */
package com.vinplay.dichvuthe.dao;

import com.vinplay.dichvuthe.entities.NganLuongModel;
import com.vinplay.dichvuthe.response.LogApiOtpConfirmResponse;
import com.vinplay.dichvuthe.response.LogApiOtpRequestResponse;
import com.vinplay.dichvuthe.response.LogSMS8x98Response;
import com.vinplay.dichvuthe.response.LogSMSPlusCheckMoResponse;
import com.vinplay.dichvuthe.response.LogSMSPlusResponse;
import com.vinplay.dichvuthe.response.ZoanMomoReq;
import com.vinplay.iap.lib.Purchase;
import com.vinplay.usercore.response.LogRechargeBankNLResponse;
import com.vinplay.usercore.response.LogRechargeBankNapasResponse;
import com.vinplay.vbee.common.exceptions.KeyNotFoundException;
import com.vinplay.vbee.common.messages.dvt.RechargeByBankMessage;
import com.vinplay.vbee.common.messages.dvt.RechargeByCardMessage;
import com.vinplay.vbee.common.messages.dvt.RechargeByCoinMessage;
import com.vinplay.vbee.common.models.BankTranferModel;
import com.vinplay.vbee.common.models.CardTranferModel;
import com.vinplay.vbee.common.models.MomoTranferModel;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.List;
import org.bson.Document;

public interface RechargeDao {
    List<RechargeByCardMessage> getListCardPending(String var1, String var2) throws NumberFormatException, KeyNotFoundException;

    List<RechargeByCardMessage> getListCardPending() throws NumberFormatException, KeyNotFoundException;

    boolean updateCard(String var1, int var2, int var3, String var4, int var5);

    RechargeByBankMessage getRechargeByBank(String var1);

    boolean logRechargeByBank(RechargeByBankMessage var1, String client, String platform) throws Exception;

    boolean logRechargeByCoin(RechargeByCoinMessage mess, String client, String platform) throws Exception;

    boolean updateRechargeByBank(String var1, String var2, String var3, String var4, String var5, String var6) throws Exception;

    boolean insertLogRechargeByBankError(String var1, String var2, String var3, String var4, String var5, String var6, String var7, int var8, String var9, String var10, String var11, String var12, String var13, String var14) throws Exception;

    boolean logRechargeByNL(String var1, String var2, String var3, String var4, String var5, int var6, int var7, int var8, int var9, String var10, String var11, String var12, String var13, String var14, String var15, String var16, String var17);

    boolean updateRechargeByNL(String var1, String var2, String var3);

    boolean logRechargeByNLError(String var1, String var2, String var3);

    NganLuongModel getNLTrans(String var1);

    LogRechargeBankNapasResponse getLogNapas(String var1, String var2, String var3, String var4, String var5, String var6, String var7, String var8, int var9);

    LogRechargeBankNLResponse getLogNL(String var1, String var2, String var3, String var4, String var5, String var6, String var7, String var8, int var9);

    boolean saveLogIAP(Purchase var1, String var2, int var3, int var4, String var5);

    boolean checkOrderId(String var1);

    long getTotalRechargeIapInday(String var1, Calendar var2) throws ParseException;

    boolean checkRequestIdSMS(String var1);

    boolean checkRequestIdSMSPlus(String var1);

    boolean saveLogRechargeBySMS(String var1, String var2, String var3, int var4, String var5, String var6, String var7, int var8, String var9, int var10);

    boolean saveLogRechargeBySMSPlus(String var1, String var2, String var3, int var4, String var5, String var6, String var7, String var8, String var9, int var10, String var11, int var12);

    boolean saveLogRechargeBySMSPlusCheckMO(String var1, String var2, int var3, String var4, int var5, String var6);

    LogSMS8x98Response getLogSMS8x98(String var1, String var2, int var3, String var4, String var5, int var6, String var7, String var8, int var9);

    LogSMSPlusResponse getLogSMSPlus(String var1, String var2, int var3, String var4, int var5, String var6, String var7, int var8);

    boolean saveLogRequestApiOTP(String var1, String var2, int var3, String var4, String var5, String var6, String var7, int var8, String var9);

    boolean saveLogConfirmApiOTP(String var1, String var2, int var3, String var4, String var5, String var6, String var7, String var8, int var9, String var10, int var11);

    LogApiOtpConfirmResponse getApiOtpConfirm(String var1, String var2, int var3, String var4, int var5, String var6, String var7, int var8);

    LogApiOtpRequestResponse getApiOtpRequest(String var1, String var2, int var3, String var4, int var5, String var6, String var7, int var8);

    RechargeByCardMessage getPendingCardByReferenceId(String var1);

    boolean insertLogUpdateCardPending(String var1, String var2, String var3, String var4, String var5, String var6, String var7, String var8, String var9, String var10, String var11, String var12) throws Exception;

    boolean isAgent(String var1) throws SQLException;

    List<String> getListSmsIdNearly();

    List<String> getListSmsPlusIdNearly();

    LogSMSPlusCheckMoResponse getLogSMSPlusCheckMO(String var1, int var2, int var3, String var4, String var5, int var6);

    boolean updateSMS(String var1, int var2, String var3, int var4);

    Document getRechargeByGachthe(String var1);

    Document getRechargeByGachthe(String var1, String var2, String var3);

    List<Document> getRechargeByGachtheRecently();

    boolean saveLogRechargeByGachThe(String var1, String var2, String var3, long var4, String var6, String var7, int var8, String var9, long var10, String var12, String var13, long var14, long var16, int var18, String var19, String var20, String var21);

    boolean UpdateGachtheTransctions(String var1, int var2, String var3);

    boolean UpdateGachtheTransctionsSent(String var1);

    Document getRechargeByNapTienGa(String var1);

    Document getRechargeByNapTienGa(String var1, String var2, String var3);

    List<Document> getRechargeByNapTienGaRecently();

    boolean saveLogRechargeByNapTienGa(String var1, String var2, String var3, long var4, String var6, String var7, int var8, String var9, long var10, String var12, String var13, long var14, long var16, int var18, String var19, int var20);

    boolean UpdateNapTienGaTransctions(String var1, int var2, String var3);

    boolean UpdateNapTienGaTransctionsSent(String var1);

    boolean saveZoanMomoTran(ZoanMomoReq var1, int var2, String var3, long var4, String var6, String var7, String var8, String platform, long addMoney);

    boolean saveZoanMomoM32(String var1, long var2, String var4, String var5, int var6, String var7, long var8, String var10, String var11, String var12);

    boolean saveLogBankTransfer(String var1, String var2, long var3, long var5, String var7, String var8, String var9, int var10);

    boolean updateLogBankTransfer(String var1, int var2);

    Document GetLogBankTransfer(String var1);

    List<BankTranferModel> GetBankTransfer(int var1, int var2, String var3, int var4);

    int GetBankTransferCount(String var1, int var2);

    List<CardTranferModel> GetCardTransfer(int var1, int var2, String var3, int var4);

    List<MomoTranferModel> GetMomoTransfer(int var1, int var2, String var3, int var4);

    int GetMomoTransferCount(String var1, int var2);

    boolean updateLogBankTransferSent(String var1);

    List<Document> getRechargeByBankRecently();

    Document getRechargeByIronMan(String var1);

    boolean UpdateIronManTransctions(String var1, int var2, String var3);

    Document getRechargeByIronMan(String var1, String var2, String var3);

    boolean saveLogRechargeByIronMan(String var1, String var2, String var3, long var4, String var6, String var7, int var8, String var9, long var10, String var12, String var13, long var14, long var16, int var18, String var19);
}

