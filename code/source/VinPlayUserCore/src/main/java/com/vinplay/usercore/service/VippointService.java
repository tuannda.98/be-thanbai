/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.usercore.service;

import com.vinplay.usercore.entities.VPResponse;
import com.vinplay.usercore.entities.VippointResponse;
import java.util.List;

public interface VippointService {
    VippointResponse cashoutVP(String var1);

    byte checkCashoutVP(String var1);

    VPResponse getVippoint(String var1);

    List<String> subVippointEvent();

    List<String> addVippointEvent();

    int updateVippointEvent(String var1, int var2, String var3);

    boolean updateVippointAgent(String var1, String var2, long var3, long var5, int var7);

    boolean resetEvent();
}

