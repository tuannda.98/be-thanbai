package com.vinplay.usercore.service.impl;

import casio.king365.GU;
import casio.king365.util.KingUtil;
import com.google.common.base.Throwables;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.transaction.TransactionContext;
import com.hazelcast.transaction.TransactionOptions;
import com.hazelcast.transaction.TransactionOptions.TransactionType;
import com.vinplay.usercore.dao.impl.MoneyInGameDaoImpl;
import com.vinplay.usercore.logger.MoneyLogger;
import com.vinplay.usercore.service.MoneyInGameService;
import com.vinplay.usercore.utils.VippointUtils;
import com.vinplay.vbee.common.enums.FreezeInGame;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.messages.*;
import com.vinplay.vbee.common.messages.vippoint.VippointEventMessage;
import com.vinplay.vbee.common.models.FreezeModel;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.response.FreezeMoneyResponse;
import com.vinplay.vbee.common.response.MoneyResponse;
import com.vinplay.vbee.common.rmq.RMQApi;
import com.vinplay.vbee.common.statics.TransType;
import com.vinplay.vbee.common.utils.VinPlayUtils;
import org.apache.log4j.Logger;

import java.sql.SQLException;
import java.text.ParseException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

public class MoneyInGameServiceImpl implements MoneyInGameService {
    private static final Logger logger = Logger.getLogger("user_core");

    private final UserServiceImpl userService = new UserServiceImpl();

    public MoneyInGameServiceImpl() {
    }

    @Override
    public FreezeModel getFreeze(String sessionId) throws SQLException {
        HazelcastInstance client = HazelcastClientFactory.getInstance();
        IMap<String, FreezeModel> freezeMap = client.getMap("freeze");
        if (freezeMap.containsKey(sessionId)) {
            return freezeMap.get(sessionId);
        } else {
            MoneyInGameDaoImpl dao = new MoneyInGameDaoImpl();
            return dao.getFreeze(sessionId);
        }
    }

    @Override
    public void pushFreezeToCache(FreezeModel model) {
        if (model == null) return;

        IMap<String, FreezeModel> freezeMap = HazelcastClientFactory.getInstance().getMap("freeze");
        freezeMap.put(model.getSessionId(), model);
    }

    @Override
    public FreezeMoneyResponse freezeMoneyInGame(
            String nickname, String gameName, String roomId, long money, String moneyType) {
        logger.debug("Request freezeMoneyInGame: nickname: " + nickname + ", gameName: " + gameName + ", roomId: " + roomId + ", money: " + money + ", moneyType: " + moneyType);
        KingUtil.printLog("Request freezeMoneyInGame: nickname: " + nickname + ", gameName: " + gameName + ", roomId: " + roomId + ", money: " + money + ", moneyType: " + moneyType);

        FreezeMoneyResponse response = new FreezeMoneyResponse(false, "1001");
        if (money < 0L) {
            response.setErrorCode("1017");
            MoneyLogger.log(nickname, gameName, money, 0L, moneyType, "Dong bang tien", "1017", "money < 0");
            logger.error("freezeMoneyInGame: nickName: " + nickname + " " + response.toJson());
            return response;
        }

        HazelcastInstance client = HazelcastClientFactory.getInstance();
        if (client == null) {
            MoneyLogger.log(nickname, gameName, money, 0L, moneyType, "Dong bang tien", "1030", "can not connect hazelcast");
            response.setErrorCode("1030");
            logger.error("freezeMoneyInGame: nickName: " + nickname + " " + response.toJson());
            return response;
        }

        UserCacheModel userCacheModel = userService.forceGetCachedUser(nickname);
        if (userCacheModel.getMoney(moneyType) < money) {
            response.setErrorCode("1002");
            MoneyLogger.log(nickname, gameName, money, 0L, moneyType, "Dong bang tien", "1002", "Khong du tien");
            return response;
        }

        String sessionId = VinPlayUtils.genSessionId(userCacheModel.getId(), gameName);
        response.setSessionId(sessionId);
        IMap<String, FreezeModel> freezeMap = client.getMap("freeze");
        if (freezeMap.containsKey(sessionId)) {
            response.setErrorCode("1004");
            MoneyLogger.log(nickname, gameName, money, 0L, moneyType, "Dong bang tien", "1002", "sessionId duplicate");
            GU.sendOperation("Lỗi đóng băng tiền: Đã tồn tại sessionId " + sessionId + " ,nickname " + nickname + ", gameName " + gameName
                    + " exist sessionId " + freezeMap.get(sessionId).getSessionId());
            return response;
        }

        userService.updateUser(nickname,
                user -> {
                    if (userCacheModel.getMoney(moneyType) < money) {
                        throw new NotEnoughMoneyRuntimeException();
                    }
                    try {
                        if (money != 0L) {
                            long vinBefore = userCacheModel.getMoney(moneyType);
                            user.setMoney(moneyType, vinBefore - money);
                            LogMoneyUserMessage messageLog = new LogMoneyUserMessage(
                                    user.getId(), user.getNickname(), "freezeMoneyInGame", "MoneyInGame",
                                    user.getCurrentMoney(moneyType), money, moneyType,
                                    "Đóng băng tiền, vinBefore: " + vinBefore
                                            + ", vinAfter: " + user.getMoney(moneyType)
                                            + ", vinTotal: " + user.getCurrentMoney(moneyType)
                                            + ", change money: " + money
                                            + ", gameName: " + gameName
                                            + ", roomId: " + roomId
                                    , 0L, false, user.isBot());
                            RMQApi.publishMessageLogMoney(messageLog);
                        }

                        FreezeMoneyMessage message = new FreezeMoneyMessage(
                                VinPlayUtils.genMessageId(), sessionId, user.getId(), nickname, gameName, roomId,
                                money, user.getMoney(moneyType), user.getCurrentMoney(moneyType), moneyType, null);
                        RMQApi.publishMessagePayment(message, 12);

                        FreezeModel freeze = new FreezeModel(
                                sessionId, nickname, gameName, roomId,
                                money, moneyType, new Date(), null, 0);
                        freezeMap.put(sessionId, freeze);

                        response.setSuccess(true);
                        response.setErrorCode("0");
                        response.setCurrentMoney(user.getCurrentMoney(moneyType));
                    } catch (Exception var24) {
                        throw new RuntimeException(var24);
                    }
                }, (userCacheModel1, exception) -> {
                    if (exception instanceof NotEnoughMoneyRuntimeException) {
                        response.setErrorCode("1002");
                        MoneyLogger.log(nickname, gameName, -money, 0L, moneyType,
                                "Tru tien", "1002", "Khong du tien");
                        logger.error("freezeMoneyInGame" + ": sessionId: " + sessionId + " : nickName: " + nickname + " " + response.toJson());
                        return;
                    }
                    MoneyLogger.log(nickname, gameName, money, 0L, moneyType, "freezeMoneyInGame", "1031", "error: " + exception.getMessage());
                    response.setErrorCode("1031");
                    GU.sendOperation("freezeMoneyInGame: " + Throwables.getStackTraceAsString(exception));
                });

        logger.debug("Response freezeMoneyInGame: nickname: " + nickname + ", gameName: " + gameName + " " + response.toJson());
        return response;
    }

    @Override
    public FreezeMoneyResponse restoreMoneyInGame(
            String sessionId, String nickname, String gameName, String roomId, String moneyType) {
        logger.debug("Request restoreMoneyInGame: sessionId: " + sessionId + ", nickname: " + nickname + ", gameName: " + gameName + ", roomId: " + roomId + ", moneyType: " + moneyType);
        KingUtil.printLog("Request restoreMoneyInGame: sessionId: " + sessionId + ", nickname: " + nickname + ", gameName: " + gameName + ", roomId: " + roomId + ", moneyType: " + moneyType);
        FreezeMoneyResponse response = new FreezeMoneyResponse(false, "1001");

        HazelcastInstance client = HazelcastClientFactory.getInstance();
        if (client == null) {
            MoneyLogger.log(nickname, gameName, 0L, 0L, moneyType, "Mo dong bang tien", "1030", "can not connect hazelcast");
            response.setErrorCode("1030");
            logger.error("restoreMoneyInGame: nickName: " + nickname + " " + response.toJson());
            return response;
        }

        IMap<String, FreezeModel> freezeMap = client.getMap("freeze");
        if (!freezeMap.containsKey(sessionId)) {

            response.setErrorCode("1003");
            MoneyLogger.log(nickname, gameName, 0L, 0L, moneyType, "Mo dong bang tien", "1003", "sessionId not exist: ");
            logger.error("restoreMoneyInGame: nickName: " + nickname + " " + response.toJson(), new Exception("restoreMoneyInGame-1003"));
            GU.sendOperation("Lỗi mở đóng băng tiền, session id khong tồn tại: nickname " + nickname + ", gameName " + gameName
                    + ", sessionId: " + sessionId);
            return response;
        }

        userService.updateUser(nickname,
                user -> {
                    try {
                        FreezeModel freeze = freezeMap.get(sessionId);
                        final long vinBefore = user.getMoney(moneyType);

                        if (freeze.getMoney() <= 0L) {
                            freezeMap.remove(sessionId);
                            response.setCurrentMoney(user.getCurrentMoney(moneyType));
                            response.setSuccess(true);
                            response.setErrorCode("0");
                            return;
                        }

                        if (freeze.getMoney() > 0L) {
                            user.setMoney(moneyType, vinBefore + freeze.getMoney());
                        }

                        LogMoneyUserMessage messageLog = new LogMoneyUserMessage(
                                user.getId(), user.getNickname(), "restoreMoneyInGame", "MoneyInGame",
                                user.getCurrentMoney(moneyType), freeze.getMoney(), "vin",
                                "Mở đóng băng tiền, vinBefore: " + vinBefore
                                        + ", vinAfter: " + user.getMoney(moneyType)
                                        + ", vinTotal: " + user.getCurrentMoney(moneyType)
                                        + ", change money: " + freeze.getMoney()
                                        + ", gameName: " + freeze.getGameName()
                                        + ", roomId: " + freeze.getRoomId()
                                , 0L, false, user.isBot());
                        RMQApi.publishMessageLogMoney(messageLog);

                        FreezeMoneyMessage message = new FreezeMoneyMessage(
                                VinPlayUtils.genMessageId(), sessionId, user.getId(), nickname, gameName, roomId,
                                freeze.getMoney(), user.getMoney(moneyType), user.getCurrentMoney(moneyType), moneyType, null);
                        RMQApi.publishMessagePayment(message, 13);

                        freezeMap.remove(sessionId);

                        response.setCurrentMoney(user.getCurrentMoney(moneyType));
                        response.setSuccess(true);
                        response.setErrorCode("0");
                    } catch (Exception var30) {
                        throw new RuntimeException(var30);
                    }
                },
                (userCacheModel, exception) -> {
                    MoneyLogger.log(nickname, gameName, 0L, 0L, moneyType, "Mo dong bang tien",
                            "1031", "error rmq: " + exception.getMessage());
                    response.setErrorCode("1031");
                    GU.sendOperation("freezeMoneyInGame: " + Throwables.getStackTraceAsString(exception));
                });

        response.setSessionId(sessionId);
        logger.debug("Response restoreMoneyInGame: " + response.toJson());
        return response;
    }

    @Override
    public MoneyResponse addingMoneyInGame(
            String sessionId, String nickname, String gameName, String roomId, long money, String moneyType,
            long maxFreeze, String matchId, long fee) {

        logger.debug("Request addingMoneyInGame: sessionId: " + sessionId + ", nickname: " + nickname + ", gameName: "
                + gameName + ", roomId: " + roomId + ", matchId: " + matchId + ", money: "
                + money + ", moneyType: " + moneyType + ", maxFreeze: " + maxFreeze + ", fee: " + fee);

        MoneyResponse response = new MoneyResponse(false, "1001");
        HazelcastInstance client = HazelcastClientFactory.getInstance();
        if (client == null) {
            MoneyLogger.log(nickname, gameName, money, fee, moneyType, "Cong tien", "1030", "can not connect hazelcast");
            response.setErrorCode("1030");
            logger.error("addingMoneyInGame" + ": sessionId: " + sessionId + " : nickName: " + nickname + " " + response.toJson());
            return response;
        }

        IMap<String, FreezeModel> freezeMap = client.getMap("freeze");
        if (!freezeMap.containsKey(sessionId)) {
            response.setErrorCode("1003");
            MoneyLogger.log(nickname, gameName, 0L, 0L, moneyType, "addingMoneyInGame", "1003", "sessionId not exist: ");
            GU.sendOperation("Lỗi addingMoneyInGame, session id khong tồn tại: nickname " + nickname + ", gameName " + gameName
                    + ", sessionId: " + sessionId);
            return response;
        }

        userService.updateUser(nickname,
                user -> {
                    final FreezeModel freeze = freezeMap.get(sessionId);
                    final long freezeMoneyBefore = freeze.getMoney();

                    if (money <= 0L) {
                        response.setSuccess(true);
                        response.setErrorCode("0");
                        response.setCurrentMoney(user.getCurrentMoney(moneyType));
                        response.setFreezeMoney(freeze.getMoney());
                        logger.warn("addingMoneyInGame <= 0" + ": sessionId: " + sessionId + " : nickName: " + nickname + " " + response.toJson());
                        return;
                    }

                    long addMoneyUser = 0L;
                    long addMoneyFreeze = 0L;
                    final boolean bBuyIn = this.hasBuyInByGameName(gameName);
                    if (bBuyIn) {
                        addMoneyFreeze = money;
                    } else if (freezeMoneyBefore >= maxFreeze) {
                        addMoneyUser = money;
                    } else if (freezeMoneyBefore + money > maxFreeze) {
                        addMoneyUser = freezeMoneyBefore + money - maxFreeze;
                        addMoneyFreeze = maxFreeze - freezeMoneyBefore;
                    } else {
                        addMoneyFreeze = money;
                    }

                    try {
                        user.setMoney(moneyType, user.getMoney(moneyType) + addMoneyUser);
                        user.setCurrentMoney(moneyType, user.getCurrentMoney(moneyType) + money);
                        if (addMoneyFreeze > 0L) {
                            freeze.setMoney(freezeMoneyBefore + addMoneyFreeze);
                        }

                        int vp = 0;
                        int moneyVPs = 0;
                        //int vpAddEvent = false;
                        if (moneyType.equals("vin") && !bBuyIn) {
                            List<Integer> vpLst = VippointUtils.calculateVP(client, user.getNickname(), (long) user.getMoneyVP() + Math.abs(money), false);
                            vp = vpLst.get(0);
                            moneyVPs = vpLst.get(1);
                            int vpAddEvent = vpLst.get(2);
                            user.setVippoint(user.getVippoint() + vp);
                            user.setVippointSave(user.getVippointSave() + vp);
                            user.setMoneyVP(moneyVPs);
                            if (vpAddEvent > 0) {
                                int vpReal = user.getVpEventReal();
                                int vpEvent = user.getVpEvent();
                                int place = VippointUtils.calculatePlace(vpEvent += vpAddEvent);
                                int placeMax = place > user.getPlace() ? place : user.getPlace();
                                user.setVpEventReal(vpReal += vpAddEvent);
                                user.setVpEvent(vpEvent);
                                user.setPlace(place);
                                user.setPlaceMax(placeMax);
                                VippointEventMessage vpEventMessage = new VippointEventMessage(user.getId(), nickname, vpReal, vpEvent, 0, 0, 0, 0, place, placeMax, 0, 0);
                                RMQApi.publishMessage("queue_vippoint_event", vpEventMessage, 801);
                            }
                        }

                        LogMoneyUserMessage messageLog = new LogMoneyUserMessage(
                                user.getId(), nickname, gameName, VinPlayUtils.getServiceName(gameName),
                                user.getCurrentMoney(moneyType), money, moneyType,
                                "Phòng: " + roomId + ", Bàn: " + matchId, fee, true, user.isBot());
                        RMQApi.publishMessageLogMoney(messageLog);

                        MoneyMessageInGame message = new MoneyMessageInGame(
                                VinPlayUtils.genMessageId(), user.getId(), nickname, gameName,
                                user.getMoney(moneyType), user.getCurrentMoney(moneyType),
                                money, moneyType, freeze.getMoney(), sessionId, fee, moneyVPs, vp);
                        RMQApi.publishMessagePayment(message, 10);

                        freeze.updated();
                        freezeMap.put(sessionId, freeze);

                        response.setSuccess(true);
                        response.setErrorCode("0");
                        response.setCurrentMoney(user.getCurrentMoney(moneyType));
                        response.setFreezeMoney(freeze.getMoney());
                        response.setMoneyUse(user.getMoney(moneyType));
                    } catch (Exception var54) {
                        throw new RuntimeException(var54);
                    }
                },
                (userCacheModel, exception) -> {
                    MoneyLogger.log(nickname, gameName, 0L, 0L, moneyType, "Cong tien trong game",
                            "1031", "error rmq: " + exception.getMessage());
                    response.setErrorCode("1031");
                    GU.sendOperation("addingMoneyInGame: " + Throwables.getStackTraceAsString(exception));
                });

        logger.debug("Response addingMoneyInGame: sessionId: " + sessionId + " " + response.toJson());
        return response;
    }

    @Override
    public MoneyResponse subtractMoneyInGame(
            String sessionId, String nickname, String gameName, String roomId, long money, String moneyType,
            String matchId) {
        logger.debug("Request subtractMoneyInGame: sessionId: " + sessionId + ", nickname: " + nickname
                + ", gameName: " + gameName + ", roomId: " + roomId + ", matchId: " + matchId
                + ", money: " + money + ", moneyType: " + moneyType);
        KingUtil.printLog("Request subtractMoneyInGame: sessionId: " + sessionId + ", nickname: "
                + nickname + ", gameName: " + gameName + ", roomId: " + roomId + ", matchId: "
                + matchId + ", money: " + money + ", moneyType: " + moneyType);
        MoneyResponse response = new MoneyResponse(false, "1001");

        HazelcastInstance client = HazelcastClientFactory.getInstance();
        if (client == null) {
            MoneyLogger.log(nickname, gameName, -money, 0L, moneyType, "Tru tien", "1030", "can not connect hazelcast");
            response.setErrorCode("1030");
            logger.error("subtractMoneyInGame" + ": sessionId: " + sessionId + " : nickName: " + nickname + " " + response.toJson());
            return response;
        }

        IMap<String, FreezeModel> freezeMap = client.getMap("freeze");
        if (!freezeMap.containsKey(sessionId)) {
            response.setErrorCode("1003");
            MoneyLogger.log(nickname, gameName, 0L, 0L, moneyType, "subtractMoneyInGame", "1003", "sessionId not exist: ");
            GU.sendOperation("Lỗi subtractMoneyInGame, session id khong tồn tại: nickname " + nickname + ", gameName " + gameName
                    + ", sessionId: " + sessionId);
            return response;
        }

        userService.updateUser(nickname,
                user -> {

                    FreezeModel freeze = freezeMap.get(sessionId);

                    final long moneyBefore = user.getMoney(moneyType);
                    final long moneyTotalBefore = user.getCurrentMoney(moneyType);
                    if (money <= 0L) {
                        response.setSuccess(true);
                        response.setErrorCode("0");
                        response.setCurrentMoney(moneyTotalBefore);
                        response.setFreezeMoney(freeze.getMoney());
                        logger.error("subtractMoneyInGame <= 0" + ": sessionId: " + sessionId + " : nickName: " + nickname + " " + response.toJson());
                        return;
                    }

                    long subtractMoneyUser = 0L;
                    long subtractMoneyFreeze = 0L;


                    // TODO: Check các case subtract vin và vinTotal?
                    final long freezeMoney = freeze.getMoney();
                    boolean bBuyIn = this.hasBuyInByGameName(gameName);
                    if (bBuyIn) {
                        if (freezeMoney >= money) {
                            subtractMoneyFreeze = money;
                        } else if (moneyBefore + freezeMoney >= money) {
                            subtractMoneyFreeze = freezeMoney;
                            subtractMoneyUser = money - freezeMoney;
                        } else {
                            subtractMoneyUser = moneyBefore;
                            subtractMoneyFreeze = freezeMoney;
                        }
                    } else if (moneyBefore >= money) {
                        subtractMoneyUser = money;
                    } else if (moneyBefore + freezeMoney >= money) {
                        subtractMoneyUser = moneyBefore;
                        subtractMoneyFreeze = money - moneyBefore;
                    } else {
                        subtractMoneyUser = moneyBefore;
                        subtractMoneyFreeze = freezeMoney;
                    }

                    final long subtractCurrentMoney = subtractMoneyUser + subtractMoneyFreeze;
                    if (subtractCurrentMoney <= 0L) {
                        throw new NotEnoughMoneyRuntimeException();
                    }

                    try {
                        user.setMoney(moneyType, moneyBefore - subtractMoneyUser);
                        user.setCurrentMoney(moneyType, moneyTotalBefore - money);
                        if (subtractMoneyFreeze > 0L) {
                            freeze.setMoney(freeze.getMoney() - subtractMoneyFreeze);
                        }

                        int vp = 0;
                        int moneyVPs = 0;
                        //int vpAddEvent = false;
                        if (moneyType.equals("vin") && !bBuyIn) {
                            List<Integer> vpLst = VippointUtils.calculateVP(client, user.getNickname(), (long) user.getMoneyVP() + Math.abs(money), false);
                            vp = vpLst.get(0);
                            moneyVPs = vpLst.get(1);
                            int vpAddEvent = vpLst.get(2);
                            user.setVippoint(user.getVippoint() + vp);
                            user.setVippointSave(user.getVippointSave() + vp);
                            user.setMoneyVP(moneyVPs);
                            if (vpAddEvent > 0) {
                                int vpReal = user.getVpEventReal();
                                int vpEvent = user.getVpEvent();
                                int place = VippointUtils.calculatePlace(vpEvent += vpAddEvent);
                                int placeMax = place > user.getPlace() ? place : user.getPlace();
                                user.setVpEventReal(vpReal += vpAddEvent);
                                user.setVpEvent(vpEvent);
                                user.setPlace(place);
                                user.setPlaceMax(placeMax);
                                VippointEventMessage vpEventMessage = new VippointEventMessage(user.getId(), nickname, vpReal, vpEvent, 0, 0, 0, 0, place, placeMax, 0, 0);
                                RMQApi.publishMessage("queue_vippoint_event", vpEventMessage, 801);
                            }
                        }

                        LogMoneyUserMessage messageLog = new LogMoneyUserMessage(
                                user.getId(), nickname, gameName, VinPlayUtils.getServiceName(gameName),
                                user.getCurrentMoney(moneyType), -subtractCurrentMoney, moneyType,
                                "Phòng: " + roomId + ", Bàn: " + matchId +
                                        "\nTiền trong game, vinBefore: " + moneyBefore
                                        + ", vinAfter: " + user.getMoney(moneyType)
                                        + ", vinTotal before: " + moneyTotalBefore
                                        + ", vinTotal after: " + user.getCurrentMoney(moneyType)
                                        + ", money: " + money
                                        + ", freezeMoney: " + freezeMoney
                                        + ", change money: " + " -" + subtractCurrentMoney, 0L, true, user.isBot());
                        RMQApi.publishMessageLogMoney(messageLog);

                        MoneyMessageInGame message = new MoneyMessageInGame(
                                VinPlayUtils.genMessageId(), user.getId(), nickname, gameName,
                                user.getMoney(moneyType), user.getCurrentMoney(moneyType),
                                subtractCurrentMoney, moneyType, freeze.getMoney(), sessionId, 0L, moneyVPs, vp);
                        RMQApi.publishMessagePayment(message, 10);

                        freeze.updated();
                        freezeMap.put(sessionId, freeze);

                        response.setSuccess(true);
                        response.setErrorCode("0");
                        response.setCurrentMoney(user.getCurrentMoney(moneyType));
                        response.setSubtractMoney(subtractCurrentMoney);
                        response.setFreezeMoney(freeze.getMoney());
                        response.setMoneyUse(user.getMoney(moneyType));
                    } catch (Exception var52) {
                        throw new RuntimeException(var52);
                    }
                },
                (userCacheModel, exception) -> {
                    if (exception instanceof NotEnoughMoneyRuntimeException) {
                        response.setErrorCode("1002");
                        MoneyLogger.log(nickname, gameName, -money, 0L, moneyType,
                                "Tru tien", "1002", "Khong du tien");
                        logger.error("subtractMoneyInGame" + ": sessionId: " + sessionId + " : nickName: " + nickname + " " + response.toJson());
                        return;
                    }
                    MoneyLogger.log(nickname, gameName, -money, 0L, moneyType, "Tru tien", "1031", "error: " + exception.getMessage());
                    response.setErrorCode("1031");
                    GU.sendOperation("Lỗi subtractMoneyInGame: " + Throwables.getStackTraceAsString(exception));
                });

        logger.debug("Response subtractMoneyInGame: sessionId: " + sessionId + " " + response.toJson());
        return response;
    }

    @Override
    public MoneyResponse updateMoneyInGameByFreeze(
            String sessionId, String nickname, String gameName, String roomId, long money, String moneyType,
            String matchId, long fee) {
        logger.debug("Request updateMoneyInGameByFreeze: sessionId: " + sessionId + ", nickname: " + nickname
                + ", gameName: " + gameName + ", roomId: " + roomId + ", matchId: " + matchId
                + ", money: " + money + ", moneyType: " + moneyType + ", fee: " + fee);
        KingUtil.printLog("Request updateMoneyInGameByFreeze: sessionId: " + sessionId + ", nickname: " + nickname
                + ", gameName: " + gameName + ", roomId: " + roomId + ", matchId: " + matchId
                + ", money: " + money + ", moneyType: " + moneyType + ", fee: " + fee);

        MoneyResponse response = new MoneyResponse(false, "1001");
        HazelcastInstance client = HazelcastClientFactory.getInstance();
        if (client == null) {
            MoneyLogger.log(nickname, gameName, money, fee, moneyType, "Cong tru tien dong bang", "1030", "can not connect hazelcast");
            response.setErrorCode("1030");
            logger.error("updateMoneyInGameByFreeze" + ": sessionId: " + sessionId + " : nickName: " + nickname + " " + response.toJson());
            return response;
        }

        IMap<String, FreezeModel> freezeMap = client.getMap("freeze");
        if (!freezeMap.containsKey(sessionId)) {
            response.setErrorCode("1003");
            MoneyLogger.log(nickname, gameName, 0L, 0L, moneyType, "updateMoneyInGameByFreeze", "1003", "sessionId not exist: ");
            GU.sendOperation("Lỗi updateMoneyInGameByFreeze, session id khong tồn tại: nickname " + nickname + ", gameName " + gameName
                    + ", sessionId: " + sessionId);
            return response;
        }

        userService.updateUser(nickname,
                user -> {
                    FreezeModel freeze = freezeMap.get(sessionId);

                    if (money == 0L) {
                        response.setSuccess(true);
                        response.setErrorCode("0");
                        response.setCurrentMoney(user.getCurrentMoney(moneyType));
                        response.setFreezeMoney(freeze.getMoney());
                        logger.warn("updateMoneyInGameByFreeze = 0" + ": sessionId: " + sessionId + " : nickName: " + nickname + " " + response.toJson());
                        return;
                    }

                    if (freeze.getMoney() + money < 0L) {
                        throw new NotEnoughMoneyRuntimeException();
                    }

                    try {
                        final long moneyBefore = user.getMoney(moneyType);
                        final long moneyTotalBefore = user.getCurrentMoney(moneyType);

                        user.setCurrentMoney(moneyType, user.getCurrentMoney(moneyType) + money);
                        freeze.setMoney(freeze.getMoney() + money);

                        boolean bBuyIn = this.hasBuyInByGameName(gameName);
                        int vp = 0;
                        int moneyVPs = 0;
                        //int vpAddEvent = false;
                        if (moneyType.equals("vin") && !bBuyIn) {
                            List<Integer> vpLst = VippointUtils.calculateVP(client, user.getNickname(), (long) user.getMoneyVP() + Math.abs(money), false);
                            vp = vpLst.get(0);
                            moneyVPs = vpLst.get(1);
                            int vpAddEvent = vpLst.get(2);
                            user.setVippoint(user.getVippoint() + vp);
                            user.setVippointSave(user.getVippointSave() + vp);
                            user.setMoneyVP(moneyVPs);
                            if (vpAddEvent > 0) {
                                int vpReal = user.getVpEventReal();
                                int vpEvent = user.getVpEvent();
                                int place = VippointUtils.calculatePlace(vpEvent += vpAddEvent);
                                int placeMax = place > user.getPlace() ? place : user.getPlace();
                                user.setVpEventReal(vpReal += vpAddEvent);
                                user.setVpEvent(vpEvent);
                                user.setPlace(place);
                                user.setPlaceMax(placeMax);
                                VippointEventMessage vpEventMessage = new VippointEventMessage(user.getId(), nickname, vpReal, vpEvent, 0, 0, 0, 0, place, placeMax, 0, 0);
                                RMQApi.publishMessage("queue_vippoint_event", vpEventMessage, 801);
                            }
                        }

                        LogMoneyUserMessage messageLog = new LogMoneyUserMessage(
                                user.getId(), nickname, gameName, VinPlayUtils.getServiceName(gameName),
                                user.getCurrentMoney(moneyType), money, moneyType,
                                "Phòng: " + roomId + ", Bàn: " + matchId +
                                        "\nTiền trong game, vinBefore: " + moneyBefore
                                        + ", vinAfter: " + user.getMoney(moneyType)
                                        + ", vinTotal before: " + moneyTotalBefore
                                        + ", vinTotal after: " + user.getCurrentMoney(moneyType)
                                        + ", money: " + money
                                        + ", freezeMoney: " + freeze.getMoney()
                                        + ", change money: " + money, fee, true, user.isBot());
                        RMQApi.publishMessageLogMoney(messageLog);


                        MoneyMessageInGame message = new MoneyMessageInGame(
                                VinPlayUtils.genMessageId(), user.getId(), nickname, gameName,
                                user.getMoney(moneyType), user.getCurrentMoney(moneyType), money, moneyType,
                                freeze.getMoney(), sessionId, fee, moneyVPs, vp);
                        RMQApi.publishMessagePayment(message, 10);

                        freeze.updated();
                        freezeMap.put(sessionId, freeze);

                        response.setSuccess(true);
                        response.setErrorCode("0");
                        response.setCurrentMoney(user.getCurrentMoney(moneyType));
                        response.setFreezeMoney(freeze.getMoney());
                        response.setMoneyUse(user.getMoney(moneyType));
                        if (money < 0L) {
                            response.setSubtractMoney(Math.abs(money));
                        }
                    } catch (Exception var46) {
                        throw new RuntimeException(var46);
                    }
                },
                (userCacheModel, exception) -> {
                    if (exception instanceof NotEnoughMoneyRuntimeException) {
                        response.setErrorCode("1002");
                        MoneyLogger.log(nickname, gameName, money, fee, moneyType,
                                "Cong tru tien dong bang", "1002", "Khong du tien");
                        logger.info("updateMoneyInGameByFreeze" + ": sessionId: " + sessionId + " : nickName: " + nickname + " " + response.toJson());
                        return;
                    }
                    MoneyLogger.log(nickname, gameName, money, fee, moneyType,
                            "updateMoneyInGameByFreeze", "1031", "error: " + exception.getMessage());
                    response.setErrorCode("1031");
                    GU.sendOperation("Lỗi updateMoneyInGameByFreeze: " + Throwables.getStackTraceAsString(exception));
                });

        logger.debug("Response updateMoneyInGameByFreeze: sessionId: " + sessionId + " " + response.toJson());
        return response;
    }

    @Override
    public MoneyResponse subtractMoneyInGameExactly(
            String sessionId, String nickname, String gameName, String roomId, long money, String moneyType,
            String matchId) {
        logger.debug("Request subtractMoneyInGameExactly: sessionId: " + sessionId + ", nickname: " + nickname + ", gameName: " + gameName + ", roomId: " + roomId + ", matchId: " + matchId + ", money: " + money + ", moneyType: " + moneyType);
        KingUtil.printLog("Request subtractMoneyInGameExactly: sessionId: " + sessionId + ", nickname: " + nickname + ", gameName: " + gameName + ", roomId: " + roomId + ", matchId: " + matchId + ", money: " + money + ", moneyType: " + moneyType);

        MoneyResponse response = new MoneyResponse(false, "1001");
        HazelcastInstance client = HazelcastClientFactory.getInstance();
        if (client == null) {
            MoneyLogger.log(nickname, gameName, -money, 0L, moneyType, "Tru tien", "1030", "can not connect hazelcast");
            response.setErrorCode("1030");
            logger.error("subtractMoneyInGameExactly" + ": sessionId: " + sessionId + " : nickName: " + nickname + " " + response.toJson());
            return response;
        }

        IMap<String, FreezeModel> freezeMap = client.getMap("freeze");
        if (!freezeMap.containsKey(sessionId)) {
            response.setErrorCode("1003");
            MoneyLogger.log(nickname, gameName, 0L, 0L, moneyType, "subtractMoneyInGameExactly", "1003", "sessionId not exist: ");
            GU.sendOperation("Lỗi subtractMoneyInGameExactly, session id khong tồn tại: nickname " + nickname + ", gameName " + gameName
                    + ", sessionId: " + sessionId);
            return response;
        }

        userService.updateUser(nickname,
                user -> {
                    final long moneyUserBefore = user.getMoney(moneyType);
                    final long currentMoneyBefore = user.getCurrentMoney(moneyType);
                    FreezeModel freeze = freezeMap.get(sessionId);
                    if (money <= 0L) {
                        response.setSuccess(true);
                        response.setErrorCode("0");
                        response.setCurrentMoney(currentMoneyBefore);
                        response.setFreezeMoney(freeze.getMoney());
                        logger.warn("subtractMoneyInGameExactly <= 0L" + ": sessionId: " + sessionId + " : nickName: " + nickname + " " + response.toJson());
                        return;
                    }
                    if (moneyUserBefore + freeze.getMoney() < money) {
                        throw new NotEnoughMoneyRuntimeException();
                    }

                    long subtractMoneyUser = 0L;
                    long subtractMoneyFreeze = 0L;
                    final long freezeMoneyBefore = freeze.getMoney();

                    boolean bBuyIn = this.hasBuyInByGameName(gameName);
                    if (bBuyIn) {
                        if (freezeMoneyBefore >= money) {
                            subtractMoneyFreeze = money;
                        } else if (moneyUserBefore + freezeMoneyBefore >= money) {
                            subtractMoneyFreeze = freezeMoneyBefore;
                            subtractMoneyUser = money - freezeMoneyBefore;
                        } else {
                            subtractMoneyUser = moneyUserBefore;
                            subtractMoneyFreeze = freezeMoneyBefore;
                        }
                    } else if (moneyUserBefore >= money) {
                        subtractMoneyUser = money;
                    } else if (moneyUserBefore + freezeMoneyBefore >= money) {
                        subtractMoneyUser = moneyUserBefore;
                        subtractMoneyFreeze = money - moneyUserBefore;
                    } else {
                        subtractMoneyUser = moneyUserBefore;
                        subtractMoneyFreeze = freezeMoneyBefore;
                    }

                    final long subtractCurrentMoney = subtractMoneyUser + subtractMoneyFreeze;
                    if (subtractCurrentMoney <= 0L) {
                        throw new NotEnoughMoneyRuntimeException();
                    }

                    try {
                        user.setMoney(moneyType, moneyUserBefore - subtractMoneyUser);
                        user.setCurrentMoney(moneyType, user.getCurrentMoney(moneyType) - subtractCurrentMoney);

                        if (subtractMoneyFreeze > 0L) {
                            freeze.setMoney(freezeMoneyBefore - subtractMoneyFreeze);
                        }

                        int vp = 0;
                        int moneyVPs = 0;
                        //int vpAddEvent = false;
                        if (moneyType.equals("vin") && !bBuyIn) {
                            List<Integer> vpLst = VippointUtils.calculateVP(client, user.getNickname(), (long) user.getMoneyVP() + Math.abs(money), false);
                            vp = vpLst.get(0);
                            moneyVPs = vpLst.get(1);
                            int vpAddEvent = vpLst.get(2);
                            user.setVippoint(user.getVippoint() + vp);
                            user.setVippointSave(user.getVippointSave() + vp);
                            user.setMoneyVP(moneyVPs);
                            if (vpAddEvent > 0) {
                                int vpReal = user.getVpEventReal();
                                int vpEvent = user.getVpEvent();
                                int place = VippointUtils.calculatePlace(vpEvent += vpAddEvent);
                                int placeMax = place > user.getPlace() ? place : user.getPlace();
                                user.setVpEventReal(vpReal += vpAddEvent);
                                user.setVpEvent(vpEvent);
                                user.setPlace(place);
                                user.setPlaceMax(placeMax);
                                VippointEventMessage vpEventMessage = new VippointEventMessage(user.getId(), nickname, vpReal, vpEvent, 0, 0, 0, 0, place, placeMax, 0, 0);
                                RMQApi.publishMessage("queue_vippoint_event", vpEventMessage, 801);
                            }
                        }

                        LogMoneyUserMessage messageLog = new LogMoneyUserMessage(
                                user.getId(), nickname, gameName, VinPlayUtils.getServiceName(gameName),
                                user.getCurrentMoney(moneyType), -subtractCurrentMoney, moneyType,
                                "Phòng: " + roomId + ", Bàn: " + matchId
                                        + "\nTiền trong game, vinBefore: " + moneyUserBefore
                                        + ", vinAfter: " + user.getMoney(moneyType)
                                        + ", vinTotal before: " + currentMoneyBefore
                                        + ", vinTotal after: " + user.getCurrentMoney(moneyType)
                                        + ", money: " + money
                                        + ", freezeMoney: " + freeze.getMoney()
                                        + ", change money: " + "-" + subtractCurrentMoney, 0L, true, user.isBot());
                        RMQApi.publishMessageLogMoney(messageLog);

                        MoneyMessageInGame message = new MoneyMessageInGame(
                                VinPlayUtils.genMessageId(), user.getId(), nickname, gameName,
                                user.getMoney(moneyType), user.getCurrentMoney(moneyType), subtractCurrentMoney, moneyType,
                                freeze.getMoney(), sessionId, 0L, moneyVPs, vp);
                        RMQApi.publishMessagePayment(message, 10);

                        freeze.updated();
                        freezeMap.put(sessionId, freeze);

                        response.setSuccess(true);
                        response.setErrorCode("0");
                        response.setCurrentMoney(user.getCurrentMoney(moneyType));
                        response.setSubtractMoney(subtractCurrentMoney);
                        response.setFreezeMoney(freeze.getMoney());
                        response.setMoneyUse(user.getMoney(moneyType));
                    } catch (Exception var52) {
                        throw new RuntimeException(var52);
                    }
                },
                (userCacheModel, exception) -> {
                    if (exception instanceof NotEnoughMoneyRuntimeException) {
                        response.setErrorCode("1002");
                        MoneyLogger.log(nickname, gameName, money, 0L, moneyType,
                                "subtractMoneyInGameExactly", "1002", "Khong du tien");
                        return;
                    }
                    MoneyLogger.log(nickname, gameName, money, 0L, moneyType,
                            "subtractMoneyInGameExactly", "1031", "error: " + exception.getMessage());
                    response.setErrorCode("1031");
                    GU.sendOperation("Lỗi subtractMoneyInGameExactly: " + Throwables.getStackTraceAsString(exception));
                });

        logger.debug("Response subtractMoneyInGameExactly: sessionId: " + sessionId + " " + response.toJson());
        return response;
    }

    @Override
    public MoneyResponse addFreezeMoneyInGame(
            String sessionId, String nickname, String gameName, String roomId, String matchId,
            long money, String moneyType, FreezeInGame type) {
        logger.debug("Request addFreezeMoneyInGame: sessionId: " + sessionId + ", nickname: " + nickname + ", gameName: " + gameName + ", roomId: " + roomId + ", matchId: " + matchId + ", money: " + money + ", moneyType: " + moneyType + ", type: " + type.getId());
        MoneyResponse response = new MoneyResponse(false, "1001");

        HazelcastInstance client = HazelcastClientFactory.getInstance();
        if (client == null) {
            MoneyLogger.log(nickname, gameName, money, 0L, moneyType, "Dong bang them tien", "1030", "can not connect hazelcast");
            response.setErrorCode("1030");
            return response;
        }

        IMap<String, FreezeModel> freezeMap = client.getMap("freeze");
        if (!freezeMap.containsKey(sessionId)) {
            response.setErrorCode("1003");
            MoneyLogger.log(nickname, gameName, money, 0L, moneyType, "Dong bang them tien", "1003", "session not exist");
            logger.error("addFreezeMoneyInGame" + ": sessionId: " + sessionId + " : nickName: " + nickname + " " + response.toJson());
            return response;
        }

        userService.updateUser(nickname,
                user -> {
                    FreezeModel freeze = freezeMap.get(sessionId);

                    if (money <= 0L) {
                        response.setSuccess(true);
                        response.setErrorCode("0");
                        response.setCurrentMoney(user.getCurrentMoney(moneyType));
                        response.setFreezeMoney(freeze.getMoney());
                        return;
                    }

                    final long freezeMoneyBefore = freeze.getMoney();
                    final long moneyUserBefore = user.getMoney(moneyType);

                    long moneyUserExchange = 0L;
                    long moneyFreezeExchange = 0L;
                    boolean next = false;
                    if (type == FreezeInGame.MORE) {
                        if (moneyUserBefore >= money) {
                            moneyUserExchange = -money;
                            next = true;
                        }
                    } else if (type == FreezeInGame.SET) {
                        if (moneyUserBefore + freezeMoneyBefore >= money) {
                            moneyUserExchange = freezeMoneyBefore - money;
                            next = true;
                        }
                    } else if (type == FreezeInGame.ALL_MIN) {
                        if (moneyUserBefore + freezeMoneyBefore >= money) {
                            moneyUserExchange = -moneyUserBefore;
                            next = true;
                        }
                    } else if (type == FreezeInGame.ALL) {
                        moneyUserExchange = -moneyUserBefore;
                        next = true;
                    }

                    moneyFreezeExchange = -moneyUserExchange;
                    if (!next) {
                        throw new NotEnoughMoneyRuntimeException();
                    }

                    user.setMoney(moneyType, moneyUserBefore + moneyUserExchange);
                    freeze.setMoney(freezeMoneyBefore + moneyFreezeExchange);

                    try {
                        LogMoneyUserMessage messageLog = new LogMoneyUserMessage(
                                user.getId(), nickname, gameName, VinPlayUtils.getServiceName(gameName),
                                user.getCurrentMoney(moneyType), moneyFreezeExchange, moneyType,
                                "Phòng: " + roomId + ", Bàn: " + matchId
                                        + "\nTiền trong game, vinBefore: " + moneyUserBefore
                                        + ", vinAfter: " + user.getMoney(moneyType)
                                        + ", vinTotal before: " + user.getCurrentMoney(moneyType)
                                        + ", vinTotal after: " + user.getCurrentMoney(moneyType)
                                        + ", money: " + money
                                        + ", freezeMoney: " + freeze.getMoney()
                                        + ", vin to freeze money: " + moneyFreezeExchange, 0L, false, user.isBot());
                        RMQApi.publishMessageLogMoney(messageLog);

                        MoneyMessageInGame message = new MoneyMessageInGame(
                                VinPlayUtils.genMessageId(), user.getId(), nickname, gameName,
                                user.getMoney(moneyType), user.getCurrentMoney(moneyType), 0L, moneyType,
                                freeze.getMoney(), sessionId, 0L, user.getMoneyVP(), 0);
                        RMQApi.publishMessagePayment(message, 10);
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }

                    freeze.updated();
                    freezeMap.put(sessionId, freeze);

                    response.setSuccess(true);
                    response.setErrorCode("0");
                    response.setCurrentMoney(user.getCurrentMoney(moneyType));
                    response.setFreezeMoney(freeze.getMoney());
                    response.setMoneyUse(user.getMoney(moneyType));
                },
                (userCacheModel, exception) -> {
                    if (exception instanceof NotEnoughMoneyRuntimeException) {
                        response.setErrorCode("1002");
                        MoneyLogger.log(nickname, gameName, money, 0L, moneyType,
                                "addFreezeMoneyInGame", "1002", "Khong du tien");
                        return;
                    }
                    MoneyLogger.log(nickname, gameName, money, 0L, moneyType,
                            "addFreezeMoneyInGame", "1031", "error: " + exception.getMessage());
                    response.setErrorCode("1031");
                    GU.sendOperation("Lỗi addFreezeMoneyInGame: " + Throwables.getStackTraceAsString(exception));
                });

        logger.debug("Response addFreezeMoneyInGame:  sessionId: " + sessionId + " " + response.toJson());
        return response;
    }

    @Override
    public MoneyResponse updateMoneyUser(
            String nickname, long money, String moneyType, String gameName, String serviceName,
            String description, long fee, Long transId, TransType type, boolean playGame) {
        logger.debug("Request updateMoneyUser:  nickname: " + nickname + ", money: " + money + ", moneyType: " + moneyType + ", gameName: " + gameName + ", serviceName: " + serviceName + ", description: " + description + ", fee: " + fee + ", transId: " + transId + ", TransType: " + type.getId());
        KingUtil.printLog("Request updateMoneyUser:  nickname: " + nickname + ", money: " + money + ", moneyType: " + moneyType + ", gameName: " + gameName + ", serviceName: " + serviceName + ", description: " + description + ", fee: " + fee + ", transId: " + transId + ", TransType: " + type.getId());
        MoneyResponse response = new MoneyResponse(false, "1001");
        HazelcastInstance client = HazelcastClientFactory.getInstance();
        if (client == null) {
            MoneyLogger.log(nickname, gameName, money, fee, moneyType, serviceName, "1030", "can not connect hazelcast");
            response.setErrorCode("1030");
            logger.error("updateMoneyUser: nickName: " + nickname + " " + response.toJson());
            return response;
        }

        IMap<String, UserModel> userMap = client.getMap("users");
        try {
            userMap.lock(nickname);
            UserCacheModel user = (UserCacheModel) userMap.get(nickname);
            long moneyUser = user.getMoney(moneyType);
            long currentMoney = user.getCurrentMoney(moneyType);
            int vpReal;
            int vpEvent;
            int place;
            int placeMax;
            int vpAddEvent;
            if (money != 0L) {
                if (moneyUser + money >= 0L) {
                    TransactionContext context = client.newTransactionContext((new TransactionOptions()).setTransactionType(TransactionType.ONE_PHASE));
                    context.beginTransaction();

                    try {
                        user.setMoney(moneyType, moneyUser += money);
                        user.setCurrentMoney(moneyType, currentMoney += money);
                        long moneyVP = VippointUtils.calculateMoneyVP(moneyType, transId, client, nickname, gameName, money, type);
                        int vp = 0;
                        int moneyVPs = 0;
                        //int vpAddEvent = false;
                        if (moneyVP > 0L) {
                            List<Integer> vpLst = VippointUtils.calculateVP(client, user.getNickname(), (long) user.getMoneyVP() + moneyVP, false);
                            vp = vpLst.get(0);
                            moneyVPs = vpLst.get(1);
                            vpAddEvent = vpLst.get(2);
                            user.setVippoint(user.getVippoint() + vp);
                            user.setVippointSave(user.getVippointSave() + vp);
                            user.setMoneyVP(moneyVPs);
                            if (vpAddEvent > 0) {
                                vpReal = user.getVpEventReal();
                                vpEvent = user.getVpEvent();
                                place = VippointUtils.calculatePlace(vpEvent += vpAddEvent);
                                placeMax = place > user.getPlace() ? place : user.getPlace();
                                user.setVpEventReal(vpReal += vpAddEvent);
                                user.setVpEvent(vpEvent);
                                user.setPlace(place);
                                user.setPlaceMax(placeMax);
                                VippointEventMessage vpEventMessage = new VippointEventMessage(user.getId(), nickname, vpReal, vpEvent, 0, 0, 0, 0, place, placeMax, 0, 0);
                                RMQApi.publishMessage("queue_vippoint_event", vpEventMessage, 801);
                            }
                        }

                        LogMoneyUserMessage messageLogx = new LogMoneyUserMessage(
                                user.getId(), user.getNickname(), "subtractMoneyInGameExtractly", "MoneyInGame",
                                currentMoney, money, "vin",
                                "Tiền trong game, vinBefore: " + moneyUser
                                        + ", vinAfter: " + user.getMoney(moneyType)
                                        + ", vinTotal before: " + currentMoney
                                        + ", vinTotal after: " + user.getCurrentMoney(moneyType)
                                        + ", change money: " + money
                                , 0L, false, user.isBot());
                        RMQApi.publishMessageLogMoney(messageLogx);

                        LogMoneyUserMessage messageLog = new LogMoneyUserMessage(
                                user.getId(), nickname, gameName, serviceName,
                                currentMoney, money, moneyType, description, fee, playGame, user.isBot());
                        RMQApi.publishMessageLogMoney(messageLog);

                        MoneyMessageInMinigame message = new MoneyMessageInMinigame(
                                VinPlayUtils.genMessageId(), user.getId(), nickname, gameName,
                                moneyUser, currentMoney, money, moneyType, fee, moneyVPs, vp);
                        RMQApi.publishMessagePayment(message, 16);

                        userMap.put(nickname, user);
                        context.commitTransaction();
                        response.setSuccess(true);
                        response.setErrorCode("0");
                    } catch (Exception var38) {
                        context.rollbackTransaction();
                        MoneyLogger.log(nickname, gameName, money, fee, moneyType, serviceName, "1031", "error rmq: " + var38.getMessage());
                        response.setErrorCode("1031");
                    }
                } else {
                    response.setErrorCode("1002");
                    MoneyLogger.log(nickname, gameName, money, fee, moneyType, serviceName, "1002", "khong du tien");
                }
            } else {
                if (moneyType.equals("vin") && transId != null && type.getId() == TransType.END_TRANS.getId()) {
                    IMap vpCache = client.getMap("VPMinigame");
                    String vpCacheId = nickname + gameName + transId;
                    long moneyVP2 = 0L;
                    if (vpCache.containsKey(vpCacheId)) {
                        moneyVP2 = Math.abs((Long) vpCache.get(vpCacheId));
                        vpCache.remove(vpCacheId);
                        if (moneyVP2 > 0L) {
                            List<Integer> vpLst2 = VippointUtils.calculateVP(client, user.getNickname(), (long) user.getMoneyVP() + moneyVP2, false);
                            vpAddEvent = vpLst2.get(0);
                            int moneyVPs2 = vpLst2.get(1);
                            vpReal = vpLst2.get(2);
                            user.setVippoint(user.getVippoint() + vpAddEvent);
                            user.setVippointSave(user.getVippointSave() + vpAddEvent);
                            user.setMoneyVP(moneyVPs2);
                            if (vpReal > 0) {
                                vpEvent = user.getVpEventReal();
                                place = user.getVpEvent();
                                placeMax = VippointUtils.calculatePlace(place += vpReal);
                                int placeMax2 = placeMax > user.getPlace() ? placeMax : user.getPlace();
                                user.setVpEventReal(vpEvent += vpReal);
                                user.setVpEvent(place);
                                user.setPlace(placeMax);
                                user.setPlaceMax(placeMax2);
                                VippointEventMessage vpEventMessage2 = new VippointEventMessage(user.getId(), nickname, vpEvent, place, 0, 0, 0, 0, placeMax, placeMax2, 0, 0);
                                RMQApi.publishMessage("queue_vippoint_event", vpEventMessage2, 801);
                            }

                            VippointMessage message2 = new VippointMessage(user.getId(), nickname, moneyVPs2, vpAddEvent);
                            RMQApi.publishMessagePayment(message2, 18);
                            userMap.put(nickname, user);
                        }
                    }
                }

                response.setSuccess(true);
                response.setErrorCode("0");
            }

            response.setCurrentMoney(currentMoney);
        } catch (Exception var39) {
            logger.debug(var39);
            MoneyLogger.log(nickname, gameName, money, fee, moneyType, serviceName, "1030", "error hazelcast: " + var39.getMessage());
            response.setErrorCode("1030");
        } finally {
            userMap.unlock(nickname);
        }

        logger.debug("Response updateMoneyUser:" + response.toJson());
        return response;
    }

    @Override
    public List<FreezeModel> getListFreeze(
            String gameName, String nickName, String moneyType, String startTime, String endTime, int page, boolean getAllPages) throws ParseException {
        int numStart = (page - 1) * 50;
        int numEnd = numStart + 50;
        HazelcastInstance client = HazelcastClientFactory.getInstance();
        IMap<String, FreezeModel> freezeMap = client.getMap("freeze");

        long longStartTime = 0L;
        long longEndTime = 0L;
        if (!startTime.isEmpty()) {
            longStartTime = VinPlayUtils.getDateTime(startTime).getTime();
        }

        if (!endTime.isEmpty()) {
            longEndTime = VinPlayUtils.getDateTime(endTime).getTime();
        }

        Set<String> xocDiaActiveSessionSet = new HashSet<>();
        if (gameName.isEmpty() || gameName.equals("XocDia")) {
            try {
                XocDiaServiceImpl xocDiaService = new XocDiaServiceImpl();
                // TODO: Check seems it is boss sessions?
                xocDiaActiveSessionSet.addAll(xocDiaService.getListSessionActive());
            } catch (SQLException var21) {
                var21.printStackTrace();
            }
        }

        ArrayList<FreezeModel> filteredFreezeModels = new ArrayList<>();
        int index = 0;
        for (FreezeModel model : freezeMap.values()) {
            if (xocDiaActiveSessionSet.contains(model.getSessionId()))
                continue;
            if (gameName != null && !gameName.isEmpty() && !model.getGameName().equalsIgnoreCase(gameName))
                continue;
            if (nickName != null && !nickName.isEmpty() && !model.getNickname().equals(nickName))
                continue;
            if (moneyType != null && !moneyType.isEmpty() && !model.getMoneyType().equals(moneyType))
                continue;
            if ((longStartTime != 0L || longEndTime != 0L)
                    && (model.getCreateTime().getTime() < longStartTime || model.getCreateTime().getTime() > longEndTime))
                continue;

            if (getAllPages) {
                filteredFreezeModels.add(model);
                continue;
            }

            if (index++ >= numStart && index <= numEnd) {
                filteredFreezeModels.add(model);
                continue;
            }

            if (index > numEnd) break;
        }

        return filteredFreezeModels;
    }

    @Override
    public boolean restoreFreeze(String nickname, String gamename, String startTime, String endTime, long ignoreByLastActiveTimeDurationInSecond) throws ParseException {
        if (!(nickname != null
                && gamename != null
                && startTime != null
                && endTime != null
                && !nickname.isEmpty()
                && !gamename.isEmpty()
                && !startTime.isEmpty()
                && !endTime.isEmpty())) {
            return false;
        }

        if (gamename.equals("*")) {
            return false;
        }

        if (nickname.equals("*")) {
            nickname = "";
        }

        if (startTime.equals("*")) {
            startTime = "";
        }

        if (endTime.equals("*")) {
            endTime = "";
        }

        List<FreezeModel> list = this.getListFreeze(gamename, nickname, "", startTime, endTime, 0, true);
        if (ignoreByLastActiveTimeDurationInSecond != 0) {
            list = list.stream()
                    .filter(freezeModel -> freezeModel.getLastActiveTime() == null || Duration.between(freezeModel.getLastActiveTime(), LocalDateTime.now()).toMillis() > ignoreByLastActiveTimeDurationInSecond * 1000)
                    .collect(Collectors.toList());
        }
        list.forEach(freezeModel -> this.restoreFreeze(freezeModel.getSessionId()));

        return true;
    }

    @Override
    public boolean restoreFreeze(String sessionId) {
        HazelcastInstance client = HazelcastClientFactory.getInstance();
        IMap<String, FreezeModel> freezeMap = client.getMap("freeze");
        if (freezeMap.containsKey(sessionId)) {
            FreezeModel model = freezeMap.get(sessionId);
            this.restoreMoneyInGame(sessionId, model.getNickname(), model.getGameName(), "", model.getMoneyType());
        }

        return true;
    }

    @Override
    public void addVippoint(String nickname, long money, String moneyType) {
        logger.debug("Request addVippoint: nickname: " + nickname + ", money: " + money + ", moneyType: " + moneyType);
        HazelcastInstance client;
        IMap userMap;
        if (moneyType.equals("vin") && (userMap = (client = HazelcastClientFactory.getInstance()).getMap("users")).containsKey(nickname)) {
            try {
                userMap.lock(nickname);
                UserCacheModel user = (UserCacheModel) userMap.get(nickname);
                List<Integer> vpLst = VippointUtils.calculateVP(client, user.getNickname(), (long) user.getMoneyVP() + Math.abs(money), false);
                int vp = vpLst.get(0);
                int moneyVPs = vpLst.get(1);
                int vpAddEvent = vpLst.get(2);
                if (vpAddEvent > 0) {
                    int vpReal = user.getVpEventReal();
                    int vpEvent = user.getVpEvent();
                    int place = VippointUtils.calculatePlace(vpEvent += vpAddEvent);
                    int placeMax = place > user.getPlace() ? place : user.getPlace();
                    user.setVpEventReal(vpReal += vpAddEvent);
                    user.setVpEvent(vpEvent);
                    user.setPlace(place);
                    user.setPlaceMax(placeMax);
                    VippointEventMessage vpEventMessage = new VippointEventMessage(user.getId(), nickname, vpReal, vpEvent, 0, 0, 0, 0, place, placeMax, 0, 0);
                    RMQApi.publishMessage("queue_vippoint_event", vpEventMessage, 801);
                }

                user.setVippoint(user.getVippoint() + vp);
                user.setVippointSave(user.getVippointSave() + vp);
                user.setMoneyVP(moneyVPs);
                MoneyInGameDaoImpl dao = new MoneyInGameDaoImpl();
                if (dao.updateVippoint(nickname, vp, moneyVPs)) {
                    userMap.put(nickname, user);
                }
            } catch (Exception var20) {
                logger.debug(var20);
            } finally {
                userMap.unlock(nickname);
            }
        }

    }

    private boolean hasBuyInByGameName(String gameName) {
        return gameName.equals("Poker") || gameName.equals("Lieng") || gameName.equals("XiTo") || gameName.equals("XocDia");
    }

    static class NotEnoughMoneyRuntimeException extends RuntimeException {
    }
}
