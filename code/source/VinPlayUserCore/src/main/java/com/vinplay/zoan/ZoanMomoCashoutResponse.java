/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.fasterxml.jackson.core.JsonProcessingException
 *  com.fasterxml.jackson.databind.ObjectMapper
 */
package com.vinplay.zoan;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ZoanMomoCashoutResponse {
    private int code;
    private String message;
    private ZoanMomoData data;

    public ZoanMomoCashoutResponse(int _code, String _message) {
        this.code = _code;
        this.message = _message;
    }

    public String toJson() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(this);
        }
        catch (JsonProcessingException mapper) {
            return "{\"code\":500,\"message\":\"error\"}";
        }
    }

    public ZoanMomoData getData() {
        return this.data;
    }

    public void setData(ZoanMomoData data) {
        this.data = data;
    }

    public int getCode() {
        return this.code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public class ZoanMomoData {
        public String partner_reference;
        public String server_reference;
        public String trans_time;
        public String target_acc_no;
        public String target_acc_name;
        public String transfer_id;
        public String transfer_message;
    }
}

