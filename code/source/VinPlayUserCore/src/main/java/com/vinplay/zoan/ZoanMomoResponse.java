/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.fasterxml.jackson.core.JsonProcessingException
 *  com.fasterxml.jackson.databind.ObjectMapper
 */
package com.vinplay.zoan;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;

public class ZoanMomoResponse {
    private int code;
    private String message;
    private ZoanMomoData data;

    public ZoanMomoResponse(int _code, String _message) {
        this.code = _code;
        this.message = _message;
    }

    public String toJson() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(this);
        }
        catch (JsonProcessingException mapper) {
            return "{\"code\":500,\"message\":\"error\"}";
        }
    }

    public int getCode() {
        return this.code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ZoanMomoData getData() {
        return this.data;
    }

    public void setData(ZoanMomoData data) {
        this.data = data;
    }

    public class ZoanMomoAccount {
        private String phone;
        private int isMaxquota;
        private String name;

        public String getPhone() {
            return this.phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public int getIsMaxquota() {
            return this.isMaxquota;
        }

        public void setIsMaxquota(int isMaxquota) {
            this.isMaxquota = isMaxquota;
        }

        public String getName() {
            return this.name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public class ZoanMomoData {
        private String partnerName;
        private List<ZoanMomoAccount> accounts;

        public String getPartnerName() {
            return this.partnerName;
        }

        public void setPartnerName(String partnerName) {
            this.partnerName = partnerName;
        }

        public List<ZoanMomoAccount> getAccounts() {
            return this.accounts;
        }

        public void setAccounts(List<ZoanMomoAccount> accounts) {
            this.accounts = accounts;
        }
    }
}

