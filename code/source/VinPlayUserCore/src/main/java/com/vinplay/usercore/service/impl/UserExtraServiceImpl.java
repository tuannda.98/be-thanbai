/*
 * Decompiled with CFR 0.150.
 *
 * Could not load the following classes:
 *  com.hazelcast.core.HazelcastInstance
 *  com.hazelcast.core.IMap
 *  com.vinplay.vbee.common.hazelcast.HazelcastClientFactory
 *  com.vinplay.vbee.common.models.cache.UserExtraInfoModel
 */
package com.vinplay.usercore.service.impl;

import casio.king365.core.HCMap;
import com.hazelcast.core.IMap;
import com.vinplay.usercore.service.UserExtraService;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.models.cache.UserExtraInfoModel;

public class UserExtraServiceImpl
        implements UserExtraService {

    @Override
    public UserExtraInfoModel getModelFromToken(String accessToken) {
        IMap<String, String> tokenMap = HCMap.getTokenMap();
        if (accessToken == null || accessToken.isEmpty() || !tokenMap.containsKey(accessToken)) {
            return null;
        }

        String nickname = tokenMap.get(accessToken);
        IMap<String, UserExtraInfoModel> userExtraMap = HCMap.getUserExtraInfoMap();
        if (userExtraMap.containsKey(nickname)) {
            return userExtraMap.get(nickname);
        }
        return null;
    }

    @Override
    public String getPlatformFromToken(String accessToken) {
        UserExtraInfoModel model = this.getModelFromToken(accessToken);
        if (model != null) {
            return model.getPlatfrom();
        }
        return "wp";
    }

    @Override
    public void cacheUserExtraInfo(UserCacheModel userCache, String platform) {
        IMap<String, UserExtraInfoModel> userExtraMap = HCMap.getUserExtraInfoMap();
        userExtraMap.put(userCache.getNickname(), new UserExtraInfoModel(userCache.getNickname(), platform));
    }

    @Override
    public String getPlatformFromNickname(String nickname) {
        IMap<String, UserExtraInfoModel> userExtraMap = HCMap.getUserExtraInfoMap();
        UserExtraInfoModel model = userExtraMap.get(nickname);
        if(model == null)
            return null;
        return model.getPlatfrom();
    }
}

