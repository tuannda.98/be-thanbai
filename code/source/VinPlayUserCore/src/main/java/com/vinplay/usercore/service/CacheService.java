/*
 * Decompiled with CFR 0.150.
 *
 * Could not load the following classes:
 *  com.vinplay.vbee.common.exceptions.KeyNotFoundException
 */
package com.vinplay.usercore.service;

import com.vinplay.vbee.common.exceptions.KeyNotFoundException;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public interface CacheService {

    final class CacheServiceKey<T> {
        public final String key;

        public CacheServiceKey(String key) {
            this.key = key;
        }
    }

    @Deprecated
    void setValue(String var1, String var2);

    @Deprecated
    void setValue(String var1, int var2);

    @Deprecated
    String getValueStr(String var1) throws KeyNotFoundException;

    @Deprecated
    int getValueInt(String var1) throws KeyNotFoundException, NumberFormatException;

    @Deprecated
    boolean removeKey(String var1) throws KeyNotFoundException;

    @Deprecated
    void setObject(String var1, Object var2);

    @Deprecated
    void setObject(String var1, int var2, Object var3);

    @Deprecated
    Object getObject(String var1) throws KeyNotFoundException;

    @Deprecated
    Object removeObject(String var1) throws KeyNotFoundException;

    @Deprecated
    Map<String, Object> getBulk(Set<String> var1);

    @Deprecated
    Object getCachedObject(String var1, Function<String, Object> callback);

    <T> void setObject(CacheServiceKey<T> key, T value);

    <T> void setObject(CacheServiceKey<T> key, T value, long timeToLive, TimeUnit timeUnit);

    <T> T getObject(CacheServiceKey<T> key) throws KeyNotFoundException;

    <T> T getCachedObject(CacheServiceKey<T> key, Function<String, T> generator);
}

