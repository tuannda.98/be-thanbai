package com.vinplay.dichvuthe.service.impl;

import casio.king365.core.HCMap;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.vinplay.channel.TelegramUtil;
import com.vinplay.dichvuthe.client.VinplayClient;
import com.vinplay.dichvuthe.entities.TelegramBotConfig;
import com.vinplay.dichvuthe.service.AlertService;
import com.vinplay.dichvuthe.service.LogSMSDAOImpl;
import com.vinplay.usercore.dao.impl.UserDaoImpl;
import com.vinplay.usercore.utils.PartnerConfig;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.utils.VinPlayUtils;
import misa.api.handle.TeleHandle;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class AlertServiceImpl
        implements AlertService {
    private static final Logger logger = Logger.getLogger("api");

    @Override
    public boolean SendSMSEsms(String phone, String messge) {
        try {
            String messageText = URLEncoder.encode(messge, "UTF-8");
            String surl = "http://rest.esms.vn/MainService.svc/json/SendMultipleMessage_V4_get?Phone=" + phone + "&Content=" + messageText + "&ApiKey=" + PartnerConfig.ESMSApiKey + "&SecretKey=" + PartnerConfig.ESMSSecretKey + "&IsUnicode=0&brandname=Verify&SmsType=2";
            URL url = new URL(surl);
            HttpURLConnection request = (HttpURLConnection) url.openConnection();
            request.setConnectTimeout(90000);
            request.setUseCaches(false);
            request.setDoOutput(true);
            request.setDoInput(true);
            HttpURLConnection.setFollowRedirects(true);
            request.setInstanceFollowRedirects(true);
            request.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            request.setRequestMethod("GET");
            String result;
            try (BufferedReader rd = new BufferedReader(new InputStreamReader(request.getInputStream()))) {
                result = "";
                String line = "";
                while ((line = rd.readLine()) != null) {
                    result = result.concat(line);
                }
            }
            logger.debug("json send sms:" + result);
            if (result.contains("<CodeResult>")) {
                LogSMSDAOImpl log = new LogSMSDAOImpl();
                if (result.contains("<CodeResult>100</CodeResult>")) {
                    log.saveLog(VinPlayUtils.genTransactionId(6688), phone, messge, "1", VinPlayUtils.getCurrentDateTime(), "OTP");
                    return true;
                }
                log.saveLog(VinPlayUtils.genTransactionId(6688), phone, messge, "0", VinPlayUtils.getCurrentDateTime(), "OTP");
                return false;
            }
            JSONObject json = (JSONObject) new JSONParser().parse(result);
            boolean rs = json != null && json.get("CodeResult") != null && String.valueOf(json.get("CodeResult")).equals("100");
            LogSMSDAOImpl log = new LogSMSDAOImpl();
            if (rs) {
                log.saveLog(VinPlayUtils.genTransactionId(6688), phone, messge, "1", VinPlayUtils.getCurrentDateTime(), "OTP");
            } else {
                log.saveLog(VinPlayUtils.genTransactionId(6688), phone, messge, "0", VinPlayUtils.getCurrentDateTime(), "OTP");
            }
            return rs;
        } catch (Exception ex) {
            logger.debug("ex:" + ex.getMessage());
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw);
            String sStackTrace = sw.toString();
            logger.debug("trace:" + sStackTrace);
            return false;
        }
    }

    @Override
    public boolean SendSMSRutCuoc(String phone, String messge) {
        try {
            String messageText = URLEncoder.encode(messge, "UTF-8");
            String surl = "http://rutcuoc.com:10009/api/NTH/RegCharge?apiKey=" + PartnerConfig.RutCuocApiKey + "&type=sms&phoneNum=" + phone + "&content=" + messageText;
            URL url = new URL(surl);
            HttpURLConnection request = (HttpURLConnection) url.openConnection();
            request.setConnectTimeout(90000);
            request.setUseCaches(false);
            request.setDoOutput(true);
            request.setDoInput(true);
            HttpURLConnection.setFollowRedirects(true);
            request.setInstanceFollowRedirects(true);
            request.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            request.setRequestMethod("GET");
            String result;
            try (BufferedReader rd = new BufferedReader(new InputStreamReader(request.getInputStream()))) {
                result = "";
                String line = "";
                while ((line = rd.readLine()) != null) {
                    result = result.concat(line);
                }
            }
            logger.debug("json send sms:" + result);
            LogSMSDAOImpl log = new LogSMSDAOImpl();
            log.saveLog(VinPlayUtils.genTransactionId(6688), phone, messge, "1", VinPlayUtils.getCurrentDateTime(), "OTP");
            return true;
        } catch (Exception ex) {
            logger.debug("ex:" + ex.getMessage());
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw);
            String sStackTrace = sw.toString();
            logger.debug("trace:" + sStackTrace);
            return false;
        }
    }

    @Override
    public void SendOtpTelegram(String nickname, String otp){
        TeleHandle.sendMessageTeleToUser(nickname, "Mã OTP của bạn là: "+otp);
    }

    @Override
    public boolean sendSMS2One(String mobile, String content, boolean call) {
        try {
            String messageText = URLEncoder.encode(content, "UTF-8");
            String surl = "http://rest.esms.vn/MainService.svc/json/SendMultipleMessage_V4_get?Phone=" + mobile + "&Content=" + messageText + "&ApiKey=" + PartnerConfig.ESMSApiKey + "&SecretKey=" + PartnerConfig.ESMSSecretKey + "&IsUnicode=0&brandname=Verify&SmsType=2";
            URL url = new URL(surl);
            HttpURLConnection request = (HttpURLConnection) url.openConnection();
            request.setConnectTimeout(90000);
            request.setUseCaches(false);
            request.setDoOutput(true);
            request.setDoInput(true);
            HttpURLConnection.setFollowRedirects(true);
            request.setInstanceFollowRedirects(true);
            request.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            request.setRequestMethod("GET");
            String result;
            try (BufferedReader rd = new BufferedReader(new InputStreamReader(request.getInputStream()))) {
                result = "";
                String line = "";
                while ((line = rd.readLine()) != null) {
                    result = result.concat(line);
                }
            }
            logger.debug("json send sms:" + result);
            logger.debug("json send sms:" + result);
            if (result.contains("<CodeResult>")) {
                LogSMSDAOImpl log = new LogSMSDAOImpl();
                if (result.contains("<CodeResult>100</CodeResult>")) {
                    log.saveLog(VinPlayUtils.genTransactionId(6688), mobile, content, "1", VinPlayUtils.getCurrentDateTime(), "OTP");
                    return true;
                }
                log.saveLog(VinPlayUtils.genTransactionId(6688), mobile, content, "0", VinPlayUtils.getCurrentDateTime(), "OTP");
                return false;
            }
            JSONObject json = (JSONObject) new JSONParser().parse(result);
            boolean rs = json != null && json.get("CodeResult") != null && String.valueOf(json.get("CodeResult")).equals("100");
            LogSMSDAOImpl log = new LogSMSDAOImpl();
            if (rs) {
                log.saveLog(VinPlayUtils.genTransactionId(6688), mobile, content, "1", VinPlayUtils.getCurrentDateTime(), "OTP");
            } else {
                log.saveLog(VinPlayUtils.genTransactionId(6688), mobile, content, "0", VinPlayUtils.getCurrentDateTime(), "OTP");
            }
            return rs;
        } catch (Exception ex) {
            logger.debug("ex:" + ex.getMessage());
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw);
            String sStackTrace = sw.toString();
            logger.debug("trace:" + sStackTrace);
            return false;
        }
    }

    @Override
    public boolean alert2List(List<String> receives, String content, boolean call) {
        return this.alert(receives, content, call);
    }

    @Override
    public boolean alert2One(String mobile, String content, boolean call) {
        ArrayList<String> receives = new ArrayList<String>();
        receives.add(mobile);
        return this.alert(receives, content, call);
    }

    private boolean alert(List<String> receives, String content, boolean call) {
        try {
            VinplayClient.aleft(receives, content, call);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug(e);
            return false;
        }
    }

    private String revertMobile(String mobile) {
        if (mobile.charAt(0) == '0') {
            return "84" + mobile.substring(1);
        }
        return mobile;
    }

    private String revertMobileBegin84(String mobile) {
        if (mobile.charAt(0) == '0') {
            return "84" + mobile.substring(1);
        }
        return mobile;
    }

    private String revertMobileBegin0(String mobile) {
        if (mobile.startsWith("84")) {
            return "0" + mobile.substring(2);
        }
        return mobile;
    }

    @Override
    public boolean sendEmail(String subject, String content, List<String> receives) {
        try {
            VinplayClient.sendEmail(subject, content, receives);
        } catch (Exception e) {
            logger.debug(e);
        }
        return true;
    }

    @Override
    public boolean sendSMS2User(String username, String content) {
        String mobile;
        UserCacheModel model;
        HazelcastInstance client = HazelcastClientFactory.getInstance();
        IMap userMap = client.getMap("users");
        if (userMap.containsKey(username) && (model = (UserCacheModel) userMap.get(username)) != null && model.isHasMobileSecurity() && (mobile = model.getMobile()) != null && !mobile.isEmpty()) {
            try {
                return this.sendSMS2One(mobile, content, false);
            } catch (Exception exception) {
                // empty catch block
            }
        }
        return false;
    }

    @Override
    public void sendCustomerService(String message) {
        TelegramBotConfig config = TelegramBotConfig.defaultConfig();
        if (config == null || config.getAdminIds() == null) return;
        for (String telegramId : config.getAdminIds()) {
            try {
                TelegramUtil.asyncSendMessage(config.getToken(), telegramId, message);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void sendOperation(String message) {
        TelegramBotConfig config = TelegramBotConfig.defaultConfig();
        if (config == null || config.getOperationIds() == null) return;
        for (String telegramId : config.getOperationIds()) {
            try {
                TelegramUtil.asyncSendMessage(config.getToken(), telegramId, message);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void sendClientErrorMonitor(String message) {
        TelegramBotConfig config = TelegramBotConfig.defaultConfig();
        if (config == null || config.getClientErrorMonitorIds() == null) return;
        for (String telegramId : config.getClientErrorMonitorIds()) {
            try {
                TelegramUtil.asyncSendMessage(config.getToken(), telegramId, message);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void sendPublicGroup(String message) {
        TelegramBotConfig config = TelegramBotConfig.defaultConfig();
        if (config == null || config.getPublicGroupIds() == null) return;
        for (String telegramId : config.getPublicGroupIds()) {
            try {
                TelegramUtil.asyncSendMessage(config.getToken(), telegramId, message);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

