package com.vinplay.vbee.common.cp;

public interface BaseProcessor<T, R> {
    R execute(Param<T> var1) throws Exception;

    default int getShouldWarningProcessingTimeInMillisecond() {
        return 300;
    }

    default int getShouldAlertProcessingTimeInMillisecond() {
        return 2000;
    }
}

