/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.khothe.models;

public class BankWithdrawReq {
    public String partner_id;
    public String partner_reference;
    public String telco;
    public int denomination;
    public int quantity;
    public String request_acc_no;
    public String request_acc_name;
    public String request_desc;
    public String request_bank_code;
    public String signature;
}

