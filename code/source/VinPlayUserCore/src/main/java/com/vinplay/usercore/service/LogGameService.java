/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.vinplay.vbee.common.messages.LogGameMessage
 */
package com.vinplay.usercore.service;

import com.vinplay.vbee.common.messages.LogGameMessage;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

public interface LogGameService {
    boolean saveLogGameByNickName(LogGameMessage var1) throws IOException, TimeoutException, InterruptedException;

    boolean saveLogGameDetail(LogGameMessage var1) throws IOException, TimeoutException, InterruptedException;
}

