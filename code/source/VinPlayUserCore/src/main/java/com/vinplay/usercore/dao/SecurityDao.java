/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.vinplay.vbee.common.models.UserModel
 */
package com.vinplay.usercore.dao;

import com.vinplay.vbee.common.models.UserModel;
import java.sql.SQLException;
import java.text.ParseException;

public interface SecurityDao {
    boolean updateUserInfo(int var1, String var2, int var3) throws SQLException;

    boolean updateUserInfos(int var1, String var2, String var3, String var4) throws SQLException;

    boolean updateClient(int var1, String var2) throws SQLException;

    boolean updateUserVipInfo(int var1, String var2, boolean var3, String var4) throws SQLException, ParseException;

    boolean checkEmail(String var1) throws SQLException;

    boolean checkMobile(String var1) throws SQLException;

    boolean checkIdentification(String var1) throws SQLException;

    UserModel getStatus(String var1) throws SQLException;

    boolean updateNewMobile(String var1, String var2, String var3);

    boolean checkNewMobile(String var1);

    boolean updateInfoLogin(int var1, String var2, String var3, String var4, String var5, int var6);
}

