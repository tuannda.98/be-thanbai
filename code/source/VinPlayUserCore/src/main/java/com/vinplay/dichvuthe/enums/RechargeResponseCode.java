package com.vinplay.dichvuthe.enums;

import com.vinplay.dichvuthe.response.RechargeResponse;

import java.util.Arrays;

public enum RechargeResponseCode {
    SUCCESS(0, "Success"),
    UNKNOWN(1, "Unknown"),
    ERROR(2, "Error"),
    PENDING_CARD(30, "Pending card"),
    INVALID_PIN(36, "Invalid pin"),
    INVALID_SERIAL(35, "Invalid serial"),
    PAYMENT_GATEWAY_FAILED(500, "Charge failed"),
    INVALID_TRANS_STATUS(100, "Invalid trans status"),
    CANNOT_FIND_USER(101, "Cannot find user by nickname");

    private final int code;
    private final String name;

    RechargeResponseCode(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public RechargeResponse buildRes() {
        return RechargeResponse.fail(this);
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static RechargeResponseCode getByCode(int id) {
        return Arrays.stream(values())
                .filter(dvtCode -> dvtCode.code == id)
                .findFirst()
                .orElse(UNKNOWN);
    }
}
