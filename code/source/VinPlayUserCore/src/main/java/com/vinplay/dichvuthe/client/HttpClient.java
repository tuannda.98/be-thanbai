/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.dichvuthe.client;

import com.google.common.base.Charsets;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class HttpClient {
    public static final int TIME_OUT = 60000;

    public static String get(String uri) throws Exception {
        int c;
        URL url = new URL(uri);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setDoOutput(true);
        conn.setConnectTimeout(60000);
        StringBuffer response;
        try (InputStream inputStream = conn.getInputStream();
             InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charsets.UTF_8);
             BufferedReader in = new BufferedReader(inputStreamReader)) {
            response = new StringBuffer();
            while ((c = ((Reader) in).read()) >= 0) {
                response.append((char) c);
            }
        }
        conn.disconnect();
        return response.toString();
    }

    public static String post(String uri, String postData) throws Exception {
        int c;
        URL url = new URL(uri);
        byte[] postDataBytes = postData.getBytes(StandardCharsets.UTF_8);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        conn.setDoOutput(true);
        conn.setConnectTimeout(60000);
        StringBuffer response;
        try (OutputStream outputStream = conn.getOutputStream()) {
            outputStream.write(postDataBytes);
            outputStream.flush();
            try (InputStream inputStream = conn.getInputStream();
                 InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charsets.UTF_8);
                 BufferedReader in = new BufferedReader(inputStreamReader)) {
                response = new StringBuffer();
                while ((c = ((Reader) in).read()) >= 0) {
                    response.append((char) c);
                }
            }
        }
        conn.disconnect();
        return response.toString();
    }
}

