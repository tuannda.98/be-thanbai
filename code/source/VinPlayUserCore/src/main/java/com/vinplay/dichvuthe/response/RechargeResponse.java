/*
 * Decompiled with CFR 0.150.
 *
 * Could not load the following classes:
 *  com.fasterxml.jackson.core.JsonProcessingException
 *  com.fasterxml.jackson.databind.ObjectMapper
 */
package com.vinplay.dichvuthe.response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vinplay.dichvuthe.enums.RechargeResponseCode;

public class RechargeResponse {
    private int code;
    private String description;
    private long currentMoney;
    private int fail;
    private long time;
    private String tid;

    public RechargeResponse(int code, long currentMoney, int fail, long time) {
        this.code = code;
        this.currentMoney = currentMoney;
        this.fail = fail;
        this.time = time;
    }

    public static RechargeResponse of(int code, long currentMoney, int fail, long time) {
        return new RechargeResponse(code, currentMoney, fail, time);
    }

    public static RechargeResponse success(long currentMoney) {
        RechargeResponse rechargeResponse = new RechargeResponse(
                RechargeResponseCode.SUCCESS.getCode(), currentMoney, 0, System.currentTimeMillis());
        return rechargeResponse;
    }

    public static RechargeResponse fail(String description) {
        RechargeResponse rechargeResponse = new RechargeResponse(
                RechargeResponseCode.UNKNOWN.getCode(), 0, 0, System.currentTimeMillis());
        rechargeResponse.setDescription(description);
        return rechargeResponse;
    }

    public static RechargeResponse fail(RechargeResponseCode code) {
        RechargeResponse rechargeResponse = new RechargeResponse(
                code.getCode(), 0, 0, System.currentTimeMillis());
        rechargeResponse.setDescription(code.getName());
        return rechargeResponse;
    }

    public int getCode() {
        return this.code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public long getCurrentMoney() {
        return this.currentMoney;
    }

    public void setCurrentMoney(long currentMoney) {
        this.currentMoney = currentMoney;
    }

    public int getFail() {
        return this.fail;
    }

    public void setFail(int fail) {
        this.fail = fail;
    }

    public long getTime() {
        return this.time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getTid() {
        return this.tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String toJson() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            return "";
        }
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isSuccess() {
        return code == RechargeResponseCode.SUCCESS.getCode();
    }

    @Override
    public String toString() {
        return "RechargeResponse{" +
                "code=" + code +
                ", description='" + description + '\'' +
                ", currentMoney=" + currentMoney +
                ", fail=" + fail +
                ", time=" + time +
                ", tid='" + tid + '\'' +
                '}';
    }
}

