package com.vinplay.dichvuthe.enums;

import com.vinplay.dichvuthe.response.RechargeResponse;

import java.util.Arrays;

public enum WithdrawResponseCode {
    UNKNOWN(0, "UNKNOWN"),
    SUCCESS(1, "Success"),
    PENDING(30, "Pending card"),
    FAILURE(99, "Failure on gateway oepration");

    private final int code;
    private final String name;

    WithdrawResponseCode(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static WithdrawResponseCode getByCode(int id) {
        return Arrays.stream(values())
                .filter(dvtCode -> dvtCode.code == id)
                .findFirst()
                .orElse(UNKNOWN);
    }
}
