package com.vinplay.usercore.dao.impl;

import com.vinplay.usercore.dao.GameBaiDao;
import com.vinplay.vbee.common.pools.ConnectionPool;

import java.sql.*;

public class GameBaiDaoImpl
        implements GameBaiDao {
    @Override
    public String getString(String key) throws SQLException {
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_gamebai");
             PreparedStatement stm = conn.prepareStatement("SELECT `value` FROM game_data WHERE `key`=?");) {
            stm.setString(1, key);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    return rs.getString("value");
                }
            }
        }
        return null;
    }

    @Override
    public boolean saveString(String key, String value) throws SQLException {
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_gamebai");
             CallableStatement call = conn.prepareCall("CALL save_game_data(?,?)");) {

            int param = 1;
            call.setString(param++, key);
            call.setString(param++, value);
            call.executeUpdate();
            return true;
        }
    }
}

