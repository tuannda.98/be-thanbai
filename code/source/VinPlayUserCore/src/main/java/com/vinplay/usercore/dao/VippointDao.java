/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.vinplay.vbee.common.models.vippoint.EventVPBonusModel
 *  com.vinplay.vbee.common.models.vippoint.UserVPEventModel
 */
package com.vinplay.usercore.dao;

import com.vinplay.vbee.common.models.vippoint.EventVPBonusModel;
import com.vinplay.vbee.common.models.vippoint.UserVPEventModel;
import com.vinplay.vippoint.entiies.EventVPMapModel;
import com.vinplay.vippoint.entiies.EventVPTopIntelModel;
import com.vinplay.vippoint.entiies.EventVPTopStrongModel;
import com.vinplay.vippoint.entiies.EventVPTopVipModel;
import java.sql.SQLException;
import java.util.List;

public interface VippointDao {
    List<EventVPBonusModel> getEventVPBonus() throws SQLException;

    boolean updateEventVPBonus(int var1) throws SQLException;

    int getVPEvent(String var1) throws SQLException;

    int getVPEventReal(String var1) throws SQLException;

    List<EventVPMapModel> getEventMaps() throws SQLException;

    List<EventVPTopIntelModel> getEventIntel() throws SQLException;

    List<EventVPTopStrongModel> getEventStrong() throws SQLException;

    List<EventVPTopVipModel> getEventVips() throws SQLException;

    UserVPEventModel getUserVPByNickName(String var1) throws SQLException;

    int getEventIntelIndex(int var1, int var2, int var3) throws SQLException;

    int getEventStrongIndex(int var1, int var2, int var3) throws SQLException;

    int getEventVipsIndex(int var1) throws SQLException;

    boolean logVippointEvent(String var1, int var2, int var3, int var4);

    int getNumRunInDay(String var1, int var2) throws SQLException;

    boolean updateNumInDay(String var1, int var2) throws SQLException;

    boolean resetEvent() throws SQLException;
}

