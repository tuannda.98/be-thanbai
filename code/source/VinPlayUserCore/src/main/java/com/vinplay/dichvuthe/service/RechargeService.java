package com.vinplay.dichvuthe.service;

import com.vinplay.dichvuthe.response.RechargeIAPResponse;
import com.vinplay.dichvuthe.response.RechargeResponse;
import com.vinplay.vbee.common.enums.ProviderType;

public interface RechargeService {

    byte checkRechargeIAP(String var1, int var2);

    RechargeIAPResponse rechargeIAP(String var1, String var2, String var3);

    RechargeResponse rechargeByIzPay(
            String nickname, ProviderType provider, String serial, String pin,
            int amount, String platform, int UserId) throws Exception;

    RechargeResponse receiveResultFromIzpay(String orderId, String uuid, String cardCode, String cardSerial, String status, String amount, String ipAddress);

    void receiveResultFromZoanMomo(String var1, String ip) throws Exception;

    void receiveResultFromZoanBank(String transId, String bankNo, String bankName, String bankCode, String amount, String comment, String nickname, String transTime, String signature) throws Exception;

    void receiveResultFromCoinGate(String nickname, String amount, String trx_id, String address, String fromAddress,String token, String tokenType, String signature, String status, long transTime) throws Exception;

    boolean updateUserClient(String nickName, String client);

    boolean updateUserPlatform(String nickName, String pf);
}

