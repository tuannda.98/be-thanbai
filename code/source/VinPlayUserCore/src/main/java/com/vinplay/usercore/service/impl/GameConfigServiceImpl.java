/*
 * Decompiled with CFR 0.150.
 *
 * Could not load the following classes:
 *  com.vinplay.vbee.common.response.ResultGameConfigResponse
 */
package com.vinplay.usercore.service.impl;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.vinplay.usercore.dao.impl.GameConfigDaoImpl;
import com.vinplay.usercore.service.GameConfigService;
import com.vinplay.vbee.common.response.ResultGameConfigResponse;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class GameConfigServiceImpl
        implements GameConfigService {
    private static final LoadingCache<ConfigKey, List<ResultGameConfigResponse>> resultGameConfigResponseCache
            = CacheBuilder.newBuilder()
            .expireAfterAccess(60, TimeUnit.SECONDS)
            .build(new CacheLoader<ConfigKey, List<ResultGameConfigResponse>>() {
                @Override
                public List<ResultGameConfigResponse> load(ConfigKey configKey) throws Exception {
                    GameConfigDaoImpl dao = new GameConfigDaoImpl();
                    return dao.getGameConfigAdmin(configKey.name, configKey.platform);
                }
            });
    private static final LoadingCache<String, Map<String, String>> gameConfigCache
            = CacheBuilder.newBuilder()
            .expireAfterAccess(60, TimeUnit.SECONDS)
            .build(new CacheLoader<String, Map<String, String>>() {
                @Override
                public Map<String, String> load(String s) throws Exception {
                    GameConfigDaoImpl dao = new GameConfigDaoImpl();
                    return dao.getGameConfig();
                }
            });

    final static class ConfigKey implements Serializable {
        private final String name;
        private final String platform;

        private ConfigKey(String name, String platform) {
            this.name = name;
            this.platform = platform;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            ConfigKey configKey = (ConfigKey) o;
            return Objects.equals(name, configKey.name) && Objects.equals(platform, configKey.platform);
        }

        @Override
        public int hashCode() {
            return Objects.hash(name, platform);
        }
    }

    @Override
    public Map<String, String> getGameConfig() throws SQLException {
        try {
            return gameConfigCache.get("GameConfig");
        } catch (ExecutionException e) {
            GameConfigDaoImpl dao = new GameConfigDaoImpl();
            return dao.getGameConfig();
        }
    }

    @Override
    public List<ResultGameConfigResponse> getGameConfigAdmin(String name, String platform) throws SQLException {
        try {
            return resultGameConfigResponseCache.get(new ConfigKey(name, platform));
        } catch (ExecutionException e) {
            GameConfigDaoImpl dao = new GameConfigDaoImpl();
            return dao.getGameConfigAdmin(name, platform);
        }
    }

    @Override
    public boolean createGameConfig(String name, String value, String version, String platform) throws SQLException {
        GameConfigDaoImpl dao = new GameConfigDaoImpl();
        return dao.createGameConfig(name, value, version, platform);
    }

    @Override
    public boolean updateGameConfig(String id, String value, String version, String platform) throws SQLException {
        resultGameConfigResponseCache.invalidate(new ConfigKey(value, platform));
        GameConfigDaoImpl dao = new GameConfigDaoImpl();
        return dao.updateGameConfig(id, value, version, platform);
    }
}

