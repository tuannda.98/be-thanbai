//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.vinplay.usercore.dao.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.vinplay.usercore.dao.MailBoxDao;
import com.vinplay.vbee.common.models.MailBoxModel;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.response.MailBoxResponse;
import com.vinplay.vbee.common.utils.VinPlayUtils;
import org.apache.log4j.Logger;
import org.bson.Document;

import java.sql.SQLException;
import java.util.*;

public class MailBoxDaoImpl implements MailBoxDao {
    private static final Logger logger = Logger.getLogger("api");

    private int flag = 0;

    public MailBoxDaoImpl() {
    }

    public boolean sendMailBoxFromByNickName(List<String> nickName, String title, String content) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("mail_box");
        Iterator var6 = nickName.iterator();

        while (var6.hasNext()) {
            String name = (String) var6.next();
            Document doc = new Document();
            doc.append("mail_id", String.valueOf(System.currentTimeMillis()));
            doc.append("nick_name", name);
            doc.append("title", title);
            doc.append("content", content);
            doc.append("create_time", VinPlayUtils.getCurrentDateTime());
            doc.append("author", "Hệ thống");
            doc.append("status", 0);
            doc.append("created", new Date());
            col.insertOne(doc);
        }

        return true;
    }

    public boolean sendMailBoxFromByNickNameAdmin(String nickName, String title, String content) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("mail_box");
        MailBoxModel model = MailBoxModel.builder()
                .setNickName(nickName)
                .setTitle(title)
                .setContent(content)
                .setType(MailBoxModel.MailBoxType.NORMAL)
                .createMailBoxModel();
        col.insertOne(model.toDoc());
        return true;
    }

    public List<MailBoxResponse> listMailBox(String nickName, int page) {
        final ArrayList<MailBoxResponse> results = new ArrayList();
        int num_start = (page - 1) * 5;
        //int num_end = true;
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        HashMap<String, Object> conditions = new HashMap();
        BasicDBObject objsort = new BasicDBObject();
        conditions.put("nick_name", nickName);
        objsort.put("_id", -1);
        FindIterable iterable = db.getCollection("mail_box").find(new Document(conditions)).skip(num_start).limit(5).sort(objsort);
        iterable.forEach((Block<Document>) document -> {
            results.add(MailBoxModel.fromDoc(document).toMailBoxResponse());
        });
        return results;
    }

    public int updateStatusMailBox(String mailId) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection colmail = db.getCollection("mail_box");
        colmail.updateOne(new Document("mail_id", mailId), new Document("$set", new Document("status", 1)));
        return 0;
    }

    public int deleteMailBox(String mailId) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final BasicDBObject obj = new BasicDBObject();
        obj.put("mail_id", mailId);
        HashMap<String, Object> conditions = new HashMap();
        conditions.put("mail_id", mailId);
        FindIterable<Document> iterable = db.getCollection("mail_box").find(new Document(conditions));
        iterable.forEach((Block<Document>) document -> {
            if (!document.getString("nick_name").equals("*")) {
                db.getCollection("mail_box").deleteOne(obj);
                MailBoxDaoImpl.this.flag = 0;
            } else {
                MailBoxDaoImpl.this.flag = 1;
            }
        });
        return this.flag;
    }

    public int countMailBox(String nickName) {
        int record = 0;
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        HashMap<String, Object> conditions = new HashMap();
        conditions.put("nick_name", nickName);
        record = (int) db.getCollection("mail_box").count(new Document(conditions));
        return record;
    }

    public boolean sendmailGiftCode(String nickName, String giftcode, String title, String type, String price) throws SQLException {
        String content = "";
        if (type.equals("1")) {
            content = content + "Chào bạn " + nickName + "\n";
            content = content + "Chúc mừng bạn đã được tặng Giftcode: " + giftcode + "\n";
            content = content + "Để sử dụng được Gift code bạn hãy kích hoạt số điện thoại bảo mật\n";
            content = content + "Lưu ý: Mỗi tài khoản và số điện thoại chỉ được nhận Giftcode 1 lần \n";
        }

        if (type.equals("2")) {
            content = content + "Chào bạn " + nickName + "\n";
            content = content + "Chúc mừng bạn đã được tặng Giftcode tri ân trị giá " + price + " Vin: " + giftcode + "\n";
            content = content + "Rất cám ơn bạn đã quan tâm và ủng hộ Viplay trong thời gian qua. \n";
            content = content + "Lưu ý: Mỗi tài khoản và số điện thoại chỉ được nhận Giftcode 1 lần \n";
        }

        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("mail_box");

        MailBoxModel model = MailBoxModel.builder()
                .setNickName(nickName)
                .setMailId(giftcode)
                .setMailGiftCode(giftcode)
                .setTitle(title)
                .setContent(content)
                .setType(MailBoxModel.MailBoxType.GIFT_CODE)
                .createMailBoxModel();
        col.insertOne(model.toDoc());
        return true;
    }

    public int countMailBoxInActive(String nickName) throws SQLException {
        int record = 0;
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        HashMap<String, Object> conditions = new HashMap();
        conditions.put("nick_name", nickName);
        conditions.put("status", 0);
        record = (int) db.getCollection("mail_box").count(new Document(conditions));
        return record;
    }

    public boolean sendMailGiftCode(String nickName, String giftcode, String title, String content) throws SQLException {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("mail_box");
        MailBoxModel model = MailBoxModel.builder()
                .setNickName(nickName)
                .setType(MailBoxModel.MailBoxType.GIFT_CODE)
                .setMailGiftCode(giftcode)
                .setTitle(title)
                .setContent(content)
                .createMailBoxModel();
        col.insertOne(model.toDoc());
        return true;
    }

    public int deleteMailBoxAdmin(String mailId) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        final BasicDBObject obj = new BasicDBObject();
        obj.put("mail_id", mailId);
        HashMap<String, Object> conditions = new HashMap();
        conditions.put("mail_id", mailId);
        FindIterable iterable = db.getCollection("mail_box").find(new Document(conditions));
        iterable.forEach((Block<Document>) document -> {
            db.getCollection("mail_box").deleteOne(obj);
            MailBoxDaoImpl.this.flag = 0;
        });
        return this.flag;
    }

    public int deleteMutilMailBox(String nickName, String title) {
        final MongoDatabase db = MongoDBConnectionFactory.getDB();
        HashMap conditions = new HashMap();
        final BasicDBObject obj = new BasicDBObject();
        if (nickName != null && !nickName.equals("") || title != null && !title.equals("")) {
            BasicDBObject query1 = new BasicDBObject("nick_name", nickName);
            BasicDBObject query2 = new BasicDBObject("title", title);
            ArrayList<BasicDBObject> myList = new ArrayList();
            myList.add(query1);
            myList.add(query2);
            conditions.put("$or", myList);
        }

        if (nickName != null && !nickName.equals("")) {
            obj.put("nick_name", nickName);
        }

        if (title != null && !title.equals("")) {
            obj.put("title", title);
        }

        FindIterable iterable = db.getCollection("mail_box").find(new Document(conditions));
        iterable.forEach(new Block<Document>() {
            public void apply(Document document) {
                db.getCollection("mail_box").deleteOne(obj);
                MailBoxDaoImpl.this.flag = 0;
            }
        });
        return this.flag;
    }

    public boolean sendMailCardMobile(String nickName, String provider, String serial, String pin, String title, String content) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("mail_box");
        MailBoxModel model = MailBoxModel.builder()
                .setNickName(nickName)
                .setProvider(provider)
                .setSerial(serial)
                .setPin(pin)
                .setTitle(title)
                .setContent(content)
                .setType(MailBoxModel.MailBoxType.MOBILE_CARD)
                .createMailBoxModel();
        col.insertOne(model.toDoc());
        return true;
    }

    @Override
    public boolean sendMailPopupMessage(String nickName, String title, String content) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("mail_box");
        MailBoxModel model = MailBoxModel.builder()
                .setNickName(nickName)
                .setTitle(title)
                .setContent(content)
                .setType(MailBoxModel.MailBoxType.POPUP)
                .createMailBoxModel();
        col.insertOne(model.toDoc());
        return true;
    }
}
