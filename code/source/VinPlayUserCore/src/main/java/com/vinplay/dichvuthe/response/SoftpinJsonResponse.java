//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.vinplay.dichvuthe.response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SoftpinJsonResponse {
    public String id;
    private String serial;
    private String pin;
    private String expire;
    private String provider;
    private int amount;

    public SoftpinJsonResponse() {
    }

    public SoftpinJsonResponse(String id, String serial, String pin, String expire) {
        this.id = id;
        this.serial = serial;
        this.pin = pin;
        this.expire = expire;
    }

    public SoftpinJsonResponse(String id, String serial, String pin, String expire, int amount, String provider) {
        this.id = id;
        this.serial = serial;
        this.pin = pin;
        this.expire = expire;
        this.amount = amount;
        this.provider = provider;
    }

    public String toJson() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException var2) {
            return "";
        }
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSerial() {
        return this.serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getPin() {
        return this.pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getExpire() {
        return this.expire;
    }

    public void setExpire(String expire) {
        this.expire = expire;
    }

    public String getProvider() {
        return this.provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public int getAmount() {
        return this.amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
