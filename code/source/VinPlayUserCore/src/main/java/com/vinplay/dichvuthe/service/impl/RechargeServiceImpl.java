package com.vinplay.dichvuthe.service.impl;

import casio.king365.Constants;
import casio.king365.GU;
import casio.king365.dto.UpdateUserMoneyMsg;
import casio.king365.mqtt.MqttSender;
import casio.king365.service.MongoDbService;
import casio.king365.service.impl.MongoDbServiceImpl;
import casio.king365.util.KingUtil;
import com.google.gson.Gson;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.vinplay.dichvuthe.dao.RechargeDao;
import com.vinplay.dichvuthe.dao.impl.RechargeDaoImpl;
import com.vinplay.dichvuthe.enums.RechargeResponseCode;
import com.vinplay.dichvuthe.response.RechargeIAPResponse;
import com.vinplay.dichvuthe.response.RechargeResponse;
import com.vinplay.dichvuthe.response.ZoanMomoReq;
import com.vinplay.dichvuthe.service.RechargeService;
import com.vinplay.dichvuthe.utils.DvtUtils;
import com.vinplay.iap.lib.Purchase;
import com.vinplay.iap.lib.Security;
import com.vinplay.usercore.entities.IAPModel;
import com.vinplay.usercore.logger.MoneyLogger;
import com.vinplay.usercore.service.UserService;
import com.vinplay.usercore.service.impl.GameConfigServiceImpl;
import com.vinplay.usercore.service.impl.MailBoxServiceImpl;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.usercore.utils.GameCommon;
import com.vinplay.usercore.utils.PartnerConfig;
import com.vinplay.vbee.common.enums.ProviderType;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.hazelcast.HazelcastUtils;
import com.vinplay.vbee.common.messages.LogMoneyUserMessage;
import com.vinplay.vbee.common.messages.MoneyMessageInMinigame;
import com.vinplay.vbee.common.messages.dvt.RechargeByBankMessage;
import com.vinplay.vbee.common.messages.dvt.RechargeByCardMessage;
import com.vinplay.vbee.common.messages.dvt.RechargeByCoinMessage;
import com.vinplay.vbee.common.messages.socket.SocketMessageFactory;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.response.ResultGameConfigResponse;
import com.vinplay.vbee.common.rmq.RMQApi;
import com.vinplay.vbee.common.utils.UserValidaton;
import com.vinplay.vbee.common.utils.VinPlayUtils;
import misa.data.AccountTeleLink;
import org.apache.log4j.Logger;
import org.bson.Document;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import vn.yotel.payment.CoinPaymentClient;
import vn.yotel.payment.ECardClient;
import vn.yotel.yoker.dao.CashinItemDao;
import vn.yotel.yoker.dao.CashinNotifyDao;
import vn.yotel.yoker.dao.CashoutRequestDao;
import vn.yotel.yoker.dao.impl.CashinItemDaoImpl;
import vn.yotel.yoker.dao.impl.CashinNotifyDaoImpl;
import vn.yotel.yoker.dao.impl.CashoutRequestDaoImpl;
import vn.yotel.yoker.domain.CashinItem;
import vn.yotel.yoker.domain.CashinNotify;

import javax.persistence.PersistenceException;
import java.sql.Timestamp;
import java.util.*;

import static com.vinplay.dichvuthe.enums.RechargeResponseCode.*;

public class RechargeServiceImpl implements RechargeService {
    private static final Logger logger = Logger.getLogger("api");
    private UserService userService;
    private MongoDbService mongodbService = new MongoDbServiceImpl();

    public RechargeServiceImpl() {
        this.userService = new UserServiceImpl();
    }

    @Override
    public RechargeIAPResponse rechargeIAP(String nickname, String signedData, String signature) {
        int code = 1;
        long currentMoney = 0L;
        RechargeIAPResponse res = new RechargeIAPResponse(code, currentMoney, 0);
        try {
            if (signature == null || signedData == null || GameCommon.getValueInt("IS_RECHARGE_IAP") == 1) {
                return res;
            }
            IMap<String, UserCacheModel> usersMap = HazelcastUtils.getUsersMap();
            if (usersMap.containsKey(nickname)) {
                try {
                    usersMap.lock(nickname);
                    String[] split = GameCommon.getValueStr("IAP_KEY").split(",,,");
                    String[] var12 = split;
                    int var13 = split.length;

                    for (int var14 = 0; var14 < var13; ++var14) {
                        String base64PublicKey = var12[var14];
                        if (Security.verifyPurchase(base64PublicKey, signedData, signature)) {
                            String itemType = "";
                            RechargeDaoImpl dao = new RechargeDaoImpl();

                            try {
                                Purchase pc = new Purchase("", signedData, signature);
                                if (pc != null) {
                                    IAPModel iapPK = GameCommon.getIAPPackageByName(pc.getSku());
                                    if (iapPK != null) {
                                        res.setProductId(iapPK.getId());
                                        if (!dao.checkOrderId(pc.getOrderId())) {
                                            UserCacheModel user = (UserCacheModel) usersMap.get(nickname);
                                            Calendar cal = Calendar.getInstance();
                                            long iapUserInDay = dao.getTotalRechargeIapInday(nickname, cal);
                                            cal = Calendar.getInstance();
                                            long iapSystemInDay = dao.getTotalRechargeIapInday("", cal);
                                            if (iapUserInDay + (long) iapPK.getValue() <= (long) GameCommon.getValueInt("IAP_MAX") && iapSystemInDay + (long) iapPK.getValue() <= (long) GameCommon.getValueInt("SYSTEM_IAP_MAX")) {
                                                int money = iapPK.getValue();
                                                long moneyUser = user.getVin();
                                                currentMoney = user.getVinTotal();
                                                res.setCurrentMoney(currentMoney);
                                                long rechargeMoney = user.getRechargeMoney();
                                                int iapInDay = 0;
                                                if (user.getIapTime() != null && VinPlayUtils.compareDate(new Date(), user.getIapTime()) == 0) {
                                                    iapInDay = user.getIapInDay();
                                                }

                                                user.setVin(moneyUser += money);
                                                user.setVinTotal(currentMoney += money);
                                                user.setRechargeMoney(rechargeMoney + (long) money);
                                                user.setIapInDay(iapInDay + money);
                                                user.setIapTime(new Date());
                                                String description = "Káº¿t quáº£: ThÃ nh cÃ´ng, GÃ³i: " + money + " vin.";
                                                MoneyMessageInMinigame messageMoney = new MoneyMessageInMinigame(VinPlayUtils.genMessageId(), user.getId(), nickname, "RechargeByIAP", moneyUser, currentMoney, money, "vin", 0L, 0, 0);
                                                RMQApi.publishMessageInMiniGame(messageMoney);
                                                LogMoneyUserMessage messageLog = new LogMoneyUserMessage(user.getId(), nickname, "RechargeByIAP", "Náº¡p Vin qua Google", currentMoney, money, "vin", description, 0L, false, user.isBot());
                                                RMQApi.publishMessageLogMoney(messageLog);
                                                dao.saveLogIAP(pc, nickname, money, 0, "ThÃ nh cÃ´ng");
                                                usersMap.put(nickname, user);
                                                res.setCurrentMoney(currentMoney);
                                                code = 0;
                                            }
                                        } else {
                                            code = 2;
                                            logger.debug("Order id Ä‘Ã£ tá»“n táº¡i: " + pc.getOrderId());
                                        }
                                    } else {
                                        logger.debug("Product ID khÃ´ng há»£p lá»‡: " + pc.getSku());
                                    }
                                    break;
                                }
                            } catch (Exception var40) {
                                logger.debug("Parse signedData fail: " + signedData);
                            }
                        } else {
                            logger.debug("Purchase verification failed: " + signature);
                        }
                    }
                } catch (Exception var41) {
                    logger.debug(var41);
                } finally {
                    usersMap.unlock(nickname);
                }
            }
        } catch (Exception var43) {
            logger.debug(var43);
        }

        res.setCode(code);
        return res;
    }

    @Override
    public byte checkRechargeIAP(String nickname, int iapPackage) {
        int res = 1;
        try {
            if (GameCommon.getValueInt("IS_RECHARGE_IAP") == 1) {
                return (byte) res;
            }
            IMap<String, UserCacheModel> usersMap = HazelcastUtils.getUsersMap();
            IAPModel iapPK = GameCommon.getIAPPackageById(iapPackage);
            if (iapPK != null && usersMap.containsKey(nickname)) {
                try {
                    usersMap.lock(nickname);
                    RechargeDaoImpl dao = new RechargeDaoImpl();
                    Calendar cal = Calendar.getInstance();
                    long iapUserInDay = dao.getTotalRechargeIapInday(nickname, cal);
                    cal = Calendar.getInstance();
                    long iapSystemInDay = dao.getTotalRechargeIapInday("", cal);
                    res = iapUserInDay + (long) iapPK.getValue() <= (long) GameCommon.getValueInt("IAP_MAX") && iapSystemInDay + (long) iapPK.getValue() <= (long) GameCommon.getValueInt("SYSTEM_IAP_MAX") ? 0 : 2;
                } catch (Exception var17) {
                    logger.debug(var17);
                } finally {
                    usersMap.unlock(nickname);
                }
            }
        } catch (Exception var19) {
            logger.debug(var19);
        }

        return (byte) res;
    }

    @Override
    public RechargeResponse rechargeByIzPay(
            String nickname, ProviderType provider, String serial, String pin,
            int amount, String platform, int UserId) throws Exception {
        logger.debug("Start rechargeByCard Gachthe:  nickname: " + nickname + ", provider: " + provider.getName() + ", serial: " + serial + ", pin: " + pin);
        RechargeResponseCode code = UNKNOWN;
        RechargeResponse res = RechargeResponse.of(code.getCode(), 0L, 0, 0L);
        if (GameCommon.getValueInt("IS_RECHARGE_CARD") == 1 || provider == null) {
            logger.debug("rechargeByCard: param fail");
            return res;
        }

        if (pin != null) {
            pin = pin.trim();
        }

        if (serial != null) {
            serial = serial.trim();
        }

        HazelcastInstance client;
        if ((client = HazelcastClientFactory.getInstance()) == null) {
            MoneyLogger.log(nickname, "RechargeByCard", 0L, 0L
                    , "vin", "Nap qua the"
                    , "1030", "can not connect hazelcast");
            return res;
        }

        IMap<String, UserCacheModel> usersMap = HazelcastUtils.getUsersMap();
        if (!usersMap.containsKey(nickname))
            return res;

        try {
            UserCacheModel user = (UserCacheModel) usersMap.get(nickname);
            res.setCurrentMoney(user.getVinTotal());
            boolean cardFail = false;
            if (!UserValidaton.validateSerialPin(serial)) {
                cardFail = true;
                code = INVALID_PIN;
            }
            if (!UserValidaton.validateSerialPin(pin)) {
                cardFail = true;
                code = INVALID_SERIAL;
            }
            if (cardFail) {
                try {
                    usersMap.lock(nickname);
                    user = (UserCacheModel) usersMap.get(nickname);
                    user.setRechargeFail(user.getRechargeFail() + 1);
                    user.setRechargeFailTime(new Date());
                    usersMap.put(nickname, user);
                } catch (Exception var43) {
                    code = UNKNOWN;
                    logger.debug(var43);
                    MoneyLogger.log(nickname, "RechargeByCard", 0L, 0L
                            , "vin", "Nap qua the"
                            , "1001", var43.getMessage());
                } finally {
                    usersMap.unlock(nickname);
                }
                res.setFail(user.getRechargeFail());
                res.setCode(code.getCode());
                return res;
            }

            // check exists card pending
            RechargeDaoImpl dao = new RechargeDaoImpl();
            {
                Document doc = dao.getRechargeByGachthe(nickname, serial, pin);
                if (doc != null) {
                    // pending card
                    code = PENDING_CARD;
                    res.setFail(user.getRechargeFail());
                    res.setCode(code.getCode());
                    return res;
                }
            }

            String id = VinPlayUtils.genTransactionId(user.getId());
            res.setTid(id);
            // GachTheClient gachTheClient = new GachTheClient();
            // JSONObject result = gachTheClient.doCharge(provider.getValue().toUpperCase(), pin, serial, id, amount);
            ECardClient http = ECardClient.getInstance();
            String rep = http.createCard(id, provider.getValue(), pin, serial, amount);
            logger.debug("rechargeByIzPay-http rep: " + rep);
            Object repObj = JSONValue.parse(rep);
            JSONObject result = (JSONObject) repObj;
            if (!"200".equals(result.get("code").toString())) {
                logger.debug(result);
                code = UNKNOWN;
                res.setFail(user.getRechargeFail());
                res.setCode(code.getCode());
                return res;
            }

            // thẻ được chấp nhận
            user = (UserCacheModel) usersMap.get(nickname);
            long currentMoney = user.getVinTotal();
            long money = Math.round(amount * GameCommon.getValueDouble("RATIO_RECHARGE_CARD"));
            code = PENDING_CARD;
            dao.saveLogRechargeByGachThe(nickname, serial, pin, amount, id
                    , System.currentTimeMillis() + "", code.getCode(), "pending_card",
                    currentMoney, provider.getValue(), platform, currentMoney, money, UserId, user.getUsername(), "izpay", user.getClient());
            res.setFail(user.getRechargeFail());
        } catch (Exception var47) {
            code = UNKNOWN;
            logger.debug(var47);
            MoneyLogger.log(nickname, "RechargeByCard", 0L, 0L, "vin"
                    , "Nap qua the", "1001", var47.getMessage());
            throw var47;
        }

        res.setCode(code.getCode());
        return res;
    }

    @Override
    public RechargeResponse receiveResultFromIzpay(
            String orderId, String uuid, String cardCode, String cardSerial, String status, String amount, String ipAddress) {

        CashinNotifyDao cashinNotifyDao = new CashinNotifyDaoImpl();
        // Save data to db
        CashinNotify cashinNotify = new CashinNotify();
        cashinNotify.setUuid(uuid);
        cashinNotify.setGateType("card");
        cashinNotify.setIp(ipAddress);
        cashinNotify.setReceiveTime(new Timestamp(new Date().getTime()));
        // cashinNotify.setComment(result.getComment());
        String paramStr = "orderId: " + orderId
                + ", uuid: " + uuid + ", cardCode: " + cardCode + ", cardSerial: " + cardSerial + ", status: " + status + ", amount: " + amount;
        cashinNotify.setData(paramStr);
        cashinNotify.setError("true");
        try {
            cashinNotifyDao.save(cashinNotify);
        } catch (PersistenceException e) {
            // Lỗi này xảy ra khi data đã có trong db
            // có nghĩa là callback với trans_id đã được xử lí rồi
            // KingUtil.printLog("BankCallbackProcessor PersistenceException: "+KingUtil.printException(e));
            // Kiểm tra nếu transId này chưa thành công thì cho xử lí,
            // nếu đã thành công thì ignore callback này
            /*(CashinNotify oldCashinNotify = cashinNotifyDao.findById(uuid);
            if(oldCashinNotify.getError().equals("false")) {
                // ignore callback này thôi
                // throw new Exception("Giao dịch đã được xử lí trước đó.");
                return ERROR.buildRes();
            }*/
        }

        // Tính addMoneyAmount
        RechargeDaoImpl dao = new RechargeDaoImpl();
        Document trans = dao.getRechargeByGachthe(orderId);
        if (trans == null) {
            return RechargeResponse.fail("Trans is null");
        }

        if (trans.getInteger("code") == SUCCESS.getCode()) {
            MoneyLogger.log("", "RechargeByCard", 0L, 0L
                    , "vin", "Nap qua the"
                    , "1030", "trans == null || trans.getInteger(\"code\") == PENDING_CARD.getCode()");
            return INVALID_TRANS_STATUS.buildRes();
        }

        // Tìm cashinItem, tính được số tiền cộng cho user
        MailBoxServiceImpl mailBoxService = new MailBoxServiceImpl();
        CashinItemDao cashinItemDao = new CashinItemDaoImpl();
        List<CashinItem> listCashinItem = cashinItemDao.findByProvider(trans.getString("provider"));
        if (listCashinItem == null || listCashinItem.size() == 0) {
            KingUtil.printLogError("listCashinItem == null || listCashinItem.size() == 0");
            return RechargeResponse.fail("Lỗi hệ thống, không tìm thấy cashinItem config");
        }

        CashinItem cashinItem = null;
        for (CashinItem e : listCashinItem) {
            if (e.getMoney().intValue() == Integer.parseInt(amount)) {
                cashinItem = e;
                break;
            }
        }
        if (cashinItem == null) {
            KingUtil.printLogError("cashinItem == null");
            return RechargeResponse.fail("Lỗi hệ thống, không tìm thấy cashinItem config");
        }
//         fields.put("addMoneyAmount", cashinItem.getGold().toString());
        String addMoneyAmount = cashinItem.getGold().toString();
        String nickname = trans.getString("nick_name");
        if (status == null || !"1".equals(status)) {
            RechargeByCardMessage message = new RechargeByCardMessage(nickname
                    , trans.getString("request_id")
                    , trans.getString("provider")
                    , trans.getString("serial")
                    , trans.getString("pin")
                    , 0, -1, "The loi"
                    , -1, 0, null, null
                    , "izpay", trans.getString("platform"), null);
            try {
                RMQApi.publishMessage("queue_dvt", message, 301);
            } catch (Exception e) {
                e.printStackTrace();
            }
            dao.UpdateGachtheTransctions(orderId, 1, "failed");
            return PAYMENT_GATEWAY_FAILED.buildRes();
        }

        UserCacheModel user = userService.getCacheUserIgnoreCase(nickname);
        if (user == null) {
            logger.error("receiveResultFromIzpay: Không tìm thấy user " + nickname);
            GU.sendCustomerService("Có lỗi khi nạp CARD, Không tìm thấy người dùng. info: " + trans.toJson());
            return CANNOT_FIND_USER.buildRes();
        }
        double cardAmount = Double.parseDouble(amount);
        RechargeByCardMessage message = new RechargeByCardMessage(user.getNickname()
                , trans.getString("request_id")
                , trans.getString("provider")
                , trans.getString("serial")
                , trans.getString("pin")
                , (int) cardAmount
                , 1
                , "Thanh cong"
                , 1, (int) cardAmount
                , null, null
                , "izpay"
                , trans.getString("platform"), null);
        try {
            KingUtil.printLog("user info, vin: " + user.getVin() + ", total vin: " + user.getVinTotal() + ", recharegMoeny: " + user.getRechargeMoney());
            String description = "Kết quả: Thành công, Mã GD: "
                    + trans.getString("request_id")
                    + ", Thẻ: " + trans.getString("provider")
                    + ", Mệnh giá: " + cardAmount
                    + ", Serial: " + trans.getString("serial")
                    + ", Pin: " + trans.getString("pin") + "";
            long depositGoldAmount = Math.round(new Double(Double.parseDouble(addMoneyAmount)).longValue());
            long moneyAmount = Math.round(cashinItem.getMoney().longValue());
            recharge(user, depositGoldAmount, moneyAmount, "RechargeByCard", "Nạp thẻ", description, "card");
            KingUtil.printLog("user info after add depositGoldAmount, vin: " + user.getVin() + ", total vin: " + user.getVinTotal() + ", recharegMoeny: " + user.getRechargeMoney());

            //RMQApi.publishMessage("queue_dvt", message, 301);
            dao.UpdateGachtheTransctions(orderId, SUCCESS.getCode(), "success");
            RMQApi.publishMessage("queue_dvt", message, 301);

            KingUtil.printLog("CardRechargeCallbackProcessor 3");
            // Send update user depositGoldAmount

            description = "Nạp thành công thẻ: "
                    + trans.getString("provider")
                    + ", Mệnh giá: " + cardAmount
                    + ", Serial: " + trans.getString("serial")
                    + ", Pin: " + trans.getString("pin") + "";
            KingUtil.printLog("CardRechargeCallbackProcessor 8");
            // update depositGoldAmount messsage
            UpdateUserMoneyMsg msgg = new UpdateUserMoneyMsg();
            msgg.userNickname = nickname;
            msgg.totalMoney = user.getVinTotal();
            msgg.changeMoney = depositGoldAmount;
            msgg.desc = description;
            msgg.showPopup = true;
            MqttSender.SendMsgToNickname(user.getNickname(), msgg);
            mailBoxService.sendMailBoxFromByNickNameAdmin(nickname, "Nạp thẻ thành công", description);

            // Gửi tele mesage nếu user không online
            if (!userService.isOnline(user.getNickname())) {
                userService.sendTeleMessage(user.getNickname(), description);
            }

            cashinNotify = cashinNotifyDao.findById(uuid);
            cashinNotify.setError("false");
            cashinNotifyDao.update(cashinNotify);

            return SUCCESS.buildRes();
        } catch (Exception var28) {
            logger.error(var28);
            MoneyLogger.log(user.getNickname()
                    , "RechargeByGachThe"
                    , 0L, 0L
                    , "vin", "Nap qua ngan hang"
                    , "1001", var28.getMessage());

            KingUtil.printLog("CardRechargeCallbackProcessor 10");
            String title = "Kết quả giao dịch nạp thẻ";
            String description = "Kết quả giao dịch nạp thẻ: Thất bại, Mã GD: "
                    + trans.getString("request_id")
                    + ", Thẻ: " + trans.getString("provider")
                    + ", Mệnh giá: " + cardAmount
                    + ", Serial: " + trans.getString("serial")
                    + ", Pin: " + trans.getString("pin") + "";
            mailBoxService.sendMailBoxFromByNickNameAdmin(
                    nickname, title, description);

            return ERROR.buildRes();
        }
    }

    private String mapVinplayLucky79Message(String lucky79Code) {
        String var3 = lucky79Code.trim();
        byte var4 = -1;
        switch (var3.hashCode()) {
            case -1867169789:
                if (var3.equals("success")) {
                    var4 = 2;
                }
                break;
            case -245465363:
                if (var3.equals("card_fail")) {
                    var4 = 3;
                }
                break;
            case 422194963:
                if (var3.equals("processing")) {
                    var4 = 1;
                }
                break;
            case 1116313165:
                if (var3.equals("waiting")) {
                    var4 = 0;
                }
        }

        switch (var4) {
            case 0:
                return "Thẻ đang chờ để nạp";
            case 1:
                return "Thẻ đang được nạp trên thiết bị";
            case 2:
                return "Nạp thẻ thành công";
            case 3:
                return "Nạp Không thẻ thành công";
            default:
                return "Trạng thái không xác định";
        }
    }

    private int mapLucky79ToVinplayCode(String lucky79Code) {
        String var3 = lucky79Code.trim();
        byte var4 = -1;
        switch (var3.hashCode()) {
            case -1867169789:
                if (var3.equals("success")) {
                    var4 = 0;
                }
                break;
            case -245465363:
                if (var3.equals("card_fail")) {
                    var4 = 1;
                }
                break;
            case 422194963:
                if (var3.equals("processing")) {
                    var4 = 2;
                }
                break;
            case 1116313165:
                if (var3.equals("waiting")) {
                    var4 = 3;
                }
        }

        switch (var4) {
            case 0:
                return 0;
            case 1:
                return 35;
            case 2:
                return 30;
            case 3:
                return 30;
            default:
                return 30;
        }
    }

    private String mapVinplayMessage(String maxpayCode) {
        byte var3 = -1;
        switch (maxpayCode.hashCode()) {
            case 49:
                if (maxpayCode.equals("1")) {
                    var3 = 3;
                }
                break;
            case 50:
                if (maxpayCode.equals("2")) {
                    var3 = 4;
                }
                break;
            case 51:
                if (maxpayCode.equals("3")) {
                    var3 = 5;
                }
                break;
            case 52:
                if (maxpayCode.equals("4")) {
                    var3 = 6;
                }
                break;
            case 53:
                if (maxpayCode.equals("5")) {
                    var3 = 7;
                }
                break;
            case 54:
                if (maxpayCode.equals("6")) {
                    var3 = 8;
                }
                break;
            case 55:
                if (maxpayCode.equals("7")) {
                    var3 = 9;
                }
                break;
            case 56:
                if (maxpayCode.equals("8")) {
                    var3 = 10;
                }
                break;
            case 57:
                if (maxpayCode.equals("9")) {
                    var3 = 11;
                }
                break;
            case 1567:
                if (maxpayCode.equals("10")) {
                    var3 = 12;
                }
                break;
            case 1568:
                if (maxpayCode.equals("11")) {
                    var3 = 13;
                }
                break;
            case 1569:
                if (maxpayCode.equals("12")) {
                    var3 = 14;
                }
                break;
            case 1570:
                if (maxpayCode.equals("13")) {
                    var3 = 15;
                }
                break;
            case 1571:
                if (maxpayCode.equals("14")) {
                    var3 = 16;
                }
                break;
            case 1572:
                if (maxpayCode.equals("15")) {
                    var3 = 17;
                }
                break;
            case 1573:
                if (maxpayCode.equals("16")) {
                    var3 = 18;
                }
                break;
            case 1574:
                if (maxpayCode.equals("17")) {
                    var3 = 19;
                }
                break;
            case 1821:
                if (maxpayCode.equals("96")) {
                    var3 = 20;
                }
                break;
            case 1823:
                if (maxpayCode.equals("98")) {
                    var3 = 21;
                }
                break;
            case 1824:
                if (maxpayCode.equals("99")) {
                    var3 = 22;
                }
                break;
            case 49586:
                if (maxpayCode.equals("200")) {
                    var3 = 0;
                }
                break;
            case 51508:
                if (maxpayCode.equals("400")) {
                    var3 = 1;
                }
                break;
            case 51512:
                if (maxpayCode.equals("404")) {
                    var3 = 2;
                }
        }

        switch (var3) {
            case 0:
                return "Gọi api thành công";
            case 1:
                return "Dữ liệu gửi lên không chính xác";
            case 2:
                return "Không tìm thấy giao dịch thẻ";
            case 3:
                return "Nạp thẻ thành công";
            case 4:
                return "Thẻ sai hoặc đã sử dụng";
            case 5:
                return "Thẻ bị khóa";
            case 6:
                return "Số lần nạp thẻ sai liên tiếp vượt quy định";
            case 7:
                return "Thông tin merchant sai";
            case 8:
                return "Chưa truyền transaction id";
            case 9:
                return "Thẻ sai định dạng";
            case 10:
                return "Không tìm thấy nhà cung cấp thẻ";
            case 11:
                return "Thông tin Session sai";
            case 12:
                return "Session timeout";
            case 13:
                return "Lỗi hệ thống nhà cung cấp";
            case 14:
                return "Merchant bị khóa";
            case 15:
                return "Ip không hợp lệ";
            case 16:
                return "Transaction id bị trùng";
            case 17:
                return "Thẻ không hợp lệ";
            case 18:
                return "Loại thẻ bị khóa";
            case 19:
                return "Thẻ đang xử lý";
            case 20:
                return "Nạp thẻ thất bại";
            case 21:
                return "Chờ xử lý";
            case 22:
                return "Trạng thái không xác định";
            default:
                return "Trạng thái không xác định";
        }
    }

    private int mapVinplayCode(String maxpayCode) {
        byte var3 = -1;
        switch (maxpayCode.hashCode()) {
            case 49:
                if (maxpayCode.equals("1")) {
                    var3 = 0;
                }
                break;
            case 50:
                if (maxpayCode.equals("2")) {
                    var3 = 1;
                }
                break;
            case 51:
                if (maxpayCode.equals("3")) {
                    var3 = 2;
                }
                break;
            case 52:
                if (maxpayCode.equals("4")) {
                    var3 = 5;
                }
                break;
            case 53:
                if (maxpayCode.equals("5")) {
                    var3 = 16;
                }
                break;
            case 54:
                if (maxpayCode.equals("6")) {
                    var3 = 17;
                }
                break;
            case 55:
                if (maxpayCode.equals("7")) {
                    var3 = 4;
                }
                break;
            case 56:
                if (maxpayCode.equals("8")) {
                    var3 = 8;
                }
                break;
            case 57:
                if (maxpayCode.equals("9")) {
                    var3 = 11;
                }
                break;
            case 1567:
                if (maxpayCode.equals("10")) {
                    var3 = 12;
                }
                break;
            case 1568:
                if (maxpayCode.equals("11")) {
                    var3 = 13;
                }
                break;
            case 1569:
                if (maxpayCode.equals("12")) {
                    var3 = 14;
                }
                break;
            case 1570:
                if (maxpayCode.equals("13")) {
                    var3 = 18;
                }
                break;
            case 1571:
                if (maxpayCode.equals("14")) {
                    var3 = 15;
                }
                break;
            case 1572:
                if (maxpayCode.equals("15")) {
                    var3 = 9;
                }
                break;
            case 1573:
                if (maxpayCode.equals("16")) {
                    var3 = 3;
                }
                break;
            case 1574:
                if (maxpayCode.equals("17")) {
                    var3 = 19;
                }
                break;
            case 1821:
                if (maxpayCode.equals("96")) {
                    var3 = 10;
                }
                break;
            case 1823:
                if (maxpayCode.equals("98")) {
                    var3 = 20;
                }
                break;
            case 1824:
                if (maxpayCode.equals("99")) {
                    var3 = 21;
                }
                break;
            case 51508:
                if (maxpayCode.equals("400")) {
                    var3 = 6;
                }
                break;
            case 51512:
                if (maxpayCode.equals("404")) {
                    var3 = 7;
                }
        }

        switch (var3) {
            case 0:
                return 0;
            case 1:
                return 31;
            case 2:
                return 32;
            case 3:
                return 32;
            case 4:
                return 35;
            case 5:
                return 1;
            case 6:
                return 1;
            case 7:
                return 1;
            case 8:
                return 1;
            case 9:
                return 1;
            case 10:
                return 1;
            case 11:
                return 1;
            case 12:
                return 1;
            case 13:
                return 1;
            case 14:
                return 1;
            case 15:
                return 1;
            case 16:
                return 1;
            case 17:
                return 1;
            case 18:
                return 1;
            case 19:
                return 30;
            case 20:
                return 30;
            case 21:
                return 30;
            default:
                return 30;
        }
    }

    public void receiveResultFromZoanBank(
            String transId, String bankNo, String bankName, String bankCode, String amount,
            String comment, String nickname, String transTime, String signature) throws Exception {
        // check valid data (signature)
        String validSignature = KingUtil.MD5(Constants.BankInInfo.partnerKey + "|"
                + bankNo + "|" + amount + "|" + comment + "|" + transTime + "|" + transId);
        if (!validSignature.equals(signature)) {
            RechargeDaoImpl rechargeDao = new RechargeDaoImpl();
            rechargeDao.insertLogRechargeByBankError(
                    transId, "version", "command", transId, "merchantID", "orderInfo", "currency",
                    Integer.parseInt(amount), "locale", "cardType", transId, comment, "secureHash", "Sai signature");
            throw new Exception("Sai signature");
        }

        GameConfigServiceImpl gameConfigServiceImpl = new GameConfigServiceImpl();
        ResultGameConfigResponse gateInOutConfigData = gameConfigServiceImpl.getGameConfigAdmin("king365_gate_in_out_rate", "").get(0);
        if (gateInOutConfigData == null) {
            logger.debug("gateInOutConfigData == null");
            throw new Exception("Lỗi hệ thống, không tìm thấy config");
        }
        org.json.JSONObject gateInOutConfig = new org.json.JSONObject(gateInOutConfigData.value);
        Double rateMoneyToGold = gateInOutConfig.getDouble("bank_in_money_to_gold_rate");
        if (rateMoneyToGold == null) {
            logger.debug("rateMoneyToGold == null");
            throw new Exception("Lỗi hệ thống, không tìm thấy config");
        }
        long depositGoldAmount = Double.valueOf(Integer.parseInt(amount) * rateMoneyToGold).longValue();
        long moneyAmount = Long.parseLong(amount);

        UserCacheModel user = userService.getCacheUserIgnoreCase(nickname);
        if (user == null) {
            throw new Exception("Không tìm thấy user với nick " + nickname);
        }

        String description = "Kết quả: Thành công. Max GD: " + transId + ". Ngân hàng: " + bankCode + ", Mệnh giá: " + amount;
        KingUtil.logUser("receiveResultFromZoanBank() before updating", user);
        recharge(user, depositGoldAmount, moneyAmount, "RechargeByBank", "Nạp qua ngân hàng", description, "bank");
        KingUtil.logUser("receiveResultFromZoanBank() after updating", user);

        // Bắn notify về báo thay đổi tiền
        KingUtil.printLog("updateMoneyWithNotify() nickname: " + nickname + ", change depositGoldAmount: " + depositGoldAmount + ", after depositGoldAmount: " + user.getVinTotal());
        UpdateUserMoneyMsg msgg = SocketMessageFactory.createUpdateUserMoneyMsg(
                user, depositGoldAmount, "Nạp thành công BANK, mã giao dịch: " + transId + ", mệnh giá: " + amount, true);
        MqttSender.SendMsgToNickname(nickname, msgg);

        //rechargeDao.updateRechargeByBank(merchTxnRef, SUCCESS, description, trans_id, comment, amount);
        RechargeByBankMessage saveMess = new RechargeByBankMessage();
        saveMess.setNickname(nickname);
        saveMess.setMoney(user.getVin());
        saveMess.setBank(bankName);
        saveMess.setTransId(transId);
        saveMess.setAmount(Integer.parseInt(amount));
        saveMess.setAddMoney(depositGoldAmount);
        saveMess.setCreateTime(transTime);
        RechargeDao rechargeDao = new RechargeDaoImpl();
        rechargeDao.logRechargeByBank(saveMess, user.getClient(), user.getPlatform());
    }

    public void receiveResultFromCoinGate(
            String nickname, String amount, String trx_id, String address, String fromAddress,String token, String tokenType, String signature, String status, long transTime) throws Exception {
        // check valid data (signature)
        String validSignature = KingUtil.MD5(Constants.CoinPaymentInfo.partnerName + "|"
                + Constants.CoinPaymentInfo.secretKey + "|" + nickname + "|" + amount + "|" + trx_id + "|" + token + "|" + tokenType);
        if (!validSignature.equals(signature)) {
            RechargeDaoImpl rechargeDao = new RechargeDaoImpl();
            /*rechargeDao.insertLogRechargeByBankError(
                    transId, "version", "command", transId, "merchantID", "orderInfo", "currency",
                    Integer.parseInt(amount), "locale", "cardType", transId, comment, "secureHash", "Sai signature");
                    */
            throw new Exception("Sai signature");
        }

        GameConfigServiceImpl gameConfigServiceImpl = new GameConfigServiceImpl();
        ResultGameConfigResponse gateInOutConfigData = gameConfigServiceImpl.getGameConfigAdmin("king365_gate_in_out_rate", "").get(0);
        if (gateInOutConfigData == null) {
            logger.debug("gateInOutConfigData == null");
            throw new Exception("Lỗi hệ thống, không tìm thấy config");
        }
        /*org.json.JSONObject gateInOutConfig = new org.json.JSONObject(gateInOutConfigData.value);
        Double rateMoneyToGold = gateInOutConfig.getDouble("coin_in_money_to_gold_rate");
        if (rateMoneyToGold == null) {
            logger.debug("rateMoneyToGold == null");
            throw new Exception("Lỗi hệ thống, không tìm thấy config");
        }*/
        long depositGoldAmount = Double.valueOf(Integer.parseInt(amount) * CoinPaymentClient.getUsdtToVndRate()).longValue();
        long moneyAmount = Long.parseLong(amount);

        UserCacheModel user = userService.forceGetCachedUser(nickname);
        if (user == null) {
            throw new Exception("Không tìm thấy user với nick " + nickname);
        }

        String description = "Kết quả: Thành công. Max GD: " + trx_id + ". Từ địa chỉ ví: " + fromAddress + ", Mệnh giá: " + amount+", token: "+token+", token_type: "+tokenType;
        KingUtil.logUser("receiveResultFromCoinGate() before updating", user);
        recharge(user, depositGoldAmount, moneyAmount, "RechargeByCoin", "Nạp qua coin", description, "coin");
        KingUtil.logUser("receiveResultFromCoinGate() after updating", user);

        // Bắn notify về báo thay đổi tiền
        KingUtil.printLog("updateMoneyWithNotify() nickname: " + nickname + ", change depositGoldAmount: " + depositGoldAmount + ", after depositGoldAmount: " + user.getVinTotal());
        UpdateUserMoneyMsg msgg = SocketMessageFactory.createUpdateUserMoneyMsg(
                user, depositGoldAmount, "Nạp thành công COIN, mã giao dịch: " + trx_id + ", mệnh giá: " + amount, true);
        MqttSender.SendMsgToNickname(nickname, msgg);

        //rechargeDao.updateRechargeByBank(merchTxnRef, SUCCESS, description, trans_id, comment, amount);
        RechargeByCoinMessage saveMess = new RechargeByCoinMessage();
        saveMess.setWalletCode(nickname);
        saveMess.setAddress(address);
        saveMess.setFromAddress(fromAddress);
        saveMess.setAmount(Integer.parseInt(amount));
        saveMess.setToken(token);
        saveMess.setTokenType(tokenType);
        saveMess.setTrxId(trx_id);
        saveMess.setStatus(status);
        saveMess.setTransTime(transTime);
        RechargeDao rechargeDao = new RechargeDaoImpl();
        rechargeDao.logRechargeByCoin(saveMess, user.getClient(), user.getPlatform());
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    @Override
    public void receiveResultFromZoanMomo(String jsonReceived, String ipAddress) throws Exception {
        Gson gson = new Gson();
        ZoanMomoReq result = gson.fromJson(jsonReceived, ZoanMomoReq.class);
        if (result == null) {
            throw new Exception("Data from momo == null");
        }
        KingUtil.printLog("receiveResultFromZoanMomo() " + result);
        if (result.getComment() == null) {
            throw new Exception("Không tìm thấy người dùng");
        }
        CashinNotifyDao cashinNotifyDao = new CashinNotifyDaoImpl();
        // Save data to db
        CashinNotify cashinNotify = new CashinNotify();
        cashinNotify.setUuid(result.getTrans_id());
        cashinNotify.setGateType("momo");
        cashinNotify.setIp(ipAddress);
        cashinNotify.setReceiveTime(new Timestamp(new Date().getTime()));
        cashinNotify.setComment(result.getComment());
        cashinNotify.setData(jsonReceived);
        cashinNotify.setError("true");
        try {
            cashinNotifyDao.save(cashinNotify);
        } catch (PersistenceException e) {
            // Lỗi này xảy ra khi data đã có trong db
            // có nghĩa là callback với trans_id đã được xử lí rồi
            KingUtil.printLog("BankCallbackProcessor PersistenceException: " + KingUtil.printException(e));
            // Kiểm tra nếu transId này chưa thành công thì cho xử lí,
            // nếu đã thành công thì ignore callback này
            CashinNotify oldCashinNotify = cashinNotifyDao.findById(result.getTrans_id());
            if (oldCashinNotify.getError().equals("false")) {
                // ignore callback này thôi
                // throw new Exception("Giao dịch đã được xử lí trước đó.");
                return;
            }
        }

        RechargeDao dao = new RechargeDaoImpl();
        if (dao.getRechargeByGachthe(result.getTrans_id()) != null) {
            throw new Exception("This transaction has done before.");
        }
        final String nickname = UserValidaton.filterNicknameInvaliChar(result.getComment());

        UserCacheModel user = userService.getCacheUserIgnoreCase(nickname);
        if (user == null) {
            // Tìm tài khoản theo sdt
            Map<String, Object> mapFindData = new HashMap<>();
            String mobile084 = result.getFrom();
            if (mobile084.startsWith("0")) {
                mobile084 = "84" + mobile084.substring(1);
            }
            mapFindData.put("mobile", mobile084);
            List<AccountTeleLink> docs = mongodbService.find("AccountTeleLink", mapFindData, AccountTeleLink::fromDocument);
            if (docs.size() > 0) {
                AccountTeleLink doc = docs.get(0);
                String nickName = doc.getNickName();
                user = userService.getCacheUserIgnoreCase(nickName);
                if (user == null) {
                    GU.sendCustomerService("Có lỗi khi nạp MOMO, Không tìm thấy người dùng. info: " + jsonReceived);
                    dao.saveZoanMomoTran(result, 50, nickname, 0L, result.getFrom() + "-" + result.getComment(),
                            "ZoanMomo", "", user.getPlatform(), result.getAmount());
                    // throw new Exception("User null, nickname: " + nickname);
                }
            } else {
                GU.sendCustomerService("Có lỗi khi nạp MOMO, Không tìm thấy người dùng. info: " + jsonReceived);
                dao.saveZoanMomoTran(
                        result, 50, nickname, 0L, result.getFrom() + "-" + result.getComment(),
                        "ZoanMomo", "", "", result.getAmount());
                // throw new Exception("User null, nickname: " + nickname);
            }
        }

        String sign = result.getRequest_id() + "|" + Constants.MomoInfo.partnerKey + "|" + result.getTrans_time();
        sign = DvtUtils.getMd5(sign);
        if (!sign.equals(result.getSignature())) {
            RechargeByCardMessage message = new RechargeByCardMessage(
                    user.getNickname(), result.getRequest_id(), "MOMO", result.getFrom(), result.getFrom(), 0, -1,
                    "Sai chữ ký", -1, 0, null, null, "ZoanMomo", "", null);
            RMQApi.publishMessage("queue_dvt", message, 301);
            dao.saveZoanMomoTran(result, 70, user.getNickname(), 0L, user.getNickname(), "ZoanMomo", user.getClient(), user.getPlatform(), result.getAmount());
            throw new Exception("Fail check sign.");
        }

        double moneyAmount = result.getAmount();
        long longMoneyAmount = result.getAmount();
        CashinItemDao cashinItemDao = new CashinItemDaoImpl();
        List<CashinItem> listCashinItem = cashinItemDao.findByProvider("MOMO");
        CashinItem item = listCashinItem.stream()
                .filter((cashinItem -> cashinItem.getMoney() == moneyAmount))
                .findFirst()
                .orElse(null);
        long depositGoldAmount;

        if (item != null) {
            depositGoldAmount = item.getGold().longValue();
        } else {
            GameConfigServiceImpl gameConfigServiceImpl = new GameConfigServiceImpl();
            ResultGameConfigResponse gateInOutConfigData = gameConfigServiceImpl.getGameConfigAdmin("king365_gate_in_out_rate", "").get(0);
            if (gateInOutConfigData == null) {
                logger.debug("gateInOutConfigData == null");
                throw new Exception("Lỗi hệ thống, không tìm thấy config");
            }
            org.json.JSONObject gateInOutConfig = new org.json.JSONObject(gateInOutConfigData.value);
            Double rateMoneyToGold = gateInOutConfig.getDouble("momo_in_money_to_gold_rate");
            if (rateMoneyToGold == null) {
                logger.debug("rateMoneyToGold == null");
                throw new Exception("Lỗi hệ thống, không tìm thấy config");
            }
            depositGoldAmount = (long) (moneyAmount * rateMoneyToGold);
        }

        String description = "Kết quả MOMO: Thành công, Mã GD: " + result.getRequest_id()
                + ", mệnh giá: " + moneyAmount;
        KingUtil.logUser("receiveResultFromZoanMomo() before updating", user);
        recharge(user, depositGoldAmount, longMoneyAmount, "RechargeByMomo", "Zoan", description, "momo");
        KingUtil.logUser("receiveResultFromZoanMomo() after updating", user);

        user.setRechargeFail(0);

        dao.saveZoanMomoTran(result, 1, user.getNickname(), user.getVin(), user.getNickname(), "ZoanMomo", user.getClient(), user.getPlatform(), depositGoldAmount);

        RechargeByCardMessage message = new RechargeByCardMessage(
                user.getNickname(), result.getRequest_id(), "MOMO", result.getFrom(), result.getFrom(), (int) moneyAmount,
                1, "Thanh cong", 1, (int) moneyAmount, null, null, "ZoanMomo", "web", null);
        RMQApi.publishMessage("queue_dvt", message, 301);


        UpdateUserMoneyMsg msgg = SocketMessageFactory.createUpdateUserMoneyMsg(
                user, depositGoldAmount, description, true);
        MqttSender.SendMsgToNickname(user.getNickname(), msgg);

        String desc = "Cộng tiền nạp Momo thành công, Mã giao dịch: " + result.getRequest_id() + ", Mệnh giá: " + moneyAmount;
        // Mail inbox
        MailBoxServiceImpl service = new MailBoxServiceImpl();
        service.sendMailPopupMessage(
                user.getNickname(), "Kết quả nạp Momo", desc);
        if (!userService.isOnline(user.getNickname())) {
            userService.sendTeleMessage(user.getNickname(), desc);
        }

        cashinNotify = cashinNotifyDao.findById(result.getTrans_id());
        cashinNotify.setError("false");
        cashinNotifyDao.update(cashinNotify);
    }

    private int mapErrorCodeEpayMegaCard(String status) {
        byte var3 = -1;
        switch (status.hashCode()) {
            case 48:
                if (status.equals("0")) {
                    var3 = 11;
                }
                break;
            case 49:
                if (status.equals("1")) {
                    var3 = 0;
                }
                break;
            case 51:
                if (status.equals("3")) {
                    var3 = 12;
                }
                break;
            case 52:
                if (status.equals("4")) {
                    var3 = 13;
                }
                break;
            case 53:
                if (status.equals("5")) {
                    var3 = 14;
                }
                break;
            case 55:
                if (status.equals("7")) {
                    var3 = 15;
                }
                break;
            case 56:
                if (status.equals("8")) {
                    var3 = 16;
                }
                break;
            case 57:
                if (status.equals("9")) {
                    var3 = 17;
                }
                break;
            case 1445:
                if (status.equals("-2")) {
                    var3 = 4;
                }
                break;
            case 1446:
                if (status.equals("-3")) {
                    var3 = 5;
                }
                break;
            case 1567:
                if (status.equals("10")) {
                    var3 = 18;
                }
                break;
            case 1568:
                if (status.equals("11")) {
                    var3 = 19;
                }
                break;
            case 1569:
                if (status.equals("12")) {
                    var3 = 20;
                }
                break;
            case 1570:
                if (status.equals("13")) {
                    var3 = 21;
                }
                break;
            case 1573:
                if (status.equals("16")) {
                    var3 = 30;
                }
                break;
            case 1691:
                if (status.equals("50")) {
                    var3 = 6;
                }
                break;
            case 1692:
                if (status.equals("51")) {
                    var3 = 2;
                }
                break;
            case 1693:
                if (status.equals("52")) {
                    var3 = 22;
                }
                break;
            case 1694:
                if (status.equals("53")) {
                    var3 = 8;
                }
                break;
            case 1696:
                if (status.equals("55")) {
                    var3 = 23;
                }
                break;
            case 1697:
                if (status.equals("56")) {
                    var3 = 29;
                }
                break;
            case 1698:
                if (status.equals("57")) {
                    var3 = 25;
                }
                break;
            case 1699:
                if (status.equals("58")) {
                    var3 = 26;
                }
                break;
            case 1700:
                if (status.equals("59")) {
                    var3 = 7;
                }
                break;
            case 1722:
                if (status.equals("60")) {
                    var3 = 27;
                }
                break;
            case 1723:
                if (status.equals("61")) {
                    var3 = 28;
                }
                break;
            case 1724:
                if (status.equals("62")) {
                    var3 = 24;
                }
                break;
            case 1725:
                if (status.equals("63")) {
                    var3 = 31;
                }
                break;
            case 1726:
                if (status.equals("64")) {
                    var3 = 32;
                }
                break;
            case 1727:
                if (status.equals("65")) {
                    var3 = 33;
                }
                break;
            case 1728:
                if (status.equals("66")) {
                    var3 = 34;
                }
                break;
            case 1729:
                if (status.equals("67")) {
                    var3 = 35;
                }
                break;
            case 1730:
                if (status.equals("68")) {
                    var3 = 36;
                }
                break;
            case 1824:
                if (status.equals("99")) {
                    var3 = 1;
                }
                break;
            case 44812:
                if (status.equals("-10")) {
                    var3 = 3;
                }
                break;
            case 44813:
                if (status.equals("-11")) {
                    var3 = 10;
                }
                break;
            case 44847:
                if (status.equals("-24")) {
                    var3 = 9;
                }
        }

        switch (var3) {
            case 0:
                return 0;
            case 1:
                return 30;
            case 2:
                return 36;
            case 3:
                return 35;
            case 4:
                return 32;
            case 5:
                return 34;
            case 6:
                return 31;
            case 7:
                return 33;
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
                return 40;
            default:
                return 30;
        }
    }

    private int getErrorCode(int status) {
        switch (status) {
            case 0:
                return 0;
            case 10:
                return 1;
            case 12:
                return 31;
            case 13:
                return 35;
            case 14:
                return 32;
            case 15:
                return 33;
            case 16:
                return 34;
            case 17:
            case 18:
                return 36;
            case 99:
                return 30;
            default:
                return 30;
        }
    }

    private void alert(String number, String content, boolean isCall) {
        AlertServiceImpl alertService = new AlertServiceImpl();
        if (number.contains(",")) {
            String[] arr = number.split(",");
            ArrayList<String> mList = new ArrayList();
            String[] var7 = arr;
            int var8 = arr.length;

            for (int var9 = 0; var9 < var8; ++var9) {
                String m = var7[var9];
                m = m.trim();
                mList.add(m);
            }

            alertService.alert2List(mList, content, isCall);
        } else {
            alertService.alert2One(number, content, isCall);
        }

    }

    private void recharge(UserCacheModel user, long depositAmount, long moneyAmount, String actionName, String serviceName, String description, String channel) {
        userService.updateUser(user.getNickname(), (u) -> {
            u.setVin(u.getVin() + depositAmount);
            u.setVinTotal(u.getVinTotal() + depositAmount);
            u.setRechargeMoney(u.getRechargeMoney() + depositAmount);
        });
        try {
            user = userService.forceGetCachedUser(user.getNickname());
            MoneyMessageInMinigame messageMoney = new MoneyMessageInMinigame(
                    VinPlayUtils.genMessageId(),
                    user.getId(), user.getNickname()
                    , actionName
                    , user.getVin(), user.getVinTotal(), depositAmount
                    , "vin", 0L, 0, 0);
            RMQApi.publishMessageInMiniGame(messageMoney);

            LogMoneyUserMessage messageLog = new LogMoneyUserMessage(
                    user.getId(), user.getNickname(), actionName, serviceName,
                    user.getVinTotal(), depositAmount, "vin", description, 0L, false, user.isBot());
            RMQApi.publishMessageLogMoney(messageLog);
        } catch (Exception e) {
            e.printStackTrace();
            GU.sendOperation("RechargeService.recharge: Notify MoneyMessageInMinigame exception " + e.getMessage());
        }
    }

    public boolean updateUserClient(String nickName, String client) {
        // update client cho cashin card va momo
        Map<String, Object> filterMap = new HashMap<>();
        filterMap.put("nick_name", nickName);
        Map<String, Object> updateMap = new HashMap<>();
        updateMap.put("client", client);
        mongodbService.update("dvt_recharge_by_gachthe", filterMap, updateMap);
        // udpate cho cashin bank
        mongodbService.update("dvt_recharge_by_bank", filterMap, updateMap);

        // update cho cashout card / momo / bank
        CashoutRequestDao cashoutRequestDao = new CashoutRequestDaoImpl();
        try {
            cashoutRequestDao.updateClient(nickName, client);
        } catch (Exception e) {
            KingUtil.printException("RecharegeService", e);
            e.printStackTrace();
        }
        return true;
    }

    public boolean updateUserPlatform(String nickName, String pf) {
        // update client cho cashin card va momo
        Map<String, Object> filterMap = new HashMap<>();
        filterMap.put("nick_name", nickName);
        Map<String, Object> updateMap = new HashMap<>();
        updateMap.put("platform", pf);
        mongodbService.update("dvt_recharge_by_gachthe", filterMap, updateMap);
        // udpate cho cashin bank
        mongodbService.update("dvt_recharge_by_bank", filterMap, updateMap);

        // update cho cashout card / momo / bank
        CashoutRequestDao cashoutRequestDao = new CashoutRequestDaoImpl();
        try {
            cashoutRequestDao.updatePlatform(nickName, pf);
        } catch (Exception e) {
            KingUtil.printException("RecharegeService", e);
            e.printStackTrace();
        }
        return true;
    }
}
