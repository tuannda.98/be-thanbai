package com.vinplay.usercore.dao;

import com.vinplay.vbee.common.models.TopCaoThu;
import com.vinplay.vbee.common.models.UserAdminInfo;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.models.userMission.MissionObj;
import com.vinplay.vbee.common.models.userMission.UserMissionCacheModel;
import com.vinplay.vbee.common.response.UserInfoModel;
import java.sql.SQLException;
import java.util.List;

public interface UserDao {
    boolean checkUsername(String var1) throws SQLException;

    boolean checkNickname(String var1) throws SQLException;

    int checkAgent(String var1) throws SQLException;

    boolean checkNicknameExist(String var1) throws SQLException;

    boolean updateMoney(int var1, long var2, String var4) throws SQLException;

    boolean updateRechargeMoney(int var1, long var2) throws SQLException;

    boolean updateCashoutMoney(int var1, long var2) throws SQLException;

    boolean updateClient(int id, String appId) throws SQLException;

    boolean updatePlatform(int id, String appId) throws SQLException;

    UserModel getUserByUserName(String var1) throws SQLException;

    UserModel getUserByNickName(String var1) throws SQLException;

    UserModel getUserByFBId(String var1) throws SQLException;

    UserModel getUserByAppleId(String var1) throws SQLException;

    UserModel getUserByDeviceId(String var1) throws SQLException;

    UserModel getUserByGGId(String var1) throws SQLException;

    long getMoneyUser(String var1, String var2) throws SQLException;

    long getCurrentMoney(String var1, String var2) throws SQLException;

    int getIdByNickname(String var1) throws SQLException;

    int getIdByUsername(String var1) throws SQLException;

    boolean restoreMoneyByAdmin(int var1, long var2, long var4, long var6, String var8) throws SQLException;

    boolean checkMobile(String var1) throws SQLException;

    boolean checkMobileDaiLy(String var1) throws SQLException;

    boolean checkMobileSecurity(String var1) throws SQLException;

    boolean checkEmailSecurity(String var1) throws SQLException;

    List<TopCaoThu> getTopCaoThu(String var1, String var2, int var3);

    UserModel getUserNormalByNickName(String var1) throws SQLException;

    boolean updateStatusDailyByNickName(String var1, int var2) throws SQLException;

    List<UserAdminInfo> searchUserAdmin(String var1, String var2, String var3, String var4, String var5, String var6, String var7, String var8, int var9, int var10, String var11, String var12, String var13) throws SQLException;

    int countSearchUserAdmin(String var1, String var2, String var3, String var4, String var5, String var6, String var7, String var8, String var9) throws SQLException;

    boolean insertBot(String var1, String var2, String var3, long var4, long var6, int var8) throws SQLException;

    int checkBotByNickname(String var1) throws SQLException;

    List<UserInfoModel> checkPhoneByUser(String var1) throws SQLException;

    UserInfoModel checkPhoneExists(String var1) throws SQLException;

    UserMissionCacheModel getListMissionByNickName(String var1, String var2, int var3) throws SQLException;

    void insertUserMission(String var1, MissionObj var2, UserModel var3) throws SQLException;

    void updateUserMission(String var1, String var2, MissionObj var3) throws SQLException;

    void resetUserMission() throws Exception;

    UserCacheModel getUserByNickNameCache(String var1) throws SQLException;

    List<UserCacheModel> GetNickNameFreeze() throws SQLException;

    void insertCommission(int var1, String var2, long var3, String var5) throws SQLException;
}

