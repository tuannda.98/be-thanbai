/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.zoan;

public class M32MomoResponse {
    private long currentTime;
    private String errorCode;
    private String errorDescription;
    private String destinationInfo;

    public long getCurrentTime() {
        return this.currentTime;
    }

    public void setCurrentTime(long currentTime) {
        this.currentTime = currentTime;
    }

    public String getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return this.errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public String getDestinationInfo() {
        return this.destinationInfo;
    }

    public void setDestinationInfo(String destinationInfo) {
        this.destinationInfo = destinationInfo;
    }

    public class M32Account {
        private String name;
        private String phone;

        public M32Account(String _name, String _phone) {
            this.name = _name;
            this.phone = _phone;
        }

        public String getName() {
            return this.name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return this.phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }
    }
}

