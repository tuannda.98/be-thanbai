/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.vinplay.vbee.common.messages.vtcpay.LogVTCPayTopupMessage
 */
package com.vinplay.vtcpay.dao;

import com.vinplay.vbee.common.messages.vtcpay.LogVTCPayTopupMessage;

public interface VTCPayDao {
    boolean logVTCPayTopup(LogVTCPayTopupMessage var1);

    LogVTCPayTopupMessage checkTrans(String var1);
}

