/*
 * Decompiled with CFR 0.150.
 *
 * Could not load the following classes:
 *  com.fasterxml.jackson.core.JsonProcessingException
 *  com.hazelcast.core.IMap
 *  org.json.JSONObject
 */
package com.vinplay.usercore.dao;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.hazelcast.core.IMap;
import com.vinplay.vbee.common.messages.GiftCodeMessage;
import com.vinplay.vbee.common.models.SpecialGiftCode;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.response.*;
import com.vinplay.vbee.common.response.giftcode.GiftcodeStatisticObj;
import org.json.JSONObject;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

public interface GiftCodeDAO {
    boolean xuatGiftCode(GiftCodeMessage var1);

    JSONObject GetGiftCode(String var1);

    GiftCodeUpdateResponse updateGiftCode(String var1, String var2) throws SQLException;

    List<GiftCodeResponse> searchAllGiftCode(String var1, String var2, String var3, String var4, String var5, String var6, String var7, String var8, int var9, int var10, String var11, String var12, String var13, String var14);

    List<GiftCodeResponse> searchAllGiftCodeAdmin(String var1, String var2, String var3, String var4, String var5, String var6, int var7, int var8, String var9);

    GiftCodeCountResponse countGiftCodeByPrice(String var1, String var2, String var3, String var4, String var5, String var6, String var7, String var8);

    List<ReportGiftCodeResponse> ToolReportGiftCode(String var1, String var2, String var3, String var4, String var5, String var6, String var7) throws SQLException, ParseException, JsonProcessingException;

    List<String> ListAllPrice(int var1) throws SQLException;

    boolean genGiftCode(GiftCodeMessage var1);

    GiftCodeCountResponse countGiftCodeByPriceAdmin(String var1, String var2, String var3, String var4, String var5, String var6);

    long countsearchAllGiftCode(String var1, String var2, String var3, String var4, String var5, String var6, String var7, String var8, String var9, String var10, String var11, String var12);

    long countsearchAllGiftCodeAdmin(String var1, String var2, String var3, String var4, String var5, String var6, String var7);

    List<ReportGiftCodeResponse> ToolReportGiftCodeBySource(String var1, String var2, String var3, String var4, String var5, int var6, String var7, String var8) throws SQLException, ParseException, JsonProcessingException;

    String uploadFileGiftCode(String var1, long var2, long var4);

    boolean RestoreGiftCode(String var1, String var2, String var3, String var4);

    List<GiftCodeResponse> searchAllGiftCodeByNickName(String var1, int var2);

    long countAllGiftCodeByNickName(String var1);

    GiftCodeByNickNameResponse getUserInfoByGiftCode(String var1, IMap<String, UserCacheModel> var2, int var3, int var4);

    GiftCodeDeleteResponse DeleteGiftCode(String var1, String var2, String var3, String var4);

    List<String> loadAllGiftcode(String gia, String dotphathanh);

    List<GiftcodeStatisticObj> thongKeGiftcodeDaXuat(String var1, String var2, String var3, String var4, String var5);

    List<SpecialGiftCode> GetSpecialGiftCodes();

    boolean CheckSpecialGiftCodes(String var1);

    GiftCodeUpdateResponse updateSpecialGiftCodeNew(String var1, String var2) throws SQLException;

    boolean InsertSpecialGiftcode(SpecialGiftCode var1);

    boolean UpdateSpecialGiftcode(SpecialGiftCode var1);

    GiftCodeUpdateResponse updateSpecialGiftCode(String nickName, String giftCode, final int amount, int useCount) throws SQLException;

    boolean DeleteSpecialGiftcode(String var1);

    String GetGiftCodeByTypeNN(int var1, String var2);

    List<SpecialGiftCode> GetSpecialGiftCodesByQuery(int var1, int var2, String var3, long var4, String var6, int var7);

    void loadAllGiftcodeIntoCache();

    void updateGiftCodeStore(String var1);
}

