/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.zoan;

public class M32MomoCashoutResponse {
    private int errorCode;
    private String errorDesc;
    private String transId;
    private M32MomoCashoutMsg msg;

    public int getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDesc() {
        return this.errorDesc;
    }

    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc;
    }

    public String getTransId() {
        return this.transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public M32MomoCashoutMsg getMsg() {
        return this.msg;
    }

    public void setMsg(M32MomoCashoutMsg msg) {
        this.msg = msg;
    }

    public class M32MomoCashoutMsg {
        private String momoTransId;
        private String finishTime;
        private String userName;
        private String accountReceive;
        private String accountName;
        private long amount;
        private String comment;
        private String authKey;

        public String getMomoTransId() {
            return this.momoTransId;
        }

        public void setMomoTransId(String momoTransId) {
            this.momoTransId = momoTransId;
        }

        public String getFinishTime() {
            return this.finishTime;
        }

        public void setFinishTime(String finishTime) {
            this.finishTime = finishTime;
        }

        public String getUserName() {
            return this.userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getAccountReceive() {
            return this.accountReceive;
        }

        public void setAccountReceive(String accountReceive) {
            this.accountReceive = accountReceive;
        }

        public String getAccountName() {
            return this.accountName;
        }

        public void setAccountName(String accountName) {
            this.accountName = accountName;
        }

        public long getAmount() {
            return this.amount;
        }

        public void setAmount(long amount) {
            this.amount = amount;
        }

        public String getComment() {
            return this.comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getAuthKey() {
            return this.authKey;
        }

        public void setAuthKey(String authKey) {
            this.authKey = authKey;
        }
    }
}

