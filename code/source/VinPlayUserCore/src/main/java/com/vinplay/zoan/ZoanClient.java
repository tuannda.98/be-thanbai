/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.gson.Gson
 *  org.apache.log4j.Logger
 *  org.json.JSONObject
 */
package com.vinplay.zoan;

import casio.king365.Constants;
import com.google.gson.Gson;
import com.vinplay.dichvuthe.client.ApacheHttpClient;
import com.vinplay.dichvuthe.utils.RSAUtil;
import com.vinplay.usercore.utils.PartnerConfig;
import com.vinplay.vbee.common.utils.VinPlayUtils;
import com.vinplay.zoan.M32BankCashoutResponse;
import com.vinplay.zoan.M32MomoCashoutResponse;
import com.vinplay.zoan.M32MomoRequest;
import com.vinplay.zoan.M32MomoResponse;
import com.vinplay.zoan.ZoanMomoCashoutResponse;
import com.vinplay.zoan.ZoanMomoResponse;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;
import org.json.JSONObject;

public class ZoanClient {
    private static final Logger logger = Logger.getLogger("api");

    public static ZoanMomoResponse GetShippers() throws Exception {
        long request_time = System.currentTimeMillis();
        String id = request_time + "";
        String username = Constants.MomoInfo.partnerName;
        String partnerKey = Constants.MomoInfo.partnerKey;
        String signature = id + "|" + request_time + "|" + username + "|" + partnerKey;
        String url = PartnerConfig.MomoZoanEndpoint + "?username=" + Constants.MomoInfo.partnerName + "&request_id=" + id + "&request_time=" + request_time + "&signature=" + VinPlayUtils.getMD5Hash(signature);
        String res = ApacheHttpClient.get(url);
        Gson gson = new Gson();
        ZoanMomoResponse result = gson.fromJson(res, ZoanMomoResponse.class);
        return result;
    }

    public static M32MomoResponse GetShippersM32() throws Exception {
        String authKey;
        M32MomoRequest req = new M32MomoRequest();
        req.requestTime = System.currentTimeMillis() + "";
        req.type = "2";
        req.username = PartnerConfig.M32Username;
        req.authKey = authKey = VinPlayUtils.getMD5Hash(req.username + "|" + req.type + "|" + PartnerConfig.M32SecretKey);
        Gson gson = new Gson();
        String res = ApacheHttpClient.post(PartnerConfig.M32Endpoint, gson.toJson(req));
        logger.debug(res);
        M32MomoResponse result = gson.fromJson(res, M32MomoResponse.class);
        return result;
    }

    public static ZoanMomoCashoutResponse doCashoutMomo(String transid, String phone, String name, int valueExchange) {
        try {
            JSONObject data = new JSONObject();
            data.put("partner_id", PartnerConfig.ZoanMomoCashoutPartnerId);
            data.put("partner_reference", transid);
            data.put("quantity", 1);
            data.put("telco", "MOMO");
            data.put("denomination", valueExchange);
            data.put("request_acc_no", phone);
            data.put("request_acc_name", name);
            data.put("request_desc", transid);
            String signature = "partner_id=" + PartnerConfig.ZoanMomoCashoutPartnerId + "&partner_reference=" + transid + "&quantity=1&telco=MOMO&denomination=" + valueExchange;
            String signature64 = RSAUtil.sign(PartnerConfig.ZoanMomoCashoutPrivateKey, signature);
            data.put("signature", signature64);
            String result = ApacheHttpClient.post(PartnerConfig.ZoanMomoCashoutEndpoint, data.toString());
            logger.debug(result);
            Gson gson = new Gson();
            return gson.fromJson(result, ZoanMomoCashoutResponse.class);
        }
        catch (Exception ex) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw);
            String sStackTrace = sw.toString();
            logger.debug(ex.getMessage());
            logger.debug(sStackTrace);
            return null;
        }
    }

    public static M32MomoCashoutResponse doCashoutMomoM32(String transid, String phone, String name, int valueExchange) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            Date now = new Date();
            String requestTime = sdf.format(now);
            JSONObject data = new JSONObject();
            data.put("requestTime", requestTime);
            data.put("transId", transid);
            data.put("userName", PartnerConfig.M32Username);
            data.put("accountReceive", phone);
            data.put("amount", valueExchange);
            data.put("comment", "Chuyen tien cho sdt " + phone);
            String authKey = VinPlayUtils.getMD5Hash(requestTime + "|" + transid + "|" + PartnerConfig.M32Username + "|" + phone + "|" + valueExchange + "|Chuyen tien cho sdt " + phone + "|" + PartnerConfig.M32SecretKey);
            data.put("authKey", authKey);
            String result = ApacheHttpClient.post(PartnerConfig.M32MomoEndpoint, data.toString());
            logger.debug(result);
            Gson gson = new Gson();
            return gson.fromJson(result, M32MomoCashoutResponse.class);
        }
        catch (Exception ex) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw);
            String sStackTrace = sw.toString();
            logger.debug(ex.getMessage());
            logger.debug(sStackTrace);
            return null;
        }
    }

    public static M32BankCashoutResponse doCashoutBankM32(String transid, String accountId, String accountName, String bankName, int valueExchange) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            Date now = new Date();
            String requestTime = sdf.format(now);
            JSONObject data = new JSONObject();
            data.put("requestTime", requestTime);
            data.put("transId", transid);
            data.put("userName", PartnerConfig.M32Username);
            data.put("accountId", accountId);
            data.put("accountName", accountName);
            data.put("shortBankName", bankName);
            data.put("amount", valueExchange);
            data.put("comment", "Chuyen tien cho stk " + accountId);
            String authKey = VinPlayUtils.getMD5Hash(requestTime + "|" + transid + "|" + PartnerConfig.M32Username + "|" + accountId + "|" + accountName + "|" + bankName + "|" + valueExchange + "|Chuyen tien cho stk " + accountId + "|" + PartnerConfig.M32SecretKey);
            data.put("authKey", authKey);
            String result = ApacheHttpClient.post(PartnerConfig.M32BankEndpoint, data.toString());
            logger.debug(result);
            Gson gson = new Gson();
            return gson.fromJson(result, M32BankCashoutResponse.class);
        }
        catch (Exception ex) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw);
            String sStackTrace = sw.toString();
            logger.debug(ex.getMessage());
            logger.debug(sStackTrace);
            return null;
        }
    }
}

