/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.apache.commons.codec.binary.Hex
 *  org.apache.http.conn.ssl.X509HostnameVerifier
 *  org.json.simple.JSONObject
 *  org.json.simple.parser.JSONParser
 *  org.json.simple.parser.ParseException
 */
package com.vinplay.lucky79;

import com.vinplay.lucky79.ChargeLucky79Response;
import com.vinplay.lucky79.Lucky79Exception;
import com.vinplay.lucky79.ReCheckLucky79Response;
import com.vinplay.usercore.utils.GameCommon;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.TreeMap;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.commons.codec.binary.Hex;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Lucky79Client {
    private final String merchantId;
    private final String secretKey;

    public Lucky79Client(String merchantId, String secretKey) {
        this.merchantId = merchantId;
        this.secretKey = secretKey;
    }

    public void installMyPolicy() throws Exception {
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager(){

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            @Override
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }

            @Override
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }};
        SSLContext sc = SSLContext.getInstance("SSL");
        X509HostnameVerifier verifier = new X509HostnameVerifier(){

            public void verify(String host, SSLSocket ssl) throws IOException {
            }

            public void verify(String host, X509Certificate cert) throws SSLException {
            }

            public void verify(String host, String[] cns, String[] subjectAlts) throws SSLException {
            }

            public boolean verify(String s, SSLSession sslSession) {
                return true;
            }
        };
        sc.init(null, trustAllCerts, new SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        HttpsURLConnection.setDefaultHostnameVerifier(verifier);
    }

    public void installAllTrustManager() throws Exception {
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager(){

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            @Override
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }

            @Override
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }};
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier(){

                @Override
                public boolean verify(String urlHostname, SSLSession _session) {
                    return true;
                }
            });
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ReCheckLucky79Response doCharge(String cardType, String pin, String seri, String transId) throws Exception {
        try {
            this.validateCharge(this.luckyProviderMaxpay(cardType), pin, seri);
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("merchant_id", this.merchantId);
            map.put("code", pin);
            map.put("serial", seri);
            map.put("type", this.luckyProviderMaxpay(cardType));
            map.put("merchant_txn_id", transId);
            URL url = new URL(GameCommon.getValueStr("LUCKY79_URL") + "/RegCharge?apiKey=" + this.secretKey + "&type=" + this.luckyProviderMaxpay(cardType) + "&code=" + pin + "&serial=" + seri);
            HttpURLConnection request = (HttpURLConnection)url.openConnection();
            request.setConnectTimeout(90000);
            request.setUseCaches(false);
            request.setDoOutput(true);
            request.setDoInput(true);
            HttpURLConnection.setFollowRedirects(true);
            request.setInstanceFollowRedirects(true);
            request.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            request.setRequestMethod("GET");
            BufferedReader rd = new BufferedReader(new InputStreamReader(request.getInputStream()));
            String result = "";
            String line = "";
            while ((line = rd.readLine()) != null) {
                result = result.concat(line);
            }
            JSONObject json = (JSONObject)new JSONParser().parse(result);
            ChargeLucky79Response chargelucky = new ChargeLucky79Response();
            chargelucky = this.convertToChargeMaxpay(json);
            String code = chargelucky.getCode();
            if (chargelucky != null && code.trim().equals("1") && Integer.parseInt(chargelucky.getTxn_id()) > 0) {
                return this.doReCheck(chargelucky.getTxn_id());
            }
        }
        catch (Exception exception) {
            // empty catch block
        }
        return null;
    }

    public ReCheckLucky79Response doReCheck(String transId) throws Exception {
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("merchant_txn_id", transId);
            map.put("merchant_id", this.merchantId);
            URL url = new URL(GameCommon.getValueStr("LUCKY79_URL") + "/CheckCharge?apikey=" + this.secretKey + "&id=" + transId);
            HttpURLConnection request = (HttpURLConnection)url.openConnection();
            request.setConnectTimeout(10000);
            request.setUseCaches(false);
            request.setDoOutput(true);
            request.setDoInput(true);
            HttpURLConnection.setFollowRedirects(true);
            request.setInstanceFollowRedirects(true);
            request.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            request.setRequestMethod("GET");
            BufferedReader rd = new BufferedReader(new InputStreamReader(request.getInputStream()));
            String result = "";
            String line = "";
            while ((line = rd.readLine()) != null) {
                result = result.concat(line);
            }
            JSONObject json = (JSONObject)new JSONParser().parse(result);
            return this.convertToReCheckMaxpay(json, transId);
        }
        catch (Exception ex) {
            return null;
        }
    }

    public ReCheckLucky79Response doReCheck2(String transId) throws Exception {
        URL url = new URL(GameCommon.getValueStr("LUCKY79_URL") + "/CheckCharge?apikey=" + this.secretKey + "&id=" + transId);
        HttpURLConnection request = (HttpURLConnection)url.openConnection();
        request.setConnectTimeout(10000);
        request.setUseCaches(false);
        request.setDoOutput(true);
        request.setDoInput(true);
        HttpURLConnection.setFollowRedirects(true);
        request.setInstanceFollowRedirects(true);
        request.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        request.setRequestMethod("GET");
        BufferedReader rd = new BufferedReader(new InputStreamReader(request.getInputStream()));
        String result = "";
        String line = "";
        while ((line = rd.readLine()) != null) {
            result = result.concat(line);
        }
        JSONObject json = (JSONObject)new JSONParser().parse(result);
        ReCheckLucky79Response ret = this.convertToReCheckMaxpay(json, transId);
        if (ret != null && (ret.getStatus().equals("waiting") || ret.getStatus().equals("processing"))) {
            return this.doReCheck(transId);
        }
        return null;
    }

    private ChargeLucky79Response convertToChargeMaxpay(JSONObject jData) {
        ChargeLucky79Response response = new ChargeLucky79Response();
        if (jData.get("stt") != null) {
            response.setCode(String.valueOf(jData.get("stt")));
        }
        if (jData.get("msg") != null) {
            response.setMessage(String.valueOf(jData.get("msg")));
        }
        if (jData.get("data") != null) {
            try {
                JSONObject json = (JSONObject)new JSONParser().parse(String.valueOf(jData.get("data")));
                if (json.get("id") != null) {
                    response.setTxn_id(String.valueOf(json.get("id")));
                }
            }
            catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return response;
    }

    private ReCheckLucky79Response convertToReCheckMaxpay(JSONObject jData, String transId) {
        ReCheckLucky79Response response = new ReCheckLucky79Response();
        if (jData.get("stt") != null) {
            response.setResponse_code(String.valueOf(jData.get("stt")));
        }
        if (jData.get("msg") != null) {
            response.setResponse_message(String.valueOf(jData.get("msg")));
        }
        if (jData.get("data") != null) {
            try {
                JSONObject json = (JSONObject)new JSONParser().parse(String.valueOf(jData.get("data")));
                if (json != null) {
                    response.setCard_amount(Double.parseDouble(String.valueOf(json.get("cardPrice"))));
                    if (json.get("status") != null) {
                        response.setStatus(String.valueOf(json.get("status")));
                    }
                    if (json.get("id") != null) {
                        response.setTxn_id(String.valueOf(json.get("id")));
                    }
                    if (json.get("cardSerial") != null) {
                        response.setCardSerial(String.valueOf(json.get("cardSerial")));
                    }
                    if (json.get("cardType") != null) {
                        response.setCardType(String.valueOf(json.get("cardType")));
                    }
                    if (json.get("createDate") != null) {
                        response.setDate(String.valueOf(json.get("createDate")));
                    }
                    response.setId(transId);
                }
            }
            catch (ParseException e) {
                e.printStackTrace();
            }
            if (jData.get("net_amount") != null) {
                response.setNet_amount(Double.parseDouble(String.valueOf(jData.get("net_amount"))));
            }
        }
        return response;
    }

    private String luckyProviderMaxpay(String vinplayProvider) {
        switch (vinplayProvider) {
            case "vtt": {
                return "vt";
            }
            case "vnp": {
                return "vn";
            }
            case "vms": {
                return "mb";
            }
            case "gate": {
                return "FPT";
            }
        }
        return vinplayProvider;
    }

    private boolean validateCharge(String cardType, String pin, String seri) throws Lucky79Exception {
        if (cardType == null || cardType.isEmpty()) {
            throw new Lucky79Exception("Ch\u01b0a c\u00f3 lo\u1ea1i th\u1ebb", 1);
        }
        String[] stringlistProvider = new String[]{"vt", "vn", "mb", "FPT"};
        if (!Arrays.asList(stringlistProvider).contains(cardType)) {
            throw new Lucky79Exception("Lo\u1ea1i th\u1ebb kh\u00f4ng h\u1ee3p l\u1ec7", 2);
        }
        if (pin == null || pin.isEmpty()) {
            throw new Lucky79Exception("Ch\u01b0a c\u00f3 m\u00e3 pin", 3);
        }
        if (seri == null || seri.isEmpty()) {
            throw new Lucky79Exception("Ch\u01b0a c\u00f3 serial", 4);
        }
        return true;
    }

    private String createChecksum(HashMap<String, String> args, String secretKey) {
        TreeMap abc = new TreeMap(Collections.reverseOrder());
        abc.putAll(args);
        Collection<String> allVal = abc.values();
        String tmp = "";
        for (String next : allVal) {
            tmp = "|" + next + tmp;
        }
        tmp = tmp.substring(1);
        return this.hmacSha1(tmp, secretKey);
    }

    private String hmacSha1(String value, String key) {
        try {
            byte[] keyBytes = key.getBytes();
            SecretKeySpec signingKey = new SecretKeySpec(keyBytes, "HmacSHA1");
            Mac mac = Mac.getInstance("HmacSHA1");
            mac.init(signingKey);
            byte[] rawHmac = mac.doFinal(value.getBytes());
            byte[] hexBytes = new Hex().encode(rawHmac);
            return new String(hexBytes, StandardCharsets.UTF_8);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

