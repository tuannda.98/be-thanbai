/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.gachthe;

public class MuaTheRequest {
    private String telco;
    private String code;
    private String serial;
    private long amount;
    private String request_id;
    private String partner_id;
    private String command;
    private String sign;

    public String getTelco() {
        return this.telco;
    }

    public void setTelco(String telco) {
        this.telco = telco;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSerial() {
        return this.serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public long getAmount() {
        return this.amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public String getRequest_id() {
        return this.request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    public String getPartner_id() {
        return this.partner_id;
    }

    public void setPartner_id(String partner_id) {
        this.partner_id = partner_id;
    }

    public String getCommand() {
        return this.command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getSign() {
        return this.sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}

