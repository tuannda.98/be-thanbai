/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.abtpay;

public class AbtException
extends Exception {
    private static final long serialVersionUID = 1L;

    public AbtException(String message) {
        super(message);
    }
}

