package com.vinplay.dichvuthe.dao.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.vinplay.dichvuthe.dao.CashoutDao;
import com.vinplay.dichvuthe.entities.BankAccountInfo;
import com.vinplay.dichvuthe.response.CashoutTransResponse;
import com.vinplay.dichvuthe.response.CashoutUserDailyResponse;
import com.vinplay.usercore.utils.GameCommon;
import com.vinplay.vbee.common.messages.dvt.CashoutByBankMessage;
import com.vinplay.vbee.common.messages.dvt.CashoutByCardMessage;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.pools.ConnectionPool;
import com.vinplay.vbee.common.response.cashout.BankRequest;
import com.vinplay.vbee.common.response.cashout.MomoCashout;
import com.vinplay.vbee.common.utils.VinPlayUtils;
import org.bson.Document;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

public class CashoutDaoImpl
        implements CashoutDao {
    @Override
    public long getSystemCashout() throws SQLException {
        long money = 0L;
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement("SELECT money FROM system_cashout WHERE date=?");) {
            stm.setString(1, VinPlayUtils.getCurrentDate());
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    money = rs.getLong("money");
                }
            }
        }
        return money;
    }

    @Override
    public boolean updateSystemCashout(long money) throws SQLException {
        boolean res = false;
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement("INSERT INTO system_cashout(date, money, update_time) VALUES(?, ?, now()) ON DUPLICATE KEY UPDATE money=money+?, update_time=now()");) {
            stm.setString(1, VinPlayUtils.getCurrentDate());
            stm.setLong(2, money);
            stm.setLong(3, money);
            if (stm.executeUpdate() == 1) {
                res = true;
            }
        }
        return res;
    }

    @Override
    public void logCashoutByBank(CashoutByBankMessage message) throws Exception {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("dvt_cash_out_by_bank");
        Document doc = new Document();
        doc.append("reference_id", message.getReferenceId());
        doc.append("nick_name", message.getNickname());
        doc.append("bank", message.getBank());
        doc.append("account", message.getAccount());
        doc.append("name", message.getName());
        doc.append("amount", message.getAmount());
        doc.append("status", message.getStatus());
        doc.append("message", message.getMessage());
        doc.append("sign", message.getSign());
        doc.append("code", message.getCode());
        doc.append("description", message.getDesc());
        doc.append("time_log", message.getCreateTime());
        doc.append("update_time", message.getCreateTime());
        col.insertOne(doc);
    }

    @Override
    public void requestCashoutByBank(CashoutByBankMessage message) throws Exception {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("request_cashout_bank");
        Document doc = new Document();
        doc.append("reference_id", message.getReferenceId());
        doc.append("nick_name", message.getNickname());
        doc.append("bank", message.getBank());
        doc.append("account", message.getAccount());
        doc.append("name", message.getName());
        doc.append("amount", message.getAmount());
        doc.append("status", message.getStatus());
        doc.append("message", message.getMessage());
        doc.append("description", message.getDesc());
        doc.append("time_log", message.getCreateTime());
        doc.append("update_time", message.getCreateTime());
        col.insertOne(doc);
    }

    @Override
    public void requestCashoutByCard(CashoutByCardMessage message) throws Exception {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("request_cashout_card");
        Document doc = new Document();
        doc.append("reference_id", message.getReferenceId());
        doc.append("nick_name", message.getNickname());
        doc.append("provider", message.getProvider());
        doc.append("amount", message.getAmount());
        doc.append("quantity", message.getQuantity());
        doc.append("status", message.getStatus());
        doc.append("code", message.getCode());
        doc.append("message", message.getMessage());
        doc.append("time_log", message.getCreateTime());
        doc.append("update_time", message.getCreateTime());
        col.insertOne(doc);
    }

    @Override
    public BankAccountInfo getBankAccountInfo(String nickname) throws SQLException {
        BankAccountInfo info = null;
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_admin");
             PreparedStatement stm = conn.prepareStatement("SELECT * FROM useragent WHERE nickname=? AND parentid = -1 AND active = 1");) {
            stm.setString(1, nickname);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    String bank = rs.getString("namebank");
                    String name = rs.getString("nameaccount");
                    String account = rs.getString("numberaccount");
                    info = new BankAccountInfo(bank, name, account);
                }
            }
        }
        return info;
    }

    @Override
    public CashoutUserDailyResponse getCashoutUserToday(String nickname) {
        CashoutUserDailyResponse model = new CashoutUserDailyResponse(new Date(), 0);
        try {
            MongoDatabase db = MongoDBConnectionFactory.getDB();
            MongoCollection col = db.getCollection("log_cash_out_user_daily");
            if (col != null) {
                HashMap<String, Object> conditions = new HashMap<String, Object>();
                conditions.put("nick_name", nickname);
                conditions.put("date", VinPlayUtils.getCurrentDate());
                Document doccument = (Document) col.find(new Document(conditions)).first();
                if (doccument != null) {
                    model = new CashoutUserDailyResponse(VinPlayUtils.getDateTimeFromDate(doccument.getString("date")), doccument.getInteger("money"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return model;
    }

    @Override
    public List<CashoutTransResponse> getListCashoutByCardPending(String partner, String startTime, String endTime) throws Exception {
        final ArrayList<CashoutTransResponse> response = new ArrayList<CashoutTransResponse>();
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        FindIterable iterable = null;
        Document conditions = new Document();
        conditions.put("code", 30);
        conditions.put("partner", partner);
        if (!startTime.isEmpty() && !endTime.isEmpty()) {
            BasicDBObject obj = new BasicDBObject();
            obj.put("$gte", startTime);
            obj.put("$lte", endTime);
            conditions.put("time_log", obj);
        }
        iterable = db.getCollection("dvt_cash_out_by_card").find(conditions);
        iterable.forEach(new Block<Document>() {

            public void apply(Document document) {
                CashoutTransResponse message = new CashoutTransResponse(document.getString("reference_id"), document.getString("provider"), document.getInteger("amount"), document.getInteger("quantity"), document.getString("partner"), document.getString("partner_trans_id"), document.getInteger("is_scanned"), document.getInteger("code"), document.getString("message"), document.getInteger("status"), document.getString("softpin"), document.getString("nick_name"));
                response.add(message);
            }
        });
        return response;
    }

    @Override
    public List<CashoutTransResponse> getListCashoutByCardPending() throws Exception {
        final ArrayList<CashoutTransResponse> response = new ArrayList<CashoutTransResponse>();
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        FindIterable iterable = null;
        Document conditions = new Document();
        conditions.put("code", 30);
        conditions.put("time_log", new BasicDBObject("$gte", VinPlayUtils.parseDateTimeToString(VinPlayUtils.subtractDay(new Date(), GameCommon.getValueInt("TIME_RECHECK_CASHOUT_BY_CARD")))));
        iterable = db.getCollection("dvt_cash_out_by_card").find(conditions);
        iterable.forEach(new Block<Document>() {

            public void apply(Document document) {
                CashoutTransResponse message = new CashoutTransResponse(document.getString("reference_id"), document.getString("provider"), document.getInteger("amount"), document.getInteger("quantity"), document.getString("partner"), document.getString("partner_trans_id"), document.getInteger("is_scanned"), document.getInteger("code"), document.getString("message"), document.getInteger("status"), document.getString("softpin"), document.getString("nick_name"));
                response.add(message);
            }
        });
        return response;
    }

    @Override
    public void updateCashOutByCard(String referenceId, String softpin, int status, String message, int code, int isScanned) throws Exception {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        BasicDBObject updateFields = new BasicDBObject();
        updateFields.append("softpin", softpin);
        updateFields.append("status", status);
        updateFields.append("message", message);
        updateFields.append("code", code);
        updateFields.append("is_scanned", isScanned);
        updateFields.append("update_time", VinPlayUtils.getCurrentDateTime());
        db.getCollection("dvt_cash_out_by_card").updateOne(new Document("reference_id", referenceId), new Document("$set", updateFields));
    }

    @Override
    public void insertCardIntoDB(String provider, int amount, String serial, String pin, String expire) throws Exception {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("card_store");
        Document doc = new Document();
        doc.append("reference_id", VinPlayUtils.genMessageId());
        doc.append("provider", provider);
        doc.append("amount", amount);
        doc.append("serial", serial);
        doc.append("pin", pin);
        doc.append("expire_time", expire);
        doc.append("status", 0);
        doc.append("message", "Th\u00e1\u00ba\u00bb ch\u00c6\u00b0a s\u00e1\u00bb\u00ad d\u00e1\u00bb\u00a5ng");
        doc.append("create_time", VinPlayUtils.getCurrentDateTime());
        doc.append("update_time", VinPlayUtils.getCurrentDateTime());
        doc.append("partner_trans_id", "");
        col.insertOne(doc);
    }

    @Override
    public void logCashoutByMomo(String nickname, String referenceId, String phone, String name, int amount, int status, String message, String partner, int code, String desc) throws Exception {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("dvt_cash_out_by_momo");
        Document doc = new Document();
        doc.append("reference_id", referenceId);
        doc.append("nick_name", nickname);
        doc.append("phone", phone);
        doc.append("name", name);
        doc.append("amount", amount);
        doc.append("status", status);
        doc.append("message", message);
        doc.append("code", code);
        doc.append("partner", partner);
        doc.append("description", desc);
        doc.append("time_log", VinPlayUtils.getCurrentDateTime());
        doc.append("update_time", VinPlayUtils.getCurrentDateTime());
        col.insertOne(doc);
    }

    @Override
    public long countNumberExchangeInday(String nickName) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        HashMap<String, Object> conditions = new HashMap<String, Object>();
        BasicDBObject obj = new BasicDBObject();
        SimpleDateFormat startTimeFormat = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        SimpleDateFormat endTimeFormat = new SimpleDateFormat("yyyy-MM-dd 23:59:59");
        Calendar cal = Calendar.getInstance();
        String timeStart = startTimeFormat.format(cal.getTime());
        String timeEnd = endTimeFormat.format(cal.getTime());
        if (timeStart != null && !timeStart.isEmpty() && timeEnd != null && !timeEnd.isEmpty()) {
            obj.put("$gte", timeStart);
            obj.put("$lte", timeEnd);
            conditions.put("update_time", obj);
        }
        if (nickName != null && !nickName.isEmpty()) {
            conditions.put("nick_name", nickName);
        }
        return db.getCollection("dvt_cash_out_by_momo").count(new Document(conditions)) + db.getCollection("dvt_cash_out_by_card").count(new Document(conditions));
    }

    @Override
    public List<MomoCashout> getListMomoCashout(int page, int page_size, String nick_name, int status) {
        final ArrayList<MomoCashout> response = new ArrayList<MomoCashout>();
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        HashMap<String, Object> conditions = new HashMap<String, Object>();
        BasicDBObject obj = new BasicDBObject();
        if (nick_name != null && !nick_name.isEmpty()) {
            conditions.put("nick_name", nick_name);
        }
        if (status > -1) {
            conditions.put("status", status);
        }
        BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        FindIterable iterable = db.getCollection("dvt_cash_out_by_momo").find(new Document(conditions)).sort(objsort).skip((page - 1) * page_size).limit(page_size);
        iterable.forEach(new Block<Document>() {

            public void apply(Document document) {
                MomoCashout momo = new MomoCashout();
                momo.amount = document.getInteger("amount");
                momo.code = document.getInteger("code");
                momo.description = document.getString("description");
                momo.message = document.getString("message");
                momo.name = document.getString("name");
                momo.nick_name = document.getString("nick_name");
                momo.partner = document.getString("partner");
                momo.phone = document.getString("phone");
                momo.reference_id = document.getString("reference_id");
                momo.status = document.getInteger("status");
                momo.time_log = document.getString("time_log");
                momo.update_time = document.getString("update_time");
                response.add(momo);
            }
        });
        return response;
    }

    @Override
    public long getListMomoCashoutCount(String nick_name, int status) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        HashMap conditions = new HashMap();
        BasicDBObject obj = new BasicDBObject();
        if (nick_name != null && !nick_name.isEmpty()) {
            obj.put("nick_name", nick_name);
        }
        if (status > -1) {
            obj.put("status", status);
        }
        return db.getCollection("dvt_cash_out_by_momo").count(obj);
    }

    @Override
    public List<BankRequest> getListBankRequest(int page, int page_size, String nick_name, int status) {
        final ArrayList<BankRequest> response = new ArrayList<BankRequest>();
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        HashMap<String, Object> conditions = new HashMap<String, Object>();
        BasicDBObject obj = new BasicDBObject();
        if (nick_name != null && !nick_name.isEmpty()) {
            conditions.put("nick_name", nick_name);
        }
        if (status > -1) {
            conditions.put("status", status);
        }
        BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        FindIterable iterable = db.getCollection("request_cashout_bank").find(new Document(conditions)).sort(objsort).skip((page - 1) * page_size).limit(page_size);
        iterable.forEach(new Block<Document>() {

            public void apply(Document document) {
                BankRequest momo = new BankRequest();
                momo.amount = document.getInteger("amount");
                momo.account = document.getString("account");
                momo.description = document.getString("description");
                momo.message = document.getString("message");
                momo.name = document.getString("name");
                momo.nick_name = document.getString("nick_name");
                momo.reference_id = document.getString("reference_id");
                momo.status = document.getInteger("status");
                momo.time_log = document.getString("time_log");
                momo.update_time = document.getString("update_time");
                response.add(momo);
            }
        });
        return response;
    }

    @Override
    public List<CashoutByCardMessage> getListCardRequest(int page, int page_size, String nick_name, int status) {
        final ArrayList<CashoutByCardMessage> response = new ArrayList<CashoutByCardMessage>();
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        HashMap<String, Object> conditions = new HashMap<String, Object>();
        BasicDBObject obj = new BasicDBObject();
        if (nick_name != null && !nick_name.isEmpty()) {
            conditions.put("nick_name", nick_name);
        }
        if (status > -1) {
            conditions.put("status", status);
        }
        BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        FindIterable iterable = db.getCollection("request_cashout_card").find(new Document(conditions)).sort(objsort).skip((page - 1) * page_size).limit(page_size);
        iterable.forEach(new Block<Document>() {

            public void apply(Document document) {
                CashoutByCardMessage card = new CashoutByCardMessage(document.getString("nick_name"), document.getString("reference_id"), document.getString("provider"), document.getInteger("amount"), document.getInteger("quantity"), document.getInteger("status"), "", "", "", document.getInteger("code"), "", "");
                card.setCreateTime(document.getString("updated_time"));
                response.add(card);
            }
        });
        return response;
    }

    @Override
    public long getListBankRequestCount(String nick_name, int status) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        HashMap conditions = new HashMap();
        BasicDBObject obj = new BasicDBObject();
        if (nick_name != null && !nick_name.isEmpty()) {
            obj.put("nick_name", nick_name);
        }
        if (status > -1) {
            obj.put("status", status);
        }
        return db.getCollection("request_cashout_bank").count(obj);
    }

    @Override
    public long getListCardRequestCount(String nick_name, int status) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        HashMap conditions = new HashMap();
        BasicDBObject obj = new BasicDBObject();
        if (nick_name != null && !nick_name.isEmpty()) {
            obj.put("nick_name", nick_name);
        }
        if (status > -1) {
            obj.put("status", status);
        }
        return db.getCollection("request_cashout_card").count(obj);
    }

    @Override
    public Document GetBankRequestById(String tid) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        HashMap conditions = new HashMap();
        BasicDBObject obj = new BasicDBObject();
        obj.put("reference_id", tid);
        return db.getCollection("request_cashout_bank").find(obj).first();
    }

    @Override
    public Document GetCardRequestById(String tid) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        HashMap conditions = new HashMap();
        BasicDBObject obj = new BasicDBObject();
        obj.put("reference_id", tid);
        return db.getCollection("request_cashout_card").find(obj).first();
    }

    @Override
    public boolean UpdateBankRequest(String tid, int status) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        BasicDBObject updateFields = new BasicDBObject();
        updateFields.append("status", status);
        updateFields.append("update_time", VinPlayUtils.getCurrentDateTime());
        db.getCollection("request_cashout_bank").updateOne(new Document("reference_id", tid), new Document("$set", updateFields));
        return true;
    }

    @Override
    public boolean UpdateCardRequest(String tid, int status) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        BasicDBObject updateFields = new BasicDBObject();
        updateFields.append("status", status);
        updateFields.append("update_time", VinPlayUtils.getCurrentDateTime());
        db.getCollection("request_cashout_card").updateOne(new Document("reference_id", tid), new Document("$set", updateFields));
        return true;
    }

    @Override
    public Document GetMomoRequestById(String tid) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        HashMap conditions = new HashMap();
        BasicDBObject obj = new BasicDBObject();
        obj.put("reference_id", tid);
        return db.getCollection("dvt_cash_out_by_momo").find(obj).first();
    }

    @Override
    public boolean UpdateMomoRequest(String tid, int status) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        BasicDBObject updateFields = new BasicDBObject();
        updateFields.append("status", status);
        updateFields.append("code", status);
        updateFields.append("update_time", VinPlayUtils.getCurrentDateTime());
        db.getCollection("dvt_cash_out_by_momo").updateOne(new Document("reference_id", tid), new Document("$set", updateFields));
        return true;
    }
}

