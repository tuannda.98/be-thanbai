/*
 * Decompiled with CFR 0.150.
 *
 * Could not load the following classes:
 *  org.apache.log4j.Logger
 *  org.json.simple.JSONObject
 *  org.json.simple.parser.JSONParser
 */
package com.vinplay.usercore.utils;

import com.vinplay.usercore.dao.impl.GameConfigDaoImpl;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileInputStream;
import java.util.Properties;

public class PartnerConfig {
    public static String CongGachThe = "";
    public static String CongMuaThe = "";
    public static String CongMomo = "";
    public static String CongRutMomo = "";
    public static String CongBank = "";
    public static String CongRutBank = "";
    public static String GachTheSecretKey = "";
    public static String GachTheCallBackUrl = "";
    public static String NapTienGaSecretKey = "";
    public static String NapTienGaApiKey = "";
    public static String MuaCardSecretKey = "";
    public static String MuaCardCallback = "";
    public static String ESMSApiKey = "";
    public static String ESMSSecretKey = "";
    public static String MuaCard24hPartnerId = "";
    public static String MuaCard24hPartnerKey = "";
    public static String ECardPartnerId = "";
    public static String ECardUsername = "";
    public static String Client = "";
    public static String RutCuocApiKey = "";
    public static String HostBot = "http://bot-web-service.default:6868/botteleapi";
    public static String SMSPartner = "";
    public static String Airpay24hUsername = "";
    public static String Airpay24hPassword = "";
    public static String NapTienGaApiKeyVip52 = "";
    public static String NapTienGaSecretKeyVip52 = "";
    public static String KhoTheEndpoint = "";
    public static String KhoThePartnerId = "";
    public static String KhoTheUsername = "";
    public static String KhoThePassword = "";
    public static String KhoThePrivateKey = "";
    public static String KhoThePublicKey = "";
    public static String MomoZoanPartnerName = "";
    public static String MomoZoanPartnerKey = "";
    public static String MomoZoanEndpoint = "";
    public static String ZoanCardEndpoint = "";
    public static String ZoanCardUsername = "";
    public static String ZoanCardPassword = "";
    public static String ZoanCardCallbackUrl = "";
    public static String ZoanBankGetBankEndpoint = "";
    public static String ZoanBankGetAccountEndpoint = "";
    public static String ZoanBankPartnerName = "";
    public static String ZoanBankPartkerKey = "";
    public static String ZoanMomoCashoutPartnerId = "";
    public static String ZoanMomoCashoutEndpoint = "";
    public static String ZoanMomoCashoutPublicKey = "";
    public static String ZoanMomoCashoutPrivateKey = "";
    public static String NapNhanhSecretKey = "";
    public static String NapNhanhApiKey = "";
    public static String NapTheZingApiKey = "";
    public static String NapTheZingCallback = "";
    public static String RutThe99Endpoint = "";
    public static String RutThe99Username = "";
    public static String RutThe99Password = "";
    public static String M32Endpoint = "";
    public static String M32Username = "";
    public static String M32SecretKey = "";
    public static String M32MomoEndpoint = "";
    public static String M32BankEndpoint = "";
    public static String IRONMAN_API_KEY = "";
    public static String IRONMAN_NAPTHE_CALLBACK = "";
    public static String KHOTHE_GET_BANK_URL = "";
    public static String KHOTHE_WITHDRAW_BANK_URL = "";
    private static final Logger logger = Logger.getLogger("recharge");

    public static void ReadConfig() {
        try {
            GameConfigDaoImpl dao = new GameConfigDaoImpl();
            String partnerConfig = dao.getGameCommon("partner");
            logger.debug("PartnerConfig:" + partnerConfig);
            JSONObject json = (JSONObject) new JSONParser().parse(partnerConfig);
            CongGachThe = (String) json.get("cong_gach_the");
            CongMuaThe = (String) json.get("cong_mua_the");
            CongMomo = (String) json.get("cong_momo");
            CongBank = (String) json.get("cong_bank");
            CongRutMomo = (String) json.get("cong_rut_momo");
            CongRutBank = (String) json.get("cong_rut_bank");
            GachTheSecretKey = (String) json.get("gachthe_api_key");
            GachTheCallBackUrl = (String) json.get("gachthe_callback_url");
            NapTienGaApiKey = (String) json.get("naptienga_api_key");
            NapTienGaSecretKey = (String) json.get("naptienga_secret_key");
            ESMSApiKey = (String) json.get("esm_api_key");
            ESMSSecretKey = (String) json.get("esm_secret_key");
            MuaCardSecretKey = (String) json.get("mua_card_secret_key");
            MuaCardCallback = (String) json.get("mua_card_callback");
            MuaCard24hPartnerId = (String) json.get("mua_card_24h_partner_id");
            MuaCard24hPartnerKey = (String) json.get("mua_card_24h_partner_key");
            ECardPartnerId = (String) json.get("ecard_partner_id");
            ECardUsername = (String) json.get("ecard_username");
            Client = (String) json.get("client");
            RutCuocApiKey = (String) json.get("rut_cuoc_api_key");
            HostBot = (String) json.get("host_bot");
            SMSPartner = (String) json.get("sms_partner");
            Airpay24hUsername = (String) json.get("airpay24h_username");
            Airpay24hPassword = (String) json.get("airpay24h_password");
            NapTienGaApiKeyVip52 = (String) json.get("naptienga_api_key_vip52");
            NapTienGaSecretKeyVip52 = (String) json.get("naptienga_secret_key_vip52");
            KhoTheEndpoint = (String) json.get("khothe_endpoint");
            KhoThePartnerId = (String) json.get("khothe_partner_id");
            KhoTheUsername = (String) json.get("khothe_username");
            KhoThePassword = (String) json.get("khothe_password");
            KhoThePrivateKey = (String) json.get("khothe_private_key");
            KhoThePublicKey = (String) json.get("khothe_public_key");
            MomoZoanPartnerKey = (String) json.get("momo_zoan_partner_key");
            MomoZoanPartnerName = (String) json.get("momo_zoan_partner_name");
            MomoZoanEndpoint = (String) json.get("momo_zoan_endpoint");
            ZoanCardEndpoint = (String) json.get("zoan_card_endpoint");
            ZoanCardUsername = (String) json.get("zoan_card_username");
            ZoanCardPassword = (String) json.get("zoan_card_password");
            ZoanCardCallbackUrl = (String) json.get("zoan_card_callback_url");
            ZoanBankPartkerKey = (String) json.get("zoan_bank_partner_key");
            ZoanBankPartnerName = (String) json.get("zoan_bank_partner_name");
            ZoanBankGetBankEndpoint = (String) json.get("zoan_bank_get_bank_endpoint");
            ZoanBankGetAccountEndpoint = (String) json.get("zoan_bank_get_account_endpoint");
            ZoanMomoCashoutPartnerId = (String) json.get("zoan_momo_cashout_partnerid");
            ZoanMomoCashoutEndpoint = (String) json.get("zoan_momo_cashout_endpoint");
            ZoanMomoCashoutPublicKey = (String) json.get("zoan_momo_cashout_publickey");
            ZoanMomoCashoutPrivateKey = (String) json.get("zoan_momo_cashout_privatekey");
            NapNhanhApiKey = (String) json.get("napnhanh_api_key");
            NapNhanhSecretKey = (String) json.get("napnhanh_secret_key");
            NapTheZingApiKey = (String) json.get("napthezing_api_key");
            NapTheZingCallback = (String) json.get("napthezing_callback");
            RutThe99Endpoint = (String) json.get("rutthe99_endpoint");
            RutThe99Username = (String) json.get("rutthe99_username");
            RutThe99Password = (String) json.get("rutthe99_password");
            M32Endpoint = (String) json.get("m32_endpoint");
            M32Username = (String) json.get("m32_username");
            M32SecretKey = (String) json.get("m32_secret_key");
            M32MomoEndpoint = (String) json.get("m32_momo_endpoint");
            M32BankEndpoint = (String) json.get("m32_bank_endpoint");
            if (json.containsKey("ironman_api_key")) {
                IRONMAN_API_KEY = (String) json.get("ironman_api_key");
            }
            if (json.containsKey("ironman_napthe_callback")) {
                IRONMAN_NAPTHE_CALLBACK = (String) json.get("ironman_napthe_callback");
            }
            if (json.containsKey("khothe_get_bank_url")) {
                KHOTHE_GET_BANK_URL = (String) json.get("khothe_get_bank_url");
            }
            if (json.containsKey("khothe_withdraw_bank_url")) {
                KHOTHE_WITHDRAW_BANK_URL = (String) json.get("khothe_withdraw_bank_url");
            }
        } catch (Exception exception) {
            // empty catch block
        }
    }
}

