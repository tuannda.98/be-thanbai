/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  org.json.JSONException
 */
package com.vinplay.usercore.dao;

import com.vinplay.gamebai.entities.BossXocDiaModel;
import com.vinplay.gamebai.entities.XocDiaBoss;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import org.json.JSONException;

public interface XocDiaDao {
    boolean saveRoomBoss(XocDiaBoss var1) throws SQLException;

    boolean updateRoomBoss(String var1, String var2, long var3, int var5, long var6, int var8) throws SQLException;

    XocDiaBoss getRoomBoss(String var1, int var2) throws SQLException;

    Map<Integer, XocDiaBoss> getListRoomBossActive() throws SQLException;

    List<String> getListBossActive() throws SQLException;

    List<String> getListSessionActive() throws SQLException;

    List<BossXocDiaModel> getListRoomBoss(String var1, int var2, int var3, int var4) throws SQLException, JSONException;
}

