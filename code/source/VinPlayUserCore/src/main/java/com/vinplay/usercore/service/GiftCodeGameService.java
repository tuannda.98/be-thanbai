/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.vinplay.vbee.common.response.GiftCodeGameResponse
 */
package com.vinplay.usercore.service;

import com.vinplay.vbee.common.response.GiftCodeGameResponse;
import java.util.List;

public interface GiftCodeGameService {
    boolean exportGiftCodeStore(GiftCodeGameResponse var1);

    boolean exportGiftCode(GiftCodeGameResponse var1);

    List<GiftCodeGameResponse> searchAllGiftCode(String var1, String var2, String var3, String var4, String var5, String var6, String var7, String var8, String var9, int var10, int var11);

    List<GiftCodeGameResponse> searchAllGiftCodeAdmin(String var1, String var2, String var3, String var4, String var5, int var6, int var7);

    boolean blockGiftCode(String var1, String var2);

    long countSearchAllGiftCode(String var1, String var2, String var3, String var4, String var5, String var6, String var7, String var8, String var9);

    long countSearchAllGiftCodeAdmin(String var1, String var2, String var3, String var4, String var5);
}

