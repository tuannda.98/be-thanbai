//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.vinplay.usercore.dao.impl;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import com.vinplay.usercore.dao.UserInfoDao;
import com.vinplay.usercore.entities.ExportUser;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.pools.ConnectionPool;
import com.vinplay.vbee.common.response.GetUserInfoResponse;
import com.vinplay.vbee.common.response.UserInfoResponse;
import org.bson.Document;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class UserInfoDaoImpl implements UserInfoDao {
    public UserInfoDaoImpl() {
    }

    public List<UserInfoResponse> searchUserInfo(String nickName, String ip, String startDate, String endDate, String type, int page) {
        final ArrayList<UserInfoResponse> result = new ArrayList();
        int num_start = (page - 1) * 50;
        // int num_end = true;
        BasicDBObject obj = new BasicDBObject();
        BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        HashMap<String, Object> conditions = new HashMap();
        if (nickName != null && !nickName.equals("")) {
            conditions.put("nick_name", nickName);
        }

        if (ip != null && !ip.equals("")) {
            BasicDBList or = new BasicDBList();
            or.add(new BasicDBObject("ip", ip));
            or.add(new BasicDBObject("main_ip", ip));
            conditions.put("$or", or);
        }

        if (type != null && !type.equals("")) {
            conditions.put("type", Integer.parseInt(type));
        }

        if (startDate != null && !startDate.equals("") && endDate != null && !endDate.equals("")) {
            obj.put("$gte", startDate);
            obj.put("$lte", endDate);
            conditions.put("time_log", obj);
        }

        final UserServiceImpl service = new UserServiceImpl();
        FindIterable iterable = db.getCollection("user_login_info").find(new Document(conditions)).skip(num_start).limit(50).sort(objsort);
        iterable.forEach(new Block<Document>() {
            public void apply(Document document) {
                String nn = document.getString("nick_name");
                UserInfoResponse userinfo = new UserInfoResponse();
                userinfo.user_id = document.getInteger("user_id");
                userinfo.user_name = document.getString("user_name");
                userinfo.nick_name = document.getString("nick_name");
                userinfo.ip = document.getString("ip");
                userinfo.agent = document.getString("agent");
                userinfo.type = document.getInteger("type");
                userinfo.time_log = document.getString("time_log");

                try {
                    UserModel model = service.getUserByNickName(nn);
                    userinfo.security = model.isHasMobileSecurity();
                } catch (SQLException var5) {
                    var5.printStackTrace();
                }

                result.add(userinfo);
            }
        });
        return result;
    }

    public int countsearchUserInfo(String nickName, String ip, String startDate, String endDate, String type) {
        final ArrayList result = new ArrayList();
        BasicDBObject obj = new BasicDBObject();
        BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        HashMap<String, Object> conditions = new HashMap();
        if (nickName != null && !nickName.equals("")) {
            conditions.put("nick_name", nickName);
        }

        if (ip != null && !ip.equals("")) {
            conditions.put("ip", ip);
        }

        if (type != null && !type.equals("")) {
            conditions.put("type", Integer.parseInt(type));
        }

        if (startDate != null && !startDate.equals("") && endDate != null && !endDate.equals("")) {
            obj.put("$gte", startDate);
            obj.put("$lte", endDate);
            conditions.put("time_log", obj);
        }

        FindIterable iterable = db.getCollection("user_login_info").find(new Document(conditions)).sort(objsort);
        iterable.forEach(new Block<Document>() {
            public void apply(Document document) {
                UserInfoResponse userinfo = new UserInfoResponse();
                userinfo.user_id = document.getInteger("user_id");
                userinfo.user_name = document.getString("user_name");
                userinfo.nick_name = document.getString("nick_name");
                userinfo.ip = document.getString("ip");
                userinfo.agent = document.getString("agent");
                userinfo.type = document.getInteger("type");
                userinfo.time_log = document.getString("time_log");
                result.add(userinfo);
            }
        });
        return result.size();
    }

    public GetUserInfoResponse listGetNickName(String nickName) throws SQLException {
        GetUserInfoResponse userinfo = new GetUserInfoResponse();
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement("SELECT * FROM users WHERE nick_name=?");
        ) {
            stm.setString(1, nickName);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    userinfo.nick_name = rs.getString("nick_name");
                    userinfo.user_name = rs.getString("user_name");
                    userinfo.ip = "";
                    userinfo.time_log = rs.getString("create_time");
                    userinfo.phone = rs.getString("mobile");
                } else {
                    userinfo.nick_name = nickName;
                }
            }
        }

        return userinfo;
    }

    public List<ExportUser> GetExportUser(String startDate, String endDate) throws SQLException {
        List<ExportUser> users = new ArrayList();
        String sql = "SELECT nick_name,mobile,recharge_money FROM vinplay.users where create_time >= ? and create_time <= ? and is_bot = 0 and dai_ly = 0 and mobile is not null and recharge_money > 0";
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement(sql);
        ) {
            stm.setString(1, startDate);
            stm.setString(2, endDate);
            try (ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {
                    ExportUser user = new ExportUser();
                    user.setNick_name(rs.getString("nick_name"));
                    user.setMobile(rs.getString("mobile"));
                    user.setRecharge_money(rs.getLong("recharge_money"));
                    users.add(user);
                }
            }
            return users;
        }
    }
}
