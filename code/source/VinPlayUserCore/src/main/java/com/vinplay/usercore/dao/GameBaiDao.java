/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.usercore.dao;

import java.sql.SQLException;

public interface GameBaiDao {
    String getString(String var1) throws SQLException;

    boolean saveString(String var1, String var2) throws SQLException;
}

