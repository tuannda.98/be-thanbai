/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.vinplay.vbee.common.models.cache.UserExtraInfoModel
 */
package com.vinplay.usercore.service;

import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.models.cache.UserExtraInfoModel;

public interface UserExtraService {
    UserExtraInfoModel getModelFromToken(String var1);

    String getPlatformFromToken(String var1);

    void cacheUserExtraInfo(UserCacheModel userCache, String platform);

    String getPlatformFromNickname(String nickname);
}

