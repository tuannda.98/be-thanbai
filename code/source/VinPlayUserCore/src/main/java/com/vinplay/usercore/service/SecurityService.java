/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.vinplay.vbee.common.exceptions.KeyNotFoundException
 *  com.vinplay.vbee.common.models.ConfigGame
 *  com.vinplay.vbee.common.response.BaseResponseModel
 *  com.vinplay.vbee.common.response.MoneyResponse
 */
package com.vinplay.usercore.service;

import com.vinplay.vbee.common.exceptions.KeyNotFoundException;
import com.vinplay.vbee.common.models.ConfigGame;
import com.vinplay.vbee.common.response.BaseResponseModel;
import com.vinplay.vbee.common.response.MoneyResponse;
import java.util.List;

public interface SecurityService {
    byte updateEmail(String var1, String var2);

    byte updateMobile(String var1, String var2);

    byte updateUserInfo(String var1, String var2, String var3, String var4);

    int updateUserVipInfo(String var1, String var2, String var3, String var4);

    MoneyResponse sendMoneyToSafe(String var1, long var2, boolean var4);

    MoneyResponse takeMoneyInSafe(String var1, long var2, boolean var4);

    BaseResponseModel updateAvatar(String var1, String var2);

    byte changePassword(String var1, String var2, String var3, boolean var4);

    byte activeMobile(String var1, boolean var2);

    byte activeEmail(String var1);

    String receiveActiveEmail(String var1);

    byte updateNewMobile(String var1, String var2, boolean var3);

    boolean checkMobileSecurity(String var1);

    byte loginWithOTP(String var1, long var2, byte var4);

    byte configGame(String var1, String var2);

    List<ConfigGame> getListGameBai(int var1) throws KeyNotFoundException;

    boolean updateStatusUser(String var1, int var2, String var3);

    boolean changeSecurityUser(String var1, int var2, String var3);

    boolean saveLoginInfo(int var1, String var2, String var3, String var4, String var5, int var6, String var7);
}

