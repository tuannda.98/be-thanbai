/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.vinplay.vbee.common.messages.BaseMessage
 *  com.vinplay.vbee.common.messages.LogGameMessage
 *  com.vinplay.vbee.common.rmq.RMQApi
 */
package com.vinplay.usercore.service.impl;

import com.vinplay.usercore.service.LogGameService;
import com.vinplay.vbee.common.messages.BaseMessage;
import com.vinplay.vbee.common.messages.LogGameMessage;
import com.vinplay.vbee.common.rmq.RMQApi;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class LogGameServiceImpl
implements LogGameService {
    @Override
    public boolean saveLogGameByNickName(LogGameMessage message) throws IOException, TimeoutException, InterruptedException {
        RMQApi.publishMessage("queue_log_gamebai", message, 501);
        return true;
    }

    @Override
    public boolean saveLogGameDetail(LogGameMessage message) throws IOException, TimeoutException, InterruptedException {
        RMQApi.publishMessage("queue_log_gamebai", message, 502);
        return true;
    }
}

