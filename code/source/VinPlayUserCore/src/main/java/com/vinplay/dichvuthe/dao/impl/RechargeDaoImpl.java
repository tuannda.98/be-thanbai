/*
 * Decompiled with CFR 0.150.
 *
 * Could not load the following classes:
 *  com.mongodb.BasicDBObject
 *  com.mongodb.Block
 *  com.mongodb.client.FindIterable
 *  com.mongodb.client.MongoCollection
 *  com.mongodb.client.MongoDatabase
 *  com.mongodb.client.result.UpdateResult
 *  org.bson.Document
 *  org.bson.conversions.Bson
 */
package com.vinplay.dichvuthe.dao.impl;

import casio.king365.util.KingUtil;
import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.UpdateResult;
import com.vinplay.dichvuthe.dao.RechargeDao;
import com.vinplay.dichvuthe.entities.NganLuongModel;
import com.vinplay.dichvuthe.response.*;
import com.vinplay.iap.lib.Purchase;
import com.vinplay.usercore.entities.*;
import com.vinplay.usercore.response.LogRechargeBankNLResponse;
import com.vinplay.usercore.response.LogRechargeBankNapasResponse;
import com.vinplay.usercore.utils.GameCommon;
import com.vinplay.vbee.common.exceptions.KeyNotFoundException;
import com.vinplay.vbee.common.messages.dvt.RechargeByBankMessage;
import com.vinplay.vbee.common.messages.dvt.RechargeByCardMessage;
import com.vinplay.vbee.common.messages.dvt.RechargeByCoinMessage;
import com.vinplay.vbee.common.models.BankTranferModel;
import com.vinplay.vbee.common.models.CardTranferModel;
import com.vinplay.vbee.common.models.MomoTranferModel;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.pools.ConnectionPool;
import com.vinplay.vbee.common.utils.VinPlayUtils;
import org.bson.Document;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.*;

public class RechargeDaoImpl
        implements RechargeDao {
    @Override
    public List<RechargeByCardMessage> getListCardPending(String startTime, String endTime) throws NumberFormatException, KeyNotFoundException {
        final ArrayList<RechargeByCardMessage> results = new ArrayList<RechargeByCardMessage>();
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        FindIterable iterable = null;
        Document conditions = new Document();
        conditions.put("code", 30);
        if (!startTime.isEmpty() && !endTime.isEmpty()) {
            BasicDBObject obj = new BasicDBObject();
            obj.put("$gte", startTime);
            obj.put("$lte", endTime);
            conditions.put("time_log", obj);
        }
        iterable = db.getCollection("dvt_recharge_by_card").find(conditions);
        iterable.forEach(new Block<Document>() {

            public void apply(Document document) {
                RechargeByCardMessage message = new RechargeByCardMessage(document.getString("nick_name"), document.getString("reference_id"), document.getString("provider"), document.getString("serial"), document.getString("pin"), document.getInteger("amount"), document.getInteger("status"), document.getString("message"), document.getInteger("code"), 0, document.getString("time_log"), null, document.getString("partner"), document.getString("platform"), document.getString("user_mega"));
                results.add(message);
            }
        });
        return results;
    }

    @Override
    public List<RechargeByCardMessage> getListCardPending() throws NumberFormatException, KeyNotFoundException {
        final ArrayList<RechargeByCardMessage> results = new ArrayList<RechargeByCardMessage>();
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        FindIterable iterable = null;
        Document conditions = new Document();
        conditions.put("code", 30);
        conditions.put("time_log", new BasicDBObject("$gte", VinPlayUtils.parseDateTimeToString(VinPlayUtils.subtractDay(new Date(), GameCommon.getValueInt("TIME_RECHECK_RECHARGE")))));
        iterable = db.getCollection("dvt_recharge_by_card").find(conditions);
        iterable.forEach(new Block<Document>() {

            public void apply(Document document) {
                RechargeByCardMessage message = new RechargeByCardMessage(document.getString("nick_name"), document.getString("reference_id"), document.getString("provider"), document.getString("serial"), document.getString("pin"), document.getInteger("amount"), document.getInteger("status"), document.getString("message"), document.getInteger("code"), 0, document.getString("time_log"), null, document.getString("partner"), document.getString("platform"), document.getString("user_mega"));
                results.add(message);
            }
        });
        return results;
    }

    @Override
    public boolean updateSMS(String requestId, int code, String des, int smsType) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = null;
        col = smsType == 2 ? db.getCollection("dvt_recharge_by_sms_plus") : db.getCollection("dvt_recharge_by_sms");
        BasicDBObject updateFields = new BasicDBObject();
        updateFields.append("code", code);
        updateFields.append("description", des);
        col.updateOne(new Document("request_id", requestId), new Document("$set", updateFields));
        return true;
    }

    @Override
    public RechargeByCardMessage getPendingCardByReferenceId(String referenceId) {
        RechargeByCardMessage result = null;
        Document conditions = new Document();
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        conditions.put("reference_id", referenceId);
        Document dc = db.getCollection("dvt_recharge_by_card").find(conditions).first();
        if (dc != null) {
            result = new RechargeByCardMessage(dc.getString("nick_name"), dc.getString("reference_id"), dc.getString("provider"), dc.getString("serial"), dc.getString("pin"), dc.getInteger("amount"), dc.getInteger("status"), dc.getString("message"), dc.getInteger("code"), 0, dc.getString("time_log"), null, dc.getString("partner"), dc.getString("platform"), dc.getString("user_mega"));
        }
        return result;
    }

    @Override
    public boolean insertLogUpdateCardPending(String reference_id, String nick_name, String provider, String serial, String pin, String amount, String status, String message, String code, String time_log, String money, String actor) throws Exception {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("log_update_card_pending");
        Document doc = new Document();
        doc.append("reference_id", reference_id);
        doc.append("nick_name", nick_name);
        doc.append("provider", provider);
        doc.append("serial", serial);
        doc.append("pin", pin);
        doc.append("amount", amount);
        doc.append("status", status);
        doc.append("message", message);
        doc.append("code", code);
        doc.append("time_log", time_log);
        doc.append("plus_money_log", VinPlayUtils.getCurrentDateTime());
        doc.append("money", money);
        doc.append("actor", actor);
        col.insertOne(doc);
        return true;
    }

    @Override
    public boolean updateCard(String id, int amount, int status, String message, int code) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        BasicDBObject updateFields = new BasicDBObject();
        updateFields.append("amount", amount);
        updateFields.append("status", status);
        updateFields.append("message", message);
        updateFields.append("code", code);
        updateFields.append("update_time", VinPlayUtils.getCurrentDateTime());
        db.getCollection("dvt_recharge_by_card").updateOne(new Document("reference_id", id), new Document("$set", updateFields));
        return true;
    }

    @Override
    public RechargeByBankMessage getRechargeByBank(String transId) {
        RechargeByBankMessage result = null;
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        Document conditions = new Document();
        conditions.put("trans_id", transId);
        Document dc = db.getCollection("dvt_recharge_by_bank").find(conditions).first();
        if (dc != null) {
            result = new RechargeByBankMessage(dc.getString("nick_name"), dc.getLong("money"), dc.getString("bank"), transId, dc.getInteger("amount"), dc.getString("order_info"), dc.getString("ticket_no"));
        }
        return result;
    }

    @Override
    public boolean logRechargeByBank(RechargeByBankMessage message, String client, String platform) throws Exception {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("dvt_recharge_by_bank");
        Document doc = new Document();
        doc.append("nick_name", message.getNickname());
        doc.append("money", message.getMoney());
        doc.append("bank", message.getBank());
        doc.append("trans_id", message.getTransId());
        doc.append("amount", message.getAmount());
        doc.append("add_money", message.getAddMoney());
        doc.append("order_info", message.getOrderInfo());
        doc.append("ticket_no", message.getTicketNo());
        doc.append("trans_time", message.getCreateTime());
        doc.append("create_time", new Date());
        doc.append("time_log", VinPlayUtils.getCurrentDateTime());
        doc.append("time_request", System.currentTimeMillis() + "");
        doc.append("provider", "BANK");
        doc.append("response_code", "");
        doc.append("description", "");
        doc.append("amount_receive", "");
        doc.append("transaction_no", "");
        doc.append("message", "");
        doc.append("update_time", "");
        doc.append("client", client);
        doc.append("platform", platform);
        col.insertOne(doc);
        return true;
    }

    @Override
    public boolean logRechargeByCoin(RechargeByCoinMessage message, String client, String platform) throws Exception {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("dvt_recharge_by_coin");
        Document doc = new Document();
        doc.append("wallet_code", message.getWalletCode());
        doc.append("address", message.getAddress());
        doc.append("from_address", message.getFromAddress());
        doc.append("amount", message.getAmount());
        doc.append("token", message.getToken());
        doc.append("token_type", message.getTokenType());
        doc.append("trx_id", message.getTrxId());
        doc.append("status", message.getStatus());
        doc.append("trans_time", message.getTransTime());
        doc.append("create_time", new Date());
        doc.append("time_log", VinPlayUtils.getCurrentDateTime());
        doc.append("time_request", System.currentTimeMillis() + "");
        doc.append("provider", "COIN");
        doc.append("client", client);
        doc.append("platform", platform);
        col.insertOne(doc);
        return true;
    }

    @Override
    public boolean updateRechargeByBank(String transId, String responseCode, String description, String transactionNo, String message, String amountReceive) throws Exception {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        BasicDBObject updateFields = new BasicDBObject();
        updateFields.append("response_code", responseCode);
        updateFields.append("description", description);
        updateFields.append("transaction_no", transactionNo);
        updateFields.append("message", message);
        updateFields.append("amount_receive", amountReceive);
        updateFields.append("update_time", VinPlayUtils.getCurrentDateTime());
        db.getCollection("dvt_recharge_by_bank").updateOne(new Document("trans_id", transId), new Document("$set", updateFields));
        return true;
    }

    @Override
    public boolean insertLogRechargeByBankError(String txnResponseCode, String version, String command, String merchTxnRef, String merchantID, String orderInfo, String currency, int amount, String locale, String cardType, String transactionNo, String message, String secureHash, String description) throws Exception {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("dvt_recharge_by_bank_error");
        Document doc = new Document();
        doc.append("trans_id", merchTxnRef);
        doc.append("response_code", txnResponseCode);
        doc.append("version", version);
        doc.append("command", command);
        doc.append("merchant_id", merchantID);
        doc.append("order_info", orderInfo);
        doc.append("currency_code", currency);
        doc.append("amount", amount);
        doc.append("locale", locale);
        doc.append("card_type", cardType);
        doc.append("transaction_no", transactionNo);
        doc.append("message", message);
        doc.append("secure_hash", secureHash);
        doc.append("description", description);
        doc.append("trans_time", VinPlayUtils.getCurrentDateTime());
        col.insertOne(doc);
        return true;
    }

    @Override
    public boolean logRechargeByNL(String nickname, String email, String mobile, String ip, String orderCode, int totalAmount, int taxAmount, int discountAmount, int feeShipping, String paymentMethod, String bank, String errorCodeSend, String descVP, String token, String checkoutUrl, String timeLimit, String descNL) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("ngan_luong_transaction");
        Document doc = new Document();
        doc.append("nick_name", nickname);
        doc.append("email", email);
        doc.append("mobile", mobile);
        doc.append("ip", ip);
        doc.append("order_code", orderCode);
        doc.append("total_amount", totalAmount);
        doc.append("tax_amount", taxAmount);
        doc.append("discount_amount", discountAmount);
        doc.append("fee_shipping", feeShipping);
        doc.append("payment_method", paymentMethod);
        doc.append("bank", bank);
        doc.append("error_code_send", errorCodeSend);
        doc.append("desc_vp", descVP);
        doc.append("token", token);
        doc.append("checkout_url", checkoutUrl);
        doc.append("time_limit", timeLimit);
        doc.append("desc_nl", descNL);
        doc.append("trans_time", VinPlayUtils.getCurrentDateTime());
        doc.append("error_code_return", "");
        doc.append("desc_return", "");
        doc.append("update_time", "");
        col.insertOne(doc);
        return true;
    }

    @Override
    public boolean updateRechargeByNL(String token, String errorCodeReturn, String descReturn) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        BasicDBObject updateFields = new BasicDBObject();
        updateFields.append("error_code_return", errorCodeReturn);
        updateFields.append("desc_return", descReturn);
        updateFields.append("update_time", VinPlayUtils.getCurrentDateTime());
        UpdateResult result = db.getCollection("ngan_luong_transaction").updateOne(new Document("token", token), new Document("$set", updateFields));
        return result.isModifiedCountAvailable();
    }

    @Override
    public boolean logRechargeByNLError(String token, String errorCodeReturn, String descReturn) {
        return true;
    }

    @Override
    public NganLuongModel getNLTrans(String token) {
        NganLuongModel result = null;
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        Document conditions = new Document();
        conditions.put("token", token);
        ArrayList<String> listErr = new ArrayList<String>();
        listErr.add("");
        listErr.add("81");
        conditions.put("error_code_return", new BasicDBObject("$in", listErr));
        Document dc = db.getCollection("ngan_luong_transaction").find(conditions).first();
        if (dc != null) {
            result = new NganLuongModel(dc.getString("nick_name"), dc.getInteger("total_amount"), dc.getString("order_code"), dc.getString("bank"));
        }
        return result;
    }

    @Override
    public LogRechargeBankNapasResponse getLogNapas(String nickname, String bank, String transId, String ip, String transNo, String status, String startTime, String endTime, int page) {
        String pattern;
        final ArrayList<LogRechargeBankNapas> records = new ArrayList<LogRechargeBankNapas>();
        final ArrayList<Long> num = new ArrayList<Long>();
        num.add(0, 0L);
        num.add(1, 0L);
        num.add(2, 0L);
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        int numStart = (page - 1) * 50;
        int numEnd = 50;
        BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        HashMap<String, Object> conditions = new HashMap<String, Object>();
        if (!nickname.isEmpty()) {
            pattern = ".*" + nickname + ".*";
            conditions.put("nick_name", new BasicDBObject().append("$regex", pattern).append("$options", "i"));
        }
        if (!bank.isEmpty()) {
            conditions.put("bank", bank);
        }
        if (!transId.isEmpty()) {
            pattern = ".*" + transId + ".*";
            conditions.put("trans_id", new BasicDBObject().append("$regex", pattern).append("$options", "i"));
        }
        if (!ip.isEmpty()) {
            conditions.put("ticket_no", ip);
        }
        if (!transNo.isEmpty()) {
            pattern = ".*" + transNo + ".*";
            conditions.put("transaction_no", new BasicDBObject().append("$regex", pattern).append("$options", "i"));
        }
        if (!status.isEmpty()) {
            conditions.put("response_code", status);
        }
        if (!startTime.isEmpty() && !endTime.isEmpty()) {
            BasicDBObject obj = new BasicDBObject();
            obj.put("$gte", startTime);
            obj.put("$lte", endTime);
            conditions.put("trans_time", obj);
        }
        FindIterable iterable = db.getCollection("dvt_recharge_by_bank").find(new Document(conditions)).sort(objsort).skip(numStart).limit(50);
        iterable.forEach(new Block<Document>() {

            public void apply(Document document) {
                LogRechargeBankNapas model = new LogRechargeBankNapas();
                model.nickname = document.getString("nick_name");
                model.money = document.getLong("money");
                model.bank = document.getString("bank");
                model.transId = document.getString("trans_id");
                model.ticketNo = document.getString("ticket_no");
                model.transTime = document.getString("trans_time");
                model.responseCode = document.getString("response_code");
                model.description = document.getString("description");
                model.transactionNo = document.getString("transaction_no");
                model.updateTime = document.getString("update_time");
                records.add(model);
            }
        });
        FindIterable iterable2 = db.getCollection("dvt_recharge_by_bank").find(new Document(conditions));
        iterable2.forEach(new Block<Document>() {

            public void apply(Document document) {
                LogRechargeBankNapas model = new LogRechargeBankNapas();
                model.money = document.getLong("money");
                model.responseCode = document.getString("response_code");
                long count = num.get(0) + 1L;
                num.set(0, count);
                if (model.responseCode.equals("0")) {
                    long numSuccess = num.get(1) + 1L;
                    num.set(1, numSuccess);
                    long moneySuccess = num.get(2) + model.money;
                    num.set(2, moneySuccess);
                }
            }
        });
        LogRechargeBankNapasResponse res = new LogRechargeBankNapasResponse(true, "0", num.get(0) / 50L + 1L, num.get(1), num.get(2), records);
        return res;
    }

    @Override
    public LogRechargeBankNLResponse getLogNL(String nickname, String bank, String transId, String ip, String transNo, String status, String startTime, String endTime, int page) {
        String pattern;
        final ArrayList<LogRechargeBankNL> records = new ArrayList<LogRechargeBankNL>();
        final ArrayList<Long> num = new ArrayList<Long>();
        num.add(0, 0L);
        num.add(1, 0L);
        num.add(2, 0L);
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        int numStart = (page - 1) * 50;
        int numEnd = 50;
        BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        HashMap<String, Object> conditions = new HashMap<String, Object>();
        if (!nickname.isEmpty()) {
            pattern = ".*" + nickname + ".*";
            conditions.put("nick_name", new BasicDBObject().append("$regex", pattern).append("$options", "i"));
        }
        if (!bank.isEmpty()) {
            conditions.put("bank", bank);
        }
        if (!transId.isEmpty()) {
            pattern = ".*" + transId + ".*";
            conditions.put("order_code", new BasicDBObject().append("$regex", pattern).append("$options", "i"));
        }
        if (!ip.isEmpty()) {
            conditions.put("ip", ip);
        }
        if (!transNo.isEmpty()) {
            pattern = ".*" + transNo + ".*";
            conditions.put("token", new BasicDBObject().append("$regex", pattern).append("$options", "i"));
        }
        if (!status.isEmpty()) {
            conditions.put("error_code_return", status);
        }
        if (!startTime.isEmpty() && !endTime.isEmpty()) {
            BasicDBObject obj = new BasicDBObject();
            obj.put("$gte", startTime);
            obj.put("$lte", endTime);
            conditions.put("trans_time", obj);
        }
        FindIterable iterable = db.getCollection("ngan_luong_transaction").find(new Document(conditions)).sort(objsort).skip(numStart).limit(50);
        iterable.forEach(new Block<Document>() {

            public void apply(Document document) {
                LogRechargeBankNL model = new LogRechargeBankNL();
                model.nickname = document.getString("nick_name");
                model.email = document.getString("email");
                model.mobile = document.getString("mobile");
                model.ip = document.getString("ip");
                model.orderCode = document.getString("order_code");
                model.totalAmount = document.getInteger("total_amount");
                model.bank = document.getString("bank");
                model.token = document.getString("token");
                model.transTime = document.getString("trans_time");
                model.errorCodeReturn = document.getString("error_code_return");
                model.descReturn = document.getString("desc_return");
                model.updateTime = document.getString("update_time");
                records.add(model);
            }
        });
        FindIterable iterable2 = db.getCollection("ngan_luong_transaction").find(new Document(conditions));
        iterable2.forEach(new Block<Document>() {

            public void apply(Document document) {
                LogRechargeBankNL model = new LogRechargeBankNL();
                model.totalAmount = document.getInteger("total_amount");
                model.errorCodeReturn = document.getString("error_code_return");
                long count = num.get(0) + 1L;
                num.set(0, count);
                if (model.errorCodeReturn.equals("00")) {
                    long numSuccess = num.get(1) + 1L;
                    num.set(1, numSuccess);
                    long moneySuccess = num.get(2) + (long) model.totalAmount;
                    num.set(2, moneySuccess);
                }
            }
        });
        LogRechargeBankNLResponse res = new LogRechargeBankNLResponse(true, "0", num.get(0) / 50L + 1L, num.get(1), num.get(2), records);
        return res;
    }

    @Override
    public boolean saveLogIAP(Purchase pc, String nickname, int amount, int code, String des) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("dvt_recharge_by_iap");
        Document doc = new Document();
        doc.append("nick_name", nickname);
        doc.append("amount", amount);
        doc.append("code", code);
        doc.append("description", des);
        doc.append("trans_time", VinPlayUtils.getCurrentDateTime());
        doc.append("order_id", pc.getOrderId());
        doc.append("package_name", pc.getPackageName());
        doc.append("product_id", pc.getSku());
        doc.append("purchase_time", pc.getPurchaseTime());
        doc.append("purchase_state", pc.getPurchaseState());
        doc.append("developer_payload", pc.getDeveloperPayload());
        doc.append("token", pc.getToken());
        doc.append("signature", pc.getSignature());
        col.insertOne(doc);
        return true;
    }

    @Override
    public boolean checkOrderId(String orderId) {
        boolean res = false;
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        Document conditions = new Document();
        conditions.put("order_id", orderId);
        conditions.put("code", 0);
        Document dc = db.getCollection("dvt_recharge_by_iap").find(conditions).first();
        if (dc != null) {
            res = true;
        }
        return res;
    }

    @Override
    public long getTotalRechargeIapInday(String nickname, Calendar cal) throws ParseException {
        long res = 0L;
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        BasicDBObject obj = new BasicDBObject();
        Document conditions = new Document();
        if (nickname != null && !nickname.equals("")) {
            conditions.put("nick_name", nickname);
        }
        conditions.put("code", 0);
        String timeStart = VinPlayUtils.getDateTimeStr(VinPlayUtils.getDateTimeFromDate(VinPlayUtils.getDateFromDateTime(VinPlayUtils.getDateTimeStr(cal.getTime()))));
        cal.add(5, 1);
        String timeEnd = VinPlayUtils.getDateTimeStr(VinPlayUtils.getDateTimeFromDate(VinPlayUtils.getDateFromDateTime(VinPlayUtils.getDateTimeStr(cal.getTime()))));
        obj.put("$gte", timeStart);
        obj.put("$lt", timeEnd);
        conditions.put("trans_time", obj);
        Document dc = db.getCollection("dvt_recharge_by_iap").aggregate(Arrays.asList(new Document("$match", conditions), new Document("$group", new Document("_id", null).append("money", new Document("$sum", "$amount"))))).first();
        if (dc != null) {
            res = dc.getInteger("money").intValue();
        }
        return res;
    }

    @Override
    public boolean checkRequestIdSMS(String requestId) {
        boolean res = false;
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        Document conditions = new Document();
        conditions.put("request_id", requestId);
        Document dc = db.getCollection("dvt_recharge_by_sms").find(conditions).first();
        if (dc != null) {
            res = true;
        }
        return res;
    }

    @Override
    public boolean checkRequestIdSMSPlus(String requestId) {
        boolean res = false;
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        Document conditions = new Document();
        conditions.put("request_id", requestId);
        Document dc = db.getCollection("dvt_recharge_by_sms_plus").find(conditions).first();
        if (dc != null) {
            res = true;
        }
        return res;
    }

    @Override
    public boolean saveLogRechargeBySMS(String nickname, String mobile, String moMessage, int amount, String shortCode, String requestId, String requestTime, int code, String des, int money) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("dvt_recharge_by_sms");
        Document doc = new Document();
        doc.append("nick_name", nickname);
        doc.append("mobile", mobile);
        doc.append("message_MO", moMessage);
        doc.append("amount", amount);
        doc.append("money", money);
        doc.append("short_code", shortCode);
        doc.append("request_id", requestId);
        doc.append("time_request", requestTime);
        doc.append("code", code);
        doc.append("description", des);
        doc.append("time_log", VinPlayUtils.getCurrentDateTime());
        doc.append("create_time", new Date());
        col.insertOne(doc);
        return true;
    }

    @Override
    public boolean saveLogRechargeBySMSPlus(String nickname, String mobile, String moMessage, int amount, String shortCode, String errorCode, String errorMessage, String requestId, String requestTime, int code, String des, int money) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("dvt_recharge_by_sms_plus");
        Document doc = new Document();
        doc.append("nick_name", nickname);
        doc.append("mobile", mobile);
        doc.append("message_MO", moMessage);
        doc.append("amount", amount);
        doc.append("money", money);
        doc.append("short_code", shortCode);
        doc.append("error_code", errorCode);
        doc.append("error_message", errorMessage);
        doc.append("request_id", requestId);
        doc.append("time_request", requestTime);
        doc.append("code", code);
        doc.append("description", des);
        doc.append("time_log", VinPlayUtils.getCurrentDateTime());
        doc.append("create_time", new Date());
        col.insertOne(doc);
        return true;
    }

    @Override
    public boolean saveLogRechargeBySMSPlusCheckMO(String mobile, String moMessage, int amount, String shortCode, int code, String des) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("dvt_recharge_by_sms_plus_check_mo");
        Document doc = new Document();
        doc.append("mobile", mobile);
        doc.append("message_MO", moMessage);
        doc.append("amount", amount);
        doc.append("short_code", shortCode);
        doc.append("code", code);
        doc.append("description", des);
        doc.append("time_log", VinPlayUtils.getCurrentDateTime());
        doc.append("create_time", new Date());
        col.insertOne(doc);
        return true;
    }

    @Override
    public LogSMS8x98Response getLogSMS8x98(String nickname, String mobile, int amount, String shortCode, String requestId, int code, String startTime, String endTime, int page) {
        final ArrayList<LogSMS8x98> records = new ArrayList<LogSMS8x98>();
        final ArrayList<Long> num = new ArrayList<Long>();
        num.add(0, 0L);
        num.add(1, 0L);
        num.add(2, 0L);
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("dvt_recharge_by_sms");
        int numStart = (page - 1) * 50;
        int numEnd = 50;
        BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        HashMap<String, Object> conditions = new HashMap<String, Object>();
        if (!nickname.isEmpty()) {
            String pattern = ".*" + nickname + ".*";
            conditions.put("nick_name", new BasicDBObject().append("$regex", pattern).append("$options", "i"));
        }
        if (!mobile.isEmpty()) {
            conditions.put("mobile", mobile);
        }
        if (amount >= 0) {
            conditions.put("amount", amount);
        }
        if (!shortCode.isEmpty()) {
            conditions.put("short_code", shortCode);
        }
        if (!requestId.isEmpty()) {
            conditions.put("request_id", requestId);
        }
        if (code >= 0) {
            conditions.put("code", code);
        }
        if (!startTime.isEmpty() && !endTime.isEmpty()) {
            BasicDBObject obj = new BasicDBObject();
            obj.put("$gte", startTime);
            obj.put("$lte", endTime);
            conditions.put("time_log", obj);
        }
        FindIterable iterable = col.find(new Document(conditions)).sort(objsort).skip(numStart).limit(50);
        iterable.forEach(new Block<Document>() {

            public void apply(Document document) {
                String nickname = document.getString("nick_name");
                String mobile = document.getString("mobile");
                String moMessage = document.getString("message_MO");
                int amount = document.getInteger("amount");
                String shortCode = document.getString("short_code");
                String requestId = document.getString("request_id");
                String requestTime = document.getString("time_request");
                int code = document.getInteger("code");
                String des = document.getString("description");
                int money = document.getInteger("money");
                String timeLog = document.getString("time_log");
                LogSMS8x98 model = new LogSMS8x98(nickname, mobile, moMessage, amount, shortCode, requestId, requestTime, code, des, money, timeLog);
                records.add(model);
            }
        });
        FindIterable iterable2 = col.find(new Document(conditions));
        iterable2.forEach(new Block<Document>() {

            public void apply(Document document) {
                int amount = document.getInteger("amount");
                int code = document.getInteger("code");
                long count = num.get(0) + 1L;
                num.set(0, count);
                if (code == 0) {
                    long numSuccess = num.get(1) + 1L;
                    num.set(1, numSuccess);
                    long moneySuccess = num.get(2) + (long) amount;
                    num.set(2, moneySuccess);
                }
            }
        });
        LogSMS8x98Response res = new LogSMS8x98Response(true, "0", num.get(0) / 50L + 1L, num.get(1), num.get(2), records);
        return res;
    }

    @Override
    public LogSMSPlusResponse getLogSMSPlus(String nickname, String mobile, int amount, String requestId, int code, String startTime, String endTime, int page) {
        final ArrayList<LogSMSPlus> records = new ArrayList<LogSMSPlus>();
        final ArrayList<Long> num = new ArrayList<Long>();
        num.add(0, 0L);
        num.add(1, 0L);
        num.add(2, 0L);
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("dvt_recharge_by_sms_plus");
        int numStart = (page - 1) * 50;
        int numEnd = 50;
        BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        HashMap<String, Object> conditions = new HashMap<String, Object>();
        if (!nickname.isEmpty()) {
            String pattern = ".*" + nickname + ".*";
            conditions.put("nick_name", new BasicDBObject().append("$regex", pattern).append("$options", "i"));
        }
        if (!mobile.isEmpty()) {
            conditions.put("mobile", mobile);
        }
        if (amount >= 0) {
            conditions.put("amount", amount);
        }
        if (!requestId.isEmpty()) {
            conditions.put("request_id", requestId);
        }
        if (code >= 0) {
            conditions.put("code", code);
        }
        if (!startTime.isEmpty() && !endTime.isEmpty()) {
            BasicDBObject obj = new BasicDBObject();
            obj.put("$gte", startTime);
            obj.put("$lte", endTime);
            conditions.put("time_log", obj);
        }
        FindIterable iterable = col.find(new Document(conditions)).sort(objsort).skip(numStart).limit(50);
        iterable.forEach(new Block<Document>() {

            public void apply(Document document) {
                String nickname = document.getString("nick_name");
                String mobile = document.getString("mobile");
                String moMessage = document.getString("message_MO");
                int amount = document.getInteger("amount");
                String shortCode = document.getString("short_code");
                String errorCode = document.getString("error_code");
                String errorMessage = document.getString("error_message");
                String requestId = document.getString("request_id");
                String requestTime = document.getString("time_request");
                int code = document.getInteger("code");
                String des = document.getString("description");
                int money = document.getInteger("money");
                String timeLog = document.getString("time_log");
                LogSMSPlus model = new LogSMSPlus(nickname, mobile, moMessage, amount, shortCode, errorCode, errorMessage, requestId, requestTime, code, des, money, timeLog);
                records.add(model);
            }
        });
        FindIterable iterable2 = col.find(new Document(conditions));
        iterable2.forEach(new Block<Document>() {

            public void apply(Document document) {
                int amount = document.getInteger("amount");
                int code = document.getInteger("code");
                long count = num.get(0) + 1L;
                num.set(0, count);
                if (code == 0) {
                    long numSuccess = num.get(1) + 1L;
                    num.set(1, numSuccess);
                    long moneySuccess = num.get(2) + (long) amount;
                    num.set(2, moneySuccess);
                }
            }
        });
        LogSMSPlusResponse res = new LogSMSPlusResponse(true, "0", num.get(0) / 50L + 1L, num.get(1), num.get(2), records);
        return res;
    }

    @Override
    public List<String> getListSmsIdNearly() {
        final ArrayList<String> res = new ArrayList<String>();
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("dvt_recharge_by_sms");
        BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        FindIterable iterable = col.find().sort(objsort).skip(0).limit(1000);
        iterable.forEach(new Block<Document>() {

            public void apply(Document document) {
                res.add(document.getString("request_id"));
            }
        });
        return res;
    }

    @Override
    public List<String> getListSmsPlusIdNearly() {
        final ArrayList<String> res = new ArrayList<String>();
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("dvt_recharge_by_sms_plus");
        BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        FindIterable iterable = col.find().sort(objsort).skip(0).limit(1000);
        iterable.forEach(new Block<Document>() {

            public void apply(Document document) {
                res.add(document.getString("request_id"));
            }
        });
        return res;
    }

    @Override
    public LogSMSPlusCheckMoResponse getLogSMSPlusCheckMO(String mobile, int amount, int code, String startTime, String endTime, int page) {
        final ArrayList<LogSMSPlusCheckMO> records = new ArrayList<LogSMSPlusCheckMO>();
        final ArrayList<Long> num = new ArrayList<Long>();
        num.add(0, 0L);
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("dvt_recharge_by_sms_plus_check_mo");
        int numStart = (page - 1) * 50;
        int numEnd = 50;
        BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        HashMap<String, Object> conditions = new HashMap<String, Object>();
        if (!mobile.isEmpty()) {
            conditions.put("mobile", mobile);
        }
        if (amount >= 0) {
            conditions.put("amount", amount);
        }
        if (code >= 0) {
            conditions.put("code", code);
        }
        if (!startTime.isEmpty() && !endTime.isEmpty()) {
            BasicDBObject obj = new BasicDBObject();
            obj.put("$gte", startTime);
            obj.put("$lte", endTime);
            conditions.put("time_log", obj);
        }
        FindIterable iterable = col.find(new Document(conditions)).sort(objsort).skip(numStart).limit(numEnd);
        iterable.forEach(new Block<Document>() {

            public void apply(Document document) {
                String mobile = document.getString("mobile");
                String moMessage = document.getString("message_MO");
                int amount = document.getInteger("amount");
                String shortCode = document.getString("short_code");
                int code = document.getInteger("code");
                String des = document.getString("description");
                String timeLog = document.getString("time_log");
                LogSMSPlusCheckMO model = new LogSMSPlusCheckMO(mobile, moMessage, amount, shortCode, code, des, timeLog);
                records.add(model);
                long count = num.get(0) + 1L;
                num.set(0, count);
            }
        });
        LogSMSPlusCheckMoResponse res = new LogSMSPlusCheckMoResponse(true, "0", num.get(0) / 50L + 1L, records);
        return res;
    }

    @Override
    public boolean saveLogRequestApiOTP(String nickname, String mobile, int amount, String errorCode, String errorMessage, String requestId, String transId, int code, String redirectURL) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("dvt_recharge_by_otp_request");
        Document doc = new Document();
        doc.append("nick_name", nickname);
        doc.append("mobile", mobile);
        doc.append("amount", amount);
        doc.append("error_code", errorCode);
        doc.append("error_message", errorMessage);
        doc.append("request_id", requestId);
        doc.append("trans_id", transId);
        doc.append("code", code);
        doc.append("redirect_url", redirectURL);
        doc.append("time_log", VinPlayUtils.getCurrentDateTime());
        doc.append("create_time", new Date());
        col.insertOne(doc);
        return true;
    }

    @Override
    public boolean saveLogConfirmApiOTP(String nickname, String mobile, int amount, String otp, String errorCode, String errorMessage, String requestId, String transId, int code, String des, int money) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("dvt_recharge_by_otp_confirm");
        Document doc = new Document();
        doc.append("nick_name", nickname);
        doc.append("mobile", mobile);
        doc.append("amount", amount);
        doc.append("otp", otp);
        doc.append("error_code", errorCode);
        doc.append("error_message", errorMessage);
        doc.append("request_id", requestId);
        doc.append("trans_id", transId);
        doc.append("code", code);
        doc.append("description", des);
        doc.append("money", money);
        doc.append("time_log", VinPlayUtils.getCurrentDateTime());
        doc.append("create_time", new Date());
        col.insertOne(doc);
        return true;
    }

    @Override
    public LogApiOtpConfirmResponse getApiOtpConfirm(String nickname, String mobile, int amount, String requestId, int code, String startTime, String endTime, int page) {
        final ArrayList<LogApiOtpConfirm> records = new ArrayList<LogApiOtpConfirm>();
        final ArrayList<Long> num = new ArrayList<Long>();
        num.add(0, 0L);
        num.add(1, 0L);
        num.add(2, 0L);
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("dvt_recharge_by_otp_confirm");
        int numStart = (page - 1) * 50;
        int numEnd = 50;
        BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        HashMap<String, Object> conditions = new HashMap<String, Object>();
        if (!nickname.isEmpty()) {
            String pattern = ".*" + nickname + ".*";
            conditions.put("nick_name", new BasicDBObject().append("$regex", pattern).append("$options", "i"));
        }
        if (!mobile.isEmpty()) {
            conditions.put("mobile", mobile);
        }
        if (amount >= 0) {
            conditions.put("amount", amount);
        }
        if (!requestId.isEmpty()) {
            conditions.put("request_id", requestId);
        }
        if (code >= 0) {
            conditions.put("code", code);
        }
        if (!startTime.isEmpty() && !endTime.isEmpty()) {
            BasicDBObject obj = new BasicDBObject();
            obj.put("$gte", startTime);
            obj.put("$lte", endTime);
            conditions.put("time_log", obj);
        }
        FindIterable iterable = col.find(new Document(conditions)).sort(objsort).skip(numStart).limit(50);
        iterable.forEach(new Block<Document>() {

            public void apply(Document document) {
                String nickname = document.getString("nick_name");
                String mobile = document.getString("mobile");
                int amount = document.getInteger("amount");
                String otp = document.getString("otp");
                String errorCode = document.getString("error_code");
                String errorMessage = document.getString("error_message");
                String requestId = document.getString("request_id");
                String transId = document.getString("trans_id");
                int code = document.getInteger("code");
                String des = document.getString("description");
                int money = document.getInteger("money");
                String timeLog = document.getString("time_log");
                LogApiOtpConfirm model = new LogApiOtpConfirm(nickname, mobile, amount, otp, errorCode, errorMessage, requestId, transId, code, des, money, timeLog);
                records.add(model);
            }
        });
        FindIterable iterable2 = col.find(new Document(conditions));
        iterable2.forEach(new Block<Document>() {

            public void apply(Document document) {
                int amount = document.getInteger("amount");
                int code = document.getInteger("code");
                long count = num.get(0) + 1L;
                num.set(0, count);
                if (code == 0) {
                    long numSuccess = num.get(1) + 1L;
                    num.set(1, numSuccess);
                    long moneySuccess = num.get(2) + (long) amount;
                    num.set(2, moneySuccess);
                }
            }
        });
        LogApiOtpConfirmResponse res = new LogApiOtpConfirmResponse(true, "0", num.get(0) / 50L + 1L, num.get(1), num.get(2), records);
        return res;
    }

    @Override
    public LogApiOtpRequestResponse getApiOtpRequest(String nickname, String mobile, int amount, String requestId, int code, String startTime, String endTime, int page) {
        final ArrayList<LogApiOtpRequest> records = new ArrayList<LogApiOtpRequest>();
        final ArrayList<Long> num = new ArrayList<Long>();
        num.add(0, 0L);
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("dvt_recharge_by_otp_request");
        int numStart = (page - 1) * 50;
        int numEnd = 50;
        BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        HashMap<String, Object> conditions = new HashMap<String, Object>();
        if (!nickname.isEmpty()) {
            String pattern = ".*" + nickname + ".*";
            conditions.put("nick_name", new BasicDBObject().append("$regex", pattern).append("$options", "i"));
        }
        if (!mobile.isEmpty()) {
            conditions.put("mobile", mobile);
        }
        if (amount >= 0) {
            conditions.put("amount", amount);
        }
        if (!requestId.isEmpty()) {
            conditions.put("request_id", requestId);
        }
        if (code >= 0) {
            conditions.put("code", code);
        }
        if (!startTime.isEmpty() && !endTime.isEmpty()) {
            BasicDBObject obj = new BasicDBObject();
            obj.put("$gte", startTime);
            obj.put("$lte", endTime);
            conditions.put("time_log", obj);
        }
        FindIterable iterable = col.find(new Document(conditions)).sort(objsort).skip(numStart).limit(50);
        iterable.forEach(new Block<Document>() {

            public void apply(Document document) {
                String nickname = document.getString("nick_name");
                String mobile = document.getString("mobile");
                int amount = document.getInteger("amount");
                String errorCode = document.getString("error_code");
                String errorMessage = document.getString("error_message");
                String requestId = document.getString("request_id");
                String transId = document.getString("trans_id");
                int code = document.getInteger("code");
                String redirectUrl = document.getString("redirect_url");
                String timeLog = document.getString("time_log");
                LogApiOtpRequest model = new LogApiOtpRequest(nickname, mobile, amount, errorCode, errorMessage, requestId, transId, code, redirectUrl, timeLog);
                records.add(model);
                long count = num.get(0) + 1L;
                num.set(0, count);
            }
        });
        LogApiOtpRequestResponse res = new LogApiOtpRequestResponse(true, "0", num.get(0) / 50L + 1L, records);
        return res;
    }

    @Override
    public boolean isAgent(String nickName) throws SQLException {
        boolean result = false;
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_admin");
             PreparedStatement stmt = conn.prepareStatement(" SELECT *  FROM vinplay_admin.useragent  WHERE nickname = ? ");
        ) {
            stmt.setString(1, nickName);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    result = true;
                }
            }
        }
        return result;
    }

    @Override
    public Document getRechargeByGachthe(String transId) {
        Object result = null;
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        Document conditions = new Document();
        conditions.put("request_id", transId);
        Document dc = db.getCollection("dvt_recharge_by_gachthe").find(conditions).first();
        return dc;
    }

    @Override
    public Document getRechargeByGachthe(String nickname, String serial, String pin) {
        Object result = null;
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        Document conditions = new Document();
        conditions.put("nick_name", nickname);
        conditions.put("serial", serial);
        conditions.put("pin", pin);
        conditions.put("code", 30);
        Document dc = db.getCollection("dvt_recharge_by_gachthe").find(conditions).first();
        return dc;
    }

    @Override
    public List<Document> getRechargeByGachtheRecently() {
        Object result = null;
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        Document conditions = new Document();
        BasicDBObject codeCoditions = new BasicDBObject();
        codeCoditions.append("$ne", 30);
        conditions.put("code", codeCoditions);
        conditions.put("is_sent", false);
        List dc = db.getCollection("dvt_recharge_by_gachthe").find(conditions).into(new ArrayList());
        return dc;
    }

    @Override
    public boolean saveLogRechargeByGachThe(String nickname, String serial, String pin, long amount, String requestId, String requestTime, int code, String des, long money, String provider, String platform, long currentMoney, long addMoney, int userId, String username, String partner, String client) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("dvt_recharge_by_gachthe");
        Document doc = new Document();
        doc.append("nick_name", nickname);
        doc.append("serial", serial);
        doc.append("pin", pin);
        doc.append("amount", amount);
        doc.append("money", money);
        doc.append("provider", provider);
        doc.append("platform", platform);
        doc.append("request_id", requestId);
        doc.append("time_request", requestTime);
        doc.append("code", code);
        doc.append("description", des);
        doc.append("time_log", VinPlayUtils.getCurrentDateTime());
        doc.append("create_time", new Date());
        doc.append("is_sent", false);
        doc.append("current_money", currentMoney);
        doc.append("add_money", addMoney);
        doc.append("user_id", userId);
        doc.append("username", username);
        doc.append("partner", partner);
        doc.append("client", client);
        col.insertOne(doc);
        return true;
    }

    @Override
    public boolean saveZoanMomoTran(ZoanMomoReq req, int code, String nickname, long currentMoney, String username, String partner, String client, String platform, long addMoney) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("dvt_recharge_by_gachthe");
        Document doc = new Document();
        doc.append("nick_name", nickname);
        doc.append("serial", req.getFrom());
        doc.append("pin", req.getFrom());
        doc.append("amount", req.getAmount());
        doc.append("money", req.getAmount());
        doc.append("provider", "MOMO");
        doc.append("request_id", req.getRequest_id());
        doc.append("time_request", req.getTrans_time());
        doc.append("code", code);
        doc.append("description", "");
        doc.append("time_log", VinPlayUtils.getCurrentDateTime());
        doc.append("create_time", new Date());
        doc.append("is_sent", false);
        doc.append("current_money", currentMoney);
        doc.append("add_money", addMoney);
        doc.append("user_id", 0);
        doc.append("username", username);
        doc.append("partner", partner);
        doc.append("client", client);
        doc.append("platform", platform);
        col.insertOne(doc);
        return true;
    }

    @Override
    public boolean saveZoanMomoM32(String from, long amount, String id, String time, int code, String nickname, long currentMoney, String username, String partner, String client) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("dvt_recharge_by_gachthe");
        Document doc = new Document();
        doc.append("nick_name", nickname);
        doc.append("serial", from);
        doc.append("pin", from);
        doc.append("amount", amount);
        doc.append("money", amount);
        doc.append("provider", "MOMO");
        doc.append("platform", "MOMO");
        doc.append("request_id", id);
        doc.append("time_request", time);
        doc.append("code", code);
        doc.append("description", "");
        doc.append("time_log", VinPlayUtils.getCurrentDateTime());
        doc.append("create_time", new Date());
        doc.append("is_sent", false);
        doc.append("current_money", currentMoney);
        doc.append("add_money", amount);
        doc.append("user_id", 0);
        doc.append("username", username);
        doc.append("partner", partner);
        doc.append("client", client);
        col.insertOne(doc);
        return true;
    }

    @Override
    public boolean saveLogBankTransfer(String bankNo, String bankCode, long current_money, long amount, String comment, String time, String transId, int status) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("dvt_recharge_by_bank");
        Document doc = new Document();
        doc.append("bank_no", bankNo);
        doc.append("bank_code", bankCode);
        doc.append("current_money", current_money);
        doc.append("amount", amount);
        doc.append("comment", comment);
        doc.append("time", time);
        doc.append("trans_id", transId);
        doc.append("is_sent", false);
        doc.append("status", status);
        col.insertOne(doc);
        return true;
    }

    @Override
    public boolean updateLogBankTransfer(String transId, int status) {
        Object result = null;
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        Document conditions = new Document();
        conditions.put("trans_id", transId);
        BasicDBObject updateFields = new BasicDBObject();
        updateFields.append("status", status);
        db.getCollection("dvt_recharge_by_bank").updateOne(conditions, new Document("$set", updateFields));
        return true;
    }

    @Override
    public boolean updateLogBankTransferSent(String transId) {
        Object result = null;
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        Document conditions = new Document();
        conditions.put("trans_id", transId);
        BasicDBObject updateFields = new BasicDBObject();
        updateFields.append("is_sent", true);
        db.getCollection("dvt_recharge_by_bank").updateOne(conditions, new Document("$set", updateFields));
        return true;
    }

    @Override
    public Document GetLogBankTransfer(String transId) {
        Object result = null;
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        Document conditions = new Document();
        conditions.put("trans_id", transId);
        Document dc = db.getCollection("dvt_recharge_by_bank").find(conditions).first();
        return dc;
    }

    @Override
    public List<BankTranferModel> GetBankTransfer(int page, int pageSize, String nickName, int status) {
        final ArrayList<BankTranferModel> results = new ArrayList<BankTranferModel>();
        int num_start = (page - 1) * pageSize;
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        Document conditions = new Document();
        if (status > -1) {
            conditions.put("status", status);
        }
        if (nickName != null && !"".equals(nickName)) {
            conditions.put("nick_name", nickName);
        }
        FindIterable iterable = db.getCollection("dvt_recharge_by_bank").find(conditions).skip(num_start).limit(pageSize).sort(objsort);
        iterable.forEach(new Block<Document>() {

            public void apply(Document document) {
                BankTranferModel bank = new BankTranferModel();
                try {
                    bank.amount = document.getLong("amount");
                } catch (ClassCastException e){
                    // Lỗi convert , bỏ qua và lấy lại data
                    bank.amount = document.getInteger("amount");
                }
                if(document.containsKey("add_money"))
                    bank.addMoney = document.getLong("add_money");
                else
                    bank.addMoney = bank.amount;
                bank.bankCode = document.getString("bank_code");
                bank.bankNo = document.getString("bank_no");
                bank.comment = document.getString("comment");
                if(document.containsKey("status"))
                    bank.status = document.getInteger("status");
                bank.time = document.getString("time_log");
                if(document.containsKey("time_request")) {
                    try{
                        bank.timeRequest = document.getString("time_request");
                    }catch (ClassCastException e){
                        // Lỗi convert , bỏ qua và lấy lại data
                        bank.timeRequest = String.valueOf(document.getLong("time_request"));
                    }
                } else
                    bank.timeRequest = "0";
                bank.transId = document.getString("trans_id");
                results.add(bank);
            }
        });
        return results;
    }

    @Override
    public int GetBankTransferCount(String nickName, int status) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        Document conditions = new Document();
        if (status > -1) {
            conditions.put("status", status);
        }
        if (nickName != null && !"".equals(nickName)) {
            conditions.put("comment", nickName);
        }
        int record = (int) db.getCollection("dvt_recharge_by_bank").count(conditions);
        return record;
    }

    @Override
    public List<CardTranferModel> GetCardTransfer(int page, int pageSize, String nick_name, int status) {
        final ArrayList<CardTranferModel> results = new ArrayList<CardTranferModel>();
        int num_start = (page - 1) * pageSize;
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        Document conditions = new Document();
        BasicDBObject providerCoditions = new BasicDBObject();
        providerCoditions.append("$ne", "MOMO");
        conditions.put("provider", providerCoditions);
        if (status > -1) {
            conditions.put("status", status);
        }
        if (nick_name != null && !"".equals(nick_name)) {
            conditions.put("nick_name", nick_name);
        }
        FindIterable iterable = db.getCollection("dvt_recharge_by_gachthe").find(conditions).skip(num_start).limit(pageSize).sort(objsort);
        iterable.forEach(new Block<Document>() {
            public void apply(Document document) {
                CardTranferModel bank = new CardTranferModel();
                try {
                    bank.amount = document.getLong("amount");
                } catch (ClassCastException e){
                    // Lỗi convert , bỏ qua và lấy lại data
                    bank.amount = document.getInteger("amount");
                }
                if(document.containsKey("add_money"))
                    bank.addMoney = document.getLong("add_money");
                else
                    bank.addMoney = bank.amount;
                bank.nickname = document.getString("nick_name");
                bank.serial = document.getString("serial");
                bank.pin = document.getString("pin");
                bank.provider = document.getString("provider");
                bank.comment = document.getString("description");
                bank.status = document.getInteger("code");
                bank.time = document.getString("time_log");
                if(document.containsKey("time_request")) {
                    try{
                        bank.timeRequest = document.getString("time_request");
                    }catch (ClassCastException e){
                        // Lỗi convert , bỏ qua và lấy lại data
                        bank.timeRequest = String.valueOf(document.getLong("time_request"));
                    }
                } else
                    bank.timeRequest = "0";
                bank.transId = document.getString("request_id");
                results.add(bank);
            }
        });
        return results;
    }

    @Override
    public List<MomoTranferModel> GetMomoTransfer(int page, int pageSize, String nick_name, int status) {
        final ArrayList<MomoTranferModel> results = new ArrayList<MomoTranferModel>();
        int num_start = (page - 1) * pageSize;
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        Document conditions = new Document();
        conditions.put("provider", "MOMO");
        if (status > -1) {
            conditions.put("status", status);
        }
        if (nick_name != null && !"".equals(nick_name)) {
            conditions.put("nick_name", nick_name);
        }
        FindIterable iterable = db.getCollection("dvt_recharge_by_gachthe").find(conditions).skip(num_start).limit(pageSize).sort(objsort);
        iterable.forEach(new Block<Document>() {

            public void apply(Document document) {
                MomoTranferModel bank = new MomoTranferModel();
                try {
                    bank.amount = document.getLong("amount");
                } catch (ClassCastException e){
                    // Lỗi convert , bỏ qua và lấy lại data
                    bank.amount = document.getInteger("amount");
                }
                if(document.containsKey("add_money"))
                    bank.addMoney = document.getLong("add_money");
                else
                    bank.addMoney = bank.amount;
                bank.nickname = document.getString("nick_name");
                bank.comment = document.getString("description");
                bank.status = document.getInteger("code");
                bank.time = document.getString("time_log");
                if(document.containsKey("time_request")) {
                    try{
                        bank.timeRequest = document.getString("time_request");
                    }catch (ClassCastException e){
                        // Lỗi convert , bỏ qua và lấy lại data
                        bank.timeRequest = String.valueOf(document.getLong("time_request"));
                    }
                } else
                    bank.timeRequest = "0";
                bank.transId = document.getString("request_id");
                results.add(bank);
            }
        });
        return results;
    }

    @Override
    public int GetMomoTransferCount(String nick_name, int status) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        Document conditions = new Document();
        conditions.put("provider", "MOMO");
        if (status > -1) {
            conditions.put("status", status);
        }
        if (nick_name != null && !"".equals(nick_name)) {
            conditions.put("nick_name", nick_name);
        }
        int record = (int) db.getCollection("dvt_recharge_by_gachthe").count(conditions);
        return record;
    }

    @Override
    public boolean UpdateGachtheTransctions(String transId, int code, String message) {
        Object result = null;
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        Document conditions = new Document();
        conditions.put("request_id", transId);
        BasicDBObject updateFields = new BasicDBObject();
        updateFields.append("code", code);
        updateFields.append("description", message);
        db.getCollection("dvt_recharge_by_gachthe").updateOne(conditions, new Document("$set", updateFields));
        return true;
    }

    @Override
    public boolean UpdateGachtheTransctionsSent(String transId) {
        Object result = null;
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        Document conditions = new Document();
        conditions.put("request_id", transId);
        BasicDBObject updateFields = new BasicDBObject();
        updateFields.append("is_sent", true);
        db.getCollection("dvt_recharge_by_gachthe").updateOne(conditions, new Document("$set", updateFields));
        return true;
    }

    @Override
    public Document getRechargeByNapTienGa(String transId) {
        Object result = null;
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        Document conditions = new Document();
        conditions.put("request_id", transId);
        Document dc = db.getCollection("dvt_recharge_by_naptienga").find(conditions).first();
        return dc;
    }

    @Override
    public Document getRechargeByNapTienGa(String nickname, String serial, String pin) {
        Object result = null;
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        Document conditions = new Document();
        conditions.put("nick_name", nickname);
        conditions.put("serial", serial);
        conditions.put("pin", pin);
        conditions.put("code", 30);
        Document dc = db.getCollection("dvt_recharge_by_naptienga").find(conditions).first();
        return dc;
    }

    @Override
    public List<Document> getRechargeByNapTienGaRecently() {
        Object result = null;
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        Document conditions = new Document();
        conditions.put("code", 0);
        conditions.put("is_sent", false);
        List dc = db.getCollection("dvt_recharge_by_naptienga").find(conditions).into(new ArrayList());
        return dc;
    }

    @Override
    public boolean saveLogRechargeByNapTienGa(String nickname, String serial, String pin, long amount, String requestId, String requestTime, int code, String des, long money, String provider, String platform, long currentMoney, long addMoney, int userId, String username, int napTienGaId) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("dvt_recharge_by_naptienga");
        Document doc = new Document();
        doc.append("nick_name", nickname);
        doc.append("serial", serial);
        doc.append("pin", pin);
        doc.append("amount", amount);
        doc.append("money", money);
        doc.append("provider", provider);
        doc.append("platform", platform);
        doc.append("request_id", requestId);
        doc.append("time_request", requestTime);
        doc.append("code", code);
        doc.append("description", des);
        doc.append("time_log", VinPlayUtils.getCurrentDateTime());
        doc.append("create_time", new Date());
        doc.append("is_sent", false);
        doc.append("current_money", currentMoney);
        doc.append("add_money", addMoney);
        doc.append("user_id", userId);
        doc.append("username", username);
        doc.append("nap_tien_ga_id", napTienGaId);
        col.insertOne(doc);
        return true;
    }

    @Override
    public boolean UpdateNapTienGaTransctions(String transId, int code, String message) {
        Object result = null;
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        Document conditions = new Document();
        conditions.put("request_id", transId);
        BasicDBObject updateFields = new BasicDBObject();
        updateFields.append("code", code);
        updateFields.append("description", message);
        db.getCollection("dvt_recharge_by_naptienga").updateOne(conditions, new Document("$set", updateFields));
        return true;
    }

    @Override
    public boolean UpdateNapTienGaTransctionsSent(String transId) {
        Object result = null;
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        Document conditions = new Document();
        conditions.put("request_id", transId);
        BasicDBObject updateFields = new BasicDBObject();
        updateFields.append("is_sent", true);
        db.getCollection("dvt_recharge_by_naptienga").updateOne(conditions, new Document("$set", updateFields));
        return true;
    }

    @Override
    public List<Document> getRechargeByBankRecently() {
        Object result = null;
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        Document conditions = new Document();
        conditions.put("is_sent", false);
        List dc = db.getCollection("dvt_recharge_by_bank").find(conditions).into(new ArrayList());
        return dc;
    }

    @Override
    public Document getRechargeByIronMan(String transId) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        Document conditions = new Document();
        conditions.put("request_id", transId);
        Document dc = db.getCollection("dvt_recharge_by_ironman").find(conditions).first();
        return dc;
    }

    @Override
    public boolean UpdateIronManTransctions(String transId, int code, String message) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        Document conditions = new Document();
        conditions.put("request_id", transId);
        BasicDBObject updateFields = new BasicDBObject();
        updateFields.append("code", code);
        updateFields.append("description", message);
        db.getCollection("dvt_recharge_by_ironman").updateOne(conditions, new Document("$set", updateFields));
        return true;
    }

    @Override
    public Document getRechargeByIronMan(String nickname, String serial, String pin) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        Document conditions = new Document();
        conditions.put("nick_name", nickname);
        conditions.put("serial", serial);
        conditions.put("pin", pin);
        conditions.put("code", 30);
        Document dc = db.getCollection("dvt_recharge_by_ironman").find(conditions).first();
        return dc;
    }

    @Override
    public boolean saveLogRechargeByIronMan(String nickname, String serial, String pin, long amount, String requestId, String requestTime, int code, String des, long money, String provider, String platform, long currentMoney, long addMoney, int userId, String username) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("dvt_recharge_by_ironman");
        Document doc = new Document();
        doc.append("nick_name", nickname);
        doc.append("serial", serial);
        doc.append("pin", pin);
        doc.append("amount", amount);
        doc.append("money", money);
        doc.append("provider", provider);
        doc.append("platform", platform);
        doc.append("request_id", requestId);
        doc.append("time_request", requestTime);
        doc.append("code", code);
        doc.append("description", des);
        doc.append("time_log", VinPlayUtils.getCurrentDateTime());
        doc.append("create_time", new Date());
        doc.append("is_sent", false);
        doc.append("current_money", currentMoney);
        doc.append("add_money", addMoney);
        doc.append("user_id", userId);
        doc.append("username", username);
        col.insertOne(doc);
        return true;
    }
}
