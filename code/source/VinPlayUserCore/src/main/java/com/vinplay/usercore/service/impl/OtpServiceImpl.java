/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.hazelcast.core.HazelcastInstance
 *  com.hazelcast.core.IMap
 *  com.vinplay.vbee.common.exceptions.KeyNotFoundException
 *  com.vinplay.vbee.common.hazelcast.HazelcastClientFactory
 *  com.vinplay.vbee.common.messages.BaseMessage
 *  com.vinplay.vbee.common.messages.LogMoneyUserMessage
 *  com.vinplay.vbee.common.messages.MoneyMessageInMinigame
 *  com.vinplay.vbee.common.messages.OtpMessage
 *  com.vinplay.vbee.common.models.OtpModel
 *  com.vinplay.vbee.common.models.UserModel
 *  com.vinplay.vbee.common.models.cache.UserCacheModel
 *  com.vinplay.vbee.common.rmq.RMQApi
 *  com.vinplay.vbee.common.statics.TimeBasedOneTimePasswordUtil
 *  com.vinplay.vbee.common.utils.StringUtils
 *  com.vinplay.vbee.common.utils.VinPlayUtils
 *  org.apache.log4j.Logger
 */
package com.vinplay.usercore.service.impl;

import casio.king365.util.KingUtil;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.vinplay.dichvuthe.service.AlertService;
import com.vinplay.dichvuthe.service.impl.AlertServiceImpl;
import com.vinplay.usercore.dao.OtpDao;
import com.vinplay.usercore.dao.impl.OtpDaoImpl;
import com.vinplay.usercore.dao.impl.UserDaoImpl;
import com.vinplay.usercore.entities.MessageMTResponse;
import com.vinplay.usercore.service.OtpService;
import com.vinplay.usercore.service.impl.UserServiceImpl;
import com.vinplay.usercore.utils.GameCommon;
import com.vinplay.vbee.common.exceptions.KeyNotFoundException;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.messages.BaseMessage;
import com.vinplay.vbee.common.messages.LogMoneyUserMessage;
import com.vinplay.vbee.common.messages.MoneyMessageInMinigame;
import com.vinplay.vbee.common.messages.OtpMessage;
import com.vinplay.vbee.common.models.OtpModel;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.rmq.RMQApi;
import com.vinplay.vbee.common.statics.TimeBasedOneTimePasswordUtil;
import com.vinplay.vbee.common.utils.StringUtils;
import com.vinplay.vbee.common.utils.VinPlayUtils;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.concurrent.TimeoutException;

import misa.data.AccountTeleLink;
import org.apache.log4j.Logger;

public class OtpServiceImpl
implements OtpService {
    private static final Logger logger = Logger.getLogger("api");

    @Override
    public MessageMTResponse genMessageMT(OtpMessage message, String mobile) throws Exception {
        UserServiceImpl userSer = new UserServiceImpl();
        boolean success = false;
        String messageMT = "";
        String otp = "";
        if (message.getMessageMO().equals("OZZ OTP")) {
            if (userSer.checkMobile(mobile)) {
                otp = VinPlayUtils.genOtpSMS(mobile, message.getCommandCode());
                success = true;
                messageMT = String.format(GameCommon.MESSAGE_OTP_SUCCESS, otp);
            } else {
                messageMT = GameCommon.MESSAGE_ERROR_MOBILE;
            }
        } else if (message.getMessageMO().equals("OZZ APP")) {
            if (userSer.checkMobile(mobile)) {
                otp = VinPlayUtils.genOtpSMS(mobile, message.getCommandCode());
                success = true;
                messageMT = String.format(GameCommon.MESSAGE_APP_SUCCESS, otp);
            } else {
                messageMT = GameCommon.MESSAGE_ERROR_MOBILE;
            }
        } else if (message.getMessageMO().equals("OZZ ODP")) {
            if (userSer.checkMobileDaiLy(mobile)) {
                OtpDaoImpl otpDao = new OtpDaoImpl();
                OtpModel otpModel = otpDao.getOtpSMS(mobile, "OZZ ODP");
                if (otpModel != null && VinPlayUtils.compareDate(new Date(), otpModel.getOtpTime()) == 0) {
                    otp = otpModel.getOtp();
                } else {
                    otp = VinPlayUtils.genOtpSMS(mobile, message.getCommandCode());
                    success = true;
                }
                messageMT = String.format(GameCommon.MESSAGE_ODP_SUCCESS, otp, VinPlayUtils.getCurrentDate());
            } else {
                messageMT = GameCommon.MESSAGE_ERROR_MOBILE;
            }
        } else {
            messageMT = GameCommon.MESSAGE_ERROR_SYNTAX;
        }
        MessageMTResponse res = new MessageMTResponse(success, otp, messageMT);
        return res;
    }

    @Override
    public boolean logOTP(OtpMessage message) throws IOException, TimeoutException, InterruptedException {
        RMQApi.publishMessage("queue_otp", message, 201);
        return true;
    }

    @Override
    public boolean updateOtp(String mobile, String otp, String messageMO) throws SQLException {
        OtpDaoImpl dao = new OtpDaoImpl();
        return dao.updateOtpSMS(mobile, otp, messageMO);
    }

    @Override
    public String revertMobile(String mobile) {
        if (mobile.startsWith("84")) {
            return "0" + mobile.substring(2);
        }
        return mobile;
    }

    @Override
    public OtpModel CheckValidSMS(String nick_name) throws SQLException {
        Object dao;
        HazelcastInstance client = HazelcastClientFactory.getInstance();
        IMap userMap = client.getMap("users");
        UserModel model = null;
        if (userMap.containsKey(nick_name)) {
            model = (UserModel)userMap.get(nick_name);
            UserCacheModel userCacheModel = (UserCacheModel)model;
        } else {
            dao = new UserDaoImpl();
            model = ((UserDaoImpl)dao).getUserByNickName(nick_name);
        }
        if (model != null) {
            dao = new OtpDaoImpl();
            try {
                OtpModel otpModel = ((OtpDaoImpl)dao).getOtpSMS(model.getMobile(), "OZZ OTP");
                if (otpModel != null) {
                    if (System.currentTimeMillis() > otpModel.getOtpTime().getTime() + 180000L) {
                        otpModel.setCommandCode("OK");
                        return otpModel;
                    }
                    otpModel.setCommandCode("NOTOK");
                    return otpModel;
                }
                return new OtpModel(model.getMobile(), "", null, "FIRST_TIME", 0);
            }
            catch (ParseException parseException) {
                // empty catch block
            }
        }
        return null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int checkOtp(String otp, String nickname, String type, String mobile) throws SQLException, UnsupportedEncodingException, NoSuchAlgorithmException, KeyNotFoundException {
        KingUtil.printLog("checkOtp() 1 nickname: "+nickname+", otp: "+otp);
        if (GameCommon.getValueStr("OTP_DEFAULT").isEmpty()) {
            int res = -1;
            block24: {
                res = 77;
                if (this.checkAppOTP(nickname, otp) == 0) {
                    KingUtil.printLog("checkOtp() 2");
                    return 0;
                }
                try {
                    if (otp == null || type == null || otp.length() != 5 || !type.equals("0") && !type.equals("1")) break block24;
                    KingUtil.printLog("checkOtp() 3");
                    HazelcastInstance client = HazelcastClientFactory.getInstance();
                    IMap userMap = client.getMap("users");
                    UserModel model = null;
                    if (userMap.containsKey(nickname)) {
                        model = (UserModel)userMap.get(nickname);
                        UserCacheModel userCacheModel = (UserCacheModel)model;
                    } else {
                        UserDaoImpl userDao = new UserDaoImpl();
                        model = userDao.getUserByNickName(nickname);
                    }
                    KingUtil.printLog("checkOtp() 4");
                    if (model == null) break block24;
                    KingUtil.printLog("checkOtp() 5");
                    String string = mobile = mobile == null ? model.getMobile() : mobile;
                    if (mobile == null || mobile.isEmpty()) break block24;
                    if (type.equals("0")) {
                        try {
                            KingUtil.printLog("checkOtp() 6");
                            OtpDaoImpl dao = new OtpDaoImpl();
                            KingUtil.printLog("checkOtp() mobile: "+mobile);
                            OtpModel otpModel = dao.getOtpSMS(mobile, "OZZ OTP");
                            KingUtil.printLog("checkOtp() 6a");
                            KingUtil.printLog("checkOtp() otpModel: "+otpModel.toString());
                            if (userMap.containsKey(nickname)) {
                                KingUtil.printLog("checkOtp() 6b");
                                userMap.lock(nickname);
                            }
                            KingUtil.printLog("checkOtp() 6c");
                            if (otpModel != null && otpModel.getOtp() != null && otpModel.getOtpTime() != null && otp.equals(otpModel.getOtp())) {
                                KingUtil.printLog("checkOtp() 6d");
                                if (!VinPlayUtils.checkOtpTimeout(otpModel.getOtpTime())) {
                                    KingUtil.printLog("checkOtp() 7");
                                    res = 0;
                                    KingUtil.printLog("checkOtp() 6e");
                                    dao.updateOtpSMS(mobile, "", "OZZ OTP");
                                    KingUtil.printLog("checkOtp() 6f");
                                } else {
                                    KingUtil.printLog("checkOtp() 8");
                                    res = 4;
                                }
                                KingUtil.printLog("checkOtp() 6g");
                            }
                            KingUtil.printLog("checkOtp() 6h");
                            break block24;
                        }
                        catch (Exception e) {
                            KingUtil.printLog("checkOtp() 9");
                            logger.debug(e);
                            res = 74;
                            break block24;
                        }
                        finally {
                            if (userMap.containsKey(nickname)) {
                                userMap.unlock(nickname);
                            }
                        }
                    }
                    if (model.getMobile() == null || model.getMobile().isEmpty() || !model.isHasMobileSecurity() || !userMap.containsKey(nickname)) break block24;
                    try {
                        KingUtil.printLog("checkOtp() 10");
                        String otpApp;
                        userMap.lock(nickname);
                        UserCacheModel user = (UserCacheModel)userMap.get(nickname);
                        if ((user.getOtpApp() == null || !user.getOtpApp().equals(otp)) && (otpApp = VinPlayUtils.genOtpApp(nickname, mobile)).equals(otp)) {
                            KingUtil.printLog("checkOtp() 11");
                            user.setOtpApp(otp);
                            userMap.put(nickname, user);
                            res = 0;
                        }
                    }
                    catch (Exception e) {
                        KingUtil.printLog("checkOtp() 12");
                        logger.debug(e);
                        res = 75;
                    }
                    finally {
                        userMap.unlock(nickname);
                    }
                }
                catch (Exception e2) {
                    KingUtil.printLog("checkOtp() 13");
                    logger.debug(e2);
                    res = 76;
                }
            }
            logger.info("Verify Otp response:" + res);
            return res;
        }
        if (otp.equals(GameCommon.getValueStr("OTP_DEFAULT"))) {
            KingUtil.printLog("checkOtp() 14");
            return 0;
        }
        return 3;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int checkOtpLogin(String otp, String otpType, String nickname, String mobile, boolean appSecure) throws Exception {
        if (GameCommon.getValueStr("OTP_DEFAULT").isEmpty()) {
            int res;
            block17: {
                res = 3;
                if (this.checkAppOTP(nickname, otp) == 0) {
                    return 0;
                }
                try {
                    if (otp == null || otpType == null || otp.length() != 5 || !otpType.equals("0") && !otpType.equals("1")) break block17;
                    if (otpType.equals("0")) {
                        OtpDaoImpl dao = new OtpDaoImpl();
                        OtpModel model = dao.getOtpSMS(mobile, "OZZ OTP");
                        if (model != null && model.getOtp() != null && model.getOtpTime() != null && otp.equals(model.getOtp())) {
                            if (!VinPlayUtils.checkOtpTimeout(model.getOtpTime())) {
                                res = 0;
                                dao.updateOtpSMS(mobile, "", "OZZ OTP");
                            } else {
                                res = 4;
                            }
                        }
                        break block17;
                    }
                    HazelcastInstance client = HazelcastClientFactory.getInstance();
                    IMap userMap = client.getMap("users");
                    if (userMap.containsKey(nickname)) {
                        try {
                            String otpApp;
                            userMap.lock(nickname);
                            UserCacheModel user = (UserCacheModel)userMap.get(nickname);
                            if ((user.getOtpApp() == null || !user.getOtpApp().equals(otp)) && (otpApp = VinPlayUtils.genOtpApp(nickname, mobile)).equals(otp)) {
                                user.setOtpApp(otp);
                                userMap.put(nickname, user);
                                res = 0;
                            }
                            break block17;
                        }
                        catch (Exception e) {
                            logger.debug(e);
                            break block17;
                        }
                        finally {
                            userMap.unlock(nickname);
                        }
                    }
                    String otpApp2 = VinPlayUtils.genOtpApp(nickname, mobile);
                    if (otpApp2.equals(otp)) {
                        res = 0;
                    }
                }
                catch (Exception e2) {
                    logger.debug(e2);
                }
            }
            return res;
        }
        if (otp.equals(GameCommon.getValueStr("OTP_DEFAULT"))) {
            return 0;
        }
        return 3;
    }

    @Override
    public int getOdp(String nickname) throws Exception {
        int code = 1;
        HazelcastInstance client = HazelcastClientFactory.getInstance();
        IMap userMap = client.getMap("users");
        UserModel model = null;
        if (userMap.containsKey(nickname)) {
            model = (UserModel)userMap.get(nickname);
            UserCacheModel userCacheModel = (UserCacheModel)model;
        } else {
            UserDaoImpl dao = new UserDaoImpl();
            model = dao.getUserByNickName(nickname);
        }
        if (model != null) {
            if (model.getDaily() != 0) {
                if (model.getMobile() != null && !model.getMobile().isEmpty() && model.isHasMobileSecurity()) {
                    OtpDaoImpl otpDao = new OtpDaoImpl();
                    OtpModel otpModel = otpDao.getOtpSMS(model.getMobile(), "OZZ ODP");
                    if (otpModel != null && VinPlayUtils.compareDate(new Date(), otpModel.getOtpTime()) == 0) {
                        code = 5;
                    } else {
                        String odp = VinPlayUtils.genOtpSMS(model.getMobile(), "OZZ ODP");
                        otpDao.updateOtpSMS(model.getMobile(), odp, "OZZ ODP");
                        AlertServiceImpl service = new AlertServiceImpl();
                        String content = String.format(GameCommon.MESSAGE_ODP_SUCCESS, odp, VinPlayUtils.getCurrentDate());
                        boolean rs = service.SendSMSRutCuoc(model.getMobile(), odp);
                        code = 0;
                    }
                } else {
                    code = 4;
                }
            } else {
                code = 3;
            }
        } else {
            code = 2;
        }
        return code;
    }

    @Override
    public int getVoiceOdp(String nickname) throws Exception {
        int code = 1;
        HazelcastInstance client = HazelcastClientFactory.getInstance();
        IMap userMap = client.getMap("users");
        UserModel model = null;
        if (userMap.containsKey(nickname)) {
            model = (UserModel)userMap.get(nickname);
            UserCacheModel userCacheModel = (UserCacheModel)model;
        } else {
            UserDaoImpl dao = new UserDaoImpl();
            model = dao.getUserByNickName(nickname);
        }
        if (model != null) {
            if (model.getDaily() != 0) {
                if (model.getMobile() != null && !model.getMobile().isEmpty() && model.isHasMobileSecurity()) {
                    OtpDaoImpl otpDao = new OtpDaoImpl();
                    OtpModel otpModel = otpDao.getOtpSMS(model.getMobile(), "OZZ ODP");
                    if (otpModel != null && VinPlayUtils.compareDate(new Date(), otpModel.getOtpTime()) == 0) {
                        code = 5;
                    } else {
                        String odp = VinPlayUtils.genOtpSMS(model.getMobile(), "OZZ ODP");
                        otpDao.updateOtpSMS(model.getMobile(), odp, "OZZ ODP");
                        AlertServiceImpl service = new AlertServiceImpl();
                        String content = String.format(GameCommon.MESSAGE_ODP_SUCCESS, odp, VinPlayUtils.getCurrentDate());
                        boolean rs = service.SendSMSRutCuoc(model.getMobile(), odp);
                        code = 0;
                    }
                } else {
                    code = 4;
                }
            } else {
                code = 3;
            }
        } else {
            code = 2;
        }
        return code;
    }

    @Override
    public int getOdp(String nickname, String type) throws Exception {
        int code = 1;
        HazelcastInstance client = HazelcastClientFactory.getInstance();
        IMap userMap = client.getMap("users");
        UserModel model = null;
        if (userMap.containsKey(nickname)) {
            model = (UserModel)userMap.get(nickname);
            UserCacheModel userCacheModel = (UserCacheModel)model;
        } else {
            UserDaoImpl dao = new UserDaoImpl();
            model = dao.getUserByNickName(nickname);
        }
        if (model != null) {
            if (model.getDaily() != 0) {
                if (model.getMobile() != null && !model.getMobile().isEmpty() && model.isHasMobileSecurity()) {
                    OtpDaoImpl otpDao = new OtpDaoImpl();
                    OtpModel otpModel = otpDao.getOtpSMS(model.getMobile(), "OZZ ODP");
                    if (otpModel != null && VinPlayUtils.compareDate(new Date(), otpModel.getOtpTime()) == 0) {
                        code = 5;
                    } else {
                        String odp = VinPlayUtils.genOtpSMS(model.getMobile(), "OZZ ODP");
                        otpDao.updateOtpSMS(model.getMobile(), odp, "OZZ ODP");
                        AlertServiceImpl service = new AlertServiceImpl();
                        String content = String.format(GameCommon.MESSAGE_ODP_SUCCESS, odp, VinPlayUtils.getCurrentDate());
                        service.SendSMSRutCuoc(model.getMobile(), content);
                        code = 0;
                    }
                } else {
                    code = 4;
                }
            } else {
                code = 3;
            }
        } else {
            code = 2;
        }
        return code;
    }

    @Override
    public int checkOdp(String nickname, String odp) throws Exception {
        OtpDaoImpl otpDao;
        OtpModel otpModel;
        if (this.checkAppOTP(nickname, odp) == 0) {
            return 0;
        }
        int code = 1;
        if (odp == null || odp.length() != 5) {
            code = 5;
            return code;
        }
        HazelcastInstance client = HazelcastClientFactory.getInstance();
        IMap userMap = client.getMap("users");
        UserModel model = null;
        if (userMap.containsKey(nickname)) {
            model = (UserModel)userMap.get(nickname);
            UserCacheModel userCacheModel = (UserCacheModel)model;
        } else {
            UserDaoImpl dao = new UserDaoImpl();
            model = dao.getUserByNickName(nickname);
        }
        int n = model != null ? (model.getDaily() != 0 ? (model.getMobile() != null && !model.getMobile().isEmpty() && model.isHasMobileSecurity() ? ((otpModel = (otpDao = new OtpDaoImpl()).getOtpSMS(model.getMobile(), "OZZ ODP")) != null && odp.equals(otpModel.getOtp()) ? (VinPlayUtils.compareDate(new Date(), otpModel.getOtpTime()) == 0 ? 0 : 6) : 5) : 4) : 3) : 2;
        code = n;
        return code;
    }

    @Override
    public int checkOtpSmsForApp(String nickname, String otp) throws Exception {
        if (GameCommon.getValueStr("OTP_DEFAULT").isEmpty()) {
            int res = 3;
            if (this.checkAppOTP(nickname, otp) == 0) {
                return 0;
            }
            return res;
        }
        if (otp.equals(GameCommon.getValueStr("OTP_DEFAULT"))) {
            return 0;
        }
        return 3;
    }

    @Override
    public int getEsmsOTP(String nickname, String mobile, String type) throws Exception {
        int code = 1;
        HazelcastInstance client = HazelcastClientFactory.getInstance();
        IMap userMap = client.getMap("users");
        UserModel model = null;
        if (userMap.containsKey(nickname)) {
            model = (UserModel)userMap.get(nickname);
            UserCacheModel userCacheModel = (UserCacheModel)model;
        } else {
            UserDaoImpl dao = new UserDaoImpl();
            model = dao.getUserByNickName(nickname);
        }
        if (model != null) {
            if (model.getMobile() != null && !model.getMobile().isEmpty() && model.isHasMobileSecurity()) {
                OtpDaoImpl otpDao = new OtpDaoImpl();
                String mobile2 = this.revertMobile(model.getMobile());
                String otp = null;
                try {
                    otp = VinPlayUtils.genOtpSMS(model.getMobile(), "");
                }
                catch (Exception e) {
                    logger.debug(e);
                }
                otpDao.updateOtpSMS(model.getMobile(), otp, "OZZ OTP");
                AlertServiceImpl service = new AlertServiceImpl();
                String content = String.format(GameCommon.MESSAGE_OTP_SUCCESS, otp, VinPlayUtils.getCurrentDate());
                service.SendSMSRutCuoc(mobile2, content);
                code = 0;
            } else {
                code = 4;
            }
        } else {
            code = 2;
        }
        return code;
    }

    @Override
    public String GenerateOTP(String nickname, String mobile) throws Exception {
        boolean code = true;
        HazelcastInstance client = HazelcastClientFactory.getInstance();
        IMap userMap = client.getMap("users");
        UserModel model = null;
        if (userMap.containsKey(nickname)) {
            model = (UserModel)userMap.get(nickname);
            UserCacheModel userCacheModel = (UserCacheModel)model;
        } else {
            UserDaoImpl dao = new UserDaoImpl();
            model = dao.getUserByNickName(nickname);
        }
        if (model != null) {
            if (model.getMobile() != null && !model.getMobile().isEmpty()) {
                OtpDaoImpl otpDao = new OtpDaoImpl();
                String mobile2 = this.revertMobile(model.getMobile());
                String otp = null;
                try {
                    otp = VinPlayUtils.genOtpSMS(model.getMobile(), "");
                }
                catch (Exception e) {
                    logger.debug(e);
                }
                otpDao.updateOtpSMS(model.getMobile(), otp, "OZZ OTP");
                return otp;
            }
            return null;
        }
        return null;
    }

    @Override
    public int checkOtpEsms(String nickname, String odp) throws Exception {
        OtpDaoImpl otpDao;
        OtpModel otpModel;
        if (this.checkAppOTP(nickname, odp) == 0) {
            return 0;
        }
        int code = 1;
        if (odp == null || odp.length() != 5) {
            code = 5;
            return code;
        }
        HazelcastInstance client = HazelcastClientFactory.getInstance();
        IMap userMap = client.getMap("users");
        UserModel model = null;
        if (userMap.containsKey(nickname)) {
            model = (UserModel)userMap.get(nickname);
            UserCacheModel userCacheModel = (UserCacheModel)model;
        } else {
            UserDaoImpl dao = new UserDaoImpl();
            model = dao.getUserByNickName(nickname);
        }
        int n = model != null ? (model.getDaily() != 0 ? (model.getMobile() != null && !model.getMobile().isEmpty() && model.isHasMobileSecurity() ? ((otpModel = (otpDao = new OtpDaoImpl()).getOtpSMS(model.getMobile(), "OZZ ODP")) != null && odp.equals(otpModel.getOtp()) ? (VinPlayUtils.compareDate(new Date(), otpModel.getOtpTime()) == 0 ? 0 : 6) : 5) : 4) : 3) : 2;
        code = n;
        return code;
    }

    @Override
    public int checkAppOTP(String nickname, String otp) {
        int res = 3;
        if (otp != null && otp.length() == 6) {
            try {
                String Secret = VinPlayUtils.getUserSecretKey(nickname);
                int INTOTP = Integer.parseInt(otp);
                if (TimeBasedOneTimePasswordUtil.validateCurrentNumber(Secret, INTOTP, 0)) {
                    res = 0;
                }
            }
            catch (Exception e) {
                logger.debug(e);
            }
        }
        return res;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int sendOtpEsms(String nickname, String mobile) throws Exception {
        int res = 3;
        HazelcastInstance client = HazelcastClientFactory.getInstance();
        IMap userMap = client.getMap("users");
        UserModel model = null;
        if (userMap.containsKey(mobile)) {
            model = (UserModel)userMap.get(nickname);
            UserCacheModel userCacheModel = (UserCacheModel)model;
        } else {
            UserDaoImpl userDao = new UserDaoImpl();
            model = userDao.getUserByNickName(nickname);
        }
        if (model == null) {
            return res;
        }
        String string = mobile = mobile == "" ? model.getMobile() : mobile;
        if (mobile == null || mobile.isEmpty()) {
            return res;
        }
        try {
            if (userMap.containsKey(nickname)) {
                userMap.lock(nickname);
            }
            String otp = StringUtils.randomStringNumber(5);
            OtpDaoImpl otpDao = new OtpDaoImpl();
            OtpModel checkResponse = this.CheckValidSMS(nickname);
            if (checkResponse != null && checkResponse.getCommandCode().equals("OK")) {
                if (checkResponse.getCount() >= 1 && model.getVin() > 1000L) {
                    otpDao.updateOtpSMS(model.getMobile(), otp, "OZZ OTP", checkResponse.getCount() + 1);
                    AlertServiceImpl service = new AlertServiceImpl();
                    service.SendSMSRutCuoc(model.getMobile(), otp);
                    res = 0;
                    if (checkResponse.getCount() >= 2) {
                        userMap = client.getMap("users");
                        UserCacheModel user = (UserCacheModel)userMap.get(nickname);
                        user = (UserCacheModel)userMap.get(nickname);
                        long moneyUser = user.getVin();
                        long currentMoney = user.getVinTotal();
                        long money = -1000L;
                        user.setVin(moneyUser += money);
                        user.setVinTotal(currentMoney += money);
                        String description = "Thanh toan phi SMS";
                        MoneyMessageInMinigame messageMoney = new MoneyMessageInMinigame(VinPlayUtils.genMessageId(), user.getId(), nickname, "RechargeByCard", moneyUser, currentMoney, money, "vin", 0L, 0, 0);
                        LogMoneyUserMessage messageLog = new LogMoneyUserMessage(user.getId(), nickname, "SMSFee", "Thanh toan phi SMS", currentMoney, money, "vin", description, 0L, false, user.isBot());
                        RMQApi.publishMessagePayment(messageMoney, 16);
                        RMQApi.publishMessageLogMoney(messageLog);
                        userMap.put(nickname, user);
                    }
                } else if (checkResponse.getCount() < 2) {
                    otpDao.updateOtpSMS(model.getMobile(), otp, "OZZ OTP");
                    AlertServiceImpl service = new AlertServiceImpl();
                    service.SendSMSRutCuoc(model.getMobile(), otp);
                    res = 0;
                } else {
                    res = 30;
                }
            } else if (checkResponse != null && checkResponse.getCommandCode().equals("FIRST_TIME")) {
                otpDao.updateOtpSMSFirst(model.getMobile(), otp, "OZZ OTP");
                AlertServiceImpl service = new AlertServiceImpl();
                service.SendSMSRutCuoc(model.getMobile(), otp);
                res = 0;
            } else {
                res = 30;
            }
        }
        catch (Exception e) {
            logger.debug(e);
        }
        finally {
            if (userMap.containsKey(nickname)) {
                userMap.unlock(nickname);
            }
        }
        return res;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int sendVoiceOtp(String nickname, String mobile) throws Exception {
        int res = 3;
        HazelcastInstance client = HazelcastClientFactory.getInstance();
        IMap userMap = client.getMap("users");
        UserModel model = null;
        if (userMap.containsKey(mobile)) {
            model = (UserModel)userMap.get(nickname);
            UserCacheModel userCacheModel = (UserCacheModel)model;
        } else {
            UserDaoImpl userDao = new UserDaoImpl();
            model = userDao.getUserByNickName(nickname);
        }
        if (model == null) {
            return res;
        }
        String string = mobile = mobile == "" ? model.getMobile() : mobile;
        if (mobile == null || mobile.isEmpty()) {
            return res;
        }
        try {
            if (userMap.containsKey(nickname)) {
                userMap.lock(nickname);
            }
            String otp = StringUtils.randomStringNumber(5);
            OtpDao otpDao = new OtpDaoImpl();
            OtpModel checkResponse = this.CheckValidSMS(nickname);
            KingUtil.printLog("sendVoiceOtp() checkResponse: "+checkResponse);
            if (checkResponse != null && (checkResponse.getCommandCode().equals("OK") || checkResponse.getCommandCode().equals("FIRST_TIME"))) {
                if (checkResponse.getCount() >= 1 && model.getVin() > 1000L) {
                    otpDao.updateOtpSMS(model.getMobile(), otp, "OZZ OTP", checkResponse.getCount() + 1);
                    AlertService service = new AlertServiceImpl();
                    service.SendOtpTelegram(nickname, otp);
                    res = 0;
                    /*if (checkResponse.getCount() >= 2) {
                        userMap = client.getMap("users");
                        UserCacheModel user = (UserCacheModel)userMap.get(nickname);
                        user = (UserCacheModel)userMap.get(nickname);
                        long moneyUser = user.getVin();
                        long currentMoney = user.getVinTotal();
                        long money = -1000L;
                        user.setVin(moneyUser += money);
                        user.setVinTotal(currentMoney += money);
                        String description = "Thanh toan phi SMS";
                        MoneyMessageInMinigame messageMoney = new MoneyMessageInMinigame(VinPlayUtils.genMessageId(), user.getId(), nickname, "RechargeByCard", moneyUser, currentMoney, money, "vin", 0L, 0, 0);
                        LogMoneyUserMessage messageLog = new LogMoneyUserMessage(user.getId(), nickname, "SMSFee", "Thanh toan phi SMS", currentMoney, money, "vin", description, 0L, false, user.isBot());
                        RMQApi.publishMessagePayment(messageMoney, 16);
                        RMQApi.publishMessageLogMoney(messageLog);
                        userMap.put(nickname, user);
                    }*/
                } else if (checkResponse.getCount() < 2) {
                    otpDao.updateOtpSMS(model.getMobile(), otp, "OZZ OTP");
                    AlertServiceImpl service = new AlertServiceImpl();
                    service.SendOtpTelegram(nickname, otp);
                    KingUtil.printLog("sendVoiceOtp() 6a");
                    res = 0;
                } else {
                    res = 30;
                }
            }
            /*else if (checkResponse != null && checkResponse.getCommandCode().equals("FIRST_TIME")) {
                KingUtil.printLog("sendVoiceOtp() 7");
                otpDao.updateOtpSMSFirst(model.getMobile(), otp, "OZZ OTP");
                AlertServiceImpl service = new AlertServiceImpl();
                service.SendSMSRutCuoc(model.getMobile(), otp);
                res = 0;
            }*/
            else {
                res = 30;
            }
        }
        catch (Exception e) {
            KingUtil.printException("otp ", e);
            logger.debug(e);
        }
        finally {
            if (userMap.containsKey(nickname)) {
                userMap.unlock(nickname);
            }
        }
        return res;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int sendOdpEsms(String nickname, String mobile) throws Exception {
        int res = 3;
        HazelcastInstance client = HazelcastClientFactory.getInstance();
        IMap userMap = client.getMap("users");
        UserModel model = null;
        if (userMap.containsKey(mobile)) {
            model = (UserModel)userMap.get(nickname);
            UserCacheModel userCacheModel = (UserCacheModel)model;
        } else {
            UserDaoImpl userDao = new UserDaoImpl();
            model = userDao.getUserByNickName(nickname);
        }
        if (model == null) {
            return res;
        }
        String string = mobile = mobile == null ? model.getMobile() : mobile;
        if (mobile == null || mobile.isEmpty()) {
            return res;
        }
        try {
            if (userMap.containsKey(nickname)) {
                userMap.lock(nickname);
            }
            String odp = StringUtils.randomStringNumber(5);
            OtpDaoImpl otpDao = new OtpDaoImpl();
            otpDao.updateOtpSMS(model.getMobile(), odp, "OZZ ODP");
            AlertServiceImpl service = new AlertServiceImpl();
            service.SendSMSRutCuoc(model.getMobile(), odp);
            res = 0;
        }
        catch (Exception e) {
            logger.debug(e);
        }
        finally {
            if (userMap.containsKey(nickname)) {
                userMap.unlock(nickname);
            }
        }
        return res;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public String GenerateOdp(String nickname, String mobile) throws Exception {
        int res = 3;
        HazelcastInstance client = HazelcastClientFactory.getInstance();
        IMap userMap = client.getMap("users");
        UserModel model = null;
        if (userMap.containsKey(mobile)) {
            model = (UserModel)userMap.get(nickname);
            UserCacheModel userCacheModel = (UserCacheModel)model;
        } else {
            UserDaoImpl userDao = new UserDaoImpl();
            model = userDao.getUserByNickName(nickname);
        }
        if (model == null) {
            return null;
        }
        String string = mobile = mobile == null ? model.getMobile() : mobile;
        if (mobile == null || mobile.isEmpty()) {
            return null;
        }
        try {
            if (userMap.containsKey(nickname)) {
                userMap.lock(nickname);
            }
            String odp = StringUtils.randomStringNumber(5);
            OtpDaoImpl otpDao = new OtpDaoImpl();
            otpDao.updateOtpSMS(model.getMobile(), odp, "OZZ ODP");
            String string2 = odp;
            return string2;
        }
        catch (Exception e) {
            logger.debug(e);
        }
        finally {
            if (userMap.containsKey(nickname)) {
                userMap.unlock(nickname);
            }
        }
        return null;
    }
}

