/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.vinplay.vbee.common.messages.GiftCodeMessage
 */
package com.vinplay.usercore.dao;

import com.vinplay.vbee.common.messages.GiftCodeMessage;
import java.sql.SQLException;
import java.util.List;

public interface GiftCodeXCDao {
    List<String> loadAllGiftcode() throws SQLException;

    void insertGiftcodeStore(GiftCodeMessage var1) throws SQLException;
}

