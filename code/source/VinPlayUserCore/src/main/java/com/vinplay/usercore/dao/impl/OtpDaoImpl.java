//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.vinplay.usercore.dao.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.FindOneAndUpdateOptions;
import com.vinplay.doisoat.entities.DoisoatVmg;
import com.vinplay.doisoat.entities.DoisoatVmgByProvider;
import com.vinplay.usercore.dao.OtpDao;
import com.vinplay.usercore.entities.LogSmsOtp;
import com.vinplay.usercore.response.LogSMSOtpResponse;
import com.vinplay.vbee.common.models.OtpModel;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.utils.PhoneUtils;
import com.vinplay.vbee.common.utils.VinPlayUtils;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.bson.Document;

public class OtpDaoImpl implements OtpDao {
    public OtpDaoImpl() {
    }

    public boolean updateOtpSMS(String mobile, String otp, String messageMO) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = null;
        if (messageMO.equals("OZZ OTP")) {
            col = db.getCollection("sms_vin_otp");
        } else if (messageMO.equals("OZZ APP")) {
            col = db.getCollection("sms_vin_app");
        } else {
            if (!messageMO.equals("OZZ ODP")) {
                return false;
            }

            col = db.getCollection("sms_vin_odp");
        }

        BasicDBObject updateFields = new BasicDBObject();
        updateFields.append("otp", otp);
        updateFields.append("otp_time", VinPlayUtils.getCurrentDateTime());
        BasicDBObject conditions = new BasicDBObject();
        conditions.append("mobile", mobile);
        FindOneAndUpdateOptions options = new FindOneAndUpdateOptions();
        options.upsert(true);
        col.findOneAndUpdate(conditions, new Document("$set", updateFields), options);
        return true;
    }

    public boolean updateOtpSMS(String mobile, String otp, String messageMO, int count) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = null;
        if (messageMO.equals("OZZ OTP")) {
            col = db.getCollection("sms_vin_otp");
        } else if (messageMO.equals("OZZ APP")) {
            col = db.getCollection("sms_vin_app");
        } else {
            if (!messageMO.equals("OZZ ODP")) {
                return false;
            }

            col = db.getCollection("sms_vin_odp");
        }

        BasicDBObject updateFields = new BasicDBObject();
        updateFields.append("otp", otp);
        updateFields.append("otp_time", VinPlayUtils.getCurrentDateTime());
        updateFields.append("count", count);
        BasicDBObject conditions = new BasicDBObject();
        conditions.append("mobile", mobile);
        FindOneAndUpdateOptions options = new FindOneAndUpdateOptions();
        options.upsert(true);
        col.findOneAndUpdate(conditions, new Document("$set", updateFields), options);
        return true;
    }

    public boolean updateOtpSMSFirst(String mobile, String otp, String messageMO) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = null;
        if (messageMO.equals("OZZ OTP")) {
            col = db.getCollection("sms_vin_otp");
        } else if (messageMO.equals("OZZ APP")) {
            col = db.getCollection("sms_vin_app");
        } else {
            if (!messageMO.equals("OZZ ODP")) {
                return false;
            }

            col = db.getCollection("sms_vin_odp");
        }

        BasicDBObject updateFields = new BasicDBObject();
        updateFields.append("otp", otp);
        updateFields.append("otp_time", VinPlayUtils.getCurrentDateTime());
        updateFields.append("count", 1);
        BasicDBObject conditions = new BasicDBObject();
        conditions.append("mobile", mobile);
        FindOneAndUpdateOptions options = new FindOneAndUpdateOptions();
        options.upsert(true);
        col.findOneAndUpdate(conditions, new Document("$set", updateFields), options);
        return true;
    }

    public OtpModel getOtpSMS(String mobile, String commandCode) throws ParseException {
        OtpModel model = null;
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = null;
        if (commandCode.equals("OZZ OTP")) {
            col = db.getCollection("sms_vin_otp");
        } else if (commandCode.equals("OZZ APP")) {
            col = db.getCollection("sms_vin_app");
        } else {
            if (!commandCode.equals("OZZ ODP")) {
                return null;
            }

            col = db.getCollection("sms_vin_odp");
        }

        HashMap<String, Object> conditions = new HashMap();
        conditions.put("mobile", mobile);
        Document doccument = (Document)col.find(new Document(conditions)).first();
        if (doccument != null) {
            if (doccument.getInteger("count") != null) {
                model = new OtpModel(mobile, doccument.getString("otp"), VinPlayUtils.getDateTime(doccument.getString("otp_time")), commandCode, doccument.getInteger("count"));
            } else {
                model = new OtpModel(mobile, doccument.getString("otp"), VinPlayUtils.getDateTime(doccument.getString("otp_time")), commandCode);
            }
        }

        return model;
    }

    public LogSMSOtpResponse getLogSMSOtp(String mobile, String startTime, String endTime, int page, String requestId) {
        final ArrayList<LogSmsOtp> records = new ArrayList();
        final ArrayList<Integer> num = new ArrayList();
        num.add(0, 0);
        num.add(1, 0);
        num.add(2, 0);
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        int numStart = (page - 1) * 50;
        // int numEnd = true;
        BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        HashMap<String, Object> conditions = new HashMap();
        if (!mobile.isEmpty()) {
            conditions.put("mobile", mobile);
        }

        if (!startTime.isEmpty() && !endTime.isEmpty()) {
            BasicDBObject obj = new BasicDBObject();
            obj.put("$gte", startTime);
            obj.put("$lte", endTime);
            conditions.put("trans_time", obj);
        }

        if (requestId != null && !requestId.isEmpty()) {
            conditions.put("request_id", requestId);
        }

        FindIterable iterable = db.getCollection("vmg_transaction").find(new Document(conditions)).sort(objsort).skip(numStart).limit(50);
        iterable.forEach(new Block<Document>() {
            public void apply(Document document) {
                LogSmsOtp model = new LogSmsOtp();
                model.requestId = document.getString("request_id");
                model.mobile = document.getString("mobile");
                model.commandCode = document.getString("command_code");
                model.messageMO = document.getString("message_MO");
                model.responseMO = document.getString("response_MO");
                model.messageMT = document.getString("message_MT");
                model.responseMT = document.getString("response_MT");
                model.transTime = document.getString("trans_time");
                records.add(model);
            }
        });
        FindIterable iterable2 = db.getCollection("vmg_transaction").find(new Document(conditions));
        iterable2.forEach(new Block<Document>() {
            public void apply(Document document) {
                LogSmsOtp model = new LogSmsOtp();
                model.responseMO = document.getString("response_MO");
                model.responseMT = document.getString("response_MT");
                int count = num.get(0) + 1;
                num.set(0, count);
                int numSendSuccess;
                if (model.responseMO.equals("1")) {
                    numSendSuccess = num.get(1) + 1;
                    num.set(1, numSendSuccess);
                }

                if (model.responseMT.equals("1")) {
                    numSendSuccess = num.get(2) + 1;
                    num.set(2, numSendSuccess);
                }

            }
        });
        LogSMSOtpResponse res = new LogSMSOtpResponse(true, "0", num.get(0) / 50 + 1, num.get(0), num.get(1), num.get(2), records);
        return res;
    }

    public DoisoatVmg doisoatVMG(DoisoatVmg model, String startTime, String endTime) {
        final Map<Integer, DoisoatVmgByProvider> providers = model.getProviders();
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        HashMap<String, Object> conditions = new HashMap();
        if (!startTime.isEmpty() && !endTime.isEmpty()) {
            BasicDBObject obj = new BasicDBObject();
            obj.put("$gte", startTime);
            obj.put("$lte", endTime);
            conditions.put("trans_time", obj);
        }

        FindIterable iterable = db.getCollection("vmg_transaction").find(new Document(conditions));
        iterable.forEach(new Block<Document>() {
            public void apply(Document document) {
                String mobile = document.getString("mobile");
                int id = PhoneUtils.getProviderByMobile(mobile, false);
                String messageMO = document.getString("message_MO");
                int mo = PhoneUtils.getNumMessage(messageMO, false);
                String messageMT = document.getString("message_MT");
                int mt = PhoneUtils.getNumMessage(messageMT, false);
                String responseMO = document.getString("response_MO");
                String responseMT = document.getString("response_MT");
                DoisoatVmgByProvider pv = providers.get(id);
                if (responseMO.equals("1")) {
                    pv.setMo(pv.getMo() + (long)mo);
                }

                if (responseMT.equals("1")) {
                    pv.setMt(pv.getMt() + (long)mt);
                }

                providers.put(id, pv);
            }
        });
        model.setProviders(providers);
        return model;
    }
}
