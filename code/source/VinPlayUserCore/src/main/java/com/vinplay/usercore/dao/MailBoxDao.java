/*
 * Decompiled with CFR 0.150.
 *
 * Could not load the following classes:
 *  com.vinplay.vbee.common.response.MailBoxResponse
 */
package com.vinplay.usercore.dao;

import com.vinplay.vbee.common.response.MailBoxResponse;

import java.sql.SQLException;
import java.util.List;

public interface MailBoxDao {
    boolean sendMailBoxFromByNickName(List<String> var1, String var2, String var3);

    boolean sendMailBoxFromByNickNameAdmin(String var1, String var2, String var3);

    List<MailBoxResponse> listMailBox(String var1, int var2);

    int countMailBox(String var1);

    int updateStatusMailBox(String var1);

    int deleteMailBox(String var1);

    boolean sendmailGiftCode(String var1, String var2, String var3, String var4, String var5) throws SQLException;

    int countMailBoxInActive(String var1) throws SQLException;

    boolean sendMailGiftCode(String var1, String var2, String var3, String var4) throws SQLException;

    int deleteMailBoxAdmin(String var1);

    int deleteMutilMailBox(String var1, String var2);

    boolean sendMailCardMobile(String nickName, String provider, String serial, String pin, String title, String content);
    boolean sendMailPopupMessage(String nickName, String title, String content);
}

