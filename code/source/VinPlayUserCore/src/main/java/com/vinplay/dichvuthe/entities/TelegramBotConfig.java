package com.vinplay.dichvuthe.entities;

import casio.king365.util.KingUtil;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.gson.annotations.Expose;
import lombok.Getter;
import lombok.Setter;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@Getter
@Setter
public class TelegramBotConfig implements Serializable {
    private static final long serialVersionUID = 1L;
    @Expose
    protected String token;
    @Expose
    protected String startUrl;
    @Expose
    protected String botNickName;
    @Expose
    protected String csNickName;
    @Expose
    protected String groupNickName;
    @Expose
    protected List<String> adminIds;
    @Expose
    protected List<String> publicGroupIds;
    @Expose
    protected List<String> operationIds;
    @Expose
    protected List<String> clientErrorMonitorIds;
    //	@Expose
    protected String webHookUrl;
    @Expose
    protected byte status;//0:stop, 1:start

    private static String CONFIG_FILE = "config/tele.properties";

    private TelegramBotConfig(
            String token, String startUrl, String botNickName, String csNickName,
            String groupNickName, List<String> adminIds, List<String> publicGroupIds,
            List<String> operationIds, List<String> clientErrorMonitorIds, String webHookUrl, byte status) {
        this.token = token;
        this.startUrl = startUrl;
        this.botNickName = botNickName;
        this.csNickName = csNickName;
        this.groupNickName = groupNickName;
        this.adminIds = adminIds;
        this.publicGroupIds = publicGroupIds;
        this.operationIds = operationIds;
        this.webHookUrl = webHookUrl;
        this.status = status;
        this.clientErrorMonitorIds = clientErrorMonitorIds;
    }

    private static LoadingCache<String, TelegramBotConfig> configCache;

    static {
        configCache = CacheBuilder.newBuilder()
                .expireAfterWrite(60, TimeUnit.MINUTES)
                .build(new CacheLoader<String, TelegramBotConfig>() {  // build the cacheloader
                    @Override
                    public TelegramBotConfig load(String k) {
                        return loadConfigFromFile();
                    }
                });
    }

    private static TelegramBotConfig loadConfigFromFile() {
        String tokenAccess = null;
        String startUrl = null;
        String botNickName = null;
        String csNickName = null;
        String groupNickName = null;
        String adminIds = null;
        String publicGroupIds = null;
        String operationIds = null;
        String clientErrorMonitorIds = null;
        String webHookUrl = null;
        byte status = 1;

        try {
            File fConfigFile = new File(CONFIG_FILE);
            if (fConfigFile.exists()) {
                try (InputStream input = new FileInputStream(CONFIG_FILE)) {
                    Properties prop = new Properties();
                    prop.load(input);

                    tokenAccess = prop.getProperty("TOKEN_ACCESS_HTTP_API");
                    startUrl = prop.getProperty("TELE_GROUP_START_URL");
                    botNickName = prop.getProperty("TELE_GROUP_BOT_NICK_NAME");
                    csNickName = prop.getProperty("TELE_GROUP_CS_NICK_NAME");
                    groupNickName = prop.getProperty("TELE_GROUP_NICK_NAME");
                    adminIds = prop.getProperty("TELE_GROUP_ADMIN_IDS");
                    publicGroupIds = prop.getProperty("TELE_GROUP_PUBLIC_GROUP_IDS");
                    operationIds = prop.getProperty("TELE_GROUP_OPERATION_IDS");
                    clientErrorMonitorIds = prop.getProperty("TELE_GROUP_CLIENT_ERROR_MONITOR_IDS");
                    webHookUrl = prop.getProperty("TELE_GROUP_WEB_HOOK_URL");
                    status = (prop.getProperty("TELE_GROUP_STATUS") != null) ?
                            Byte.parseByte(prop.getProperty(
                                    "TELE_GROUP_STATUS")) : 1;

                } catch (IOException e) {
                    KingUtil.printException(e);
                }

            } else {
                KingUtil.printLog("TeleConfig file config not found!");
            }
        } catch (Exception ex) {
            KingUtil.printException(ex);
        }

        if (tokenAccess == null || tokenAccess.isEmpty()) {
            return null;
        }

        return new TelegramBotConfig(
                tokenAccess,
                startUrl,
                botNickName,
                csNickName,
                groupNickName,
                Arrays.asList(adminIds.split(",")),
                Arrays.asList(publicGroupIds.split(",")),
                Arrays.asList(operationIds.split(",")),
                Arrays.asList(clientErrorMonitorIds.split(",")),
                webHookUrl,
                status);
    }

    public static TelegramBotConfig defaultConfig() {
        try {
            return configCache.get("CACHE_KEY_TELEGRAM");
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return loadConfigFromFile();
    }
}
