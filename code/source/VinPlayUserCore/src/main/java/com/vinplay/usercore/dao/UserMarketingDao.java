/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.vinplay.vbee.common.messages.UserMarketingMessage
 *  com.vinplay.vbee.common.models.MarketingModel
 *  com.vinplay.vbee.common.models.cache.StatisticUserMarketing
 */
package com.vinplay.usercore.dao;

import com.vinplay.vbee.common.messages.UserMarketingMessage;
import com.vinplay.vbee.common.models.MarketingModel;
import com.vinplay.vbee.common.models.cache.StatisticUserMarketing;
import java.sql.SQLException;
import java.util.List;

public interface UserMarketingDao {
    boolean saveUserMarketing(UserMarketingMessage var1);

    boolean saveLoginDailyMarketing(UserMarketingMessage var1);

    boolean updateLoginDailyMarketing(UserMarketingMessage var1);

    List<UserMarketingMessage> getNickNameUserMarketing(String var1, String var2);

    List<String> getCampaignList(String var1) throws SQLException;

    List<String> getSourceList(String var1) throws SQLException;

    List<String> getMediumList(String var1) throws SQLException;

    void logMKTInfo(StatisticUserMarketing var1);

    List<MarketingModel> getHistoryMKT(String var1, String var2, String var3, String var4, String var5);

    MarketingModel getMKTInfo(String var1);
}

