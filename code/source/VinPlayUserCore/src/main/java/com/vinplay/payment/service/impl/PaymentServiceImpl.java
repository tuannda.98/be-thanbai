package com.vinplay.payment.service.impl;

import casio.king365.service.MongoDbService;
import casio.king365.service.impl.MongoDbServiceImpl;
import casio.king365.util.KingUtil;
import com.vinplay.payment.dao.PaymentDao;
import com.vinplay.payment.dao.impl.PaymentDaoImpl;
import com.vinplay.payment.service.PaymentService;
import com.vinplay.usercore.response.LogExchangeMoneyResponse;
import com.vinplay.vbee.common.messages.pay.ExchangeMessage;
import org.bson.Document;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PaymentServiceImpl
        implements PaymentService {
    private final PaymentDao dao = new PaymentDaoImpl();

    MongoDbService mongoDbService = new MongoDbServiceImpl();

    @Override
    public boolean logExchangeMoney(ExchangeMessage message) {
        return this.dao.logExchangeMoney(message);
    }

    @Override
    public boolean checkMerchantTransId(String merchantId, String merchantTransId) {
        return this.dao.checkMerchantTransId(merchantId, merchantTransId);
    }

    @Override
    public LogExchangeMoneyResponse getLogExchangeMoney(String nickname, String merchantId, String transId, String transNo, String type, int code, String startTime, String endTime, int page) throws Exception {
        return this.dao.getLogExchangeMoney(nickname, merchantId, transId, transNo, type, code, startTime, endTime, page);
    }

    @Override
    public long getTotalMoney(String merchantId, String nickname, String startTime, String endTime, String type) throws Exception {
        return this.dao.getTotalMoney(merchantId, nickname, startTime, endTime, type);
    }

    @Override
    public List<ExchangeMessage> getExchangeMoney(String nickname, String merchantId, String transId, String transNo, String type, int code, String startTime, String endTime) throws Exception {
        return this.dao.getExchangeMoney(nickname, merchantId, transId, transNo, type, code, startTime, endTime);
    }

    @Override
    public List<Document> getSuccessRechargeCardData(Date startDate, Date endDate) {
        Map<String, Object> map = new HashMap<>();
        map.put("code", 0);
        Document timespan = new Document();
        timespan.append("$gte", String.valueOf(startDate.getTime()));
        timespan.append("$lt", String.valueOf(endDate.getTime()));
        map.put("time_request", timespan);
        List<Document> result = mongoDbService.find("dvt_recharge_by_gachthe", map, "time_request");
        return result;
    }

    @Override
    public List<Document> getSuccessRechargeMomoData(Date startDate, Date endDate) {
        System.out.println("getMomoData()");
        Map<String, Object> map = new HashMap<>();
        map.put("code", 1);
        map.put("provider", "MOMO");
        Document timespan = new Document();
        timespan.append("$gte", startDate.getTime());
        timespan.append("$lt", endDate.getTime());
        System.out.println("getMomoData() startDate: " + startDate + ", start mili: " + startDate.getTime() + ", endDate: " + endDate + ", end mili: " + endDate.getTime());
        map.put("time_request", timespan);
        List<Document> result = mongoDbService.find("dvt_recharge_by_gachthe", map, "time_request");
        return result;
    }

    @Override
    public List<Document> getSuccessRechargeBankData(Date startDate, Date endDate) {
        System.out.println("getBankData() startDate: " + startDate + ", start mili: " + startDate.getTime() + ", endDate: " + endDate + ", end mili: " + endDate.getTime());
        KingUtil.printLog("getBankData() startDate: " + startDate + ", start mili: " + startDate.getTime() + ", endDate: " + endDate + ", end mili: " + endDate.getTime());
        Map<String, Object> map = new HashMap<>();
        // map.put("provider", "BANK");
        Document timespan = new Document();
        timespan.append("$gte", String.valueOf(startDate.getTime()));
        timespan.append("$lt", String.valueOf(endDate.getTime()));
        map.put("time_request", timespan);
        List<Document> result = mongoDbService.find("dvt_recharge_by_bank", map, "time_request");
        return result;
    }

    @Override
    public List<Document> getSuccessRechargeCardData(String nickName) {
        Map<String, Object> map = new HashMap<>();
        map.put("code", 0);
        map.put("nick_name", nickName);
        List<Document> result = mongoDbService.find("dvt_recharge_by_gachthe", map, "time_request");
        return result;
    }

    @Override
    public List<Document> getSuccessRechargeMomoData(String nickName) {
        System.out.println("getMomoData()");
        Map<String, Object> map = new HashMap<>();
        map.put("code", 1);
        map.put("provider", "MOMO");
        map.put("nick_name", nickName);
        List<Document> result = mongoDbService.find("dvt_recharge_by_gachthe", map, "time_request");
        return result;
    }

    @Override
    public List<Document> getSuccessRechargeBankData(String nickName) {
        Map<String, Object> map = new HashMap<>();
        map.put("nick_name", nickName);
        List<Document> result = mongoDbService.find("dvt_recharge_by_bank", map, "time_request");
        return result;
    }
}

