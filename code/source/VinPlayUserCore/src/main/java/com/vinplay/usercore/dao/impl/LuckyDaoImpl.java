//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.vinplay.usercore.dao.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.vinplay.usercore.dao.LuckyDao;
import com.vinplay.usercore.entities.vqmm.LuckyHistory;
import com.vinplay.usercore.entities.vqmm.LuckyVipHistory;
import com.vinplay.vbee.common.models.cache.SlotFreeDaily;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.pools.ConnectionPool;
import com.vinplay.vbee.common.utils.VinPlayUtils;
import org.apache.log4j.Logger;
import org.bson.Document;

import java.io.UnsupportedEncodingException;
import java.sql.*;
import java.util.Date;
import java.util.*;

public class LuckyDaoImpl implements LuckyDao {

    private static final Logger logger = Logger.getLogger("api");

    public LuckyDaoImpl() {
    }

    public int receiveRotateDaily(int userId, String nickname) throws SQLException {
        Integer rotateCount = -1;
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_minigame");
             CallableStatement call = conn.prepareCall("CALL lucky_receive_rotate_daily(?,?,?)");) {
            call.setInt(1, userId);
            call.setString(2, nickname);
            call.registerOutParameter(3, Types.INTEGER);
            call.executeUpdate();
            rotateCount = call.getInt(3);
        }

        return rotateCount;
    }

    public List<Integer> getRotateCount(int userId, String ipAddress) throws SQLException {
        ArrayList<Integer> result = new ArrayList();
        Integer rotateDaily = -1;
        Integer rotateFree = -1;
        Integer rotateInDay = -1;
        Integer rotateByIp = -1;

        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_minigame");
             CallableStatement call = conn.prepareCall("CALL lucky_get_rotate_count(?,?,?,?,?,?)");) {
            int param = 1;
            //int param = param + 1;
            call.setInt(1, userId);
            call.setString(2, ipAddress);
            call.registerOutParameter(3, 4);
            call.registerOutParameter(4, 4);
            call.registerOutParameter(5, 4);
            call.registerOutParameter(6, 4);
            call.executeUpdate();
            rotateDaily = call.getInt(3);
            rotateFree = call.getInt(4);
            rotateInDay = call.getInt(5);
            rotateByIp = call.getInt(6);
        }

        result.add(rotateDaily);
        result.add(rotateFree);
        result.add(rotateInDay);
        result.add(rotateByIp);
        return result;
    }

    public boolean saveResultLucky(int userId, String nickname, String ipAddress, int rotateDaily, int rotateFree, int rotateInDay, int rotateByIp, int slotFree, String slotName, int slotMaxWin, int slotRoom) throws SQLException, UnsupportedEncodingException {
        boolean success = false;

        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_minigame");
             CallableStatement call = conn.prepareCall("CALL save_result_lucky(?,?,?,?,?,?,?,?,?,?,?)");) {
            int param = 1;
            int var21 = param + 1;
            call.setInt(param, userId);
            call.setString(var21++, nickname);
            call.setString(var21++, ipAddress);
            call.setInt(var21++, rotateDaily);
            call.setInt(var21++, rotateFree);
            call.setInt(var21++, rotateInDay);
            call.setInt(var21++, rotateByIp);
            call.setInt(var21++, slotFree);
            call.setString(var21++, slotName);
            call.setInt(var21++, slotMaxWin);
            call.setInt(var21++, slotRoom);
            call.executeUpdate();
            success = true;
        }

        return success;
    }

    public List<LuckyHistory> getLuckyHistory(String nickname, int page) {
        //int pageSize = true;
        int skipNumber = (page - 1) * 10;
        final ArrayList<LuckyHistory> luckyHistory = new ArrayList();
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        HashMap<String, Object> conditions = new HashMap();
        conditions.put("nick_name", nickname);
        BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        FindIterable iterable = db.getCollection("lucky_new_history").find(new Document(conditions)).sort(objsort).skip(skipNumber).limit(10);
        iterable.forEach(new Block<Document>() {
            public void apply(Document document) {
                LuckyHistory lucky = new LuckyHistory();
                lucky.setTransId(document.getLong("trans_id"));
                lucky.setNickname(document.getString("nick_name"));
                lucky.setResultVin(document.getString("result_vin"));
                lucky.setResultXu(document.getString("result_xu"));
                lucky.setResultSlot(document.getString("result_slot"));
                lucky.setDescription(document.getString("description"));
                lucky.setTransTime(document.getString("trans_time"));
                luckyHistory.add(lucky);
            }
        });
        return luckyHistory;
    }

    public List<LuckyVipHistory> getLuckyVipHistory(String nickname, int page) {
        //int pageSize = true;
        int skipNumber = (page - 1) * 10;
        final ArrayList<LuckyVipHistory> luckyHistory = new ArrayList();
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        HashMap<String, Object> conditions = new HashMap();
        conditions.put("nick_name", nickname);
        BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        FindIterable iterable = db.getCollection("lucky_vip_history").find(new Document(conditions)).sort(objsort).skip(skipNumber).limit(10);
        iterable.forEach(new Block<Document>() {
            public void apply(Document document) {
                LuckyVipHistory lucky = new LuckyVipHistory();
                lucky.setTransId(document.getLong("trans_id"));
                lucky.setNickname(document.getString("nick_name"));
                lucky.setResultVin(document.getInteger("result_vin"));
                lucky.setResultMulti(document.getInteger("result_multi"));
                lucky.setTransTime(document.getString("time_log"));
                luckyHistory.add(lucky);
            }
        });
        return luckyHistory;
    }

    public Map<String, SlotFreeDaily> getAllSlotFree() throws SQLException {
        HashMap<String, SlotFreeDaily> res = new HashMap();
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpool_minigame");
             PreparedStatement stm = conn.prepareStatement("SELECT * FROM rotate_slot_free WHERE DATE(update_time) = CURDATE()");
             ResultSet rs = stm.executeQuery();) {

            while (rs.next()) {
                String gameName = rs.getString("game_name");
                String nickname = rs.getString("nick_name");
                int room = rs.getInt("room");
                int rotateFree = rs.getInt("rotate_free");
                long maxWin = rs.getInt("max_winning");
                SlotFreeDaily model = new SlotFreeDaily(rotateFree, maxWin);
                String key = nickname + "-" + gameName + "-" + room;
                res.put(key, model);
            }

            return res;
        }
    }

    public boolean logLuckyVip(long transId, String nickname, String month, int resultVin, int resultMulti) throws Exception {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("lucky_vip_history");
        Document doc = new Document();
        doc.append("trans_id", transId);
        doc.append("nick_name", nickname);
        doc.append("month", month);
        doc.append("result_vin", resultVin);
        doc.append("result_multi", resultMulti);
        doc.append("time_log", VinPlayUtils.getCurrentDateTime());
        doc.append("create_time", new Date());
        col.insertOne(doc);
        return true;
    }

    public int getLuckyVipInMonth(String nickname, String month) throws Exception {
        final ArrayList res = new ArrayList();
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        HashMap<String, Object> conditions = new HashMap();
        conditions.put("nick_name", nickname);
        conditions.put("month", month);
        FindIterable iterable = db.getCollection("lucky_vip_history").find(new Document(conditions));
        if (iterable != null) {
            iterable.forEach(new Block<Document>() {
                public void apply(Document document) {
                    res.add(1);
                }
            });
        }

        return res.size();
    }

    public long getLuckyVipLastReferenceId() {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        HashMap conditions = new HashMap();
        BasicDBObject objsort = new BasicDBObject();
        objsort.put("_id", -1);
        FindIterable iterable = db.getCollection("lucky_vip_history").find(new Document(conditions)).sort(objsort).limit(1);
        Document document = iterable != null ? (Document) iterable.first() : null;
        long transId = document == null ? 0L : document.getLong("trans_id");
        return transId;
    }
}
