/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.usercore.service;

import java.sql.SQLException;
import java.util.List;

public interface GiftCodeXCService {
    List<String> loadAllGiftcode() throws SQLException;
}

