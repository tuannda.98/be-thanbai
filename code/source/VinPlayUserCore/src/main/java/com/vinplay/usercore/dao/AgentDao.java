/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.usercore.dao;

import java.sql.SQLException;

public interface AgentDao {
    boolean checkSMSAgent(String var1, long var2) throws SQLException;

    String getNicknameAgent1(String var1) throws SQLException;

    String getAgentLevel1ByNickName(String var1) throws SQLException;
}

