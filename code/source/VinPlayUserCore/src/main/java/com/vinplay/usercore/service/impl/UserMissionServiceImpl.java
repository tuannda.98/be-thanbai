package com.vinplay.usercore.service.impl;

import casio.king365.core.HCMap;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.vinplay.usercore.dao.impl.UserDaoImpl;
import com.vinplay.usercore.service.UserMissionService;
import com.vinplay.usercore.utils.GameCommon;
import com.vinplay.vbee.common.enums.MissionName;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.messages.LogMoneyUserMessage;
import com.vinplay.vbee.common.messages.MoneyMessageInMinigame;
import com.vinplay.vbee.common.messages.userMission.LogReceivedRewardMissionMessage;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.models.userMission.*;
import com.vinplay.vbee.common.rmq.RMQApi;
import com.vinplay.vbee.common.utils.VinPlayUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserMissionServiceImpl
        implements UserMissionService {

    @Override
    public UserMissionResponse getUserMission(String nickName) throws Exception {
        List<MissionObj> listMissionVin = this.getUserMission(
                nickName, "vin", GameCommon.getValueInt("MAX_LEVEL_MISSION"));
        if (listMissionVin == null || listMissionVin.size() == 0) {
            ArrayList<String> bonusVin = new ArrayList<>();
            for (int i = 0; i <= 20; ++i) {
                bonusVin.add(GameCommon.getValueStr("BONUS_VIN_" + i));
            }
            listMissionVin = this.initUserMission(
                    nickName, "vin", GameCommon.getValueStr("MATCH_MAX_VIN"), bonusVin);
            if (listMissionVin == null) {
                UserMissionResponse response = new UserMissionResponse();
                response.setError("1052");
                return response;
            }
        }

        ArrayList<MissionResponse> lMisVin = new ArrayList<>();
        for (MissionObj obj : listMissionVin) {
            MissionName enumMissionName = MissionName.getMissionByName(obj.getMisNa());
            if (enumMissionName == null) {
                UserMissionResponse response = new UserMissionResponse();
                response.setError("1049");
                return response;
            }
            if (enumMissionName.getShow() != 0 || enumMissionName.getType() == 2) {
                continue;
            }
            MissionResponse missionResponse = new MissionResponse();
            missionResponse.setMisNa(obj.getMisNa());
            missionResponse.setMisWin(obj.getMisWin());
            missionResponse.setMisMax(obj.getMisMax());
            if (obj.getMisWin() == obj.getMisMax() && obj.getMisLev() == obj.getRecReLev() + 1) {
                missionResponse.setRecReLev(0);
            } else {
                missionResponse.setRecReLev(1);
            }
            if (obj.getMisWin() == obj.getMisMax() && obj.getMisLev() == obj.getRecReLev()) {
                missionResponse.setCompAllLev(0);
            } else {
                missionResponse.setCompAllLev(1);
            }
            String moneyBonus = GameCommon.getValueStr("BONUS_VIN_" + enumMissionName.getId());
            String[] moneyBonusArr = moneyBonus.split(",");
            missionResponse.setMoBo(Long.parseLong(moneyBonusArr[obj.getMisLev() - 1]));
            lMisVin.add(missionResponse);
        }
        List<MissionObj> listMissionXu = this.getUserMission(
                nickName, "xu", GameCommon.getValueInt("MAX_LEVEL_MISSION"));
        if (listMissionXu == null || listMissionXu.size() == 0) {
            ArrayList<String> bonusXu = new ArrayList<String>();
            for (int j = 0; j <= 20; ++j) {
                bonusXu.add(GameCommon.getValueStr("BONUS_XU_" + j));
            }
            listMissionXu = this.initUserMission(
                    nickName, "xu", GameCommon.getValueStr("MATCH_MAX_XU"), bonusXu);
            if (listMissionXu == null) {
                UserMissionResponse response = new UserMissionResponse();
                response.setError("1052");
                return response;
            }
        }
        ArrayList<MissionResponse> lMisXu = new ArrayList<MissionResponse>();
        for (MissionObj obj2 : listMissionXu) {
            MissionName enumMissionName2 = MissionName.getMissionByName(obj2.getMisNa());
            if (enumMissionName2 == null) {
                UserMissionResponse response = new UserMissionResponse();
                response.setError("1049");
                return response;
            }
            if (enumMissionName2.getShow() != 0 || enumMissionName2.getType() == 2) continue;
            MissionResponse missionResponse2 = new MissionResponse();
            missionResponse2.setMisNa(obj2.getMisNa());
            missionResponse2.setMisWin(obj2.getMisWin());
            missionResponse2.setMisMax(obj2.getMisMax());
            if (obj2.getMisWin() == obj2.getMisMax() && obj2.getMisLev() == obj2.getRecReLev() + 1) {
                missionResponse2.setRecReLev(0);
            } else {
                missionResponse2.setRecReLev(1);
            }
            if (obj2.getMisWin() == obj2.getMisMax() && obj2.getMisLev() == obj2.getRecReLev()) {
                missionResponse2.setCompAllLev(0);
            } else {
                missionResponse2.setCompAllLev(1);
            }
            String moneyBonus2 = GameCommon.getValueStr("BONUS_XU_" + enumMissionName2.getId());
            String[] moneyBonusArr2 = moneyBonus2.split(",");
            missionResponse2.setMoBo(Long.parseLong(moneyBonusArr2[obj2.getMisLev() - 1]));
            lMisXu.add(missionResponse2);
        }
        UserMissionResponse response = new UserMissionResponse();
        response.setnN(nickName);
        response.setError("0");
        response.setlMisVin(lMisVin);
        response.setlMisXu(lMisXu);
        response.setDepTaiXiuVin(GameCommon.getValueInt("MIN_TAI_XIU_VIN"));
        response.setDepTaiXiuXu(GameCommon.getValueInt("MIN_TAI_XIU_XU"));
        return response;
    }

    private List<MissionObj> getUserMission(
            String nickName, String moneyType, int maxLevel) throws Exception {
        String cacheName = "";
        cacheName = moneyType.equals("vin") ? "cacheUserMissionVin" : "cacheUserMissionXu";
        HazelcastInstance client = HazelcastClientFactory.getInstance();
        IMap<String, UserMissionCacheModel> userMissionMap = client.getMap(cacheName);
        if (!userMissionMap.containsKey(nickName)) {
            UserDaoImpl dao = new UserDaoImpl();
            UserMissionCacheModel userMissionCacheModel = dao.getListMissionByNickName(nickName, moneyType, maxLevel);
            userMissionCacheModel.setLastMessageId(0L);
            userMissionCacheModel.setLastActive(new Date());
            userMissionMap.put(nickName, userMissionCacheModel);
        }
        userMissionMap.lock(nickName);
        UserMissionCacheModel userMissionCacheModel = (UserMissionCacheModel) userMissionMap.get(nickName);
        List<MissionObj> listMissionResponse = userMissionCacheModel.getListMission();
        userMissionMap.unlock(nickName);
        return listMissionResponse;
    }

    private List<MissionObj> initUserMission(
            String nickName, String moneyType, String matchMax, List<String> bonusVin) throws Exception {
        String cacheName = moneyType.equals("vin") ? "cacheUserMissionVin" : "cacheUserMissionXu";
        HazelcastInstance client = HazelcastClientFactory.getInstance();
        IMap<String, UserMissionCacheModel> userMissionMap = client.getMap(cacheName);
        UserMissionCacheModel userMissionCacheModel = new UserMissionCacheModel();
        String[] matchMaxArr = matchMax.split(",");
        ArrayList<MissionObj> listMission = new ArrayList<MissionObj>();
        UserDaoImpl dao = new UserDaoImpl();
        UserModel userModel = dao.getUserByNickName(nickName);
        if (userModel == null) {
            return null;
        }
        for (int i = 0; i < bonusVin.size(); ++i) {
            MissionObj obj = new MissionObj(MissionName.getMissionById(i).getName(), 1, 0, Integer.parseInt(matchMaxArr[0]), false, false, 0);
            listMission.add(obj);
            dao.insertUserMission(moneyType, obj, userModel);
        }
        userMissionCacheModel.setLastActive(new Date());
        userMissionCacheModel.setLastMessageId(Long.parseLong(VinPlayUtils.genMessageId()));
        userMissionCacheModel.setListMission(listMission);
        userMissionCacheModel.setNickName(nickName);
        userMissionCacheModel.setUserId(userModel.getId());
        userMissionCacheModel.setUserName(userModel.getUsername());
        userMissionMap.put(nickName, userMissionCacheModel);
        return listMission;
    }

    /*
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    private CompleteMissionObj updateMissionCompleteTH1(
            String nickName, String gameName, String moneyType, int maxLevel, String matchMax, String moneyBonus) throws Exception {
        CompleteMissionObj response = new CompleteMissionObj(false, 0L, 0L, "1001");
        String cacheName = "";
        cacheName = moneyType.equals("vin") ? "cacheUserMissionVin" : "cacheUserMissionXu";
        HazelcastInstance client = HazelcastClientFactory.getInstance();
        IMap userMissionMap = client.getMap(cacheName);
        IMap userMap = client.getMap("users");
        try {
            UserDaoImpl dao = new UserDaoImpl();
            userMissionMap.lock(nickName);
            userMap.lock(nickName);
            UserMissionCacheModel userMission = (UserMissionCacheModel) userMissionMap.get(nickName);
            UserCacheModel user = (UserCacheModel) userMap.get(nickName);
            List<MissionObj> listMission = userMission.getListMission();
            ArrayList<MissionObj> listMissionReturn = new ArrayList<MissionObj>();
            String[] moneyBonusArr = moneyBonus.split(",");
            long moneyExchange = 0L;
            String des = "";
            int levelReceivedReward = 0;
            for (MissionObj obj : listMission) {
                if (obj.getMisNa().equals(gameName)) {
                    CompleteMissionObj completeMissionObj;
                    if (obj.getMisLev() == obj.getRecReLev()) {
                        CompleteMissionObj completeMissionObj2;
                        response.setError("1048");
                        completeMissionObj = completeMissionObj2 = response;
                        return completeMissionObj;
                    }
                    if (obj.getMisWin() < obj.getMisMax()) {
                        CompleteMissionObj completeMissionObj3;
                        response.setError("1047");
                        completeMissionObj = completeMissionObj3 = response;
                        return completeMissionObj;
                    }
                    moneyExchange = Long.parseLong(moneyBonusArr[obj.getMisLev() - 1]);
                    des = "Th\u00c6\u00b0\u00e1\u00bb\u0178ng Nhi\u00e1\u00bb\u2021m V\u00e1\u00bb\u00a5 " + gameName + " - Level " + obj.getMisLev() + " - " + moneyType;
                    levelReceivedReward = obj.getMisLev();
                    if (obj.getMisLev() == maxLevel) {
                        obj.setCompAllLev(true);
                        obj.setCompMis(true);
                        obj.setRecReLev(obj.getMisLev());
                    } else {
                        String[] matchMaxArr = matchMax.split(",");
                        obj.setMisMax(Integer.parseInt(matchMaxArr[obj.getMisLev()]));
                        obj.setMisWin(0);
                        obj.setRecReLev(obj.getMisLev());
                        obj.setMisLev(obj.getMisLev() + 1);
                        obj.setCompAllLev(false);
                        obj.setCompMis(false);
                    }
                    dao.updateUserMission(moneyType, nickName, obj);
                }
                listMissionReturn.add(obj);
            }
            userMission.setListMission(listMissionReturn);
            userMission.setLastActive(new Date());
            userMissionMap.put(nickName, userMission);
            long moneyUser = user.getVin();
            long currentMoney = user.getVinTotal();
            user.setVin(moneyUser += moneyExchange);
            user.setVinTotal(currentMoney += moneyExchange);
            userMap.put(nickName, user);
            MoneyMessageInMinigame messageMoney = new MoneyMessageInMinigame(VinPlayUtils.genMessageId(), userMission.getUserId(), userMission.getNickName(), "NhiemVu", moneyUser, currentMoney, moneyExchange, moneyType, 0L, 0, 0);
            RMQApi.publishMessagePayment(messageMoney, 16);
            LogMoneyUserMessage messageLog = new LogMoneyUserMessage(userMission.getUserId(), userMission.getNickName(), "NhiemVu", "Th\u00c6\u00b0\u00e1\u00bb\u0178ng Nhi\u00e1\u00bb\u2021m V\u00e1\u00bb\u00a5", currentMoney, moneyExchange, "vin", des, 0L, false, user.isBot());
            RMQApi.publishMessageLogMoney(messageLog);
            LogReceivedRewardMissionMessage messageLogReceivedReward = new LogReceivedRewardMissionMessage(userMission.getUserId(), userMission.getUserName(), userMission.getNickName(), gameName, levelReceivedReward, moneyExchange, moneyUser, moneyType);
            RMQApi.publishMessage("queue_user_mission", messageLogReceivedReward, 902);
            response.setUpdateSuccess(true);
            response.setError("0");
            response.setMoneyBonus(moneyExchange);
            response.setMoneyUser(moneyUser);
            CompleteMissionObj completeMissionObj = response;
            return completeMissionObj;
        } finally {
            userMissionMap.unlock(nickName);
            userMap.unlock(nickName);
        }
    }

    /*
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    private CompleteMissionObj updateMissionCompleteTH2(
            String nickName, String gameName, String moneyType, int maxLevel, String matchMax, String moneyBonus) throws Exception {
        CompleteMissionObj response = new CompleteMissionObj(false, 0L, 0L, "1001");
        String cacheName = "";
        cacheName = moneyType.equals("vin") ? "cacheUserMissionVin" : "cacheUserMissionXu";
        HazelcastInstance client = HazelcastClientFactory.getInstance();
        IMap userMissionMap = client.getMap(cacheName);
        IMap userMap = client.getMap("users");
        try {
            userMap.lock(nickName);
            UserCacheModel user = (UserCacheModel) userMap.get(nickName);
            UserDaoImpl dao = new UserDaoImpl();
            UserMissionCacheModel userMission = dao.getListMissionByNickName(nickName, moneyType, maxLevel);
            List<MissionObj> listMission = userMission.getListMission();
            ArrayList<MissionObj> listMissionReturn = new ArrayList<MissionObj>();
            String[] moneyBonusArr = moneyBonus.split(",");
            long moneyExchange = 0L;
            String des = "";
            int levelReceivedReward = 0;
            for (MissionObj obj : listMission) {
                if (obj.getMisNa().equals(gameName)) {
                    CompleteMissionObj completeMissionObj;
                    if (obj.getMisLev() == obj.getRecReLev()) {
                        CompleteMissionObj completeMissionObj2;
                        response.setError("1048");
                        completeMissionObj = completeMissionObj2 = response;
                        return completeMissionObj;
                    }
                    if (obj.getMisWin() < obj.getMisMax()) {
                        CompleteMissionObj completeMissionObj3;
                        response.setError("1047");
                        completeMissionObj = completeMissionObj3 = response;
                        return completeMissionObj;
                    }
                    moneyExchange = Long.parseLong(moneyBonusArr[obj.getMisLev() - 1]);
                    des = "Th\u00c6\u00b0\u00e1\u00bb\u0178ng Nhi\u00e1\u00bb\u2021m V\u00e1\u00bb\u00a5 " + gameName + " - Level " + obj.getMisLev() + " - " + moneyType;
                    levelReceivedReward = obj.getMisLev();
                    if (obj.getMisLev() == maxLevel) {
                        obj.setCompAllLev(true);
                        obj.setCompMis(true);
                        obj.setRecReLev(obj.getMisLev());
                    } else {
                        String[] matchMaxArr = matchMax.split(",");
                        obj.setMisMax(Integer.parseInt(matchMaxArr[obj.getMisLev()]));
                        obj.setMisWin(0);
                        obj.setRecReLev(obj.getMisLev());
                        obj.setMisLev(obj.getMisLev() + 1);
                        obj.setCompAllLev(false);
                        obj.setCompMis(false);
                    }
                    dao.updateUserMission(moneyType, nickName, obj);
                }
                listMissionReturn.add(obj);
            }
            userMission.setListMission(listMissionReturn);
            userMission.setLastActive(new Date());
            userMissionMap.put(nickName, userMission);
            long moneyUser = user.getVin();
            long currentMoney = user.getVinTotal();
            user.setVin(moneyUser += moneyExchange);
            user.setVinTotal(currentMoney += moneyExchange);
            userMap.put(nickName, user);
            MoneyMessageInMinigame messageMoney = new MoneyMessageInMinigame(VinPlayUtils.genMessageId(), userMission.getUserId(), userMission.getNickName(), "NhiemVu", moneyUser, currentMoney, moneyExchange, moneyType, 0L, 0, 0);
            RMQApi.publishMessagePayment(messageMoney, 16);
            LogMoneyUserMessage messageLog = new LogMoneyUserMessage(userMission.getUserId(), userMission.getNickName(), "NhiemVu", "Th\u00c6\u00b0\u00e1\u00bb\u0178ng Nhi\u00e1\u00bb\u2021m V\u00e1\u00bb\u00a5", currentMoney, moneyExchange, "vin", des, 0L, false, user.isBot());
            RMQApi.publishMessageLogMoney(messageLog);
            LogReceivedRewardMissionMessage messageLogReceivedReward = new LogReceivedRewardMissionMessage(userMission.getUserId(), userMission.getUserName(), userMission.getNickName(), gameName, levelReceivedReward, moneyExchange, moneyUser, moneyType);
            RMQApi.publishMessage("queue_user_mission", messageLogReceivedReward, 902);
            response.setUpdateSuccess(true);
            response.setError("0");
            response.setMoneyBonus(moneyExchange);
            response.setMoneyUser(moneyUser);
            CompleteMissionObj completeMissionObj = response;
            return completeMissionObj;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            userMap.unlock(nickName);
        }
    }

    @Override
    public CompleteMissionObj completeMission(
            String nickName, String gameName, String moneyType) throws Exception {
        CompleteMissionObj response = new CompleteMissionObj(false, 0L, 0L, "1001");
        String moneyBonus = "";
        MissionName enumMissionName = MissionName.getMissionByName(gameName);
        if (enumMissionName == null) {
            response.setError("1049");
            return response;
        }
        IMap userMap = HCMap.getUsersMap();
        if (!userMap.containsKey(nickName)) {
            response.setError("1051");
            return response;
        }

        moneyBonus = moneyType.equals("vin")
                ? GameCommon.getValueStr("BONUS_VIN_" + enumMissionName.getId())
                : GameCommon.getValueStr("BONUS_XU_" + enumMissionName.getId());
        String matchMax = moneyType.equals("vin")
                ? GameCommon.getValueStr("MATCH_MAX_VIN")
                : GameCommon.getValueStr("MATCH_MAX_XU");
        String cacheName = moneyType.equals("vin") ? "cacheUserMissionVin" : "cacheUserMissionXu";
        HazelcastInstance client = HazelcastClientFactory.getInstance();
        IMap userMissionMap = client.getMap(cacheName);
        response = userMissionMap.containsKey(nickName)
                ? this.updateMissionCompleteTH1(nickName, gameName, moneyType, GameCommon.getValueInt("MAX_LEVEL_MISSION"), matchMax, moneyBonus)
                : this.updateMissionCompleteTH2(nickName, gameName, moneyType, GameCommon.getValueInt("MAX_LEVEL_MISSION"), matchMax, moneyBonus);
        return response;
    }

    @Override
    public NumberCompleteMissionObj getNumberCompleteMission(String nickName) throws Exception {
        return new NumberCompleteMissionObj(
                this.getNumCompMiss("vin", nickName), this.getNumCompMiss("xu", nickName));
    }

    private int getNumCompMiss(String moneyType, String nickName) throws Exception {
        int numberComplete;
        block13:
        {
            numberComplete = 0;
            try {
                String cacheName = "";
                if (moneyType.equals("vin")) {
                    cacheName = "cacheUserMissionVin";
                } else if (moneyType.equals("xu")) {
                    cacheName = "cacheUserMissionXu";
                }
                HazelcastInstance client = HazelcastClientFactory.getInstance();
                IMap userMission = client.getMap(cacheName);
                if (userMission.containsKey(nickName)) {
                    try {
                        userMission.lock(nickName);
                        UserMissionCacheModel user = (UserMissionCacheModel) userMission.get(nickName);
                        List<MissionObj> listMission = user.getListMission();
                        for (MissionObj obj : listMission) {
                            if (obj.getMisWin() != obj.getMisMax() || obj.getMisLev() != obj.getRecReLev() + 1)
                                continue;
                            ++numberComplete;
                        }
                        break block13;
                    } catch (Exception e) {
                        e.printStackTrace();
                        throw e;
                    } finally {
                        userMission.unlock(nickName);
                    }
                }
                UserDaoImpl dao = new UserDaoImpl();
                UserMissionCacheModel user2 = dao.getListMissionByNickName(
                        nickName, moneyType, GameCommon.getValueInt("MAX_LEVEL_MISSION"));
                List<MissionObj> listMission2 = user2.getListMission();
                for (MissionObj obj2 : listMission2) {
                    if (obj2.getMisWin() != obj2.getMisMax() || obj2.getMisLev() != obj2.getRecReLev() + 1) continue;
                    ++numberComplete;
                }
            } catch (Exception e2) {
                e2.printStackTrace();
                throw e2;
            }
        }
        return numberComplete;
    }
}
