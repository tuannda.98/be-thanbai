//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.vinplay.khothe;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class BuyCardRequestObj {
    private String partner_id;
    private String partner_reference;
    private int quantity;
    private String telco;
    private int denomination;
    private String signature;

    public BuyCardRequestObj() {
    }

    public String getPartner_id() {
        return this.partner_id;
    }

    public void setPartner_id(String partner_id) {
        this.partner_id = partner_id;
    }

    public String getPartner_reference() {
        return this.partner_reference;
    }

    public void setPartner_reference(String partner_reference) {
        this.partner_reference = partner_reference;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getTelco() {
        return this.telco;
    }

    public void setTelco(String telco) {
        this.telco = telco;
    }

    public int getDenomination() {
        return this.denomination;
    }

    public void setDenomination(int denomination) {
        this.denomination = denomination;
    }

    public String getSignature() {
        return this.signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String toJson() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException var2) {
            return "{\"code\":500,\"message\":\"error\"}";
        }
    }
}
