/*
 * Decompiled with CFR 0.150.
 *
 * Could not load the following classes:
 *  com.fasterxml.jackson.databind.ObjectMapper
 *  org.apache.http.HttpEntity
 *  org.apache.http.client.methods.CloseableHttpResponse
 *  org.apache.http.client.methods.HttpGet
 *  org.apache.http.client.methods.HttpPost
 *  org.apache.http.client.methods.HttpUriRequest
 *  org.apache.http.entity.StringEntity
 *  org.apache.http.impl.client.CloseableHttpClient
 *  org.apache.http.impl.client.HttpClients
 *  org.apache.http.util.EntityUtils
 */
package com.vinplay.dichvuthe.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class ApacheHttpClient {
    public static final int TIME_OUT = 60000;

    public static String get(String uri) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String responseString;
        try (CloseableHttpClient client = HttpClients.createDefault()) {
            HttpGet httpGet = new HttpGet(uri);
            httpGet.setHeader("Accept", "application/json");
            httpGet.setHeader("Content-type", "application/json");
            HttpEntity entityResponse;
            try (CloseableHttpResponse res = client.execute(httpGet)) {
                entityResponse = res.getEntity();
            }
            responseString = EntityUtils.toString(entityResponse, "UTF-8");
        }
        return responseString;
    }

    public static String post(String uri, String postData) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String responseString;
        try (CloseableHttpClient client = HttpClients.createDefault()) {
            HttpPost httpPost = new HttpPost(uri);
            StringEntity entity = new StringEntity(postData);
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            HttpEntity entityResponse;
            try (CloseableHttpResponse res = client.execute(httpPost)) {
                entityResponse = res.getEntity();
            }
            responseString = EntityUtils.toString(entityResponse, "UTF-8");
        }
        return responseString;
    }
}

