package com.vinplay.usercore.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.hazelcast.core.IMap;
import com.vinplay.vbee.common.messages.GiftCodeMessage;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.response.GiftCodeByNickNameResponse;
import com.vinplay.vbee.common.response.GiftCodeCountResponse;
import com.vinplay.vbee.common.response.GiftCodeDeleteResponse;
import com.vinplay.vbee.common.response.GiftCodeResponse;
import com.vinplay.vbee.common.response.GiftCodeUpdateResponse;
import com.vinplay.vbee.common.response.ReportGiftCodeResponse;
import com.vinplay.vbee.common.response.giftcode.GiftcodeStatisticObj;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

public interface GiftCodeService {
    boolean xuatGiftCode(GiftCodeMessage var1);

    GiftCodeUpdateResponse updateGiftCode(String var1, String var2) throws SQLException, ParseException;

    GiftCodeUpdateResponse updateSpecialGiftCode(String var1, String var2, int var3, int var4) throws SQLException, ParseException;

    List<GiftCodeResponse> searchAllGiftCode(String var1, String var2, String var3, String var4, String var5, String var6, String var7, String var8, int var9, int var10, String var11, String var12, String var13, String var14);

    List<GiftCodeResponse> searchAllGiftCodeAdmin(String var1, String var2, String var3, String var4, String var5, String var6, int var7, int var8, String var9);

    List<GiftCodeCountResponse> countGiftCodeByPrice(String var1, String var2, String var3, String var4, String var5, String var6, String var7, String var8) throws SQLException;

    List<GiftcodeStatisticObj> thongKeGiftcodeDaXuat(String var1, String var2, String var3, String var4, String var5);

    List<GiftCodeCountResponse> countGiftCodeByPriceAdmin(String var1, String var2, String var3, String var4, String var5, String var6) throws SQLException;

    List<ReportGiftCodeResponse> ToolReportGiftCode(String var1, String var2, String var3, String var4, String var5, String var6, String var7) throws SQLException, ParseException, JsonProcessingException;

    boolean genGiftCode(GiftCodeMessage var1);

    List<String> loadAllGiftcode(String gia, String dotphathanh);

    long countsearchAllGiftCode(String var1, String var2, String var3, String var4, String var5, String var6, String var7, String var8, String var9, String var10, String var11, String var12);

    long countsearchAllGiftCodeAdmin(String var1, String var2, String var3, String var4, String var5, String var6, String var7);

    List<ReportGiftCodeResponse> ToolReportGiftCodeBySource(String var1, String var2, String var3, String var4, String var5, int var6, String var7, String var8) throws SQLException, ParseException, JsonProcessingException;

    String uploadFileGiftCode(String var1, long var2, long var4);

    boolean RestoreGiftCode(String var1, String var2, String var3, String var4);

    List<GiftCodeResponse> searchAllGiftCodeByNickName(String var1, int var2);

    long countAllGiftCodeByNickName(String var1);

    GiftCodeByNickNameResponse getUserInfoByGiftCode(String var1, IMap<String, UserCacheModel> var2, int var3, int var4);

    GiftCodeDeleteResponse DeleteGiftCode(String var1, String var2, String var3, String var4);

    void loadAllGiftcodeIntoCache();
}

