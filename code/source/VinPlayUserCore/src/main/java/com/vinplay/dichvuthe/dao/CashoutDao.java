/*
 * Decompiled with CFR 0.150.
 *
 * Could not load the following classes:
 *  org.bson.Document
 */
package com.vinplay.dichvuthe.dao;

import com.vinplay.dichvuthe.entities.BankAccountInfo;
import com.vinplay.dichvuthe.response.CashoutTransResponse;
import com.vinplay.dichvuthe.response.CashoutUserDailyResponse;
import com.vinplay.vbee.common.messages.dvt.CashoutByBankMessage;
import com.vinplay.vbee.common.messages.dvt.CashoutByCardMessage;
import com.vinplay.vbee.common.response.cashout.BankRequest;
import com.vinplay.vbee.common.response.cashout.MomoCashout;
import java.sql.SQLException;
import java.util.List;
import org.bson.Document;

public interface CashoutDao {
    long getSystemCashout() throws SQLException;

    boolean updateSystemCashout(long var1) throws SQLException;

    void logCashoutByBank(CashoutByBankMessage var1) throws Exception;

    BankAccountInfo getBankAccountInfo(String var1) throws SQLException;

    CashoutUserDailyResponse getCashoutUserToday(String var1);

    List<CashoutTransResponse> getListCashoutByCardPending() throws Exception;

    void updateCashOutByCard(String var1, String var2, int var3, String var4, int var5, int var6) throws Exception;

    void insertCardIntoDB(String var1, int var2, String var3, String var4, String var5) throws Exception;

    List<CashoutTransResponse> getListCashoutByCardPending(String var1, String var2, String var3) throws Exception;

    void logCashoutByMomo(String var1, String var2, String var3, String var4, int var5, int var6, String var7, String var8, int var9, String var10) throws Exception;

    long countNumberExchangeInday(String var1);

    void requestCashoutByBank(CashoutByBankMessage var1) throws Exception;

    void requestCashoutByCard(CashoutByCardMessage var1) throws Exception;

    List<MomoCashout> getListMomoCashout(int var1, int var2, String var3, int var4);

    long getListMomoCashoutCount(String var1, int var2);

    List<BankRequest> getListBankRequest(int var1, int var2, String var3, int var4);

    List<CashoutByCardMessage> getListCardRequest(int var1, int var2, String var3, int var4);

    long getListBankRequestCount(String var1, int var2);

    long getListCardRequestCount(String var1, int var2);

    Document GetBankRequestById(String var1);

    Document GetCardRequestById(String var1);

    boolean UpdateBankRequest(String var1, int var2);

    boolean UpdateCardRequest(String var1, int var2);

    Document GetMomoRequestById(String var1);

    boolean UpdateMomoRequest(String var1, int var2);
}

