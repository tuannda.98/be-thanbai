package com.vinplay.channel;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.pengrad.telegrambot.Callback;
import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.model.request.Keyboard;
import com.pengrad.telegrambot.model.request.KeyboardButton;
import com.pengrad.telegrambot.model.request.ReplyKeyboardMarkup;
import com.pengrad.telegrambot.request.SendMessage;
import com.pengrad.telegrambot.response.SendResponse;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class TelegramUtil {
    public static final String SEND_MESSAGE_URL = "https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s";
    public static final String SET_WEB_HOOK_URL = "https://api.telegram.org/bot%s/setWebhook?url=%s";
    public static final String ENCODING = "UTF-8";

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(TelegramUtil.class);
    static OkHttpClient client = new OkHttpClient();
    static LoadingCache<String, TelegramBot> bots;

    static {
        bots = CacheBuilder.newBuilder().expireAfterWrite(60, TimeUnit.MINUTES)
                .build(new CacheLoader<String, TelegramBot>() {  // build the cacheloader
                    @Override
                    public TelegramBot load(String k) throws Exception {
                        return new TelegramBot(k);
                    }
                });
    }

    public static boolean sendOtp(
            String token, String senderId, String code, com.pengrad.telegrambot.Callback callback) {
        Keyboard keyboard = new ReplyKeyboardMarkup(new KeyboardButton[]{new KeyboardButton("OTP")});
        SendMessage sendRequest = new SendMessage(senderId, code);
        sendRequest.replyMarkup(keyboard);
        SendResponse res = TelegramUtil.getBot(token).execute(sendRequest);
        return res.isOk();
    }

    public static boolean sendOtp(String token, String senderId, String code) {
        Keyboard keyboard = new ReplyKeyboardMarkup(new KeyboardButton[]{new KeyboardButton("OTP")});
        SendMessage sendRequest = new SendMessage(senderId, code);
        sendRequest.replyMarkup(keyboard);
        SendResponse res = TelegramUtil.getBot(token).execute(sendRequest);
        return res.isOk();
    }

    public static TelegramBot getBot(String botToken) {
        try {
            return bots.get(botToken);
        } catch (ExecutionException e) {
            LOG.error("Telegram getBot: ", e);
        }
        return null;
    }

    private static String doGetRequest(String url) throws IOException {
        Request request = new Request.Builder().url(url).build();

        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }
    }

    public static String setWebHook(
            String botApiToken, String url) throws Exception {
        String urlHook = URLEncoder.encode(url, ENCODING);
        String urlString = String.format(SET_WEB_HOOK_URL, botApiToken, urlHook);

        return doGetRequest(urlString);
    }

    public static String sendMessage(
            String botApiToken, String chatId, String text) throws Exception {
        SendMessage sendRequest = new SendMessage(chatId, text);
        return getBot(botApiToken).execute(sendRequest).message().text();
    }

    public static void asyncSendMessage(
            String botApiToken, String chatId, String text) {
        SendMessage sendRequest = new SendMessage(chatId, text);
        asyncSendMessage(botApiToken, sendRequest);
    }

    public static void asyncSendMessage(
            String botApiToken, SendMessage sendRequest) {
        getBot(botApiToken).execute(sendRequest, new Callback<SendMessage, SendResponse>() {
            @Override
            public void onResponse(SendMessage request, SendResponse response) {
                LOG.info("{} Sent successfully {} {}", this, request.getParameters().get("chat_id"), response.message());
            }

            @Override
            public void onFailure(SendMessage request, IOException e) {
                LOG.info("{} Sent fail {} {}", this, request.getParameters().get("chat_id"),
                        request.getParameters().get("text"));
            }
        });
    }

    public static void main(String[] args) throws Exception {
//
//		GetAllChats gc = new GetAllChats();
//		GetChatsResponse r = TelegramUtil.getBot("1073170315:AAF_GIJbspOl7keCWwznuhoWXhGw8TDhkrg").execute(gc);
//		System.out.println("R " + r);


//		Keyboard keyboard = new ReplyKeyboardMarkup(
//			new KeyboardButton[]{
//				new KeyboardButton("Hoàn tất").requestContact(true),
//				new InlineKeyboardButton("url").url("www.google.com")
//			});
//        SendMessage sendRequest = new SendMessage(406791426,
//                "Vui lòng Click vào nút \"Hoàn tất\" ở phía dưới và đồng ý để hoàn thành việc kích hoạt bảo mật Telegram OTP!");
//        getBot(BOT_TOKEN).execute(sendRequest, new Callback<SendMessage, SendResponse>() {
//            @Override
//            public void onResponse(SendMessage request, SendResponse response) {
//
//            }
//
//            @Override
//            public void onFailure(SendMessage request, IOException e) {
//
//            }
//        });
//        getBot(BOT_TOKEN).execute(new DeleteMessage(1, 1), new Callback<DeleteMessage, BaseResponse>() {
//            @Override
//            public void onResponse(DeleteMessage request, BaseResponse response) {
//
//            }
//
//            @Override
//            public void onFailure(DeleteMessage request, IOException e) {
//
//            }
//        });
//		Keyboard keyboard = new ReplyKeyboardMarkup(
//			new KeyboardButton[]{
//					new KeyboardButton("Hoàn tất").requestContact(true)
//			});
//		sendRequest.replyMarkup(keyboard);
//		sendRequest.replyMarkup(new InlineKeyboardMarkup(new InlineKeyboardButton[]{
//                new InlineKeyboardButton("Cộng đồng").url("https://t.me/vua365_com"),
//                new InlineKeyboardButton("Tải game").url("https://vua365.com/m/")
//        }));
//		TelegramUtil.getBot("1009346926:AAEngx3ZrNsKeUHImNVpjrn8m4-9imnSSpo").execute(sendRequest);
//		com.google.common.cache.Cache<Object,Object> messageCached = CacheBuilder.newBuilder()
//			.maximumSize(1000)
//			.expireAfterWrite(24,TimeUnit.HOURS)
//			.build();
//		TelegramBot bot = TelegramUtil.getBot("1009346926:AAEngx3ZrNsKeUHImNVpjrn8m4-9imnSSpo");
//		// Register for updates
//		String json = "{\"update_id\":371984211,\"message\":{\"message_id\":16299,\"from\":{\"id\":406791426,\"is_bot\":false,\"first_name\":\"D\",\"last_name\":\"Dragon\",\"username\":\"mrddragon\",\"language_code\":\"en\"},"
//			+ "\"chat\":{\"id\":406791426,\"first_name\":\"D\",\"last_name\":\"Dragon\",\"username\":\"mrddragon\",\"type\":\"private\"},\"date\":1581820890,"
//			+ "	\"forward_from_chat\":{\"id\":-1001469762494,\"title\":\"C\\u1ed5ng T\\u1eb7ng Gifcode SMS\",\"username\":\"pagecodesms\",\"type\":\"channel\"},\"forward_from_message_id\":861,\"forward_date\":1581788450,"
//			+ "	\"photo\":[{\"file_id\":\"AgACAgUAAxkBAAI_q15Iq9pEQWoAAVVY5vqBQuCkLl77EAACfakxGwFeQFaiIgFZeEgw-n26JTMABAEAAwIAA20AA1t9AwABGAQ\",\"file_unique_id\":\"AQADfbolMwAEW30DAAE\",\"file_size\":21570,\"width\":320,\"height\":320},"
//			+ "	{\"file_id\":\"AgACAgUAAxkBAAI_q15Iq9pEQWoAAVVY5vqBQuCkLl77EAACfakxGwFeQFaiIgFZeEgw-n26JTMABAEAAwIAA3gAA1x9AwABGAQ\",\"file_unique_id\":\"AQADfbolMwAEXH0DAAE\",\"file_size\":45134,\"width\":640,\"height\":640}],"
//			+ "	\"caption\":\"\\ud83d\\udd0a\\ud83d\\udd0a\\n\\u27141. Nh\\u1eadn GifCode 100-500k \\n\\n\\ud83d\\udca5 \\ud83d\\udca5\\n\\ud83c\\udf81 CHIA S\\u1eba V\\u00c0O 5-10 NH\\u00d3M TR\\u00caN TELEGRAM\\n\\ud83d\\udeabL\\u01b0u \\u00fd: Gi\\u00e1 tr\\u1ecb Qu\\u00e0 T\\u1eb7ng ph\\u1ee5 thu\\u1ed9c v\\u00e0o S\\u1ed0 L\\u1ea6N CHIA S\\u1eba B\\u00c0I VI\\u1ebeT TH\\u00c0NH C\\u00d4NG.\\n\\nEvent Youtube  \\ud83d\\udc49\\ud83d\\udc49 https://t.me/Youtubecode500k\\n\\nTuy\\u1ec3n C\\u1ed9ng T\\u00e1c Vi\\u00ean \\ud83d\\udc49\\ud83d\\udc49\\ud83d\\udc49 https://t.me/joinchat/AAAAAFcbkK3OV94JYKUoVQ\\n\\nH\\u01b0\\u1edbng d\\u1eabn Nh\\u1eadn GifCode \\n\\ud83d\\udc49\\ud83d\\udc49\\ud83d\\udc49 https://t.me/joinchat/OeetkhKc1JEi0eLiDezW9g\",\"caption_entities\":[{\"offset\":175,\"length\":28,\"type\":\"url\"},{\"offset\":232,\"length\":44,\"type\":\"url\"},{\"offset\":309,\"length\":44,\"type\":\"url\"}]}}";
//		Update update = BotUtils.parseUpdate(json);
//		messageCached.put(update.updateId(),update);
//		System.out.println("X " + update.updateId() + " - " + update.message().chat().id() + " - " + update.message().messageId() + " - " + update.message().caption().contains("joinchat"));
//
//
//		DeleteMessage message = new DeleteMessage(update.message().chat().id(), update.message().messageId());
//
//		EditMessageText editMessageText = new EditMessageText(update.message().chat().id(), update.message().messageId(), "new test")
//        .parseMode(ParseMode.HTML)
//        .disableWebPagePreview(true);
//        .replyMarkup(new ReplyKeyboardRemove());

//		BaseResponse response = bot.execute(message);
////		System.out.println("X " + response.description());
//
//		json = "{\"update_id\":371984178,\"message\":{\"message_id\":16242,\"from\":{\"id\":1017576970,\"is_bot\":false,\"first_name\":\"\\u0110\\u1eb7ng\",\"last_name\":\"Tr\\u01b0\\u1eddng\",\"language_code\":\"vi\"},\"chat\":{\"id\":1017576970,\"first_name\":\"\\u0110\\u1eb7ng\",\"last_name\":\"Tr\\u01b0\\u1eddng\",\"type\":\"private\"},\"date\":1581817229,\"forward_from_chat\":{\"id\":-1001469762494,\"title\":\"C\\u1ed5ng T\\u1eb7ng Gifcode SMS\",\"username\":\"pagecodesms\",\"type\":\"channel\"},\"forward_from_message_id\":861,\"forward_date\":1581788450,\"photo\":[{\"file_id\":\"AgACAgUAAxkBAAI_cl5InY26CSrmhliDVd-SQ-xFHWSLAAJ9qTEbAV5AVqIiAVl4SDD6fbolMwAEAQADAgADbQADW30DAAEYBA\",\"file_unique_id\":\"AQADfbolMwAEW30DAAE\",\"file_size\":21570,\"width\":320,\"height\":320},{\"file_id\":\"AgACAgUAAxkBAAI_cl5InY26CSrmhliDVd-SQ-xFHWSLAAJ9qTEbAV5AVqIiAVl4SDD6fbolMwAEAQADAgADeAADXH0DAAEYBA\",\"file_unique_id\":\"AQADfbolMwAEXH0DAAE\",\"file_size\":45134,\"width\":640,\"height\":640}],\"caption\":\"\\ud83d\\udd0a\\ud83d\\udd0a\\n\\u27141. Nh\\u1eadn GifCode 100-500k \\n\\n\\ud83d\\udca5 \\ud83d\\udca5\\n\\ud83c\\udf81 CHIA S\\u1eba V\\u00c0O 5-10 NH\\u00d3M TR\\u00caN TELEGRAM\\n\\ud83d\\udeabL\\u01b0u \\u00fd: Gi\\u00e1 tr\\u1ecb Qu\\u00e0 T\\u1eb7ng ph\\u1ee5 thu\\u1ed9c v\\u00e0o S\\u1ed0 L\\u1ea6N CHIA S\\u1eba B\\u00c0I VI\\u1ebeT TH\\u00c0NH C\\u00d4NG.\\n\\nEvent Youtube  \\ud83d\\udc49\\ud83d\\udc49 https://t.me/Youtubecode500k\\n\\nTuy\\u1ec3n C\\u1ed9ng T\\u00e1c Vi\\u00ean \\ud83d\\udc49\\ud83d\\udc49\\ud83d\\udc49 https://t.me/joinchat/AAAAAFcbkK3OV94JYKUoVQ\\n\\nH\\u01b0\\u1edbng d\\u1eabn Nh\\u1eadn GifCode \\n\\ud83d\\udc49\\ud83d\\udc49\\ud83d\\udc49 https://t.me/joinchat/OeetkhKc1JEi0eLiDezW9g\",\"caption_entities\":[{\"offset\":175,\"length\":28,\"type\":\"url\"},{\"offset\":232,\"length\":44,\"type\":\"url\"},{\"offset\":309,\"length\":44,\"type\":\"url\"}]}}";
//		update = BotUtils.parseUpdate(json);
//		messageCached.put(update.updateId(),update);
//		System.out.println("X " + messageCached);
    }

    class ChatMessage {
        String message_id;
        Map from;
        Map chat;
        String date;
        String text;
        List<Map> entitites;
    }

    class Bot {
        private int id;
        private String name;
        private String token;

        public Bot() {
        }

        public Bot(int id, String name, String token) {
            this.id = id;
            this.name = name;
            this.token = token;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        @Override
        public String toString() {
            return "Bot{" + "id=" + id + ", token=" + token + '}';
        }
    }

    class Conversation {
        private int id;
        private String name;
        private String chatId;
        private Bot bot;

        public Conversation() {
        }

        public Conversation(int id, String name, String chatId, Bot bot) {
            this.id = id;
            this.name = name;
            this.chatId = chatId;
            this.bot = bot;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getChatId() {
            return chatId;
        }

        public void setChatId(String chatId) {
            this.chatId = chatId;
        }

        public Bot getBot() {
            return bot;
        }

        public void setBot(Bot bot) {
            this.bot = bot;
        }

        public void say(String message) throws Exception {
            say(bot, message);
        }

        public void say(Bot bot, String message) throws Exception {
            TelegramUtil.sendMessage(bot.getToken(), chatId, message);
        }

        @Override
        public String toString() {
            return "Conversation{" + "id=" + id + ", name=" + name + ", chatId=" + chatId + ", bot=" + bot + '}';
        }
    }

}
