//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.vinplay.khothe;

import java.util.List;

public class BuyCardResponseObj {
    private int code;
    private String message;
    private BuyCardResponseObj.BuyCardResponseData data;

    public BuyCardResponseObj() {
    }

    public int getCode() {
        return this.code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public BuyCardResponseObj.BuyCardResponseData getData() {
        return this.data;
    }

    public void setData(BuyCardResponseObj.BuyCardResponseData data) {
        this.data = data;
    }

    public class BuyCardResponseCard {
        private String telco;
        private int amount;
        private String serial;
        private String pin;

        public BuyCardResponseCard() {
        }

        public String getTelco() {
            return this.telco;
        }

        public void setTelco(String telco) {
            this.telco = telco;
        }

        public int getAmount() {
            return this.amount;
        }

        public void setAmount(int amount) {
            this.amount = amount;
        }

        public String getSerial() {
            return this.serial;
        }

        public void setSerial(String serial) {
            this.serial = serial;
        }

        public String getPin() {
            return this.pin;
        }

        public void setPin(String pin) {
            this.pin = pin;
        }
    }

    public class BuyCardResponseData {
        private String partner_reference;
        private String server_reference;
        private String trans_time;
        private List<BuyCardResponseObj.BuyCardResponseCard> cards;

        public BuyCardResponseData() {
        }

        public String getPartner_reference() {
            return this.partner_reference;
        }

        public void setPartner_reference(String partner_reference) {
            this.partner_reference = partner_reference;
        }

        public String getServer_reference() {
            return this.server_reference;
        }

        public void setServer_reference(String server_reference) {
            this.server_reference = server_reference;
        }

        public String getTrans_time() {
            return this.trans_time;
        }

        public void setTrans_time(String trans_time) {
            this.trans_time = trans_time;
        }

        public List<BuyCardResponseObj.BuyCardResponseCard> getCards() {
            return this.cards;
        }

        public void setCards(List<BuyCardResponseObj.BuyCardResponseCard> cards) {
            this.cards = cards;
        }
    }
}
