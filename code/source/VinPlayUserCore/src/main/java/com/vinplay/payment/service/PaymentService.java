/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.vinplay.vbee.common.messages.pay.ExchangeMessage
 */
package com.vinplay.payment.service;

import com.vinplay.usercore.response.LogExchangeMoneyResponse;
import com.vinplay.vbee.common.messages.pay.ExchangeMessage;
import org.bson.Document;

import java.util.Date;
import java.util.List;

public interface PaymentService {
    boolean logExchangeMoney(ExchangeMessage var1);

    boolean checkMerchantTransId(String var1, String var2);

    LogExchangeMoneyResponse getLogExchangeMoney(String var1, String var2, String var3, String var4, String var5, int var6, String var7, String var8, int var9) throws Exception;

    long getTotalMoney(String var1, String var2, String var3, String var4, String var5) throws Exception;

    List<ExchangeMessage> getExchangeMoney(String var1, String var2, String var3, String var4, String var5, int var6, String var7, String var8) throws Exception;

    public List<Document> getSuccessRechargeCardData(Date startDate, Date endDate);

    public List<Document> getSuccessRechargeMomoData(Date startDate, Date endDate);

    public List<Document> getSuccessRechargeBankData(Date startDate, Date endDate);

    public List<Document> getSuccessRechargeCardData(String nickName);

    public List<Document> getSuccessRechargeMomoData(String nickName);

    public List<Document> getSuccessRechargeBankData(String nickName);
}

