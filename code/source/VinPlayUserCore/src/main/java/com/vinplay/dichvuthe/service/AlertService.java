/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.dichvuthe.service;

import java.util.List;

public interface AlertService {
    boolean sendSMS2One(String var1, String var2, boolean var3);

    boolean sendSMS2User(String var1, String var2);

    boolean sendEmail(String var1, String var2, List<String> var3);

    boolean alert2List(List<String> var1, String var2, boolean var3);

    boolean alert2One(String var1, String var2, boolean var3);

    boolean SendSMSEsms(String var1, String var2);

    boolean SendSMSRutCuoc(String var1, String var2);

    void SendOtpTelegram(String nickname, String otp);

    void sendCustomerService(String message);

    void sendOperation(String message);

    void sendClientErrorMonitor(String message);

    void sendPublicGroup(String message);
}

