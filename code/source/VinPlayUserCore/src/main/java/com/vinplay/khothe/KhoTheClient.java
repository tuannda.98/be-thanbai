/*
 * Decompiled with CFR 0.150.
 *
 * Could not load the following classes:
 *  com.fasterxml.jackson.databind.ObjectMapper
 *  com.google.gson.Gson
 *  org.apache.http.HttpEntity
 *  org.apache.http.client.methods.CloseableHttpResponse
 *  org.apache.http.client.methods.HttpGet
 *  org.apache.http.client.methods.HttpPost
 *  org.apache.http.client.methods.HttpUriRequest
 *  org.apache.http.entity.StringEntity
 *  org.apache.http.impl.client.CloseableHttpClient
 *  org.apache.http.impl.client.HttpClients
 *  org.apache.http.util.EntityUtils
 *  org.apache.log4j.Logger
 */
package com.vinplay.khothe;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.vinplay.dichvuthe.entities.SoftpinObj;
import com.vinplay.dichvuthe.utils.RSAUtil;
import com.vinplay.khothe.BuyCardRequestObj;
import com.vinplay.khothe.BuyCardResponseObj;
import com.vinplay.khothe.RutThe99ResponseObj;
import com.vinplay.khothe.models.BankWithdrawReq;
import com.vinplay.khothe.models.BankWithdrawResp;
import com.vinplay.khothe.models.GetBankReq;
import com.vinplay.khothe.models.GetBankResp;
import com.vinplay.usercore.logger.MoneyLogger;
import com.vinplay.usercore.utils.PartnerConfig;
import com.vinplay.vbee.common.models.SoftpinJson;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

public class KhoTheClient {
    private static final Logger logger = Logger.getLogger("cashout");

    public static SoftpinObj buyCard(String id, String provider, int price, int quantity) throws Exception {
        if ((provider = provider.toUpperCase()).equals("VNP")) {
            provider = "VINA";
        }
        BuyCardRequestObj reqObj = new BuyCardRequestObj();
        reqObj.setPartner_id(PartnerConfig.KhoThePartnerId);
        reqObj.setDenomination(price);
        reqObj.setPartner_reference(id);
        reqObj.setQuantity(quantity);
        reqObj.setTelco(provider);
        String data = "partner_id=" + PartnerConfig.KhoThePartnerId + "&partner_reference=" + reqObj.getPartner_reference() + "&quantity=" + reqObj.getQuantity() + "&telco=" + reqObj.getTelco() + "&denomination=" + reqObj.getDenomination();
        logger.debug(data);
        MoneyLogger.log("nickname", "CashOutByCard", 0L, 0L, "vin", data, "1030", data);
        String sign = RSAUtil.sign(PartnerConfig.KhoThePrivateKey, data);
        reqObj.setSignature(sign);
        logger.debug(sign);
        logger.debug(reqObj.toJson());
        MoneyLogger.log("nickname", "CashOutByCard", 0L, 0L, "vin", reqObj.toJson(), "1030", reqObj.toJson());
        ObjectMapper mapper = new ObjectMapper();
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(PartnerConfig.KhoTheEndpoint);
        StringEntity entity = new StringEntity(mapper.writeValueAsString(reqObj));
        httpPost.setEntity(entity);
        httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/json");
        CloseableHttpResponse res = client.execute(httpPost);
        HttpEntity entityResponse = res.getEntity();
        String responseString = EntityUtils.toString(entityResponse, "UTF-8");
        client.close();
        logger.debug(responseString);
        MoneyLogger.log("nickname", "CashOutByCard", 0L, 0L, "vin", responseString, "1030", responseString);
        Gson gson = new Gson();
        try {
            BuyCardResponseObj result = gson.fromJson(responseString, BuyCardResponseObj.class);
            SoftpinObj response = new SoftpinObj();
            response.setId(id);
            response.setProvider(provider);
            response.setAmount(price);
            response.setQuantity(quantity);
            response.setStatus(result.getCode());
            response.setMessage(result.getMessage());
            response.setSign("");
            response.setPartnerTransId(id);
            if (result.getCode() == 0) {
                ArrayList<SoftpinJson> softpinList = new ArrayList<SoftpinJson>();
                for (int i = 0; i < result.getData().getCards().size(); ++i) {
                    BuyCardResponseObj.BuyCardResponseCard card = result.getData().getCards().get(i);
                    SoftpinJson softpinJson = new SoftpinJson();
                    softpinJson.setProvider(provider);
                    softpinJson.setAmount(card.getAmount());
                    logger.debug(KhoTheClient.decrypt(card.getPin()));
                    logger.debug(KhoTheClient.decrypt(card.getSerial()));
                    softpinJson.setPin(KhoTheClient.decrypt(card.getPin()));
                    softpinJson.setSerial(KhoTheClient.decrypt(card.getSerial()));
                    softpinJson.setExpire("");
                    softpinList.add(softpinJson);
                }
                response.setSoftpinList(softpinList);
            }
            return response;
        }
        catch (Exception ex) {
            SoftpinObj response = new SoftpinObj();
            response.setMessage(responseString);
            return response;
        }
    }

    public static SoftpinObj buyCardKhoThe99(String id, String provider, int price, int quantity) throws Exception {
        provider = provider.toUpperCase();
        CloseableHttpClient client = HttpClients.createDefault();
        String url = PartnerConfig.RutThe99Endpoint + "?Username=" + PartnerConfig.RutThe99Username + "&Password=" + PartnerConfig.RutThe99Password + "&Network=" + provider + "&CardValue=" + price + "&Quantity=" + quantity;
        HttpGet httpGet = new HttpGet(url);
        CloseableHttpResponse res = client.execute(httpGet);
        HttpEntity entityResponse = res.getEntity();
        String responseString = EntityUtils.toString(entityResponse, "UTF-8");
        client.close();
        logger.debug(responseString);
        MoneyLogger.log("nickname", "CashOutByCard", 0L, 0L, "vin", responseString, "1030", responseString);
        Gson gson = new Gson();
        try {
            RutThe99ResponseObj result = gson.fromJson(responseString, RutThe99ResponseObj.class);
            SoftpinObj response = new SoftpinObj();
            response.setId(id);
            response.setProvider(provider);
            response.setAmount(price);
            response.setQuantity(quantity);
            response.setStatus(result.getCode());
            response.setMessage(result.getMessage());
            response.setSign("");
            response.setPartnerTransId(id);
            if (result.getCode() == 1) {
                ArrayList<SoftpinJson> softpinList = new ArrayList<SoftpinJson>();
                for (int i = 0; i < result.getCards().size(); ++i) {
                    RutThe99ResponseObj.RutThe99Card card = result.getCards().get(i);
                    SoftpinJson softpinJson = new SoftpinJson();
                    softpinJson.setProvider(provider);
                    softpinJson.setAmount(card.getCardValue());
                    softpinJson.setPin(card.getCardCode());
                    softpinJson.setSerial(card.getCardSeri());
                    softpinJson.setExpire("");
                    softpinList.add(softpinJson);
                }
                response.setSoftpinList(softpinList);
            }
            return response;
        }
        catch (Exception ex) {
            SoftpinObj response = new SoftpinObj();
            response.setMessage(responseString);
            return response;
        }
    }

    public static GetBankResp GetBankList(String id) throws Exception {
        String sign;
        GetBankReq req = new GetBankReq();
        req.denomination = 100000;
        req.partner_id = PartnerConfig.KhoThePartnerId;
        req.partner_reference = id;
        req.quantity = 1;
        req.telco = "BANK";
        String data = "partner_id=" + PartnerConfig.KhoThePartnerId + "&partner_reference=" + req.partner_reference + "&quantity=" + req.quantity + "&telco=" + req.telco + "&denomination=" + req.denomination;
        req.signature = sign = RSAUtil.sign(PartnerConfig.KhoThePrivateKey, data);
        ObjectMapper mapper = new ObjectMapper();
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(PartnerConfig.KHOTHE_GET_BANK_URL);
        Gson gson = new Gson();
        StringEntity entity = new StringEntity(gson.toJson(req));
        httpPost.setEntity(entity);
        httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/json");
        CloseableHttpResponse res = client.execute(httpPost);
        HttpEntity entityResponse = res.getEntity();
        String responseString = EntityUtils.toString(entityResponse, "UTF-8");
        client.close();
        if (responseString != null && !responseString.equals("")) {
            return gson.fromJson(responseString, GetBankResp.class);
        }
        return null;
    }

    public static BankWithdrawResp WithdrawBank(String nick_name, String id, String account_number, String account_name, String bankCode, int amount) throws Exception {
        String sign;
        BankWithdrawReq req = new BankWithdrawReq();
        req.partner_id = PartnerConfig.KhoThePartnerId;
        req.partner_reference = id;
        req.quantity = 1;
        req.telco = "BANK";
        req.request_acc_name = account_name;
        req.request_acc_no = account_number;
        req.request_bank_code = bankCode;
        req.denomination = amount;
        req.request_desc = nick_name + " rut ngan hang:" + amount;
        String data = "partner_id=" + PartnerConfig.KhoThePartnerId + "&partner_reference=" + req.partner_reference + "&quantity=" + req.quantity + "&telco=" + req.telco + "&denomination=" + req.denomination;
        req.signature = sign = RSAUtil.sign(PartnerConfig.KhoThePrivateKey, data);
        ObjectMapper mapper = new ObjectMapper();
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(PartnerConfig.KHOTHE_WITHDRAW_BANK_URL);
        Gson gson = new Gson();
        StringEntity entity = new StringEntity(gson.toJson(req));
        httpPost.setEntity(entity);
        httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/json");
        CloseableHttpResponse res = client.execute(httpPost);
        HttpEntity entityResponse = res.getEntity();
        String responseString = EntityUtils.toString(entityResponse, "UTF-8");
        client.close();
        if (responseString != null && !responseString.equals("")) {
            return gson.fromJson(responseString, BankWithdrawResp.class);
        }
        return null;
    }

    private static String decrypt(String input) {
        try {
            return RSAUtil.decrypt(input, PartnerConfig.KhoThePrivateKey);
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }
        catch (BadPaddingException e) {
            e.printStackTrace();
        }
        return "";
    }
}

