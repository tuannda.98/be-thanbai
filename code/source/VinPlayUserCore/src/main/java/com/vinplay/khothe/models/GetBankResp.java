/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.khothe.models;

import java.util.List;

public class GetBankResp {
    public int code;
    public String message;
    public List<GetBankData> data;

    public class GetBankData {
        public String bankCode;
        public String bankName;
        public String shortBankName;
        public int status;
    }
}

