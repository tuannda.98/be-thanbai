package com.vinplay.usercore.service;

import com.hazelcast.core.IMap;
import com.vinplay.usercore.entities.TransferMoneyResponse;
import com.vinplay.vbee.common.enums.Games;
import com.vinplay.vbee.common.models.TopCaoThu;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.models.cache.UserExtraInfoModel;
import com.vinplay.vbee.common.response.*;
import com.vinplay.vbee.common.statics.TransType;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public interface UserService {

    String insertUser(String var1, String var2) throws SQLException;

    boolean insertUserBySocial(String var1, String var2) throws SQLException;

    String updateNickname(int var1, String var2) throws SQLException;

    boolean updateClient(UserModel userModel, String appId) throws SQLException;

    boolean updatePlatform(UserModel userModel, String appId) throws SQLException;

    boolean checkNickname(String var1) throws SQLException;

    UserModel getUserByUserName(String var1) throws SQLException;

    UserModel getUserByNickName(String var1) throws SQLException;

    UserModel getUserBySocialId(String var1, String var2) throws SQLException;

    UserResponse checkSessionKey(String var1, String var2, Games var3);

    void logout(String var1);

    long getMoneyUserCache(String var1, String var2);

    UserCacheModel getMoneyUser(String var1);

    long getMoneyCashout(String var1) throws ParseException;

    long getCurrentMoneyUserCache(String var1, String var2);

    MoneyResponse updateMoney(String var1, long var2, String var4, String var5, String var6, String var7, long var8, Long var10, TransType var11);

    MoneyResponse updateMoneyFromAdmin(String var1, long var2, String var4, String var5, String var6, String var7);

    BaseResponseModel updateMoneyCacheToDB(String var1) throws SQLException;

    BaseResponseModel updateMoneyWithNotify(String nickname, long money, String actionName, String serviceName, String description, boolean showPopup);

    boolean checkMobile(String var1) throws SQLException;

    boolean checkMobileDaiLy(String var1) throws SQLException;

    boolean checkMobileSecurity(String var1) throws SQLException;

    boolean checkEmailSecurity(String var1) throws SQLException;

    byte checkUser(String var1);

    NapXuResponse napXu(String var1, long var2, boolean var4);

    List<TopCaoThu> getTopCaoThu(String var1, String var2, int var3);

    byte checkMoney(String var1, long var2, byte var4);

    TransferMoneyResponse transferMoney(String var1, String var2, long var3, String var5, boolean var6);

    double getFeeTransfer(int var1);

    byte calFeeTransfer(int var1, int var2);

    UserCacheModel forceGetCachedUser(String nickName);

    long getAvailableVinMoney(String nickName);

    UserCacheModel getCacheUserIgnoreCase(String nickname);

    boolean insertBot(String var1, String var2, String var3, long var4, long var6, int var8) throws SQLException;

    long getTotalRechargeMoney(String var1);

    int getVipPointSave(String var1);

    boolean checkAccesstoken(String var1, String var2);

    int checkBot(String var1);

    UserExtraInfoModel getUserExtraInfo(String var1);

    UserModel getNicknameExactly(String var1, IMap<String, UserCacheModel> var2) throws SQLException;

    List<UserInfoModel> checkPhoneByUser(String var1) throws SQLException;

    UserInfoModel checkPhoneExists(String var1) throws SQLException;

    UserCacheModel checkMoneyNegative(UserCacheModel var1) throws SQLException;

    void bonusFirstTimeVerifyMobileAndBonus(String nickname);

    void lockUser(String nickName);

    void unlockUser(String nickName);

    void updateUser(String nickName, Consumer<UserCacheModel> consumer, BiConsumer<UserCacheModel, Exception>... exceptionHandlers);

    void unloadUserModelFromCache(UserModel userModel);

    /**
     * Kiểm tra xem user có online ko?
     * (Kiểm tra trong danh sách user đã connect tới minigame)
     *
     * @param nickname
     * @return true nếu có online, false nếu offline
     */
    boolean isOnline(String nickname);

    /**
     * Gửi message đến nick tele của user (nếu đã đăng kí)
     *
     * @param nickname
     * @param mess
     */
    void sendTeleMessage(String nickname, String mess);

    /**
     * Tính lại số tiền thật nạp thành công của user
     *
     * @param nickname
     */
    long getDbCashinMoneyAmount(String nickname);

    /**
     * Tính lại số tiền thật rút thành công của user
     *
     * @param nickname
     */
    long getDbCashoutMoneyAmount(String nickname);

    /**
     * Kiểm tra xem trong cache có tổn tại user với nickname được cho hay không?
     *
     * @param nickname
     * @return true nếu tồn tại, false nếu ko tồn tại
     */
    boolean checkExistCache(String nickname);
}
