/*
 * Decompiled with CFR 0.150.
 *
 * Could not load the following classes:
 *  com.fasterxml.jackson.databind.ObjectMapper
 *  com.hazelcast.core.HazelcastInstance
 *  com.hazelcast.core.IMap
 *  com.vinplay.vbee.common.enums.BankType
 *  com.vinplay.vbee.common.enums.PhoneCardType
 *  com.vinplay.vbee.common.enums.ProviderType
 *  com.vinplay.vbee.common.hazelcast.HazelcastClientFactory
 *  com.vinplay.vbee.common.messages.BaseMessage
 *  com.vinplay.vbee.common.messages.LogMoneyUserMessage
 *  com.vinplay.vbee.common.messages.MoneyMessageInMinigame
 *  com.vinplay.vbee.common.messages.dvt.CashoutByBankMessage
 *  com.vinplay.vbee.common.messages.dvt.CashoutByCardMessage
 *  com.vinplay.vbee.common.messages.dvt.CashoutByTopUpMessage
 *  com.vinplay.vbee.common.models.Softpin
 *  com.vinplay.vbee.common.models.SoftpinJson
 *  com.vinplay.vbee.common.models.cache.UserCacheModel
 *  com.vinplay.vbee.common.rmq.RMQApi
 *  com.vinplay.vbee.common.utils.NumberUtils
 *  com.vinplay.vbee.common.utils.UserValidaton
 *  com.vinplay.vbee.common.utils.VinPlayUtils
 *  org.apache.log4j.Logger
 */
package com.vinplay.dichvuthe.service.impl;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.vinplay.dichvuthe.client.VinplayClient;
import com.vinplay.dichvuthe.dao.impl.CashoutDaoImpl;
import com.vinplay.dichvuthe.encode.RSA;
import com.vinplay.dichvuthe.entities.BankAccountInfo;
import com.vinplay.dichvuthe.entities.BankcoObj;
import com.vinplay.dichvuthe.service.CashOutService;
import com.vinplay.dichvuthe.utils.DvtUtils;
import com.vinplay.usercore.logger.MoneyLogger;
import com.vinplay.usercore.utils.GameCommon;
import com.vinplay.vbee.common.enums.BankType;
import com.vinplay.vbee.common.enums.ProviderType;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;
import com.vinplay.vbee.common.messages.dvt.CashoutByBankMessage;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.utils.NumberUtils;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class CashOutServiceImpl
        implements CashOutService {
    private static final Logger logger = Logger.getLogger("cashout");

    @Override
    public void cashOutByBank(String nickname, int amount) {
        HazelcastInstance client;
        String sign;
        String id;
        BankcoObj bankcoObj;
        String desc;
        int code;
        String name;
        String account;
        String bankname;
        CashoutDaoImpl dao;
        String error;
        StringBuilder content;
        block28:
        {
            logger.debug("Request cashOutByBank:  nickname: " + nickname + ", amount: " + amount);
            SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss dd-MM-yyyy");
            String time = format.format(new Date());
            content = new StringBuilder("Dai ly ").append(nickname).append(" da chuyen ").append(NumberUtils.formatNumber(String.valueOf(amount))).append(" vin cho ban luc ").append(time).append(". Dang thuc hien chuyen khoan ngan hang");
            error = "";
            dao = new CashoutDaoImpl();
            bankname = "";
            account = "";
            name = "";
            code = 0;
            desc = "";
            BankType bank = null;
            BankAccountInfo info = null;
            try {
                amount = (int) Math.round((double) amount * GameCommon.getValueDouble("RATIO_CASHOUT_BANK"));
                content.append(".So tien: ").append(NumberUtils.formatNumber(String.valueOf(amount)));
                info = dao.getBankAccountInfo(nickname);
            } catch (Exception e) {
                code = 1;
                desc = "Kh\u00f4ng t\u00ecm th\u1ea5y th\u00f4ng tin t\u00e0i kho\u1ea3n: " + e.getMessage();
                error = "He thong gian doan";
            }
            if (info == null) {
                code = 1;
                desc = "Kh\u00f4ng t\u00ecm th\u1ea5y th\u00f4ng tin t\u00e0i kho\u1ea3n";
                error = "Khong tim thay thong tin tai khoan ngan hang cua dai ly.";
            } else {
                bankname = info.getBank();
                account = info.getAccount();
                name = info.getName();
            }
            if (code == 0) {
                try {
                    bank = BankType.getBankByName(bankname);
                    if (GameCommon.getValueInt("IS_CASHOUT_BANK") == 1) {
                        code = 2;
                        desc = "\u0110ang kh\u00f3a chuy\u1ec3n kho\u1ea3n ng\u00e2n h\u00e0ng";
                        error = "Dang khoa chuyen khoan ngan hang.";
                    } else if (bank == null) {
                        desc = "Kh\u00f4ng t\u00ecm th\u1ea5y ng\u00e2n h\u00e0ng: " + bankname;
                        error = "Khong tim thay thong tin tai khoan ngan hang cua dai ly.";
                        code = 3;
                    } else if (account == null || account.isEmpty()) {
                        desc = "Kh\u00f4ng t\u00ecm th\u1ea5y s\u1ed1 t\u00e0i kho\u1ea3n: " + account;
                        error = "Khong tim thay thong tin tai khoan ngan hang cua dai ly.";
                        code = 4;
                    } else if (name == null || name.isEmpty()) {
                        desc = "Kh\u00f4ng t\u00ecm th\u1ea5y t\u00ean t\u00e0i kho\u1ea3n: " + name;
                        error = "Khong tim thay thong tin tai khoan ngan hang cua dai ly.";
                        code = 5;
                    } else if (amount > GameCommon.getValueInt("CASHOUT_BANK_MAX")) {
                        desc = "S\u1ed1 ti\u1ec1n qu\u00e1 l\u1edbn: " + amount;
                        error = "So tien qua lon.";
                        code = 6;
                    }
                } catch (Exception e) {
                    desc = "L\u1ed7i l\u1ea5y config t\u1eeb cache: ";
                    error = "He thong gian doan.";
                    code = 7;
                }
            }
            bankcoObj = null;
            id = String.valueOf(System.currentTimeMillis());
            sign = null;
            if (code == 0) {
                try {
                    sign = RSA.sign(id + bank.getValue() + account + name + amount, GameCommon.getValueStr("DVT_PRIVATE_KEY"));
                } catch (Exception e2) {
                    code = 8;
                    desc = "L\u1ed7i t\u1ea1o ch\u1eef k\u00fd s\u1ed1: " + e2.getMessage();
                    error = "He thong gian doan.";
                }
            }
            client = HazelcastClientFactory.getInstance();
            if (code == 0) {
                try {
                    bankcoObj = VinplayClient.cashOutByBank(id, bank.getValue(), account, name, amount, sign);
                } catch (Exception e3) {
                    code = 9;
                    desc = "L\u1ed7i k\u1ebft n\u1ed1i d\u1ecbch v\u1ee5 th\u1ebb: " + e3.getMessage();
                    error = "He thong gian doan.";
                    if (client == null) break block28;
                    DvtUtils.errorDvt(client, "CashOutByBank");
                }
            }
        }
        int statusDvt = -1;
        String messageDvt = "";
        if (bankcoObj != null) {
            statusDvt = bankcoObj.getStatus();
            messageDvt = bankcoObj.getMessage();
            code = this.getErrorCodeBank(statusDvt);
            error = this.getDescErrorCodeBank(statusDvt);
        }
        try {
            UserCacheModel superUser;
            IMap userMap = client.getMap("users");
            String superAgent = GameCommon.getValueStr("SUPER_AGENT");
            if (userMap.containsKey(superAgent) && (superUser = (UserCacheModel) userMap.get(superAgent)).getMobile() != null && superUser.isHasMobileSecurity()) {
                AlertServiceImpl alertService = new AlertServiceImpl();
                alertService.sendSMS2One(superUser.getMobile(), content.append(". Ket qua: ").append(error).toString(), false);
            }
        } catch (Exception e4) {
            logger.debug(e4);
            MoneyLogger.log(nickname, "CashOutByBank", amount, 0L, "vin", "Chuyen tien ngan hang", "1034", "error semd alert: " + e4.getMessage());
        }
        try {
            CashoutByBankMessage message = new CashoutByBankMessage(nickname, id, bankname, account, name, amount, statusDvt, messageDvt, sign, code, desc);
            dao.logCashoutByBank(message);
        } catch (Exception e4) {
            logger.debug(e4);
            MoneyLogger.log(nickname, "CashOutByBank", amount, 0L, "vin", "Chuyen tien ngan hang", "1033", "error mongodb: " + e4.getMessage());
        }
        logger.debug("Response cashOutByBank:  code: " + code + ", desc: " + desc);
    }

    private int mapErrorCodeEpay(int status) {
        switch (status) {
            case 0: {
                return 0;
            }
            case 23:
            case 99: {
                return 30;
            }
            case 30: {
                return 999;
            }
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 17:
            case 21:
            case 22:
            case 31:
            case 32:
            case 33:
            case 35:
            case 52:
            case 101:
            case 102:
            case 103:
            case 104:
            case 105:
            case 106:
            case 107:
            case 108:
            case 109:
            case 110:
            case 111:
            case 112:
            case 113: {
                return 1;
            }
        }
        return 30;
    }

    private int mapErrorCodeVTC(int status) {
        switch (status) {
            case 1: {
                return 0;
            }
            case -55: {
                return 999;
            }
            case -600:
            case -509:
            case -503:
            case -502:
            case -501:
            case -500:
            case -350:
            case -348:
            case -320:
            case -318:
            case -317:
            case -316:
            case -315:
            case -311:
            case -310:
            case -309:
            case -308:
            case -307:
            case -306:
            case -305:
            case -304:
            case -302: {
                return 1;
            }
            case -505:
            case -504:
            case -290:
            case -99:
            case -1:
            case 0: {
                return 30;
            }
        }
        return 30;
    }

    private int mapErrorCode1Pay(int status) {
        switch (status) {
            case 0: {
                return 0;
            }
            case 800: {
                return 999;
            }
            case 1:
            case 2:
            case 901:
            case 903:
            case 1001:
            case 2000:
            case 2001:
            case 2002:
            case 2003:
            case 2005:
            case 9999: {
                return 1;
            }
        }
        return 30;
    }

    private String mapMessage1Pay(int status) {
        switch (status) {
            case 0: {
                return "Th\u00e0nh c\u00f4ng";
            }
            case 1:
            case 9999: {
                return "Giao d\u1ecbch th\u1ea5t b\u1ea1i";
            }
            case 2: {
                return "Th\u00f4ng tin x\u00e1c th\u1ef1c kh\u00f4ng ch\u00ednh x\u00e1c";
            }
            case 800: {
                return "S\u1ed1 d\u01b0 kh\u00f4ng \u0111\u1ee7";
            }
            case 901: {
                return "Th\u00f4ng tin \u0111\u0103ng nh\u1eadp kh\u00f4ng \u0111\u00fang";
            }
            case 903: {
                return "Th\u00f4ng tin \u0111\u0103ng nh\u1eadp kh\u00f4ng \u0111\u00fang";
            }
            case 1001: {
                return "Nh\u00e0 m\u1ea1ng ng\u1eebng ho\u1ea1t \u0111\u1ed9ng ho\u1eb7c \u0111ang b\u1ea3o tr\u00ec";
            }
            case 2000: {
                return "Tham s\u1ed1 \u0111\u1ea7u v\u00e0o kh\u00f4ng \u0111\u00fang";
            }
            case 2001: {
                return "Tham s\u1ed1 \u0111\u1ea7u v\u00e0o Topup kh\u00f4ng \u0111\u00fang";
            }
            case 2002: {
                return "Tham s\u1ed1 \u0111\u1ea7u v\u00e0o Mua th\u1ebb kh\u00f4ng \u0111\u00fang";
            }
            case 2003: {
                return "S\u1ed1 \u0111i\u1ec7n tho\u1ea1i cung c\u1ea5p kh\u00f4ng \u0111\u00fang";
            }
            case 2005: {
                return "Giao d\u1ecbch b\u1ecb l\u1eb7p";
            }
        }
        return "\u0110ang x\u1eed l\u00fd";
    }

    private int getErrorCodeCard(int status) {
        switch (status) {
            case 0: {
                return 0;
            }
            case 10: {
                return 1;
            }
            case 20: {
                return 22;
            }
            case 99: {
                return 30;
            }
        }
        return 30;
    }

    private int getErrorCodeTopUp(int status) {
        switch (status) {
            case 0: {
                return 0;
            }
            case 10: {
                return 1;
            }
            case 11: {
                return 23;
            }
            case 99: {
                return 30;
            }
        }
        return 30;
    }

    private int getErrorCodeBank(int status) {
        switch (status) {
            case 0: {
                return 0;
            }
            case 10: {
                return 30;
            }
            case 30: {
                return 31;
            }
            case 31: {
                return 32;
            }
            case 32: {
                return 33;
            }
            case 98: {
                return 34;
            }
            case 99: {
                return 34;
            }
        }
        return 34;
    }

    private String getDescErrorCodeBank(int status) {
        switch (status) {
            case 0: {
                return "Thanh cong.";
            }
            case 10: {
                return "That bai.";
            }
            case 30: {
                return "Sai ngan hang.";
            }
            case 31: {
                return "Sai ten tai khoan.";
            }
            case 32: {
                return "Sai so tai khoan.";
            }
            case 98: {
                return "Dang xu ly.";
            }
            case 99: {
                return "Dang xu ly.";
            }
        }
        return "Dang xu ly.";
    }

    private String getServiceName(ProviderType provider) {
        if (provider.getType() == 0) {
            return "Mua th\u1ebb \u0111i\u1ec7n tho\u1ea1i";
        }
        return "Mua th\u1ebb game";
    }

    private void alert(String number, String content, boolean isCall) {
        AlertServiceImpl alertService = new AlertServiceImpl();
        if (number.contains(",")) {
            String[] arr = number.split(",");
            ArrayList<String> mList = new ArrayList<String>();
            for (String m : arr) {
                m = m.trim();
                mList.add(m);
            }
            alertService.alert2List(mList, content, isCall);
        } else {
            alertService.alert2One(number, content, isCall);
        }
    }
}

