/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.vinplay.vbee.common.models.userMission.CompleteMissionObj
 *  com.vinplay.vbee.common.models.userMission.NumberCompleteMissionObj
 *  com.vinplay.vbee.common.models.userMission.UserMissionResponse
 */
package com.vinplay.usercore.service;

import com.vinplay.vbee.common.models.userMission.CompleteMissionObj;
import com.vinplay.vbee.common.models.userMission.NumberCompleteMissionObj;
import com.vinplay.vbee.common.models.userMission.UserMissionResponse;

public interface UserMissionService {
    UserMissionResponse getUserMission(String var1) throws Exception;

    CompleteMissionObj completeMission(String var1, String var2, String var3) throws Exception;

    NumberCompleteMissionObj getNumberCompleteMission(String var1) throws Exception;
}

