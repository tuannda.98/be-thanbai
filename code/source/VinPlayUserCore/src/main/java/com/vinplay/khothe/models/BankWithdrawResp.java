/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.khothe.models;

public class BankWithdrawResp {
    public int code;
    public String message;
    public BankWithdrawData data;

    public class BankWithdrawData {
        public String target_acc_no;
        public String target_acc_name;
        public String partner_reference;
        public String server_reference;
        public String transfer_id;
        public String transfer_message;
    }
}

