/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.vinplay.vbee.common.models.OtpModel
 */
package com.vinplay.usercore.dao;

import com.vinplay.doisoat.entities.DoisoatVmg;
import com.vinplay.usercore.response.LogSMSOtpResponse;
import com.vinplay.vbee.common.models.OtpModel;
import java.text.ParseException;

public interface OtpDao {
    boolean updateOtpSMS(String var1, String var2, String var3);

    boolean updateOtpSMS(String var1, String var2, String var3, int var4);

    boolean updateOtpSMSFirst(String var1, String var2, String var3);

    OtpModel getOtpSMS(String var1, String var2) throws ParseException;

    LogSMSOtpResponse getLogSMSOtp(String var1, String var2, String var3, int var4, String var5);

    DoisoatVmg doisoatVMG(DoisoatVmg var1, String var2, String var3);
}

