//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.vinplay.usercore.dao.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.vinplay.usercore.dao.UserMarketingDao;
import com.vinplay.vbee.common.messages.UserMarketingMessage;
import com.vinplay.vbee.common.models.MarketingModel;
import com.vinplay.vbee.common.models.cache.StatisticUserMarketing;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.pools.ConnectionPool;
import org.bson.Document;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class UserMarketingDaoImpl implements UserMarketingDao {
    public UserMarketingDaoImpl() {
    }

    public boolean saveUserMarketing(UserMarketingMessage message) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = null;
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String timeLog = df.format(new Date());
        col = db.getCollection("user_marketing");
        Document doc = new Document();
        doc.append("user_name", message.getUserName());
        doc.append("utm_campaign", message.getUtmCampaign());
        doc.append("utm_medium", message.getUtmMedium());
        doc.append("utm_source", message.getUtmSource());
        doc.append("time_login", timeLog);
        col.insertOne(doc);
        return true;
    }

    public boolean saveLoginDailyMarketing(UserMarketingMessage message) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = null;
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String timeLog = df.format(new Date());
        col = db.getCollection("login_daily_marketing");
        Document doc = new Document();
        doc.append("user_name", message.getUserName());
        doc.append("num_login", message.getNumLogin());
        doc.append("time_login", timeLog);
        col.insertOne(doc);
        return true;
    }

    public boolean updateLoginDailyMarketing(UserMarketingMessage message) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = null;
        col = db.getCollection("login_daily_marketing");
        col.updateOne(new Document("user_name", message.getUserName()), new Document("$set", new Document("num_login", message.getNumLogin())));
        return true;
    }

    public List<UserMarketingMessage> getNickNameUserMarketing(String nickName, String timeLogin) {
        final ArrayList<UserMarketingMessage> results = new ArrayList();
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        HashMap<String, Object> conditions = new HashMap();
        conditions.put("user_name", nickName);
        conditions.put("time_login", timeLogin);
        FindIterable iterable = db.getCollection("login_daily_marketing").find(new Document(conditions));
        iterable.forEach(new Block<Document>() {
            public void apply(Document document) {
                UserMarketingMessage usermarket = new UserMarketingMessage();
                usermarket.nickName = document.getString("nick_name");
                usermarket.numLogin = document.getInteger("num_login");
                results.add(usermarket);
            }
        });
        return results;
    }

    public MarketingModel marketingToolFromDate(String campaign, String source, String medium, String startDate, String endDate) throws SQLException {
        return null;
    }

    private List<String> getMKTList(String tableName, String condition) throws SQLException {
        ArrayList<String> results = new ArrayList();
        String sql = "SELECT * FROM " + tableName;
        if (condition != null && !condition.equals("")) {
            sql = sql + " WHERE name='" + condition + "'";
        }
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement(sql);
             ResultSet rs = stm.executeQuery();
        ) {
            while (rs.next()) {
                results.add(rs.getString("name"));
            }

            return results;
        }
    }

    public List<String> getCampaignList(String condition) throws SQLException {
        return this.getMKTList("utm_campain", condition);
    }

    public List<String> getSourceList(String condition) throws SQLException {
        return this.getMKTList("utm_source", condition);
    }

    public List<String> getMediumList(String condition) throws SQLException {
        return this.getMKTList("utm_medium", condition);
    }

    public void logMKTInfo(StatisticUserMarketing entry) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("statistic_mkt_daily");
        Document doc = new Document();
        doc.append("utm_campaign", entry.getCampaign());
        doc.append("utm_medium", entry.getMedium());
        doc.append("utm_source", entry.getSource());
        doc.append("nru", entry.getNRU());
        doc.append("pu", entry.getPU());
        doc.append("nap_vin", entry.getTotalNapVin());
        doc.append("tieu_vin", entry.getTotalTieuVin());
        doc.append("time_log", entry.getDate());
        col.insertOne(doc);
    }

    public List<MarketingModel> getHistoryMKT(String campaign, String medium, String source, String startDate, String endDate) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        HashMap<String, Object> conditions = new HashMap();
        final ArrayList<MarketingModel> results = new ArrayList();
        if (campaign != null && !campaign.isEmpty()) {
            conditions.put("utm_campaign", campaign);
        }

        if (source != null && !source.isEmpty()) {
            conditions.put("utm_source", source);
        }

        if (medium != null && !medium.isEmpty()) {
            conditions.put("utm_medium", medium);
        }

        if (startDate != null && endDate != null) {
            BasicDBObject obj = new BasicDBObject();
            obj.put("$gte", startDate);
            obj.put("$lte", endDate);
            conditions.put("time_log", obj);
        }

        FindIterable iterable = db.getCollection("statistic_mkt_daily").find(new Document(conditions));
        iterable.forEach(new Block<Document>() {
            public void apply(Document document) {
                MarketingModel model = new MarketingModel();
                model.campaign = document.getString("utm_campaign");
                model.medium = document.getString("utm_medium");
                model.source = document.getString("utm_source");
                model.NRU = document.getInteger("nru");
                model.PU = document.getInteger("pu");
                model.doanhthu = document.getLong("nap_vin");
                model.dateStr = document.getString("time_log");
                results.add(model);
            }
        });
        return results;
    }

    public MarketingModel getMKTInfo(String username) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        HashMap<String, Object> conditions = new HashMap();
        conditions.put("user_name", username);
        FindIterable iterable = db.getCollection("user_marketing").find(new Document(conditions));
        final MarketingModel model = new MarketingModel();
        iterable.forEach(new Block<Document>() {
            public void apply(Document document) {
                model.campaign = document.getString("utm_campaign");
                model.medium = document.getString("utm_medium");
                model.source = document.getString("utm_source");
            }
        });
        return model;
    }
}
