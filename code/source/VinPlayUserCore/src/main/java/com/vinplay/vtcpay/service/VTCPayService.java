/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.vtcpay.service;

import com.vinplay.vtcpay.request.CheckAccountRequest;
import com.vinplay.vtcpay.request.CheckTransRequest;
import com.vinplay.vtcpay.request.TopupRequest;

public interface VTCPayService {
    String checkTrans(CheckTransRequest var1);

    String topup(TopupRequest var1);

    String checkAccount(CheckAccountRequest var1);
}

