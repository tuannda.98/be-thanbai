/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.mongodb.client.MongoCollection
 *  com.mongodb.client.MongoDatabase
 *  com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory
 *  org.bson.Document
 */
package com.vinplay.dichvuthe.service;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.vinplay.dichvuthe.service.LogSMSDAO;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import org.bson.Document;

public class LogSMSDAOImpl
implements LogSMSDAO {
    @Override
    public boolean saveLog(String request_id, String mobile, String message, String status, String transTime, String command_code) {
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        MongoCollection col = db.getCollection("vmg_transaction");
        Document doc = new Document();
        doc.append("request_id", request_id);
        doc.append("mobile", mobile);
        doc.append("message_MT", "OTP");
        doc.append("response_MT", "1");
        doc.append("message_MO", message);
        doc.append("response_MO", status);
        doc.append("trans_time", transTime);
        doc.append("command_code", command_code);
        col.insertOne(doc);
        return true;
    }
}

