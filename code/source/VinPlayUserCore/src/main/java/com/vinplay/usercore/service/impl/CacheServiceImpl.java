/*
 * Decompiled with CFR 0.150.
 *
 * Could not load the following classes:
 *  com.hazelcast.core.HazelcastInstance
 *  com.hazelcast.core.IMap
 *  com.vinplay.vbee.common.exceptions.KeyNotFoundException
 *  com.vinplay.vbee.common.hazelcast.HazelcastClientFactory
 */
package com.vinplay.usercore.service.impl;

import casio.king365.core.HCMap;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.vinplay.usercore.service.CacheService;
import com.vinplay.vbee.common.exceptions.KeyNotFoundException;
import com.vinplay.vbee.common.hazelcast.HazelcastClientFactory;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class CacheServiceImpl
        implements CacheService {

    private static final String CACHE_GAME_BAI = "cacheGameBai";

    @Override
    public void setValue(String key, String value) {
        IMap<String, String> map = HCMap.getCachedConfig();
        map.put(key, value);
    }

    @Override
    public void setValue(String key, int value) {
        IMap<String, String> map = HCMap.getCachedConfig();
        map.put(key, String.valueOf(value));
    }

    @Override
    public String getValueStr(String key) throws KeyNotFoundException {
        IMap<String, String> map = HCMap.getCachedConfig();
        if (map.containsKey(key)) {
            return map.get(key);
        }
        throw new KeyNotFoundException();
    }

    @Override
    public int getValueInt(String key) throws KeyNotFoundException, NumberFormatException {
        IMap<String, String> map = HCMap.getCachedConfig();
        if (map.containsKey(key)) {
            return Integer.parseInt(map.get(key));
        }
        throw new KeyNotFoundException();
    }

    @Override
    public boolean removeKey(String key) throws KeyNotFoundException {
        IMap<String, String> map = HCMap.getCachedConfig();
        if (map.containsKey(key)) {
            map.remove(key);
            return true;
        }
        throw new KeyNotFoundException();
    }

    /**
     * Deprecated: Use generic version
     *
     * @param key
     * @param value
     */
    @Override
    @Deprecated
    public void setObject(String key, Object value) {
        HazelcastInstance instance = HazelcastClientFactory.getInstance();
        IMap map = instance.getMap(CACHE_GAME_BAI);
        map.put(key, value);
    }

    /**
     * Deprecated: Use generic version
     *
     * @param key
     * @return
     * @throws KeyNotFoundException
     */
    @Override
    @Deprecated
    public Object getObject(String key) throws KeyNotFoundException {
        HazelcastInstance instance = HazelcastClientFactory.getInstance();
        IMap map = instance.getMap(CACHE_GAME_BAI);
        if (map.containsKey(key)) {
            return map.get(key);
        }
        throw new KeyNotFoundException();
    }

    /**
     * @param key
     * @return
     * @throws KeyNotFoundException
     */
    @Override
    public Object removeObject(String key) throws KeyNotFoundException {
        HazelcastInstance instance = HazelcastClientFactory.getInstance();
        IMap map = instance.getMap(CACHE_GAME_BAI);
        if (map.containsKey(key)) {
            return map.remove(key);
        }
        throw new KeyNotFoundException();
    }

    /**
     * @param keys
     * @return
     */
    @Override
    public Map<String, Object> getBulk(Set<String> keys) {
        HazelcastInstance instance = HazelcastClientFactory.getInstance();
        IMap map = instance.getMap(CACHE_GAME_BAI);
        return map.getAll(keys);
    }

    /**
     * Deprecated: Use generic version
     *
     * @param key
     * @param expireTime
     * @param obj
     */
    @Override
    @Deprecated
    public void setObject(String key, int expireTime, Object obj) {
        HazelcastInstance instance = HazelcastClientFactory.getInstance();
        IMap map = instance.getMap(CACHE_GAME_BAI);
        map.put(key, obj, expireTime, TimeUnit.SECONDS);
    }

    /**
     * Deprecated: Use generic version
     *
     * @param key
     * @param callback
     * @return
     */
    @Override
    public Object getCachedObject(String key, Function<String, Object> callback) {
        HazelcastInstance instance = HazelcastClientFactory.getInstance();
        IMap map = instance.getMap(CACHE_GAME_BAI);
        boolean exists = map.containsKey(key);
        if (!exists) {
            Object object = callback.apply(key);
            if (object != null) {
                map.set(key, object);
            }
        }
        return map.get(key);
    }

    @Override
    public <T> void setObject(CacheServiceKey<T> key, T value) {
        HazelcastInstance instance = HazelcastClientFactory.getInstance();
        instance.getMap(CACHE_GAME_BAI).put(key.key, value);
    }

    @Override
    public <T> void setObject(CacheServiceKey<T> key, T value, long timeToLive, TimeUnit timeUnit) {
        HazelcastInstance instance = HazelcastClientFactory.getInstance();
        instance.getMap(CACHE_GAME_BAI).put(key.key, value, timeToLive, timeUnit);
    }

    @Override
    public <T> T getObject(CacheServiceKey<T> key) throws KeyNotFoundException {
        HazelcastInstance instance = HazelcastClientFactory.getInstance();
        IMap<String, Object> map = instance.getMap(CACHE_GAME_BAI);
        if (map.containsKey(key.key)) {
            return (T) map.get(key.key);
        }
        throw new KeyNotFoundException();
    }

    @Override
    public <T> T getCachedObject(CacheServiceKey<T> key, Function<String, T> generator) {
        HazelcastInstance instance = HazelcastClientFactory.getInstance();
        IMap<String, Object> map = instance.getMap(CACHE_GAME_BAI);
        boolean exists = map.containsKey(key.key);
        if (!exists) {
            T value = generator.apply(key.key);
            if (value != null) {
                map.set(key.key, value);
            }
        }
        return (T) map.get(key.key);
    }
}
