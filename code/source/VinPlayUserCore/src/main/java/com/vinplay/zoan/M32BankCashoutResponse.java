/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.zoan;

public class M32BankCashoutResponse {
    private int errorCode;
    private String errorDesc;
    private String transId;
    private M32BankCashoutMsg msg;

    public int getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDesc() {
        return this.errorDesc;
    }

    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc;
    }

    public String getTransId() {
        return this.transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public M32BankCashoutMsg getMsg() {
        return this.msg;
    }

    public void setMsg(M32BankCashoutMsg msg) {
        this.msg = msg;
    }

    public class M32BankCashoutMsg {
        private String momoTransId;
        private String finishTime;
        private String userName;
        private String accountId;
        private String shortBankName;
        private String accountName;
        private long amount;
        private String comment;
        private String authKey;

        public String getMomoTransId() {
            return this.momoTransId;
        }

        public void setMomoTransId(String momoTransId) {
            this.momoTransId = momoTransId;
        }

        public String getFinishTime() {
            return this.finishTime;
        }

        public void setFinishTime(String finishTime) {
            this.finishTime = finishTime;
        }

        public String getUserName() {
            return this.userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getAccountId() {
            return this.accountId;
        }

        public void setAccountId(String accountId) {
            this.accountId = accountId;
        }

        public String getShortBankName() {
            return this.shortBankName;
        }

        public void setShortBankName(String shortBankName) {
            this.shortBankName = shortBankName;
        }

        public String getAccountName() {
            return this.accountName;
        }

        public void setAccountName(String accountName) {
            this.accountName = accountName;
        }

        public long getAmount() {
            return this.amount;
        }

        public void setAmount(long amount) {
            this.amount = amount;
        }

        public String getComment() {
            return this.comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getAuthKey() {
            return this.authKey;
        }

        public void setAuthKey(String authKey) {
            this.authKey = authKey;
        }
    }
}

