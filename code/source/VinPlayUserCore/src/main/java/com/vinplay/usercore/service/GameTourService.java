/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.usercore.service;

import com.vinplay.gamebai.entities.GameFreeCodeDetail;
import com.vinplay.gamebai.entities.GameFreeCodePackage;
import com.vinplay.gamebai.entities.GameFreeCodeStatistic;
import com.vinplay.gamebai.entities.PokerFreeTicket;
import com.vinplay.gamebai.entities.PokerFreeTicketResponse;
import com.vinplay.gamebai.entities.PokerFreeTicketStatistic;
import com.vinplay.gamebai.entities.PokerTicketCount;
import com.vinplay.gamebai.entities.PokerTourInfo;
import com.vinplay.gamebai.entities.PokerTourInfoDetail;
import com.vinplay.gamebai.entities.PokerTourInfoGeneral;
import com.vinplay.gamebai.entities.PokerTourPlayer;
import com.vinplay.gamebai.entities.PokerTourState;
import com.vinplay.gamebai.entities.PokerTourType;
import com.vinplay.gamebai.entities.TopTourModel;
import com.vinplay.gamebai.entities.UserTourModel;
import com.vinplay.gamebai.entities.VipTourModel;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

public interface GameTourService {
    boolean updateMark(String var1, String var2, String var3, int var4, Calendar var5, int var6, int var7, int var8, int var9, String var10) throws SQLException, ParseException;

    List<VipTourModel> getVips(String var1, String var2) throws SQLException;

    boolean saveVipTour(String var1, String var2, Calendar var3, Calendar var4, int var5, int var6, String var7) throws SQLException, ParseException;

    boolean updateMoneyJackpot(String var1, long var2, String var4) throws SQLException;

    long getMoneyJackPot(String var1) throws SQLException;

    String getString(String var1) throws SQLException;

    boolean saveString(String var1, String var2) throws SQLException;

    List<TopTourModel> getTop(String var1, Calendar var2, Calendar var3, int var4, int var5) throws SQLException, ParseException;

    List<UserTourModel> getLogUserTour(String var1, String var2, int var3, int var4) throws SQLException, ParseException;

    PokerTourInfo createPokerTour(PokerTourInfo var1) throws ParseException, SQLException;

    PokerTourInfo getPokerTour(int var1) throws ParseException, SQLException;

    boolean updatePokerTour(PokerTourInfo var1) throws ParseException, SQLException;

    boolean updatePokerTourPlayer(PokerTourPlayer var1) throws ParseException, SQLException;

    PokerTourPlayer getPokerTourPlayer(int var1, String var2) throws ParseException, SQLException;

    List<PokerTourInfo> getPokerTourList(Calendar var1, Calendar var2, PokerTourType var3) throws ParseException, SQLException;

    List<PokerTourPlayer> getPokerTourPlayers(int var1) throws ParseException, SQLException;

    boolean logJackPotPokerTour(String var1, int var2, long var3, long var5, String var7, String var8) throws IOException, TimeoutException, InterruptedException;

    PokerFreeTicket createPokerFreeTicket(PokerFreeTicket var1) throws ParseException, SQLException;

    PokerFreeTicket usePokerFreeTicket(String var1, int var2, PokerTourType var3, int var4) throws ParseException, SQLException;

    List<PokerTicketCount> getPokerFreeTicket(String var1) throws ParseException, SQLException;

    List<PokerTourInfoGeneral> getPokerTourListGeneral(Integer var1, PokerTourState var2, PokerTourType var3, int var4, int var5) throws ParseException, SQLException;

    PokerTourInfoDetail getPokerTourDetail(int var1) throws ParseException, SQLException;

    PokerFreeTicketStatistic getPokerFreeTicketStatistic(int var1, String var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, String var10, String var11, int var12, int var13) throws ParseException, SQLException;

    int exportFreeCode(String var1, int var2, int var3, int var4, Calendar var5, String var6) throws ParseException, SQLException;

    List<GameFreeCodePackage> getFreeCodePackage(int var1, String var2, int var3, int var4, String var5, String var6, String var7) throws ParseException, SQLException;

    List<GameFreeCodeDetail> getFreeCodeDetails(int var1, String var2, int var3, String var4, int var5, int var6, int var7, String var8, String var9, String var10, String var11, int var12) throws ParseException, SQLException;

    Map<Integer, GameFreeCodeStatistic> getFreeCodeStatistic(String var1, String var2, String var3, int var4) throws ParseException, SQLException;

    PokerFreeTicketResponse getTicketFromFreeCode(String var1, String var2) throws ParseException, SQLException;

    boolean updateFreeCodeDetail(int var1, String var2, int var3, String var4, int var5, int var6, int var7, List<Integer> var8) throws ParseException, SQLException;

    boolean clearUserInTour(int var1) throws SQLException;
}

