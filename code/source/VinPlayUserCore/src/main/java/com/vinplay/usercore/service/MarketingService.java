/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.vinplay.vbee.common.messages.UserMarketingMessage
 *  com.vinplay.vbee.common.models.MarketingModel
 *  com.vinplay.vbee.common.models.cache.StatisticUserMarketing
 */
package com.vinplay.usercore.service;

import com.vinplay.vbee.common.messages.UserMarketingMessage;
import com.vinplay.vbee.common.models.MarketingModel;
import com.vinplay.vbee.common.models.cache.StatisticUserMarketing;
import java.sql.SQLException;
import java.util.List;

public interface MarketingService {
    boolean saveUserMarketing(UserMarketingMessage var1);

    void loginMarketing(String var1);

    List<String> getMediumList(String var1);

    List<String> getSourceList(String var1);

    List<String> getCampaignList(String var1);

    void statisticMKTInfo(StatisticUserMarketing var1);

    List<MarketingModel> getHistoryMKT(String var1, String var2, String var3, String var4, String var5);

    List<MarketingModel> getMKTInfo(String var1, String var2, String var3, String var4, String var5) throws SQLException;

    MarketingModel getMKTInfo(String var1);
}

