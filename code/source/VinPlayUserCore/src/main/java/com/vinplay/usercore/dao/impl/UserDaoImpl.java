//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.vinplay.usercore.dao.impl;

import casio.king365.util.KingUtil;
import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import com.vinplay.usercore.dao.UserDao;
import com.vinplay.usercore.utils.GameCommon;
import com.vinplay.vbee.common.models.StatusUser;
import com.vinplay.vbee.common.models.TopCaoThu;
import com.vinplay.vbee.common.models.UserAdminInfo;
import com.vinplay.vbee.common.models.UserModel;
import com.vinplay.vbee.common.models.cache.UserCacheModel;
import com.vinplay.vbee.common.models.userMission.MissionObj;
import com.vinplay.vbee.common.models.userMission.UserMissionCacheModel;
import com.vinplay.vbee.common.mongodb.MongoDBConnectionFactory;
import com.vinplay.vbee.common.pools.ConnectionPool;
import com.vinplay.vbee.common.response.UserInfoModel;
import com.vinplay.vbee.common.utils.DateTimeUtils;
import com.vinplay.vbee.common.utils.UserUtil;
import org.bson.Document;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl implements UserDao {
    public UserDaoImpl() {
    }

    public boolean checkUsername(String username) throws SQLException {
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement("SELECT COUNT(1) as cnt FROM users WHERE user_name=? OR nick_name=?")
        ) {
            stm.setString(1, username);
            stm.setString(2, username);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next() && rs.getInt("cnt") == 1) {
                    return true;
                }
            }catch (SQLException e2) {
                KingUtil.printException("checkUsername", e2);
            }
        }
        return false;
    }

    public boolean checkNickname(String nickname) throws SQLException {
        boolean res = false;
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement("SELECT COUNT(1) as cnt FROM users WHERE nick_name=?")
        ) {
            stm.setString(1, nickname);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next() && rs.getInt("cnt") == 1) {
                    res = true;
                }
            }
        }catch (SQLException e2) {
            KingUtil.printException("checkNickname", e2);
        }
        return res;
    }

    public int checkAgent(String nickname) throws SQLException {
        int res = -1;
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement("SELECT dai_ly FROM users WHERE nick_name=?")
        ) {
            stm.setString(1, nickname);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    res = rs.getInt("dai_ly");
                }
            }
        }catch (SQLException e2) {
            KingUtil.printException("checkAgent", e2);
        }

        return res;
    }

    public boolean checkNicknameExist(String nickname) throws SQLException {
        boolean res = false;
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement("SELECT COUNT(1) as cnt FROM users WHERE nick_name=? OR user_name=?")
        ) {
            stm.setString(1, nickname);
            stm.setString(2, nickname);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next() && rs.getInt("cnt") == 1) {
                    res = true;
                }
            }
        }catch (SQLException e) {
            KingUtil.printException("checkNicknameExist", e);
        }
        return res;
    }

    public boolean updateMoney(int userId, long money, String moneyType) throws SQLException {
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             CallableStatement call = conn.prepareCall("CALL update_money_db(?,?,?)");
        ) {
            int param = 1;
            int var14 = param + 1;
            call.setInt(param, userId);
            call.setLong(var14++, money);
            call.setString(var14++, moneyType);
            call.executeUpdate();
        }catch (SQLException e) {
            KingUtil.printException("updateMoney", e);
        }

        return true;
    }

    public boolean updateRechargeMoney(int userId, long money) throws SQLException {
        boolean res = false;
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement("update users set recharge_money = recharge_money + ? where id=?");
        ) {
            stm.setLong(1, money);
            stm.setInt(2, userId);
            if (stm.executeUpdate() == 1) {
                res = true;
            }
        }catch (SQLException e) {
            KingUtil.printException("updateRechargeMoney", e);
        }

        return res;
    }

    @Override
    public boolean updateCashoutMoney(int userId, long money) throws SQLException {
        boolean res = false;
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement("update users set cashout_money = cashout_money + ? where id=?");
        ) {
            stm.setLong(1, money);
            stm.setInt(2, userId);
            if (stm.executeUpdate() == 1) {
                res = true;
            }
        }catch (SQLException e) {
            KingUtil.printException("updateCashoutMoney", e);
        }

        return res;
    }

    @Override
    public boolean updateClient(int id, String appId) throws SQLException {
        boolean res = false;
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement("update users set client = ? where id=?");
        ) {
            stm.setString(1, appId);
            stm.setInt(2, id);
            if (stm.executeUpdate() == 1) {
                res = true;
            }
        }

        return res;
    }

    @Override
    public boolean updatePlatform(int id, String appId) throws SQLException {
        boolean res = false;
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement("update users set platform = ? where id=?");
        ) {
            stm.setString(1, appId);
            stm.setInt(2, id);
            if (stm.executeUpdate() == 1) {
                res = true;
            }
        }

        return res;
    }

    public UserModel getUserByUserName(String username) throws SQLException {
        UserModel user = null;
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement("SELECT * FROM users WHERE user_name=?");
        ) {
            stm.setString(1, username);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    user = UserUtil.parseResultSetToUserModel(rs);
                }
            }
        }

        return user;
    }

    public UserModel getUserByNickName(String nickname) throws SQLException {
        UserModel user = null;
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement("SELECT * FROM users WHERE nick_name=?");
        ) {
            stm.setString(1, nickname);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    user = UserUtil.parseResultSetToUserModel(rs);
                }
            }
        }

        return user;
    }

    public UserModel getUserByFBId(String fbId) throws SQLException {
        UserModel user = null;
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement("SELECT * FROM users WHERE facebook_id=?");
        ) {

            stm.setString(1, fbId);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    user = UserUtil.parseResultSetToUserModel(rs);
                }
            }
        }

        return user;
    }

    public UserModel getUserByGGId(String ggId) throws SQLException {
        UserModel user = null;
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement("SELECT * FROM users WHERE google_id=?");
        ) {
            stm.setString(1, ggId);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    user = UserUtil.parseResultSetToUserModel(rs);
                }
            }
        }

        return user;
    }

    public UserModel getUserByAppleId(String appleId) throws SQLException {
        UserModel user = null;
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement("SELECT * FROM users WHERE user_name like 'AP%' AND identification = ?");
        ) {

            stm.setString(1, appleId);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    user = UserUtil.parseResultSetToUserModel(rs);
                }
            }
        }

        return user;
    }

    public UserModel getUserByDeviceId(String devId) throws SQLException {
        UserModel user = null;
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement("SELECT * FROM users WHERE identification = ?");
        ) {

            stm.setString(1, devId);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    user = UserUtil.parseResultSetToUserModel(rs);
                }
            }
        }

        return user;
    }

    public long getMoneyUser(String nickname, String moneyType) throws SQLException {
        long money = 0L;
        String sql = "SELECT " + moneyType + " FROM users WHERE nick_name=?";
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement(sql);
        ) {
            stm.setString(1, nickname);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    money = rs.getLong(moneyType);
                }
            }
        }

        return money;
    }

    public long getCurrentMoney(String nickname, String moneyType) throws SQLException {
        long money = 0L;
        String sql = "SELECT " + moneyType + "_total FROM users WHERE nick_name=?";
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement(sql);
        ) {
            stm.setString(1, nickname);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    money = rs.getLong(moneyType + "_total");
                }
            }
            stm.close();
        }

        return money;
    }

    public int getIdByNickname(String nickname) throws SQLException {
        int userId = 0;
        String sql = "SELECT id FROM users WHERE nick_name=?";
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement("SELECT id FROM users WHERE nick_name=?");
        ) {
            stm.setString(1, nickname);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    userId = rs.getInt("id");
                }
            }
        }

        return userId;
    }

    public int getIdByUsername(String username) throws SQLException {
        int userId = 0;
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement("SELECT id FROM users WHERE user_name=?");
        ) {
            stm.setString(1, username);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    userId = rs.getInt("id");
                }
            }
        }

        return userId;
    }

    public boolean restoreMoneyByAdmin(int userId, long moneyUse, long moneyTotal, long moneySafe, String moneyType) throws SQLException {
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             CallableStatement call = conn.prepareCall("CALL update_money_cache(?,?,?,?,?)")
        ) {
            int param = 1;
            int var17 = param + 1;
            call.setInt(param, userId);
            call.setLong(var17++, moneyUse);
            call.setLong(var17++, moneyTotal);
            call.setLong(var17++, moneySafe);
            call.setString(var17++, moneyType);
            call.executeUpdate();
        }

        return true;
    }

    public boolean checkMobile(String mobile) throws SQLException {
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement("SELECT COUNT(1) as cnt FROM users WHERE mobile=?")
        ) {
            stm.setString(1, mobile);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    return rs.getInt("cnt") >= 1;
                }
            }
        }
        return false;
    }

    public boolean checkMobileDaiLy(String mobile) throws SQLException {
        int status = -1;
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement("SELECT status FROM users WHERE mobile=? AND dai_ly <> 0");
        ) {
            stm.setString(1, mobile);
            try (ResultSet rs = stm.executeQuery()) {

                if (rs.next()) {
                    status = rs.getInt("status");
                }
            }
            if (status >= 0 && StatusUser.checkStatus(status, 4)) {
                return true;
            }
        }
        return false;
    }

    public boolean checkMobileSecurity(String mobile) throws SQLException {
        boolean res = false;
        // int status = false;
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement("SELECT status FROM users WHERE mobile=?");
        ) {
            stm.setString(1, mobile);
            try (ResultSet rs = stm.executeQuery()) {

                while (true) {
                    if (rs.next()) {
                        int status = rs.getInt("status");
                        if ((status & 16) == 0) {
                            continue;
                        }
                        res = true;
                    }
                    return res;
                }
            }
        }
    }

    public boolean checkEmailSecurity(String email) throws SQLException {
        boolean res = false;
        //int status = false;
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement("SELECT status FROM users WHERE email=?");
        ) {
            stm.setString(1, email);
            try (ResultSet rs = stm.executeQuery()) {

                while (true) {
                    if (rs.next()) {
                        int status = rs.getInt("status");
                        if ((status & 32) == 0) {
                            continue;
                        }

                        res = true;
                    }
                    return res;
                }
            }
        }
    }

    public List<TopCaoThu> getTopCaoThu(String date, String moneyType, int num) {
        final ArrayList<TopCaoThu> results = new ArrayList();
        MongoDatabase db = MongoDBConnectionFactory.getDB();
        FindIterable iterable = null;
        Document conditions = new Document();
        conditions.put("date", date);
        conditions.put("money_win", new BasicDBObject("$gt", 0));
        BasicDBObject sortCondtions = new BasicDBObject();
        sortCondtions.put("money_win", -1);
        iterable = moneyType.equals("vin") ? db.getCollection("top_user_play_game_vin").find(conditions).sort(sortCondtions).limit(num) : db.getCollection("top_user_play_game_xu").find(conditions).sort(sortCondtions).limit(num);
        iterable.forEach(new Block<Document>() {
            public void apply(Document document) {
                results.add(new TopCaoThu(document.getString("nick_name"), document.getLong("money_win")));
            }
        });
        return results;
    }

    public UserModel getUserNormalByNickName(String nickName) throws SQLException {
        UserModel user = null;
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement("SELECT * FROM users WHERE nick_name=? and dai_ly=0");
        ) {
            stm.setString(1, nickName);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    user = UserUtil.parseResultSetToUserModel(rs);
                }
            }
        }

        return user;
    }

    public boolean updateStatusDailyByNickName(String nickName, int status) throws SQLException {
        boolean res = false;
        String sql = "update users set dai_ly='" + status + "' where nick_name='" + nickName + "'";
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement(sql)
        ) {
            if (stm.executeUpdate() == 1) {
                res = true;
            }
        }

        return res;
    }

    public List<UserAdminInfo> searchUserAdmin(String userName, String nickName, String phone, String field, String
            sort, String daily, String timeStart, String timeEnd, int page, int totalrecord, String bot, String like, String
                                                       emailAddress) throws SQLException {
        ArrayList<UserAdminInfo> result = new ArrayList();
        String sql = "select * from users where 1=1";
        int num_start = (page - 1) * totalrecord;
        int index = 1;
        String limit = " LIMIT " + num_start + ", " + totalrecord + "";
        if (like.equals("1")) {
            if (userName != null && !userName.equals("")) {
                sql = sql + " AND user_name like ?";
            }

            if (nickName != null && !nickName.equals("")) {
                sql = sql + " AND nick_name like ?";
            }

            if (emailAddress != null && !emailAddress.equals("")) {
                sql = sql + " AND email like ?";
            }
        } else {
            if (userName != null && !userName.equals("")) {
                sql = sql + " AND user_name =?";
            }

            if (nickName != null && !nickName.equals("")) {
                sql = sql + " AND nick_name=?";
            }

            if (emailAddress != null && !emailAddress.equals("")) {
                sql = sql + " AND email = ?";
            }
        }

        if (phone != null && !phone.equals("")) {
            sql = sql + " AND mobile=?";
        }

        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            sql = sql + " AND create_time BETWEEN ? AND ? ";
        }

        if (daily != null && !daily.equals("")) {
            sql = sql + " AND dai_ly=?";
        }

        if (bot != null && !bot.equals("")) {
            sql = sql + " AND is_bot=?";
        }

        if (field != null && !field.equals("")) {
            if (field.equals("1")) {
                sql = sql + " order by vin_total";
            }

            if (field.equals("2")) {
                sql = sql + " order by xu_total";
            }

            if (field.equals("3")) {
                sql = sql + " order by safe";
            }

            if (field.equals("4")) {
                sql = sql + " order by vip_point";
            }

            if (field.equals("5")) {
                sql = sql + " order by vip_point_save";
            }

            if (field.equals("6")) {
                sql = sql + " order by recharge_money";
            }

            if (sort.equals("1")) {
                sql = sql + " ASC";
            }

            if (sort.equals("2")) {
                sql = sql + " DESC";
            }

            sql = sql + limit;
        } else {
            sql = sql + " order by id DESC" + limit;
        }
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement(sql)
        ) {
            if (userName != null && !userName.equals("")) {
                if (like.equals("1")) {
                    stm.setString(index, '%' + userName + '%');
                } else {
                    stm.setString(index, userName);
                }

                ++index;
            }

            if (nickName != null && !nickName.equals("")) {
                if (like.equals("1")) {
                    stm.setString(index, '%' + nickName + '%');
                } else {
                    stm.setString(index, nickName);
                }

                ++index;
            }

            if (emailAddress != null && !emailAddress.equals("")) {
                if (like.equals("1")) {
                    stm.setString(index, '%' + emailAddress + '%');
                } else {
                    stm.setString(index, emailAddress);
                }

                ++index;
            }

            if (phone != null && !phone.equals("")) {
                stm.setString(index, phone);
                ++index;
            }

            if (daily != null && !daily.equals("")) {
                stm.setInt(index, Integer.parseInt(daily));
                ++index;
            }

            if (bot != null && !bot.equals("")) {
                stm.setInt(index, Integer.parseInt(bot));
                ++index;
            }

            if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
                stm.setString(index, timeStart);
                stm.setString(index + 1, timeEnd);
                ++index;
            }

            try (ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {
                    String username = rs.getString("user_name");
                    String nickname = rs.getString("nick_name");
                    String email = rs.getString("email");
                    String mobile = rs.getString("mobile");
                    long vinTotal = rs.getLong("vin_total");
                    long xuTotal = rs.getLong("xu_total");
                    long safe = rs.getLong("safe");
                    long rechargeMoney = rs.getLong("recharge_money");
                    long cashoutMoney = rs.getLong("cashout_money");
                    int vippoint = rs.getInt("vip_point");
                    int status = rs.getInt("status");
                    String identification = rs.getString("identification");
                    int vippointSave = rs.getInt("vip_point_save");
                    long loginOtp = rs.getLong("login_otp");
                    boolean bots = rs.getInt("is_bot") == 1;
                    String sCreateTime = rs.getString("create_time");
                    String sSecurityTime = rs.getString("security_time");
                    String facebookId = rs.getString("facebook_id");
                    String googleId = rs.getString("google_id");
                    String birthday = rs.getString("birthday");
                    UserAdminInfo user = new UserAdminInfo(username, nickname, email, mobile, identification, vinTotal, xuTotal, safe, rechargeMoney, cashoutMoney, vippoint, vippointSave, loginOtp, bots, sCreateTime, sSecurityTime, status, googleId, facebookId, birthday);
                    result.add(user);
                }
            }
            return result;
        }
    }

    public int countSearchUserAdmin(String userName, String nickName, String phone, String field, String
            sort, String daily, String timeStart, String timeEnd, String bot) throws SQLException {
        int cnt = 0;
        String order = "";
        String sort2 = "";
        String sql = "";
        String query = "select count(*) as cnt from users where 1=1";
        String condition = "";
        if (userName != null && !userName.equals("")) {
            condition = condition + " AND user_name like '%" + userName + "%'";
        }

        if (nickName != null && !nickName.equals("")) {
            condition = condition + " AND nick_name like '%" + nickName + "%'";
        }

        if (phone != null && !phone.equals("")) {
            condition = condition + " AND mobile = '" + phone + "'";
        }

        if (timeStart != null && !timeStart.equals("") && timeEnd != null && !timeEnd.equals("")) {
            condition = condition + " AND create_time BETWEEN '" + timeStart + "' AND '" + timeEnd + "'";
        }

        if (daily != null && !daily.equals("")) {
            condition = condition + " AND dai_ly=" + Integer.parseInt(daily);
        }

        if (bot != null && !bot.equals(null)) {
            condition = condition + " AND is_bot=" + Integer.parseInt(bot);
        }

        if (field != null && !field.equals("")) {
            if (field.equals("1")) {
                order = order + " order by vin_total";
            }

            if (field.equals("2")) {
                order = order + " order by xu_total";
            }

            if (field.equals("3")) {
                order = order + " order by safe";
            }

            if (field.equals("4")) {
                order = order + " order by vip_point";
            }

            if (field.equals("5")) {
                order = order + " order by vip_point_save";
            }

            if (field.equals("6")) {
                order = order + " order by recharge_money";
            }

            if (sort.equals("1")) {
                sort2 = sort2 + " ASC";
            }

            if (sort.equals("2")) {
                sort2 = sort2 + " DESC";
            }

            sql = "select count(*) as cnt from users where 1=1" + condition + order + sort2;
        } else {
            order = " order by id DESC";
            sql = "select count(*) as cnt from users where 1=1" + condition + order;
        }
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement(sql);
             ResultSet rs = stm.executeQuery();
        ) {
            if (rs.next()) {
                cnt = rs.getInt("cnt");
            }
        }

        return cnt;
    }

    public boolean insertBot(String un, String nn, String pw, long vin, long xu, int status) throws SQLException {
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             CallableStatement call = conn.prepareCall("CALL insert_bot(?,?,?,?,?,?)");
        ) {
            int param = 1;
            int var17 = param + 1;
            call.setString(param, un);
            call.setString(var17++, nn);
            call.setString(var17++, pw);
            call.setLong(var17++, vin);
            call.setLong(var17++, xu);
            call.setInt(var17++, status);
            call.executeUpdate();
        }

        return true;
    }

    public int checkBotByNickname(String nickname) throws SQLException {
        int res = 0;
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement("SELECT is_bot FROM users WHERE nick_name=?");
        ) {
            stm.setString(1, nickname);
            try (ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    res = rs.getInt("is_bot");
                }
            }
        }

        return res;
    }

    public List<UserInfoModel> checkPhoneByUser(String phone) throws SQLException {
        ArrayList<UserInfoModel> user = new ArrayList();
        String sql = "SELECT user_name,nick_name,recharge_money,status,mobile,dai_ly FROM users WHERE mobile in (" + phone + ")";
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement(sql);
             ResultSet rs = stm.executeQuery()
        ) {

            while (rs.next()) {
                UserInfoModel model = new UserInfoModel();
                model.nickName = rs.getString("nick_name");
                model.userName = rs.getString("user_name");
                model.rechargeMoney = rs.getLong("recharge_money");
                model.mobile = rs.getString("mobile");
                if ((rs.getInt("status") & 16) != 0) {
                    model.isHasSercurityMobile = true;
                }

                // model.dai_ly = rs.getInt("dai_ly");
                user.add(model);
            }
        }
        return user;
    }

    public UserInfoModel checkPhoneExists(String phone) throws SQLException {
        String sql = "SELECT user_name,nick_name,recharge_money,status,mobile FROM users WHERE mobile = '" + phone + "'";
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement(sql);
             ResultSet rs = stm.executeQuery()
        ) {
            if (rs.next()) {
                UserInfoModel model = new UserInfoModel();
                model.nickName = rs.getString("nick_name");
                model.userName = rs.getString("user_name");
                model.rechargeMoney = rs.getLong("recharge_money");
                model.mobile = rs.getString("mobile");
                if ((rs.getInt("status") & 16) != 0) {
                    model.isHasSercurityMobile = true;
                }

                UserInfoModel var8 = model;
                return var8;
            }
        }

        return null;
    }

    public void resetUserMission() throws Exception {
        String[] matchMaxVin = GameCommon.getValueStr("MATCH_MAX_VIN").split(",");
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stmVin = conn.prepareStatement(" UPDATE user_mission_vin SET level = 1,      match_win = 0,      match_max = ?,      received_reward_level = 0,      update_time = ? ");
             PreparedStatement stmXu = conn.prepareStatement(" UPDATE user_mission_xu SET level = 1,      match_win = 0,      match_max = ?,      received_reward_level = 0,      update_time = ? ");
        ) {
            stmVin.setInt(1, Integer.parseInt(matchMaxVin[0]));
            stmVin.setString(2, DateTimeUtils.getCurrentTime());
            stmVin.executeUpdate();
            String[] matchMaxXu = GameCommon.getValueStr("MATCH_MAX_XU").split(",");
            stmXu.setInt(1, Integer.parseInt(matchMaxXu[0]));
            stmXu.setString(2, DateTimeUtils.getCurrentTime());
            stmXu.executeUpdate();
        }
    }

    public UserMissionCacheModel getListMissionByNickName(
            String nickName, String moneyType, int maxLevel) throws SQLException {
        String tableName = moneyType.equals("vin") ? "user_mission_vin" : "user_mission_xu";
        String sql = " SELECT user_id, user_name, nick_name, mission_name, level, match_win, match_max, received_reward_level" +
                " FROM " + tableName + " WHERE nick_name = ? ";
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement(sql);
        ) {
            UserMissionCacheModel response = new UserMissionCacheModel();
            ArrayList<MissionObj> listMissionObjResponse = new ArrayList();

            stm.setString(1, nickName);
            try (ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    boolean completeMission = rs.getInt("match_win") >= rs.getInt("match_max");
                    boolean completeAllLevel = rs.getInt("level") == maxLevel && rs.getInt("match_win") == rs.getInt("match_max");
                    MissionObj missionObj = new MissionObj(rs.getString("mission_name"), rs.getInt("level"), rs.getInt("match_win"), rs.getInt("match_max"), completeMission, completeAllLevel, rs.getInt("received_reward_level"));
                    listMissionObjResponse.add(missionObj);
                    response.setUserId(rs.getInt("user_id"));
                    response.setUserName(rs.getString("user_name"));
                    response.setNickName(rs.getString("nick_name"));
                }
            }
            response.setListMission(listMissionObjResponse);
            return response;
        }
    }

    public void insertUserMission(String moneyType, MissionObj mission, UserModel user) throws SQLException {
        String tableName = moneyType.equals("vin") ? "user_mission_vin" : "user_mission_xu";
        String sql = " INSERT INTO " + tableName + " (user_id, user_name, nick_name, mission_name, level, match_win, match_max, received_reward_level, create_time, update_time)  VALUES  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement(sql)
        ) {
            stm.setInt(1, user.getId());
            stm.setString(2, user.getUsername());
            stm.setString(3, user.getNickname());
            stm.setString(4, mission.getMisNa());
            stm.setInt(5, mission.getMisLev());
            stm.setInt(6, mission.getMisWin());
            stm.setInt(7, mission.getMisMax());
            stm.setInt(8, mission.getRecReLev());
            stm.setString(9, DateTimeUtils.getCurrentTime());
            stm.setString(10, DateTimeUtils.getCurrentTime());
            stm.executeUpdate();
        }
    }

    public void updateUserMission(String moneyType, String nickName, MissionObj mission) throws SQLException {
        String tableName = moneyType.equals("vin") ? "user_mission_vin" : "user_mission_xu";
        String sql = " UPDATE " + tableName + " SET level = ?,      match_win = ?,      match_max = ?,      received_reward_level = ?,      update_time = ?  WHERE nick_name = ?    AND mission_name = ? ";
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement(sql)
        ) {
            stm.setInt(1, mission.getMisLev());
            stm.setInt(2, mission.getMisWin());
            stm.setInt(3, mission.getMisMax());
            stm.setInt(4, mission.getRecReLev());
            stm.setString(5, DateTimeUtils.getCurrentTime());
            stm.setString(6, nickName);
            stm.setString(7, mission.getMisNa());
            stm.executeUpdate();
        }
    }

    public UserCacheModel getUserByNickNameCache(String nickName) throws SQLException {
        UserCacheModel response = new UserCacheModel();
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement("SELECT id, vin, vin_total, safe FROM vinplay.users WHERE nick_name = ?");
        ) {
            stm.setString(1, nickName);
            try (ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {
                    response.setId(rs.getInt("id"));
                    response.setVin(rs.getLong("vin"));
                    response.setVinTotal(rs.getLong("vin_total"));
                    response.setSafe(rs.getLong("safe"));
                }
            }
            return response;
        }
    }

    public List<UserCacheModel> GetNickNameFreeze() throws SQLException {
        List<UserCacheModel> response = new ArrayList();
        String sql = "SELECT id, nick_name , vin, vin_total FROM vinplay.users WHERE vin != vin_total and is_bot = 0";
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement(sql);
             ResultSet rs = stm.executeQuery();
        ) {
            while (rs.next()) {
                UserCacheModel user = new UserCacheModel();
                user.setId(rs.getInt("id"));
                user.setNickname(rs.getString("nick_name"));
                user.setVin(rs.getLong("vin"));
                user.setVinTotal(rs.getLong("vin_total"));
                response.add(user);
            }
            return response;
        }
    }

    public void insertCommission(int userId, String nickName, long fee, String month) throws SQLException {
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement(" INSERT INTO vinplay.user_fee  (user_id, nick_name, fee, month, create_time, update_time)  VALUES  (?, ?, ?, ?, ?, ?) ");
        ) {
            stm.setInt(1, userId);
            stm.setString(2, nickName);
            stm.setLong(3, fee);
            stm.setString(4, month);
            stm.setString(5, DateTimeUtils.getCurrentTime("yyyy-MM-dd HH:mm:ss"));
            stm.setString(6, DateTimeUtils.getCurrentTime("yyyy-MM-dd HH:mm:ss"));
            stm.executeUpdate();
        }
    }
}
