/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.vinplay.vbee.common.exceptions.KeyNotFoundException
 *  com.vinplay.vbee.common.messages.OtpMessage
 *  com.vinplay.vbee.common.models.OtpModel
 */
package com.vinplay.usercore.service;

import com.vinplay.usercore.entities.MessageMTResponse;
import com.vinplay.vbee.common.exceptions.KeyNotFoundException;
import com.vinplay.vbee.common.messages.OtpMessage;
import com.vinplay.vbee.common.models.OtpModel;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.concurrent.TimeoutException;

public interface OtpService {
    MessageMTResponse genMessageMT(OtpMessage var1, String var2) throws Exception;

    boolean logOTP(OtpMessage var1) throws IOException, TimeoutException, InterruptedException;

    boolean updateOtp(String var1, String var2, String var3) throws SQLException;

    int checkOtp(String var1, String var2, String var3, String var4) throws SQLException, UnsupportedEncodingException, NoSuchAlgorithmException, KeyNotFoundException;

    int checkOtpLogin(String var1, String var2, String var3, String var4, boolean var5) throws Exception;

    String revertMobile(String var1);

    OtpModel CheckValidSMS(String var1) throws SQLException;

    int getOdp(String var1) throws Exception;

    int getVoiceOdp(String var1) throws Exception;

    int getOdp(String var1, String var2) throws Exception;

    int checkOdp(String var1, String var2) throws Exception;

    int checkOtpSmsForApp(String var1, String var2) throws Exception;

    int sendOtpEsms(String var1, String var2) throws Exception;

    int sendVoiceOtp(String var1, String var2) throws Exception;

    int sendOdpEsms(String var1, String var2) throws Exception;

    int checkOtpEsms(String var1, String var2) throws Exception;

    int getEsmsOTP(String var1, String var2, String var3) throws Exception;

    String GenerateOTP(String var1, String var2) throws Exception;

    String GenerateOdp(String var1, String var2) throws Exception;

    int checkAppOTP(String var1, String var2);
}

