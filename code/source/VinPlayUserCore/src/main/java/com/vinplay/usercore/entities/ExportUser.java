/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.usercore.entities;

public class ExportUser {
    private String nick_name;
    private String mobile;
    private long recharge_money;

    public String getNick_name() {
        return this.nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public long getRecharge_money() {
        return this.recharge_money;
    }

    public void setRecharge_money(long recharge_money) {
        this.recharge_money = recharge_money;
    }
}

