/*
 * Decompiled with CFR 0.150.
 */
package com.vinplay.dichvuthe.response;

public class ZoanMomoReq {
    private String from;
    private String shipper;
    private long amount;
    private String comment;
    private String request_id;
    private long trans_time;
    private String trans_id;
    private String signature;

    public String getFrom() {
        return this.from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getShipper() {
        return this.shipper;
    }

    public void setShipper(String shipper) {
        this.shipper = shipper;
    }

    public long getAmount() {
        return this.amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public String getRequest_id() {
        return this.request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    public long getTrans_time() {
        return this.trans_time;
    }

    public void setTrans_time(long trans_time) {
        this.trans_time = trans_time;
    }

    public String getTrans_id() {
        return this.trans_id;
    }

    public void setTrans_id(String trans_id) {
        this.trans_id = trans_id;
    }

    public String getSignature() {
        return this.signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getComment() {
        return this.comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "ZoanMomoReq{" +
                "from='" + from + '\'' +
                ", shipper='" + shipper + '\'' +
                ", amount=" + amount +
                ", comment='" + comment + '\'' +
                ", request_id='" + request_id + '\'' +
                ", trans_time=" + trans_time +
                ", trans_id='" + trans_id + '\'' +
                ", signature='" + signature + '\'' +
                '}';
    }
}

