package com.vinplay.vbee.common.cp;

import casio.king365.GU;
import casio.king365.util.KingUtil;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Constructor;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class BaseController<T, R> {
    private final Map<Integer, BaseProcessor<T, R>> map = new HashMap<>();
    private static final Logger logger = Logger.getLogger("api");
    private ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(
            50, new ThreadFactoryBuilder().setNameFormat("BaseController-thread-%d").build());

    public void initCommands(Map<Integer, String> commandMap) {
        commandMap.forEach((k, v) -> {
            if (v == null) return;
            try {
                Class<?> clazz = Class.forName(v);
                Constructor<?> ctor = clazz.getConstructor();
                BaseProcessor<T, R> processor = (BaseProcessor<T, R>) ctor.newInstance(new Object[0]);
                this.map.put(k, processor);
            } catch (ClassNotFoundException ex) {
                GU.sendOperation("[CẢNH BÁO - NoCommandRegistered] Class not found: " + v);
                logger.error("Class not found " + v);
            } catch (Exception ex) {
                GU.sendOperation("[CẢNH BÁO - init command] " + ex.getMessage());
                ex.printStackTrace();
            }
        });
    }

    public R processCommand(Integer command, Param<T> param) {
        BaseProcessor<T, R> processor = this.map.get(command);
        String paramData = "";
        try {
            Param<HttpServletRequest> httpParam = (Param<HttpServletRequest>) param;
            HttpServletRequest request = httpParam.get();
            Enumeration enumeration = request.getParameterNames();
            while (enumeration.hasMoreElements()) {
                String parameterName = (String) enumeration.nextElement();
                paramData += "&" + parameterName + "=" + request.getParameter(parameterName);
            }
        } catch (Exception e) {
        }
        KingUtil.printLog("process " + processor.getClass().getName() + " params: " + paramData);
        ScheduledFuture future = scheduledExecutorService.schedule(() -> {
            GU.sendOperation("[CẢNH BÁO - KHẢ NĂNG LỖI]Xử lý controller chậm hơn "
                    + processor.getShouldAlertProcessingTimeInMillisecond() + " ms: " + command + " - " + processor.getClass().getName());
        }, processor.getShouldAlertProcessingTimeInMillisecond(), TimeUnit.MILLISECONDS);
        R r = null;
        try {
            if (processor == null) {
                throw new NoCommandRegistered("Command " + command + " not found");
            }
            LocalDateTime start = LocalDateTime.now();
            r = processor.execute(param);
            Duration duration = Duration.between(start, LocalDateTime.now());
            long durationInMs = duration.toMillis();
            if (durationInMs > processor.getShouldWarningProcessingTimeInMillisecond()) {
                GU.sendOperation("[CẢNH BÁO]Xử lý controller chậm: " + command + " - " + processor.getClass().getName()
                        + " - " + durationInMs + " ms. Param: " + paramData);
            }
            return r;
        } catch (Exception ex) {
            ex.printStackTrace();
            GU.sendOperation("[CẢNH BÁO LỖI]processCommand: " + processor.getClass().getName() + " " + ex.getMessage());
        } finally {
            if (!future.isDone()) {
                future.cancel(true);
            }
        }
        return null;
    }
}