package vn.yotel.payment;

import casio.king365.Constants;
import casio.king365.util.KingUtil;
import com.vinplay.vbee.common.utils.NumberUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vn.yotel.payment.buycard.api.RSAUtil;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

public class CoinPaymentClient {

    public static Map<String, String> errors;

    public static int getUsdtToVndRate(){
        return Integer.parseInt(Constants.CoinPaymentInfo.usdtToVndRate);
    }

    public static void getListErrors() throws Exception {
        if(true)
            return;
        String url = Constants.CoinPaymentInfo.CoinPaymentInfo_urlError;
        Map<String, String> headers = new HashMap<>();
        headers.put("x-version", Constants.CoinPaymentInfo.xVersion);
        headers.put("x-sign", KingUtil.MD5(Constants.CoinPaymentInfo.partnerName+Constants.CoinPaymentInfo.secretKey));
        headers.put("x-token", Constants.CoinPaymentInfo.token);
        headers.put("Content-Type", "application/json; charset=utf-8");
        headers.put("User-Agent", "okhttp/3.9.1");
        String postResult = KingUtil.postOkHttp(url, "{}", headers);
        if(postResult.equals(""))
            throw new Exception("Data nhận về từ cổng coin rỗng");
        JSONObject result = new JSONObject(postResult);
        String code = result.getString("code");
        if(code.equals("200")){
            errors = new HashMap<>();
            JSONObject data = result.getJSONObject("data");
            JSONArray keys = data.names ();
            for (int i = 0; i < keys.length(); i++) {
                String key = keys.getString(i); // Here's your key
                String value = data.getString(key); // Here's your value
                errors.put(key, value);
            }
        }
    }

    public static CoinWallet createWallet(String nickname) throws Exception {
        KingUtil.printLog("CoinPaymentClient createWallet() nickname: "+nickname);
        if(errors == null)
            getListErrors();
        String url = Constants.CoinPaymentInfo.CoinPaymentInfo_urlCreateWallet;
        Map<String, String> headers = new HashMap<>();
        headers.put("x-version", Constants.CoinPaymentInfo.xVersion);
        headers.put("x-sign", KingUtil.MD5(Constants.CoinPaymentInfo.partnerName+Constants.CoinPaymentInfo.secretKey));
        headers.put("x-token", Constants.CoinPaymentInfo.token);
        JSONObject requestData = new JSONObject();
        requestData.put("wallet_code", nickname);
        String jsonRequestData = requestData.toString();
        KingUtil.printLog("jsonRequestData: " + jsonRequestData);
        String postResult = KingUtil.postOkHttp(url, jsonRequestData, headers);
        KingUtil.printLog("CoinPaymentClient createWallet() postResult: "+postResult);
        if(postResult.equals(""))
            throw new Exception("Data nhận về từ cổng coin rỗng");
        JSONObject result = new JSONObject(postResult);
        String code = result.getString("code");
        if(code.equals("200")){
            JSONObject data = result.getJSONObject("data");
            return walletFromJson(data);
        } else
            throw new Exception(errors.get(code));
    }

    public static CoinWallet getWallet(String nickname) throws Exception {
        KingUtil.printLog("CoinPaymentClient getWallet() nickname: "+nickname);
        if(errors == null)
            getListErrors();
        String url = Constants.CoinPaymentInfo.CoinPaymentInfo_urlGetWallet+nickname;
        Map<String, String> headers = new HashMap<>();
        headers.put("x-version", Constants.CoinPaymentInfo.xVersion);
        headers.put("x-sign", KingUtil.MD5(Constants.CoinPaymentInfo.partnerName+Constants.CoinPaymentInfo.secretKey));
        headers.put("x-token", Constants.CoinPaymentInfo.token);
        String postResult = KingUtil.getOkHttp(url, headers);
        KingUtil.printLog("CoinPaymentClient getWallet() postResult: "+postResult);
        if(postResult.equals(""))
            throw new Exception("Data nhận về từ cổng coin rỗng");
        JSONObject result = new JSONObject(postResult);
        String code = result.getString("code");
        if(code.equals("200")){
            JSONObject data = result.getJSONObject("data");
            return walletFromJson(data);
        } else
            throw new Exception(errors.get(code));
    }

    public static CoinWallet reActiveWallet(String nickname) throws Exception {
        KingUtil.printLog("CoinPaymentClient reActiveWallet() nickname: "+nickname);
        if(errors == null)
            getListErrors();
        String url = Constants.CoinPaymentInfo.CoinPaymentInfo_urlReActiveWallet;
        Map<String, String> headers = new HashMap<>();
        headers.put("x-version", Constants.CoinPaymentInfo.xVersion);
        headers.put("x-sign", KingUtil.MD5(Constants.CoinPaymentInfo.partnerName+Constants.CoinPaymentInfo.secretKey));
        headers.put("x-token", Constants.CoinPaymentInfo.token);
        JSONObject requestData = new JSONObject();
        requestData.put("wallet_code", nickname);
        requestData.put("status", 1);
        String jsonRequestData = requestData.toString();
        KingUtil.printLog("jsonRequestData: " + jsonRequestData);
        String postResult = KingUtil.postOkHttp(url, jsonRequestData, headers);
        KingUtil.printLog("CoinPaymentClient reActiveWallet() postResult: "+postResult);
        if(postResult.equals(""))
            throw new Exception("Data nhận về từ cổng coin rỗng");
        JSONObject result = new JSONObject(postResult);
        String code = result.getString("code");
        if(code.equals("200")){
            JSONObject data = result.getJSONObject("data");
            return walletFromJson(data);
        } else
            throw new Exception(errors.get(code));
    }

    public static void checkRecharge(String nickname) throws Exception {
        KingUtil.printLog("CoinPaymentClient checkRecharge() nickname: "+nickname);
        if(errors == null)
            getListErrors();
        String url = Constants.CoinPaymentInfo.CoinPaymentInfo_urlRecharge;
        Map<String, String> headers = new HashMap<>();
        headers.put("x-version", Constants.CoinPaymentInfo.xVersion);
        headers.put("x-sign", KingUtil.MD5(Constants.CoinPaymentInfo.partnerName+Constants.CoinPaymentInfo.secretKey));
        headers.put("x-token", Constants.CoinPaymentInfo.token);
        // headers.put("Host", "localhost:9090");
        // headers.put("User-Agent", "okhttp/3.9.1");
        JSONObject requestData = new JSONObject();
        requestData.put("wallet_code", nickname);
        requestData.put("start_time", Instant.now().toEpochMilli() - 600_000);  // 600 000 là 10 phút (đổi ra milisecond)
        requestData.put("end_time", Instant.now().toEpochMilli());
        String jsonRequestData = requestData.toString();
        KingUtil.printLog("jsonRequestData: " + jsonRequestData);
        String postResult = KingUtil.postOkHttp(url, jsonRequestData, headers);
        KingUtil.printLog("CoinPaymentClient checkRecharge() postResult: "+postResult);
        if(postResult.equals(""))
            throw new Exception("Data nhận về từ cổng coin rỗng");
        JSONObject result = new JSONObject(postResult);
        String code = result.getString("code");
        if(!code.equals("200"))
            throw new Exception(errors.get(code));
    }

    public static String withdraw(String targerAddress, String token, String tokenType, int amountInt, String requestId) throws Exception {
        Double amount = new Double(amountInt);
        KingUtil.printLog("CoinPaymentClient withdraw() targerAddress: "+targerAddress+", token: "+token
                +", tokenType: "+tokenType+", amount: "+amount);
        if(errors == null)
            getListErrors();
        String url = Constants.CoinPaymentInfo.CoinPaymentInfo_urlWithdraw;

        JSONObject requestData = new JSONObject();
        // String requestId = Instant.now().toEpochMilli()+"_"+ NumberUtils.ranDomIntMinMax(1, 10000);
        requestData.put("request_id", requestId);
        requestData.put("partner_username", Constants.CoinPaymentInfo.partnerName);
        requestData.put("targer_address", targerAddress);
        requestData.put("token_type", tokenType);
        requestData.put("token", token);
        requestData.put("amount", amount);
        String textToSign = "partner_username="+Constants.CoinPaymentInfo.partnerName
                +"&request_id=" +requestId
                +"&amount=" +amount
                +"&targer_address=" +targerAddress
                +"&token="+token;
        KingUtil.printLog("text to sign: "+textToSign);
        requestData.put("signature", RSAUtil.sign(Constants.CoinPaymentInfo.privateKey, textToSign));
        KingUtil.printLog("signed : "+requestData.getString("signature"));
        String jsonRequestData = requestData.toString();

        Map<String, String> headers = new HashMap<>();
        headers.put("x-version", Constants.CoinPaymentInfo.xVersion);
        headers.put("x-sign", KingUtil.MD5(Constants.CoinPaymentInfo.partnerName+Constants.CoinPaymentInfo.secretKey));
        headers.put("x-token", Constants.CoinPaymentInfo.token);
        //headers.put("Host", "localhost:9090");
        headers.put("User-Agent", "okhttp/3.9.1");

        KingUtil.printLog("jsonRequestData: " + jsonRequestData);
        String postResult = KingUtil.postOkHttp(url, jsonRequestData, headers);
        KingUtil.printLog("CoinPaymentClient withdraw() postResult: "+postResult);
        if(postResult.equals(""))
            throw new Exception("Data nhận về từ cổng coin rỗng");

        return postResult;
    }

    public static String checkWithdraw(String refId) throws Exception {
        KingUtil.printLog("CoinPaymentClient checkWithdraw() refId: "+refId);
        if(errors == null)
            getListErrors();
        String url = Constants.CoinPaymentInfo.CoinPaymentInfo_urlCheckWithdraw+refId;
        Map<String, String> headers = new HashMap<>();
        headers.put("x-version", Constants.CoinPaymentInfo.xVersion);
        headers.put("x-sign", KingUtil.MD5(Constants.CoinPaymentInfo.partnerName+Constants.CoinPaymentInfo.secretKey));
        headers.put("x-token", Constants.CoinPaymentInfo.token);
        String postResult = KingUtil.getOkHttp(url, headers);
        KingUtil.printLog("CoinPaymentClient checkWithdraw() postResult: "+postResult);
        if(postResult.equals(""))
            throw new Exception("Data nhận về từ cổng coin rỗng");
        return postResult;
    }

    public static CoinWallet walletFromJson(JSONObject json) throws JSONException {
        CoinWallet wallet = new CoinWallet();
        if(json.has("wallet_code"))
            wallet.wallet_code = json.getString("wallet_code");
        if(json.has("address"))
            wallet.address = json.getString("address");
        if(json.has("token"))
            wallet.token = json.getString("token");
        if(json.has("token_type"))
            wallet.tokenType = json.getString("token_type");
        if(json.has("status"))
            wallet.status = (byte) json.getInt("status");
        if(json.has("active_time"))
            wallet.active_time = json.getLong("active_time");
        if(json.has("expired_time"))
            wallet.expired_time = json.getLong("expired_time");
        return wallet;
    }

    public static class CoinWallet implements Serializable {
        String wallet_code;
        String address;
        String token;
        String tokenType;
        byte status;
        long active_time;
        long expired_time;

        @Override
        public String toString() {
            return "CoinWallet{" +
                    "wallet_code='" + wallet_code + '\'' +
                    ", address='" + address + '\'' +
                    ", token='" + token + '\'' +
                    ", tokenType='" + tokenType + '\'' +
                    ", status=" + status +
                    ", active_time=" + active_time +
                    ", expired_time=" + expired_time +
                    '}';
        }

        public JSONObject toJsonObject(){
            JSONObject json = new JSONObject();
            try {
                json.put("wallet_code", wallet_code);
                json.put("address", address);
                json.put("token", token);
                json.put("tokenType", tokenType);
                json.put("status", status);
                json.put("active_time", active_time);
                json.put("expired_time", expired_time);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return json;
        }
    }
}
