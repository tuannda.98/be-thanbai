package vn.yotel.yoker.service.impl;

import vn.yotel.yoker.dao.RequestIapDao;
import vn.yotel.yoker.dao.impl.RequestIapDaoImpl;
import vn.yotel.yoker.domain.RequestIap;
import vn.yotel.yoker.service.RequestIapBo;

public class RequestIapBoImpl implements RequestIapBo {
    private RequestIapDao requestIapDao = new RequestIapDaoImpl();
    @Override
    public boolean existTransId(String transId) throws Exception {
        return requestIapDao.existTransId(transId);
    }

    @Override
    public boolean create(RequestIap requestIap) throws Exception {
        return requestIapDao.create(requestIap);
    }
}
