package vn.yotel.yoker.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class DateUtil
{
	public static void main(String[] args)
	{
		//	String expire="00:00:00 17/04/2025";
		//DateUtil.showDateInMonth();
		System.out.println(DateUtil.isDateInMonth(30));
	}

	public static void showDateInMonth()
	{
		//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.set(Calendar.DAY_OF_MONTH,1);
		int myMonth = cal.get(Calendar.MONTH);
		while(myMonth == cal.get(Calendar.MONTH))
		{
			System.out.println(cal.get(Calendar.DAY_OF_MONTH));
			cal.add(Calendar.DAY_OF_MONTH,1);
		}
	}

	public static boolean isDateInMonth(int day)
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.set(Calendar.DAY_OF_MONTH,1);
		int myMonth = cal.get(Calendar.MONTH);
		while(myMonth == cal.get(Calendar.MONTH))
		{
			int date = cal.get(Calendar.DAY_OF_MONTH);
			if(day == date)
				return true;
			cal.add(Calendar.DAY_OF_MONTH,1);
		}
		return false;
	}

	public static boolean isToday(Timestamp lastLoginTime)
	{
		boolean result = true;
		if(lastLoginTime == null)
			result = false;
		else
		{
			Date lastLoginDate = new Date(lastLoginTime.getTime());
			lastLoginDate = removeTime(lastLoginDate);

			Date today = new Date();
			today = removeTime(today);
			result = today.equals(lastLoginDate);
		}
		return result;
	}

	public static Date removeTime(Date date)
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY,0);
		cal.set(Calendar.MINUTE,0);
		cal.set(Calendar.SECOND,0);
		cal.set(Calendar.MILLISECOND,0);
		return cal.getTime();
	}

	public static Date addDay(Date date,int days)
	{
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE,days);  // number of days to add
		return c.getTime();  // dt is now the new date
	}

	public static Timestamp getCurrTimetamp()
	{
		Calendar c = Calendar.getInstance();
		Timestamp currentTimestamp = new Timestamp(c.getTime().getTime());
		return currentTimestamp;  // dt is now the new date
	}

	public static String getDateDiffLottery()
	{
		Date now = new Date();
		Date endDate = new Date();
		TimeUnit timeUnit = TimeUnit.HOURS;
		Calendar cal = Calendar.getInstance();
		cal.setTime(endDate);
		cal.set(Calendar.HOUR_OF_DAY,6);
		cal.set(Calendar.MINUTE,0);
		cal.set(Calendar.SECOND,0);
		cal.set(Calendar.MILLISECOND,0);

		long diffInMillies = now.getTime() - cal.getTime().getTime();
		StringBuilder timeLeft = new StringBuilder();
		long hours = timeUnit.convert(diffInMillies,TimeUnit.HOURS);
		long minutes = timeUnit.convert(diffInMillies,TimeUnit.MINUTES);
		long seconds = timeUnit.convert(diffInMillies,TimeUnit.SECONDS);
		timeLeft.append(hours);
		timeLeft.append(":");
		timeLeft.append(minutes);
		timeLeft.append(":");
		timeLeft.append(seconds);
		return timeLeft.toString();
	}

	/**
	 * Get a diff between two dates
	 *
	 * @param dateOld
	 * @param dateNew
	 * @param timeUnit the unit in which you want the diff
	 * @return the diff value, in the provided unit
	 */
	public static long getDateDiff(Date dateOld,Date dateNew,TimeUnit timeUnit)
	{
		long diffInMillies = dateNew.getTime() - dateOld.getTime();
		return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
	}

	public static String getMMddYYYYHHmmss()
	{
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		Date today = Calendar.getInstance().getTime();
		return df.format(today);
	}

	public static String getMMddYYYYHHmm()
	{
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		Date today = Calendar.getInstance().getTime();
		return df.format(today);
	}

	public static String getMMddYYYYHHmmss(Date date)
	{
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		if(date == null)
			return "";
		return df.format(date);
	}

	public static String getMMddYYYY(Date date)
	{
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		if(date == null)
			return "";
		return df.format(date);
	}

	public static String getMMddYYYY(String HHmmssddMMyyyy)
	{
		//00:00:00 17/04/2025
		DateFormat format = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		Date date;
		try
		{
			date = format.parse(HHmmssddMMyyyy);
			if(date == null)
				return "";
			return df.format(date);
		}
		catch(ParseException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}

	public static String dateToString(Date date,String formatString)
	{
		DateFormat dateFormat = new SimpleDateFormat(formatString);//"yyyy/MM/dd HH:mm:ss");	
		return dateFormat.format(date);
	}
}
