package vn.yotel.yoker.domain;

import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

/**
 * The persistent class for the user_cashout database table.
 *
 */
@Entity
@Table(name = "cashout_request")
@NamedQuery(name = "CashoutRequest.findAll",query = "SELECT u FROM CashoutRequest u")
public class CashoutRequest implements Serializable
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Expose
	private Integer id;

	// @ManyToOne
	@Column(name="user_id")
	@Expose
	private Integer userId;

	@Expose
	private Byte status;

	@Expose
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "reques_time")
	private Timestamp requesTime;

	@Expose
	@Transient
	private String requesTimeStr;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "process_time")
	@Expose
	private Timestamp processTime;

	@Expose
	private String note;

	@Expose
	@Column(name = "extra_data")
	private String extraData;

	// @ManyToOne
	// @JoinColumn(name = "item_id")
	@Expose
	private Integer itemId;

	@Expose
	private Integer quantity;

	@Column(name = "user_name")
	@Expose
	private String userName;

	@Column(name = "user_mobile")
	@Expose
	private String userMobile;

	@Expose
	@Column(name = "action_note")
	private String actionNote;

	@Expose
	@Column(name = "client")
	private String client;

	@Expose
	@Column(name = "platform")
	private String platform;

	@Expose
	@Transient
	private Long rechargeMoney;

	@Expose
	@Transient
	private Long cashoutMoney;

	public CashoutRequest()
	{
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	public Date getRequesTime() {
		return requesTime;
	}

	public void setRequesTime(Timestamp requesTime) {
		this.requesTime = requesTime;
	}

	public Date getProcessTime() {
		return processTime;
	}

	public void setProcessTime(Timestamp processTime) {
		this.processTime = processTime;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Integer getItemId() {
		return itemId;
	}

	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserMobile() {
		return userMobile;
	}

	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}

	public String getExtraData() {
		return extraData;
	}

	public void setExtraData(String extraData) {
		this.extraData = extraData;
	}

	public String getRequesTimeStr() {
		return requesTimeStr;
	}

	public void setRequesTimeStr(String requesTimeStr) {
		this.requesTimeStr = requesTimeStr;
	}

	public String getActionNote() {
		return actionNote;
	}

	public void setActionNote(String actionNote) {
		this.actionNote = actionNote;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public Long getRechargeMoney() {
		return rechargeMoney;
	}

	public void setRechargeMoney(Long rechargeMoney) {
		this.rechargeMoney = rechargeMoney;
	}

	public Long getCashoutMoney() {
		return cashoutMoney;
	}

	public void setCashoutMoney(Long cashoutMoney) {
		this.cashoutMoney = cashoutMoney;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	@Override
	public String toString() {
		return "CashoutRequest{" +
				"id=" + id +
				", userId=" + userId +
				", status=" + status +
				", requesTime=" + requesTime +
				", requesTimeStr='" + requesTimeStr + '\'' +
				", processTime=" + processTime +
				", note='" + note + '\'' +
				", extraData='" + extraData + '\'' +
				", itemId=" + itemId +
				", quantity=" + quantity +
				", userName='" + userName + '\'' +
				", userMobile='" + userMobile + '\'' +
				", actionNote='" + actionNote + '\'' +
				", client='" + client + '\'' +
				", platform='" + platform + '\'' +
				", rechargeMoney=" + rechargeMoney +
				", cashoutMoney=" + cashoutMoney +
				'}';
	}
}
