package vn.yotel.payment.buycard.entities;

import java.util.List;

/**
 * @author emo
 * Jun 23, 2019
 */
public class Data{
	public String partner_reference;
	public String server_references;
	public String telco;
	public String demonition;
	public String trans_time;
	public List<Card> cards;

	@Override
	public String toString()
	{
		return "Data{" + "partner_reference=" + partner_reference + ", server_references=" + server_references + ", telco=" + telco + ", demonition=" + demonition + ", trans_time=" + trans_time + ", cards=" + cards + '}';
	}
}