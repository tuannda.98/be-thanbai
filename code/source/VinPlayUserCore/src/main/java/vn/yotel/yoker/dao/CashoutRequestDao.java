package vn.yotel.yoker.dao;

import casio.king365.Constants;
import vn.yotel.yoker.domain.CashoutRequest;

import java.sql.Timestamp;
import java.util.List;

public interface CashoutRequestDao extends GenericDao<CashoutRequest> {
    List<CashoutRequest> findAll(int page, int numPerPage) throws Exception;
    List<CashoutRequest> findAllByDate(Timestamp beginDate, Timestamp endDate) throws Exception;
    List<CashoutRequest> findAllByDateStatus(Timestamp beginDate, Timestamp endDate, byte status) throws Exception;
    List<CashoutRequest> findAllByUserStatus(String nickName, byte status) throws Exception;
    CashoutRequest getRandomPendingBankRequest() throws Exception;
    void updateClient(String nickName, String client) throws Exception;
    void updatePlatform(String nickName, String pf) throws Exception;
}
