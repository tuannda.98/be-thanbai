package vn.yotel.payment;

import com.google.gson.annotations.Expose;

public class CardPaymentResponse {
	
	@Expose private String code;
	@Expose private String message;
	@Expose private String txn_id;
	@Expose private String card_amount;
	@Expose private String net_amount;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getTxn_id() {
		return txn_id;
	}
	public void setTxn_id(String txn_id) {
		this.txn_id = txn_id;
	}
	public String getCard_amount() {
		return card_amount;
	}
	public void setCard_amount(String card_amount) {
		this.card_amount = card_amount;
	}
	public String getNet_amount() {
		return net_amount;
	}
	public void setNet_amount(String net_amount) {
		this.net_amount = net_amount;
	}
	
}
