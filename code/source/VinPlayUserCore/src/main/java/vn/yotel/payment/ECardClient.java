package vn.yotel.payment;

import casio.king365.Constants;
import casio.king365.GU;
import casio.king365.util.KingUtil;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

public class ECardClient {
    private static final Logger logger = Logger.getLogger("api");
    private final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0";
    private String url = "http://v3.izisoft.info/api";
    private final String username;// = "stv";
    private final String password;// = "dacuoi";
    private String scratchCallbackUrl;//"http://vua365.com/api/payment/card_callback?orderId="
    private static final Gson gson = new GsonBuilder().serializeNulls().excludeFieldsWithoutExposeAnnotation().create();
    private static ECardClient instance;
    private final LoadingCache<String, String> sid;
    private final Map<Integer, Integer> denominations = new HashMap<>();

    public synchronized static ECardClient getInstance() {
        if (instance != null)
            return instance;

        instance = new ECardClient(Constants.CardinInfo.apiUrl
                , Constants.CardinInfo.username, Constants.CardinInfo.pw);
        instance.scratchCallbackUrl = Constants.CardinInfo.callbackUrl;//"http://vua365.com/api/payment/card_callback?orderId=";
        return instance;
    }

    private ECardClient(String url, String username, String password) {
        super();
        this.url = url;
        this.username = username;
        this.password = password;
        this.sid = CacheBuilder.newBuilder()
                .maximumSize(2)
                .expireAfterWrite(10, TimeUnit.MINUTES)
                .build(new CacheLoader<String, String>() {  // build the cacheloader
                    @Override
                    public String load(String k) throws Exception {
                        return login();
                    }
                });
    }

    public static void main(String[] args) throws Exception {
//		ECardClient http = new ECardClient("http://pushcard.zoan.info/api","stv","dacuoi");
////		ECardClient http = ECardClient.getInstance();
//		http.scratchCallbackUrl = "https://vua365.com/apigame/system/zoan_callback.jsp";
//		String requestId = "requestId" + String.valueOf(System.currentTimeMillis());
//		String rep = http.createCard(requestId,"VNP","56646629931443","59000001101284",50000);
//		System.out.println("rep:" + rep);
        ECardClient http = ECardClient.getInstance();
        System.out.println("login: ");
        System.out.println(http.login());
    }

    public String login() throws Exception {
        String call = url + "/user/login";
        URL obj = new URL(call);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        con.setRequestProperty("Content-Type", "application/json; charset=utf8");
        con.setRequestProperty("Accept", "application/json");
        con.setRequestProperty("x-partner", Constants.CardinInfo.username);
        con.setRequestProperty("x-version", "2");
        con.setConnectTimeout(5000); //set timeout to 5 seconds
        Map<String, Object> mapResp = new ConcurrentHashMap<>();
        mapResp.put("username", username);
        mapResp.put("password", password);
        String urlParameters = gson.toJson(mapResp);
        con.setDoOutput(true);
        try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
            wr.writeBytes(urlParameters);
            wr.flush();
        }

        int responseCode = con.getResponseCode();
        logger.debug("ECardClient URL : " + call);
        logger.debug("ECardClient Parameters : " + urlParameters);
        logger.debug("ECardClient Response Code : " + responseCode);
        System.out.println("ECardClient URL : " + call);
        System.out.println("ECardClient Parameters : " + urlParameters);
        System.out.println("ECardClient Response Code : " + responseCode);

        if (responseCode != HttpURLConnection.HTTP_OK)
            return "";
        StringBuilder response = new StringBuilder();
        try (InputStreamReader inputStreamReader = new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8);
             BufferedReader in = new BufferedReader(inputStreamReader)) {
            String inputLine;

            while ((inputLine = in.readLine()) != null)
                response.append(inputLine);
        }
        logger.debug("ECardClient response: " + response.toString());
        System.out.println("ECardClient response: " + response.toString());

        String sid = getSidCode(response.toString());
        try {
            getDenominations(sid);
        } catch (Exception e){
            KingUtil.printLog("ECardClient getDenominations exception: "+KingUtil.printException(e));
            GU.sendOperation("Lỗi khi lấy mệnh giá: " + KingUtil.printException(e));
        }
        return sid;
    }

    private void getDenominations(String sid) throws Exception {
        String call = url + "/denominations?auth=" + sid;
        URL obj = new URL(call);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        con.setRequestProperty("Content-Type", "application/json; charset=utf8");
        con.setRequestProperty("Accept", "application/json");
        con.setRequestProperty("x-partner", Constants.CardinInfo.username);
        con.setRequestProperty("x-version", "2");
        con.setConnectTimeout(10000); //set timeout to 10 seconds

        int responseCode = con.getResponseCode();
        if (responseCode != HttpURLConnection.HTTP_OK)
            return;
        StringBuilder response = new StringBuilder();
        try (InputStreamReader inputStreamReader = new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8);
             BufferedReader in = new BufferedReader(inputStreamReader)) {
            String inputLine;

            while ((inputLine = in.readLine()) != null)
                response.append(inputLine);
        }
        logger.debug("ECardClient getDenominations response: " + response.toString());
        JSONObject result = new JSONObject(response.toString());
        JSONArray resultData = result.getJSONArray("data");
        denominations.clear();
        for(int i=0; i<resultData.length(); i++){
            JSONObject deno = resultData.getJSONObject(i);
            denominations.put(deno.getInt("id"), deno.getInt("value"));
        }
    }

    public String createCard(String requestId, String provider, String pin, String serial, int value) throws Exception {
        String call = url + "/v2/cp/card?auth=" + sid.get("SID");
        URL obj = new URL(call);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        con.setRequestProperty("Content-Type", "application/json; charset=utf8");
        con.setRequestProperty("Accept", "application/json");
        con.setRequestProperty("x-partner", Constants.CardinInfo.username);
        con.setRequestProperty("x-version", "2");
        KingUtil.printLog("createCard x-partner: "+Constants.CardinInfo.username);
        KingUtil.printLog("createCard x-partner 2: "+con.getRequestProperty("x-partner"));
        KingUtil.printLog("createCard x-version: "+con.getRequestProperty("x-version"));
        con.setConnectTimeout(5000); //set timeout to 5 seconds

        Map<String, Object> mapResp = new ConcurrentHashMap<>();
        mapResp.put("telcoId", getProviderId(provider));
        mapResp.put("denId", getDenId(value));
        mapResp.put("code", pin);
        mapResp.put("serial", serial);
        mapResp.put("scratchCallbackUrl", scratchCallbackUrl + "&orderId=" + requestId);
        String urlParameters = gson.toJson(mapResp);
        con.setDoOutput(true);
        try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
            wr.writeBytes(urlParameters);
            wr.flush();
        }

        int responseCode = con.getResponseCode();
        logger.debug("ECardClient URL : " + call);
        logger.debug("ECardClient Parameters : " + urlParameters);
        logger.debug("ECardClient Response Code : " + responseCode);
        if (responseCode != HttpURLConnection.HTTP_OK)
            return "";
        StringBuilder response = new StringBuilder();
        try (InputStreamReader inputStreamReader = new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8);
             BufferedReader in = new BufferedReader(inputStreamReader)) {
            String inputLine;

            while ((inputLine = in.readLine()) != null)
                response.append(inputLine);
        }
        logger.debug("ECardClient response: " + response.toString());
        return response.toString();
    }
//
//    public String checkCard(String providerid) throws Exception {
//        String call = url + "/v2/cp/card/" + providerid;
//        URL obj = new URL(call);
//        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
//        con.setRequestMethod("GET");
//        con.setRequestProperty("User-Agent", USER_AGENT);
//        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
//        con.setRequestProperty("Content-Type", "application/json; charset=utf8");
//        con.setRequestProperty("Accept", "application/json");
//        con.setConnectTimeout(5000); //set timeout to 5 seconds
//        con.setDoOutput(true);
//        try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
//            wr.flush();
//        }
//
//        int responseCode = con.getResponseCode();
//        logger.debug("ECardClient URL : " + call);
//        logger.debug("ECardClient Response Code : " + responseCode);
//        StringBuilder response = new StringBuilder();
//        if (responseCode == HttpURLConnection.HTTP_OK)
//            try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8))) {
//                String inputLine;
//
//                while ((inputLine = in.readLine()) != null)
//                    response.append(inputLine);
//            }
//        logger.debug("ECardClient response: " + response.toString());
//        return response.toString();
//    }

    public int getProviderId(String provider) {
        if (provider.equalsIgnoreCase("VTT") || provider.equalsIgnoreCase("VT") || provider.equalsIgnoreCase("VIETTEL"))
            return 1;
        else if (provider.equalsIgnoreCase("VINA") || provider.equalsIgnoreCase("VNP"))
            return 2;
        else
            return 3;
    }

    public int getDenId(int value) {
        if(denominations.containsKey(value))
            return denominations.get(value);
        if (value <= 0)
            return 81;
        else if (value == 5000)
            return 82;
        else if (value <= 10000)
            return 1;
        else if (value <= 20000)
            return 2;
        else if (value <= 30000)
            return 21;
        else if (value <= 50000)
            return 3;
        else if (value <= 100000)
            return 4;
        else if (value <= 200000)
            return 5;
        else if (value <= 300000)
            return 22;
        else if (value <= 500000)
            return 6;
        else if (value <= 1000000)
            return 41;
        else
            return 41;
    }

    public String getSidCode(String reponse) {
        String sid = "";
        JSONObject jsonobj = null;
        try {
            jsonobj = new JSONObject(reponse);
            if (jsonobj.has("data")) {
                JSONObject data = jsonobj.getJSONObject("data");
                if (data != null && data.has("sid"))
                    sid = data.getString("sid");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return sid;
    }
}
