package vn.yotel.yoker.dao;

import vn.yotel.yoker.domain.CashinNotify;

public interface CashinNotifyDao extends GenericDao<CashinNotify> {
}
