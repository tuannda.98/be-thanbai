package vn.yotel.payment;

import java.util.ArrayList;
import java.util.List;

public class CardBuyResponse {
	String resultCode;
	String orgTransId;
	int amount;
	String listCard;
	String partnerBalance;
	String trippdesKey;
	List<CardTelco> cards;
	public CardBuyResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getOrgTransId() {
		return orgTransId;
	}

	public void setOrgTransId(String orgTransId) {
		this.orgTransId = orgTransId;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getListCard() {
		return listCard;
	}
	public void setListCard(String listCard) {
		this.listCard = listCard;
		if(this.listCard!=null && this.listCard.length()>0){
			this.cards=new ArrayList<>();
			String[] StringCard=this.listCard.split(",");
			for (int i = 0; i < StringCard.length; i++) {
				String[] str=StringCard[i].split("\\|");
				CardTelco card=new CardTelco();
				card.setCode(str[0]);
				card.setSerial(str[1]);
				card.setExprice(str[2]);
				this.cards.add(card);
			}
		}
	}
	public void setListCard1(String listCard) {
		this.listCard = listCard;
		if(this.listCard!=null && this.listCard.length()>0){
			this.cards=new ArrayList<>();
			String[] StringCard=this.listCard.split(",");
			for (int i = 0; i < StringCard.length; i++) {
				String[] str=StringCard[i].split("\\|");
				CardTelco card=new CardTelco();
				card.setCode(str[0]);
				card.setSerial(str[1]);
				card.setExprice(str[2]);
				this.cards.add(card);
			}
		}
	}

	public String getPartnerBalance() {
		return partnerBalance;
	}

	public void setPartnerBalance(String partnerBalance) {
		this.partnerBalance = partnerBalance;
	}

	public String getTrippdesKey() {
		return trippdesKey;
	}

	public void setTrippdesKey(String trippdesKey) {
		this.trippdesKey = trippdesKey;
	}

	public List<CardTelco> getCards() {
		
		return cards;
	}

	public void setCards(List<CardTelco> cards) {
		this.cards = cards;
	}
}
