package vn.yotel.yoker.domain;

import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

/**
 * The persistent class for the cashin_notify database table.
 *
 */
@Entity
@Table(name = "cashin_notify")
@NamedQuery(name = "CashinNotify.findAll",query = "SELECT m FROM CashinNotify m")
public class CashinNotify implements Serializable
{
	@Id
	@Column(name="uuid")
	@Expose
	private String uuid;

	@Column(name="gate_type")
	@Expose
	private String gateType;

	@Column(name="ip")
	@Expose
	private String ip;

	@Expose
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "receive_time")
	private Timestamp receiveTime;

	@Expose
	@Column(name = "comment")
	private String comment;

	@Expose
	@Column(name = "error")
	private String error;

	@Expose
	@Column(name = "data")
	private String data;

	public CashinNotify()
	{
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Timestamp getReceiveTime() {
		return receiveTime;
	}

	public void setReceiveTime(Timestamp receiveTime) {
		this.receiveTime = receiveTime;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getGateType() {
		return gateType;
	}

	public void setGateType(String gateType) {
		this.gateType = gateType;
	}

	@Override
	public String toString() {
		return "MomoCallback{" +
				"uuid='" + uuid + '\'' +
				", gateType='" + gateType + '\'' +
				", ip='" + ip + '\'' +
				", receiveTime=" + receiveTime +
				", comment='" + comment + '\'' +
				", error=" + error +
				", data='" + data + '\'' +
				'}';
	}
}
