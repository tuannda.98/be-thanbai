package vn.yotel.yoker.dao;

import vn.yotel.yoker.domain.CashoutItem;

public interface CashoutItemDao extends GenericDao<CashoutItem> {
    CashoutItem getMaxMoney();
}
