package vn.yotel.yoker.domain;

import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The persistent class for the cashin_items database table.
 *
 */
@Entity
@Table(name = "cashin_items")
@NamedQuery(name = "CashinItem.findAll",query = "SELECT c FROM CashinItem c")
public class CashinItem implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Expose
	private Integer id;
	@Expose
	private Byte hot;
	@Expose
	private String image;
	@Expose
	@Column(name="image_url")
	private String imageUrl;
	@Expose
	private String name;
	@Expose
	private Byte status;
	@Expose
	private Byte type;
	@Expose
	private Double money;
	@Expose
	private Double gold;
	@Expose
	private String provider;

	public CashinItem()
	{
	}

	public Integer getId()
	{
		return this.id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public Byte getHot()
	{
		return this.hot;
	}

	public void setHot(byte hot)
	{
		this.hot = hot;
	}

	public String getImage()
	{
		return this.image;
	}

	public void setImage(String image)
	{
		this.image = image;
	}

	public String getImageUrl()
	{
		return imageUrl;
	}

	public void setImageUrl(String imageUrl)
	{
		this.imageUrl = imageUrl;
	}

	public String getName()
	{
		return this.name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Byte getStatus()
	{
		return this.status;
	}

	public void setStatus(byte status)
	{
		this.status = status;
	}

	public Double getMoney()
	{
		return money;
	}

	public void setMoney(double money)
	{
		this.money = money;
	}

	public Byte getType()
	{
		return type;
	}

	public void setType(byte type)
	{
		this.type = type;
	}

	public Double getGold()
	{
		return gold;
	}

	public void setGold(double gold)
	{
		this.gold = gold;
	}

	public String getProvider()
	{
		return provider;
	}

	public void setProvider(String provider)
	{
		this.provider = provider;
	}

	@Override
	public String toString()
	{
		return "CashinItem{" + "name=" + name + ", type=" + type + ", money=" + money + ", provider=" + provider + '}';
	}
}
