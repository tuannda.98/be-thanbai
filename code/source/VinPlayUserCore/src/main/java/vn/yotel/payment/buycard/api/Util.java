package vn.yotel.payment.buycard.api;

import vn.yotel.payment.buycard.entities.Card;
import vn.yotel.payment.buycard.entities.CardRes;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;


/**
 * @author emo
 * Jun 22, 2019
 */
public class Util
{
	public static String buildReference_no_ls(int quantity)
	{
		String res = "";
		for (int i = 0; i < quantity; i++) {
			if (res.isEmpty()) {
				res += System.currentTimeMillis() + i;
			}else {
				long value = System.currentTimeMillis() + i ;
				res += "," + value;
			}
		}
		return res;
	}

	public static CardRes decrypt(CardRes cardRes, String privateKey)
	{
		CardRes tmp = cardRes;
		List<Card> cards = new ArrayList<>();
		for(Card card : cardRes.data.cards)
		{
			Card card2 = card;
			try
			{
				card2.serial  = RSAUtil.decrypt(card.serial, privateKey);
				card2.pin = RSAUtil.decrypt(card.pin, privateKey);

			}
			catch(InvalidKeyException | IllegalBlockSizeException | BadPaddingException
				      | NoSuchAlgorithmException | NoSuchPaddingException e)
			{
				e.printStackTrace();
			}
			cards.add(card2);
		}
		tmp.data.cards = cards;
		return tmp;
	}
}
