package vn.yotel.yoker.dao.impl;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import vn.yotel.yoker.dao.RequestCardDao;
import vn.yotel.yoker.domain.RequestCard;

import java.util.List;

public class RequestCardDaoImpl extends AbstractGenericDao<RequestCard> implements RequestCardDao {
    @Override
    public RequestCard findByGwStransid(String gwTransId) {
        List<RequestCard> list = null;
        Transaction tx = null;
        try (Session session = openSession();) {
            tx = session.beginTransaction();
            String hql = "FROM RequestCard r WHERE r.gwTransId = :gwTransId";
            Query query = session.createQuery(hql);
            query.setParameter("gwTransId", gwTransId);
            list = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
            e.printStackTrace();
        }
        if (list == null || list.size() == 0)
            return null;

        return list.get(0);
    }
}
