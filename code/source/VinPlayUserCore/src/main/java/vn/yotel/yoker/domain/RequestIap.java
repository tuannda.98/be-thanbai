package vn.yotel.yoker.domain;

import lombok.ToString;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;


/**
 * The persistent class for the request_iap database table.
 *
 */
@Entity
@Table(name="request_iap")
@NamedQuery(name="RequestIap.findAll", query="SELECT r FROM RequestIap r")
public class RequestIap implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @Temporal(TemporalType.TIMESTAMP)
    private Timestamp datetime;

    @Column(name="eq_koin")
    private Double eqKoin;

    @Lob
    private String message;

    private Double money;

    private String note;

    @Column(name="process_status")
    private Byte processStatus;

    private String provider;

    private String transDate;

    private String transId;

    @Column(name="user_id")
    private Integer userId;

    private String username;

    public RequestIap() {
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getDatetime() {
        return this.datetime;
    }

    public void setDatetime(Timestamp datetime) {
        this.datetime = datetime;
    }

    public double getEqKoin() {
        return this.eqKoin;
    }

    public void setEqKoin(double eqKoin) {
        this.eqKoin = eqKoin;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public double getMoney() {
        return this.money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public String getNote() {
        return this.note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public byte getProcessStatus() {
        return this.processStatus;
    }

    public void setProcessStatus(byte processStatus) {
        this.processStatus = processStatus;
    }

    public String getProvider() {
        return this.provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getTransDate() {
        return this.transDate;
    }

    public void setTransDate(String transDate) {
        this.transDate = transDate;
    }

    public String getTransId() {
        return this.transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public int getUserId() { return this.userId; }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "RequestIap{" +
                "id=" + id +
                ", datetime=" + datetime +
                ", eqKoin=" + eqKoin +
                ", message='" + message + '\'' +
                ", money=" + money +
                ", note='" + note + '\'' +
                ", processStatus=" + processStatus +
                ", provider='" + provider + '\'' +
                ", transDate='" + transDate + '\'' +
                ", transId='" + transId + '\'' +
                ", userId=" + userId +
                ", username='" + username + '\'' +
                '}';
    }
}