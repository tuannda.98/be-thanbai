package vn.yotel.yoker.dao.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import casio.king365.util.KingUtil;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import vn.yotel.yoker.dao.GenericDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Example;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@SuppressWarnings("unchecked")
public abstract class AbstractGenericDao<E> implements GenericDao<E> {

    private static final Logger logger = Logger.getLogger("api");
    private static SessionFactory sessionFactory;
    private final Class<E> entityClass;

    public AbstractGenericDao() {
        this.entityClass = (Class<E>) ((ParameterizedType) this.getClass().getGenericSuperclass())
                .getActualTypeArguments()[0];
    }

    protected SessionFactory getSessionFactory() {
        if (sessionFactory == null)
            sessionFactory = new Configuration().configure().buildSessionFactory();
        return sessionFactory;
    }

//    protected Session getSession() {
//        return getSessionFactory().getCurrentSession();
//    }

    protected Session openSession() {
        return getSessionFactory().openSession();
    }

    @Override
    public E findById(final Serializable id) {
        Session session = openSession();
        E ele = session.get(this.entityClass, id);
        session.close();
        return ele;
    }

    @Override
    public Serializable save(E entity) {
        Serializable re = null;
        Session session = openSession();

        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            re = session.save(entity);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
            KingUtil.printException("AbstractGenericDao save()", e);
            re = "";
        } finally {
            session.close();
        }
        return re;
    }

    @Override
    public Serializable persist(E entity) {
        Serializable re = null;
        Session session = openSession();
        session.persist(entity);
        session.close();
        /*Transaction tx = null;
        try {
            tx = session.beginTransaction();
            re = session.save(entity);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
            e.printStackTrace();
            re = "Loi hibernate: "+e.getMessage();
        } finally {
            session.close();
        }*/
        return re;
    }

    @Override
    public void saveOrUpdate(E entity) {
        Session session = openSession();
        session.saveOrUpdate(entity);
        session.close();
    }

    @Override
    public void update(E entity) {
        Session session = openSession();
        Transaction tx = session.beginTransaction();
        session.update(entity);
        tx.commit();
        session.close();
    }

    @Override
    public void delete(E entity) {
        Session session = openSession();
        session.delete(entity);
        session.close();
    }

    @Override
    public void deleteAll() {
        Session session = openSession();
        List<E> entities = findAll();
        for (E entity : entities) {
            session.delete(entity);
        }
        session.close();
    }

    @Override
    public List<E> findAll() {
        Session session = openSession();
        // Create CriteriaBuilder
        CriteriaBuilder builder = session.getCriteriaBuilder();

        // Create CriteriaQuery
        CriteriaQuery<E> criteria = builder.createQuery(this.entityClass);

        // Specify criteria root
        criteria.from(this.entityClass);

        // Execute query
        List<E> entityList = session.createQuery(criteria).getResultList();

        session.close();
        return entityList;
    }

    @Override
    public List<E> findAllByPage(int page, int numPerPage, String sortDescColumnName) {
        try {
            Session session = openSession();
            // Create CriteriaBuilder
            CriteriaBuilder builder = session.getCriteriaBuilder();

            // Create CriteriaQuery
            CriteriaQuery<E> criteria = builder.createQuery(this.entityClass);

            // Specify criteria root
            Root<E> rootE = criteria.from(this.entityClass);

            criteria.orderBy(builder.desc(rootE.get(sortDescColumnName)));

            int firstOffset = (page - 1) * numPerPage;
            // Execute query
            List<E> entityList = session.createQuery(criteria).
                    setFirstResult(firstOffset)
                    .setMaxResults(numPerPage)
                    .getResultList();

            session.close();
            return entityList;
        } catch (Exception e) {
            KingUtil.printException("AbstractGenericDao findAllByPage", e);
        }
        return null;
    }

    @Override
    public List<E> findAllByExample(E entity) {
        Session session = openSession();
        Example example = Example.create(entity).ignoreCase().enableLike().excludeZeroes();
        List<E> re = session.createCriteria(this.entityClass).add(example).list();
        session.close();
        return re;
    }

    @Override
    public void clear() {
//        getSession().clear();
    }

    @Override
    public void flush() {
//        getSession().flush();
    }

}