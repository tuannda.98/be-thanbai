package vn.yotel.payment;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vn.yotel.yoker.util.TripleDES3;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

public class CardBuyClient {

    private final Logger LOG = LoggerFactory.getLogger(CardBuyClient.class);
    private String partnerCode = "homedirect";
    private String password = "12345678";
    private String secretKey = "12345678";
    private String url = "https://sandbox.paydirect.vn/paygate-core/rest/buyCard";

    public CardBuyClient(String url, String partnerCode, String password, String secretKey) {
        super();
        this.url = url;
        this.partnerCode = partnerCode;
        this.password = password;
        this.secretKey = secretKey;
    }

    public CardBuyClient() {
        super();
        partnerCode = "homedirect";
        password = "12345678";
        secretKey = "12345678";
        url = "https://sandbox.paydirect.vn/paygate-core/rest/buyCard";
    }

    public CardBuyResponse buyCard(String serviceCode, int price, int quantity, String orgTransID) {
        CardBuyResponse response = new CardBuyResponse();
        try {
            URL url = new URL(this.url);
            String signature = MD5(serviceCode + orgTransID + partnerCode + password + secretKey);
            //request
            JSONObject obj = new JSONObject();
            obj.put("serviceCode", serviceCode);
            obj.put("price", price);
            obj.put("quantity", quantity);
            obj.put("orgTransId", orgTransID);
            obj.put("partnerCode", this.partnerCode);
            obj.put("password", this.password);
            obj.put("signature", signature);
            String query = obj.toString();
            //open connection
            URLConnection urlc = url.openConnection();
            urlc.setRequestProperty("Content-Type", "application/json");
            urlc.setDoOutput(true);
            urlc.setAllowUserInteraction(false);
            try (PrintStream ps = new PrintStream(urlc.getOutputStream())) {
                ps.print(query);
                System.out.println("query: " + query);
            }
            //read response
            StringBuilder rep;
            try (InputStreamReader inputStreamReader = new InputStreamReader(urlc.getInputStream());
                 BufferedReader in = new BufferedReader(inputStreamReader)) {
                String inputLine;
                rep = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    rep.append(inputLine);
                }
            }
            System.out.println("rep: " + rep.toString());
            JSONObject respJson = new JSONObject(rep.toString());
            String resultCode = (String) respJson.get("resultCode");
            String orgTransId = (String) respJson.get("orgTransId");
            response.setResultCode(resultCode);
            response.setOrgTransId(orgTransId);
            if (resultCode.equalsIgnoreCase("1")) {
                String listCard = (String) respJson.get("listCard");
                String trippdesKey = (String) respJson.get("trippdesKey");
                String card = TripleDES3.decryptText(listCard, trippdesKey);
//         		LOG.debug("card: " + card);
                response.setListCard(card);
                response.setTrippdesKey(trippdesKey);
                int amount = (int) respJson.get("amount");
                response.setAmount(amount);
            }
        } catch (Exception ex) {
            LOG.error("buyCard", ex);
        }
        return response;
    }

    public static void main(String[] args) {
        CardBuyClient client = new CardBuyClient();
        CardBuyResponse resp = client.buyCard("VTT", 20000, 1, "1231232122");
        System.out.println("code: " + resp.getResultCode());
        System.out.println("cards: " + resp.getListCard());
    }

    public static String MD5(String str) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] result = md.digest((str).getBytes(StandardCharsets.UTF_8));
            StringBuilder hexString = new StringBuilder();
            for (int i = 0; i < result.length; i++) {
                String hex = Integer.toHexString(0xff & result[i]);
                if (hex.length() == 1) {
                    hexString.append('0');
                }
                hexString.append(hex);
            }
            return hexString.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public String getPartnerCode() {
        return partnerCode;
    }

    public void setPartnerCode(String partnerCode) {
        this.partnerCode = partnerCode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
