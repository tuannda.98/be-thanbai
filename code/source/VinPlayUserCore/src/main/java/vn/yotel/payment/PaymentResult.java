package vn.yotel.payment;

public class PaymentResult {
	private String transId = "";
	private String status = "-1";
	private String message = "";
	private String amount ="0";
	
	public String getTransId() {
		return transId;
	}
	public void setTransId(String transId) {
		this.transId = transId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}

	public boolean isSuccess() {
		return "1".equals(this.getStatus());
	}
	
	public int getAmountAsInt() {
		try {
			return (int) Double.parseDouble(this.getAmount());
		} catch (Exception e) {
			return 0;
		}
	}
	
	public int getStatusAsInt() {
		try {
			return Integer.parseInt(this.getStatus());
		} catch (Exception e) {
			return -1;
		}
	}
}
