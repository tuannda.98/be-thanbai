package vn.yotel.yoker.dao.impl;

import casio.king365.Constants;
import casio.king365.core.CommonDbPool;
import casio.king365.util.KingUtil;
import com.vinplay.vbee.common.pools.ConnectionPool;
import vn.yotel.yoker.dao.CashoutRequestDao;
import vn.yotel.yoker.domain.CashoutRequest;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CashoutRequestDaoImpl extends AbstractGenericDao<CashoutRequest> implements CashoutRequestDao {
    @Override
    public List<CashoutRequest> findAll(int page, int numPerPage) throws Exception {
        List<CashoutRequest> listCashoutRequest = new ArrayList<>();
        int offset = (page - 1) * numPerPage;
        String sql = "SELECT * FROM cashout_request ORDER BY id DESC LIMIT ? OFFSET ?";
        try (final Connection conn = CommonDbPool.getInstance().getVinplayDataSource().getConnection();
             final PreparedStatement stmt = conn.prepareStatement(sql);
        ) {
            stmt.setInt(1, numPerPage);
            stmt.setInt(2, offset);
            final ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                listCashoutRequest.add(bindResultSet(rs));
            }
            rs.close();
        }
        return listCashoutRequest;
    }

    @Override
    public List<CashoutRequest> findAllByDate(Timestamp beginDate, Timestamp endDate) throws Exception {
        List<CashoutRequest> listCashoutRequest = new ArrayList<>();
        String sql = "SELECT * FROM cashout_request WHERE reques_time BETWEEN ? AND ? ORDER BY id DESC";
        try (final Connection conn = CommonDbPool.getInstance().getVinplayDataSource().getConnection();
             final PreparedStatement stmt = conn.prepareStatement(sql);
        ) {
            stmt.setTimestamp(1, beginDate);
            stmt.setTimestamp(2, endDate);
            final ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                listCashoutRequest.add(bindResultSet(rs));
            }
            rs.close();
        }
        return listCashoutRequest;
    }

    @Override
    public List<CashoutRequest> findAllByDateStatus(Timestamp beginDate, Timestamp endDate, byte status) throws Exception {
        List<CashoutRequest> listCashoutRequest = new ArrayList<>();
        String sql = "SELECT * FROM cashout_request WHERE (reques_time BETWEEN ? AND ?) AND status = ?  ORDER BY id DESC";
        try (final Connection conn = CommonDbPool.getInstance().getVinplayDataSource().getConnection();
             final PreparedStatement stmt = conn.prepareStatement(sql);
        ) {
            stmt.setTimestamp(1, beginDate);
            stmt.setTimestamp(2, endDate);
            stmt.setByte(3, status);
            final ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                listCashoutRequest.add(bindResultSet(rs));
            }
            rs.close();
        }
        return listCashoutRequest;
    }

    @Override
    public List<CashoutRequest> findAllByUserStatus(String nickName, byte status) throws Exception {
        List<CashoutRequest> listCashoutRequest = new ArrayList<>();
        String sql = "SELECT * FROM cashout_request WHERE user_name = ? AND status = ?  ORDER BY id DESC";
        try (final Connection conn = CommonDbPool.getInstance().getVinplayDataSource().getConnection();
             final PreparedStatement stmt = conn.prepareStatement(sql);
        ) {
            stmt.setString(1, nickName);
            stmt.setByte(2, status);
            final ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                listCashoutRequest.add(bindResultSet(rs));
            }
            rs.close();
        }
        return listCashoutRequest;
    }

    @Override
    public CashoutRequest getRandomPendingBankRequest()  throws Exception{
        CashoutRequest cashoutRequest = null;
        String sql = "SELECT * FROM `cashout_request` \n" +
                "WHERE item_id in (28,29,31,32,33,34,39,40,41) \n" +
                "AND status = 5 \n" +
                "ORDER BY rand() LIMIT 1";
        try (final Connection conn = CommonDbPool.getInstance().getVinplayDataSource().getConnection();
             final PreparedStatement stmt = conn.prepareStatement(sql);
        ) {
            final ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                cashoutRequest = bindResultSet(rs);
            }
            rs.close();
        }
        return cashoutRequest;
    }

    @Override
    public void updateClient(String nickName, String client) throws Exception {
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement("update cashout_request set client = ? where user_name=?");
        ) {
            stm.setString(1, client);
            stm.setString(2, nickName);
            stm.executeUpdate();
        }
    }

    @Override
    public void updatePlatform(String nickName, String pf) throws Exception {
        try (Connection conn = ConnectionPool.getInstance().getConnection("mysqlpoolname");
             PreparedStatement stm = conn.prepareStatement("update cashout_request set platform = ? where user_name=?");
        ) {
            stm.setString(1, pf);
            stm.setString(2, nickName);
            stm.executeUpdate();
        }
    }

    private CashoutRequest bindResultSet(ResultSet rs) throws SQLException {
        CashoutRequest cr = new CashoutRequest();
        cr.setId(rs.getInt("id"));
        cr.setUserId(rs.getInt("user_id"));
        cr.setStatus(rs.getByte("status"));
        cr.setRequesTime(rs.getTimestamp("reques_time"));
        cr.setProcessTime(rs.getTimestamp("process_time"));
        cr.setNote(rs.getString("note"));
        cr.setExtraData(rs.getString("extra_data"));
        cr.setItemId(rs.getInt("item_id"));
        cr.setQuantity(rs.getInt("quantity"));
        cr.setUserName(rs.getString("user_name"));
        cr.setUserMobile(rs.getString("user_mobile"));
        cr.setActionNote(rs.getString("action_note"));
        cr.setClient(rs.getString("client"));
        cr.setPlatform(rs.getString("platform"));
        return cr;
    }
}
