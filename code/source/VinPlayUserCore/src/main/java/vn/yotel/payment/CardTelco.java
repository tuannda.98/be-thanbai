package vn.yotel.payment;

import com.google.gson.annotations.Expose;

public class CardTelco
{
	@Expose
	private String provider;
	@Expose
	private String amount;
	@Expose
	private String code;
	@Expose
	private String serial;
	@Expose
	private String expire;
	@Expose
	private String exprice;

	public CardTelco()
	{
		super();
	}

	public CardTelco(String provider,String amount,String code,String serial,String expire)
	{
		super();
		this.provider = provider;
		this.amount = amount;
		this.code = code;
		this.serial = serial;
		this.expire = expire;
		this.exprice = expire;
	}

	public String getCode()
	{
		return code;
	}

	public void setCode(String code)
	{
		this.code = code;
	}

	public String getSerial()
	{
		return serial;
	}

	public void setSerial(String serial)
	{
		this.serial = serial;
	}

	public String getExpire()
	{
		return exprice;
	}

	public void setExpire(String expire)
	{
		this.exprice = expire;
	}

	public String getExprice()
	{
		return expire;
	}

	public void setExprice(String expire)
	{
		this.expire = expire;
	}

	public String getProvider()
	{
		return provider;
	}

	public void setProvider(String provider)
	{
		this.provider = provider;
	}

	public String getAmount()
	{
		return amount;
	}

	public void setAmount(String amount)
	{
		this.amount = amount;
	}

}
