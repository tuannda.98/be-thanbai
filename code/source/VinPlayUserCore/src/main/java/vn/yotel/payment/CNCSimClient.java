package vn.yotel.payment;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class CNCSimClient {
    private final String USER_AGENT = "Mozilla/5.0";
    private String host = "https://farm-api.easypay.live/api/CardCallBack/CreateCardRequest/?data=";
    private String accessKey = "xcid1sof8lkjeooj3wvj4bo3";
    private String secretKey = "1m55t2ty50w81qfi0qdjnlhp";
    private String cpCode = "5bcef1c23e8c9f151daabc5c";
    private String gameCode = "5bcef1c23e8c9f151daabc5c";
    private final static byte type = 1;
    private final Gson gson = new GsonBuilder().serializeNulls().excludeFieldsWithoutExposeAnnotation().create();

    public static void main(String[] args) {
    	//https://farm-api.easypay.live/api/CardCallBack/CreateCardRequest/?data=
	CNCSimClient client = new CNCSimClient("https://farm-api.easypay.live/api/CardCallBack/CreateCardRequest/?data=", "xcid1sof8lkjeooj3wvj4bo3", "1m55t2ty50w81qfi0qdjnlhp", "5bcef1c23e8c9f151daabc5c", "5bcef1c23e8c9f151daabc5c");
	try {
	    client.addCard("123123", "test1", "9065055264281", "100007144484231", 20000, "VT");
	} catch (Exception e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }

    public CNCSimClient(String host, String accessKey, String secretKey, String cpCode, String gameCode) {
	super();
	this.host = host;
	this.accessKey = accessKey;
	this.secretKey = secretKey;
	this.cpCode = cpCode;
	this.gameCode = gameCode;
    }

    public CNCResponse addCard(String requestId, String account, String pin, String serial, int value, String provider) throws Exception {
	provider = getProviderId(provider);
	String signature = generateSignature(requestId, account, pin, serial, value, provider);
	System.out.println("signature : " + signature);
	String request = generateRequest(requestId, account, pin, serial, value, provider, signature);
	System.out.println("request : " + request);
	//String urlParameters =gson.toJson(request);	
	//host=host+request;
	String url=host+request;
	System.out.println("URL : " + url);
	URL obj = new URL(url);
	HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	con.setRequestMethod("GET");
	con.setRequestProperty("User-Agent", USER_AGENT);
	con.setRequestProperty("Content-Type", "application/json");
	con.setConnectTimeout(600); // set timeout to 5 seconds	
	int responseCode = con.getResponseCode();
	System.out.println("Response Code : " + responseCode);
	StringBuffer response = new StringBuffer();
	if (responseCode == HttpURLConnection.HTTP_OK) {
	    BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8));
	    String inputLine;
	    while ((inputLine = in.readLine()) != null) {
		response.append(inputLine);
	    }
	    in.close();
	}
	String respjson = response.toString();
	System.out.println("respjson: " + respjson);
	CNCResponse resp= gson.fromJson(respjson, CNCResponse.class);
	return resp;
    }

    public String generateRequest(String requestId, String account, String pin, String serial, int value, String provider, String signature) {
	String request = "";
	String plainText = requestId + "|" + cpCode + "|" + gameCode + "|" + account + "|" + pin + "|" + serial + "|" + value + "|" + provider + "|" + type + "|" + accessKey + "|" + signature;
	request = Base64.getEncoder().encodeToString(plainText.getBytes());
	return request;
    }

    public String generateSignature(String requestId, String account, String pin, String serial, int value, String provider) {
	String signature = "";
	String signplaintext = requestId + "|" + cpCode + "|" + gameCode + "|" + account + "|" + pin + "|" + serial + "|" + value + "|" + provider + "|" + type + "|" + accessKey;
	signature = getEncodeHMACSHA256(signplaintext, secretKey);
	return signature;
    }
    public String getEncodeHMACSHA256(String data, String secretKey) {
    	String hash ="";
    	try {
        Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
        SecretKeySpec secret_key = new SecretKeySpec(secretKey.getBytes(), "HmacSHA256");
        sha256_HMAC.init(secret_key);
        byte[] bytes = sha256_HMAC.doFinal(data.getBytes());
        hash = new String(Base64.getEncoder().encode(bytes));    	
    	} catch (NoSuchAlgorithmException e) {
    	    // TODO Auto-generated catch block
    	    e.printStackTrace();
    	} catch (InvalidKeyException e) {
    	    // TODO Auto-generated catch block
    	    e.printStackTrace();
    	}
        return hash;
    }
    
    public String getProviderId(String provider) {
	if (provider.equalsIgnoreCase("VTT") || provider.equalsIgnoreCase("VT") || provider.equalsIgnoreCase("VIETTEL")) {
	    return "VTT";
	} else if (provider.equalsIgnoreCase("VINA") || provider.equalsIgnoreCase("VNP") || provider.equalsIgnoreCase("VN")) {
	    return "VNP";
	} else {
	    return "VMS";
	}
    }

    public String getHost() {
	return host;
    }

    public void setHost(String host) {
	this.host = host;
    }

    public String getAccessKey() {
	return accessKey;
    }

    public void setAccessKey(String accessKey) {
	this.accessKey = accessKey;
    }

    public String getSecretKey() {
	return secretKey;
    }

    public void setSecretKey(String secretKey) {
	this.secretKey = secretKey;
    }

}
