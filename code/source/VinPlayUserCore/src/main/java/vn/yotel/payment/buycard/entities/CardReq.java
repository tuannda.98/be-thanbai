package vn.yotel.payment.buycard.entities;

/**
 * reference_no_ls: list của transaction id tương ứng <br>
 * size tương ứng với quantity <br>
 * 
 * @author luongvv
 */
public class CardReq{
	
	public String partner_id;
	public String partner_reference;
	public int quantity;
	public String telco;
	public long denomination;
	public String signature;
}
