package vn.yotel.yoker.dao.impl;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import vn.yotel.yoker.dao.CashinItemDao;
import vn.yotel.yoker.domain.CashinItem;

import java.util.List;

public class CashinItemDaoImpl extends AbstractGenericDao<CashinItem>  implements CashinItemDao {

    @Override
    public List<CashinItem> findByProvider(String providerCode) {
        List<CashinItem> re = null;
        Transaction tx = null;
        try (Session session = openSession()){
            tx = session.beginTransaction();
            String hql = "FROM CashinItem C WHERE C.provider = :provider";
            Query query = session.createQuery(hql);
            query.setParameter("provider",providerCode);
            re = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null)
                tx.rollback();
            e.printStackTrace();
        }
        return re;
    }
}
