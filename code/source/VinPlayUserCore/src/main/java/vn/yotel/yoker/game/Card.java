package vn.yotel.yoker.game;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Card implements Serializable, Cloneable {

    private static final long serialVersionUID = 1L;

    @Expose
    private byte card;
    @Expose
    private byte face;

    public Card() {
    }

    public Card(byte card, byte face) {
        super();
        this.card = card;
        this.face = face;
    }

    public Card(int card, int face) {
        super();
        this.card = (byte) card;
        this.face = (byte) face;
    }

    public static void sortCards(List<Card> cards) {
        int size = cards.size();
        for (int i = 0; i < size - 1; i++)
            for (int j = i + 1; j < size; j++)
                if (cards.get(i).compareBaCay(cards.get(j))) Collections.swap(cards, i, j);
    }

    public static void main(String[] args) {
        System.out.println("xx " + new Card(Suit.ACE, Face.HEART).compareBaCay(new Card(Suit.QUEEN, Face.HEART)));
        List<Card> cardsA = new ArrayList<>();
        cardsA.add(new Card(Suit.SIX, Face.CLUBS));
        cardsA.add(new Card(Suit.SEVEN, Face.CLUBS));
        cardsA.add(new Card(Suit.SEVEN, Face.SPADES));
        sortCards(cardsA);
        System.out.println("aa " + cardsA);
        List<Card> cardsB = new ArrayList<>();
        cardsB.add(new Card(Suit.FOUR, Face.DIAMOND));
        cardsB.add(new Card(Suit.FIVE, Face.SPADES));
        cardsB.add(new Card(Suit.ACE, Face.DIAMOND));
        sortCards(cardsB);
        System.out.println("bb " + cardsB);

        System.out.println("vv " + cardsA.get(0).compareBaCay(cardsB.get(0)));
    }

    public byte getCard() {
        return card;
    }

    public void setCard(byte card) {
        this.card = card;
    }

    public byte getFace() {
        return face;
    }

    public void setFace(byte face) {
        this.face = face;
    }

    public String toStringFace() {
        switch (face) {
            case Face.CLUBS:
                return "B";
            case Face.SPADES:
                return "T";
            case Face.DIAMOND:
                return "R";
            case Face.HEART:
                return "C";
        }
        return "";
    }

    public String toStringCard() {
        switch (card) {
            case Suit.ACE:
                return "A";
            case Suit.JACK:
                return "J";
            case Suit.QUEEN:
                return "Q";
            case Suit.KING:
                return "K";
            default:
                return "" + card;
        }
    }

    @Override
    public boolean equals(Object card2) {
        if (!(card2 instanceof Card)) return false;
        Card objCard2 = (Card) card2;
        return (this.card == objCard2.getCard() && this.face == objCard2.getFace());
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + this.card;
        hash = 89 * hash + this.face;
        return hash;
    }

    public boolean equalsCard(Object card2) {
        if (!(card2 instanceof Card)) return false;
        Card objCard2 = (Card) card2;
        return this.card == objCard2.getCard();
    }

    public boolean isPreCard(Card card2) {
        if (this.card == Suit.KING && card2.getCard() == Suit.ACE) return true;
        return (card2.getCard() - this.card) == 1;
    }

    public boolean isThreeClubs() {
        return card == Suit.THREE && face == Face.CLUBS;
    }

    public boolean isRed() {
        return face == Face.HEART || face == Face.DIAMOND;
    }

    public boolean isNextCard(Card card2) {
        if (this.card == Suit.ACE && card2.getCard() == Suit.KING) return true;
        return (this.card - card2.getCard()) == 1;
    }

    public boolean compareSam(Object card2) {
        if (!(card2 instanceof Card)) return false;
        Card objCard2 = (Card) card2;
        int value1 = this.card;
        int value2 = objCard2.getCard();

        if (value1 > value2) switch (value2) {
            case 1:
                return value1 == 2;
            case 2:
                return false;
            default:
                return true;
        }

        else if (value1 < value2) switch (value1) {
            case 1:
                return value2 != 2;
            case 2:
                return true;
            default:
                return false;
        }
        return false;
    }

    public int compare(Object card2) {
        if (!(card2 instanceof Card)) return 0;

        Card objCard2 = (Card) card2;
        int value1 = this.card;
        int value2 = objCard2.getCard();

        if (value1 > value2) switch (value2) {
            case 1:
                if (value1 == 2) return 1;
                else return -1;
            case 2:
                return -1;
            default:
                return 1;
        }

        else if (value1 < value2) switch (value1) {
            case 1:
                if (value2 == 2) return -1;
                else return 1;
            case 2:
                return 1;
            default:
                return -1;
        }

        else {
            int face1 = this.face;
            int face2 = objCard2.getFace();
            if (face1 > face2) return 1;
            else if (face1 < face2) return -1;
            else return 0;
        }
    }

    // ----------------- Compare Value -----------------
    public boolean compareCard(Object card2) {
        if (!(card2 instanceof Card)) return false;
        else {
            Card objCard2 = (Card) card2;
            return this.card == objCard2.getCard();
        }
    }

    // ---------------- Compare Face -------------------
    public boolean compareFace(Object card2) {
        if (!(card2 instanceof Card)) return false;
        else {
            Card objCard2 = (Card) card2;
            return this.face == objCard2.getFace();
        }
    }

    public boolean compareSamFace(Object card2) {
        if (!(card2 instanceof Card)) return false;
        else {
            Card objCard2 = (Card) card2;
            int val1;
            int val2;
            if (Face.CLUBS == this.getFace() || Face.SPADES == this.getFace()) val1 = 1;
            else val1 = 2;
            if (Face.CLUBS == objCard2.getFace() || Face.SPADES == objCard2.getFace()) val2 = 1;
            else val2 = 2;
            return (val1 == val2);
        }
    }

    public boolean comparePhom(Object card2) {
        if (!(card2 instanceof Card)) return false;
        else {
            Card objCard2 = (Card) card2;
            return (this.card) >= objCard2.getCard();
        }
    }

    public boolean equalsPhom(Object card2) {
        if (card2 == null || !(card2 instanceof Card)) return false;
        else {
            Card objCard2 = (Card) card2;
            return this.card == objCard2.getCard() && this.face == objCard2.getFace();
        }
    }

    /**
     * Thứ tự so sánh: Rô, cơ, nhép, bích
     *
     * @param card2
     * @return
     */
    public boolean compareBaCay(Object card2) {
        if (!(card2 instanceof Card)) return false;
        Card objCard2 = (Card) card2;
        if (objCard2.face == this.face && this.face == Face.DIAMOND) {
            if (this.card == Suit.ACE) return true;
        }
        if (objCard2.face == this.face) return this.card > objCard2.card;

        if (this.face == Face.DIAMOND) return true;
        if (objCard2.face == Face.DIAMOND) return false;
        return this.face > objCard2.face;
    }

    /**
     * Thứ tự so sánh: Rô, cơ, nhép, bích
     *
     * @param card2
     * @return
     */
    public int compareFaceLieng(Object card2) {
        if (!(card2 instanceof Card)) return 0;
        Card objCard2 = (Card) card2;
        if (this.face == Face.DIAMOND) return 1;
        else if (objCard2.getFace() == Face.DIAMOND) return -1;
        else if (this.face == Face.HEART) return 1;
        else if (objCard2.getFace() == Face.HEART) return -1;
        else if (this.face == Face.CLUBS) return 1;
        else return -1;
    }

    public int compareMauBinh(Object card2) {
        if (!(card2 instanceof Card)) return 0;
        Card objCard2 = (Card) card2;
        if (this.card == objCard2.getCard()) return 0;
        else if (this.card == Suit.ACE) return 1;
        else if (objCard2.getCard() == Suit.ACE) return -1;
        else {
            int value1 = this.card;
            int value2 = objCard2.getCard();
            if (value1 > value2) return 1;
            else return -1;
        }
    }

    public int compareMauBinhFace(Object card2) {
        if (card2 == null || !(card2 instanceof Card)) return 0;
        Card objCard2 = (Card) card2;
        if (this.face == objCard2.getFace()) return 0;
        else {
            int value1 = this.face;
            int value2 = objCard2.getFace();
            if (value1 > value2) return 1;
            else return -1;
        }
    }

    public int compareSanh(Object card2) {
        if (!(card2 instanceof Card)) return 0;
        Card objCard2 = (Card) card2;
        if (this.card == objCard2.getCard()) return 0;
        else if (this.card == Suit.ACE) return -1;
        else if (objCard2.getCard() == Suit.ACE) return 1;
        else {
            int value1 = this.card;
            int value2 = objCard2.getCard();
            if (value1 > value2) return 1;
            else if (value1 < value2) return -1;
            else return 0;
        }
    }

    public int compareLieng(Object card2) {
        if (!(card2 instanceof Card)) return 0;
        Card objCard2 = (Card) card2;
        if (this.card == objCard2.getCard()) return compareFaceLieng(objCard2);
        else if (this.card == Suit.ACE) return 1;
        else if (objCard2.getCard() == Suit.ACE) return -1;
        else {
            int value1 = this.card;
            int value2 = objCard2.getCard();
            if (value1 > value2) return 1;
            else return -1;
        }
    }

    public int compareCardLieng(Object card2) {
        if (card2 == null || !(card2 instanceof Card)) return 0;
        Card objCard2 = (Card) card2;
        if (this.card == objCard2.getCard()) return 0;
        else if (this.card == Suit.ACE) return 1;
        else if (objCard2.getCard() == Suit.ACE) return -1;
        else {
            int value1 = this.card;
            int value2 = objCard2.getCard();
            if (value1 > value2) return 1;
            else if (value1 < value2) return -1;
            else return 0;
        }
    }

    public int compareHighLow(Card card2) {
        // -1 là thấp hơn, 0 là bằng, 1 là cao hơn
        if (this.card == card2.getCard() && this.face == card2.getFace()) return 0;
        if (this.card == card2.getCard()) return this.face > card2.getFace() ? 1 : -1;
        // Trường hợp có 1 con là A
        if (this.card == Suit.ACE || card2.getCard() == Suit.ACE)
            // Nếu con bài này là A thì nó sẽ lớn hơn, còn ko thì bé hơn
            return this.card == 1 ? 1 : -1;
        return this.card > card2.getCard() ? 1 : -1;
    }

    public float getHighLowHighMultipleRate(float afterFee) throws Exception {
        if (afterFee > 1) throw new Exception("Invalid rate");

        if (card == Suit.TWO || card == Suit.ACE) return 0;
        float winRate = (float) (card - 2) / (14 - card);
        return winRate * afterFee;
    }

    public float getHighLowLowMultipleRate(float afterFee) throws Exception {
        if (afterFee > 1) throw new Exception("Invalid rate");

        if (card == Suit.TWO || card == Suit.ACE) return 0;
        float winRate = (float) (14 - card) / (card - 2);
        return winRate * afterFee;
    }

    @Override
    public Card clone() {
        try {
            return (Card) super.clone();
        } catch (CloneNotSupportedException e) {
            return new Card(this.card, this.face);
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(toStringCard());
        builder.append(toStringFace());
        return builder.toString();
    }

    public String toHumanRead() {
        StringBuilder builder = new StringBuilder();
        switch (card) {
            case Suit.ACE:
                builder.append("A");
                break;
            case Suit.TWO:
            case Suit.THREE:
            case Suit.FOUR:
            case Suit.FIVE:
            case Suit.SIX:
            case Suit.SEVEN:
            case Suit.EIGHT:
            case Suit.NINE:
            case Suit.TEN:
                builder.append(card);
                break;
            case Suit.JACK:
                builder.append("J");
                break;
            case Suit.QUEEN:
                builder.append("Q");
                break;
            case Suit.KING:
                builder.append("K");
                break;
            default:
                break;
        }
        switch (face) {
            case Face.CLUBS:
                builder.append("b");
                break;
            case Face.SPADES:
                builder.append("t");
                break;
            case Face.DIAMOND:
                builder.append("r");
                break;
            case Face.HEART:
                builder.append("c");
                break;
            default:
                break;
        }
        return builder.toString();
    }
}
