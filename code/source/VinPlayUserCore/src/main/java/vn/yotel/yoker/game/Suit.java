package vn.yotel.yoker.game;

public class Suit
{
	public static final byte ACE = 1;
	public static final byte TWO = 2;
	public static final byte THREE = 3;
	public static final byte FOUR = 4;
	public static final byte FIVE = 5;
	public static final byte SIX = 6;
	public static final byte SEVEN = 7;
	public static final byte EIGHT = 8;
	public static final byte NINE = 9;
	public static final byte TEN = 10;
	public static final byte JACK = 11;
	public static final byte QUEEN = 12;
	public static final byte KING = 13;
}
