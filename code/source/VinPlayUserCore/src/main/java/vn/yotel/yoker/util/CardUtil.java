package vn.yotel.yoker.util;

import vn.yotel.yoker.game.Card;
import vn.yotel.yoker.game.Face;
import vn.yotel.yoker.game.Suit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class CardUtil {
    private final static byte[] FACES = new byte[]{4, 3, 2, 1};
    private final static byte[] BACAY_NUMBERS = new byte[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
    private final static byte[] NUMBERS = new byte[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
    private final static byte[] PICTURE_NUMBERS = new byte[]{11, 12, 13};

    static Random random = new Random();

    public static void main(String[] args) throws Exception {
        {
            List<Card> allCards = generateCards();
            System.out.println(allCards);
            List<Card> servedCards = new ArrayList<>();
//			for(int i = 0;i < 20;i++)
//			{
//				servedCards.add(allCards.remove(0));
//			}
            servedCards.add(new Card(Suit.ACE, Face.CLUBS));
            servedCards.add(new Card(Suit.ACE, Face.DIAMOND));
            servedCards.add(new Card(Suit.EIGHT, Face.HEART));
            servedCards.add(new Card(Suit.THREE, Face.CLUBS));
            servedCards.add(new Card(Suit.ACE, Face.SPADES));
//			System.out.println("bestSamco " + getBestSamCo(servedCards));
//			sortCardsHighLowDesc(servedCards);
            System.out.println("servedCards " + countCard(servedCards, Suit.ACE));
//
//			Card card = CardUtil.getRandomCard();
//
//			System.out.println("card " + card);
//			System.out.println("card " + card.getHighLowHighMultipleRate((float)0.95));
//			System.out.println("card " + card.getHighLowLowMultipleRate((float)0.95));
//
//			card = CardUtil.getRandomCard();
//			card.setCard(Suit.KING);
//
//			System.out.println("card " + card);
//			System.out.println("card " + card.getHighLowHighMultipleRate((float)0.95));
//			System.out.println("card " + card.getHighLowLowMultipleRate((float)0.95));
        }
//		List<Card> cards = randomShuffle(generateCards(),1);
//		List<Card> allCards1 = getLastCards(cards,3);
//		List<Card> allCards2 = getLastCards(cards,3);
//		List<Card> allCards3 = getLastCards(cards,3);
//		List<List<Card>> servedCards = new ArrayList<>();
//		servedCards.add(allCards1);
//		servedCards.add(allCards2);
//		servedCards.add(allCards3);
//		System.out.println("_ " + humanReadCardss(servedCards));
    }

    public static List<Card> randomShuffle(final List<Card> cards, int numOfShuffler) {
        List<Card> result = new ArrayList<>();
        while (cards.size() > 0) {
            int position = random.nextInt(cards.size());
            Card card = cards.remove(position);
            result.add(card);
        }
        Collections.shuffle(result);
        numOfShuffler--;
        if (numOfShuffler == 0) return result;
        else return CardUtil.randomShuffle(result, numOfShuffler);
    }

    public static List<Card> generateCards() {
        List<Card> cards = new ArrayList<>();
        for (int i = 1; i < 14; i++)
            for (byte face : FACES) {
                Card card = new Card((byte) i, face);
                cards.add(card);
            }
        // Random shuffle
        Collections.shuffle(cards);
        return cards;
    }

    public static List<Card> generateCards(int size) {
        List<Card> cards = new ArrayList<>();
        for (int i = 1; i < size; i++)
            for (byte face : FACES) {
                Card card = new Card((byte) i, face);
                cards.add(card);
            }
        // Random shuffle
        Collections.shuffle(cards);
        return cards;
    }

    public static List<Card> getLastCards(final List<Card> cards, int size) {
        List<Card> gotCards = new ArrayList<>();
        for (int i = 0; i < size; i++)
            if (!cards.isEmpty()) gotCards.add(cards.remove(cards.size() - 1));
            else break;
        return gotCards;
    }

    public static Card getRandomCard(final List<Card> cards) {
        int position = random.nextInt(cards.size());
        return cards.get(position);
    }

    public static List<Card> getRandomHighScore() {//3cay score >=8
        List<Card> cards = new ArrayList<>();
        byte[] iCards = getHighScoreCards();
        byte[] iFaces = getRandomFaces();
        for (int i = 0; i < iFaces.length; i++) {
            Card nCard = new Card(iCards[i], iFaces[i]);
            cards.add(nCard);
        }
        return cards;
    }

    public static List<Card> getLiengHighScore() {//3cay score >=8
        List<Card> cards = new ArrayList<>();
        byte[] iCards = getLiengHighScoreCards();
        byte[] iFaces = getRandomFaces();
        for (int i = 0; i < iFaces.length; i++) {
            Card nCard = new Card(iCards[i], iFaces[i]);
            cards.add(nCard);
        }
        return cards;
    }

    public static List<Card> getRandomLieng() {//Lieng
        List<Card> myCards = new ArrayList<>();
        byte[] iCards = getLiengCards();
        byte[] iFaces = getRandomFaces();
        for (int i = 0; i < iFaces.length; i++) {
            Card nCard = new Card(iCards[i], iFaces[i]);
            myCards.add(nCard);
        }
        return myCards;
    }

    public static List<Card> getRandomAnh() {//Anh
        List<Card> cards = new ArrayList<>();
        byte[] iCards = getAnhCards();
        byte[] iFaces = getRandomFaces();
        for (int i = 0; i < iFaces.length; i++) {
            Card nCard = new Card(iCards[i], iFaces[i]);
            cards.add(nCard);
        }
        return cards;
    }

    public static List<Card> getRandomSap() {
        List<Card> cards = new ArrayList<>();
        int position = random.nextInt(BACAY_NUMBERS.length);
        byte card = BACAY_NUMBERS[position];
        byte[] iFaces = getRandomFaces();
        for (int i = 0; i < iFaces.length; i++) {
            Card nCard = new Card(card, iFaces[i]);
            cards.add(nCard);
        }
        return cards;
    }

    public static byte[] getLiengCards() {
        byte[] myCards = new byte[3];
        Random rand = new Random();
        byte card1 = 1, card2 = 2, card3 = 3;
        int index1 = rand.nextInt(NUMBERS.length);
        card1 = NUMBERS[index1];
        if (card1 > 11) {
            card2 = (byte) (card1 - 1);
            card3 = (byte) (card2 - 1);
        } else {
            card2 = (byte) (card1 + 1);
            card3 = (byte) (card2 + 1);
        }
        //
        myCards[0] = card1;
        myCards[1] = card2;
        myCards[2] = card3;
        return myCards;
    }

    public static byte[] getAnhCards() {
        byte[] myCards = new byte[3];
        Random rand = new Random();
        byte card1 = 1, card2 = 2, card3 = 3;
        int index1 = rand.nextInt(PICTURE_NUMBERS.length);
        int index2 = rand.nextInt(PICTURE_NUMBERS.length);
        int index3 = rand.nextInt(PICTURE_NUMBERS.length);
        card1 = PICTURE_NUMBERS[index1];
        card2 = PICTURE_NUMBERS[index2];
        card3 = PICTURE_NUMBERS[index3];
        //
        myCards[0] = card1;
        myCards[1] = card2;
        myCards[2] = card3;
        return myCards;
    }

    public static byte[] getLiengHighScoreCards() {
        byte[] myCards = new byte[3];
        Random rand = new Random();
        int score = -1;
        byte card1 = 1, card2 = 2, card3 = 7;
        while (score < 8) {
            int index1 = rand.nextInt(BACAY_NUMBERS.length);
            int index2 = rand.nextInt(BACAY_NUMBERS.length);
            int index3 = rand.nextInt(BACAY_NUMBERS.length);
            card1 = BACAY_NUMBERS[index1];
            card2 = BACAY_NUMBERS[index2];
            card3 = BACAY_NUMBERS[index3];
            score = (card1 + card2 + card3) % 10;
        }
        myCards[0] = card1;
        myCards[1] = card2;
        myCards[2] = card3;
        return myCards;
    }

    public static byte[] getHighScoreCards() {
        byte[] myCards = new byte[3];
        Random rand = new Random();
        int score = -1;
        byte card1 = 1, card2 = 2, card3 = 7;
        while (score != 0 && score < 8) {
            int index1 = rand.nextInt(BACAY_NUMBERS.length);
            int index2 = rand.nextInt(BACAY_NUMBERS.length);
            int index3 = rand.nextInt(BACAY_NUMBERS.length);
            card1 = BACAY_NUMBERS[index1];
            card2 = BACAY_NUMBERS[index2];
            card3 = BACAY_NUMBERS[index3];
            score = (card1 + card2 + card3) % 10;
        }
        myCards[0] = card1;
        myCards[1] = card2;
        myCards[2] = card3;
        return myCards;
    }

    public static byte[] getRandomFaces() {
        byte[] myFaces = new byte[3];
        List<Byte> aList = new ArrayList<>();
        for (int i = 0; i < FACES.length; i++)
            aList.add(FACES[i]);
        Collections.shuffle(aList);
        for (int i = 0; i < myFaces.length; i++)
            myFaces[i] = aList.get(i);
        return myFaces;
    }

    public static boolean sameCard(List<Card> cards) {
        for (int i = 0; i < cards.size() - 1; i++)
            if (cards.get(i).getCard() != cards.get(i + 1).getCard()) return false;
        return true;
    }

    // Check neu co Sam Co
    public static List<Card> getBestSamCo(List<Card> cards) {
        if (cards == null || cards.size() < 3) return null;
        List<Card> testCards = new ArrayList<>(cards);
        sortCardsHighLowDesc(testCards);
        int mark = -1;
        for (int i = 0; i < testCards.size() - 2; i++)
            if (testCards.get(i).equalsCard(testCards.get(i + 1)) &&
                testCards.get(i + 1).equalsCard(testCards.get(i + 2)))
            {
                mark = i;
                break;
            }
        if (mark >= 0) {
            List<Card> samco = new ArrayList<>();
            samco.add(testCards.get(mark));
            samco.add(testCards.get(mark + 1));
            samco.add(testCards.get(mark + 2));
            return samco;
        }
        return null;
    }

    public static int countCard(List<Card> listCard, byte suit) {
        int aceCount = 0;
        for (Card card : listCard)
            if (card.getCard() == suit) aceCount++;
        return aceCount;
    }

    public static byte getRandomFace() {
        Random rand = new Random();
        int index1 = rand.nextInt(FACES.length);
        return FACES[index1];
    }

    public static byte getRandomNum() {
        return (byte) Util.getRandom(1, 14);
    }

    public static Card getRandomCard() {
        return new Card(getRandomNum(), getRandomFace());
    }

    public static int getKoinCard(Card card) {
        int koid = 0;
        int icard = 0;
        int iface = 0;
        if (card.getCard() == Suit.ACE) icard = 15;
        else icard = card.getCard();
        if (card.getFace() == Face.HEART) iface = 2000;
        else if (card.getFace() == Face.DIAMOND) iface = 1500;
        else if (card.getFace() == Face.CLUBS) iface = 1000;
        else iface = 500;
        koid = icard * iface;
        return koid;
    }

    // --- Sorting cards with parameters ---------
    public static void sortCards(List<Card> cards) {
        int size = cards.size();
        for (int i = 0; i < size - 1; i++)
            for (int j = i + 1; j < size; j++)
                if (cards.get(i).compare(cards.get(j)) == 1) Collections.swap(cards, i, j);
    }

    // Sap xep bai theo thu tu giam dan
    public static void sortCardsHighLowDesc(List<Card> cards) {
        int size = cards.size();
        for (int i = 0; i < size - 1; i++)
            for (int j = i + 1; j < size; j++)
                if (cards.get(i).compareMauBinh(cards.get(j)) == -1) Collections.swap(cards, i, j);
    }

    public static String humanReadCardss(List<List<Card>> cards) {
        if (cards.isEmpty()) return "";
        String humanRead = "";
        ArrayList<List<Card>> tmpCards = (ArrayList) ((ArrayList) cards).clone();

        boolean first = true;
        for (List<Card> lst : tmpCards) {
            if (!first) humanRead += ",";
            else first = false;
            humanRead += humanReadCards(lst);
        }
        return humanRead;
    }

    public static String humanReadCards(List<Card> cards) {
        if (cards.isEmpty()) return "";
        String humanRead = "";
        ArrayList<Card> tmpCards = (ArrayList) ((ArrayList) cards).clone();
        CardUtil.sortCards(tmpCards);
        for (Card card : tmpCards)
            humanRead += card.toHumanRead();
        return humanRead;
    }
}
