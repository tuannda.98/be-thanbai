package vn.yotel.yoker.service;

import vn.yotel.yoker.domain.RequestIap;

public interface RequestIapBo {

    boolean existTransId(String transId) throws Exception;

    boolean create(RequestIap requestIap) throws Exception;
}
