package vn.yotel.yoker.dao;

import vn.yotel.yoker.domain.CashinItem;

import java.util.List;

public interface CashinItemDao extends GenericDao<CashinItem> {
    List<CashinItem> findByProvider(String providerCode);
}
