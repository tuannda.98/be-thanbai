package vn.yotel.payment;

import com.google.gson.annotations.Expose;

public class CNCResponse {
	@Expose private boolean IsError;
	@Expose private String ErrorCode;
	@Expose private int amount;
	@Expose private String Message;
	
	public CNCResponse() {
		super();
	}

	public boolean isIsError() {
		return IsError;
	}

	public void setIsError(boolean isError) {
		IsError = isError;
	}

	public String getErrorCode() {
		return ErrorCode;
	}

	public void setErrorCode(String errorCode) {
		ErrorCode = errorCode;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	@Override
	public String toString() {
		return "CNCResponse [IsError=" + IsError + ", ErrorCode=" + ErrorCode + ", amount=" + amount + ", Message="
				+ Message + "]";
	}
}
