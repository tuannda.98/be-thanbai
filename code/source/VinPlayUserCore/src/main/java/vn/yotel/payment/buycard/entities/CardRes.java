package vn.yotel.payment.buycard.entities;

/**
 * reference_no_ls: list của transaction id tương ứng, size tương ứng với quantity
 * 
 * @author luongvv
 */
public class CardRes{
	public int code;
	public String message;
	public Data data;

	@Override
	public String toString()
	{
		return "CardRes{" + "code=" + code + ", message=" + message + ", data=" + data + '}';
	}
	
}

