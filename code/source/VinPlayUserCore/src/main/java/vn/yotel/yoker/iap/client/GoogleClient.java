package vn.yotel.yoker.iap.client;

import casio.king365.util.KingUtil;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.*;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class GoogleClient {
    private final Logger LOG = LoggerFactory.getLogger(GoogleClient.class);

    public GoogleClient() {
    }

    /**
     * Path to the private key file (only used for Service Account auth).
     */
    //private final String SRC_RESOURCES_KEY_P12 = "src/main/resources/Yoker-dab4b84e0b5e.p12";
    private final String SRC_RESOURCES_KEY_P12 = "resources/Yoker-dab4b84e0b5e.p12";
    private final String emailAddress = "account-1@yoker-1135.iam.gserviceaccount.com";

    private JsonFactory JSON_FACTORY;
    private HttpTransport httpTransport;

    /**
     * OAuth 2.0 scopes.
     */
    private final List<String> SCOPES = Arrays.asList("https://www.googleapis.com/auth/androidpublisher");

    private GoogleCredential authorize() throws Exception {
        httpTransport = GoogleNetHttpTransport.newTrustedTransport();
        JSON_FACTORY = GsonFactory.getDefaultInstance();
        return new GoogleCredential.Builder().setTransport(httpTransport).setJsonFactory(JSON_FACTORY).setServiceAccountId(emailAddress).setServiceAccountScopes(SCOPES).setServiceAccountPrivateKeyFromP12File(new File(SRC_RESOURCES_KEY_P12)).build();
    }

    public boolean isPurchased(String packageName, String productId, String token) {
        try {
            GoogleCredential credential = authorize();
            HttpRequestFactory requestFactory = httpTransport.createRequestFactory(credential);
            GenericUrl url = new GenericUrl(
                    "https://www.googleapis.com/androidpublisher/v2/applications/" + packageName + "/purchases/products/" + productId + "/tokens/" + token);
            HttpRequest request = requestFactory.buildGetRequest(url);
            HttpResponse response = request.execute();
            if (response.isSuccessStatusCode()) {
                String json = response.parseAsString();
                KingUtil.printLog("Google isPurchased result json: " + json);
                JSONObject jsonObj = new JSONObject(json);
                int purchaseState = jsonObj.getInt("purchaseState");
                if (purchaseState == 0)
                    return true;
            }
        } catch (Exception e) {
            LOG.error(e.toString());
        }
        return false;
    }

    public static void main(String[] args) {
        GoogleClient client = new GoogleClient();
        try {
            client.isPurchased("gamebai.yoker.online.phom.tienlen.miennam.mienphi", "gamebai.yoker.online.phom.tienlen.miennam.mienphi.100k", "jllhkehlkfndibefbnbkheki.AO-J1OzY1_r_biPZnl1UNQ80CxIic9uIX8imvcfaYwsi9v9PYK__beSxxvPU2uy0wsxuJ9HD3Ll_qMgfOkRlm_u-qWQzTEc1TBPsEMtdz13sLBPTlJZus8GDINvubI3FNck5O3aqubv34S7Ve56_REQVXMeQmAOSlLv7hBPwuKyQPpvOa7PNd8SsGL59brziGJRasoORgxkWaFkP5EfJNIubPkoYSqdk3A");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
