package vn.yotel.payment.buycard.api;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

/**
 * gen key:
 * -
 * 
 * type private key: PKCS#8 <br>
 * type public key: PKCS#1 <br>
 * 
 * to sign and verify data:
 * - use RSAPublicKey and X509EncodedKeySpec
 * to build java.security.PublicKey<br>
 * - type: signature and modify: SHA256withRSA <br>
 * 
 * decrypt and encrypt:
 * - use type Cipher: RSA/ECB/PKCS1Padding <br>
 * - encrypt: using private key <br>
 * - decrypt: using public key
 * 
 * @author nemo <br>
 * 
 */
public class RSAUtil
{
	public static PublicKey getPublicKey(String base64PublicKey)
	{
		PublicKey publicKey = null;
		try
		{
			X509EncodedKeySpec keySpec = new X509EncodedKeySpec(Base64.getDecoder().decode(
					base64PublicKey.getBytes()));
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			publicKey = keyFactory.generatePublic(keySpec);
			return publicKey;
		}
		catch(NoSuchAlgorithmException | InvalidKeySpecException e)
		{
			e.printStackTrace();
		}
		return publicKey;
	}

	public static PrivateKey getPrivateKey(String base64PrivateKey)
	{
		PrivateKey privateKey = null;
		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(
				base64PrivateKey.getBytes()));
		KeyFactory keyFactory = null;
		try
		{
			keyFactory = KeyFactory.getInstance("RSA");
		}
		catch(NoSuchAlgorithmException e)
		{
			e.printStackTrace();
		}
		try
		{
			privateKey = keyFactory.generatePrivate(keySpec);
		}
		catch(InvalidKeySpecException e)
		{
			e.printStackTrace();
		}
		return privateKey;
	}

	public static String decrypt(byte[] data, PrivateKey privateKey) throws NoSuchPaddingException,
			NoSuchAlgorithmException, InvalidKeyException, BadPaddingException,
			IllegalBlockSizeException
	{
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.DECRYPT_MODE, privateKey);
		return new String(cipher.doFinal(data));
	}

	public static String decrypt(String data, String base64PrivateKey)
			throws IllegalBlockSizeException,
			InvalidKeyException, BadPaddingException, NoSuchAlgorithmException,
			NoSuchPaddingException
	{
		return decrypt(Base64.getDecoder().decode(data.getBytes()), getPrivateKey(
				base64PrivateKey));
	}

	public static String sign(String privateKey, String message) throws NoSuchAlgorithmException,
			InvalidKeyException,
			SignatureException, UnsupportedEncodingException, InvalidKeySpecException
	{
		String privateKeyStr1 = privateKey.replace("\n", "");
		byte[] privateKeyBytes = Base64.getDecoder().decode(privateKeyStr1);
		PrivateKey priKey = KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(
				privateKeyBytes));
		Signature sign = Signature.getInstance("SHA256withRSA");
		sign.initSign(priKey);
		sign.update(message.getBytes(StandardCharsets.UTF_8));
		return new String(Base64.getEncoder().encode(sign.sign()), StandardCharsets.UTF_8);
	}
}
