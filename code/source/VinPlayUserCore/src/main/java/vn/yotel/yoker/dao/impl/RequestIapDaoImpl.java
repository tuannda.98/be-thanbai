package vn.yotel.yoker.dao.impl;

import casio.king365.core.CommonDbPool;
import com.vinplay.vbee.common.pools.ConnectionPool;
import com.vinplay.vbee.common.utils.VinPlayUtils;
import org.hibernate.Query;
import vn.yotel.yoker.dao.RequestIapDao;
import vn.yotel.yoker.domain.CashoutRequest;
import vn.yotel.yoker.domain.RequestIap;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RequestIapDaoImpl implements RequestIapDao {
    @Override
    public boolean existTransId(String transId) throws Exception {
        List<RequestIap> list = new ArrayList<>();
        String sql = "SELECT * FROM request_iap WHERE transId=:transId";
        try (final Connection conn = CommonDbPool.getInstance().getVinplayDataSource().getConnection();
             final PreparedStatement stmt = conn.prepareStatement(sql);
        ) {
            stmt.setString(1, transId);
            final ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                list.add(bindResultSet(rs));
            }
            rs.close();
        }
        return list != null && !list.isEmpty();
    }

    @Override
    public boolean create(RequestIap requestIap) throws Exception {
        boolean res = false;
        String sql = "INSERT INTO request_iap (user_id, username, provider, money, eq_koin, datetime, process_status, note, message, transId, transDate) " +
                "VALUES (:user_id, :username, :provider, :money, :eq_koin, :datetime, :process_status, :note, :message, :transId, :transDate);";
        try (final Connection conn = CommonDbPool.getInstance().getVinplayDataSource().getConnection();
             final PreparedStatement stmt = conn.prepareStatement(sql);
        ) {
            stmt.setInt(1, requestIap.getUserId());
            stmt.setString(2, requestIap.getUsername());
            stmt.setString(3, requestIap.getProvider());
            stmt.setDouble(4, requestIap.getMoney());
            stmt.setDouble(5, requestIap.getEqKoin());
            stmt.setTimestamp(6, requestIap.getDatetime());
            stmt.setByte(7, requestIap.getProcessStatus());
            stmt.setString(8, requestIap.getNote());
            stmt.setString(9, requestIap.getMessage());
            stmt.setString(10, requestIap.getTransId());
            stmt.setString(11, requestIap.getTransDate());

            if (stmt.executeUpdate() == 1) {
                res = true;
            }
        }
        return res;
    }

    private RequestIap bindResultSet(ResultSet rs) throws SQLException {
        RequestIap cr = new RequestIap();
        cr.setId(rs.getInt("id"));
        cr.setUserId(rs.getInt("user_id"));
        cr.setUsername(rs.getString("username"));
        cr.setProvider(rs.getString("provider"));
        cr.setMoney(rs.getDouble("money"));
        cr.setEqKoin(rs.getDouble("eq_koin"));
        cr.setDatetime(rs.getTimestamp("datetime"));
        cr.setProcessStatus(rs.getByte("process_status"));
        cr.setNote(rs.getString("note"));
        cr.setMessage(rs.getString("message"));
        cr.setTransId(rs.getString("transId"));
        cr.setTransDate(rs.getString("transDate"));
        return cr;
    }
}
