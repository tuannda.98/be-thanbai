package vn.yotel.yoker.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.stream.Collectors;

public class Util {
    private static final Logger LOG = LoggerFactory.getLogger(Util.class);
    static int SALT_BYTE_SIZE = 8;
    static Random random = new SecureRandom();

    public static List<String> getRandomName(String[] arrName, int size) {
        List<String> names = new ArrayList<>();
        Random rand = new Random();
        while (names.size() < size) {
            int index = rand.nextInt(arrName.length);
            String name = arrName[index];
            if (!names.contains(name)) names.add(name);
        }
        return names;
    }

    public static Gson gson() {
        return new GsonBuilder().serializeNulls().excludeFieldsWithoutExposeAnnotation().create();
    }
    // include min, exclude max

    public static int getRandom(int min, int max) {
        synchronized (random) {
            return random.nextInt(max - min) + min;
        }
    }

    public static void main(String[] args) throws Exception {
        String mobile = "84944888888";
        String number = mobile.replaceFirst("(0|84)?(\\d{9,10})", "84$2");
        System.out.println(number);

        for (int i = 0; i < 20; i++) {
            System.out.println(Util.getRandom(0,3));
        }

//        for (int i = 0; i < 10; i++) {
//            System.out.println(Util.randInt(0, 4));
//        }
//
//        for (int i = 0; i < 10; i++) {
//            System.out.println(new Random().nextInt(5));
//        }

        //		String otp = "123111";
//		Map<String,Object> mapResp = new ConcurrentHashMap<>();
//		mapResp.put("OTP",otp);
//		mapResp.put("content","Vui lòng điền \"/start " + otp + "\" với nick OTP Robot. Mã sẽ hết hạn trong 1 phút.");
//
//		OkHttpClient client = new OkHttpClient();// build an instance;
//		okhttp3.Request request = new okhttp3.Request.Builder().url("http://momo.zoan.info/api/get_shipper.jsp").build();
//		RequestBody.create(MediaType.parse("application/json"),
//						   "{\n"
//						   + "  \"username\": \"String {Tên truy cập mà cổng cung cấp}\",\n"
//						   + "  \"request_id\": \"String {ID giao dịch do Game tự sinh}\",\n"
//						   + "  \"request_time\": " + System.currentTimeMillis() + ",\n"
//						   + "  \"signature\": \"String {md5(request_id|request_time|username|partner_key)}\"\n"
//						   + "}");
//
//		ResponseBody responseBody = client.newCall(request).execute().body();
//		String resString = responseBody.string();
//		Gson gson = new Gson();
//		System.out.println("X " + resString + " - " + gson.fromJson(resString,Map.class));

//		Gson gson = new GsonBuilder().serializeNulls().excludeFieldsWithoutExposeAnnotation().create();
//		String jsonString = gson.toJson(mapResp);
//		System.out.println("xx " + jsonString);
////					String jsonString = "OTP=626620, content=Vui lòng điền \"/start 626620\" với nick OTP Robot. Mã sẽ hết hạn trong 1 phút."
////			+ "Vui lòng điền \\\"\\/start 626620\\\" với nick OTP Robot. Mã sẽ hết hạn trong 1 phút.";
//		HashMap<String,Object> result = new ObjectMapper().readValue(jsonString,HashMap.class);
//		System.out.println("X " + result);
//
//		vn.yotel.yoker.util.Util.gson().fromJson("",Map.class);
//		HashFunction hashFunction = Hashing.md5();
//        // Pass input and charset to hashString() method
//        HashCode hash = hashFunction.hashString("3976d0db-b229-4b15-a381-a7f8c9151274"+"|"+"jvD5Iz3sslmFVrMd"+"|"+"1578107630000", StandardCharsets.UTF_8);
//		System.out.println("hash " + hash);
//		User u = new User();
//		UserImpl i = new UserImpl(u,args);
//		i.setTotalRechargeGold(10L + (new Double(100)).longValue());
//		System.out.println("i total rechar gold: " + i.getTotalRechargeGold());
    }

    public static boolean isNumeric(String strNum) {
        if (strNum == null) return false;
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static int randInt(int min, int max) {

        // NOTE: This will (intentionally) not run as written so that folks
        // copy-pasting have to think about how to initialize their
        // Random instance.  Initialization of the Random instance is outside
        // the main scope of the question, but some decent options are to have
        // a field that is initialized once and then re-used as needed or to
        // use ThreadLocalRandom (if using at least Java 1.7).
        //
        // In particular, do NOT do 'Random rand = new Random()' here or you
        // will get not very good / not very random results.
        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

    public static String numberToString(int length, int number) {
        return String.format("%0" + length + "d", number);
    }

    public static String generateRandomSalt() {
        // Generate a random salt
        SecureRandom random = new SecureRandom();
        byte[] salt = new byte[SALT_BYTE_SIZE];
        random.nextBytes(salt);
        return toHex(salt);
    }

    public static Map<String, Object> GeneralRandomUserOtp() {
        Map<String, Object> userCode = new ConcurrentHashMap<>();
        String randomCode = Util.numberToString(6, randInt(1000, 999900));
        userCode.put("code", randomCode);
        Date dt = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        c.add(Calendar.DATE, 1);
        dt = c.getTime();
        userCode.put("expriteDate", dt);
        return userCode;
    }

    public static boolean random(int percent) {
        Random rand = new Random();
        return rand.nextInt(100) <= percent;
    }

    public static byte getRandom() {
        Random rand = new Random();
        int number = rand.nextInt(100);
        if (number < 5) return 2;
        else if (number < 30) return 1;
        else return 0;
    }

    public static byte getRandomBaCay() {
        Random rand = new Random();
        int number = rand.nextInt(100);
        if (number < 5) return 2;
        else if (number < 30) return 1;
        else return 0;
    }

    public static byte getRandomLieng() {
        Random rand = new Random();
        int number = rand.nextInt(100);
        if (number < 5)//sap
            return 4;
        else if (number < 15)//lieng
            return 3;
        else if (number < 25)//anh
            return 2;
        else if (number < 45)//diem cao
            return 1;
        else return 0;
    }

    public static int generateRandom(int len) {
        SecureRandom random = new SecureRandom();
        return random.nextInt(len);
    }

    public static String generateRandomKey(int len) {
        // Generate a random salt
        SecureRandom random = new SecureRandom();
        byte[] salt = new byte[len];
        random.nextBytes(salt);
        return toHex(salt);
    }

    public static String generateRandomNumber(int len, int max) {
        // Generate a random salt
        SecureRandom random = new SecureRandom();
        byte[] salt = new byte[len];
        random.nextBytes(salt);
        for (byte b : salt)
            b = (byte) (b % max);
        return toHex(salt);
    }

//    public static String encryptPassword(String salt, String normalPassword) {
//        try {
//            byte[] keyBytes = salt.getBytes();
//            SecretKey secretKey = new SecretKeySpec(keyBytes, "HmacSHA1");
//            Mac mac = Mac.getInstance("HmacSHA1");
//            mac.init(secretKey);
//            byte[] text = normalPassword.getBytes();
//            return Base64.encode(mac.doFinal(text)).trim();
//        } catch (Exception e) {
//            return normalPassword;
//        }
//    }

//    public static ByteBuf buildYokerMessageBuffer(Object message) {
//        YokerMessage gameMessage = (YokerMessage) message;
//        ByteBuf buffer = null;
//        if (gameMessage.getType() == Constants.YokerMessageTypeCode.RESPONSE) {
//            YokerResponseMessage gameResponse = (YokerResponseMessage) message;
//            int capacity = 9 + gameResponse.getPayLoadSize();
//            buffer = Unpooled.buffer(capacity);
//
//            buffer.writeByte(Constants.YokerMessageTypeCode.RESPONSE);
//            buffer.writeByte(gameResponse.getRequestType().id);
//            buffer.writeByte(gameResponse.getResultCode());
//            buffer.writeByte(gameResponse.getReserved());
//            buffer.writeByte(gameResponse.getPayLoadType());
//            buffer.writeInt(gameResponse.getPayLoadSize());
//            buffer.writeBytes(gameResponse.getPayLoad());
//
//        } else if (gameMessage.getType() == Constants.YokerMessageTypeCode.UPDATE) {
//            YokerNotifyMessage notify = (YokerNotifyMessage) message;
//            int capacity = 8 + notify.getPayLoadSize();
//            buffer = Unpooled.buffer(capacity);
//
//            buffer.writeByte(Constants.YokerMessageTypeCode.UPDATE);
//            buffer.writeByte(notify.getNotifyType().id);
//            buffer.writeByte(notify.getReserved());
//            buffer.writeByte(notify.getPayLoadType());
//            buffer.writeInt(notify.getPayLoadSize());
//            buffer.writeBytes(notify.getPayLoad());
//        } else {
//            /* This is Yoker client request message */
//            YokerRequestMessage gameRequest = (YokerRequestMessage) message;
//            int capacity = 16 + gameRequest.getPayLoadSize();
//            buffer = Unpooled.buffer(capacity);
//
//            buffer.writeByte(Constants.YokerMessageTypeCode.REQUEST);
//            buffer.writeByte(gameRequest.getRequestType().id);
//            buffer.writeInt(gameRequest.getSessionId());
//            buffer.writeInt(gameRequest.getRequestId());
//            buffer.writeByte(gameRequest.getReserved());
//            buffer.writeByte(gameRequest.getPayLoadType());
//            buffer.writeInt(gameRequest.getPayLoadSize());
//            buffer.writeBytes(gameRequest.getPayLoad());
//        }
//        return buffer;
//    }
//
//    public static YokerMessage buildYokerMessagePack(YokerMessage message) {
//        if (message.getType() == Constants.YokerMessageTypeCode.RESPONSE) {
//            YokerResponseMessage gameResponse = (YokerResponseMessage) message;
//            byte[] pack = toPack(gameResponse.getPayLoad());
//            return new YokerResponseMessage(gameResponse.getType(), gameResponse.getResultCode(),
//                    gameResponse.getRequestType(), gameResponse.getReserved(), Constants.PayloadTypeCode.PACK,
//                    pack.length, pack);
//        } else {
//            YokerNotifyMessage notify = (YokerNotifyMessage) message;
//            byte[] pack = toPack(notify.getPayLoad());
//            return new YokerNotifyMessage(notify.getNotifyType(), notify.getType(), notify.getReserved(),
//                    Constants.PayloadTypeCode.PACK, pack.length, pack);
//        }
//    }
//
//    public static YokerMessage buildYokerMessageJson(YokerMessage message) {
//        YokerMessage gameMessage = message;
//        byte[] pack = unPack(gameMessage.getPayLoad());
//        gameMessage.setPayLoadType(Constants.PayloadTypeCode.JSON);
//        gameMessage.setPayLoad(pack);
//        gameMessage.setPayLoadSize(pack.length);
//        return gameMessage;
//    }

    /**
     * Converts a string of hexadecimal characters into a byte array.
     *
     * @param hex the hex string
     * @return the hex string decoded into a byte array
     */
    public static byte[] fromHex(String hex) {
        byte[] binary = new byte[hex.length() / 2];
        for (int i = 0; i < binary.length; i++)
            binary[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
        return binary;
    }

    /**
     * Converts a byte array into a hexadecimal string.
     *
     * @param array the byte array to convert
     * @return a length*2 character string encoding the byte array
     */
    public static String toHex(byte[] array) {
        BigInteger bi = new BigInteger(1, array);
        String hex = bi.toString(16);
        int paddingLength = (array.length * 2) - hex.length();
        if (paddingLength > 0) return String.format("%0" + paddingLength + "d", 0) + hex;
        else return hex;
    }

    /**
     * @param payLoad
     * @return
     */
    public static JSONObject toJson(byte[] payLoad) {
        if (payLoad.length == 0) return new JSONObject();
        String payLoadMsg = "";
        try {
            payLoadMsg = new String(payLoad, StandardCharsets.UTF_8);
            return new JSONObject(payLoadMsg);
        } catch (Exception ex) {
            LOG.error("Error ", ex);
            LOG.error("Raw message: " + payLoadMsg);
            return null;
        }
    }

//    public static JSONObject pack2Json(byte[] payLoad) {
//        MessagePack msgpack = new MessagePack();
//        if (payLoad.length == 0) return new JSONObject();
//        String payLoadMsg = "";
//        try {
//            MapValue value = (MapValue) msgpack.read(payLoad);
//            payLoadMsg = value.toString();
//            if (payLoadMsg.trim().startsWith("[")) {
//                LOG.warn("Not expected JSONObject: {}", payLoadMsg);
//                return new JSONObject();
//            } else {
//                JSONObject jsonObj = new JSONObject(payLoadMsg);
//                return jsonObj;
//            }
//        } catch (Exception ex) {
//            LOG.error("Error ", ex);
//            LOG.error("Raw message: " + payLoadMsg);
//            return null;
//        }
//    }

//    public static byte[] unPack(byte[] payLoad) {
//        MessagePack msgpack = new MessagePack();
//        try {
//            // deserialize
//            Value dynamic = msgpack.read(payLoad);
//            if (dynamic.isMapValue()) {
//                MapValue value = dynamic.asMapValue();
//                return value.toString().getBytes();
//            } else if (dynamic.isArrayValue()) {
//                ArrayValue value = dynamic.asArrayValue();
//                return value.toString().getBytes();
//            } else if (dynamic.isBooleanValue()) {
//                BooleanValue value = dynamic.asBooleanValue();
//                return value.toString().getBytes();
//            } else if (dynamic.isFloatValue()) {
//                FloatValue value = dynamic.asFloatValue();
//                return value.toString().getBytes();
//            } else if (dynamic.isIntegerValue()) {
//                IntegerValue value = dynamic.asIntegerValue();
//                return value.toString().getBytes();
//            } else if (dynamic.isNilValue()) {
//                NilValue value = dynamic.asNilValue();
//                return value.toString().getBytes();
//            } else if (dynamic.isRawValue()) {
//                RawValue value = dynamic.asRawValue();
//                return value.getByteArray();
//            } else throw new MessageTypeException("Expected map but got not map value");
//			/*MapValue value = (MapValue) msgpack.read(payLoad);
//			return value.toString().getBytes();*/
//        } catch (Exception ex) {
//            LOG.error("Error ", ex);
//        }
//        return null;
//    }
//
//    @SuppressWarnings("unchecked")
//    public static byte[] toPack(byte[] payLoad) {
//        MessagePack msgpack = new MessagePack();
//        // Serialize
//        try {
//            String jsonString = new String(payLoad);
//            HashMap<String, Object> result = new ObjectMapper().readValue(jsonString, HashMap.class);
//            return msgpack.write(result);
//        } catch (IOException ex) {
//            // TODO Auto-generated catch block
//            LOG.error("Error json2Pack " + new String(payLoad), ex);
//            return null;
//        }
//    }

    public static int compareVersion(String ver1, String ver2) {
        String[] arr1 = ver1.split("\\.");
        String[] arr2 = ver2.split("\\.");
        int min = Math.min(arr1.length, arr2.length);
        int result = 0;
        for (int i = 0; i < min; i++) {
            result = arr1[i].compareTo(arr2[i]);
            if (result != 0) return result;
        }
        return result;
    }

//    public static byte[] zipStream(byte[] inputStream) {
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        ZipOutputStream zos = new ZipOutputStream(baos);
//        ZipEntry entry = new ZipEntry("");
//        entry.setSize(inputStream.length);
//        try {
//            zos.putNextEntry(entry);
//            zos.write(inputStream);
//            zos.closeEntry();
//            zos.close();
//            return baos.toByteArray();
//        } catch (IOException ex) {
//            LOG.error("Error json2Pack ", ex);
//            return inputStream;
//        }
//    }
//https://stackoverflow.com/questions/23057549/lambda-expression-to-convert-array-list-of-string-to-array-list-of-integers	

    //for lists
    public static <T, U> List<U> convertList(Collection<T> from, Function<T, U> func) {
        return from.stream().map(func).collect(Collectors.toList());
    }

    //for arrays
    public static <T, U> U[] convertArray(
            T[] from, Function<T, U> func, IntFunction<U[]> generator) {
        return Arrays.stream(from).map(func).toArray(generator);
    }

    public static String encodeUrl(String value) {
        try {
            return URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex.getCause());
        }
    }

//    public static String getGoldName(UserBo userBo, User u) {
//        String tmpStr_115 = "${gold}";
//        if (u != null) tmpStr_115 = userBo.replaceProviderConfig(u.getProviderCode(), tmpStr_115);
//        else tmpStr_115 = userBo.replaceProviderConfig(null, tmpStr_115);
//        return tmpStr_115;
//    }

    public static Date getCurrentDateWithZeroTime() {

        // Get Calendar object set to the date and time of the given Date object
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());

        // Set time fields to zero
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        // Put it back in the Date object
        return cal.getTime();
    }
}
