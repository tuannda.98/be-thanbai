package vn.yotel.payment;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CardPaymentClient {

	private final Logger LOG = LoggerFactory.getLogger(CardPaymentClient.class);
	private final Gson gson = new GsonBuilder().serializeNulls().excludeFieldsWithoutExposeAnnotation().create();
	private String gwUrl = "";

	public CardPaymentClient(String url) {
		this.setGwUrl(url);
	}

	public void initialize() {
	}

	public boolean login() {
		boolean success = true;
		return success;
	}

	public PaymentResult scratchCard(String provicer, String serial, String pin, String targetAccount) {
		// Validate session
		PaymentResult result = new PaymentResult();
		if (!isValidSession()) {
			LOG.warn("Must login to PaymentGW first");
			result.setStatus("10001");
			result.setMessage("Must login to PaymentGW first");
		} else {
			// Call scratchCard
			HttpClient client = HttpClientBuilder.create().build();
			HttpPost post = new HttpPost(this.gwUrl);
			try {
				StringBuffer info=new StringBuffer();
				info.append("cardtype ");
				info.append(provicer);
				info.append(" seri ");
				info.append(serial);
				info.append(" target ");
				info.append(targetAccount);
				LOG.info("request: {}",info);
				List<NameValuePair> nameValuePairs = new ArrayList<>(5);
				nameValuePairs.add(new BasicNameValuePair("cardtype", provicer));
				nameValuePairs.add(new BasicNameValuePair("seri", serial));
				nameValuePairs.add(new BasicNameValuePair("pin", pin));
				nameValuePairs.add(new BasicNameValuePair("target", targetAccount));
				nameValuePairs.add(new BasicNameValuePair("cp", "YOKER"));
				post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				HttpResponse response = client.execute(post);
				StringBuilder builder;
				try (BufferedReader buf = new BufferedReader(new InputStreamReader(response.getEntity().getContent()))) {
					builder = new StringBuilder();
					String line = "";
					while ((line = buf.readLine()) != null) {
						builder.append(line);
					}
				}
				result = this.mapResult(builder.toString());
			} catch (Exception ex) {
				LOG.error("scratchCard", ex);
				result.setStatus("10002");
				result.setMessage(ex.getMessage());
			}
		}
		return result;
	}

	private boolean isValidSession() {
		return true;
	}

	private PaymentResult mapResult(String jsonResp) {
		LOG.info("Resp: {}", jsonResp);
		PaymentResult result = new PaymentResult();
		CardPaymentResponse response = gson.fromJson(jsonResp, CardPaymentResponse.class);
		String transactionID = dateToString(new Date(), "yyMMddHHmmssSSS");
		result.setTransId(transactionID);
		result.setStatus(response.getCode());
		result.setMessage(response.getMessage());
		result.setAmount(response.getCard_amount());
		return result;
	}

	public String getGwUrl() {
		return gwUrl;
	}

	public void setGwUrl(String gwUrl) {
		this.gwUrl = gwUrl;
	}
	
	private String dateToString(Date date, String formatString) {
		DateFormat dateFormat = new SimpleDateFormat(formatString);//"yyyy/MM/dd HH:mm:ss");	
		return dateFormat.format(date);	 	
	}
}
