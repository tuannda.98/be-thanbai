package vn.yotel.payment.buycard;

import casio.king365.Constants;
import com.google.gson.Gson;
import lombok.ToString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vn.yotel.payment.buycard.api.Request;
import vn.yotel.payment.buycard.api.Util;
import vn.yotel.payment.buycard.entities.CardReq;
import vn.yotel.payment.buycard.entities.CardRes;

import java.io.IOException;

/**
 *
 */
public class BuyCardClient {
    //private static final String password = "dacuoi_dev";
    // u: stv_kt
    // p: dacuoi_kt
    private static final Gson gson = new Gson();
    static Logger LOG = LoggerFactory.getLogger(BuyCardClient.class);
    private static String privateKey;// = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBANFUarodVWpHQlmB+1unp9SYulAxS2R64pkBR3b184IoiBiV4ICjUXG50K17+638gUvgJZT6mUexsY3ZEJJ1tDFAWs4VbReQetuy/LuswqWT6egbqtPLZVyLR50eZCQu5wIMSIr5ke2KB9f8923KLeZ4bMlWow+YmCoA7FfYpxlfAgMBAAECgYAAn3Kt2RZ++XVOFN/tluMd89JiGLNzuhXghKt0xCiUU5CMGi4P0+yTHQ1s5riJ/P1Zf3I9Lj20g+fWVzYAaY8+sOOmv3H0L/tgw5807ehxb9WVDg1M4IY4MM72PiQBPvDbAgnqqTXaO2zvfpf+adkimmzV4R6A3+JsorBCh6OuOQJBAPm7GmLtXebsD8WWBL0p7MpP7bGgJtnGyEichfE8bq3HmtRC2ufsPqlX4Sc6T7tWvoClZO4O2up6JaVKnRJgvzUCQQDWla0BqOeyAVhmOArOSX/41oNtg4xahLJrKl3vIKMyOGn7u0BP/Q+pVCv3OyOERic7eDklH19+dpsvIhluCiTDAkEAjVv8xqzIL5PGJbkSe6VEtQxjZt1ZwIJGEtwO8Yuctt+645XoZcmTerYU6+iVA11JvnZg2GQuRPXZY2QF30ZGVQJAGpjEr876oNKP3WOW20i65uHXumwyqkmGP8gpimkWMTRS1pO3/fIUzd0T8fnPhiaX2EQneRVxZaBXzbjBRCVXtwJBAM7qDcV8igbO3nOg8UevxrLcRPpq+Zc2lam8A0YAuz56g2AQGBcrieYvPqUAUxiUPMUDB8uLeUwk6F5CeZcRlmw=";
    //private static final String url = "https://dev.cashout.firetank.xyz/api/sandbox/transfer/purchase";
    private static String CARD_URL = "https://cashout.firetank.xyz/api/transfer/purchase";
    private static String MOMO_URL = "https://dev.cashout.firetank.xyz/api/sandbox/withdraw/momo";
    private static String BANK_URL = "https://dev.cashout.firetank.xyz/api/sandbox/withdraw/momo";
    //private static final String partnerId = "stv_dev";
    private static String partnerId = "stv_kt";
    private static BuyCardClient instance;

    public BuyCardClient(String privateKey, String partnerId, String cardUrl, String momoUrl, String bankUrl) {
        super();
        BuyCardClient.privateKey = privateKey;
        BuyCardClient.partnerId = partnerId;
        BuyCardClient.CARD_URL = cardUrl;
        BuyCardClient.MOMO_URL = momoUrl;
        BuyCardClient.BANK_URL = bankUrl;
    }

    public static BuyCardClient getInstance() {
        if (instance != null) return instance;

        instance = new BuyCardClient(Constants.BuyCardClient.privateKey
                , Constants.BuyCardClient.partnerId,  // hanoi_card
                Constants.BuyCardClient.cardUrl  // https://cashout.firetank.xyz/api/transfer/purchase
                , Constants.BuyCardClient.momoUrl
                , Constants.BuyCardClient.bankUrl);
        return instance;
    }

    public static void main(String[] args) {
        //		BuyCardClient client = new BuyCardClient("MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAJKotDGG7g4umh5GYrjW3ICEaYFkPDpnpFCt5vbiifzxWFv9gzcedpkf+yKSDlpyElRdzztV1IScfye9WEC+OxF9qurr6anKg9rLJJIlFF4pVWJ1oF+LL9Lpaf6jGKr9avAf/07XFO6Q29VfpzoLD81RbTyQzu04S6iGORiAvHXTAgMBAAECgYEAjgm1+wGjLTkVKp0gn8P4ar/TLu3VJvuZuEm0lwdwfPmBihZNLuTiGAtRPnoWTKKN/gec3OqTmJu4ytQm1Q1oRmBG4lk2XamYz3J6s9bT8/47L0pukbrHSf8Ar7JbsBSvcXPudjyRzKsWJ3HJiXKAwTbqh5UeZSHqaJvV7RBE54ECQQDOl3PAtvIl91FL18dcMzOJStgVWIMy1czbH2t/3Yz7iVtnBA64OBmLK13ILFS1dexMKbHjajykv8xNHc6TnFJdAkEAtbvia9MNeJOI3vXn8ni1nn7nBZ8POTXuSFPO4eDTmZlHqESVMbz6+tYk1EP/fGiPrcZrYWoiN3XgdP+iDXLF7wJBAJcUbQM3Bvi618oGH/YOP5cck89wbmnjrq9kV+InbG2a8qvwbK9N32hGK4wy1zPo7Ah6yWuNCpJU7RxMOwD55cUCQCf9SOx7C4rNxB9zsu9a+nP+7q0A/COVp3JI5swzYbx5yxfOx5mjQQLzHjWLJOvmQj912ukbpp3XlWWJXspgyAcCQBSiGMgRa7jUoAThJMOLNHjQHcduqbtLfmHQnl97SqaOTFEj6ATmLDzpfJ+Ceh9ml7pVAUD9Qpb2CBlYHmeh+Tc=",
        //			"stv_kt");
        System.out.println("\n\n\n=================== hello");
        BuyCardClient client = new BuyCardClient("MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAJIzmXeqfh4ozTXG02SQul9qFpw3p6q1YnYVbv6VjWKaidi/Wo48fHTK5YP5iBIglth4Y1STA/zA4RLJ774AIICqcu1ekvvd/cQB7ZSj1UBXtcvEFJeQQBEEjxDCp5HhCgwHERudlTR3Ppqd3jgEMJpuX9ysYbgUERrG49ds4zKlAgMBAAECgYBnmp1EidM5uBxD4ZPoDg+kkd9X8LCmbkdJ+sJfMsewemb47BFV/lMLyMSAk5HzlpiO6jEpu8z1jhFxaRy1fg2JTce/0mBOVgy4pmbcLgkIoSbMy/mRmLJFiu2+GCcJOaBAMYzRB3FvEMhnYqhnR/f/3SZYjnoohy/BJ7Pfgb0ZAQJBAMphDAj+dUe85zkXWWkBbX70Zxue3KOighHHmG+T4lV0+OTLdfbNh2e9f9S6zY0y2NEsjnTFOGYevfj24YihzqECQQC48CW9m1AaFBww90pn1XOa4Me99KWJCCcrB7cWbMRjkPauREQM1M2re173Zfic2nTuq5tGIz7nMgby1Hq9MTmFAkBf9oyiZyZtkeGCbNP0uryVCocpzbaJ0TEkBST/I3ybURAETYXuNZ8TTgQjd0Cox4eQCDS0oiAQLFughlCDXVaBAkAaMVkHC7JCn0qG3N9a6W5gJynjC31OdSsm+wbBVFJUT7nAYMbKknE2slh27ahSftZE32U0f9Vu5TT6u4FOvmQJAkBCkfpiVv6zdy/N+IgdxuYKyYXfSLjFYqYO/2HXlQYjQfGgJ6lLXY+PANwhHpOFNt2C344h8Tfuo0qsNcNbI8oe"
                , "hanoi_dev",  // hanoi_card
                "https://dev.cashout.firetank.xyz/api/sandbox/transfer/purchase"  // https://khothetopup.com/api/transfer/purchase
                , "https://cashout.firetank.xyz/api/withdraw/momo",
                "");
        CardRes cr = null;
        try {
            cr = client.buyCard("VTT", 20000, 1, "12231112215131");
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("cr: " + cr.toString());
        // System.out.println("cr: "+gson.toJson(cr));

        System.out.println("\n\n\n=================== finish");
        //		System.out.println("zxmncbzxbckjasndnasd".substring(0,Math.min("zxmncbzxbckjasndnasd".length(),4)));
        //		System.out.println("A " + Duration.between(
        //			new Date().toInstant().atZone(ZoneId.systemDefault()),LocalDateTime.now().atZone(ZoneId.systemDefault())).toMinutes());
        //
        //		new Date().toInstant();
    }

    public CardRes buyCard(String serviceCode, int demonition, int quantity, String orgTransID) throws Exception {
        String telco;
        switch (serviceCode.toUpperCase()) {
            case "MOBI":
            case "MOBIFONE":
            case "VMS":
                telco = "VMS";
                break;
            case "VINA":
            case "VINAPHONE":
            case "VNP":
                telco = "VINA";
                break;
            case "VT":
            case "VTT":
            case "VTE":
            case "VIETTEL":
                telco = "VTT";
                break;
            default:
                telco = serviceCode.toUpperCase();
                break;
        }
        String partnerRef = orgTransID;
        CardReq cardReq = new CardReq();
        cardReq.partner_id = partnerId;
        cardReq.denomination = demonition;
        cardReq.quantity = quantity;
        cardReq.telco = telco;
        cardReq.partner_reference = partnerRef;
        cardReq.signature = "";
        System.out.println("buyCard(), url: " + CARD_URL);
        CardRes ca = Request.getInstance().purchaseCard(cardReq, privateKey, CARD_URL);
        System.out.println("buyCard(), ca: " + ca.toString());
        if (ca.code != 0) {
            String res = gson.toJson(ca);
            LOG.warn("Can not get card: " + res);
            return ca;
        }

        CardRes decriptedData = Util.decrypt(ca, privateKey);
        if (decriptedData == null) {
            LOG.warn("Can not decrypt: " + decriptedData);
            return ca;
        }
        return decriptedData;
    }

    public String buyBank(String url, String jsonData) throws IOException {
        return Request.getInstance().purchaseBank(url, jsonData);
    }

    public MomoRes buyMomo(
            long demonition, String orgTransID, String request_acc_no, String request_acc_name, String request_desc) {
        MomoRes decriptedData = null;
        String partnerRef = orgTransID;
        try {
            MomoReq momoReq = new MomoReq();
            momoReq.partner_id = partnerId;
            momoReq.denomination = demonition;
            momoReq.quantity = 1;
            momoReq.telco = "MOMO";
            momoReq.partner_reference = partnerRef;
            momoReq.signature = "";
            momoReq.request_acc_no = request_acc_no.trim();
            momoReq.request_acc_name = request_acc_name.trim();
            momoReq.request_desc = request_desc;
            try {
                decriptedData = Request.getInstance().purchaseMomo(momoReq, privateKey, MOMO_URL);
                String res = gson.toJson(decriptedData);
                if (decriptedData.code == 0) {
                    LOG.debug("------------");
                    String plantextData = gson.toJson(decriptedData);
                    LOG.debug("decrypted data: " + plantextData);
                    //,"cards":[{"telco":"VTT","amount":50000,"serial":"77461001896057","pin":"40093977004"}]
                } else LOG.warn("Can not get card: " + res);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return decriptedData;
    }

    @ToString
    public static class MomoReq {
        public String partner_id;
        public String partner_reference;
        public String telco;
        public String request_acc_no;
        public String request_acc_name;
        public String request_bank_code;
        public long denomination;
        public int quantity;
        public String request_desc;
        public String signature;
    }

    @ToString
    public static class MomoData {
        public String partner_reference;
        public String server_reference;
        public String trans_time;
        public String target_acc_no;
        public String target_acc_name;
        public String transfer_id;
        public String transfer_message;
    }

    @ToString
    public static class MomoRes {
        public int code;
        public String message;
        public MomoData data;
    }
}
