package vn.yotel.yoker.promotion;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

import com.google.gson.annotations.Expose;

public class PurchasePromotion implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Expose private double first_multiply = 0;
    @Expose private float out_target = 0;
    @Expose private double out_multiply = 0;
    @Expose private Date fromDate = new Date();
    @Expose private Date toDate = new Date();
    @Expose private double multiply = 1;
    @Expose private int type = 1;
    @Expose private double value10 = 1;
    @Expose private double value20 = 1;
    @Expose private double value30 = 1;
    @Expose private double value50 = 1;
    @Expose private double value100 = 1;
    @Expose private double value200 = 1;
    @Expose private double value300 = 1;
    @Expose private double value500 = 1;
    public double getFirst_multiply() {
        return first_multiply;
    }
    public void setFirst_multiply(double first_multiply) {
        this.first_multiply = first_multiply;
    }
    public float getOut_target() {
        return out_target;
    }
    public void setOut_target(float out_target) {
        this.out_target = out_target;
    }
    public double getOut_multiply() {
        return out_multiply;
    }
    public void setOut_multiply(double out_multiply) {
        this.out_multiply = out_multiply;
    }
    public Date getFromDate() {
        return fromDate;
    }
    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }
    public Date getToDate() {
        return toDate;
    }
    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }
    public double getMultiply() {
        return multiply;
    }
    public void setMultiply(double multiply) {
        this.multiply = multiply;
    }
    public int getType() {
        return type;
    }
    public void setType(int type) {
        this.type = type;
    }
    public double getValue10() {
        return value10;
    }
    public void setValue10(double value10) {
        this.value10 = value10;
    }
    public double getValue20() {
        return value20;
    }
    public void setValue20(double value20) {
        this.value20 = value20;
    }
    public double getValue30() {
        return value30;
    }
    public void setValue30(double value30) {
        this.value30 = value30;
    }
    public double getValue50() {
        return value50;
    }
    public void setValue50(double value50) {
        this.value50 = value50;
    }
    public double getValue100() {
        return value100;
    }
    public void setValue100(double value100) {
        this.value100 = value100;
    }
    public double getValue200() {
        return value200;
    }
    public void setValue200(double value200) {
        this.value200 = value200;
    }
    public double getValue300() {
        return value300;
    }
    public void setValue300(double value300) {
        this.value300 = value300;
    }
    public double getValue500() {
        return value500;
    }
    public void setValue500(double value500) {
        this.value500 = value500;
    }

}
