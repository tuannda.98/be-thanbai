package vn.yotel.yoker.util;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TripleDES3
{
	private final static String initializationVector = "PAY-GATE";
	private final static String DEFAULT_KEY = "Casino@123yyyyMMddHHmmss";

	private static final Random random = new Random((new Date()).getTime());

	public static String generateRandomString(int length)
	{
		char[] values =
		{
			'a','b','c','d','e','f','g','h','i','j',
			'k','l','m','n','o','p','q','r','s','t',
			'u','v','w','x','y','z',
			'A','B','C','D','E','F','G','H','I','J',
			'K','L','M','N','O','P','Q','R','S','T',
			'U','V','W','X','Y','Z','0','1','2','3',
			'4','5','6','7','8','9'
		};

		String out = "";

		for(int i = 0;i < length;i++)
		{
			int idx = random.nextInt(values.length);
			out += values[idx];
		}

		return out;
	}

	public static void main(String[] args)
	{
		String key = generateRandomString(10) + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());

		System.out.println(key);
		try
		{
			String esc = TripleDES3.encryptText("plainText",key);
			System.out.println(esc);
			System.out.println(TripleDES3.decryptText(esc,key));

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static String encryptText(String plainText,String key) throws Exception
	{
		byte[] plaintext = plainText.getBytes();
		byte[] tdesKeyData = key.getBytes();
		Cipher c3des = Cipher.getInstance("DESede/CBC/PKCS5Padding");
		SecretKeySpec myKey = new SecretKeySpec(tdesKeyData,"DESede");
		IvParameterSpec ivspec = new IvParameterSpec(
			initializationVector.getBytes());
		c3des.init(Cipher.ENCRYPT_MODE,myKey,ivspec);
		byte[] cipherText = c3des.doFinal(plaintext);
		return new String(Base64.encodeBase64(cipherText));
	}

	public static String decryptText(String cipherText,String key) throws Exception
	{
		byte[] encData = Base64.decodeBase64(cipherText.getBytes());
		Cipher decipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
		byte[] tdesKeyData = key.getBytes();
		SecretKeySpec myKey = new SecretKeySpec(tdesKeyData,"DESede");
		IvParameterSpec ivspec = new IvParameterSpec(
			initializationVector.getBytes());
		decipher.init(Cipher.DECRYPT_MODE,myKey,ivspec);
		byte[] plainText = decipher.doFinal(encData);
		return new String(plainText);
	}

	public static String encryptText(String plainText)
	{
		try
		{
			return encryptText(plainText,DEFAULT_KEY);
		}
		catch(Exception ex)
		{
			Logger.getLogger(TripleDES3.class.getName()).log(Level.SEVERE,null,ex);
		}
		return null;
	}

	public static String decryptText(String cipherText)
	{
		try
		{
			return decryptText(cipherText,DEFAULT_KEY);
		}
		catch(Exception ex)
		{
			Logger.getLogger(TripleDES3.class.getName()).log(Level.SEVERE,null,ex);
		}
		return null;
	}
}
