package vn.yotel.payment.buycard.api;

import casio.king365.Constants;
import casio.king365.util.KingUtil;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;
import vn.yotel.payment.buycard.BuyCardClient;
import vn.yotel.payment.buycard.entities.CardReq;
import vn.yotel.payment.buycard.entities.CardRes;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.Map;


/**
 * @author luongvv
 * https://www.baeldung.com/httpclient-connection-management
 */
public class Request {

    private static final Logger logger = Logger.getLogger("api");

    private int timeout = 30_000;

    private static final Gson GSON = new Gson();

    private static Request instance;

    public Request() throws IOException {
    }

    public static Request getInstance() throws IOException {
        if (instance == null) {
            synchronized (Request.class) {
                if (instance == null) {
                    instance = new Request();
                }
            }
        }
        return instance;
    }

    public static void setInstance(Request instance) {
        Request.instance = instance;
    }

    public static <T> T parseJSON(String jsonString, Class<T> classOfT) throws Exception {
        try {
            logger.debug("json string:" + jsonString);
            return GSON.fromJson(jsonString, classOfT);
        } catch (JsonSyntaxException ex) {
            logger.debug("<T> T parseJSON, exception: " + ex.getMessage());
            ex.printStackTrace(System.out);
            Map<String, Object> data = new HashMap<>();
            data.put("JsonString", jsonString);
            data.put("Class", classOfT);
            throw new Exception("Cant not parse String to JSON" + data);
        }
    }

    public <T> T post(String url, String jsonData, Class<T> classOfT) throws IOException {
        String response = post(url, jsonData);
        logger.debug("<T> T post, response");
        T instance = null;
        try {
            instance = parseJSON(response, classOfT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return instance;
    }

    public <T> T postMomo(String url, String jsonData, Class<T> classOfT) throws IOException {
        String response = post(url, jsonData);
        T instance = null;
        try {
            instance = parseJSON(response, classOfT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return instance;
    }

    public String post(String url, String jsonData) throws IOException {
        logger.debug("do send to url: " + url + "\n with data: " + jsonData);
        String body = "";
        try (CloseableHttpClient client = HttpClients.createDefault()) {
            HttpPost httpost = new HttpPost(url);
            httpost.setHeader("User-Agent", "Mozilla/5.0");
            httpost.addHeader("Content-Type", "application/json;charset=UTF-8");
            httpost.setHeader("x-version", Constants.BuyCardClient.xVersion);
            httpost.setHeader("x-partner", Constants.BuyCardClient.partnerId);
            StringEntity entity = new StringEntity(jsonData, "UTF-8");
            httpost.setEntity(entity);
            try (CloseableHttpResponse response = client.execute(httpost)) {
                BasicResponseHandler handler = new BasicResponseHandler();
                body = handler.handleResponse(response);
            }
        }
        logger.debug("res content: " + body);
        return body;
    }

    public CardRes purchaseCard(CardReq cardReq, String privateKey, String urlPurchaseCard) throws Exception {
        logger.debug("urlPurchaseCard: " + urlPurchaseCard);
        cardReq.signature = signData(cardReq, privateKey);
        String jsonData = GSON.toJson(cardReq);
        return post(urlPurchaseCard, jsonData, CardRes.class);
    }

    public BuyCardClient.MomoRes purchaseMomo(BuyCardClient.MomoReq cardReq, String privateKey, String urlPurchaseCard)
            throws Exception {
        cardReq.signature = signData(cardReq, privateKey);
        String jsonData = GSON.toJson(cardReq);
        BuyCardClient.MomoRes purchaseCardRes = postMomo(urlPurchaseCard, jsonData, BuyCardClient.MomoRes.class);
        return purchaseCardRes;
    }

    public String purchaseBank(String url, String jsonData) throws IOException {
        return post(url, jsonData);
    }

    public String getListBankCashout(CardReq cardReq, String privateKey, String url) throws Exception {
        logger.debug((Object) ("getListBankCashout() - urlPurchaseCard: " + url));
        cardReq.signature = signData(cardReq, privateKey);
        String jsonData = GSON.toJson(cardReq);
        logger.debug((Object) ("getListBankCashout() - jsonData: " + jsonData));
        // return jsonData;
        return KingUtil.post(url, jsonData);
    }

    public String checkBankCashout(CardReq cardReq, String privateKey, String url) throws Exception {
        logger.debug((Object) ("checkBankCashout() - urlPurchaseCard: " + url));
        cardReq.signature = signDataCheckBankCashout(cardReq, privateKey);
        String jsonData = GSON.toJson(cardReq);
        logger.debug((Object) ("checkBankCashout() - jsonData: " + jsonData));
        // return jsonData;
        return KingUtil.post(url, jsonData);
    }

    public String signDataCheckBankCashout(CardReq cardReq, String privateKey)
            throws InvalidKeyException, NoSuchAlgorithmException, SignatureException, UnsupportedEncodingException,
            InvalidKeySpecException {
        String data = "partner_id=" + cardReq.partner_id + "&partner_reference=" + cardReq.partner_reference + "&telco=" + cardReq.telco;
        KingUtil.printLog("sign string: " + data);
        KingUtil.printLog("privateKey: " + privateKey);
        return RSAUtil.sign(privateKey, data);
    }

    public String signData(CardReq cardReq, String privateKey)
            throws InvalidKeyException, NoSuchAlgorithmException, SignatureException, UnsupportedEncodingException,
            InvalidKeySpecException {
        String data = "partner_id=" + cardReq.partner_id + "&partner_reference=" + cardReq.partner_reference + "&quantity=" + cardReq.quantity + "&telco=" + cardReq.telco + "&denomination=" + cardReq.denomination;
        return RSAUtil.sign(privateKey, data);
    }

    public String signData(BuyCardClient.MomoReq momoReq, String privateKey)
            throws InvalidKeyException, NoSuchAlgorithmException, SignatureException, UnsupportedEncodingException,
            InvalidKeySpecException {
        String data = "partner_id=" + momoReq.partner_id + "&partner_reference=" + momoReq.partner_reference + "&quantity=" + momoReq.quantity + "&telco=" + momoReq.telco + "&denomination=" + momoReq.denomination;
        return RSAUtil.sign(privateKey, data);
    }

}
