package vn.yotel.yoker.dao;

import vn.yotel.yoker.domain.RequestCard;

public interface RequestCardDao extends GenericDao<RequestCard> {
    RequestCard findByGwStransid(String gwTransId);
}
