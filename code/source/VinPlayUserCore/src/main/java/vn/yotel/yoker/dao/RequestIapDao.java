package vn.yotel.yoker.dao;

import vn.yotel.yoker.domain.RequestIap;

import java.sql.SQLException;

public interface RequestIapDao {
    boolean existTransId(String transId) throws Exception;
    boolean create(RequestIap requestIap) throws Exception;
}
