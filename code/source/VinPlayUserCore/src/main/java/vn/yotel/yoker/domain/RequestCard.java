package vn.yotel.yoker.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;


/**
 * The persistent class for the request_card database table.
 * 
 */
@Entity
@Table(name="request_card")
@NamedQuery(name="RequestCard.findAll", query="SELECT r FROM RequestCard r")
public class RequestCard implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = true, columnDefinition="TIMESTAMP default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP")
	private Timestamp datetime;

	@Column(name="money")
	private Double money;
	
	@Column(name="eq_koin")
	private Double eqKoin;

	private String note;

	private String pin;

	@Column(name="process_status")
	private Byte processStatus;

	private String provider;

	private String serial;

	@Column(name="user_id")
	private Integer userId;
	
	@Column(name = "username")
	private String username;

	@Column(name="gw_transid")
	private String gwTransId;
	
	@Column(name="gw_message")
	private String gwMessage;
	
//	@Column(name="gw_type")
//	private String gwType;

	@Column(name="gw_status")
	private Integer gwStatus;

	public RequestCard() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDatetime() {
		return this.datetime;
	}

	public void setDatetime(Timestamp datetime) {
		this.datetime = datetime;
	}

	public Double getEqKoin() {
		return this.eqKoin;
	}

	public void setEqKoin(Double eqKoin) {
		this.eqKoin = eqKoin;
	}

	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getPin() {
		return this.pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public byte getProcessStatus() {
		return this.processStatus;
	}

	public void setProcessStatus(byte processStatus) {
		this.processStatus = processStatus;
	}

	public String getProvider() {
		return this.provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getSerial() {
		return this.serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Double getMoney() {
		return money;
	}

	public void setMoney(Double money) {
		this.money = money;
	}

	public String getGwTransId() {
		return gwTransId;
	}

	public void setGwTransId(String gwTransId) {
		this.gwTransId = gwTransId;
	}

	public String getGwMessage() {
		return gwMessage;
	}

	public void setGwMessage(String gwMessage) {
		this.gwMessage = gwMessage;
	}

	public Integer getGwStatus() {
		return gwStatus;
	}

	public void setGwStatus(Integer gwStatus) {
		this.gwStatus = gwStatus;
	}

	@Override
	public String toString() {
		return "RequestCard{" +
				"id=" + id +
				", datetime=" + datetime +
				", money=" + money +
				", eqKoin=" + eqKoin +
				", note='" + note + '\'' +
				", pin='" + pin + '\'' +
				", processStatus=" + processStatus +
				", provider='" + provider + '\'' +
				", serial='" + serial + '\'' +
				", userId=" + userId +
				", username='" + username + '\'' +
				", gwTransId='" + gwTransId + '\'' +
				", gwMessage='" + gwMessage + '\'' +
				", gwStatus=" + gwStatus +
				'}';
	}
}