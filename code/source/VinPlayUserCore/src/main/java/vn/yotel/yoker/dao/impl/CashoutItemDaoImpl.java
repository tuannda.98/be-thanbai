package vn.yotel.yoker.dao.impl;

import org.hibernate.Session;
import org.hibernate.query.Query;
import vn.yotel.yoker.dao.CashoutItemDao;
import vn.yotel.yoker.domain.CashoutItem;

import java.util.List;

public class CashoutItemDaoImpl extends AbstractGenericDao<CashoutItem> implements CashoutItemDao {
    @Override
    public CashoutItem getMaxMoney() {
        CashoutItem cashoutItem = null;
        try (Session session = openSession()) {
            String hql = "FROM CashoutItem E ORDER BY E.money DESC";
            Query query = session.createQuery(hql);
            query.setMaxResults(1);
            List results = query.list();
            if (results.size() > 0)
                cashoutItem = (CashoutItem) results.get(0);
        }
        return cashoutItem;
    }
}
