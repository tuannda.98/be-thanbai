package vn.yotel.payment.buycard.entities;

/**
 * @author luongvv
 *
 */
public class Card {
	public String telco;
	public int amount;
	public String serial;
	public String pin;
	public String expiry;
	public String code;
	public String expire;

	@Override
	public String toString()
	{
		return "Card [telco=" + telco + ", amount=" + amount 
				+ ", serial=" + serial + ", pin=" + pin + ", expiry=" + expiry + ", code=" + code + "]";
	}
}
