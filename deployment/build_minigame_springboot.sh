gameName=minigame

echo "============================================================"
echo "============================================================"
echo "================== Build $gameName ============================="
echo "============================================================"
echo "============================================================"

########## YYMMDD_HHMI GMT +7 to order ##########
image=$gameName-springboot:9950_$(date +"%y%m%d_%H%M")
echo "=========> Build image $image"

docker build -t registry.gitlab.com/r99net/deployment-project/$image -f ./$gameName/Dockerfile .
docker push registry.gitlab.com/r99net/deployment-project/$image

echo "=========> Pushed image $image"

echo "=========> wait for 5 secs"

ping -n 500 -w 1000 0.0.0.1 > ./tmp.txt
