echo "============================================================"
echo "============================================================"
echo "================== Build PORTAL-DEV ========================"
echo "============================================================"
echo "============================================================"

image=api-portal:0_dev
echo "=========> Build image $image"

docker build -t registry.gitlab.com/r99net/deployment-project/$image .
docker push registry.gitlab.com/r99net/deployment-project/$image
