echo "============================================================"
echo "============================================================"
echo "================== Build APIPORTAL =============================="
echo "============================================================"
echo "============================================================"

########## YYMMDD_HHMI GMT +7 to order ##########
image=api-portal:1742_211005_1146
echo "=========> Build image $image"

docker build -t registry.gitlab.com/r99net/deployment-project/$image .
docker push registry.gitlab.com/r99net/deployment-project/$image

echo "=========> Pushed image $image"

echo "=========> wait for 5 secs"

ping -n 5 -w 1000 0.0.0.1 > ./../tmp.txt
