[
    {
        "Id": "sha256:9e815f1a9dfafdb91fe5c9c2f8ecd462b632c3e96e5ef2074d1e5e7ac87fa698",
        "RepoTags": [
            "registry.gitlab.com/hungnv2612.2/docker-bot:latest"
        ],
        "RepoDigests": [
            "registry.gitlab.com/hungnv2612.2/docker-bot@sha256:8802b64afcf715f6977111c085e5c621a3d677614b8bfff61f7120ba3139e151"
        ],
        "Parent": "",
        "Comment": "",
        "Created": "2020-05-25T14:16:21.686199788Z",
        "Container": "c71c10d5b9c66bef91c8486592457ed66086dbf78423929d6a9a518f9393177e",
        "ContainerConfig": {
            "Hostname": "c71c10d5b9c6",
            "Domainname": "",
            "User": "",
            "AttachStdin": false,
            "AttachStdout": false,
            "AttachStderr": false,
            "Tty": false,
            "OpenStdin": false,
            "StdinOnce": false,
            "Env": [
                "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
                "ASPNETCORE_URLS=http://+:80",
                "DOTNET_RUNNING_IN_CONTAINER=true",
                "DOTNET_VERSION=3.0.3"
            ],
            "Cmd": [
                "/bin/sh",
                "-c",
                "#(nop) ",
                "ENTRYPOINT [\"dotnet\" \"BotTele.dll\"]"
            ],
            "Image": "sha256:c27f257f950ac320fed786a93bb27e6f1c85f3d005036270f1517487df778df9",
            "Volumes": null,
            "WorkingDir": "/app",
            "Entrypoint": [
                "dotnet",
                "BotTele.dll"
            ],
            "OnBuild": null,
            "Labels": {}
        },
        "DockerVersion": "19.03.8",
        "Author": "",
        "Config": {
            "Hostname": "",
            "Domainname": "",
            "User": "",
            "AttachStdin": false,
            "AttachStdout": false,
            "AttachStderr": false,
            "Tty": false,
            "OpenStdin": false,
            "StdinOnce": false,
            "Env": [
                "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
                "ASPNETCORE_URLS=http://+:80",
                "DOTNET_RUNNING_IN_CONTAINER=true",
                "DOTNET_VERSION=3.0.3"
            ],
            "Cmd": null,
            "Image": "sha256:c27f257f950ac320fed786a93bb27e6f1c85f3d005036270f1517487df778df9",
            "Volumes": null,
            "WorkingDir": "/app",
            "Entrypoint": [
                "dotnet",
                "BotTele.dll"
            ],
            "OnBuild": null,
            "Labels": null
        },
        "Architecture": "amd64",
        "Os": "linux",
        "Size": 197644054,
        "VirtualSize": 197644054,
        "GraphDriver": {
            "Data": {
                "LowerDir": "/var/lib/docker/overlay2/8c43c915dd3e359e5b57cffa4ba2035f2346a46cfb1a45a91902c3fbefc37319/diff:/var/lib/docker/overlay2/c88826c703127e406bcf05bdd11367565873214834b40837f6f0d5c2919b57c3/diff:/var/lib/docker/overlay2/a290ecdb1c4e48e1e1c8b6bbf37c19e0d8a71579e0b621daa5aed02d077d4115/diff:/var/lib/docker/overlay2/fecd189d9aa7f3278f7852360404d0bbbd13be5231fdffe4824c25cdc555d429/diff:/var/lib/docker/overlay2/89eb07f6aa71fbb95bc16ed07d292ea883ce2b4a437567543d07cb925b4d78fd/diff",
                "MergedDir": "/var/lib/docker/overlay2/21c5d8316aa3f30f880336bcda27b5bf3d03bcf78c154c961f4eac88c23c7e4e/merged",
                "UpperDir": "/var/lib/docker/overlay2/21c5d8316aa3f30f880336bcda27b5bf3d03bcf78c154c961f4eac88c23c7e4e/diff",
                "WorkDir": "/var/lib/docker/overlay2/21c5d8316aa3f30f880336bcda27b5bf3d03bcf78c154c961f4eac88c23c7e4e/work"
            },
            "Name": "overlay2"
        },
        "RootFS": {
            "Type": "layers",
            "Layers": [
                "sha256:f2cb0ecef392f2a630fa1205b874ab2e2aedf96de04d0b8838e4e728e28142da",
                "sha256:29d90ae5bfcf9d30aa696882d34556f33a770b8eacd59fc9659cd3459903e180",
                "sha256:ba77a4b04402ac32ac68878a273f5d73a66429a4dd175f5451d9c8e3d3ee181a",
                "sha256:6ec5f54d57df93c78b5a1cda145f9cd04c7e41784f278e033800b91955e369b7",
                "sha256:25c320e3078b5de0076798079762788b5cc2d8621a5d9c849c1a7e6ea1e9ba04",
                "sha256:102faf706fa9badf0e0cd04069f1e729e3a17ea0e08d5457da57af38f9062470"
            ]
        },
        "Metadata": {
            "LastTagTime": "0001-01-01T00:00:00Z"
        }
    }
]