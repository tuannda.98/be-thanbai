#!/bin/bash

echo "Backup current K8s"

echo "Backup services"
kubectl get svc -o yaml > svc.yaml

echo "Backup deployment"
kubectl get deploy -o yaml > deploy.yaml

echo "Backup ingress"
kubectl get ing -o yaml > ing.yaml

echo "Backup volumes claims"
kubectl get pvc -o yaml > pvc.yaml

echo "Backup volumes"
kubectl get pv -o yaml > pv.yaml

echo "Backup config Map"
kubectl get cm -o yaml > cm.yaml

echo "Backup Secret"
kubectl get secret -o yaml > secret.yaml
