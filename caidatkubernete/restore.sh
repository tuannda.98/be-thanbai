#!/bin/bash
echo "Restore current K8s from backup files"

echo "Restore config Map"
kubectl apply -f cm.yaml

echo "Restore Secret"
kubectl apply -f secret.yaml

#echo "Restore volumes"
#kubectl apply -f pv.yaml

#echo "Restore volumes claims"
#kubectl apply -f pvc.yaml

echo "Restore deployment"
kubectl apply -f deploy.yaml

echo "Restore services"
kubectl apply -f svc.yaml

echo "Restore ingress"
kubectl apply -f ing.yaml
